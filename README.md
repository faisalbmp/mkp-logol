![Logol](./src/assets/images/logol_colored.png)

# Logol Trucking And Freight Front End Version 2

## Contents Of The Application

1. Marketplace 
    * Marketplace Trucking
    * Marketplace Freight

2. CMS
    * Admin
    * Customer


## Technologies

* react hook
* redux
* axios
* moment 
* i18next
* google map

## Installation project

* npm install
* yarn install


## run and build project

```bash

#development
npm 
start in development  => npm run start:dev
build in development => npm run build:dev

yarn 
start in development  => npm run start:dev
build in development => yarn run build:dev

#production
npm 
start in production  => npm run start:prod
build in production => npm run build:prod

yarn
start in production  => yarn run start:prod
build in production => yarn run build:prod


```