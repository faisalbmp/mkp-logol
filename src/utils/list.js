export const msContainerType = () => [
  {
    ID: 1,
    ContainerTypeID: "CNTP-0001",
    ClientID: 2,
    ExtContainerTypeID: "General/ Dry Cargo",
    Size: "20",
    CreatedDate: "2020-06-01 12:31:37",
    CreatedBy: null,
    UpdatedDate: "2020-06-01 12:31:37",
    UpdateBy: null
  },
  {
    ID: 2,
    ContainerTypeID: "CNTP-0002",
    ClientID: 2,
    ExtContainerTypeID: "General/ Dry Cargo",
    Size: "40",
    CreatedDate: "2020-06-01 12:33:10",
    CreatedBy: null,
    UpdatedDate: "2020-06-01 12:33:10",
    UpdateBy: null
  },
  {
    ID: 3,
    ContainerTypeID: "CNTP-0006",
    ClientID: 2,
    ExtContainerTypeID: "Reefer/ Refigerator",
    Size: "20",
    CreatedDate: "2020-06-01 12:33:10",
    CreatedBy: null,
    UpdatedDate: "2020-06-01 12:33:10",
    UpdateBy: null
  },
  {
    ID: 4,
    ContainerTypeID: "CNTP-0007",
    ClientID: 2,
    ExtContainerTypeID: "Reefer/ Refigerator",
    Size: "40",
    CreatedDate: "2020-06-01 12:33:10",
    CreatedBy: null,
    UpdatedDate: "2020-06-01 12:33:10",
    UpdateBy: null
  },
  {
    ID: 5,
    ContainerTypeID: "CNTP-0001",
    ClientID: 4,
    ExtContainerTypeID: "20GP,22G1",
    Size: "20",
    CreatedDate: "2020-09-08 15:57:32",
    CreatedBy: null,
    UpdatedDate: "2020-09-08 15:57:32",
    UpdateBy: null
  },
  {
    ID: 6,
    ContainerTypeID: "CNTP-0002",
    ClientID: 4,
    ExtContainerTypeID: "40GP,42GP,45GP",
    Size: "40",
    CreatedDate: "2020-09-08 15:57:43",
    CreatedBy: null,
    UpdatedDate: "2020-09-08 15:57:43",
    UpdateBy: null
  },
  {
    ID: 7,
    ContainerTypeID: "CNTP-0004",
    ClientID: 4,
    ExtContainerTypeID: "L2G0,L5G0,L5G1,LEG1,LEG8,LEVA",
    Size: "45",
    CreatedDate: "2020-09-08 15:57:51",
    CreatedBy: null,
    UpdatedDate: "2020-09-08 15:57:51",
    UpdateBy: null
  },
  {
    ID: 8,
    ContainerTypeID: "CNTP-0005",
    ClientID: 4,
    ExtContainerTypeID: "40H0,40H1,40H2,40H5,40H6,40HI,42H0,42H1,42H2,42H5,42H6,42HI",
    Size: "40",
    CreatedDate: "2020-09-08 15:58:05",
    CreatedBy: null,
    UpdatedDate: "2020-09-08 15:58:05",
    UpdateBy: null
  },
  {
    ID: 9,
    ContainerTypeID: "CNTP-0006",
    ClientID: 4,
    ExtContainerTypeID: "20R0,20R1,20R2,20R3,20RE,20RS,20RT,22R0,22R1,22R2,22R3,22R8,22RE,22RS,22RT,25R1",
    Size: "20",
    CreatedDate: "2020-09-08 15:58:14",
    CreatedBy: null,
    UpdatedDate: "2020-09-08 15:58:14",
    UpdateBy: null
  },
  {
    ID: 10,
    ContainerTypeID: "CNTP-0007",
    ClientID: 4,
    ExtContainerTypeID: "40R0,40R1,40R2,40R3,40RE,40RS,40RT,42R0,42R1,42R2,42R3,42RE,42RS,42RT,44R0,45R0,45R1,45R2,45R3,45R5,45R6,45R8,45R9,45RS,45RT,4ER1",
    Size: "40",
    CreatedDate: "2020-09-08 15:58:33",
    CreatedBy: null,
    UpdatedDate: "2020-09-08 15:58:33",
    UpdateBy: null
  }
];

export const gatepassTerminal = () => [
  {
    key: '1',
    portName: 'Tj. Priok - KOJA',
    terminal: 'PORT-0002',
  },
  {
    key: '2',
    portName: 'NPCT1',
    terminal: 'PORT-0003',
  }
];
