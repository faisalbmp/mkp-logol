export const formatPrice = (value) => {
    const formatter = new Intl.NumberFormat('IDR', {
        currency: 'IDR',
        minimumFractionDigits: 0
    })
    return "IDR " + formatter.format(value)
}