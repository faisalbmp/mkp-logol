export const months = [
  { id: 1, name: "January", abbr: "Jan" },
  { id: 2, name: "February", abbr: "Feb" },
  { id: 3, name: "March", abbr: "Mar" },
  { id: 4, name: "April", abbr: "Apr" },
  { id: 5, name: "May", abbr: "May" },
  { id: 6, name: "June", abbr: "Jun" },
  { id: 7, name: "July", abbr: "Jul" },
  { id: 8, name: "August", abbr: "Aug" },
  { id: 9, name: "September", abbr: "Sep" },
  { id: 10, name: "October", abbr: "Oct" },
  { id: 11, name: "November", abbr: "Nov" },
  { id: 12, name: "December", abbr: "Dec" },
];

export const daysOfWeek = [
  { id: 1, name: "Senin", abbr: "Sen" },
  { id: 2, name: "Selasa", abbr: "Sel" },
  { id: 3, name: "Rabu", abbr: "Rab" },
  { id: 4, name: "Kamis", abbr: "Kam" },
  { id: 5, name: "Jumat", abbr: "Jum" },
  { id: 6, name: "Sabtu", abbr: "Sab" },
  { id: 7, name: "Minggu", abbr: "Min" },
]

export const dates = [
  { id: 1, monthId: 1, bottom: 1, top: 7 },
  { id: 2, monthId: 1, bottom: 8, top: 14 },
  { id: 3, monthId: 1, bottom: 15, top: 21 },
  { id: 4, monthId: 1, bottom: 22, top: 31 },

  { id: 5, monthId: 2, bottom: 1, top: 7 },
  { id: 6, monthId: 2, bottom: 8, top: 14 },
  { id: 7, monthId: 2, bottom: 15, top: 21 },
  { id: 9, monthId: 2, bottom: 22, top: 28 },

  { id: 10, monthId: 3, bottom: 1, top: 7 },
  { id: 11, monthId: 3, bottom: 8, top: 14 },
  { id: 12, monthId: 3, bottom: 15, top: 21 },
  { id: 13, monthId: 3, bottom: 22, top: 31 },

  { id: 14, monthId: 4, bottom: 1, top: 7 },
  { id: 15, monthId: 4, bottom: 8, top: 14 },
  { id: 16, monthId: 4, bottom: 15, top: 21 },
  { id: 17, monthId: 4, bottom: 22, top: 30 },

  { id: 18, monthId: 5, bottom: 1, top: 7 },
  { id: 19, monthId: 5, bottom: 8, top: 14 },
  { id: 20, monthId: 5, bottom: 15, top: 21 },
  { id: 21, monthId: 5, bottom: 22, top: 31 },

  { id: 22, monthId: 6, bottom: 1, top: 7 },
  { id: 23, monthId: 6, bottom: 8, top: 14 },
  { id: 24, monthId: 6, bottom: 15, top: 21 },
  { id: 25, monthId: 6, bottom: 22, top: 30 },

  { id: 26, monthId: 7, bottom: 1, top: 7 },
  { id: 27, monthId: 7, bottom: 8, top: 14 },
  { id: 28, monthId: 7, bottom: 15, top: 21 },
  { id: 29, monthId: 7, bottom: 22, top: 31 },

  { id: 30, monthId: 8, bottom: 1, top: 7 },
  { id: 31, monthId: 8, bottom: 8, top: 14 },
  { id: 32, monthId: 8, bottom: 15, top: 21 },
  { id: 33, monthId: 8, bottom: 22, top: 31 },

  { id: 34, monthId: 9, bottom: 1, top: 7 },
  { id: 35, monthId: 9, bottom: 8, top: 14 },
  { id: 36, monthId: 9, bottom: 15, top: 21 },
  { id: 37, monthId: 9, bottom: 22, top: 30 },

  { id: 38, monthId: 10, bottom: 1, top: 7 },
  { id: 39, monthId: 10, bottom: 8, top: 14 },
  { id: 40, monthId: 10, bottom: 15, top: 21 },
  { id: 41, monthId: 10, bottom: 22, top: 31 },

  { id: 42, monthId: 11, bottom: 1, top: 7 },
  { id: 43, monthId: 11, bottom: 8, top: 14 },
  { id: 44, monthId: 11, bottom: 15, top: 21 },
  { id: 45, monthId: 11, bottom: 22, top: 30 },

  { id: 46, monthId: 12, bottom: 1, top: 7 },
  { id: 47, monthId: 12, bottom: 8, top: 14 },
  { id: 48, monthId: 12, bottom: 15, top: 21 },
  { id: 49, monthId: 12, bottom: 22, top: 31 },
];
