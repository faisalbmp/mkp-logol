import {
    GET_STORAGE_LIST_REQUEST,
    GET_STORAGE_LIST_SUCCESS,
    GET_STORAGE_LIST_FAILURE
  } from "actions";
  
  export default (
    state = {
        getError: false,
        storageListData : {},
    },
    action
  ) => {
    switch (action.type) {
      case GET_STORAGE_LIST_REQUEST:
        return {
          ...state,
          getError:false
        };
      case GET_STORAGE_LIST_SUCCESS:
        return {
          ...state,
          getError:false,
          storageListData: action.storageListData
        };
      case GET_STORAGE_LIST_FAILURE:
          return {
          ...state,
          getError:true,
        };     
      default:
        return state;
    }
  };