import {
    SEARCH_PORT_LIST_REQUEST,
    SEARCH_PORT_LIST_SUCCESS,
    SEARCH_PORT_LIST_FAILURE
  } from "actions";
  
  export default (
    state = {
        getError: false,
        porListData : [],
    },
    action
  ) => {
    switch (action.type) {
      case SEARCH_PORT_LIST_REQUEST:
        return {
          ...state,
          getError:false
        };
      case SEARCH_PORT_LIST_SUCCESS:
        return {
          ...state,
          getError:false,
          porListData: action.porListData
        };
      case SEARCH_PORT_LIST_FAILURE:
          return {
          ...state,
          getError:true,
        };     
      default:
        return state;
    }
  };