import {
    SEARCH_PORT_REQUEST,
    SEARCH_PORT_SUCCESS,
    SEARCH_PORT_FAILURE
  } from "actions";
  
  export default (
    state = {
        getError: false,
        searchPortData : [],
    },
    action
  ) => {
    switch (action.type) {
      case SEARCH_PORT_REQUEST:
        return {
          ...state,
          getError:false
        };
      case SEARCH_PORT_SUCCESS:
        return {
          ...state,
          getError:false,
          searchPortData: action.searchPortData
        };
      case SEARCH_PORT_FAILURE:
          return {
          ...state,
          getError:true,
        };     
      default:
        return state;
    }
  };