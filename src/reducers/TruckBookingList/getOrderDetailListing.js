import {
    GET_TRUCK_ORDER_DETAIL_LIST_REQUEST,
    GET_TRUCK_ORDER_DETAIL_LIST_SUCCESS,
    GET_TRUCK_ORDER_DETAIL_LIST_FAILURE
  } from "actions";
  
  export default (
    state = {
        getError: false,
        truckDetailOrderData : [],
    },
    action
  ) => {
    switch (action.type) {
      case GET_TRUCK_ORDER_DETAIL_LIST_REQUEST:
        return {
          ...state,
          getError:false
        };
      case GET_TRUCK_ORDER_DETAIL_LIST_SUCCESS:
        return {
          ...state,
          getError:false,
          truckDetailOrderData: action.truckDetailOrderData
        };
      case GET_TRUCK_ORDER_DETAIL_LIST_FAILURE:
          return {
          ...state,
          getError:true,
        };     
      default:
        return state;
    }
  };