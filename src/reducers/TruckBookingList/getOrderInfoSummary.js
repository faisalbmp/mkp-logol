import {
    GET_ORDER_SUMMARY_REQUEST,
    GET_ORDER_SUMMARY_SUCCESS,
    GET_ORDER_SUMMARY_FAILURE
  } from "actions";
  
  export default (
    state = {
        getError: false,
        orderSummaryData : [],
    },
    action
  ) => {
    switch (action.type) {
      case GET_ORDER_SUMMARY_REQUEST:
        return {
          ...state,
          getError:false
        };
      case GET_ORDER_SUMMARY_SUCCESS:
        return {
          ...state,
          getError:false,
          orderSummaryData: action.orderSummaryData
        };
      case GET_ORDER_SUMMARY_FAILURE:
          return {
          ...state,
          getError:true,
        };     
      default:
        return state;
    }
  };