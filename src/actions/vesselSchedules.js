import {
  GET_VESSEL_SCHEDULES,
  GET_VESSEL_SCHEDULES_SUCCESS,
  GET_VESSEL_SCHEDULES_FAILED,
  GET_SHIPPING_LINES,
  GET_SHIPPING_LINES_SUCCESS,
  GET_SHIPPING_LINES_FAILED,
} from "./types";
import { getSchedules, getShippingLines } from "services/vesselSchedules";
import { actionKecamatan } from "./locationApi";

export const getVesselSchedules = (
  origin,
  destination,
  dDate,
  aDate,
  range
) => async (dispatch) => {
  try {
    dispatch({ type: GET_VESSEL_SCHEDULES });
    const res = await getSchedules(origin, destination, dDate, aDate, range);
    if (res.status === 200) {
      dispatch({ type: GET_VESSEL_SCHEDULES_SUCCESS, payload: res.data });
      return 1;
    } else {
      dispatch({
        type: GET_VESSEL_SCHEDULES_FAILED,
        payload: res.data.message,
      });
      return 0;
    }
  } catch (err) {
    dispatch({ type: GET_VESSEL_SCHEDULES_FAILED, payload: err.message });
    return 0;
  }
};

export const fetchShippingLines = () => async (dispatch) => {
  try {
    dispatch({ type: GET_SHIPPING_LINES });
    const res = await getShippingLines();
    if (res.status === 200) {
      dispatch({ type: GET_SHIPPING_LINES_SUCCESS, payload: res.data.result });
    } else {
      dispatch({ type: GET_SHIPPING_LINES_FAILED, payload: res.data.message });
    }
  } catch (err) {
    dispatch({ type: GET_SHIPPING_LINES_FAILED, payload: err.message });
  }
};
