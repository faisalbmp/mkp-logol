import { PortApi } from "services";
import { RESPONSE_STATUS } from "utils/contants";

export const getOriginPorts = () => {
  return async (dispatch) => {
    const response = await PortApi.getOrigins();
    if (response.status === RESPONSE_STATUS.SUCCESS) {
      const ports = response.data.result;
      // const ports = [...newArr];
      for (let key in ports) {
        for (let ind in ports[key].countries) {
          for (let ind2 in ports[key].countries[ind].ports) {
            ports[key].countries[ind].ports[ind2].countryName =
              ports[key].countries[ind].countryName;
            ports[key].countries[ind].ports[ind2].countryFlag =
              ports[key].countries[ind].countryFlag;
          }
        }
      }
      const sorted = ports.sort((a, b) => {
        if (a.areaName === "Indonesia") {
          return -1;
        } else {
          return a.areaName < b.areaName ? -1 : 1;
        }
      });
      dispatch({
        type: "PORT_ORIGINS",
        payload: sorted,
      });
    }
  };
};

export const getDestinationPorts = () => {
  return async (dispatch) => {
    const response = await PortApi.getDestinations();
    if (response.status === RESPONSE_STATUS.SUCCESS) {
      const ports = response.data.result;
      for (let key in ports) {
        for (let ind in ports[key].countries) {
          for (let ind2 in ports[key].countries[ind].ports) {
            ports[key].countries[ind].ports[ind2].countryName =
              ports[key].countries[ind].countryName;
            ports[key].countries[ind].ports[ind2].countryFlag =
              ports[key].countries[ind].countryFlag;
          }
        }
      }
      const sorted = ports.sort((a, b) => {
        if (a.areaName === "Indonesia") {
          return -1;
        } else {
          return a.areaName < b.areaName ? -1 : 1;
        }
      });

      dispatch({
        type: "PORT_DESTINATIONS",
        payload: { ports: sorted },
      });
    }
  };
};

export const getAllPorts = () => {
  return async (dispatch) => {
    const response = await PortApi.getAll()
    if (response.status === RESPONSE_STATUS.SUCCESS) {
      const ports = response.data.result
      dispatch({
        type: "PORT_GET_ALL",
        payload: { ports }
      })
    }

  }
}

export const searchOriginPorts = (data) => {
  return (dispatch) => {
    dispatch({
      type: "PORT_ORIGINS_SEARCH",
      payload: data,
    });
  };
};

export const searchDestinationPorts = (data) => {
  return (dispatch) => {
    dispatch({
      type: "PORT_DESTINATIONS_SEARCH",
      payload: data,
    });
  };
};
