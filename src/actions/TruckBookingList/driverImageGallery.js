import { getTruckDetailOrderList } from '../../services/TruckBookingList/getOrderDetailListing'

export const GET_DRIVER_IMAGE_GALLERY_REQUEST = "GET_DRIVER_IMAGE_GALLERY_REQUEST"
export const GET_DRIVER_IMAGE_GALLERY_SUCCESS  = "GET_DRIVER_IMAGE_GALLERY_SUCCESS"
export const GET_DRIVER_IMAGE_GALLERY_FAILURE  = "GET_DRIVER_IMAGE_GALLERY_FAILURE"

const getTruckDetailOrderRequest = () => {
    return {
      type: GET_TRUCK_ORDER_DETAIL_LIST_REQUEST
    }
  }
  
  const getTruckDetailOrderSuccess = truckDetailOrderData => {
    return {
      type: GET_TRUCK_ORDER_DETAIL_LIST_SUCCESS,
      truckDetailOrderData
    }
  }
  
  const getTruckDetailOrderFailure = () => {
    return {
      type: GET_TRUCK_ORDER_DETAIL_LIST_FAILURE
    }
  }

export const getTruckDetailOrderData = (orderID) => async dispatch => {
    dispatch(getTruckDetailOrderRequest())
    const response = await getTruckDetailOrderList(orderID)
    if (response) {
        dispatch(getTruckDetailOrderSuccess(response))
        return true
    } else {
      dispatch(getTruckDetailOrderFailure())
      return false
    }
  }