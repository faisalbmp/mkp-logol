import React, { useState } from 'react'

export default function ConfirmationBox (props)  {
    return (
        <div className = "confirmation-modal">
           <div className = "confirmation-modal-content">
                <div className = "confirmation-modal-header" >
                    Apakah Data Sudah Benar
                    <div className = "confirmation-modal-body">
                        Pastikan data Anda sudah benar. Anda tidak bisa mengubah detail pemesanan setelah melanjutkan ke pembayaran.
                    </div>
                </div>
                <div className = "confirmation-modal-btn-container">
                    <button type="submit" className = "confirmation-modal-cancel-btn" onClick = {props.onCancelPressed}>Cek Kembali</button>
                    <button type = "submit" className = "confirmation-modal-submit-btn" onClick = {props.onSubmitPressed}>Ya, Sudah Benar</button>
                </div>
           </div>
        </div>
    )
}