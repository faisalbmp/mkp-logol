import React from 'react';

import Styles from 'assets/scss/dashboard/vendor/vendor.module.scss';

const ListContainer = props => {
	return (
		<div className={Styles.item}>
			<span style={{ marginRight: 16 }}>{props.icon}</span>
			<div>
				<div
					style={{
						display: 'flex',
						justifyContent: 'space-between',
						flexFlow: 'row',
					}}
				>
					<p
						style={{
							fontSize: 14,
							color: '#004DFF',
							fontWeight: 'bold',
						}}
					>
						{props.type}
					</p>
				</div>
				<span>{props.from}</span>
				<i
					className='icon-tail-arrow'
					style={{ paddingLeft: 8, paddingRight: 8 }}
				/>
				<span>{props.to}</span>
				<div style={{ display: 'flex', flexFlow: 'row', marginTop: 16 }}>
					<div
						className={Styles.tipeDetail}
						style={{ marginRight: 16, textAlign: 'center', padding: 4 }}
					>
						<span>20' GP : </span>
						<span
							style={{
								color: '#002985',
								fontSize: 14,
								fontWeight: 'bold',
							}}
						>
							2
						</span>
					</div>
					<div
						className={Styles.tipeDetail}
						style={{ textAlign: 'center', padding: 4 }}
					>
						<span>40' GP : </span>
						<span
							style={{
								color: '#002985',
								fontSize: 14,
								fontWeight: 'bold',
							}}
						>
							8
						</span>
					</div>
				</div>
			</div>
			<div style={{ alignItems: 'right' }}>
				<button className={`${Styles.btn} ${Styles.noProses}`}>
					{props.label}
				</button>
			</div>
		</div>
	);
};

export default ListContainer;
