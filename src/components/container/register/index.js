import React, { useState, useEffect } from 'react'
import { newOrderTruck } from 'actions'
import { useDispatch, useSelector } from 'react-redux'

import InputAuth from 'components/input/inputAuth'
import { UserIcon, EmailIcon, NoPhone, CompanyIcon } from 'components/icon/iconSvg'
import { wrepperButton, btnPrimary, hello, textWelcome, form, left, container, content, textBottom, text, blue } from 'assets/splitPage.module.scss';
import { textError, showTextEerror } from 'assets/scss/megamenu/megamenu.module.scss';
import Header from 'components/header/headerSplit'
import { validationPhone } from 'utils/validation'

export default function LeftComponent({ sliderLogin, sliderDone }) {
    const [value, setValue] = useState({ companyName: "", contactName: "", email: "", mobilePhone: "" })
    const dispatch = useDispatch()
    const [filterValue, setFilterValue] = useState([])
    const { resNewOrder } = useSelector(state => state.home);

    const { companyName, contactName, email, mobilePhone } = value

    const onChange = (e) => {
        value[e.target.name] = e.target.value
        setValue(value)
    }

    useEffect(() => {
        if (resNewOrder === 'SUCCESS') sliderDone()
    }, [resNewOrder])


    const clickDaftar = async () => {
        const filtValue = Object.values(value).filter(value => value === "")
        setFilterValue(filtValue)
        if (!filtValue.length) {
            dispatch(newOrderTruck({ customerInfo: value }))
        }
    }

    return (
        <div className={left}>
            <div className={container}>
                <Header />
                <div className={content}></div>
                <div className={hello}>Let’s get started</div>
                <div className={textWelcome}>Join Logol to compare, book, and manage your global freight.</div>

                <div className={form}>

                    <InputAuth
                        Icon={CompanyIcon}
                        title="Perusahaan"
                        name="companyName"
                        value={companyName}
                        onChange={onChange}
                        activeIcon={true}
                    />
                    <InputAuth
                        Icon={UserIcon}
                        title="Name"
                        value={contactName}
                        name="contactName"
                        onChange={onChange}
                        activeIcon={true}
                    />
                    <InputAuth
                        Icon={EmailIcon}
                        title="Email"
                        value={email}
                        name="email"
                        onChange={onChange}
                        activeIcon={true}
                    />
                    <InputAuth
                        Icon={NoPhone}
                        title="No"
                        value={mobilePhone}
                        name="mobilePhone"
                        onChange={onChange}
                        activeIcon={true}
                        activeInput={validationPhone(mobilePhone)}
                    />
                </div>
                <div className={`${textError}  ${filterValue.length ? showTextEerror : ""}`} style={{ fontSize: 14, marginTop: 20 }}>Form diatas wajib di masukan, tidak boleh kosong</div>

                <div className={wrepperButton}>
                    <button className={btnPrimary} onClick={clickDaftar}>Daftar</button>
                    <div className={textBottom} style={{ marginTop: 32 }}>
                        <span>Sudah Punya Akun? </span>
                        <span
                            className={`${text}  ${blue}`}
                            onClick={sliderLogin}
                            style={{ marginLeft: 5 }}
                        >Login</span>
                    </div>
                </div>
            </div>
        </div>
    )
}