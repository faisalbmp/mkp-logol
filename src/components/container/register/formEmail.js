import React, { useState, useEffect } from 'react'
import { requestSchedule } from 'actions'
import { useDispatch, useSelector } from 'react-redux'

import InputAuth from 'components/input/inputAuth'
import { UserIcon, EmailIcon, NoPhone, CompanyIcon } from 'components/icon/iconSvg'
import { wrepperButton, btnPrimary, hello, textWelcome, form, left, container, content } from 'assets/splitPage.module.scss';
import { textError, showTextEerror } from 'assets/scss/megamenu/megamenu.module.scss';
import Header from 'components/header/headerSplit'
import { validationPhone, validateEmail } from 'utils/validation'
import Loading from "components/loading/loadingButtonRight";
import { REQUEST_SEND_EMAIL, SUCCESS_SEND_EMAIL, ERROR_SEND_EMAIL, GETCONTAINER } from 'actions/types'
import { ArrowLeftDetail, } from 'components/icon/iconSvg'
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import useWindowDimensions from 'utils/windowDimensions'
import DetailBookingMobile from 'components/cards/DetailBookingMobile'

export default function LeftComponent({ noHeader, sliderDone }) {
    const history = useHistory()
    const [value, setValue] = useState({ companyName: "", contactName: "", email: "", mobilePhone: "" })
    const dispatch = useDispatch()
    const [valueValidat, setValidation] = useState({ companyName: false, contactName: false, email: false, mobilePhone: false })
    const [filterValue, setFilterValue] = useState([])
    const [listContainerFil, setListContainerFil] = useState([])
    const [totalPrice, setTotalPrice] = useState(0)
    const [onclick, setOnClick] = useState(false)
    const { sendEmail, home, booking } = useSelector(state => state);
    const { requestSendEmail, successSendEmail, errorSendEmail } = sendEmail
    const { companyName, contactName, email, mobilePhone } = value
    const { height, width } = useWindowDimensions()
    const { t } = useTranslation();


    const { bookingTruck, listContainer, typeBooking, typeExportImport } = home
    const { selected_origin, selected_destination, selectedDate, selectedContainer } = booking
    const { addressB, addressA, date } = bookingTruck
    useEffect(() => {
        const successSlider = async () => {
            if (successSendEmail) {
                await sliderDone()
                dispatch({ type: REQUEST_SEND_EMAIL, requestSendEmail: false });
                dispatch({ type: ERROR_SEND_EMAIL, errorSendEmail: false });
                dispatch({ type: SUCCESS_SEND_EMAIL, requestSendEmail: false });
                dispatch({
                    type: "SEARCH_SCHEDULES", payload: {
                        selected_origin: null,
                        selected_destination: null,
                        selectedDate: null,
                        selectedContainer: null
                    }
                });

                dispatch({
                    type: "BOOKINGTRUCK", bookingTruck: {
                        addressA: {},
                        addressB: {},
                        date: "",
                        container: {
                            price: 0,
                            quantity: 0
                        }
                    }
                });
                dispatch({ type: GETCONTAINER, listContainer: [] })

            }
        }
        successSlider()
    }, [successSendEmail])


    useEffect(() => {
        if (listContainer.length) {
            const listContainerFil = listContainer.filter(value => value.priceAll)
            setListContainerFil(listContainerFil)
            if (listContainerFil.length) {
                const totalPrice = listContainerFil.reduce(function (a, b) { return a + b.priceAll }, 0)
                setTotalPrice(totalPrice)
            }
        }
    }, [listContainer]);


    const onChange = (e) => {
        value[e.target.name] = e.target.value
        setValue(value)
        if (e.target.name === "mobilePhone") {
            valueValidat.mobilePhone = validationPhone(e.target.value)
            setValidation(valueValidat)
        }
        if (e.target.name === "email") {
            valueValidat.email = validateEmail(e.target.value)
            setValidation(valueValidat)
        }
    }

    const onSendEmail = async (e) => {
        e.preventDefault()
        const filtValue = await Object.values(value).filter(value => value === "")
        setFilterValue(filtValue)
        setOnClick(true)
        if (!filtValue.length && valueValidat.mobilePhone && valueValidat.email) {
            dispatch(requestSchedule(value))
        }
    }

    return (
        <div className={left}>
            <div className={container} style={{ height }}>
                {!noHeader && (
                    <React.Fragment>
                        <Header />
                        <div onClick={() => history.goBack()} style={{ cursor: "pointer" }}>
                            <ArrowLeftDetail />
                        </div>
                        {
                            <div className={content} style={{ marginTop: height <= 1000 ? 20 : 96 }}></div>
                        }
                    </React.Fragment>
                )}

                <div className={hello}>{t('requestScheduler.title')}</div>
                <div className={textWelcome}>{t('requestScheduler.subTitle')}</div>

                <div className={`${textError}  ${onclick && (!valueValidat.email && !valueValidat.mobilePhone && !filterValue.length) ? showTextEerror : ""}`} style={{ fontSize: 14, marginTop: 20 }}>{t('alert.pleaseMakeSureYourInformation')}</div>
                <div className={`${textError}  ${errorSendEmail ? showTextEerror : ""}`} style={{ fontSize: 14, marginTop: 20 }}>{t('alert.networkProblem')}</div>
                <div className={`${textError}  ${!valueValidat.email && valueValidat.mobilePhone && onclick && !filterValue.length ? showTextEerror : ""}`} style={{ fontSize: 14, marginTop: 20 }}>{t('alert.invalidEmail')}</div>
                <div className={`${textError}  ${!valueValidat.mobilePhone && valueValidat.email && onclick && !filterValue.length ? showTextEerror : ""}`} style={{ fontSize: 14, marginTop: 20 }}>{t('alert.invalidPhoneNumber')}</div>

                <div className={form} style={{ marginTop: height <= 1000 ? 20 : 40 }}>
                    <InputAuth
                        Icon={CompanyIcon}
                        title={t('requestScheduler.fieldCompany')}
                        name="companyName"
                        value={companyName}
                        onChange={onChange}
                        type={"text"}
                        onclick={onclick}
                    />
                    <InputAuth
                        Icon={UserIcon}
                        title={t('requestScheduler.fieldName')}
                        value={contactName}
                        name="contactName"
                        onChange={onChange}
                        type={"text"}
                        onclick={onclick}
                    />
                    <InputAuth
                        isValidateView
                        Icon={EmailIcon}
                        title={t('requestScheduler.fieldEmail')}
                        value={email}
                        name="email"
                        onChange={onChange}
                        type={"email"}
                        onclick={onclick}
                    />
                    <InputAuth
                        isValidateView
                        Icon={NoPhone}
                        title="No"
                        value={mobilePhone}
                        name="mobilePhone"
                        onChange={onChange}
                        type={"number"}
                        onclick={onclick}
                    />
                </div>
                {
                    width <= 1000 ?
                        <DetailBookingMobile
                            selectedContainer={selectedContainer}
                            typeBooking={typeBooking}
                            listContainer={listContainerFil}
                            date={typeBooking === "freight" ? selectedDate : date}
                            totalPrice={totalPrice}
                            addressB={typeBooking === "freight" ? selected_destination : addressB}
                            addressA={typeBooking === "freight" ? selected_origin : addressA}
                            typeExportImport={typeExportImport}
                        /> : ""
                }
                <div className={wrepperButton} >
                    <button className={btnPrimary} onClick={onSendEmail} style={{ marginTop: height <= 1000 ? 20 : 40, marginBottom: 32 }}>
                        {
                            requestSendEmail ?
                                <Loading />
                                :
                                <a>{t('requestScheduler.sendInquiry')}</a>
                        }
                    </button>
                </div>
            </div>
        </div>
    )
}