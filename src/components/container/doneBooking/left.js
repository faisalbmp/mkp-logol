import React from 'react'
import { useHistory } from 'react-router-dom'
import { useSelector } from 'react-redux'

import { text3, text4, hello, btnLeft, information, text5, left, container, content } from 'assets/splitPage.module.scss';
import { LeftArrowIcon, CallIcon, MailIcon } from 'components/icon/iconSvg'
import Header from 'components/header/headerSplit';
import { useTranslation } from 'react-i18next';

export default function DoneBooking() {
    const history = useHistory()
    const { idBooking } = useSelector(state => state.home)
    const publicUrl = process.env.PUBLIC_URL

    const { t } = useTranslation();

    return (
        <div className={left}>
            <div className={container}>
                <Header />
                <div className={content}>
                    <div className={hello}>{t('requestScheduler.doneBooking.orderReceived')}</div>
                    <div className={text3}>{t('requestScheduler.doneBooking.yourOrderNumber')} {idBooking}</div>
                    <div className={text3} style={{ marginTop: 32 }}>{t('requestScheduler.doneBooking.confirmationOrder')}</div>
                    <button className={btnLeft} onClick={() => history.push(publicUrl + "/")}><LeftArrowIcon /> {t('requestScheduler.doneBooking.backToMainPage')}</button>
                    <div className={text4}>{t('requestScheduler.doneBooking.contactUs')}:</div>
                    <div className={information}>
                        <CallIcon />
                        <span className={text5}>+62 21 2452 3147</span>
                    </div>
                    <div className={information}>
                        <MailIcon />
                        <span className={text5}>Info@logol.co.id</span>
                    </div>
                </div>
            </div>
        </div>
    )
}