import React, { useState, Component } from 'react'
import Modal from "../../map/WpModalTruck"
import { CancelModalIcon, PrevButton, NextButton } from "components/icon/iconSvg"
import InputText from "../../input/inputText"
import Map from 'components/map/MapDirectionsRenderer';
import stylesGoogleMap from 'locales/stylesGoogleMap.json'
import {
    withGoogleMap,
    GoogleMap,
    withScriptjs,
    Marker,
    DirectionsRenderer
} from "react-google-maps";
const googleMapsApiKey = process.env.REACT_APP_GOOGLE_MAP;

// const places = [
//     { latitude: 25.8103146, longitude: -80.1751609 },
//     { latitude: 28.4813018, longitude: -81.4387899 }
// ]

const truckInfo = [
    {
        "title": 'Name Driver',
        "value": "Aben"
    },
    {
        "title": 'No. Plat',
        "value": "B 9051 NEH"
    },
    {
        "title": 'No. Kontainer',
        "value": "OOCL1234567"
    },
    {
        "title": 'Status',
        "value": "Proses Di Pelabuhan"
    }
]
export default class OrderMap extends React.Component {
    state = {
        places :  []
    }
    componentWillMount () {
        
    }
    componentWillReceiveProps () {
        const { coordinates } = this.props
        const places = []
        var coord = {latitude: "", longitude: ""}
        coord.latitude = parseFloat(coordinates.destination1lat)
        coord.longitude = parseFloat(coordinates.destination1lng)
        places.push(coord);
        if(coordinates.destination3lat == "" && coordinates.destination3lng == "") {
            var coord = {latitude: "", longitude: ""}
            coord.latitude = parseFloat(coordinates.destination2lat)
            coord.longitude = parseFloat(coordinates.destination2lng)
            places.push(coord);
        } else {
            var coord = {latitude: "", longitude: ""}
            coord.latitude = parseFloat(coordinates.destination3lat)
            coord.longitude = parseFloat(coordinates.destination3lng)
            places.push(coord);
        }
        this.setState({places:places}, () => {
        })
    }
    render() {
        const {
            loadingElement,
            containerElement,
            mapElement,
            coordinates,
            data
            // defaultCenter,
            // defaultZoom
        } = this.props;
        console.log('data === ', data)
        const { places } = this.state;
        return (
            <Modal visible={this.props.visible} cardStyle={{ width: '1026px', height: '521px', padding: '0px' }}>
                <div className="gallery">
                    <div className="gallery-modal-header">
                        <div className="gatepass-modal-title">
                            <div className="gatepass-title" >Lacak Posisi</div>
                        </div>
                        <div onClick = {this.props.onCancel}><CancelModalIcon /></div>
                    </div>
                    <div className="map-container">
                        <Map
                            googleMapURL={
                                'https://maps.googleapis.com/maps/api/js?key=' +
                                googleMapsApiKey +
                                '&libraries=geometry,drawing,places'
                            }
                            markers={places}
                            loadingElement={loadingElement || <div className="map-container" />}
                            containerElement={containerElement || <div className="map-container" />}
                            mapElement={mapElement || <div className="map-container" />}
                            defaultCenter={{ lat: 25.798939, lng: -80.291409 }}
                            defaultZoom={11}
                        />
                        <div className="truck-info">
                            <div className="truck-info-list">
                                <li className="truck-info-li">
                                    <div>
                                        {'Name Driver'}
                                    </div>
                                    <div>
                                        {data.driverName}
                                    </div>
                                </li>
                                <li className="truck-info-li">
                                    <div>
                                        {'NO. Plat'}
                                    </div>
                                    <div>
                                        {data.vehicleNumber}
                                    </div>
                                </li>
                                <li className="truck-info-li">
                                    <div>
                                        {'NO. Konatiner'}
                                    </div>
                                    <div>
                                        {data.containerNumber}
                                    </div>
                                </li>
                                <li className="truck-info-li">
                                    <div>
                                        {'Status'}
                                    </div>
                                    <div>
                                        {data.containerNumber}
                                    </div>
                                </li>
                            </div>
                        </div>
                    </div>
                </div>
            </Modal>
        )
    }
}