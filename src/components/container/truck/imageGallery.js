import React, { useState, Component } from 'react'
import Modal from "../../map/WpModalTruck"
import { CancelModalIcon, PrevButton, NextButton } from "components/icon/iconSvg"
import InputText from "../../input/inputText"
export default class ImageGallery extends React.Component {
    state = {
        imageIndex: 0
    }
    prevBtnClicked = () => {
        const { imageIndex } = this.state;
        if(imageIndex == 0) {
            return ;
        } 
        this.setState({
            imageIndex : this.state.imageIndex - 1
        })
    }
    nextBtnClicked = () => {
        const { imageIndex } = this.state;
        const { gallery } = this.props;
        if(imageIndex == gallery.length - 1) {
            return ;
        } 
        this.setState({
            imageIndex : this.state.imageIndex + 1
        })
    }
    render() {
        const { imageIndex } = this.state;
        const { gallery } = this.props
        return (
            <Modal visible = {this.props.visible} cardStyle={{width:'1026px', height:'685px', padding:'0px'}}>
                <div className = "gallery">
                    <div className = "gallery-modal-header">
                        <div className = "gatepass-modal-title">
                            <div className = "gatepass-title" >Foto</div>
                        </div>
                        <div onClick = {this.props.closeGallery}><CancelModalIcon/></div>
                    </div>
                    <div className = "gallery-body">
                        <img src={gallery[imageIndex].url} className = "gallery-body" />
                        <div className = "img-btn">
                            <div className = "img-prev-btn" onClick = {this.prevBtnClicked}>
                                <PrevButton/>
                            </div>
                            <div className = "img-prev-btn" onClick = {this.nextBtnClicked}>
                                <NextButton/>
                            </div>
                        </div>
                        <div className = "img-info">
                            <div className = "img-thumnail">
                                {gallery.type}
                            </div>
                            <div className = "img-date">
                                07, Agustus 2020
                            </div>
                        </div>
                    </div>
                    <div className = "img-list-container">
                        {
                            gallery.map((item, index) => {
                               return (
                                    <div key = {index} className = "img-list" onClick = {() => this.setState({imageIndex : index})}><img src={item.url} className = "img-list"/> </div> 
                                ) 
                            })
                        }
                    </div>
                </div>
            </Modal>
        )
    }
}