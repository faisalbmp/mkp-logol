import React from 'react'

import completedTruckImg from 'assets/images/bg/completed-truck.jpg'
import { contentBg } from 'assets/splitPage.module.scss';

export default function RightDone() {
    return (
        <div className={`${contentBg}`} style={{ backgroundImage: `url(${completedTruckImg})` }}>
        </div>
    )
}