import React from 'react'
import { RoundAIcon, RoundBIcon, DonwloadIfIcon, RrrowTrack } from 'components/icon/iconSvg'
import { text, container, center, card, content, title1, title2, flex, total, track, pdRight, label, bottomPosition, textFormat, feeDetail, wrRoundIcon } from 'assets/scss/component/detailBooking.module.scss'
import { formatPrice } from 'utils/function'
import moment from 'moment'
import { containerType } from 'utils/contants';
import { useTranslation } from 'react-i18next';

function DetailBooking({ date, totalPrice, addressB, addressA, listContainer, typeExportImport }) {
    const addressAStyle = <div className={`${text} ${pdRight}`}>{addressA.terms ? `${addressA.description}` : ""}</div>
    const addressBStyle = <div className={`${text} ${pdRight}`}>{Object.keys(addressB).length ? addressB.portUTC : ''}</div>
    const { t } = useTranslation();
    return (
        <div className={center}>
            <div className={container}>
                <div className={card}>
                    <div className={content}>
                        <div className={title1}>{t('requestScheduler.detailBooking.title')}</div>

                        <div className={label}>{t('requestScheduler.detailBooking.deliveryRoute')}</div>

                        <div className={track} style={{ flexDirection: 'column', alignItems: 'flex-start', marginTop: 12 }}>
                            <div className={track}>
                                <div className={wrRoundIcon}>
                                    <RoundAIcon pdRight={pdRight} />
                                </div>
                                {typeExportImport === "export" ? addressAStyle : addressBStyle}
                            </div>
                            <RrrowTrack pdRight={`${pdRight}   ${bottomPosition}`} />
                            <div className={track}>
                                <div className={wrRoundIcon}>
                                    <RoundBIcon pdRight={pdRight} />
                                </div>
                                {typeExportImport === "export" ? addressBStyle : addressAStyle}
                            </div>
                        </div>

                        <div className={label} style={{ marginTop: 17 }}>{t('requestScheduler.detailBooking.deliveryTime')}</div>
                        {/* <div className={text}>01 - 07 Januari 2020 ( Minggu 1 )</div> */}
                        <div className={text} >{date ? moment(date).format("DD-MM-YYYY") : ""}</div>

                        {/* <div className={label} style={{ marginTop: 17 }}>Jenis Pengiriman</div>
                        <div className={text}>Ekspor</div> */}
                    </div>
                    <div className={content}>
                        <div className={title2}>{t('requestScheduler.detailBooking.serviceFee')}</div>
                        {
                            listContainer.map((value, index) => (
                                <div className={`${flex} ${textFormat}`} key={index}>
                                    <div className={feeDetail}>{value.quantity} x {value.containerType}</div>
                                    <div className={feeDetail}>{formatPrice(value.priceAll)}</div>
                                </div>
                            ))
                        }
                        {/* <div className={`${flex} ${textFormat}`}>
                            <div className={feeDetail}>5 x 20’ General Purpose</div>
                            <div className={feeDetail}>IDR 1.000.000</div>
                        </div>
                        <div className={`${flex} ${textFormat}`}>
                            <div className={feeDetail}>5 x 20’ General Purpose</div>
                            <div className={feeDetail}>IDR 1.000.000</div>
                        </div>
                        <div className={`${flex} ${textFormat}`}>
                            <div className={textV}>Voucher Potongan Harga</div>
                            <div className={textV}>IDR 500.000</div>
                        </div> */}
                        <div className={`${flex} ${textFormat}`}>
                            <div className={total}>Total</div>
                            <div className={total}>{formatPrice(totalPrice)}</div>
                        </div>
                        {/* <div className={totalFalse}>IDR 12.100.000</div> */}
                    </div>
                </div>

                {/* <div className={wrDonwload}>
                    <div className={textD}>Simpan Rincian Pengiriman</div>
                    <div className={btnDonwload}>
                        <DonwloadIfIcon />
                        <div className={textD}>Unduh Informasi</div>
                    </div>
                </div> */}
            </div>
        </div >
    )
}

DetailBooking.defaultProps = {
    addressB: {},
    addressA: {}
    // listContainer: []
}

export default DetailBooking