import React, { useState, useEffect } from 'react'
import { CardOrderIcon } from "components/icon/iconSvg"
// import Card from "../../components/card"
import { CardTruckOrderDetail } from 'components/cards';
import { ArrowDownD, ArrowUpD, ArrowRightPrimary, TruckOrderDeliveryIcon, TruckOrderPortIcon, GatePassIcon, SortIcon, TruckOrderFactoryIcon, GalleryIcon, OrderMarkerIcon } from 'components/icon/iconSvg'
import SearchInput from '../../dashboard/searchInput'
import Breadcrumb from 'components/dashboard/breadcrumb'
import Select from '../../dashboard/select'
import GatePass from "./gatePass"
import ImageGallery from "./imageGallery"
import OrderMap from "./map"
import { useDispatch, useSelector } from "react-redux"
import { getOrderInfoSummaryData } from "actions/TruckBookingList/getOrderInfoSummary"
import { getTruckDetailOrderData } from "actions/TruckBookingList/getOrderDetailListing"
import Pagination from "../../paginations/pagination"
import { Redirect } from 'react-router-dom';
const titleTable = [
    { name: "Vendor Truk", iconSort: true, field : 'vendorName'  },
    { name: "Name Driver", iconSort: true , field : 'driverName'  },
    { name: "No. Kendaraan", iconSort: true, field : 'vehicleNumber'  },
    { name: "No. Kontainer", iconSort: true, field : 'containerNumber'  },
    { name: "Tipe", iconSort: true, field : 'orderType'  },
    { name: "No. Seal", iconSort: true, field : 'sealNumber'  },
    { name: "Status", iconSort: true, field : 'vendorOrderStatus'  },
    { name: "Aksi", iconSort: false, field : ''  },
]

const dataTable = [
    { vendorTruck: "PT. Anugrah Mulia Trasindo", nameDriver: "Abeng", noKendaraan: " B 9051 NEH", noKontainer: "OOCL1234567", tipe: "20’ GP", noSeal: "TSB2058312", status: { type: "no-proses", name: "Dalam Proses" }, eye: true },
    { vendorTruck: "PT. Anugrah Mulia Trasindo", nameDriver: "Abeng", noKendaraan: " B 9051 NEH", noKontainer: "OOCL1234567", tipe: "20’ GP", noSeal: "TSB2058312", status: { type: "no-proses", name: "Dalam Proses" }, eye: true },
    { vendorTruck: "PT. Anugrah Mulia Trasindo", nameDriver: "Abeng", noKendaraan: " B 9051 NEH", noKontainer: "OOCL1234567", tipe: "20’ GP", noSeal: "TSB2058312", status: { type: "no-proses", name: "Dalam Proses" }, eye: true },
    { vendorTruck: "PT. Anugrah Mulia Trasindo", nameDriver: "Abeng", noKendaraan: " B 9051 NEH", noKontainer: "OOCL1234567", tipe: "20’ GP", noSeal: "TSB2058312", status: { type: "no-proses", name: "Dalam Proses" }, eye: true },
    { vendorTruck: "PT. Anugrah Mulia Trasindo", nameDriver: "Abeng", noKendaraan: " B 9051 NEH", noKontainer: "OOCL1234567", tipe: "20’ GP", noSeal: "TSB2058312", status: { type: "no-proses", name: "Dalam Proses" }, eye: true },
]

const googleMapsApiKey = process.env.REACT_APP_GOOGLE_MAP;

const places = [
    { latitude: 25.8103146, longitude: -80.1751609 },
    { latitude: 28.4813018, longitude: -81.4387899 }
]

const galleryData = [
    {
        "url": 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Freightliner_M2_106_6x4_2014_%2814240376744%29.jpg/1200px-Freightliner_M2_106_6x4_2014_%2814240376744%29.jpg'
    },
    {
        "url": 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Freightliner_M2_112_2014_%2814270948436%29.jpg/1600px-Freightliner_M2_112_2014_%2814270948436%29.jpg'
    }
]
export default function TruckOrderDetail(props) {
    const [selectVal, setSelect] = useState(1)
    const {
        loadingElement,
        containerElement,
        mapElement,
    } = props;
    const [showGallery, setGalleryVisibility] = useState(false)
    const [gallery, setGalleryData] = useState(galleryData);
    const [showMap, setMapVisibility] = useState(false)
    const [coordinates, setCoordinates] = useState({destination1lat:"", destination1lng:"", destination2lat:"", destination2lng:"", destination3lat:"", destination3lng:""})
    const [mapData, setMapData] = useState('')
    const dispatch = useDispatch()
    const [currentPage, setCurrentPage] = useState(1);
    const [postsPerPage] = useState(5);
    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;
    const [showGatePass, setGatePassVisibility] = useState(false)
    const [sortUpindex, setsortUpindex] = useState('')
    const [sortDownindex, setsortDownindex] = useState('')

    useEffect(() => {
        dispatch(getOrderInfoSummaryData(props.orderID));
        dispatch(getTruckDetailOrderData(props.orderID, 'orderManagementID', false, ''));

    }, []);

    const orderSummaryData = useSelector(state => state.getOrderInfoSummary.orderSummaryData)
    const truckOrderDetailList = useSelector(state => state.getOrderDetailListing.truckDetailOrderData)
    console.log('orderSummaryData === ', orderSummaryData)
    var currentPosts = [];
    if (truckOrderDetailList.data != undefined) {
        currentPosts = truckOrderDetailList.data.slice(indexOfFirstPost, indexOfLastPost);
    }


    const paginate = (pageNumber) => {
        setCurrentPage(pageNumber);
    }

    const paginatePrevious = () => {
        setCurrentPage(currentPage - 1)

    }

    const paginateNext = () => {
        setCurrentPage(currentPage + 1)
    }

    const openGallery = (val) => {
        setGalleryData(val.driverImages)
        setGalleryVisibility(true)
    }

    const openMap = (value) => {
        coordinates.destination1lat = value.dest1Lat
        coordinates.destination1lng = value.dest1Lng
        coordinates.destination2lat = value.dest2Lat
        coordinates.destination2lng = value.dest2Lng
        coordinates.destination3lat = value.dest3Lat
        coordinates.destination3lng = value.dest3Lng
        setMapData(value)
        setMapVisibility(true)
    }

    const sortUpClick = (val, index) => {
        setsortUpindex(index)
        setsortDownindex('')
        dispatch(getTruckDetailOrderData(props.orderID, val, true, ''));
    }

    const sortDownClick = (val, index) => {
        setsortDownindex(index)
        dispatch(getTruckDetailOrderData(props.orderID, val, false, ''));
        setsortUpindex('')
    }

    const searchOrder = (text) => {
        dispatch(getTruckDetailOrderData(props.orderID, 'orderManagementID', false, text.target.value))
    }

    const navigateToBack = () => {
        props.navigateBack(true)
    }

    return (
        <div className="truck">
            <div className="header-truk">
                <div className="truck-order-detail-breadcrum">
                    <Breadcrumb title="No. Pesanan : 004112513" onSecondNamePressed = {navigateToBack}/>
                    <div className="truck-order-detail-si">
                        SI : SI/16/07/2020
                    </div>    
                </div>
                <div className="btn-dsb-right" onClick={() => setGatePassVisibility(true)} style={{ cursor: 'pointer' }}>
                    Request Gatepass
                    <GatePassIcon />
                </div>
            </div>
            <div className="truck-order-detail" >
                <div className="load-date" >Tanggal Muat Barang 15 Jan, 2020</div>
                <div className="card-wrapper" >
                    <CardTruckOrderDetail value={orderSummaryData.totalAcceptedOrder} title={"Total Pesanan"} icon={<img src={require('assets/images/svg/order.svg')} />} />
                    <CardTruckOrderDetail value={orderSummaryData.totalDest1Out} title={"Proses Di Pabrik"} icon={<TruckOrderFactoryIcon />} />
                    <CardTruckOrderDetail value={orderSummaryData.totalDest3In} title={"Proses Di Pelabuhan"} icon={<TruckOrderPortIcon />} />
                    <CardTruckOrderDetail value={orderSummaryData.totalOnDeliverOrder} title={"Pengiriman Selesai"} icon={<TruckOrderDeliveryIcon />} />
                </div>
                <div className="wrepper-table truck-order-table">
                    <div className="middle-component" style={{ marginTop: '0px' }}>
                        <SearchInput onChange = {searchOrder} />
                        <div className="right">
                            <div className="sort-item">
                                <SortIcon />
                                <div className="text">Urutkan</div>
                            </div>

                            <div>
                                <Select
                                    number={100}
                                    setSelect={setSelect}
                                    defaultValue={10}
                                />
                            </div>
                        </div>
                    </div>

                    <table className="table">
                        <tr className="tr">
                            {
                                titleTable.map((value, index) => (
                                    <td key={index} className="td">
                                        <div>{value.name}</div>
                                        {value.iconSort ? <div className="icon-sort">
                                            <div onClick = {() => sortUpClick(value.field, index)} ><ArrowUpD active={sortUpindex === index ? true : false} /></div>
                                            <div onClick = {() => sortDownClick(value.field, index)} ><ArrowDownD active={sortDownindex === index ? true : false} /></div>
                                        </div> : ""}
                                    </td>
                                ))
                            }
                        </tr>
                        {
                            currentPosts.map((value, index) => {
                                var status = "" 
                                if(value.vendorOrderStatus == "0") {
                                    status = "Assigned"
                                } else if(value.vendorOrderStatus == "1") {
                                    status = "Receive "
                                } else if(value.vendorOrderStatus == "2") {
                                    status = "Destination 1 In"
                                } else if(value.vendorOrderStatus == "3") {
                                    status = "Destination 1 Out"
                                } else if(value.vendorOrderStatus == "4") {
                                    status = "Destination 2 In"
                                }  else if(value.vendorOrderStatus == "5") {
                                    status = "Destination 2 Out"
                                }  else if(value.vendorOrderStatus == "6") {
                                    status = "Destination 3 In"
                                }  else if(value.vendorOrderStatus == "7") {
                                    status = "Destination 3 Out"
                                }  else if(value.vendorOrderStatus == "8") {
                                    status = "Delivered"
                                }  else if(value.vendorOrderStatus == "9") {
                                    status = "Reject"
                                } else if(value.vendorOrderStatus == "10") {
                                    status = "Cancelled"
                                } else if(value.vendorOrderStatus == "11") {
                                    status = "Export Limit"
                                }
                                return(
                                    <div key={index} className="tr">
                                        <div className="td">{value.vendorName}</div>
                                        <div className="td">{value.driverName}</div>
                                        <div className="td">{value.vehicleNumber}</div>
                                        <div className="td">{value.containerNumber}</div>
                                        <div className="td">{value.orderType}</div>
                                        <div className="td">{value.sealNumber}</div>
                                        <div className="td">
                                            <button className={"btn " + value.orderType}>{status}</button>
                                        </div>
                                        <div className="td"><div onClick={() => console.log("true")} style={{ cursor: 'pointer' }}>{<div style={{ display: 'flex', flexDirection: 'row' }}><div onClick={() => openMap(value)}><OrderMarkerIcon color={value.eye ? "#F07575" : "#868A92"} /></div><div onClick={() => openGallery(value)}><GalleryIcon color={value.eye ? "#0AC2B7" : "#868A92"} /></div></div>}</div></div>
                                    </div>
                                )
                            })
                        }
                    </table>
                    <Pagination
                        postsPerPage={postsPerPage}
                        totalPosts={truckOrderDetailList.data != undefined ? truckOrderDetailList.data.length : 0}
                        paginate={paginate}
                        currentPage={currentPage}
                        paginatePrevious={paginatePrevious}
                        paginateNext={paginateNext}
                        currentPage={currentPage}
                    />
                </div>
                <GatePass visible={showGatePass} onCancelPressed={() => setGatePassVisibility(false)} />
                <ImageGallery visible={showGallery} gallery={galleryData} closeGallery={() => setGalleryVisibility(false)} />
                <OrderMap visible={showMap} coordinates = {coordinates} data = {mapData} onCancel = {() => setMapVisibility(false)}/>
            </div>
        </div>
    )
}