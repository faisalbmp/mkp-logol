import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { formatPrice } from 'utils/function'
import moment from 'moment'

import { container, header, content, lineAT, btnDetail, btnPrimary, btnLight, font14, track } from 'assets/splitPage.module.scss';
import { seeDetailSt, total, flex, title2, text, pdRight, label, totalFalse } from 'assets/scss/component/detailBooking.module.scss'
import { ArrowLeftDetail, RightArrowIcon, RrrowTrack, RoundAIcon, RoundBIcon, PlusCircleIcon } from 'components/icon/iconSvg'
import voucher from 'assets/images/card/promo-banner.png'
import Switch from 'components/button/switch'

export default function AddTruck({ sliderRegister }) {
    let history = useHistory();
    const { rateContainer, bookingTruck, listContainer } = useSelector(state => state.home)
    const [listContainerFil, setListContainerFil] = useState([])
    const [totalPrice, setTotalPrice] = useState(0)
    const [switchCheck, switchChange] = useState(false)
    const { addressB, addressA, date } = bookingTruck

    useEffect(() => {
        if (listContainer.length) {
            const listContainerFil = listContainer.filter(value => value.priceAll)
            setListContainerFil(listContainerFil)
            if (listContainerFil.length) {
                const totalPrice = listContainerFil.reduce(function (a, b) { return a + b.priceAll }, 0)
                setTotalPrice(totalPrice)
            }
        }
    }, []);
    return (
        <div className={container}>
            <div className={header}>
                <div onClick={() => history.goBack()}>
                    <ArrowLeftDetail />
                </div>
            </div>
            <div className={content} style={{ marginTop: 0 }}>
                <div className={title2} style={{ fontSize: 20 }}>Detail Kontainer</div>
                <div className={seeDetailSt}>
                    <div className={title2} style={{ margin: "32px 0px 15px 0px" }}>Biaya Layanan</div>

                    <div>
                        {
                            listContainerFil.map((value, index) => (
                                <div className={`${flex} ${font14}`} key={index}>
                                    <div>{value.quantity} x {value.containerTypeID}</div>
                                    <div style={{ width: 168 }}>{formatPrice(value.priceAll)}</div>
                                </div>
                            ))
                        }
                        <div className={flex} style={{ marginTop: 8 }} >
                            <div></div>
                            <div className={flex} style={{ width: 168, alignItems: 'center' }}>
                                <div className={lineAT} style={{ marginRight: 24 }}></div>
                                <PlusCircleIcon />
                            </div>
                        </div>

                        <div className={`${flex} ${font14}`}>
                            <div className={total}>Total</div>
                            <div style={{ width: 168 }}>
                                <div className={total}>{formatPrice(totalPrice)}</div>
                                {/* <div className={totalFalse} style={{ textAlign: "start" }}>IDR 12.100.000</div> */}
                            </div>
                        </div>
                    </div>
                </div>
                <div className={lineAT} style={{ margin: "40px 0px 40px 0px" }}></div>
                <div className={title2} style={{ marginTop: 32 }}>Rincian Pengiriman</div>
                <div className={label} style={{ marginTop: 24 }}>Rute Pengiriman</div>
                <div className={track} style={{ marginTop: 8 }}>
                    <RoundAIcon pdRight={pdRight} />
                    <div className={`${text} ${pdRight}`}> {addressA.terms ? `${addressA.terms[addressA.terms.length - 4].value}  ${addressA.terms[addressA.terms.length - 3].value}` : ""}</div>
                    <RrrowTrack pdRight={pdRight} />
                    <RoundBIcon pdRight={pdRight} />
                    <div className={`${text} ${pdRight}`}> {addressB.portUTC}</div>
                </div>
                <div className={label} style={{ marginTop: 24 }}>Waktu Pengiriman</div>
                <div className={text} style={{ marginTop: 8 }}>{moment(date).format("DD-MM-YYYY")}</div>

                {/* <div className={lineAT} style={{ margin: "40px 0px 40px 0px" }}></div> */}
                {/* <div className={flex} style={{ marginBottom: 16 }}>
                    <div className={title2}>Promo Saat Ini</div>

                    <div className={flex} style={{ margin: "auto 0px auto 0px" }}>
                        <div style={{ fontSize: 12, marginRight: 16 }}>Gunakan sekarang</div>
                        <Switch onChange={switchChange} switchCheck={switchCheck} />
                    </div>
                </div>
                <img src={voucher} style={{ height: 102, width: 456 }} /> */}
                <div style={{ display: "flex", justifyContent: "flex-start" }}>
                    <button
                        className={btnLight}
                        style={{ width: 213, marginRight: 27 }}
                        onClick={() => history.goBack()}
                    >Ubah Pencarian</button>
                    <button
                        className={`${btnPrimary} ${btnDetail}`}
                        style={{ width: 216 }}
                        onClick={sliderRegister}
                    >
                        Berikutnya
                                <RightArrowIcon style={{ marginLeft: 20 }} />
                    </button>
                </div>

            </div>
        </div >
    )
}
