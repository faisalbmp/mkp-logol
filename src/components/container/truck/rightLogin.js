import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux';

import { contentBg } from 'assets/splitPage.module.scss';
import DetailsBookingTruck from 'components/container/truck/detailsBooking'
import DetailsBookingFreight from 'components/container/freight/detailsBooking'
// import bgLoginTruck from 'assets/images/bg/login-truck.jpg'
import bgLoginTruck from 'assets/images/bg/bg-detail-login.jpg'

export default function RightLogin() {
    const { home, booking } = useSelector(state => state)
    const { bookingTruck, listContainer, typeBooking, typeExportImport } = home
    const { selected_origin, selected_destination, selectedDate, selectedContainer } = booking
    const [listContainerFil, setListContainerFil] = useState([])
    const [totalPrice, setTotalPrice] = useState(0)
    const { addressB, addressA, date } = bookingTruck
    useEffect(() => {
        if (listContainer.length) {
            const listContainerFil = listContainer.filter(value => value.priceAll)
            setListContainerFil(listContainerFil)
            if (listContainerFil.length) {
                const totalPrice = listContainerFil.reduce(function (a, b) { return a + b.priceAll }, 0)
                setTotalPrice(totalPrice)
            }
        }
    }, [listContainer]);

    return (
        <div className={`${contentBg} `} style={{ backgroundImage: `url(${bgLoginTruck})` }}>
            {
                typeBooking === "freight" ?
                    <DetailsBookingFreight
                        selectedContainer={selectedContainer}
                        date={selectedDate}
                        addressB={selected_destination}
                        addressA={selected_origin}
                    />
                    :
                    <DetailsBookingTruck
                        listContainer={listContainerFil}
                        date={date}
                        totalPrice={totalPrice}
                        addressB={addressB}
                        addressA={addressA}
                        typeExportImport={typeExportImport}
                    />
            }
        </div>
    )
}




// import React, { useState, useEffect } from 'react'
// import { useSelector } from 'react-redux';

// import { contentBg } from 'assets/splitPage.module.scss';
// import DetailsBookingTruck from 'components/truck/detailsBooking'
// import DetailsBookingFreight from 'components/freight/detailsBooking'
// // import bgLoginTruck from 'assets/images/bg/login-truck.jpg'
// import bgLoginTruck from 'assets/images/bg/bg-detail-login.jpg'

// export default function RightLogin() {
//     const { home, booking } = useSelector(state => state)
//     const { bookingTruck, listContainer, typeBooking, typeExportImport } = home
//     const { selected_origin, selected_destination, selectedDate, selectedContainer } = booking
//     const [listContainerFil, setListContainerFil] = useState([])
//     const [totalPrice, setTotalPrice] = useState(0)
//     const { addressB, addressA, date, container: { price, quantity } } = bookingTruck
//     useEffect(() => {
//         if (listContainer.length) {
//             console.log(listContainer)
//             const listFilt = listContainer.filter(value => value.priceAll)
//             console.log(listFilt)
//             const totalPriceT = listFilt.length ? listFilt.reduce(function (a, b) {
//                 return a + b.priceAll;
//             }, 0) : 0
//             console.log(totalPriceT)
//             setTotalPrice(totalPriceT)
//         }
//     }, [listContainer]);

//     console.log(totalPrice)

//     return (
//         <div className={`${contentBg} `} style={{ backgroundImage: `url(${bgLoginTruck})` }}>
//             {
//                 typeBooking === "freight" ?
//                     <DetailsBookingFreight
//                         selectedContainer={selectedContainer}
//                         date={selectedDate}
//                         addressB={selected_destination}
//                         addressA={selected_origin}
//                     />
//                     :
//                     <DetailsBookingTruck
//                         listContainer={listContainerFil}
//                         date={date}
//                         totalPrice={totalPrice}
//                         addressB={addressB}
//                         addressA={addressA}
//                         typeExportImport={typeExportImport}
//                     />
//             }
//         </div>
//     )
// }