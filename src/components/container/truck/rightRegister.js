import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux';

import { contentBg } from 'assets/splitPage.module.scss';
import DetailsBooking from 'components/container/truck/detailsBooking'
import bgDetailLogin from 'assets/images/bg/bg-detail-login.jpg'

export default function RightLogin() {
    const { bookingTruck, listContainer } = useSelector(state => state.home)
    const [listContainerFil, setListContainerFil] = useState([])
    const [totalPrice, setTotalPrice] = useState(0)
    const { addressB, addressA, date } = bookingTruck
    useEffect(() => {
        if (listContainer.length) {
            const listContainerFil = listContainer.filter(value => value.priceAll)
            setListContainerFil(listContainerFil)
            if (listContainerFil.length) {
                const totalPrice = listContainerFil.reduce(function (a, b) { return a + b.priceAll }, 0)
                setTotalPrice(totalPrice)
            }
        }
    }, []);

    return (
        <div className={`${contentBg} `} style={{ backgroundImage: `url(${bgDetailLogin})` }}>
            <DetailsBooking
                listContainer={listContainerFil}
                date={date}
                totalPrice={totalPrice}
                addressB={addressB}
                addressA={addressA}
            />
        </div>
    )
}