import React from 'react'

import { contentBg, cardBg, textBg, decBg } from 'assets/splitPage.module.scss';
import setContainerTruck from 'assets/images/bg/set-container-truck.jpg'

export default function AddTruck() {
    return (
        <div className={`${contentBg}`} style={{ backgroundImage: `url(${setContainerTruck})` }}>
            <div className={cardBg}>
                <div className={textBg}>Pilih Kontainer sesuai dengan kebutuhan Anda</div>
                <div className={decBg}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
            </div>
        </div>
    )
}