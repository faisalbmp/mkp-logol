import React, { useState } from 'react'
import Modal from "../../map/WpModalTruck"
import { CancelModalIcon, CalendarSmall, LineIcon, ArrowDown, UploadArrowIcon } from "components/icon/iconSvg"
import InputText from "../../input/inputTextT"
import { useDispatch, useSelector } from "react-redux"
import ReactDatePicker, { CalendarContainer } from 'react-datepicker'
import { megamenu, buttonSelect, gridItem, title, lineCenter } from 'assets/scss/megamenu/megamenu.module.scss';
import 'assets/scss/component/react-datepicker.scss'
import moment from 'moment'
import { findByLabelText } from '@testing-library/react'
import { uploadFile } from "actions/uploadFile"
export default function GatePass(props) {
    let deliveryFileUpload = null;
    const [showDatePicker, setDatePickerVisibility] = useState(false)
    const [startDate, setDate] = useState("")
    const [file, setFilePath] = useState("")
    const [fileType, setFileType] = useState("")
    const ExampleCustomInput = ({ time, startDate, resetPickers, onClick }) => {
        return (
            <div className = "input-box-container" >
                <span>{'Tanggal Keluar NPE'}</span>
                <div className = "input-text-box" style = 
                    {{
                        alignItems: 'center'
                    }}  
                onClick={onClick}>
                    <CalendarSmall
                        color={startDate ? "#004DFF" : ""}
                    />
                    <div>
                        { moment(startDate).format('MMM Do, yyyy')}
                        {/* {
                        startDate.length ? startDate : <LineIcon />
                        } */}
                    </div>
                </div>
            </div>
        )
      };
    
    const MyContainer = ({ className, children }) => {
        return (
            <div style={{ marginTop: 21 }}>
            <CalendarContainer className={className}>
                <div style={{ position: "relative" }}>{children}</div>
            </CalendarContainer>
            </div>
        );
    }
    const dispatch = useDispatch()
    const getDocument = (e) => {
        if(e.target.files.length > 0) {
            const file = e.target.files[0];
            const fileName = e.target.files[0].name;

            setFilePath(e.target.files[0].name)
            setFileType(e.target.files[0].type)

            const formData = new FormData();
            formData.append('file', file, fileName);
            formData.append('fileType', e.target.files[0].type);            
            dispatch(uploadFile(formData));
        }
    }
    

    const fileUploaded = useSelector( state => state.uploadFile.uploadedFileData)

    const uploadDeliveryDocument = () => {
        deliveryFileUpload.click();
    }
    return (
        <Modal visible = {props.visible}>
            <div className = "gatepass">

                <div className = "gatepass-modal-header">
                    <div className = "gatepass-modal-title">
                        <div className = "gatepass-title" >Request Gatepass</div>
                        <div className = "gatepass-subtitle">Shipping Instruction SI/16/07/2020</div>
                    </div>
                    <div onClick = {props.onCancelPressed} ><CancelModalIcon/></div>
                </div>

                <div className = "gatepass-body">
                    <div className = "gatepass-input-container">
                        <InputText textInputTitle = {"PEB"} 
                        inputTextStyle = 
                            {{
                                width: '372px',
                                height: '52px',
                                padding:'10px'
                            }} 
                            
                        />
                        <InputText textInputTitle = {"NPE"} 
                            inputTextStyle = 
                            {{
                                width: '372px',
                                height: '52px',
                                padding:'10px'
                            }} 

                        />
                    </div>

                    <div className = "gatepass-input-container">
                        <InputText textInputTitle = {"NPWP"} 
                        inputTextStyle = 
                            {{
                                width: '372px',
                                height: '52px',
                                padding:'10px'
                            }} 

                        />
                        <ReactDatePicker
                            show={showDatePicker}
                            style={{ width: "50%" }}
                            customInput={
                            <ExampleCustomInput
                                time={"Waktu Pengiriman"}
                                startDate={startDate}
                                //resetPickers={this.resetPickers}
                            />
                            }
                            calendarContainer={MyContainer}
                            selected={new Date()}
                            onChange={startDate => setDate(startDate)}
                        >
                        </ReactDatePicker>
                    </div>

                    <div className = "gatepass-input-container">
                        <div className = "input-box-container">
                                <span>{'Unggah Dokumen'}</span>
                            <div className = "input-text-box" style={{alignItems:'center'}} onClick = {uploadDeliveryDocument}>
                                <div className="gate-pass-upload-btn">
                                    <span style={{alignItems:'flex-start', flex:1}}>
                                        Upload
                                    </span>
                                    <span>
                                        <UploadArrowIcon/>
                                    </span>
                                </div>
                                <div>{file}</div>
                                <input ref = {elem => deliveryFileUpload = elem} type = "file" style = {{ display: "none" }} onChange = {(e) =>  getDocument(e)}/>
                            </div>
                        </div>
                    </div>

                </div>
                <div className = "gatepass-footer">
                    <div className = "gatepass-btn-container">
                            <button className = "cancel-btn">
                                Batal
                            </button>
                            <button className = "sbmt-btn">
                                Simpan
                            </button>
                    </div>
                </div>
            </div>
        </Modal>
    )
}