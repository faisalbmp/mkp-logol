import React from 'react';
import Styles from "assets/scss/dashboard/freightlist/container.module.scss";
import { ArrowRight, EyeIcon } from '../../icon/iconSvg';
import PropTypes from 'prop-types';

const FreightListItem = ({ isPartialFilled, onPress }) => {

  const BannerAlert = () => {
    return (
      <div className={Styles.alertBanner}>
        <div className={Styles.shapeWrapper}>
          <div className={Styles.shape} />
          <text>
            Segera Lengkapi Data Pickup Truk
        </text>
        </div>
        <button>
          Lengkapi
        </button>
      </div>
    )
  };

  return (
    <div className={Styles.freightListItemContainer}>
      {
        isPartialFilled && BannerAlert()
      }
      <div className={Styles.tableWrapper}>
        <table>
          <thead>
            <tr>
              <td>No. Pesanan</td>
              <td>Status Pesanan</td>
              <td>No. Booking Kapal</td>
              <td>Jumlah Kontainer</td>
              <td colSpan={2}>Rute Pengiriman</td>
              <td>Bill of Lading</td>
              <td style={{ textAlign: 'center' }}>Aksi</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className={Styles.noPesanan}>0004112513</td>
              <td>0004112513</td>
              <td>FR-202003-E0003</td>
              <td>5 Kontainer</td>
              <td colSpan={2}>
                <div className={Styles.ruteWrapper}>
                  <div>
                    (CY)Tanjung Priok... <br />
                    <text className={Styles.estimateText}>(ETD) 25 Jan, 2020</text>
                  </div>
                  <div className={Styles.iconRute}>
                    <ArrowRight />
                  </div>
                  <div>
                    (CY) Singapore Po...<br />
                    <text className={Styles.estimateText}>(ETA) 30 Jan, 2020</text>
                  </div>
                </div>
              </td>
              <td>
                <text className={Styles.bolWrapper}>Draft 12122112</text>
              </td>
              <td style={{ textAlign: 'center' }}>
                <div onClick={onPress}>
                  <EyeIcon />
                </div>
              </td>
            </tr>
          </tbody>
        </table>

      </div>
    </div>
  );
};

export default FreightListItem;

FreightListItem.defaultProps = {
  isPartialFilled: false,
};

FreightListItem.propTypes = {
  isPartialFilled: PropTypes.bool,
};
