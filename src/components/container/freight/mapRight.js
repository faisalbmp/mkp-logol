import React from 'react';
import Map from 'components/map/MapDirectionsRenderer';

const googleMapsApiKey = process.env.REACT_APP_GOOGLE_MAP;

const places = [
	{ latitude: 25.8103146, longitude: -80.1751609 },
	{ latitude: 28.4813018, longitude: -81.4387899 },
];

const App = props => {
	const {
		loadingElement,
		containerElement,
		mapElement,
		// defaultCenter,
		// defaultZoom
	} = props;

	return (
		<Map
			googleMapURL={
				'https://maps.googleapis.com/maps/api/js?key=' +
				googleMapsApiKey +
				'&libraries=geometry,drawing,places'
			}
			markers={places}
			loadingElement={loadingElement || <div style={{ height: `100%` }} />}
			containerElement={containerElement || <div style={{ height: '100%' }} />}
			mapElement={mapElement || <div style={{ height: `100%` }} />}
			defaultCenter={{ lat: 25.798939, lng: -80.291409 }}
			defaultZoom={11}
		/>
	);
};

export default App;
