import React from 'react'
import { RoundAIcon, RoundBIcon, DonwloadIfIcon, RrrowTrack } from 'components/icon/iconSvg'
import { text, container, center, card, content, title1, title2, flex, total, track, pdRight, label, voucherScss, textD, textV, totalFalse, feeDetail, wrDonwload, btnDonwload } from 'assets/scss/component/detailBooking.module.scss'

export default function DetailBooking() {
    return (
        <div className={center}>
            <div className={container}>
                <div className={card}>
                    <div className={content}>
                        <div className={title1}>Rincian Pengiriman</div>

                        <div className={label}>Rute Pengiriman</div>

                        <div className={track}>
                            <RoundAIcon pdRight={pdRight} />
                            <div className={`${text} ${pdRight}`}> Cikarang, Jawa Barat</div>
                            <RrrowTrack pdRight={pdRight} />
                            <RoundBIcon pdRight={pdRight} />
                            <div className={`${text} ${pdRight}`}>Tj. Priok, Jakarta  (34 km)</div>
                        </div>

                        <div className={label} style={{ marginTop: 17 }}>Rute Pengiriman</div>
                        <div className={text}>01 - 07 Januari 2020 ( Minggu 1 )</div>

                        <div className={label} style={{ marginTop: 17 }}>Jenis Pengiriman</div>
                        <div className={text}>Ekspor</div>
                    </div>
                    <div className={content}>
                        <div className={title2}>Biaya Layanan</div>
                        <div className={flex}>
                            <div className={feeDetail}>5 x 20’ General Purpose</div>
                            <div className={feeDetail}>IDR 1.000.000</div>
                        </div>
                        <div className={flex}>
                            <div className={feeDetail}>5 x 20’ General Purpose</div>
                            <div className={feeDetail}>IDR 1.000.000</div>
                        </div>
                        <div className={voucherScss}>
                            <div className={textV}>Voucher Potongan Harga</div>
                            <div className={textV}>IDR 500.000</div>
                        </div>
                        <div className={flex}>
                            <div className={total}>Total</div>
                            <div className={total}>IDR 1.800.000</div>
                        </div>
                        <div className={totalFalse}>IDR 12.100.000</div>

                    </div>
                </div>

                <div className={wrDonwload}>
                    <div className={textD}>Simpan Rincian Pengiriman</div>
                    <div className={btnDonwload}>
                        <DonwloadIfIcon />
                        <div className={textD}>Unduh Informasi</div>
                    </div>
                </div>
            </div>
        </div >
    )
}
