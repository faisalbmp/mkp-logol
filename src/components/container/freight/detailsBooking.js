import React from 'react'
import { RoundAIcon, RoundBIcon, RrrowTrack } from 'components/icon/iconSvg'
import { text, container, center, card, content, title1, track, pdRight, label, bottomPosition, wrRoundIcon } from 'assets/scss/component/detailBooking.module.scss'
import { months } from 'utils/listDate'
import moment from 'moment';
import { useTranslation } from 'react-i18next';


function DetailBooking({ date, addressB, addressA, selectedContainer }) {
    const { bottom, monthId, top } = date || {}
    const { t } = useTranslation();
    return (
        <div className={center}>
            <div className={container}>
                <div className={card}>
                    <div className={content} style={{ border: "none", paddingBottom: 0 }}>
                        <div className={title1}>{t('requestScheduler.detailBooking.title')}</div>
                        <div className={track} style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
                            <div className={track}>
                                <div className={wrRoundIcon}>
                                    <RoundAIcon pdRight={pdRight} />
                                </div>
                                <div className={`${text} ${pdRight}`}>{addressA ? addressA.name || '' : ""}, {addressA ? addressA.country || '' : ""}</div>
                            </div>
                            <RrrowTrack pdRight={`${pdRight}   ${bottomPosition}`} />
                            <div className={track}>
                                <div className={wrRoundIcon}>
                                    <RoundBIcon pdRight={pdRight} />
                                </div>
                                <div className={`${text} ${pdRight}`}>{addressB ? addressB.name || '' : ""}, {addressB ? addressB.country || '' : ""}</div>
                            </div>
                        </div>

                        <div className={label} style={{ marginTop: 17 }}>{t('requestScheduler.detailBooking.deliveryTime')} (ETD-ETA)</div>
                        {/* <div className={text}>01 - 07 Januari 2020 ( Minggu 1 )</div> */}
                        <div className={text} >{date ? `${bottom} ${months[monthId - 1]} ${moment().format("YYYY")}` : ""} - {date ? `${top} ${months[monthId - 1]} ${moment().format("YYYY")}` : ""}</div>
                        <div className={label} style={{ marginTop: 17 }}>{t('requestScheduler.detailBooking.typeofDelivery')}</div>
                        <div className={text} >{selectedContainer ? selectedContainer.name : ""}</div>
                    </div>
                </div>
            </div>
        </div >
    )
}

export default DetailBooking