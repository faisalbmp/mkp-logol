import React, { useState } from 'react'
// import { useLocation, useHistory } from 'react-router-dom'

import { contentRound, addTruckSy, addTruckFlex } from 'assets/splitPage.module.scss';
import { text, track, pdRight } from 'assets/scss/component/detailBooking.module.scss'
import { AddTruckIcon, RrrowTrack, IndonesiaIcon, SingaporeIcon } from 'components/icon/iconSvg'

export default function Summary() {
    const [checkBox, setCheckBox] = useState(false)
    return (
        <div>
            <div style={{ marginTop: 24 }}>
                <label style={{ marginBottom: 8 }}>Rute Pengiriman</label>

                <div className={track}>
                    <IndonesiaIcon style={pdRight} />
                    <div className={`${text} ${pdRight}`}>Tanjung Priok, Jakart...</div>
                    <RrrowTrack pdRight={pdRight} />
                    <SingaporeIcon style={pdRight} />
                    <div className={`${text} ${pdRight}`}>Singapore Port, Sing...</div>
                </div>
            </div>

            <label>Waktu Pengiriman (ETD-ETA)</label>
            <div className={text} style={{ marginTop: 8, marginTop: 24 }}>25 Januari 2020- 30 Januari 2020</div>
            <label>Jenis Pengiriman</label>
            <div className={text} style={{ marginTop: 8 }}>Full Container Load</div>

            <div
                className={`${contentRound} ${addTruckSy}`}
                style={{ marginTop: 24, height: 80, paddingTop: 24 }}
                onClick={() => setCheckBox(!checkBox)}
            >
                <div className={addTruckFlex}>
                    <div className={addTruckFlex}>
                        <input
                            type="checkbox"
                            name="vehicle1"
                            checked={checkBox}
                        />
                        <div>Tambah Layanan Truk</div>
                    </div>
                    <AddTruckIcon />
                </div>
            </div>
        </div>
    )
}
