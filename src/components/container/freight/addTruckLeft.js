import React, { useState, Fragment, useEffect } from 'react';
import { useLocation, useHistory } from 'react-router-dom';

import {
	arrowUpDetail,
	container,
	header,
	content,
	lineAT,
	btnDetail,
	btnPrimary,
	btnLight,
	font14,
	contentRound,
	addTruckSy,
	addTruckFlex,
	ruteTruck,
	rightArrowSearhcIcon,
	textRute,
	track,
} from 'assets/splitPage.module.scss';
import {
	seeDetailSt,
	arrowDownDetail,
	total,
	flex,
	title2,
	text,
	pdRight,
	label,
} from 'assets/scss/component/detailBooking.module.scss';
import {
	ArrowDownDetail,
	AddTruckIcon,
	ArrowLeftDetail,
	RightArrowIcon,
	RrrowTrack,
	IndonesiaIcon,
	SingaporeIcon,
	RoundAIcon,
	RoundBIcon,
	RightArrowSearhcIcon,
	CheckBoxIcon,
	CloseCheckIcon,
} from 'components/icon/iconSvg';
import InputAutoComplete from 'components/input/inputAutoComplete';
import { autocompletePlace } from 'services/apiGoogleMap';
import delayInput from 'utils/delayInput';

export default function AddTruck({ sliderLogin, sliderMap }) {
	let { pathname } = useLocation();
	let history = useHistory();
	const publicUrl = process.env.PUBLIC_URL;

	const [hoverCheckbox, setHoverCheckbox] = useState(false);
	const [addTruckActivity, setAddTruck] = useState(false);
	const [dataAutoComplete, setDataAutoComplete] = useState([]);
	const [selectVal, setSelectVal] = useState('');

	const [truckVal, setTruckVal] = useState('');
	const [checkBox, setCheckBox] = useState(false);

	const [seedetail, setSeeDetail] = useState({
		deliveryDetails: true,
		pricingDetails: true,
		truckService: true,
	});

	const setDetail = name => {
		setSeeDetail({ ...seedetail, [name]: !seedetail[name] });
	};

	useEffect(() => {}, []);

	const getPlaces = async val => {
		setTruckVal(val);
		if (val.length) {
			delayInput(async () => {
				const res = await autocompletePlace(val);
				setDataAutoComplete(res);
			}, 1000);
		}
	};

	const setItem = val => {
		setSelectVal(val.description);
		setTruckVal(val.description);
		setDataAutoComplete([]);
	};

	return (
		<div className={container}>
			<div className={header}>
				<div onClick={() => history.goBack()}>
					<ArrowLeftDetail />
				</div>
			</div>
			<div className={content} style={{ marginTop: 0 }}>
				<div>
					<div
						onClick={() => setDetail('deliveryDetails')}
						className={`${flex}`}
						style={{
							marginBottom: 24,
							display: 'flex',
							alignItems: 'center',
							cursor: 'pointer',
						}}
					>
						<div className={title2} style={{ marginBottom: 0, width: 178 }}>
							Rincian Pengiriman
						</div>
						<div className={lineAT} style={{ maxWidth: 226 }}></div>
						<ArrowDownDetail
							style={`${arrowDownDetail}  ${
								seedetail.deliveryDetails ? arrowUpDetail : ''
							}`}
						/>
					</div>
					{seedetail.deliveryDetails ? (
						<div>
							<div style={{ marginTop: 24 }}>
								<div className={label} style={{ marginBottom: 8 }}>
									Rute Pengiriman
								</div>

								<div className={track}>
									<IndonesiaIcon style={pdRight} />
									<div className={`${text} ${pdRight}`}>
										Tanjung Priok, Jakart...
									</div>
									<RrrowTrack pdRight={pdRight} />
									<SingaporeIcon style={pdRight} />
									<div className={`${text} ${pdRight}`}>
										Singapore Port, Sing...
									</div>
								</div>
							</div>

							<div className={label} style={{ marginTop: 24 }}>
								Waktu Pengiriman (ETD-ETA)
							</div>
							<div className={text}>25 Januari 2020- 30 Januari 2020</div>
							<div className={label} style={{ marginTop: 24 }}>
								Jenis Pengiriman
							</div>
							<div className={text} style={{ marginTop: 8 }}>
								Full Container Load
							</div>
						</div>
					) : (
						''
					)}

					{!checkBox ? (
						<div
							className={`${contentRound} ${addTruckSy}`}
							style={{ marginTop: 24, height: 80, paddingTop: 24 }}
							onClick={() => {
								setCheckBox(!checkBox);
								setDetail('deliveryDetails');
								sliderMap();
							}}
						>
							<div className={addTruckFlex}>
								<div className={addTruckFlex}>
									<input type='checkbox' name='vehicle1' checked={checkBox} />
									<div>Tambah Layanan Truk</div>
								</div>
								<AddTruckIcon />
							</div>
						</div>
					) : (
						''
					)}
				</div>

				{checkBox ? (
					<div>
						<div
							className={`${flex}`}
							style={{
								marginTop: 24,
								marginBottom: 24,
								display: 'flex',
								alignItems: 'center',
								cursor: 'pointer',
							}}
						>
							<div
								onMouseLeave={() => setHoverCheckbox(false)}
								onMouseEnter={() => setHoverCheckbox(true)}
								onClick={() => setCheckBox(false)}
							>
								{hoverCheckbox ? <CloseCheckIcon /> : <CheckBoxIcon />}
							</div>

							<div className={title2} style={{ marginBottom: 0, width: 170 }}>
								Tambah Layanan Truk
							</div>
							<div className={lineAT} style={{ maxWidth: 226 }}></div>
							<ArrowDownDetail
								style={`${arrowDownDetail}  ${
									seedetail.truckService ? arrowUpDetail : ''
								}`}
							/>
						</div>
						<InputAutoComplete
							setChange={e => getPlaces(e)}
							setItem={setItem}
							value={truckVal}
							arraySuggestion={dataAutoComplete}
						/>
					</div>
				) : (
					''
				)}

				{selectVal.length ? (
					<div style={{ marginTop: 32 }}>
						<div className={label}>Rute Truk</div>
						<div className={`${font14} ${ruteTruck}`} style={{ marginTop: 8 }}>
							<RoundAIcon />
							<div className={textRute}>{selectVal}</div>
						</div>

						<div className={ruteTruck} style={{ marginTop: 8, marginLeft: -2 }}>
							<RightArrowSearhcIcon
								color={'#333333'}
								width={19}
								height={11.19}
								rightArrowSearhcIcon={rightArrowSearhcIcon}
							/>
							<div
								style={{ marginLeft: 8, marginBottom: 0 }}
								className={title2}
							>
								({34} km)
							</div>
						</div>

						<div className={`${font14} ${ruteTruck}`} style={{ marginTop: 8 }}>
							<RoundBIcon />
							<div className={textRute}>
								{' '}
								Cikarang, Cikarang Jalan Pasir Randu, Sukasari, Bekasi, , Jawa
								Barat, Indonesia
							</div>
						</div>
					</div>
				) : (
					''
				)}

				<div className={seeDetailSt}>
					<div
						onClick={() => setDetail('pricingDetails')}
						className={`${flex}`}
						style={{
							marginTop: 24,
							marginBottom: 24,
							display: 'flex',
							alignItems: 'center',
							cursor: 'pointer',
						}}
					>
						<div className={title2} style={{ marginBottom: 0 }}>
							Rincian Harga
						</div>
						<div
							className={lineAT}
							style={{ marginBottom: 8, maxWidth: 275, marginTop: 8 }}
						></div>
						<ArrowDownDetail
							style={`${arrowDownDetail}  ${
								seedetail.pricingDetails ? arrowUpDetail : ''
							}`}
						/>
					</div>

					{seedetail.pricingDetails ? (
						<div>
							<div className={`${flex} ${font14}`}>
								<div>20’ General Purpose x3</div>
								<div>IDR 1.000.000</div>
							</div>

							<div className={`${flex} ${font14}`}>
								<div>40’ High Cube x3</div>
								<div>IDR 800.000</div>
							</div>

							<div className={`${flex} ${font14}`}>
								<div>Biaya Pelayaran</div>
								<div>IDR 1.000.000</div>
							</div>

							<div className={`${flex} ${font14}`}>
								<div>Biaya Bill of Lading</div>
								<div>IDR 500.000</div>
							</div>

							<div className={`${flex} ${font14}`}>
								<div className={total}>Total</div>
								<div className={total}>IDR 89.445.000</div>
							</div>
						</div>
					) : (
						''
					)}
				</div>

				<div style={{ display: 'flex' }}>
					<button
						className={btnLight}
						style={{ width: 213, marginRight: 27 }}
						onClick={() => history.goBack()}
					>
						Ubah Pencarian
					</button>
					<button
						className={`${btnPrimary} ${btnDetail}`}
						style={{ width: 216 }}
						onClick={sliderLogin}
					>
						Berikutnya
						<RightArrowIcon style={{ marginLeft: 20 }} />
					</button>
				</div>
			</div>
		</div>
	);
}
