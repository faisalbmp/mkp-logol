import React from 'react'

import { contentBg } from 'assets/splitPage.module.scss';
import DetailsBooking from 'components/container/freight/detailBooking'
import bgDetailLogin from 'assets/images/bg/bg-detail-login.jpg'

export default function RightLogin() {
    // let { pathname } = useLocation();

    return (
        <div className={`${contentBg} `} style={{ backgroundImage: `url(${bgDetailLogin})` }}>
            <DetailsBooking />
        </div>
    )
}