import React from 'react'
import { contentBg } from 'assets/splitPage.module.scss';
import DetailsBooking from 'components/container/freight/detailBooking'
import bglogin from 'assets/images/bg/bg-login.jpg'

export default function RightLogin() {
    // let { pathname } = useLocation();

    return (
        <div className={`${contentBg} `} style={{ backgroundImage: `url(${bglogin})` }}>
            <DetailsBooking />
        </div>
    )
}