import React from 'react'
import { useHistory } from "react-router-dom";

import customerServiceImg from 'assets/images/icons/customer-service.svg'
import 'assets/Index.scss'

import { withTranslation } from 'react-i18next';

function Index({ t }) {
  const history = useHistory();
  const translate = t;
  return (
    <div className="customer-service">
      <div className="wp-customer-service">
        <div className="left item-animation" >
          <img src={customerServiceImg} alt="customer-service-img" />
          <div className="wrepper-text">
            <div className="text-customer">{translate('bannerCustomerSupport.customerSupport')}</div>
            <div className="text-customer">021 2452 3147</div>
          </div>
        </div>
        <div className="right item-animation">
          <div className="text-customer">{translate('bannerCustomerSupport.title')}</div>
          <div className="text-customer">{translate('bannerCustomerSupport.subTitle')}<p className="text-dec"
            onClick={() => {
              history.push(`/helpCenter`)
            }}
          >{translate('bannerCustomerSupport.helpCenter')}</p></div>
        </div>
      </div>
    </div>
  )
}

export default withTranslation()(Index)
