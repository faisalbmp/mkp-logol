import React from 'react'
import { contentBg } from 'assets/splitPage.module.scss';
import bglogin from 'assets/images/bg/bg-login.jpg'

export default function RightLogin() {
    return (
        <div className={`${contentBg} `} style={{ backgroundImage: `url(${bglogin})` }}>
        </div>
    )
}