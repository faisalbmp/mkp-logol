import React, { useState, memo, useCallback, Fragment, useEffect } from 'react'
import { useLocation } from "react-router-dom";
import queryString from 'query-string'
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from "react-router-dom";

import InputAuth from 'components/input/inputAuth'
import { NoPhone, CompanyIcon, NibFileIcon, DonwloadIfIcon, CheckIcon, EmailIcon, ProfileIcon, PasswordIcon, ErrorIcon } from 'components/icon/iconSvg'
import Styles from 'assets/scss/megamenu/portpicker.module.scss';
import { hello, left, container } from 'assets/splitPage.module.scss';
import { textitem, wrInput, wrInputFlex, wrWidth, btnDownload, donwloadIfIcon, wpBtn, agree, wpAgree, provision, showAnimation, inputFile, activeBtn } from 'assets/scss/signUpIn/left.module.scss';
import { inputAuth, icon, wrapping, label, wrepperIcon } from 'assets/scss/input/inputAuth.module.scss';
import { textError, showTextEerror } from 'assets/scss/megamenu/megamenu.module.scss';
import AutoCompleteAddress from 'components/input/autoCompleteAddress'
import Header from 'components/header/headerSplit'
import NextBtn from 'components/button/nextBtn'
import PreviousBtn from 'components/button/previousBtn'
import OutlineSelect from 'components/select/outlineSelect'
import { signUp, actionProvinsi, uploadFile } from 'actions'
import { CheckBoxM } from 'components/checkBox'
import UploadNpwp from 'components/input/uploadNpwp'
import useWindowDimensions from 'utils/windowDimensions';
import { validationPhone } from 'utils/validation'
import { withTranslation } from 'react-i18next';

const InputAuthMemo = memo(InputAuth)

const optionTypeTruck = [{ id: "1", nama: "Truck Provider" }, { id: "2", nama: "Shipping Line" }, { id: "3", nama: "Insurance" }, { id: "4", nama: "Other" },]

function LeftComponent({ sliderLogin, t }) {
    const dispatch = useDispatch()
    const { search } = useLocation()
    const history = useHistory()
    const { typeUser } = queryString.parse(search)
    const translate = t;

    const { auth, idlocation } = useSelector(state => state)
    const { uploadedFileData } = useSelector(state => state.uploadFile)
    const { height } = useWindowDimensions()
    // const uploadFileRedux = useSelector(state => state.uploadFile)

    const [dataCompany, setValue] = useState({
        companyName: "",
        country: "Indonesia",
        city: "",
        district: "",
        state: "",
        subDistrict: "",
        vendorType: "1",
        nib: "",
        email: "",
        npwp: "",
        npwpDocPath: "",
        officePhone: "",
        password: "",
        picName: "",
        picNumber: "",
        picPosition: "",
        postalCode: "string",
        address: ""
    })
    const [collapse, setCollapse] = useState(1)
    const [collapse1, setCollapse1] = useState(false)
    const [collapse2, setCollapse2] = useState(false)
    const [loadingNpwp, setLoadingNpwp] = useState(false)
    const [errorInput, seteErorInput] = useState(false)
    const [termsConditions, setTermsConditions] = useState(false)

    const [nameTypeVendor, setTypeVendor] = useState("")

    const { failureSingup, succesSingup, isSingup, errorSignup } = auth
    const { companyName, email, npwp, npwpDocPath, officePhone, password, picName, picNumber, picPosition, postalCode, address, nib } = dataCompany

    useEffect(() => {
        if (typeUser !== "Vendor") {
            delete dataCompany.vendorType
            delete dataCompany.nib
            setValue(dataCompany)
        }

        dispatch(actionProvinsi())
    }, [])

    useEffect(() => {
        setValue({ ...dataCompany, npwpDocPath: uploadedFileData })
    }, [uploadedFileData])

    useEffect(() => {
        if (succesSingup) {
            sliderLogin()
            setValue(dataCompany)
        }
        return () => sliderLogin()
    }, [succesSingup])

    const onChange = (e) => {
        setValue({ ...dataCompany, [e.target.name]: e.target.value })
    }

    const onSelectVendor = (name, value) => {
        setTypeVendor(value.nama)
        setValue({ ...dataCompany, [name]: value.id })
    }

    const emtypCollapse1 =
        companyName.length &&
        officePhone.length &&
        npwp.length &&
        npwpDocPath.length &&
        (typeUser === "Vendor" ? nib.length : true)

    const emtypCollapse2 =
        address.length

    const emtypCollapse3 =
        picName.length &&
        picPosition.length &&
        email.length &&
        password.length &&
        picNumber.length &&
        termsConditions

    const onClickRegister = () => {
        if (!emtypCollapse3)
            seteErorInput(true)
        else
            dispatch(signUp(typeUser, dataCompany))
    }

    const onChangeFile = async (e) => {
        const file = e.target.files[0]
        setLoadingNpwp(true)
        // && file.type === "text/plain"
        // console.log(e.target.files[0])
        if (file) {
            const data = new FormData()
            await data.append('file', file)
            await data.append('fileType', "npwp")
            // setValue(Object.assign(dataCompany, { npwpDocPath: data }))
            await dispatch(uploadFile(data))
            setLoadingNpwp(false)
        }
    }

    const clickSuggestion = (value) => {
        const termsAddress = value.terms
        const stateIndex = termsAddress[termsAddress.length - 2].value || "string"
        const cityIndex = termsAddress[termsAddress.length - 3].value || "string"
        const districtIndex = termsAddress[termsAddress.length - 4].value || "string"
        const subDistrictIndex = termsAddress[termsAddress.length - 5].value || "string" + " " + termsAddress[termsAddress.length - 6].value || ""
        setValue({ ...dataCompany, address: value.description, state: stateIndex, city: cityIndex, district: districtIndex, subDistrict: subDistrictIndex })
    }

    const onCLickNext2 = () => {
        setCollapse1(true)
        if (emtypCollapse1)
            setCollapse(2)
    }

    const onCLickNext3 = () => {
        setCollapse2(true)
        if (emtypCollapse2)
            setCollapse(3)
    }

    const npwpDocPathSplit = npwpDocPath.length ? npwpDocPath.split("/") : ""

    return (
        <div className={left} id="register">
            <div className={container} style={{ maxWidth: 552, height }}>
                <Header />
                <div className={hello} style={{ margin: "42px 0px 48px 0px" }}>{translate('registerPage.registration', { type: typeUser })}</div>

                <div className={`${textitem}`} style={{ color: collapse === 1 ? "#0045FF" : "#868A92" }}>1. {translate('registerPage.companyInformation')}</div>
                <div className={showAnimation} style={{ display: collapse === 1 ? "block" : "none" }}>
                    {typeUser === "Vendor" ?
                        <div className={wrInputFlex}>
                            <div className={wrWidth}>
                                <InputAuthMemo
                                    isValidateView
                                    Icon={CompanyIcon}
                                    activeManual={!companyName.length && collapse1}
                                    title={translate('registerPage.company')}
                                    type="text"
                                    value={companyName}
                                    activeIcon={false}
                                    name="companyName"
                                    onChange={onChange}
                                />
                            </div>
                            <div>
                                <OutlineSelect
                                    option={optionTypeTruck}
                                    defaultValue={"Truck Provider"}
                                    type="text"
                                    value={nameTypeVendor}
                                    activeManual={!nameTypeVendor.length && collapse1}
                                    name="vendorType"
                                    width={168}
                                    onSelect={onSelectVendor}
                                />
                            </div>
                        </div>
                        :
                        <InputAuthMemo
                            Icon={CompanyIcon}
                            isValidateView
                            activeManual={!companyName.length && collapse1}
                            title={translate('registerPage.company')}
                            type="text"
                            activeIcon={true}
                            value={companyName}
                            name={"companyName"}
                            onChange={onChange}
                        />}
                    <InputAuthMemo
                        isValidateView
                        Icon={NoPhone}
                        activeManual={!officePhone.length && collapse1}
                        type="number"
                        title={translate('registerPage.officePhone')}
                        activeIcon={true}
                        name="officePhone"
                        value={officePhone}
                        onChange={onChange}
                    />
                    {typeUser === "Vendor" ? <InputAuthMemo
                        isValidateView
                        Icon={NoPhone}
                        title="Induk Berusaha Number (NIB)"
                        activeManual={!nib.length && collapse1}
                        type="text"
                        name={"nib"}
                        value={nib}
                        activeIcon={true}
                        onChange={onChange}
                    /> : ""}
                    <div className={wrInputFlex}>
                        <div className={wrWidth} style={{ width: 264 }}>
                            <InputAuthMemo
                                Icon={NibFileIcon}
                                title={translate('registerPage.companyTaxNumber')}
                                type="text"
                                value={npwp}
                                activeManual={!npwp.length && collapse1}
                                activeIcon={false}
                                name="npwp"
                                onChange={onChange}
                            />
                        </div>

                        <UploadNpwp
                            loading={loadingNpwp}
                            onChangeFile={onChangeFile}
                            activeAction={npwpDocPath.length}
                            error={!npwpDocPathSplit.length && collapse1}
                            name={npwpDocPathSplit.length ? npwpDocPathSplit[npwpDocPathSplit.length - 1] : translate('registerPage.uploadTaxCard')}
                        />

                        <div className={wrepperIcon}>
                            {
                                !npwpDocPathSplit.length && collapse1 ?
                                    <ErrorIcon />
                                    :
                                    <CheckIcon color={npwpDocPath.length ? "#004DFF" : "#E0E5E8"} />
                            }
                        </div>
                    </div>

                    <div style={{ paddingTop: 20 }}>
                        <div className={`${textError}  ${!companyName.length && collapse1 ? showTextEerror : ""}`}>{translate('alert.companyMandatory')}</div>
                        <div className={`${textError}  ${!validationPhone(officePhone) && collapse1 ? showTextEerror : ""}`}>{translate('alert.officePhoneMandatory')}</div>
                        {typeUser === "Vendor" ? <div className={`${textError}  ${!nib.length && collapse1 ? showTextEerror : ""}`}>{translate('alert.nibMandatory')}</div> : ""}
                        < div className={`${textError}  ${!npwp.length && collapse1 ? showTextEerror : ""}`}>{translate('alert.companyTaxMandatory')}</div>
                        <div className={`${textError}  ${!npwpDocPathSplit.length && collapse1 ? showTextEerror : ""}`}>{translate('alert.uploadTaxCardMandatory')}</div>
                    </div>

                    <NextBtn
                        title={translate('registerPage.nextStep')}
                        style={{
                            // width: 190,
                            margin: "40px 0px 0px auto"
                        }}
                        onClick={onCLickNext2}
                    />
                </div>

                <div className={`${textitem}`} style={{ color: collapse === 2 ? "#0045FF" : "#868A92" }}>2. {translate('registerPage.companyDetail')}</div>
                <div className={showAnimation} style={{ display: collapse === 2 ? "block" : "none" }}>
                    {/* <div className={wrInput} style={{ justifyContent: "space-between" }}>
                        <OutlineSelect
                            option={dataProvinsi}
                            defaultValue={"Province"}
                            value={state}
                            type="text"
                            name={"state"}
                            width={168}
                            onSelect={onSelect}
                            activeManual={!dataProvinsi.length && errorInput}
                        />
                    </div> */}

                    <div style={{
                        marginTop: 29,
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',

                    }}>
                        {/* <InputAuthMemo
                            Icon={CompanyIcon}
                            title="Company Address"
                            type="text"
                            value={address}
                            activeIcon={false}
                            name="address"
                            onChange={onChange}
                        /> */}

                        <AutoCompleteAddress
                            clickSuggestion={clickSuggestion}
                            maxWidth={350}
                            errorManual={!address.length && collapse2}
                        />
                        <InputAuthMemo
                            title={translate('registerPage.postalCode')}
                            type="text"
                            value={postalCode}
                            activeIcon={false}
                            name="postalCode"
                            onChange={onChange}
                            // activeManual={!postalCode.length && collapse2 || true}
                            rounded
                            styleInput={{ height: 56 }}
                        />
                    </div>

                    <div style={{ paddingTop: 20 }}>
                        <div className={`${textError}  ${!address.length && collapse2 ? showTextEerror : ""}`}>{translate('alert.companyMandatory')}</div>
                    </div>

                    <div className={wpBtn}>
                        <PreviousBtn
                            title={translate('registerPage.previous')}
                            style={{ width: 180 }}
                            onClick={() => setCollapse(1)}
                        />
                        <NextBtn
                            title={translate('registerPage.nextStep')}
                            onClick={onCLickNext3}
                        />
                    </div>
                </div>

                <div className={`${textitem}`} style={{ color: collapse === 3 ? "#0045FF" : "#868A92" }}>3. {translate('registerPage.personalInformation')}</div>

                <div className={showAnimation} style={{ display: collapse === 3 ? "block" : "none" }}>

                    <div style={{ display: 'flex' }}>
                        <div className={wrWidth} style={{ width: '60%' }}>
                            <InputAuthMemo
                                isValidateView
                                Icon={ProfileIcon}
                                title="Person in Charge (PIC)"
                                activeIcon={true}
                                value={picName}
                                name={"picName"}
                                type="text"
                                onChange={onChange}
                                activeManual={!picName.length && errorInput}
                            />
                        </div>
                        <div className={wrWidth} style={{ marginRight: 0, width: '40%' }}>
                            <InputAuthMemo
                                isValidateView
                                title="Position"
                                type="text"
                                activeIcon={true}
                                value={picPosition}
                                name={"picPosition"}
                                styleInput={{ paddingLeft: 16 }}
                                onChange={onChange}
                                activeManual={!picPosition.length && errorInput}
                            />
                        </div>
                    </div>

                    <InputAuthMemo
                        isValidateView
                        Icon={NoPhone}
                        title={translate('registerPage.phoneNumber')}
                        activeIcon={true}
                        value={picNumber}
                        name="picNumber"
                        type="number"
                        onChange={onChange}
                        activeManual={!picNumber.length && errorInput}
                    />

                    <InputAuthMemo
                        isValidateView
                        type="email"
                        Icon={EmailIcon}
                        title="E-mail"
                        value={email}
                        name="email"
                        activeIcon={true}
                        onChange={onChange}
                        activeManual={!email.length && errorInput}
                    />
                    <InputAuthMemo
                        type="password"
                        Icon={PasswordIcon}
                        title={translate('registerPage.createPassword')}
                        value={password}
                        name="password"
                        activeIcon={true}
                        onChange={onChange}
                        activeManual={!password.length && errorInput}
                    />

                    <div className={wpAgree}>
                        <CheckBoxM
                            onChange={() => setTermsConditions(!termsConditions)}
                        />
                        <div className={agree}>{translate('registerPage.condition1')} <a className={provision} target="_blank" onClick={() => history.push('/termsAndConditions')}> {translate('registerPage.termsAndCondition')}</a> {translate('registerPage.condition2')}</div>
                    </div>

                    <div className={`${textError}  ${failureSingup && !errorSignup.length ? showTextEerror : ""}`} style={{ marginTop: 20, fontSize: 14 }}>{translate('alert.somethingWrong')}</div>
                    <div className={`${textError}  ${errorSignup.length ? showTextEerror : ""}`} style={{ marginTop: 20, fontSize: 14 }}>{errorSignup}</div>
                    {/* <div className={`${textError}  ${emtypCollapse3 && errorInput ? showTextEerror : ""}`} style={{ marginTop: 20, fontSize: 14 }}>Data diatas Wajib di isi, tidak boleh kosong</div> */}

                    <div style={{ paddingTop: 20 }}>
                        <div className={`${textError}  ${!picName.length && errorInput ? showTextEerror : ""}`}>{translate('alert.picMandatory')}</div>
                        <div className={`${textError}  ${!picPosition.length && errorInput ? showTextEerror : ""}`}>{translate('alert.positionMandatory')}</div>
                        {/* <div className={`${textError}  ${!picNumber.length && errorInput ? showTextEerror : ""}`}>Phone Number Tidak Boleh Kosong</div> */}
                        <div className={`${textError}  ${!email.length && errorInput ? showTextEerror : ""}`}>{translate('alert.emailMandatory')}</div>
                        <div className={`${textError}  ${!password.length && errorInput ? showTextEerror : ""}`}>{translate('alert.passwordMandatory')}</div>
                        <div className={`${textError}  ${!termsConditions && errorInput ? showTextEerror : ""}`}>{translate('alert.termsAndConditionMandatory')}</div>
                    </div>


                    <div className={wpBtn}>
                        <PreviousBtn
                            title={translate('registerPage.previous')}
                            style={{ width: 180 }}
                            onClick={() => setCollapse(2)}
                        />
                        <NextBtn
                            title={translate('register')}
                            loading={isSingup}
                            onClick={onClickRegister}
                        />
                    </div>
                </div>
            </div>
        </div >
    )
}

export default withTranslation()(LeftComponent)