import React, { useState } from 'react'

import { bg, bgImg1, bgImg2, bgImg3, bgImg4, content, listItem, item, title, container, childTitle, activeItem, WrIcon, iconHv } from 'assets/scss/signUpIn/right.module.scss'
import { hello } from 'assets/splitPage.module.scss';
import { Arrowx5Fup1Icon } from 'components/icon/iconSvg';

import { withTranslation } from 'react-i18next';

const data = [
    { title: "why1", childTitle: "why1Desc" },
    { title: "why2", childTitle: "why2Desc" },
    { title: "why3", childTitle: "why3Desc" },
    { title: "why4", childTitle: "why4Desc" },
]

function Index({ t }) {
    const [active, setActive] = useState(0)
    const translate = t;
    return (
        <div className={bg}>
            <div className={bgImg1}></div>
            <div className={bgImg2}></div>
            <div className={bgImg3}></div>
            <div className={bgImg4}></div>

            <div className={container}>
                <div className={content}>
                    <div className={hello} style={{ fontSize: 22, marginBottom: 24 }}>{translate('registerPage.rightContent.title')}</div>
                    {
                        data.map((value, index) => (
                            <div className={`${listItem} ${active === index ? activeItem : ""}`}>
                                <div onClick={() => setActive(index)} className={item}>
                                    <div className={WrIcon}>
                                        <Arrowx5Fup1Icon
                                            style={active === index ? {
                                                transform: "rotate(90deg)",
                                                color: "#0045FF"
                                            } : { className: iconHv }}
                                        />
                                    </div>
                                    <div className={title}>{translate(`registerPage.rightContent.${value.title}`)}</div>
                                </div>
                                {
                                    active === index ?
                                        <div className={childTitle}>{translate(`registerPage.rightContent.${value.childTitle}`)}</div>
                                        : ""
                                }
                            </div>
                        ))
                    }
                </div>
            </div>

        </div>
    )
}

export default withTranslation()(Index)
