import React, { useState } from 'react'
import LearnMore from 'components/button/learnMore'
import { useHistory } from "react-router-dom";

import { title, description, joinOur, leftJoinOur, rightJoinOur, wrJoinOur, infoBtn, learnMore } from 'assets/scss/joinOur/index.module.scss'
import useWindowDimensions from 'utils/windowDimensions';

import { withTranslation } from 'react-i18next';

function Index({ t }) {
    const translate = t;
    const history = useHistory();
    const { width } = useWindowDimensions()

    return (
        <div className={joinOur}>
            <div className={title} style={{ marginTop: 192, textAlign: "center" }}>{translate('joinUs.title')}</div>
            <div className={description} style={{ marginTop: 40, textAlign: "center" }}>{translate('joinUs.subTitle')}</div>
            <div className={wrJoinOur}>
                <div className={leftJoinOur}>
                    <div className={infoBtn}>Business to consumer</div>
                    <div className={infoBtn}>Business to business</div>
                </div>
                <div className={rightJoinOur}>
                    <div className={title}>{translate('joinUs.message1Title')}</div>
                    <div className={description}>{translate('joinUs.message1Desc')}</div>
                    <LearnMore
                        title={translate('joinUs.registerAsCustomer')}
                        onClick={() => {
                            window.open(process.env.REACT_APP_REGISTER_USER, '_blank')
                            // onClick={() => history.push(`/signup?typeUser=User`)}
                        }}
                        style={learnMore}
                    />
                </div>
            </div>

            <div className={wrJoinOur}>
                {width > 1128 ? < div className={rightJoinOur}>
                    <div className={title}>{translate('joinUs.message2Title')}</div>
                    <div className={description}>{translate('joinUs.message2Desc')}</div>
                    <LearnMore
                        title={translate('joinUs.registerAsVendor')}
                        onClick={() => {
                            window.open(process.env.REACT_APP_VENDOR, '_blank')
                            // history.push(`/signup?typeUser=Vendor`)
                        }}
                        style={learnMore}
                    />
                </div>
                    : ""
                }
                <div className={leftJoinOur}>
                    <div className={infoBtn}>{translate('joinUs.truckVendor')}</div>
                    <div className={infoBtn}>Freight Fowarding</div>
                    <div className={infoBtn}>Shipping Line</div>
                </div>
                {
                    width < 1128 ?
                        <div className={rightJoinOur}>
                            <div className={title}>{translate('joinUs.message2Title')}</div>
                            <div className={description}>{translate('joinUs.message2Desc')}</div>
                            <LearnMore
                                title={translate('joinUs.registerAsVendor')}
                                onClick={() => {
                                    window.open(process.env.REACT_APP_VENDOR, '_blank')
                                    // history.push(`/signup?typeUser=Vendor`)
                                }}
                                style={learnMore}
                            />
                        </div>
                        :
                        ''
                }
            </div>
        </div >
    )
}

export default withTranslation()(Index);
