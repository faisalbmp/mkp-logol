import React, { useState } from 'react'
import UploadDocument from "components/upload/upload-document"
import UploadContainer from "components/upload/uploadContainer"
import { RemoveContainerIcon, DropDownUpArrow, PhillContainerIcon, DropDownArrow, CounterIncrementIcon, CounterDecrementCounter, AddContainerIcon, FullSunIcon, HalfSunIcon, DestinationIcon, UploadIcon, PortHandleIcon } from "../../icon/iconSvg"
import InputText from "../../input/inputTextT"
import Counter from "components/content/counter";
import { connect } from 'react-redux'
import { getContainerTypeList } from "actions"
import moment from 'moment'
import ReactDatePicker, { CalendarContainer } from 'react-datepicker'
import { megamenu, buttonSelect, gridItem, title, lineCenter } from 'assets/scss/megamenu/megamenu.module.scss';
import 'assets/scss/component/react-datepicker.scss'
// Container date
    const waktuInfo = [
        {
            id: 0,
            name: "Siang",
            isFullSun: true
        },
        {
            id: 1,
            name: "Pagi",
            isFullSun: false
        }
    ]

    const ExampleCustomInput = ({ time, startDate, resetPickers, onClick }) => {
        return (
            <div className = "input-box-container" >
                <div className = "input-text-box" style = 
                    {{
                        alignItems: 'center',
                        width:'180px'
                    }}  
                onClick={onClick}>
                    {
                        startDate != ""  ?
                        <div>
                            <div>
                                { moment(startDate).format('DD MMM, yyyy')}
                            </div>
                        </div>

                        : <div style = {{color:'#AFB5BF'}}>Pilih Tanggal</div>
                    }
                </div>
            </div>
        )
    };

    const MyContainer = ({ className, children }) => {
        return (
            <div style={{ marginTop: 21 }}>
            <CalendarContainer className={className}>
                <div style={{ position: "relative" }}>{children}</div>
            </CalendarContainer>
            </div>
        );
    }

//container date end

const standardList = [
    {
        id: 0,
        title: "20’ GP - Rp 1.700.000",
    },
    {
        id: 1,
        title: "40’ HU - Rp 1.800.000",
    }
]

const twentyGeneralData = [
    {
        "no": 1,
        "containerNo" : "DFSU-7756502",
        "sealNo" : "TBA3",
        "location" : "Blok B22 Hangar 4 No. 5"
    },
    {
        "no": 2,
        "containerNo" : "DFSU-7756503",
        "sealNo" : "TBA4",
        "location" : "Blok B22 Hangar 4 No. 6"
    },
    {
        "no": 3,
        "containerNo" : "DFSU-7756504",
        "sealNo" : "TBA5",
        "location" : "Blok B22 Hangar 4 No. 7"
    },
    {
        "no": 4,
        "containerNo" : "DFSU-7756505",
        "sealNo" : "TBA6",
        "location" : "Blok B22 Hangar 4 No. 8"
    },
    {
        "no": 5,
        "containerNo" : "DFSU-7756506",
        "sealNo" : "TBA7",
        "location" : "Blok B22 Hangar 4 No. 1"
    },

]

const fortyHighlData = [
    {
        "no": 1,
        "containerNo" : "DFSU-7756507",
        "sealNo" : "TBA8",
        "location" : "Blok B22 Hangar 4 No. 2"
    },
    {
        "no": 2,
        "containerNo" : "DFSU-7756508",
        "sealNo" : "TBA9",
        "location" : "Blok B22 Hangar 4 No. 3"
    }
]
class MautaImport extends React.Component  {
    state = {
        date:'',
        showWaktu : false,
        fullSun : false,
        waktu : 'Pagi',
        standardArr : [],
        standardOpen: false,
        containerName: 'Pilih Kontainer',
        containerType: '',
        quantity: 0,
        containerTypeList: [],
        openArrayStandardList : false,
        openArrayWaktuList : false,
        twentyStandardFlag: true,
        fortyHightCube: true
    }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(getContainerTypeList()).then(() => {
            this.setState({
                containerTypeList : this.props.containerTypeListData
            })
        })
    }

    showWaktuDetail = () => {
        this.setState({
            showWaktu: !this.state.showWaktu
        })
    }

    setWaktuDetail = (item, i) => {
        this.setState({
            fullSun: item.isFullSun,
            waktu: item.name,
            showWaktu: false
        })
    }

    openStandardList = () => {
        this.setState({
            standardOpen: !this.state.standardOpen
        })
    }

    selectStandard = (item) => {
        this.setState({
            containerName: item.description,
            containerType: item.containerTypeID,
            standardOpen: false
        })
    }

    addContainer = () => {
        const { standardArr, date, waktu, containerType, containerName, quantity, fullSun } = this.state;
        var data = {
            "date" : date,
            "waktu" : waktu,
            "containerName" : containerName,
            "containerType" : containerType,
            "quantity": quantity,
            "isFullSun": fullSun,
            "info":[]
        }
        
        for(let i = 0; i < quantity; i++) {
            var infoData = {
                "containerNumber": "",
                "location": "",
                "sealNumber": ""
            }
            data.info.push(infoData)
        }
        standardArr.push(data)
        this.setState({
            standardArr: standardArr,
        }, () => {
            this.setState({
                date:'',
                showWaktu : false,
                fullSun : false,
                waktu : 'Pagi',
                containerName: 'Pilih Kontainer',
                containerType: '',
                quantity: 0,
            })
        })
        this.props.containerData(this.state.standardArr)
    }

    incrementCounter = () => {
        this.setState({ quantity : this.state.quantity + 1})
    }

    decrementCounter = () => {
        const { quantity } = this.state;
        if(quantity == 0) {
            this.setState({ quantity : 0})
        } else {
            this.setState({ quantity : this.state.quantity - 1})
        }
    }

    removeContainer = (item) => {
        const { standardArr } = this.state;
        const index = standardArr.indexOf(item);
        if( index > -1) {
            standardArr.splice(index, 1)
        }
        this.setState({
            standardArr: standardArr
        })
        this.props.containerData(this.state.standardArr)
    }

    removeInfo = (item) => {
        const { standardArr } = this.state;
        var index = '';
        standardArr.map((it, id) => {
            index = it.info.indexOf(item)
            if( index > -1) {
                it.info.splice(index, 1)
            }
            it.quantity = it.quantity - 1
        })
        this.setState({
            standardArr: standardArr
        })
        this.props.containerData(this.state.standardArr)
    }
    selectedDate = (date, i) => {
        const { standardArr } = this.state;
        standardArr.map((item, index) => {
            if(index === i) {
                item.date = date
            }
        })
        this.setState({
            standardArr : standardArr
        })
        this.props.containerData(this.state.standardArr)
    }
    openStandardArrayStandardList = () => {
        this.setState({
            openArrayStandardList : !this.state.openArrayStandardList
        })
    }

    selectedArrayStandardList = (item, index) => {
        const { standardArr } = this.state;
        standardArr.map((it, i) => {
            if(index === i) {
                it.containerName = item.description
                it.containerType = item.containerTypeID
            }
        })
        this.setState({
            standardArr : standardArr,
            openArrayStandardList : false
        })
        this.props.containerData(this.state.standardArr)
    }

    selectedArrayWaktuList = (item, index) => {
        const { standardArr } = this.state;
        standardArr.map((it, i) => {
            if(index === i) {
                it.waktu = item.name
                it.isFullSun  = item.isFullSun
            }
        })
        this.setState({
            standardArr : standardArr,
            openArrayWaktuList : false
        })
        this.props.containerData(this.state.standardArr)
    }

    toggleArrayWaktuList = () => {
        this.setState({
            openArrayWaktuList : !this.state.openArrayWaktuList
        })
    }

    onArrayIncrementClicked = (index) => {
        const { standardArr } = this.state;
        standardArr.map((it, i) => {
            if(index === i) {
                it.quantity = it.quantity + 1
            }
            var infoData = {
                "containerNumber": "",
                "location": "",
                "sealNumber": ""
            }
            it.info.push(infoData)
        })
        this.setState({
            standardArr : standardArr,
        })
        this.props.containerData(this.state.standardArr)
    }

    onArrayDecrementClicked = (index) => {
        const { standardArr } = this.state;
        standardArr.map((it, i) => {
            if(index === i) {
                if(it.quantity  == 0) {
                    it.quantity = 0
                } else {
                    it.quantity = it.quantity - 1
                    it.info.pop()
                }
            }
        })
        this.setState({
            standardArr : standardArr,
        })
        this.props.containerData(this.state.standardArr)
    }

    billOfLandingDocName = (val) => {
        this.props.getBillOfLandingName(val.target.value)
    }

    deliveryOrderDocName = (val) => {
        this.props.getDeliveryOrderName(val.target.value)
    }
    getBillOfLandingDocument = (files) => {
        this.props.billOfLandingDocument(files)
    }
    getDeliveryDocumentDocument = (files) => {
        this.props.deliveryDocumen(files)
    }

    onSealNoChanged = (e, index) => {
        const { standardArr } = this.state;
        var sealNo = e.target.value
        standardArr.map((item, i) => {
            item.info.map((it, ind) => {
                if(index == ind) {
                    it.sealNumber = sealNo
                }
            })
        })
        this.setState({
            standardArr : standardArr,
        })
        this.props.containerData(this.state.standardArr)
    }
    onLocationChanged = (e, index) => {
        const { standardArr } = this.state;
        var location = e.target.value
        standardArr.map((item, i) => {
            item.info.map((it, ind) => {
                if(index == ind) {
                    it.location = location
                }
            })
        })
        this.setState({
            standardArr : standardArr,
        })
        this.props.containerData(this.state.standardArr)
    }
    onContainerNoChanged= (e, index) => {
        const { standardArr } = this.state;
        var containerNo = e.target.value
        standardArr.map((item, i) => {
            item.info.map((it, ind) => {
                if(index == ind) {
                    it.containerNumber = containerNo
                }
            })
        })
        this.setState({
            standardArr : standardArr,
        })
        this.props.containerData(this.state.standardArr)
    }
    render() {
        const {waktu, date, fullSun, showWaktu, standardArr, standardOpen, containerName, quantity, containerTypeList, twentyStandardFlag, fortyHightCube } = this.state;
        return (
            <div className = "form-inner-container" style={this.props.containerStyle}>
                <div style = {{marginTop:'30px'}}>
                    <div className = "form-title" >
                        Waktu Muat Barang dan Pilih Kontainer
                    </div>
                        {/* container design */}
                        <div className = "load-goods-container" >
                            <div className = "load-good-container-header" >
                                <span className = "tanggal">Tanggal</span>
                                <span className = "waktu">Waktu</span>
                                <span className = "kontainer">Kontainer</span>
                                <span className = "jumlah">Jumlah</span>
                            </div>
                            {
                                standardArr.map((item, index) => {
                                    return (
                                        <div key = {index} className = "load-good-container-body" >
                                            <div className = "tanggal">
                                                <ReactDatePicker
                                                    customInput={
                                                    <ExampleCustomInput
                                                        time={"Waktu Pengiriman"}
                                                        startDate={item.date}
                                                    />
                                                    }
                                                    calendarContainer={MyContainer}
                                                    //selected={new Date()}
                                                    onChange={startDate => this.selectedDate(startDate, index)}
                                                >
                                                </ReactDatePicker>
                                            </div>
                                            <div className = "waktu">
                                                <div className = "waktu-select" onClick = {this.toggleArrayWaktuList} style={{marginTop:10}}>
                                                    {
                                                        item.isFullSun ? <FullSunIcon/> : <HalfSunIcon/>
                                                    }
                                                    <span style={{marginLeft:10}}>
                                                        {item.waktu}
                                                    </span>
                                                </div>    
                                                    {
                                                    this.state.openArrayWaktuList && 
                                                    <div className = "waktu-select-box-container" >
                                                        <ul>
                                                            {
                                                                waktuInfo.map((item,i) => (
                                                                    <li key={index} className = "waktu-select-list-container" onClick = {() => this.selectedArrayWaktuList(item, index)}>
                                                                        {
                                                                            item.isFullSun 
                                                                            ? <FullSunIcon/>
                                                                            : <HalfSunIcon/>
                                                                        }
                                                                        <span>{item.name}</span>
                                                                    </li>
                                                                ))
                                                            }
                                                            
                                                        </ul>
                                                    </div>
                                                }
                                            </div>
                                            <div className = "kontainer" >
                                                <div style = {{marginTop:'10px'}}>
                                                    <div className = "select-box-wrapper">
                                                        <div className = "select-box-header" onClick = {this.openStandardArrayStandardList} style = {{width:251, height: 52}}>
                                                            <div className = "select-box-header-title">{item.containerName}</div>
                                                            {
                                                                this.state.openArrayStandardList 
                                                                ? <DropDownArrow/>
                                                                : <DropDownUpArrow/>
                                                            }
                                                        </div>
                                                        {
                                                            this.state.openArrayStandardList  && 
                                                            <div className = "select-box-list-container">
                                                                <ul>
                                                                    {
                                                                        containerTypeList.map((item,i) => {
                                                                            return(
                                                                                <li key={index} className = "select-box-list-value" onClick={() => this.selectedArrayStandardList(item, index)} >{item.description}</li>
                                                                            )
                                                                        })
                                                                    }
                                                                    
                                                                </ul>
                                                            </div>
                                                        }
                                                    </div>  
                                                </div>  
                                            </div>
                                            <div className = "jumlah" style={{marginTop:10}}>
                                                <div className = "counter">
                                                    <div className = "counter-container" >
                                                        <div className = "counter-value" >
                                                            {item.quantity}
                                                        </div>
                                                        <div className = "counter-button-container" >
                                                            <div className = "counter-increment-button" onClick = {() => this.onArrayIncrementClicked(index)} >
                                                                <CounterIncrementIcon />
                                                            </div>
                                                            <div className = "counter-increment-button" onClick = {() => this.onArrayDecrementClicked(index)} >
                                                                <CounterDecrementCounter />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className = "add-container-icon" style={{marginTop:10}}>
                                                <div onClick = {() => this.removeContainer(item)} ><RemoveContainerIcon/></div>
                                            </div>
                                        </div>
                                    )
                                })
                            }
                            <div className = "load-good-container-body" >
                                <div className = "tanggal">
                                    <ReactDatePicker
                                        customInput={
                                        <ExampleCustomInput
                                            time={"Waktu Pengiriman"}
                                            startDate={date}
                                        />
                                        }
                                        calendarContainer={MyContainer}
                                        onChange={startDate => this.setState({date : startDate})}
                                    >
                                    </ReactDatePicker>
                                </div>
                                <div className = "waktu">
                                    <div className = "waktu-select" onClick = {this.showWaktuDetail} style={{marginTop:10}}>
                                        {
                                            fullSun ? <FullSunIcon/> : <HalfSunIcon/>
                                        }
                                        <span style={{marginLeft:10}}>
                                            {waktu}
                                        </span>
                                    </div>    
                                        {
                                        showWaktu && 
                                        <div className = "waktu-select-box-container" >
                                            <ul>
                                                {
                                                    waktuInfo.map((item,i) => (
                                                        <li key={i} className = "waktu-select-list-container" onClick = {() => this.setWaktuDetail(item)}>
                                                            {
                                                                item.isFullSun 
                                                                ? <FullSunIcon/>
                                                                : <HalfSunIcon/>
                                                            }
                                                            <span>{item.name}</span>
                                                        </li>
                                                    ))
                                                }
                                                
                                            </ul>
                                        </div>
                                    }
                                </div>
                                <div className = "kontainer" >
                                    <div style = {{marginTop:'10px'}}>
                                        <div className = "select-box-wrapper">
                                            <div className = "select-box-header" onClick = {this.openStandardList} style = {{width:251, height: 52}}>
                                                <div className = "select-box-header-title">{containerName}</div>
                                                {
                                                    this.state.openArrayStandardList 
                                                    ? <DropDownArrow/>
                                                    : <DropDownUpArrow/>
                                                }
                                            </div>
                                            {
                                                standardOpen  && 
                                                <div className = "select-box-list-container">
                                                    <ul>
                                                        {
                                                            containerTypeList.map((item,i) => {
                                                                return(
                                                                    <li key={i} className = "select-box-list-value" onClick={() => this.selectStandard(item)} >{item.description}</li>
                                                                )
                                                            })
                                                        }
                                                        
                                                    </ul>
                                                </div>
                                            }
                                        </div>  
                                    </div>  
                                    {/* <SelectBox value = {containerName} headerStyle = {{width:251, height: 52}} openList = {standardOpen} isSearch = {false} openSelectList = {this.openStandardList} selectedList = {this.selectStandard} selectList = {containerTypeList.map((v) => v.description)} /> */}
                                </div>
                                <div className = "jumlah">
                                    <Counter value = {quantity} onIncrementClicked = {this.incrementCounter} onDecrementClicked = {this.decrementCounter} counterContainerStyle = {{marginTop:10}} />
                                </div>
                                <div className = "add-container-icon" style={{marginTop:10}}>
                                    <div onClick = {this.addContainer}><AddContainerIcon/></div>
                                </div>
                            </div>
                        </div>
                    
                        {/* container design end */}
                </div>
                {
                    standardArr.map((item, index) => {
                        // const currentPage = 1;
                        // const postsPerPage = 5;
                        // const indexOfLastPost = currentPage * postsPerPage;
                        // const indexOfFirstPost = indexOfLastPost - postsPerPage;
                        return(
                            <div style={{marginTop:'20px'}}>
                                <div className = "form-title" >
                                    Data Kontainer 
                                </div>
                                <div className = "data-container-info-title" onClick = {() => this.setState({twentyStandardFlag:!this.state.twentyStandardFlag})}>
                                    <div className= "data-container-title">
                                        {item.containerName + ' | ' + moment(item.date).format('DD MMM, yyyy')} 
                                    </div>
                                    <div className = "data-container-icon">
                                        <div className = "phill-container">
                                            <PhillContainerIcon/>
                                            <span>Pilih Kontainer</span>
                                        </div>
                                        {
                                            twentyStandardFlag 
                                            ? <DropDownUpArrow color={"#333333"}/>
                                            : <DropDownArrow color = {"#333333"}/>
                                        }
                                        
                                    </div>
                                </div>
                                {
                                    twentyStandardFlag &&
                                    <div style={{marginTop:'20px'}}>
                                            <div className = "data-container-table-heading">
                                                <span style={{width:'70px'}}>
                                                    <div style={{width:'42px', textAlign:'center'}}>#</div>
                                                </span>
                                                <span style={{width:'240px'}}>Nomor Kontainer</span>
                                                <span style={{width:'200px'}}>Nomor Seal</span>
                                                <span style={{width:'300px'}}>Lokasi di Pelabuhan</span>
                                            </div>
                                            {
                                                item.info.map((ite, inde) => {
                                                    return(
                                                        <div className = "data-container-table-value">
                                                            <span style={{width:'70px'}}>
                                                                <input type="text" style = {{width:'42px', height:'52px'}} value = {inde+1} disabled = {true}/>
                                                            </span>
                                                            <span style={{width:'240px'}}>
                                                                <input type="text" style =  {{width:'225px', height:'52px'}} onChange = { (e) => this.onContainerNoChanged(e, inde)} />
                                                            </span>
                                                            <span style={{width:'200px'}}>
                                                                <input type="text" style =  {{width:'186px', height:'52px'}} onChange = { (e) => this.onSealNoChanged(e, inde)} />
                                                            </span>
                                                            <span style={{width:'300px'}}>
                                                                <input type="text" style = {{width:'291px', height:'52px'}} onChange = { (e) => this.onLocationChanged(e, inde)} />
                                                            </span>
                                                            <div className = "add-container-icon" style={{marginTop:10}}>
                                                                <div onClick = {() => this.removeInfo(ite)} ><RemoveContainerIcon/></div>
                                                            </div>
                                                        </div>
                                                    )
                                                })
                                            }
                                            {/* <Pagination
                                                postsPerPage={postsPerPage}
                                                totalPosts={orderListData.data != undefined ? orderListData.data.length : 0}
                                                paginate={paginate}
                                                currentPage={currentPage}
                                                paginatePrevious={paginatePrevious}
                                                paginateNext={paginateNext}
                                                currentPage={currentPage}
                                            /> */}
                                    </div>
                                }
                            </div>
                        )
                    })
                }
            </div>    
        )
    }
}
function mapStateToProps(state) {
    return {
        containerTypeListData : state.containerTypeList.containerTypeListData,
    }
  }
  
export default connect(mapStateToProps)(MautaImport)
