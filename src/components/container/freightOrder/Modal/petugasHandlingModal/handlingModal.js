import React, { useState } from 'react';

import Modal from 'components/modal/wrepperModal';
import InputText from 'components/input/inputText';
import Classes from '../customModal.module.scss';
import { PlusIcon, XIcon } from '../../icon/icon';

const HandlingModal = props => {
	const [showLiftOff, setShowLiftOff] = useState(false);
	return (
		<Modal
			show={props.show}
			onShow={props.onShow}
			onClose={props.onClose}
			width={420}
			height={452}
		>
			<div
				style={{
					borderRadius: 10,
					background: '#ffffff',
					padding: '20px 20px 32px 20px',
				}}
			>
				<h4>Petugas Handling</h4>
				<div style={{ marginBottom: 16 }}>
					<InputText
						textInputTitle='Petugas Lift On'
						inputTextStyle={{ width: '100%', height: 52 }}
					/>
				</div>
				<div style={{ marginBottom: 16 }}>
					<InputText
						textInputTitle='No Telpon'
						inputTextStyle={{ width: '100%', height: 52 }}
					/>
				</div>
				<div
					style={{
						borderTop: '1px solid #E0E5E8',
						borderBottom: '1px solid #E0E5E8',
						marginRight: -20,
						marginLeft: -20,
						padding: 20,
						cursor: 'pointer',
					}}
				>
					<div
						style={{
							display: 'flex',
							flexFlow: 'row',
							justifyContent: 'space-between',
						}}
						onClick={() => setShowLiftOff(!showLiftOff)}
					>
						<div>Tambah Petugas Lift Off</div>
						{showLiftOff ? <XIcon /> : <PlusIcon />}
					</div>
					{showLiftOff ? (
						<div>
							<div style={{ marginBottom: 16 }}>
								<InputText
									textInputTitle='Petugas Lift Off'
									inputTextStyle={{ width: '100%', height: 52 }}
								/>
							</div>
							<div style={{ marginBottom: 16 }}>
								<InputText
									textInputTitle='No Telpon'
									inputTextStyle={{ width: '100%', height: 52 }}
								/>
							</div>
						</div>
					) : (
						''
					)}
				</div>
				<div style={{ textAlign: 'right', marginTop: 24 }}>
					<button className='btn-primary' style={{ width: 183, height: 56 }}>
						Simpan
					</button>
				</div>
			</div>
		</Modal>
	);
};

export default HandlingModal;
