import React, { useState } from 'react';
import Styles from './InputDetail.module.scss';
import { SelectBoxM } from 'components/select';
import InputDate from 'components/input/inputDate';
import InputNumber from 'components/input/inputNumber';
import { MinusIcon, PlusContainerIcon } from '../../../icon/icon';

const InputDetail = props => {
	let [number, setNumber] = useState(0);

	const clickUp = () => {
		setNumber(number++);
	};

	const clickDown = () => {
		setNumber(number--);
	};

	return (
		<div className={Styles.container}>
			<div style={{ width: 110, marginRight: 8 }}>
				{/* <InputDate /> */}
				<SelectBoxM />
			</div>
			<div style={{ width: 100, marginRight: 16 }}>
				<SelectBoxM />
			</div>
			<div style={{ width: 195, marginRight: 8 }}>
				<SelectBoxM />
			</div>
			<div style={{ width: 80, marginRight: 10 }}>
				<InputNumber
					value={number}
					setInput={e => setNumber(e.target.value)}
					clickUp={clickUp}
					clickDown={clickDown}
				/>
			</div>
			<div style={{ alignItems: 'center', display: 'flex', cursor: 'pointer' }}>
				{props.isPlus ? <PlusContainerIcon /> : <MinusIcon />}
			</div>
		</div>
	);
};

export default InputDetail;
