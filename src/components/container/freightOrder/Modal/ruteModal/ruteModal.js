import React from 'react';
import Styles from 'assets/splitPage.module.scss';
import Classes from '../customModal.module.scss';
import { SelectBoxM } from 'components/select';
import {
	RoundAIcon,
	RoundBIcon,
	RightArrowSearhcIcon,
	CheckBoxIcon,
	CloseCheckIcon,
} from 'components/icon/iconSvg';

import Modal from 'components/modal/wrepperModal';
import InputDetail from './InputDetail/InputDetail';

const RuteModal = props => {
	return (
		<Modal
			show={props.show}
			onShow={props.onShow}
			onClose={props.onClose}
			width={606}
			height={593}
		>
			<div style={{ borderRadius: 10, background: '#ffffff', padding: 20 }}>
				<h4 className={`${Styles.label} ${Classes.title}`}>Rute Truk</h4>
				<div
					className={`${Styles.font14} ${Styles.ruteTruck}`}
					style={{ marginTop: 8 }}
				>
					<RoundAIcon />
					<div className={`${Styles.textRute} ${Classes.address}`}>
						{props.address}
					</div>
				</div>

				<div
					className={Styles.ruteTruck}
					style={{ marginTop: 8, marginLeft: -2 }}
				>
					<RightArrowSearhcIcon
						color={'#333333'}
						width={19}
						height={11.19}
						rightArrowSearhcIcon={Styles.rightArrowSearhcIcon}
					/>
					<div
						style={{ marginLeft: 8, marginBottom: 0 }}
						className={Styles.title2}
					>
						({34} km)
					</div>
				</div>

				<div
					className={`${Styles.font14} ${Styles.ruteTruck}`}
					style={{ marginTop: 8 }}
				>
					<RoundBIcon />
					<div className={`${Styles.textRute} ${Classes.address}`}>
						{' '}
						Cikarang, Cikarang Jalan Pasir Randu, Sukasari, Bekasi, , Jawa
						Barat, Indonesia
					</div>
				</div>
				<div style={{ width: 151, marginTop: 24 }}>
					<SelectBoxM label='Grade Truck A' />
				</div>

				<div style={{ marginTop: 24 }}>
					<div
						className={`${Styles.label} ${Classes.title}`}
						style={{ marginBottom: 10 }}
					>
						Waktu Muat Barang
					</div>
					<div
						style={{
							display: 'flex',
							flexFlow: 'row',
							alignItems: 'center',
							marginBottom: 8,
						}}
					>
						<div
							className={`${Styles.label} ${Classes.title}`}
							style={{ width: 110, marginRight: 8 }}
						>
							Tanggal
						</div>
						<div
							className={`${Styles.label} ${Classes.title}`}
							style={{ width: 100, marginRight: 16 }}
						>
							Waktu
						</div>
						<div
							className={`${Styles.label} ${Classes.title}`}
							style={{ width: 195, marginRight: 8 }}
						>
							Kontainer
						</div>
						<div
							className={`${Styles.label} ${Classes.title}`}
							style={{ width: 80, marginRight: 10 }}
						>
							Jumlah
						</div>
					</div>
					{/* <SelectBoxM label='Grade Truck A' /> */}
					<InputDetail />
					<InputDetail isPlus={true} />
					<div className={Classes.modalFooter} style={{ marginTop: 24 }}>
						<div className={Classes.price}>IDR 3.000.000</div>
						<button className='btn-primary' style={{ width: 183, height: 56 }}>
							Simpan
						</button>
					</div>
				</div>
			</div>
		</Modal>
	);
};

export default RuteModal;
