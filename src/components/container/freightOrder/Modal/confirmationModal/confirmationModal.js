import React from 'react';
import Modal from 'components/modal/wrepperModal';
import Styles from './confirmationModal.module.scss';
import { TextXL, TextM } from 'components/text';
import { ButtonM } from 'components/button';
import { Colors } from 'configs';

const ConfirmationModal = props => {
	return (
		<Modal
			show={props.show}
			onClose={props.onClose}
			onShow={props.onShow}
			width={500}
			height={256}
		>
			<div className={Styles.body}>
				<TextXL textStyle='bold'>Apakah Data Sudah Benar</TextXL>
				<TextM style={{ marginTop: 16 }}>
					Pastikan data Anda sudah benar. Anda tidak bisa mengubah detail
					pemesanan setelah melanjutkan ke pembayaran.
				</TextM>
				<div className={Styles.footer}>
					<ButtonM
						color={Colors.text.black}
						textStyle='bold'
						style={{
							backgroundColor: Colors.base.white,
							width: 'auto',
							padding: '12px 24px 12px 24px',
						}}
						onClick={props.onClose}
					>
						Cek Kembali
					</ButtonM>
					<ButtonM
						color={Colors.text.white}
						textStyle='bold'
						style={{
							backgroundColor: '#004DFF',
							width: 'auto',
							padding: '12px 24px 12px 24px',
							marginLeft: 24,
						}}
						onClick={() =>
							props.history.push('/dashboard/add/freight/bookingsuccess')
						}
					>
						Ya, Sudah Benar
					</ButtonM>
				</div>
			</div>
		</Modal>
	);
};

export default ConfirmationModal;
