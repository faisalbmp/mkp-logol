import React from 'react';
import Modal from 'components/modal/wrepperModal';
import Styles from './mapModal.module.scss';
import Map from 'components/map/MapDirectionsRenderer';
import InputAutoComplete from 'components/input/inputAutoComplete';
import { TextL } from 'components/text';
import { Colors } from 'configs';
import { ArrowRightSmall } from '../../icon/icon';

const googleMapsApiKey = process.env.REACT_APP_GOOGLE_MAP;

const MapModal = props => {
	const places = [
		// { latitude: 25.8103146, longitude: -80.1751609 },
		// { latitude: 28.4813018, longitude: -81.4387899 },
		'',
		'',
	];

	console.log(props.dataAutoComplete);

	return (
		<Modal
			show={props.show}
			onShow={props.onShow}
			onClose={props.onClose}
			width={606}
			height={600}
		>
			<div className={Styles.modalBody}>
				<h3>{props.id}</h3>
				<div className={Styles.searchBox}>
					<InputAutoComplete
						setChange={e => props.getPlaces(e)}
						setItem={props.setItem}
						value={props.truckVal}
						arraySuggestion={props.dataAutoComplete}
					/>
				</div>

				<Map
					googleMapURL={
						'https://maps.googleapis.com/maps/api/js?key=' +
						googleMapsApiKey +
						'&libraries=geometry,drawing,places'
					}
					markers={places}
					loadingElement={<div style={{ height: `100%` }} />}
					containerElement={<div style={{ height: '100%' }} />}
					mapElement={<div style={{ height: `100%`, borderRadius: 10 }} />}
					defaultCenter={{ lat: -6.149423, lng: 106.897308 }}
					defaultZoom={12}
					defaultStyle={true}
				/>
			</div>
			<div className={Styles.modalFooter}>
				<button className={Styles.btnNext} onClick={() => props.nextStep()}>
					<TextL color={Colors.text.white}>Berikutnya</TextL>
					<ArrowRightSmall />
				</button>
			</div>
		</Modal>
	);
};

export default MapModal;
