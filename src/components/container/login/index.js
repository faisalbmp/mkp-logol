import React, { useState } from 'react';
import { useHistory, Redirect, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { loginUser } from 'actions';

import {
	hello,
	textWelcome,
	form,
	wrepperBottom,
	wrepperCheckbox,
	text,
	blue,
	btnPrimary,
	textBottom,
	left,
	container,
	content,
} from 'assets/splitPage.module.scss';
import { textError, showTextEerror } from 'assets/scss/megamenu/megamenu.module.scss';

import InputAuth from 'components/input/inputAuth';
import { UserIcon, PasswordIcon } from 'components/icon/iconSvg';
import Header from 'components/header/headerSplit';
import CheckBox from 'components/input/checkBox';
import Loading from 'components/loading/loadingButtonRight';
import { withTranslation } from 'react-i18next';

function LeftComponent({ sliderRegister, t }) {
	const [value, setValue] = useState({ userName: '', password: '' });
	const dispatch = useDispatch();
	const { isLoggingIn, isAuthenticated } = useSelector(state => state.auth);
	const [checked, setChecked] = useState(false);
	const [active, setActive] = useState(false);
	const [resError, setErrorLogin] = useState('');
	const history = useHistory();
	const { userName, password } = value;

	const translate = t;

	const checkAction = userName.length && password.length;

	const onClickSigin = async () => {
		if (checkAction) {
			const res = await dispatch(loginUser(value));
			// dispatch(insertUserProfile(value));
			setErrorLogin(res);
			if (res === true) {
				setValue({ userName: '', password: '' });
				// if (userId.substr(0, 4) === 'VEND') {
				// 	history.push('/vendor');
				// } else {
				// 	history.push('/dashboard');
				// }
				history.push('/dashboard');
			}
		} else setActive(true);
	};
	const onChange = e => {
		setValue({ ...value, [e.target.name]: e.target.value });
	};

	// if (isAuthenticated && pathname === '/signIn') {
	// 	return <Redirect to='/dashboard' />;
	// } else {
	return (
		<div className={left}>
			<div className={container}>
				<Header />
				<div className={content}></div>
				<div className={hello}>Hallo!</div>
				<div className={textWelcome}>{translate('loginPage.greetings')}</div>
				<div className={form}>
					<InputAuth
						Icon={UserIcon}
						title={translate('loginPage.username')}
						onChange={onChange}
						activeIcon={true}
						type='text'
						name='userName'
						activeManual={!userName.length && active}
					/>
					<InputAuth
						Icon={PasswordIcon}
						title={translate('loginPage.password')}
						onChange={onChange}
						activeIcon={true}
						name='password'
						type='password'
						activeManual={!password.length && active}
					/>
				</div>
				<div className={wrepperBottom}>
					<div className={wrepperCheckbox}>
						<CheckBox checked={checked} onClick={() => setChecked(!checked)} />
						<span className={text} style={{ marginLeft: 34, fontWeight: 'normal' }}>
							{translate('loginPage.rememberMe')}
						</span>
					</div>
					<div className={`${text} ${blue}`}>{translate('loginPage.forgotPassword')}</div>
				</div>

				<div style={{ paddingTop: 20 }}>
					<div
						className={`${textError}  ${
							!userName.length && active ? showTextEerror : ''
						}`}
					>
						{translate('alert.usernameMandatory')}
					</div>
					<div
						className={`${textError}  ${
							!password.length && active ? showTextEerror : ''
						}`}
					>
						{translate('alert.passwordMandatory')}
					</div>
					<div className={`${textError}  ${resError.length ? showTextEerror : ''}`}>
						{resError}
					</div>
				</div>

				<button className={btnPrimary} onClick={onClickSigin}>
					{isLoggingIn ? <Loading /> : <a>{translate('login')}</a>}
				</button>
				<div className={textBottom} style={{ marginTop: 167 }}>
					<span>{translate('loginPage.footer1')}</span>
					<span
						className={`${text}  ${blue}`}
						onClick={sliderRegister}
						style={{ marginLeft: 5 }}
					>
						{translate('loginPage.footer2')}
					</span>
				</div>
			</div>
		</div>
	);
}
// }

export default withTranslation()(LeftComponent);
