import React from 'react'
import { useLocation } from 'react-router-dom'

import { contentBg, splitWrepper, left, container, content, activeBg } from 'assets/splitPage.module.scss';
import Header from 'components/header/headerSplit'

export default function SplitPage({ RightComponent, LeftComponent, bgRight, url }) {
    let { pathname } = useLocation();

    return (
        <div className={splitWrepper}>
            <div className={left}>
                <div className={container}>
                    <Header />
                    <div className={content}>
                        {
                            <LeftComponent />
                        }
                    </div>
                </div>
            </div>
            <div className={`${contentBg}  ${pathname === url ? activeBg : ""}`} style={{ backgroundImage: `url(${bgRight})` }}>
                {
                    <RightComponent />
                }
            </div>
        </div >
    )
}