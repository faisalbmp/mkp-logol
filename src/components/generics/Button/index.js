import React from 'react';
import PropTypes from 'prop-types';

const Button = (props) => {
  const {
    children,
    textStyle,
    color,
    className,
    style,
    onClick,
  } = props

  const getComposedStyle = () => {
    let composedStyle = []
    let newStyle = {}

    if(textStyle) newStyle.fontWeight = textStyle;
    if(color) newStyle.color = color;
    
    composedStyle = {...newStyle, ...style};
    return composedStyle;
  }

  return (
    <button className={className} style={getComposedStyle()} onClick={onClick}>{children}</button>
  );
};

export default Button;

Button.defaultProps = {
  textStyle: 'normal',
  color: 'black',
  className: '',
  style: {},
  onClick: () => {},
}

Button.propTypes = {
  textStyle: PropTypes.string,
  color: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.objectOf(String),
  onClick: PropTypes.func,
};
