import React from 'react';
import PropTypes from 'prop-types';

const Text = (props) => {
  const {
    children,
    textStyle,
    color,
    className,
    style,
  } = props

  const getComposedStyle = () => {
    let composedStyle = []
    let newStyle = {}

    if(textStyle) newStyle.fontWeight = textStyle;
    if(color) newStyle.color = color;
    
    composedStyle = {...newStyle, ...style};
    return composedStyle;
  }

  return (
    <p className={className} style={getComposedStyle()}>{children}</p>
  );
};

export default Text;

Text.defaultProps = {
  textStyle: 'normal',
  color: 'black',
  className: '',
  style: {},
}

Text.propTypes = {
  textStyle: PropTypes.string,
  color: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.objectOf(String),
};
