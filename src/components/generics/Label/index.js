import React from 'react';
import PropTypes from 'prop-types';

const Label = (props) => {
  const {
    children,
    textStyle,
    color,
    className,
    style,
  } = props

  const getComposedStyle = () => {
    let composedStyle = []
    let newStyle = {}

    if(textStyle) newStyle.fontWeight = textStyle;
    if(color) newStyle.color = color;
    
    composedStyle = {...newStyle, ...style};
    return composedStyle;
  }

  return (
    <label className={className} style={getComposedStyle()}>{children}</label>
  );
};

export default Label;

Label.defaultProps = {
  textStyle: 'normal',
  color: 'black',
  className: '',
  style: {},
}

Label.propTypes = {
  textStyle: PropTypes.string,
  color: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.objectOf(String),
};
