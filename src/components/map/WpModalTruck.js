import React, { } from 'react'
export default function Modal(props) {
    return (
        props.visible &&
        <div className = "modal-container">
            <div className = "modal" style = {props.cardStyle} >
                {props.children}
            </div>
        </div>
    )
}