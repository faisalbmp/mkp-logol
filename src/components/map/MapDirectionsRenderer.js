/* global google */
import React from 'react';
import {
    withGoogleMap,
    GoogleMap,
    withScriptjs,
    Marker,
    DirectionsRenderer,
    InfoWindow,
} from 'react-google-maps';
import stylesGoogleMap from 'locales/stylesGoogleMap.json';

import { OrderMarkerIcon, MarkerTruckIcon } from 'components/icon/iconSvg';

class MapDirectionsRenderer extends React.Component {
    state = {
        directions: null,
        error: null,
    };

    componentDidMount() {
        const { places, travelMode } = this.props;

        const waypoints = places.map(p => ({
            location: { lat: p.latitude, lng: p.longitude },
            stopover: true,
        }));
        const origin = waypoints.shift().location;
        const destination = waypoints.pop().location;

        const directionsService = new google.maps.DirectionsService();
        directionsService.route(
            {
                origin: origin,
                destination: destination,
                travelMode: travelMode,
                waypoints: waypoints,
            },
            (result, status) => {
                if (status === google.maps.DirectionsStatus.OK) {
                    this.setState({
                        directions: result,
                    });
                } else {
                    this.setState({ error: result });
                }
            }
        );
    }

    render() {
        if (this.state.error) {
            return <h1>{this.state.error}</h1>;
        }
        return (
            this.state.directions && (
                <DirectionsRenderer
                    directions={this.state.directions}
                    options={{
                        polylineOptions: { strokeColor: '#1AD3D3', strokeWeight: 5 },
                        suppressMarkers: true,
                    }}
                />
            )
        );
    }
}

export default withScriptjs(
    withGoogleMap(props => (
        <GoogleMap
            defaultCenter={props.defaultCenter}
            defaultZoom={props.defaultZoom}
            options={{
                disableDefaultUI: true,
                draggable: true,
                keyboardShortcuts: false,
                scaleControl: true,
                scrollwheel: true,
                styles: props.defaultStyle ? '' : stylesGoogleMap,
            }}
        >
            {props.markers.map((marker, index) => {
                const position = { lat: marker.latitude, lng: marker.longitude };
                return <Marker key={index} position={position} />;
            })}

            <MapDirectionsRenderer
                places={props.markers}
                travelMode={google.maps.TravelMode.DRIVING}
            />
        </GoogleMap>
    ))
);
