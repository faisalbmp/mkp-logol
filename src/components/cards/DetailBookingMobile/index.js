import React, { Fragment } from 'react'
import SelectDetail from 'components/select/selectDetail'
import { RoundAIcon, RoundBIcon, RrrowTrack } from 'components/icon/iconSvg'
import { months } from 'utils/listDate'
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import { text, container, center, card, content, title1, title2, wrRoundIcon, flex, total, track, pdRight, label, bottomPosition, textFormat, feeDetail, textV } from 'assets/scss/component/detailBooking.module.scss'
import { formatPrice } from 'utils/function'
import { containerType } from 'utils/contants';

export default function Index({ selectedContainer, typeBooking, listContainer, date, totalPrice, addressB, addressA, typeExportImport }) {
    const { bottom, monthId, top } = date || {}
    const { t } = useTranslation();

    const addressAStyle = typeBooking === "truck" && <div className={`${text} ${pdRight}`}>{addressA.terms ? `${addressA.description}` : ""}</div>
    const addressBStyle = typeBooking === "truck" && <div className={`${text} ${pdRight}`}>{Object.keys(addressB).length ? addressB.portUTC : ''}</div>
    return (
        <div>
            <div style={{ marginTop: 24 }}>
                <SelectDetail title={t('requestScheduler.detailBooking.title')}>
                    <div className={label} style={{ marginTop: 24 }}>{t('requestScheduler.detailBooking.deliveryRoute')}</div>
                    <div className={content} style={{ padding: 0 }}>
                        <div className={track} style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
                            {
                                typeBooking === "truck" ?
                                    <Fragment className={track} style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
                                        <div className={track}>
                                            <div className={wrRoundIcon}>
                                                <RoundAIcon pdRight={pdRight} />
                                            </div>
                                            {typeExportImport === "export" ? addressAStyle : addressBStyle}
                                        </div>
                                        <RrrowTrack pdRight={`${pdRight}   ${bottomPosition}`} />
                                        <div className={track}>
                                            <div className={wrRoundIcon}>
                                                <RoundBIcon pdRight={pdRight} />
                                            </div>
                                            {typeExportImport === "export" ? addressBStyle : addressAStyle}
                                        </div>
                                    </Fragment>
                                    :
                                    <Fragment>
                                        <div className={track}>
                                            <RoundAIcon pdRight={pdRight} />
                                            <div className={`${text} ${pdRight}`}>{addressA ? addressA.name || '' : ""}, {addressA ? addressA.country || '' : ""}</div>
                                        </div>
                                        <RrrowTrack pdRight={`${pdRight}   ${bottomPosition}`} />
                                        <div className={track}>
                                            <RoundBIcon pdRight={pdRight} />
                                            <div className={`${text} ${pdRight}`}>{addressB ? addressB.name || '' : ""}, {addressB ? addressB.country || '' : ""}</div>
                                        </div>
                                    </Fragment>
                            }
                        </div>
                        {
                            typeBooking === "freight" ?
                                <Fragment>
                                    <div className={label} style={{ marginTop: 24 }}>{t('requestScheduler.detailBooking.deliveryTime')} (ETD-ETA)</div>
                                    <div className={text} >{date ? `${bottom} ${months[monthId - 1]} ${moment().format("YYYY")}` : ""} - {date ? `${top} ${months[monthId - 1]} ${moment().format("YYYY")}` : ""}</div>
                                    <div className={label} style={{ marginTop: 24 }}>{t('requestScheduler.detailBooking.typeofDelivery')}</div>
                                    <div className={text} >{selectedContainer ? selectedContainer.name : ""}</div>
                                </Fragment>
                                :
                                <Fragment>
                                    <div className={label} style={{ marginTop: 17 }}>{t('requestScheduler.detailBooking.deliveryTime')}</div>
                                    <div className={text} >{date ? moment(date).format("DD-MM-YYYY") : ""}</div>
                                </Fragment>
                        }
                    </div>
                </SelectDetail>
            </div>

            {typeBooking === "truck" ? <div div style={{ marginTop: 24 }}>
                <SelectDetail
                    title={t('requestScheduler.detailBooking.serviceFee')}
                >
                    <div className={content} style={{ padding: 0 }}>
                        {
                            listContainer.map((value, index) => (
                                <div className={`${flex} ${textFormat}`} key={index}>
                                    <div className={feeDetail}>{value.quantity} x {value.containerType}</div>
                                    <div className={feeDetail}>{formatPrice(value.priceAll)}</div>
                                </div>
                            ))
                        }
                        <div className={`${flex} ${textFormat}`}>
                            <div className={total}>Total</div>
                            <div className={total}>{formatPrice(totalPrice)}</div>
                        </div>
                    </div>
                </SelectDetail>
            </div>
                : ""}
        </div>
    )
}