
import React from 'react'
import { useHistory } from "react-router-dom";

import { whyUseLogol, wpListImg, itemCard, titleInCard, subTitleInCard, title, subTitle, giftClass, btnYellow, wpBtnYellow, childItemCard } from 'assets/scss/ourServices/index.module.scss'
import iconClock from 'assets/images/ourService/clock.svg'
import iconNetwrok from 'assets/images/ourService/netwrok.svg'
import iconPay from 'assets/images/ourService/pay.svg'
import iconCheck from 'assets/images/ourService/check.svg'
import iconGift from 'assets/images/ourService/gift.svg'
import iconGiftM from 'assets/images/ourService/giftM.svg'
import useWindowDimensions from 'utils/windowDimensions';

import { withTranslation } from 'react-i18next';

const dataWhyUseLogol = [
    {
        icon: iconClock,
        title: "reason1",
        subTitle: "reason1Desc"
    },
    {
        icon: iconNetwrok,
        title: "reason2",
        subTitle: "reason2Desc"
    },
    {
        icon: iconPay,
        title: "reason3",
        subTitle: "reason3Desc"
    },
    {
        icon: iconCheck,
        title: "reason4",
        subTitle: "reason4Desc"
    }
]

function Index({ titleButton, onClick, t }) {
    const history = useHistory();
    const { width } = useWindowDimensions()
    const translate = t;
    return (
        <div className={whyUseLogol}>
            <div className={giftClass} >
                <img src={600 < width ? iconGift : iconGiftM} alt="img" />
            </div>
            <div className={title}>{translate('ourServices.whyBookingViaLogol')}</div>
            <div className={subTitle}>{translate('ourServices.whyBookingViaLogolDesc')}</div>
            <div className={wpListImg}>
                {
                    dataWhyUseLogol.map((value, index) => (
                        <div key={index} className={itemCard}>
                            <img src={value.icon} alt="img" />
                            <div className={childItemCard}>
                                <div className={titleInCard}>{translate(`ourServices.${value.title}`)}</div>
                                <div className={subTitleInCard}>{translate(`ourServices.${value.subTitle}`)}</div>
                            </div>
                        </div>
                    ))
                }
            </div>

            <div className={wpBtnYellow}>
                {titleButton && onClick ? <div className={"btn-yellow " + btnYellow} onClick={onClick}>
                    {titleButton}
                </div> : ""}
            </div>
        </div>
    )
}

export default withTranslation()(Index)
