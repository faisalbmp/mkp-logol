import React from "react";
import Styles from "./cardInfo2.module.scss";
import logo from "assets/images/helpCenter/djpktnLogo.png";
import arrow from "./arrow.svg";
// import logoDjkptn from "./logoDjkptn.svg";
import useWindowDimensions from "utils/windowDimensions";

const CardInfo2 = ({ title, subTitle, phone, onClick }) => {
  const { width } = useWindowDimensions();
  return (
    <div
      className={Styles.cardItem}
      style={{ minWidth: width > 800 ? 384 : 0 }}
    >
      <img src={logo} style={{ width: 229, height: 51 }} alt="not found" />
      <div className={Styles.bottom}>
        <div style={{ marginTop: 16 }}>
          <div className={Styles.cardTitle}>{title}</div>
          <div className={Styles.cardSubtitle}>{subTitle}</div>
          <div className={Styles.cardSubtitle}>{phone}</div>
          <a href="http://ditjenpktn.kemendag.go.id" target="_blank">
            http://ditjenpktn.kemendag.go.id
          </a>
        </div>
        <div className={Styles.arrowImg} onClick={onClick}>
          <img src={arrow} />
        </div>
      </div>
    </div>
  );
};

export default CardInfo2;
