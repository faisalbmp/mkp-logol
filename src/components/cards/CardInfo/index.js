import React from 'react'

import { cardItem, wpIcon, subTextCard, textCard, wpCardRight } from 'assets/scss/card/cardInfo.module.scss'

export default ({ icon, title, subTitle }) => (
    <div className={cardItem}>
        <div className={wpIcon}>
            <img src={icon} />
        </div>
        <div className={wpCardRight}>
            <div className={textCard}>{title}</div>
            <div className={subTextCard}>{subTitle}</div>
        </div>
    </div>
)