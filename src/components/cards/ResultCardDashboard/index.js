import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Styles from 'assets/scss/dashboard/resultcard.module.scss';
import { oocl } from 'assets/images';
import { ButtonM } from 'components/button';
import { Colors } from 'configs';
import { TextS, TextM, TextL } from 'components/text';
import { flagId } from 'assets/images';

const CardItem = ({ label, value }) => {
	return (
		<div className={Styles.cardItem}>
			<TextS color={Colors.text.grey1}>{label}</TextS>
			<div className={Styles.value}>
				<TextL textStyle='bold'>{value}</TextL>
			</div>
		</div>
	);
};

const Duration = () => {
	return (
		<div className={Styles.duration}>
			<TextS>5 hari</TextS>
			<div className={Styles.lineWrapper}>
				<div className={Styles.bulletFull}>
					<i className={`icon-arrow-right ${Styles.arrowWhite}`}></i>
				</div>
				<div className={Styles.line}></div>
				<div className={Styles.bulletEmpty}></div>
			</div>
			<TextS>Langsung</TextS>
		</div>
	);
};

const ContainerDetail = ({ name, price, qty }) => {
	return (
		<div className={Styles.containerDetail}>
			<div className={Styles.containerName}>
				<TextS className={Styles.costLabel}>{name}</TextS>
			</div>
			<div className={Styles.containerPrice}>
				<TextS className={Styles.costLabel}>{price}</TextS>
			</div>
			<div className={Styles.containerQty}>
				<TextS className={Styles.costLabel}>{qty}</TextS>
			</div>
		</div>
	);
};

const PortDetailItem = ({ name, city }) => {
	return (
		<div className={Styles.portDetailItem}>
			<img src={flagId} className={Styles.portFlag} />
			<div className={Styles.detail}>
				<TextM textStyle='bold' className={Styles.portName}>
					{name}
				</TextM>
				<TextS>{city}</TextS>
			</div>
		</div>
	);
};

const PortDetail = () => {
	return (
		<div className={Styles.portDetail}>
			<div className={Styles.dates}>
				<TextM textStyle='bold' className={Styles.date}>
					25 Jan, 2020
				</TextM>
				<TextM className={Styles.date}>5 hari</TextM>
				<TextM textStyle='bold' className={Styles.date}>
					30 Jan, 2020
				</TextM>
			</div>
			{/* <div style={{ display: 'flex', flexFlow: 'row' }}> */}
			<div className={Styles.lineWrapper}>
				<div className={Styles.bulletFull}>
					<i className={`icon-arrow-right ${Styles.arrowWhite}`}></i>
				</div>
				<div className={Styles.line}></div>
				<div className={`${Styles.bulletEmpty} ${Styles.grey}`}></div>
				<div className={Styles.line}></div>
				<div className={Styles.bulletEmpty}></div>
			</div>
			{/* </div> */}
			<div className={Styles.ports}>
				<PortDetailItem name='Tanjung Priok' city='Jakarta, Indonesia' />
				<TextM className={Styles.date}>Langsung</TextM>
				<PortDetailItem name='Singapore Port' city='Singapore, Singapore' />
			</div>
		</div>
	);
};

const CardDetail = () => {
	const costs = [
		'Terminal Handling Cost',
		'Seal (Segel)',
		'Biaya Bill of Lading',
		'Biaya lain dari Pelayaran',
	];

	const containers = [
		{ name: "20' General Purpose", price: 'Rp 150.000', qty: 'x3' },
		{ name: "40' General Purpose", price: 'Rp 400.000', qty: 'x3' },
		{ name: "40' High Cube", price: 'Rp 500.000', qty: 'x3' },
	];
	return (
		<div className={Styles.cardDetail}>
			<div className={Styles.left}>
				<PortDetail />
			</div>
			<div className={Styles.right}>
				<div className={Styles.costs}>
					<TextS color={Colors.text.darkgrey2} className={Styles.costTitle}>
						Biaya Sudah Termasuk
					</TextS>
					{costs.map(cost => (
						<TextS className={Styles.costLabel}>{cost}</TextS>
					))}
				</div>
				<div className={Styles.costs}>
					<TextS color={Colors.text.darkgrey2} className={Styles.costTitle}>
						Kontainer
					</TextS>
					{containers.map(container => (
						<ContainerDetail {...container} />
					))}
				</div>
			</div>
		</div>
	);
};

const ResultCard = ({ onChoose }) => {
	const [showDetail, setShowDetail] = useState(false);
	return (
		<div className={Styles.container}>
			<div className={Styles.mainCard}>
				<div className={Styles.left}>
					<img src={oocl} />
					<ButtonM
						color={Colors.text.mainblue}
						style={{
							width: 'auto',
							backgroundColor: Colors.base.transparent,
							fontSize: '14px',
						}}
						onClick={() => setShowDetail(!showDetail)}
					>
						Detail Pelayaran
					</ButtonM>
				</div>
				<div className={Styles.middle}>
					<div className={Styles.port}>
						<CardItem label='Perkiraan Berangkat' value='25 Jan, 2020' />
						<div className={Styles.portName}>
							<img src={flagId} className={Styles.portFlag} />
							<TextS
								style={{ textOverflow: 'ellipsis', overflow: 'hidden' }}
								color={Colors.black}
							>
								Tanjung Priok, Jakarta
							</TextS>
						</div>
					</div>
					<Duration />
					<div className={Styles.port}>
						<CardItem label='Perkiraan Tiba' value='30 Jan, 2020' />
						<div className={Styles.portName}>
							<img src={flagId} className={Styles.portFlag} />
							<TextS
								style={{ textOverflow: 'ellipsis', overflow: 'hidden' }}
								color={Colors.black}
							>
								Singapore Port, Singapore
							</TextS>
						</div>
					</div>
				</div>
				<div className={Styles.right}>
					<CardItem label='Total Harga' value='IDR 89.445.000' />
					<ButtonM
						color={Colors.text.white}
						className={Styles.buttonPick}
						onClick={onChoose}
					>
						Pilih
					</ButtonM>
				</div>
			</div>
			{showDetail ? <CardDetail /> : null}
		</div>
	);
};

export default ResultCard;

ResultCard.propTypes = {
	onChoose: PropTypes.func.isRequired,
};
