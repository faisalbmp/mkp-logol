import React from 'react'
import moment from 'moment'

import { monitor, boxShadow, headerMonitor, childHeaderMonitor, footer, smallText, footerRight, borderCardMonitorLabel, lightMonitorLabel, cardMonitorLabel, cardMonitorBoxShadow, cardMonitor, cardChild, cardChildLight, fontTextWeight, fontTextSemiBold, dot, row, rowChild, column, lineHeight, fontTextNormal, wizardProgress, stepName, pending, animationChangeColor, pending1, pending2, pending2Light, pending2Dark, anticon, classFullSc, textDes } from 'assets/scss/page/monitor/monitor.module.scss'



export default function index({
    data: {
        customerName,
        customerID,
        containerNumber,
        containerTypeID,
        containerTypeDesc,
        dest1,
        dest2,
        dest3,
        destPointID1,
        destPointID2,
        destPointID3,
        dest1Lat,
        dest1Lng,
        dest2Lat,
        dest2Lng,
        dest3Lat,
        dest3Lng,
        destPoint1In,
        destPoint1Out,
        destPoint2In,
        destPoint2Out,
        destPoint3In,
        destPoint3Out,
        driverName,
        orderManagementID,
        orderType,
        sealNumber,
        shippingDate,
        vehicleNumber,
        vendorID,
        vendorName,
        vendorOrderID,
        vendorOrderDetailID,
    },
    styleMode: {
        backgroundColor,
        colorText,
        type,
        colorIcon,
        backgroundHeader,
        colorHeader,
        backgroundColorCardImport,
        borderBottomHeader,
        textDarkMode,
        backgroundColorCardExport
    }
}) {

    // "customerName": "PT Dimas Customer",
    // "customerID": "CUST-20200429-0001",
    // "containerNumber": "FSCU9317211",
    // "containerTypeID": "CNTP-0005",
    // "containerTypeDesc": "40HU - High Cube",
    // "dest1": "Global Terminal Marunda (GTM)",
    // "dest2": "-",
    // "dest3": "T. Priok(KOJA)",
    // "destPointID1": "DEPO-0008",
    // "destPointID2": "FCTR-20191212-0002",
    // "destPointID3": "PORT-0002",
    // "dest1Lat": "-6.098803083494651",
    // "dest1Lng": "106.94505591144117",
    // "dest2Lat": null,
    // "dest2Lng": null,
    // "dest3Lat": "-6.1023223",
    // "dest3Lng": "106.90416649999997",
    // "destPoint1In": 1579804258000,
    // "destPoint1Out": 1579804321000,
    // "destPoint2In": 1579822012000,
    // "destPoint2Out": 1579863992000,
    // "destPoint3In": 1579881129000,
    // "destPoint3Out": 1579882119000,
    // "driverName": "Alif",
    // "orderManagementID": "LG/202001/E0088",
    // "orderType": "EXPORT",
    // "sealNumber": "OOLEVQ6549",
    // "shippingDate": 1580342400000,
    // "vehicleNumber": "B 9346 UIZ",
    // "vendorID": "VEND-20190225-0005",
    // "vendorName": "PT. Anugrah Mulia Transindo",
    // "vendorOrderID": "VOHD-20200123-000006",
    // "vendorOrderDetailID": "VODT-20200123-000042",
    return (
        <div className={`${cardMonitor} ${cardChild}   ${type === "light" ? cardChildLight : ""}`} style={{ height: 127, backgroundColor: backgroundColorCardExport, borderLeftColor: orderType == "EXPORT" ? "" : "#11CDAB" }} key={index}>
            {console.log(orderType)}
            <div className={row} style={{ alignItems: "flex-start", marginTop: 24 }}>
                <div className={column} style={{ color: colorText, paddingLeft: 32, textTransform: "uppercase" }}>
                    <div className={fontTextWeight}>{customerName}</div>
                    <div className={fontTextNormal}>{orderManagementID}</div>
                </div>
            </div>
            <div className={row} style={{ alignItems: "flex-start", marginTop: 24 }}>
                <div className={fontTextWeight} style={{ color: colorText, textTransform: "uppercase" }}>{moment(shippingDate).format("mm-yyyy")}</div>
                <div className={column} style={{ color: colorText, paddingLeft: 32, textTransform: "uppercase" }}>
                    <div className={fontTextWeight}>{containerNumber}</div>
                    <div className={fontTextNormal}>{containerTypeID}</div>
                    <div className={fontTextNormal}>{containerTypeDesc}</div>
                </div>
            </div>
            <div className={`${row} ${column}`} style={{ alignItems: "flex-start", textTransform: "uppercase" }}>
                <div className={fontTextWeight} style={{ color: colorText }}>{vendorName}</div>
                <div className={fontTextNormal} style={{ color: colorText, }}>{driverName}</div>
                <div className={fontTextNormal} style={{ color: "#30B1FF", textDecoration: "underline", cursor: "pointer" }}>{"driverID"}</div>
            </div>
            <div className={row} style={{ marginTop: 6, justifyContent: 'center' }}>
                <ol className={wizardProgress}>
                    <li>
                        <span className={`${stepName} ${textDes}`} style={{ color: colorText }}>{destPointID1}</span>
                        <span className={`${pending} ${pending1}`} style={{ backgroundColor: "#408AFF" }}></span>
                        <span className={stepName} style={{ color: colorText, fontSize: 14 }}>{destPoint1In}</span>
                    </li>
                    <li>
                        <span className={`${stepName} ${textDes}`} style={{ color: colorText }}>{destPointID2}</span>
                        <span className={`${pending} ${pending2} ${type === "light" ? pending2Light : pending2Dark}`} style={{ backgroundColor: "#FFD540" }}></span>
                        <span className={stepName} style={{ color: colorText, fontSize: 14 }}>{destPoint2In}</span>
                    </li>
                    <li style={{ marginRight: 0 }}>
                        <span className={`${stepName} ${textDes}`} style={{ color: colorText }}>{destPointID3}</span>
                        <span className={pending} style={{ backgroundColor: type === "light" ? "#E0E5E8" : "#747474" }}></span>
                        <span className={stepName} style={{ color: colorText, fontSize: 14 }}>{destPoint3In}</span>
                    </li>
                </ol>
            </div>
        </div>
    )
}
