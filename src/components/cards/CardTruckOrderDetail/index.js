import React, { } from 'react'
export default function Card(props) {
    return (
        <div className="card-container" style={props.cardStyle}>
            <div className="card">
                <div className="card-value-container">
                    <div>
                        <div className="card-value">{props.value}</div>
                        <div className="card-title">
                            {props.title}
                        </div>
                    </div>
                </div>
                <div className="card-icon-container">
                    <div className="icon" >
                        {props.icon}
                    </div>
                </div>
            </div>
        </div>
    )
}