import React, { useState, useEffect } from "react";
import { MegaMenu, MegaMenuTruck } from "components/pickers";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { setTypeBooking } from "actions";
import arrowRightButton from "assets/images/icons/arrow-right-button.png";
import NextBtn from "components/button/nextBtn";
import RedirectModal from "components/modal/redirectPopup";
import { withTranslation } from "react-i18next";
import i18n from "i18next";
import * as slActions from "actions/vesselSchedules";
import { months } from "utils/dates";
import moment from "moment";
import { setCookie } from "utils/cookies";

function ScheduleBooking({ t }) {
  const translate = t;
  const [portPicker, setPortPicker] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [validationInput, setValidationInput] = useState(false);
  const { home, booking, vesselSchedules } = useSelector((state) => state);
  const { typeBooking, bookingTruck, listContainer } = home;
  const {
    selected_origin,
    selected_destination,
    selectedDate,
    selectedContainer,
  } = booking;
  const { isLoading } = vesselSchedules;
  const dispatch = useDispatch();
  const publicUrl = process.env.PUBLIC_URL;
  const history = useHistory();
  const { addressA, addressB, date, container } = bookingTruck;
  const selectedLang = i18n.language;

  const interval = () =>
    setTimeout(async () => {
      await window.open("https://logol.co.id", "_blank");
      setTimeout(() => setShowModal(false), 100);
      clearInterval(interval);
    }, 5000);

  // const onLink = () => {
  //     if (typeBooking === "freight")
  //         window.open('https://freight.logol.co.id', '_blank');
  //     window.open('https://logol.co.id/marketplace/welcome', '_blank');
  // }

  const onRedirect = () => {
    setShowModal(true);
    // interval()
  };
  var objectAddres = Object.keys(addressA || "");

  const cariJadwal = () => {
    if (typeBooking === "truck") {
      if (
        Object.keys(addressA).length &&
        Object.keys(addressB).length &&
        listContainer.length &&
        typeof date !== "string"
      ) {
        // history.push("/bookingTruck");
        history.push("/requestSchedule");
      } else {
        setValidationInput(true);
      }
    } else {
      // history.push("/schedules");
      // history.push("/handleSchedules");

      // if (selected_origin && selected_destination && selectedDate) {
      // 	// history.push("/bookingTruck");
      // 	history.push('/schedules');
      // } else {
      // 	setValidationInput(true);
      // }
      // console.log(selected_origin);
      // console.log(selected_destination);
      if (
        selected_origin &&
        selected_destination &&
        selectedDate /* &&
        selectedContainer */
      ) {

        const dDate = `${moment(selectedDate?.departure).format("YYYY")}-${moment(selectedDate?.departure).format("MM")
          }-${moment(selectedDate?.departure)?.format("DD").toString().padStart(2, "0")}`;
        const aDate = `${moment(selectedDate?.arrival).format("YYYY")}-${moment(selectedDate?.arrival).format("MM")
          }-${moment(selectedDate?.arrival)?.format("DD").toString().padStart(2, "0")}`;

        const dateRange = selectedDate?.range;
        setCookie(
          "bookingOptions",
          JSON.stringify({
            origin: selected_origin,
            destination: selected_destination,
            date: selectedDate,
            // container: selectedContainer,
          }),
          2
        );
        dispatch(
          slActions.getVesselSchedules(
            selected_origin.internationalCode,
            selected_destination.internationalCode,
            dDate,
            aDate,
            dateRange
          )
        )
          .then((res) => {
            if (res === 1) {
              history.push("/schedules");
            } else {
              // history.push("/handleSchedules");
              history.push("/schedules");
            }
          })
          .catch((err) => {
            // console.log(err);
          });
      } else {
        setValidationInput(true);
      }
    }
  };
  return (
    <section className="intro-search">
      <div className="intro-card-container">
        <div className={"wrepper-tab "}>
          <div
            className={`tab  ${typeBooking === "freight" ? "selected" : ""}`}
            onClick={() => dispatch(setTypeBooking("freight"))}
          >
            {translate("scheduleBooking.freight")}
          </div>
          <div
            className={`tab  ${typeBooking === "truck" ? "selected" : ""}`}
            onClick={() => dispatch(setTypeBooking("truck"))}
          >
            {translate("scheduleBooking.truck")}
          </div>
        </div>

        <div className="tab-pane">
          <div
            className={`slide ${typeBooking === "freight" ? "move-to-first" : "move-to-second"
              }`}
          >
            {typeBooking === "freight" ? (
              <div className="first box">
                <MegaMenu
                  onOriginClick={() => setPortPicker(!portPicker)}
                  locationA={translate("scheduleBooking.from")}
                  locationB={translate("scheduleBooking.to")}
                  typeBooking={typeBooking}
                  hideVal={true}
                  time={translate("scheduleBooking.shippingTime")}
                  container={translate("scheduleBooking.shippingType")}
                  onRedirect={onRedirect}
                  validationInput={validationInput}
                  setValidationInput={() => setValidationInput(true)}
                />
              </div>
            ) : (
              <div className="second box">
                <MegaMenuTruck
                  onOriginClick={() => setPortPicker(!portPicker)}
                  typeBooking={typeBooking}
                  validationInput={validationInput}
                  setValidationInput={() => setValidationInput(true)}
                />
              </div>
            )}
          </div>
        </div>
      </div>

      <NextBtn
        title={translate("layoutHome.howToUse.findSchedule")}
        width={selectedLang === "en" ? 200 : 190}
        height={60}
        style={{
          width: window.innerWidth > 480 && (selectedLang === "en" ? 200 : 190),
          height: 60,
          margin:
            window.innerWidth > 480 ? "16px 0px 0px auto" : "75px 0px 0px auto",
        }}
        loading={isLoading}
        onClick={cariJadwal}
      // onClick={onRedirect}
      />

      <RedirectModal show={showModal} onClose={() => setShowModal(false)} />
    </section>
  );
}

export default withTranslation()(ScheduleBooking);
