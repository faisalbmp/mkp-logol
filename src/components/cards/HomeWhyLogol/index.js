import React, { useState, useEffect } from 'react'

import { container, cssImg, flex, title, sub_title, wpDot, dot, activeDot, wpContainer, wpAnimationContainer } from 'assets/scss/card/home_why_logol.module.scss'
import image_1 from 'assets/images/card/whyUseLogol/image_1.svg'
import image_2 from 'assets/images/card/whyUseLogol/image_2.svg'
import image_3 from 'assets/images/card/whyUseLogol/image_3.svg'
import image_4 from 'assets/images/card/whyUseLogol/image_4.svg'
import image_5 from 'assets/images/card/whyUseLogol/image_5.svg'
import image_6 from 'assets/images/card/whyUseLogol/image_6.svg'
import image_7 from 'assets/images/card/whyUseLogol/image_7.svg'

import { withTranslation } from 'react-i18next';

function Index({ t }) {
    const [active, setActive] = useState(1)
    const translate = t;

    // useEffect(() => {
    //     const timer = setTimeout(() => {
    //         setActive(active === 1 ? 2 : 1)
    //     }, 5000);

    //     return () => clearInterval(timer);
    // }, [active])

    return (
        <div className={wpContainer}>
            <div className={wpAnimationContainer} style={{ marginLeft: active === 1 ? 0 : "-100%" }}>
                <div className={container}>
                    <div className={flex}>
                        <img src={image_1} className={cssImg} alt="image_1" />
                        <div>
                            <div className={title}>{translate('layoutHome.whyLogol.service1')}</div>
                            <div className={sub_title}>{translate('layoutHome.whyLogol.service1Desc')}</div>
                        </div>
                    </div>
                    <div className={flex}>
                        <img src={image_2} className={cssImg} alt="image_2" />
                        <div>
                            <div className={title}>{translate('layoutHome.whyLogol.service2')}</div>
                            <div className={sub_title}>{translate('layoutHome.whyLogol.service2Desc')}</div>
                        </div>
                    </div>
                    <div className={flex}>
                        <img src={image_5} className={cssImg} alt="image_5" />
                        <div>
                            <div className={title}>{translate('layoutHome.whyLogol.service5')}</div>
                            <div className={sub_title}>{translate('layoutHome.whyLogol.service5Desc')}</div>
                        </div>
                    </div>
                    <div className={flex}>
                        <img src={image_4} className={cssImg} alt="image_4" />
                        <div>
                            <div className={title}>{translate('layoutHome.whyLogol.service4')}</div>
                            <div className={sub_title}>{translate('layoutHome.whyLogol.service4Desc')}</div>
                        </div>
                    </div>
                    <div className={flex}>
                        <img src={image_6} className={cssImg} alt="image_6" />
                        <div>
                            <div className={title}>{translate('layoutHome.whyLogol.service6')}</div>
                            <div className={sub_title}>{translate('layoutHome.whyLogol.service6Desc')}</div>
                        </div>
                    </div>
                    <div className={flex}>
                        <img src={image_7} className={cssImg} alt="image_7" />
                        <div>
                            <div className={title}>{translate('layoutHome.whyLogol.service7')}</div>
                            <div className={sub_title}>{translate('layoutHome.whyLogol.service7Desc')}</div>
                        </div>
                    </div>


                </div>

                {/* <div className={container}>
                    <div>
                        <div className={flex}>
                        <img src={image_3} className={cssImg} alt="image_3" />
                            <div>
                                <div className={title}>Gratis Biaya Layanan</div>
                                <div className={sub_title}>Eros in cursus turpis massa tincidunt. Dui accumsan sit amet nulla facilisi.</div>
                            </div>
                        </div>
                    </div>
                </div> */}
            </div>

            {/* <div className={wpDot}>
                <div className={`${dot} ${active === 1 ? activeDot : ""}`} onClick={() => setActive(1)}></div>
                <div className={`${dot} ${active === 2 ? activeDot : ""}`} onClick={() => setActive(2)}></div>
            </div> */}
        </div>
    )
}

export default withTranslation()(Index)