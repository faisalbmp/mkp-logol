import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import Styles from "assets/scss/schedules/result.module.scss";
import { ButtonM } from "components/button";
import { Colors } from "configs";
import { TextS, TextM, TextL } from "components/text";
import moment from "moment";
import useWindowDimensions from "utils/windowDimensions";

const CardItem = ({ label, value }) => {
  return (
    <div className={Styles.cardItem}>
      <TextS color={Colors.text.grey1}>{label}</TextS>
      <div className={Styles.value}>
        <TextL textStyle="bold">{value}</TextL>
      </div>
    </div>
  );
};

const Duration = (props) => {
  const { duration, isTransit } = props.schedule;
  const { t } = props;
  const { width } = useWindowDimensions();
  return (
    <div
      className={Styles.duration}
      style={{ width: width > 800 && props.hidePrice ? "262px" : "" }}
    >
      <TextS>{duration + ` ${t("vSchedules.day")}`}</TextS>
      <div className={Styles.lineWrapper}>
        <div className={Styles.bulletFull}>
          <i className={`icon-arrow-right ${Styles.arrowWhite}`}></i>
        </div>
        <div className={Styles.line}></div>
        <div className={Styles.bulletEmpty}></div>
      </div>
      <TextS>
        {isTransit === 0 ? t("vSchedules.direct") : t("vSchedules.transit")}
      </TextS>
    </div>
  );
};

const ContainerDetail = ({ name, price, qty }) => {
  return (
    <div className={Styles.containerDetail}>
      <div className={Styles.containerName}>
        <TextS className={Styles.costLabel}>{name}</TextS>
      </div>
      <div className={Styles.containerPrice}>
        <TextS className={Styles.costLabel}>{price}</TextS>
      </div>
      <div className={Styles.containerQty}>
        <TextS className={Styles.costLabel}>{qty}</TextS>
      </div>
    </div>
  );
};

const PortDetailItem = ({
  name,
  city,
  originFlag,
  destinationFlag,
  isOrigin,
}) => {
  return (
    <div className={Styles.portDetailItem}>
      <img
        src={isOrigin ? originFlag : destinationFlag}
        className={Styles.portFlag}
      />
      <div className={Styles.detail}>
        <TextM textStyle="bold" className={Styles.portName}>
          {name}
        </TextM>
        <TextS>{city}</TextS>
      </div>
    </div>
  );
};

const PortDetail = (props) => {
  const { detail, t } = props;
  let content = (
    <div
      className={Styles.portDetail}
      style={{ height: props.hidePrice && "160px" }}
    >
      <div className={Styles.dates}>
        <TextM textStyle="bold" className={Styles.date}>
          {moment(detail.departDate).format("DD MMM, YYYY")}
        </TextM>
        <TextM className={Styles.date}>
          {detail.duration + ` ${t("vSchedules.day")}`}
        </TextM>
        <TextM textStyle="bold" className={Styles.date}>
          {moment(detail.arriveDate).format("DD MMM, YYYY")}
        </TextM>
      </div>
      <div className={Styles.lineWrapper}>
        <div className={Styles.bulletFull}>
          <i className={`icon-arrow-right ${Styles.arrowWhite}`}></i>
        </div>
        <div className={Styles.line}></div>
        <div className={`${Styles.bulletEmpty} ${Styles.grey}`}></div>
        <div className={Styles.line}></div>
        <div className={Styles.bulletEmpty}></div>
      </div>
      <div className={Styles.ports}>
        <PortDetailItem
          isOrigin
          originFlag={detail.originCountryFlag}
          name={detail.pointFromNameTerminal.substring(0, 22)}
          city={detail.pointFromName + ", "}
        />
        {/* <TextM className={Styles.date}>Langsung</TextM> */}
        <PortDetailItem
          destinationFlag={detail.destinationCountryFlag}
          name={detail.pointToNameTerminal.substring(0, 22)}
          city={detail.pointToName + ", "}
        />
      </div>
    </div>
  );
  return <React.Fragment>{content}</React.Fragment>;
};

const CardDetail = (props) => {
  const { t } = props;
  const costs = [
    "Terminal Handling Cost",
    "Seal (Segel)",
    "Biaya Bill of Lading",
    "Biaya lain dari Pelayaran",
  ];

  const containers = [
    { name: "20' General Purpose", price: "Rp 150.000", qty: "x3" },
    { name: "40' General Purpose", price: "Rp 400.000", qty: "x3" },
    { name: "40' High Cube", price: "Rp 500.000", qty: "x3" },
  ];
  return (
    <div className={Styles.cardDetail}>
      <div className={Styles.left}>
        <PortDetail hidePrice={props.hidePrice} detail={props.detail} t={t} />
      </div>
      <div className={Styles.right}>
        {/* <div className={Styles.costs}>
					<TextS color={Colors.text.darkgrey2} className={Styles.costTitle}>
						Biaya Sudah Termasuk
					</TextS>
					{costs.map(cost => (
						<TextS className={Styles.costLabel}>{cost}</TextS>
					))}
				</div>
				<div className={Styles.costs}>
					<TextS color={Colors.text.darkgrey2} className={Styles.costTitle}>
						Kontainer
					</TextS>
					{containers.map(container => (
						<ContainerDetail {...container} />
					))}
				</div> */}
        <div>
          <div className="flex-row-style">
            <TextM className={Styles.date} style={{ width: "100px" }}>
              Vessel Name
            </TextM>
            <TextM textStyle="bold" className={Styles.date}>
              {props.detail.vesselName ?
                " : " + props.detail.vesselName :
                " : - "
              }
            </TextM>
          </div>
          <div className="flex-row-style">
            <TextM className={Styles.date} style={{ width: "100px" }}>
              Voyage No
            </TextM>
            <TextM textStyle="bold" className={Styles.date}>
              {props.detail.voyageReference ?
                " : " + props.detail.voyageReference :
                " : - "
              }
            </TextM>
          </div>
        </div>
      </div>
    </div>
  );
};

const ResultCard = (props) => {
  const { t } = props;
  const { schedule } = props;
  let history = useHistory();
  const [showDetail, setShowDetail] = useState(false);
  return (
    <div className={Styles.container}>
      <div className={Styles.mainCard}>
        <div className={Styles.left}>
          <img
            src={schedule.shippingIcon}
            style={{ width: "80px", height: "80px", margin: "auto" }}
            alt="not found"
          />
          <ButtonM
            color={Colors.text.mainblue}
            style={{
              width: "auto",
              backgroundColor: Colors.base.transparent,
              fontSize: "14px",
            }}
            onClick={() => setShowDetail(!showDetail)}
          >
            {t("vSchedules.freightDetail")}
          </ButtonM>
        </div>
        <div className={Styles.middle}>
          <div className={Styles.port}>
            <CardItem
              label={t("vSchedules.estimatedDeparture")}
              value={moment(schedule.departDate).format("DD MMM, YYYY")}
            />
            <div className={Styles.portName}>
              <img
                src={schedule.originCountryFlag}
                className={Styles.portFlag}
              />
              <TextS
                style={{ textOverflow: "ellipsis", overflow: "hidden" }}
                color={Colors.black}
              >
                {schedule.originTerminalName.substring(0, 9) +
                  ", " +
                  schedule.originName.substring(0, 9)}
              </TextS>
            </div>
          </div>
          <Duration hidePrice={props.hidePrice} schedule={schedule} t={t} />
          <div className={Styles.port}>
            <CardItem
              label={t("vSchedules.estimatedArrival")}
              value={moment(schedule.arriveDate).format("DD MMM, YYYY")}
            />
            <div className={Styles.portName}>
              <img
                src={schedule.destinationCountryFlag}
                className={Styles.portFlag}
              />
              <TextS
                style={{ textOverflow: "ellipsis", overflow: "hidden" }}
                color={Colors.black}
              >
                {schedule.destinationTerminalName.substring(0, 12) +
                  ", " +
                  schedule.destinationName.substring(0, 12)}
              </TextS>
            </div>
          </div>
        </div>
        {!props.hidePrice && (
          <div className={Styles.right}>
            <CardItem label="Total Harga" value="IDR 89.445.000" />
            <ButtonM
              color={Colors.text.white}
              className={Styles.buttonPick}
              onClick={() => history.push("/bookingFreight")}
            >
              Pilih
            </ButtonM>
          </div>
        )}
      </div>
      {showDetail ? (
        <div>
          {schedule.routingDetails ?
            schedule.routingDetails?.map((detail, index) => (
              <CardDetail
                key={index}
                hidePrice={props.hidePrice}
                detail={detail}
                t={t}
              />
            )) :
            <CardDetail
              // key={index}
              hidePrice={props.hidePrice}
              detail={{
                ...schedule,
                pointFromNameTerminal: schedule.originTerminalName,
                pointFromName: schedule.originName,
                pointToNameTerminal: schedule.destinationTerminalName,
                pointToName: schedule.destinationName,
              }}
              t={t}
            />
          }
        </div>
      ) : null}
    </div>
  );
};

export default ResultCard;
