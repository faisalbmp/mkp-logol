import React from 'react';
import Styles from 'assets/scss/button/button.module.scss';
import {Button} from 'components/generics';
import PropTypes from 'prop-types';

const ButtonM = (props) => {
  const {
    children,
    textStyle,
    color,
    style,
    className,
    onClick,
  } = props 
  return (
    <Button className={`${Styles.medium} ${className}`} textStyle={textStyle} color={color} style={style} onClick={onClick}>{children}</Button>
  );
};

export default ButtonM;

ButtonM.defaultProps = {
  textStyle: "normal",
  color: "black",
  style: {},
  className: '',
  onClick: () => {},
}

ButtonM.propTypes = {
  textStyle: PropTypes.string,
  color: PropTypes.string,
  style: PropTypes.objectOf(String),
  className: PropTypes.string,
  onClick: PropTypes.func,
}