import React, { useState } from 'react'
import { DropDownArrow, DropDownUpArrow, SelectSearchIcon } from "components/icon/iconSvg"
import CheckBox from 'components/input/checkBox';

export default function RadioButton(props) {
    return (
        <label className="radio-button-container" style={props.containerStyle} >
            {/* <input type="radio" value="Ekspor" className = "radio-button-input"  /> */}
            <CheckBox
              name={props.name}
              checked={props.checked}
              onClick={props.onChange}
              onChange={props.onChange}
            />
            <div style={{ marginLeft: 8 }}>  {props.title}</div>
        </label>
    )
}