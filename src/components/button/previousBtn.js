import React from 'react'
import PropTypes from 'prop-types';

import { RightArrowIcon } from 'components/icon/iconSvg'
import "assets/Index.scss"

const PreviousBtn = ({ title, onClick, style }) => {
    return (
        <button
            className="btn-previous"
            style={style}
            onClick={onClick}
        >
            <RightArrowIcon style={{ marginRight: 20, transform: "rotate(180deg)" }} color="#868A92" />
            <div style={{ color: "#868A92" }}>{title}</div>
        </button>
    )
}

PreviousBtn.defaultProps = {
    onClick: PropTypes.function,
    title: PropTypes.string,
    style: PropTypes.object
}

PreviousBtn.defaultProps = {
    onClick: () => { },
    title: "",
    style: {},
}

export default PreviousBtn