import React from 'react';
import Styles from 'assets/scss/dashboard/freightorder.module.scss'

const FreightButtonOutline = ({ title, disalbed = false, additionStyles }) => {
  return (
    <button
      disabled={disalbed}
      className={disalbed ? Styles.buttonOutlineDisabled : Styles.buttonOutline}
      style={additionStyles}>
      {title}
    </button>
  );
};

export default FreightButtonOutline;
