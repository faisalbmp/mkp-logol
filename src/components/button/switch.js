import React from 'react'
import { switchWp, round, slider, input } from 'assets/scss/button/switch.module.scss'

export default function Switch({ onChange, switchCheck }) {
    return (
        <label className={switchWp}>
            <input className={input} type="checkbox" checked={switchCheck} onChange={() => onChange(!switchCheck)} />
            <span className={`${slider} ${round}`}></span>
        </label >
    )
}