import React, { Fragment } from 'react'
import PropTypes from 'prop-types';

import { RightArrowIcon } from 'components/icon/iconSvg'
import Loading from 'components/loading/loadingButtonRight'

const NextBtn = ({ title, onClick, style, loading }) => {
    return (
        <button
            className="search-button"
            style={style}
            onClick={onClick}
        >
            {
                loading ? <Loading /> : <Fragment>
                    {title}
                    <RightArrowIcon style={{ marginLeft: 20 }} />
                </Fragment>
            }
        </button>
    )
}

NextBtn.defaultProps = {
    onClick: PropTypes.function,
    title: PropTypes.string,
    style: PropTypes.object
}

NextBtn.defaultProps = {
    onClick: () => { },
    title: "",
    style: {},
}

export default NextBtn