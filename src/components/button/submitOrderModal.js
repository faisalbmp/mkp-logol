import React, { useState } from 'react'
import { CheckMark, SubmitLogo } from "components/icon/iconSvg"

export default function SubmitOrderModal (props)  {
    return (
        <div className = "submit-order-modal" onClick = {props.onPress}>
           <div className = "submit-order-modal-content">
                <div className = "check-mark-box">
                    <CheckMark/>
                </div>
                <div className = "submit-title">
                    Pemesanan Selesai
                </div>
                <div className = "submit-content">
                    Nomor Pesanan Anda {props.value}
                </div>
                <div lassName = "submit-content" style={{marginTop:50}}>
                    Terima kasih telah menggunakan Logol!
                </div>
                <div>
                    <SubmitLogo/>
                </div>
           </div>
        </div>
    )
}