import React, { useState, useRef, useEffect } from 'react'
import { titleDropDown, selectWrapper, selectButton, selectListContainer, showSelect, wrIcon, routerAnm, activeClick, showError, errorText, errorTextShow, wrLabel } from 'assets/scss/button/wrDropDown.module.scss'
import { DropDownUpArrow, LocationA, LocationB, LocationC } from "components/icon/iconSvg"
import PropTypes from 'prop-types';

function WrDropDown(props) {
    const [hoverActiveMenu, setHoverActiveMenu] = useState(false)
    const wrapperRef = useRef(null);
    const { error, width, height, textError, label } = props

    // useEffect(() => {
    //     function handleClickOutside(event) {
    //         if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
    //             props.onClose()
    //         }
    //     }
    //     document.addEventListener("mousedown", handleClickOutside);
    //     return () => {
    //         document.removeEventListener("mousedown", handleClickOutside);
    //     };
    // }, [wrapperRef]);
    return (
        <div
            className={`${selectWrapper} ${props.show ? activeClick : ""} ${error ? showError : ""}`}
            style={width, height}
            ref={wrapperRef}
        >
            {
                label && (
                    <span className={wrLabel}>{label}</span>
                )
            }
            <div
                className={`${selectButton}  ${props.show ? activeClick : ""} ${error ? showError : ""}`}
                onClick={props.onClick}
                style={props.style}
                onMouseLeave={() => setHoverActiveMenu(false)}
                onMouseMove={() => setHoverActiveMenu(true)}
            >
                <div style={{display:'flex', alignItems:'center', justifyContent:'center'}}>
                    {
                        props.destination1 ?
                        <LocationA
                            color={props.show || hoverActiveMenu ? "#004dff" : error ? "red" : ""}
                        /> : null
                    }
                    {
                        props.destination2 &&
                        <LocationB
                            color={props.show || hoverActiveMenu ? "#004dff" : error ? "red" : ""}
                        />
                    }
                    {
                        props.destination3 &&
                        <LocationC
                            color={props.show || hoverActiveMenu ? "#004dff" : error ? "red" : ""}
                        />
                    }
                </div>
                <div className={titleDropDown}>{props.title || ""}</div>
                <div className={`${wrIcon} ${props.show ? routerAnm : ""}`}>
                    <DropDownUpArrow
                        color={props.show || hoverActiveMenu ? "#004dff" : error ? "red" : ""}
                    />
                </div>
            </div>
            <div className={`${errorText} ${error && !props.show ? errorTextShow : ""}`}>{textError || "please select"}</div>
            <div
                className={`${props.show ? showSelect : ""}`}
                style={props.selectBoxListStyle}
            >
                {props.children}
            </div>
        </div>
    )
}

WrDropDown.propTypes = {
    children: PropTypes.object.isRequired,
    onClose: PropTypes.func.isRequired,
    onClick: PropTypes.func.isRequired,
    show: PropTypes.bool.isRequired,
    error: PropTypes.bool,
    width: PropTypes.number,
    height: PropTypes.number,
    textError: PropTypes.string,
    label: PropTypes.string,
}

export default WrDropDown;