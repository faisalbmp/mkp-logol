import React from 'react';

const Button = props => {
	return (
		<button className={`bt-no-border ${props.className}`} style={props.style} onClick={props.clicked}>
			{props.children ? props.children : props.label}
		</button>
	);
};

export default Button;
