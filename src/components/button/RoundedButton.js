import React, { useState } from 'react'
import { FacebookIcon, LinkedInIcon, ShareIcon, TwitterIcon } from 'components/icon/iconSvg';

const RoundedButton = ({ title, onClick, className }) => {
  const [isShown, setIsShown] = useState(false)
  return (
    <div className={`${className}`} onClick={onClick} style={{
      width: '40px',
      height: '40px',
      background: '#FFFFFF',
      boxShadow: '0px 8px 16px rgba(0, 0, 0, 0.05)',
      borderRadius: '20px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      cursor: 'pointer'
    }}
      onMouseEnter={() => setIsShown(true)}
      onMouseLeave={() => setIsShown(false)}
    >
      {title === 'share' && <ShareIcon color={isShown ? '#004DFF' : '#868A92'} />}
      {title === 'fb' && <FacebookIcon color={isShown ? '#004DFF' : '#868A92'} />}
      {title === 'tw' && <TwitterIcon color={isShown ? '#004DFF' : '#868A92'} />}
      {title === 'li' && <LinkedInIcon color={isShown ? '#004DFF' : '#868A92'} />}
    </div>
  )
}

export default RoundedButton;