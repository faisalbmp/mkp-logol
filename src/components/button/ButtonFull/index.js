import React from "react";
import Styles from "assets/scss/button/button.module.scss";
import { Button } from "components/generics";
import PropTypes from "prop-types";

const ButtonFull = (props) => {
  const { children, textStyle, color, style, className, onClick } = props;
  return (
    <Button
      className={`${Styles.full} ${className}`}
      textStyle={textStyle}
      color={color}
      style={style}
      onClick={onClick}
    >
      {children}
    </Button>
  );
};

export default ButtonFull;

ButtonFull.defaultProps = {
  textStyle: "normal",
  color: "black",
  style: {},
  className: "",
  onClick: () => {},
};

ButtonFull.propTypes = {
  textStyle: PropTypes.string,
  color: PropTypes.string,
  style: PropTypes.objectOf(String),
  className: PropTypes.string,
  onClick: PropTypes.func,
};
