import React from 'react'

import { Arrowx5Fleft1 } from '../icon/iconSvg'

const LearnMore = ({ title, onClick, style }) => {
    return (
        <div className={`learn-more ${style}`} onClick={onClick}>
            <div> {title || ""}</div>
            <div className="animation-arrow-left">
                <Arrowx5Fleft1 />
            </div>
        </div>
    )
}

export default LearnMore;