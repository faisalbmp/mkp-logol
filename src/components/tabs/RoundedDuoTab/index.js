import React from "react";
import Styles from "assets/scss/tabs/roundedduo.module.scss";
import { TextM } from "components/text";
import { Colors } from "configs";

const Tab = ({ isActive, label, onSelect }) => {
  return (
    <div
      className={`${Styles.tab} ${isActive ? Styles.active : ""}`}
      onClick={onSelect}
    >
      <TextM
        color={isActive ? Colors.text.white : Colors.text.darkgrey}
        textStyle="bold"
      >
        {label}
      </TextM>
    </div>
  );
};
const RoundedDuoTab = ({ data, activeTab, onSelect }) => {
  return (
    <div className={Styles.container}>
      {data.map((props, index) => (
        <Tab
          isActive={index === activeTab}
          label={props}
          onSelect={() => onSelect(index)}
        />
      ))}
    </div>
  );
};

export default RoundedDuoTab;
