import React, { Fragment } from 'react'

export default function Select({ number, setSelect, defaultValue }) {
    return (
        <div className="select">
            <div className="text left">Tampilkan</div>
            <select
                defaultValue={defaultValue}
                onChange={e => setSelect(e.target.value)}
                className="select-dashboard"
            >
                {
                    Array(number - 0 + 1).fill().map((_, idx) => 0 + idx).map(value => (
                        <option key={value} value={value}>{value}</option>
                    ))
                }
            </select>

            <div className="text right">hasil per halaman</div>
        </div>
    )
}
