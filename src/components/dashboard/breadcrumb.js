import React from 'react'
import { useLocation } from "react-router-dom";

import { ArrowRightBreadcrumb } from '../icon/iconSvg'

export default function Breadcrumb({ title, onSecondNamePressed }) {
    let { pathname } = useLocation();
    var name = pathname.split("/");

    return (
        <div className="breadcrumb">
            <div className="title">{title}</div>
            <div className="wrapper-text">
                <div className="text">{name[1]}</div>
                <ArrowRightBreadcrumb />
                <div className="text" onClick = {onSecondNamePressed}>{name[2]}</div>
            </div>
        </div>
    )
}