import React from 'react';

import TrackInput from '../input/trackInput';
import {
	CalendarMiddle,
	SearchIcon,
	MessageIcon,
	NotificationIcon,
	PersonalIcon,
} from '../icon/iconSvg';
import PropTypes from 'prop-types';
import indonesia from 'assets/images/svg/indonesia.svg';
import inggris from 'assets/images/svg/united-states.svg';
import i18n from 'i18next';
import { useLocation, useHistory } from 'react-router-dom';

export default function Header({ showLeftPane = true, showTimeOnly = false }) {

	const
		{ pathname } = useLocation(),
		selectedLang = i18n.language,
		publicUrl = process.env.PUBLIC_URL,
		history = useHistory();

	const keyChanges = {
		en: {
			code: 'id',
			label: 'EN',
			icon: inggris,
		},
		id: {
			code: 'en',
			label: 'ID',
			icon: indonesia,
		},
	};

	const onChangeLang = () => {
		history.push(
			`${publicUrl + pathname}?lng=${keyChanges[selectedLang].code}`,
		);
		window.location.reload(false);
	};

	return (
		<header className='header-dashboard'>
			{
				showLeftPane ? (
					<div className='left'>
						{
							!showTimeOnly && (
								<TrackInput />
							)
						}
						<div className='track-date-header'>
							<div><CalendarMiddle /></div>
							<div className='text-calendar'>Rabu, 12 Januari 2020</div>
						</div>
					</div>
				) : (
						<div className="left" />
					)
			}

			<div className='right'>
				<div className='wrepper-icon'>
					<SearchIcon />
					<MessageIcon />
					<NotificationIcon />
					<div className="drop-down-column">
						<div
							// onClick={onChangeLang}
							className='drop-down-lang'>
							<img
								alt="country"
								src={keyChanges[selectedLang].icon} />

							<text className="countryText">{keyChanges[selectedLang].label}</text>
						</div>
						<div
							onClick={onChangeLang}
							className='item-drop-down-lang'>
							<img
								alt="country"
								src={keyChanges[selectedLang === 'en' ? 'id' : 'en'].icon} />

							<text className="countryText">{keyChanges[selectedLang === 'en' ? 'id' : 'en'].label}</text>
						</div>
					</div>
				</div>
				<div className='rounded-personal'>
					<PersonalIcon />
				</div>
			</div>
		</header>
	);
}

Header.propTypes = {
	showLeftPane: PropTypes.bool.isRequired,
};
