import React from 'react';
import Styles from 'assets/scss/dashboard/tagihanSaya/tagihanSaya.module.scss';
import Logo from 'assets/images/svg/logo-logol.svg';
import ProgressBar from '../progressBar/progressBar';

const Header = props => {
	return (
		<header className={Styles.header}>
			<div className={Styles.logo}>
				<img src={Logo} alt='Not Found' />
			</div>
			<ProgressBar steps={props.steps} />
		</header>
	);
};

export default Header;
