import React from 'react';
import { SearchInputIcon } from '../../../icon/iconSvg';
import { withTranslation } from 'react-i18next';

const SearchInput = props => {
	const { t } = props
	return (
		<div className='search' style={{ maxWidth: 438 }}>
			<input
				style={{
					background: 'none',
					fontStyle: 'italic',
					fontFamily: 'Nunito Sans',
					fontSize: 16,
				}}
				value={props.value}
				onChange={props.change}
				placeholder={t('myBill.searchPlaceholder')}
			/>
			<SearchInputIcon />
		</div>
	);
};

export default withTranslation()(SearchInput);
