import React from 'react';
import SteppedProgressBar from '../../../SteppedProgressBar/index';

const ProgressBar = props => {
	return (
		<div style={{ width: 460, paddingBottom: 22 }}>
			<SteppedProgressBar steps={props.steps} />
		</div>
	);
};

export default ProgressBar;
