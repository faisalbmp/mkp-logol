import React from 'react';
import Styles from 'assets/scss/dashboard/tagihanSaya/tagihanSaya.module.scss';
import Classes from './payment.module.scss';
import Detail from '../detail';
import bcaLogo from '../svg/bcaLogo.svg';
import { ArrowDown } from '../../../icon/iconSvg';

const Payment = props => {
	const { translate } = props;

	// const rekeningDetail = [
	// 	{
	// 		label: 'Pemilik Rekening :',
	// 		value: 'PT.Logol Group Indonesia',
	// 		action: '',
	// 	},
	// 	{ label: 'Nomor Rekening :', value: '9918200192', action: 'Salin' },
	// 	{ label: 'Nominal Transfer', value: 'IDR 25.500.123', action: 'Salin' },
	// ];
	return (
		<div style={{ paddingLeft: 200, marginTop: 48, paddingRight: 200 }}>
			<h3 className={Styles.title}>Transfer Ke Bank BCA</h3>
			<div className={Styles.flexContent}>
				<div style={{ width: 648, marginRight: 24 }}>
					<div
						className={Styles.metode}
						style={{
							width: 648,
							height: 413,
							paddingLeft: 40,
							paddingRight: 32,
						}}
					>
						<h4>Silahkan Transfer Ke</h4>
						<div
							style={{
								height: 60,
								background: '#F7F7F7',
								marginTop: 24,
								marginLeft: -40,
								marginRight: -32,
								paddingTop: 10,
							}}
						>
							<div
								className={Styles.flexContent}
								style={{ fontFamily: 'Nunito Sans' }}
							>
								<h4
									className={Styles.col6}
									style={{
										textAlign: 'left',
										fontWeight: 'bold',
										marginLeft: 40,
										marginTop: 10,
									}}
								>
									BCA
								</h4>
								<div
									className={Styles.col6}
									style={{
										textAlign: 'right',
										fontWeight: 'bold',
										marginRight: 24,
									}}
								>
									<img src={bcaLogo} alt='Not Found' />
								</div>
							</div>
						</div>
						<div style={{ marginTop: 24, fontFamily: 'Nunito Sans' }}>
							<div className={Styles.flexContent}>
								<p className={Styles.col4}>
									{translate('myBill.accountOwner')}
								</p>
								<p className={Styles.col4} style={{ fontWeight: 'bold' }}>
									PT.Logol Group Indonesia
								</p>
							</div>
							<div className={Styles.flexContent}>
								<p className={Styles.col4}>
									{translate('myBill.accountNumber')}
								</p>
								<p className={Styles.col4} style={{ fontWeight: 'bold' }}>
									9918200192
								</p>
								<div
									className={Styles.col4}
									style={{ textAlign: 'right', paddingRight: 23 }}
								>
									<p>{translate('myBill.copy')}</p>
								</div>
							</div>
							<div className={Styles.flexContent}>
								<p className={Styles.col4}>
									{translate('myBill.transferNominal')}
								</p>
								<div
									className={`${Styles.col4} ${Styles.flexContent}`}
									style={{ fontWeight: 'bold' }}
								>
									<p>IDR 25.500.</p>
									<p className={Classes.tooltip}>
										123
										<span className={Classes.tooltiptext}>
											{translate('myBill.tooltipTips')}
										</span>
									</p>
								</div>
								<div
									className={Styles.col4}
									style={{ textAlign: 'right', paddingRight: 23 }}
								>
									<p>{translate('myBill.copy')}</p>
								</div>
							</div>
						</div>
					</div>
					<div className={Styles.caraPembayaran} style={{ marginTop: 24 }}>
						<div className={Styles.flexContent}>
							<h3 className={Styles.col6}>{translate('myBill.stepPayment')}</h3>
							<div className={Styles.col6} style={{ textAlign: 'right' }}>
								<ArrowDown />
							</div>
						</div>
					</div>
				</div>
				<div style={{ width: 264 }}>
					<Detail translate={translate} />
					<button
						className={Styles.btnBayar}
						onClick={() => props.nextStep('konfirmasi')}
					>
						{translate('myBill.paymentConfirmation')}
					</button>
					<button
						className={Classes.btnGantiMetode}
						onClick={() => props.nextStep('')}
					>
						{translate('myBill.changePaymentMethod')}
					</button>
				</div>
			</div>
		</div>
	);
};

export default Payment;
