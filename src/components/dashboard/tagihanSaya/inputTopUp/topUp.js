import React, { useState } from 'react';
import {
	topUpInput,
	inputNominal,
	btnTopUp,
} from 'assets/scss/dashboard/tagihanSaya/tagihanSaya.module.scss';

const TopUp = () => {
	const [state, setstate] = useState('IDR 500.000');

	return (
		<div>
			<label style={{ marginLeft: 10 }}>Nominal</label>
			<div className={topUpInput}>
				<input
					className={inputNominal}
					value={state}
					onChange={event => setstate(event.target.value)}
				/>
				<button className={btnTopUp}>Top Up</button>
			</div>
		</div>
	);
};

export default TopUp;
