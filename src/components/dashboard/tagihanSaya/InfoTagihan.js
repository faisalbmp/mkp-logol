import React, { useState } from 'react';
import Styles from 'assets/scss/dashboard/tagihanSaya/tagihanSaya.module.scss';
import item from './svg/item.svg';
import uderLine from './svg/underline.svg';

const InfoTagihan = props => {

	const { translate } = props;

	const [active, setActive] = useState('menunggu');

	return (
		<div className={Styles.pembayaranContainer}>
			<div className={Styles.flexItem}>
				<div className={Styles.navItem} onClick={() => setActive('menunggu')}>
					<div style={{ marginBottom: 12 }}>{translate('myBill.waitingPayment')}</div>
					{active === 'menunggu' ? (
						<img src={uderLine} alt='Not Found' />
					) : null}
				</div>
				<div className={Styles.navItem} onClick={() => setActive('riwayat')}>
					<div style={{ marginBottom: 22 }}>{translate('myBill.historyPayment')}</div>
					{active === 'riwayat' ? <img src={uderLine} alt='Not Found' /> : null}
				</div>
			</div>
			<div className={Styles.itemWrapper}>
				<div
					className={Styles.item1}
					style={{ borderBottom: '1px solid #e0e5e8' }}
				>
					<div className={Styles.flexItem} style={{ paddingTop: 20 }}>
						<div className={Styles.item} style={{ textAlign: 'center' }}>
							<img src={item} alt='not founds' />
						</div>
						<div className={Styles.item}>
							<ul>
								<li>{translate('myBill.noBill')}</li>
								<li style={{ listStyle: 'none', fontWeight: 'bold' }}>
									INV-0004112513
								</li>
							</ul>
						</div>
						<div className={Styles.item}>
							<ul>
								<li>{translate('myBill.dueDate')}</li>
								<li style={{ listStyle: 'none', fontWeight: 'bold' }}>
									14 Hari Lagi
								</li>
							</ul>
						</div>
						<div className={Styles.item}>
							<button onClick={props.click}>{translate('myBill.payNow')}</button>
						</div>
					</div>
				</div>
				<div className={Styles.item1}>
					<div className={Styles.flexItem} style={{ padding: 20 }}>
						<div className={Styles.item}>
							<ul>
								<li>{translate('myBill.shipFee')}</li>
								<li style={{ listStyle: 'none', fontWeight: 'bold' }}>
									IDR 10.000.000
								</li>
							</ul>
						</div>
						<div className={Styles.item}>
							<ul>
								<li>{translate('myBill.truckCost')}</li>
								<li style={{ listStyle: 'none', fontWeight: 'bold' }}>
									IDR 10.000.000
								</li>
							</ul>
						</div>
						<div className={Styles.item}>
							<ul>
								<li>{translate('myBill.additionalCost')}</li>
								<li style={{ listStyle: 'none', fontWeight: 'bold' }}>
									IDR 500.000
								</li>
							</ul>
						</div>
						<div className={Styles.item}>
							<ul>
								<li>{translate('myBill.totalBill')}</li>
								<li style={{ listStyle: 'none', fontWeight: 'bold' }}>
									IDR 30.500.000
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default InfoTagihan;
