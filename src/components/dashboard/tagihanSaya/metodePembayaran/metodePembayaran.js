import React, { useState } from 'react';
import Styles from 'assets/scss/dashboard/tagihanSaya/tagihanSaya.module.scss';
import Classes from './metodePembayaran.module.scss';
import BankTfIcon from '../svg/transfer.svg';
import DompetIcon from '../svg/dompet.svg';
import PayLaterIcon from '../svg/payLaterIcon.svg';
import RincianTagihan from './rincian';
import Detail from '../detail';

const MetodePembayaran = props => {
	const { translate } = props;

	const [active, setActive] = useState('');
	// const [checked, setChecked] = useState(false);

	return (
		<div style={{ marginTop: 48, paddingLeft: '110px', paddingRight: '110px' }}>
			<h3 className={Styles.title}>{translate('myBill.paymentMethod')}</h3>
			<div className={Styles.flexContent}>
				<div className={Styles.metode}>
					<h3>{translate('myBill.lastUsed')}</h3>
					<div className={Styles.btnTransfer}>
						<img src={BankTfIcon} alt='not Found' style={{ marginRight: 19 }} />
						<h5>{translate('myBill.transferBank')}</h5>
					</div>
					<h3 style={{ marginTop: 48 }}>Metode Lain</h3>
					<div className={Styles.btnMetodeBayar}>
						<img src={DompetIcon} alt='not Found' style={{ marginRight: 19 }} />
						<h5>{translate('myBill.walletBalance')}</h5>
					</div>
					<div className={Styles.btnMetodeBayar}>
						<img
							src={PayLaterIcon}
							alt='not Found'
							style={{ marginRight: 19 }}
						/>
						<h5>{translate('myBill.payLater')}</h5>
					</div>
				</div>
				<div className={Styles.rincian}>
					<h4>{translate('myBill.transferBank')}</h4>
					<div
						className={`${Classes.radioBox} ${
							active === 'tfBca' ? Classes.radioActive : null
						}`}
					>
						<label className={`${Classes.container}`}>
							Transfer Bank BCA
							<input
								type='radio'
								name='radio'
								onChange={() => setActive('tfBca')}
							/>
							<span className={Classes.checkmark}></span>
						</label>
					</div>
					<div
						className={`${Classes.radioBox} ${
							active === 'tfMandiri' ? Classes.radioActive : null
						}`}
					>
						<label className={Classes.container}>
							Transfer Bank Mandiri
							<input
								type='radio'
								name='radio'
								onChange={() => setActive('tfMandiri')}
							/>
							<span className={Classes.checkmark}></span>
						</label>
					</div>
					<h4 style={{ marginTop: 32 }}>{translate('myBill.promoCode')}</h4>
					<div className={Styles.flexContent} style={{ marginBottom: 32 }}>
						<input
							className={Styles.inputKodePromo}
							placeholder={translate('myBill.promoCodePlaceholder')}
						/>
						<button className={Styles.btnGunakan}>
							{translate('myBill.use')}
						</button>
					</div>
					<RincianTagihan translate={translate} />
				</div>
				<div style={{ width: 264 }}>
					<Detail translate={translate} />
					<button
						className={Styles.btnBayar}
						onClick={() => props.nextStep('tf')}
					>
						{translate('myBill.payNow')}
					</button>
				</div>
			</div>
		</div>
	);
};

export default MetodePembayaran;
