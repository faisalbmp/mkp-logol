import React from 'react';
import Styles from 'assets/scss/dashboard/tagihanSaya/tagihanSaya.module.scss';

const rincianTagihan = ({ translate }) => {
    const rincian = [
        { title: 'Ocean Freight', price: 'IDR 20.000.000' },
        { title: 'Truck', price: 'IDR 5.000.000' },
        { title: 'Pengurusan Dokumen x 4', price: 'IDR 600.000' },
        { title: 'Biaya Pemesanan', price: 'IDR 50.000' },
    ];

    return (
        <React.Fragment>
            <h4>{translate('myBill.billingDetails')}</h4>
            {rincian.map((item, index) => (
                <div className={Styles.flexContent} key={index}>
                    <div className={Styles.col6} style={{ textAlign: 'left' }}>{item.title}</div>
                    <div className={Styles.col6} style={{ textAlign: 'right', fontWeight: 'bold' }}>{item.price}</div>
                </div>
            ))}
            <div style={{ height: 41, background: '#F7F7F7', marginTop: 24, marginLeft: -24, marginRight: -24, paddingTop: 10 }}>
                <div className={Styles.flexContent}>
                    <div className={Styles.col6} style={{ textAlign: 'left', fontWeight: 'bold', marginLeft: 24 }}>
                        {translate('myBill.totalBill')}
                    </div>
                    <div className={Styles.col6} style={{ textAlign: 'right', fontWeight: 'bold', marginRight: 24 }}>IDR 25.650.000</div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default rincianTagihan;