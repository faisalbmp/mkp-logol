import React from 'react';
import Styles from 'assets/scss/dashboard/tagihanSaya/tagihanSaya.module.scss';

const detail = ({ translate }) => {
	return (
		// <div style={{ width: 264 }}>
		<div className={Styles.detail}>
			<div className={Styles.detailHeader}>{translate('myBill.payBefore', { time: '14.00 PM' })}</div>
			<div style={{ padding: 24 }}>
				<p style={{ color: '#868A92', fontSize: 12 }}>{translate('myBill.noBill')}</p>
				<p style={{ fontSize: 16, color: '#333333', fontWeight: 'bold' }}>
					INV-0004112513
				</p>
				<p style={{ color: '#868A92', fontSize: 12 }}>{translate('myBill.noOrder')}</p>
				<p style={{ fontWeight: 'bold', fontSize: 14 }}>0004112513</p>
			</div>
		</div>
		// </div>
	);
};

export default detail;
