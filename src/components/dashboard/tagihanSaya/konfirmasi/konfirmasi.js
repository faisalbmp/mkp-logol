import React from 'react';
import Classes from './konfirmasi.module.scss';
import Styles from 'assets/scss/dashboard/tagihanSaya/tagihanSaya.module.scss';
import InputText from '../../../input/inputText';
import InputDate from '../../../input/inputDate';
import DateIcon from '../svg/dateIcon.svg';
import UploadIcon from '../svg/uploadIcon.svg';
import Detail from '../detail';
import UploadInput from '../../../input/uploadNpwp';

const Konfirmasi = ({ translate }) => {
	return (
		<div style={{ paddingLeft: 200, marginTop: 48, paddingRight: 200 }}>
			<h3 className={Styles.title} style={{ fontWeight: 800 }}>
				{translate('myBill.paymentConfirmation')}
			</h3>
			<div className={Styles.flexContent}>
				<div className={Classes.buktiForm}>
					<h4 style={{ fontSize: 22 }}>{translate('myBill.sendEvidancePayment')}</h4>
					<InputText
						value='Fahmi Hikmawan'
						textInputTitle={translate('myBill.accountOwner')}
						inputTextStyle={{ width: 528, height: 52 }}
					/>
					<InputText
						value='90019909090901'
						textInputTitle={translate('myBill.accountNumber')}
						inputTextStyle={{ width: 528, height: 52 }}
					/>
					<div className={Styles.flexContent} style={{ marginTop: 10 }}>
						<div style={{ marginRight: 27 }}>
							<span style={{ fontWeight: 'bold', fontSize: 12 }}>
								{translate('myBill.transferDate')}
							</span>
							<div className={Classes.date}>
								<div className={Styles.flexContent}>
									<img
										style={{ marginRight: 10 }}
										src={DateIcon}
										alt='Not Found'
									/>
									<InputDate class={Classes.dateInput} />
								</div>
							</div>
						</div>
						<div style={{ marginRight: 27 }}>
							<span style={{ fontWeight: 'bold', fontSize: 12 }}>
								{translate('myBill.receiptTransfer')}
							</span>

							<button className={Classes.btnUpload}>
								<input type='file' />
								<img
									src={UploadIcon}
									alt='Not Found'
									style={{ marginRight: 17.5 }}
								/>
								Upload
								{/* <UploadInput /> */}
							</button>
						</div>
					</div>
				</div>
				<div style={{ marginLeft: 24 }}>
					<Detail
						translate={translate} />
					<button className={Styles.btnBayar}>{translate('myBill.paymentConfirmation')}</button>
				</div>
			</div>
		</div>
	);
};

export default Konfirmasi;
