import React, { useState, Fragment } from 'react'
import { useLocation, useHistory } from "react-router-dom";

import { AddIcon, HomeDashboardIcon, ScriptIcon, ShipIcon, VectorIcon, FileBoardIcon, ScreenIcon, ClockIcon, SettingIcon, TruckMenuDashboard, FreightMenuDashboard, EDocument, TagihanSaya } from '../icon/iconSvg'

import { withTranslation } from 'react-i18next';

const BtnAddIcon = () => (
    // <div className={`btn-add`}>
    //     <AddIcon />
    // </div>
    <AddIcon />
)

// TODO ADD menu on en_EN.json & id_ID.json
const dataMenu = [
    {
        name: "newOrder", Icon: AddIcon, path: "/dashboard/add", childMenu: [
            { name: "freight", Icon: FreightMenuDashboard, path: "/dashboard/add/freight" },
            { name: "truck", Icon: TruckMenuDashboard, path: "/dashboard/add/truck" },
            {
                name: "eDocument", Icon: EDocument, path: "/dashboard/e-document", childMenu: [
                    { name: "eGatePass", Icon: FreightMenuDashboard, path: "/dashboard/e-document/gatePass" },
                    { name: "ePib", Icon: FreightMenuDashboard, path: "/dashboard/add/freight" },
                ]
            },
        ]
    },
    { name: "dashboard", Icon: HomeDashboardIcon, path: "/dashboard" },
    {
        name: "orderList", Icon: ScriptIcon, path: "/dashboard/list/booking", childMenu: [
            { name: "freight", Icon: ShipIcon, path: "/dashboard/list/freight" },
            { name: "truck", Icon: VectorIcon, path: "/dashboard/list/truck" },
            { name: "document", Icon: FileBoardIcon, path: "/dashboard/list/document" },
            {
                name: "eDocument", Icon: EDocument, path: "/dashboard/list/e-document", childMenu: [
                    { name: "eGatePass", Icon: FreightMenuDashboard, path: "/dashboard/list/e-document/gatePass" },
                ]
            },
        ]
    },
    { name: "monitoringSystem", Icon: ScreenIcon, path: "/dashboard/monitor" },
    // { name: "Export", Icon: ScreenIcon, path: "/dashboard/export" },
    { name: "orderHistory", Icon: ClockIcon, path: "/dashboard/history" },
    { name: "myBills", Icon: TagihanSaya, path: "/dashboard/tagihan" },
    { name: "settings", Icon: SettingIcon, path: "/dashboard/setting" },
]

function Menu({ t }) {

    let { pathname } = useLocation();
    let history = useHistory();

    const translate = t;

    const [activeChildMenu, setActiveChildMenu] = useState("")
    const [activeSubChildMenu, setActiveSubChildMenu] = useState("")
    const [activeMenu, setActiveMenu] = useState(false)
    const [hoverActiveMenu, setHoverActiveMenu] = useState(false)
    const [iconActive, setIconActive] = useState(false)

    const funcActiveChild = (param) => (
        // Add Logic if hash Child
        param.path === activeChildMenu || (param.childMenu ? param.childMenu.filter(val => val.childMenu ? val.childMenu.find((child) => child.path === pathname) : val.path === pathname).length : false)
    )

    const funcActiveSubChild = (param) => (
        param.path === activeChildMenu || (param.childMenu ? param.childMenu.filter(val => val.path === pathname).length : false)
    )

    const clickActiveMenu = (param) => (
        param.childMenu && param.path ? setActiveChildMenu(activeChildMenu === param.path ? "" : param.path) : history.push(param.path)
    )

    const isContainesSideMenu = () => {
        if (pathname.includes("e-document")) return "has_side_menu"
        return ""
    }

    return (
        <div className={`wrepper-menu ${isContainesSideMenu()}`}>
            <div className={`menu ${iconActive ? "active-menu" : ""}`}>
                <div className="item-menu item-button" onClick={() => setActiveMenu(!activeMenu)}>
                    <div className="button-menu" onClick={() => setIconActive(!iconActive)}>
                        <div className="icon"></div>
                    </div>
                </div>
                {
                    dataMenu.map((value, index) => (
                        <Fragment key={index}>
                            <div
                                className={`item-menu ${value.path === pathname ? "item-menu-active" : ""} ${value.childMenu ? "item-menu-child" : ""} ${funcActiveChild(value) ? "rotate-icon" : ""}`}
                                key={index}
                                onClick={() => {
                                    if (value.childMenu && value.childMenu.length > 0) {
                                        clickActiveMenu(value);
                                    } else {
                                        setActiveSubChildMenu(value.path)
                                        history.push(value.path)
                                        setIconActive(!iconActive);
                                    }
                                }}
                            // onMouseLeave={() => setHoverActiveMenu(false)}
                            // onMouseMove={() => setHoverActiveMenu(true)}
                            >
                                {<value.Icon color={value.path === pathname ? "#002985" : false} />}
                                <div className="text-name">{translate(`menus.${value.name}`)}</div>
                            </div>
                            {funcActiveChild(value) ?
                                value.childMenu.map((item, i) => (
                                    <div key={i}>
                                        <div
                                            className={`item-menu item-child ${item.path === pathname ? "item-menu-active" : ""} ${funcActiveChild(value) ? "block" : "none"} ${item.childMenu ? "item-menu-child" : ""} ${(funcActiveChild(item) || activeSubChildMenu) ? "rotate-icon" : ""}`}
                                            onClick={() => {
                                                if (item.childMenu && item.childMenu.length > 0) {
                                                    setActiveSubChildMenu(item.path)
                                                } else {
                                                    history.push(item.path)
                                                }
                                            }}
                                            key={i}
                                        // onMouseLeave={() => setHoverActiveMenu(false)}
                                        // onMouseMove={() => setHoverActiveMenu(true)}
                                        >
                                            {<item.Icon color={item.path === pathname ? "#004DFF" : false} />}
                                            <div className="text-name">{translate(`menus.${item.name}`)}</div>
                                        </div>
                                        {
                                            (funcActiveChild(item) || activeSubChildMenu) && item.childMenu && item.childMenu.map((childItem, childIndex) => (
                                                <div
                                                    className={`item-menu item-sub-child ${childItem.path === pathname ? "item-menu-active" : ""} ${funcActiveChild(value) ? "block" : "none"}`}
                                                    onClick={() => history.push(childItem.path)}
                                                    key={childIndex}
                                                // onMouseLeave={() => setHoverActiveMenu(false)}
                                                // onMouseMove={() => setHoverActiveMenu(true)}
                                                >
                                                    {<item.Icon color={childItem.path === pathname ? "#004DFF" : false} />}
                                                    <div className="text-name">{translate(`menus.${childItem.name}`)}</div>
                                                </div>
                                            ))
                                        }
                                    </div>
                                )) : ""
                            }
                        </Fragment>
                    ))
                }
            </div>
        </div>
    )
}

export default withTranslation()(Menu);
