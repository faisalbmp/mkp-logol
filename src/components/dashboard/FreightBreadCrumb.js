import React from "react";
import { TextXXL, TextS } from "components/text";
import Styles from "assets/scss/dashboard/freightbreadcrumb.module.scss";
import {
  ArrowRightBreadcrumb,
  // Clock,
  // TextSum,
  // EditPencil,
  // ContainerSum,
  // TruckPlus,
} from "../icon/iconSvg";
// import { Colors } from "configs";
import SteppedProgressBar from 'components/SteppedProgressBar';

// const IndicatorItem = (props) => {
//   const { icon, label, active } = props;
//   return (
//     <div className={Styles.indicatorItem}>
//       <div className={Styles.itemIcon}>
//         {icon({ fill: active ? Colors.text.mainblue : Colors.text.grey2 })}
//       </div>
//       <TextS
//         textStyle="bold"
//         color={active ? Colors.text.mainblue : Colors.text.grey2}
//         className={Styles.indicatorLabel}
//       >
//         {label}
//       </TextS>
//       <div className={`${Styles.bullet} ${active ? Styles.active : ""}`} />
//     </div>
//   );
// };

const FreightStepIndicator = ({ steps }) => {
  return (
    // <div className={Styles.freightStepIndicator}>
    //   <div className={Styles.icons}>
    //     {/* <IndicatorItem label="Tentukan Jadwal" icon={Clock} active />
    //     <IndicatorItem label="Detail Kontainer" icon={ContainerSum} />
    //     <IndicatorItem label="Lengkapi Data" icon={EditPencil} />
    //     <IndicatorItem label="Layanan Tambahan" icon={TruckPlus} />
    //     <IndicatorItem label="Ringkasan" icon={TextSum} /> */}
    //   </div>
    //   <div className={Styles.line} />
    // </div>
    <SteppedProgressBar steps={steps} />
  );
};

const FreightBreadCrumb = (props) => {
  const { title, steps } = props;

  return (
    <div className={Styles.freightBreadcrumb}>
      <div className={Styles.left}>
        <TextXXL textStyle="bold">{title}</TextXXL>
        <div className={Styles.breadcrumb}>
          <TextS className={Styles.label}>Pesanan Baru</TextS>
          <ArrowRightBreadcrumb />
          <TextS className={Styles.label}>Layanan Kapal</TextS>
        </div>
      </div>
      <FreightStepIndicator steps={steps} />
    </div>
  );
};

export default FreightBreadCrumb;
