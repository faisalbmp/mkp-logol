import React from 'react';
import Styles from './tagihan.module.scss';
import arrowDown from './imgs/arrowDown.svg';
import arrowDown2 from './imgs/arrowDown2.svg';

const Tagihan = props => {
	const { unpaidCN, isLoading, paidCN } = props.vendorReducer;
	return (
		<div className='flex-row-style' style={{ marginTop: 36 }}>
			<div className={Styles.redBox} style={{ marginRight: 24 }}>
				<div style={{ marginBottom: 8, marginRight: 42 }}>
					<p className={Styles.title}>Tagihan Belum Terbayar</p>
					<div className={Styles.amount}>
						{isLoading
							? ''
							: 'Rp ' +
							  new Intl.NumberFormat('jkt-id', {
									maximumSignificantDigits: 3,
							  }).format(unpaidCN)}
					</div>
				</div>
				<div className={Styles.selectMonth}>
					<div className={Styles.label}>Bulan Ini</div>
					<img src={arrowDown} alt='Not Found' />
				</div>
			</div>
			<div className={Styles.whiteBox}>
				<div style={{ marginBottom: 8, marginRight: 42 }}>
					<p className={Styles.title}>Tagihan Terbayar</p>
					<div className={Styles.amount}>
						{isLoading
							? ''
							: 'Rp ' +
							  new Intl.NumberFormat('jkt-id', {
									maximumSignificantDigits: 3,
							  }).format(paidCN)}
					</div>
				</div>
				<div className={Styles.selectMonth2}>
					<div className={Styles.label}>Bulan Ini</div>
					<img src={arrowDown2} alt='Not Found' />
				</div>
			</div>
		</div>
	);
};

export default Tagihan;
