import React, { useMemo, useState, useEffect, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { SortIcon, ArrowDownD, ArrowUpD } from 'components/icon/iconSvg';
import SearchInput from 'components/dashboard/searchInput';
import Styles from 'assets/scss/dashboard/table.module.scss';
import Pagination from 'components/paginations/New/Pagination';
import Button from 'components/button/Button';
import LoadingDot from 'components/loading/loadingDot';

// import { getVendorListCn } from 'actions';

const Table = props => {
	const { listCN, isLoading } = props.vendorReducer;
	const [currentPage, setCurrentPage] = useState(1);
	const [postsPerPage, setPostPerPage] = useState(10);
	const indexOfLastPost = currentPage * postsPerPage;
	const indexOfFirstPost = indexOfLastPost - postsPerPage;
	const [searchValue, setSearchValue] = useState('');

	// const { vendorReducer } = useSelector(state => state);
	// const { listCN, isLoading } = vendorReducer;
	// const dispatch = useDispatch();

	// const onFetchListCN = useCallback(() => dispatch(getVendorListCn()), [
	// 	listCN,
	// ]);

	// useEffect(() => {
	// 	// onFetchListCN();
	// }, []);

	const tableTitle = [
		{ label: 'Tipe', iconSort: true },
		{ label: 'No. Pesanan', iconSort: true, isBold: true },
		{ label: 'No. Penagihan', iconSort: true },
		{ label: 'Tgl. Terbit Tagihan', iconSort: true },
		{ label: 'Total Tagihan', iconSort: true },
		{ label: 'Total Terbayar', iconSort: true },
		{ label: 'Sisa Tagihan', iconSort: true },
		{ label: 'Tgl. Pembayaran', iconSort: true },
		{ label: 'Status', iconSort: true },
	];

	const posts = useMemo(() => {
		let postData = listCN;
		if (!listCN) {
			postData = [];
		}
		if (searchValue) {
			postData = postData.filter(
				item =>
					item.orderManagementID.toLowerCase().includes(searchValue.toLowerCase()) ||
					item.creditNoteNumber.toLowerCase().includes(searchValue.toLowerCase())
			);
		}

		return postData;
	}, [searchValue, listCN]);

	const gotoPage = page => {
		const currentPage = Math.max(0, Math.min(page, posts.length));
		setCurrentPage(currentPage);
	};

	const handleClick = page => {
		// e.preventDefault();
		gotoPage(page);
	};

	const handleMoveLeft = () => {
		// e.preventDefault();
		gotoPage(currentPage - 1);
	};

	const handleMoveRight = () => {
		// e.preventDefault();
		gotoPage(currentPage + 1);
	};

	return (
		<div className='truck'>
			<div className='wrepper-table truck-table'>
				<div className='middle-component'>
					<SearchInput
						onChange={e => {
							setSearchValue(e.target.value);
							setCurrentPage(1);
						}}
						value={searchValue}
					/>
					<div className='right'>
						<div className='sort-item'>
							<SortIcon />
							<div className='text'>Urutkan</div>
						</div>

						{/* <div>
							<Select
								number={100}
								// setSelect={setSelect} 
								defaultValue={10} />
						</div> */}
						<div>
							<div className='select'>
								<div className='text left'>Tampilkan</div>
								<select
									defaultValue={10}
									onChange={e => {
										setPostPerPage(e.target.value);
										setCurrentPage(1);
									}}
									className='select-dashboard'
								>
									<option value={5}>5</option>
									<option value={10}>10</option>
									<option value={25}>25</option>
									<option value={50}>50</option>
									<option value={100}>100</option>
								</select>

								<div className='text right'>hasil per halaman</div>
							</div>
						</div>
					</div>
				</div>
				{isLoading ? (
					<div
						style={{
							marginLeft: 'auto',
							marginRight: 'auto',
							textAlign: 'center',
						}}
					>
						<LoadingDot />
					</div>
				) : (
					<table style={{ width: '100%' }} className={Styles.table}>
						<thead>
							<tr>
								{tableTitle.map((item, index) => (
									<th className={Styles.th} key={index}>
										<div className='flex-row-style'>
											{item.isBold ? (
												<div style={{ color: '#333333' }}>{item.label}</div>
											) : (
												<div>{item.label}</div>
											)}
											{item.iconSort ? (
												<div className={Styles.iconSort}>
													<ArrowUpD active={false} />
													<ArrowDownD active={false} />
												</div>
											) : (
												''
											)}
										</div>
									</th>
								))}
							</tr>
						</thead>

						<tbody>
							{posts.slice(indexOfFirstPost, indexOfLastPost).map((item, index) => (
								<tr>
									<td>
										{/* {item.tipe === 'Expor' ? (
									<Button className={Styles.btExpor} label={item.tipe} />
								) : (
									<Button className={Styles.btImpor} label={item.tipe} />
								)} */}
										{''}
									</td>
									<td>{item.orderManagementID} </td>
									<td style={{ fontWeight: 'bold', color: '#002985' }}>
										{item.creditNoteNumber}
									</td>
									<td>{item.date}</td>
									<td style={{ fontWeight: 'bold', color: '#333333' }}>
										{'Rp ' +
											new Intl.NumberFormat('jkt-id', {
												maximumSignificantDigits: 3,
											}).format(item.amount)}
									</td>
									<td style={{ fontWeight: 'bold', color: '#333333' }}>
										{'Rp ' +
											new Intl.NumberFormat('jkt-id', {
												maximumSignificantDigits: 3,
											}).format(item.amountPaid)}
									</td>
									<td style={{ fontWeight: 'bold', color: '#333333' }}>
										{'Rp ' +
											new Intl.NumberFormat('jkt-id', {
												maximumSignificantDigits: 3,
											}).format(item.amountBalance)}
									</td>
									<td>{'-'}</td>
									<td>
										<Button className={Styles.btUnpaid} label={item.status} />
									</td>
								</tr>
							))}
						</tbody>
					</table>
				)}
				<Pagination
					totalRecords={posts.length}
					pageLimit={postsPerPage}
					pageNeighbours={1}
					currentPage={currentPage}
					handleClick={handleClick}
					handleMoveLeft={handleMoveLeft}
					handleMoveRight={handleMoveRight}
				/>
			</div>
		</div>
	);
};

export default Table;
