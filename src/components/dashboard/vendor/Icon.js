import React from 'react';

export const EditIcon = () => (
	<svg
		width='14'
		height='14'
		viewBox='0 0 14 14'
		fill='none'
		xmlns='http://www.w3.org/2000/svg'
	>
		<path
			d='M14 3.22549L10.7586 0L3.44828 6.79412V10.5686H7.17241L14 3.22549ZM4.82759 9.19608V7.34314L10.7586 1.85294L12 3.22549L6.62069 9.19608H4.82759ZM13.7931 12.6275V14C8 14 6.62069 14 0 14V12.6275C2.82759 12.6275 11.0345 12.6275 13.7931 12.6275Z'
			fill='#333333'
		/>
	</svg>
);

export const ClockIcon = () => (
	<div style={{ paddingRight: 8, display: 'flex', alignItems: 'center' }}>
		<svg
			width='16'
			height='16'
			viewBox='0 0 16 16'
			fill='none'
			xmlns='http://www.w3.org/2000/svg'
		>
			<path
				d='M8.00016 14.6666C11.6821 14.6666 14.6668 11.6818 14.6668 7.99992C14.6668 4.31802 11.6821 1.33325 8.00016 1.33325C4.31826 1.33325 1.3335 4.31802 1.3335 7.99992C1.3335 11.6818 4.31826 14.6666 8.00016 14.6666Z'
				stroke='#E0E5E8'
				stroke-width='1.5'
				stroke-linecap='round'
				stroke-linejoin='round'
			/>
			<path
				d='M8 4V8L10.6667 9.33333'
				stroke='#868A92'
				stroke-width='1.5'
				stroke-linecap='round'
				stroke-linejoin='round'
			/>
		</svg>
	</div>
);

export const ClockIcon2 = () => (
	<svg
		width='18'
		height='18'
		viewBox='0 0 18 18'
		fill='none'
		xmlns='http://www.w3.org/2000/svg'
	>
		<path
			d='M9 16.5C13.1421 16.5 16.5 13.1421 16.5 9C16.5 4.85786 13.1421 1.5 9 1.5C4.85786 1.5 1.5 4.85786 1.5 9C1.5 13.1421 4.85786 16.5 9 16.5Z'
			stroke='#868A92'
			stroke-width='1.5'
			stroke-linecap='round'
			stroke-linejoin='round'
		/>
		<path
			d='M9 4.5V9L12 10.5'
			stroke='#868A92'
			stroke-width='1.5'
			stroke-linecap='round'
			stroke-linejoin='round'
		/>
	</svg>
);

export const XIcon = () => (
	<svg
		width='16'
		height='16'
		viewBox='0 0 16 16'
		fill='none'
		xmlns='http://www.w3.org/2000/svg'
	>
		<path
			d='M9.12 8L16 14.88L14.88 16L8 9.12L1.12 16L0 14.88L6.88 8L0 1.12L1.12 0L8 6.88L14.88 0L16 1.12L9.12 8Z'
			fill='black'
		/>
	</svg>
);

export const EditIcon2 = () => (
	<svg
		style={{ cursor: 'pointer' }}
		width='24'
		height='24'
		viewBox='0 0 24 24'
		fill='none'
		xmlns='http://www.w3.org/2000/svg'
	>
		<path
			d='M12 19.9996H21'
			stroke='#868A92'
			stroke-width='2'
			stroke-linecap='round'
			stroke-linejoin='round'
		/>
		<path
			d='M16.5 3.49962C16.8978 3.10179 17.4374 2.8783 18 2.8783C18.2786 2.8783 18.5544 2.93317 18.8118 3.03977C19.0692 3.14638 19.303 3.30263 19.5 3.49962C19.697 3.6966 19.8532 3.93045 19.9598 4.18782C20.0665 4.44519 20.1213 4.72104 20.1213 4.99962C20.1213 5.27819 20.0665 5.55404 19.9598 5.81141C19.8532 6.06878 19.697 6.30263 19.5 6.49962L7 18.9996L3 19.9996L4 15.9996L16.5 3.49962Z'
			stroke='#868A92'
			stroke-width='2'
			stroke-linecap='round'
			stroke-linejoin='round'
		/>
	</svg>
);

export const DeleteIcon = () => (
	<svg
		style={{ cursor: 'pointer' }}
		width='18'
		height='18'
		viewBox='0 0 18 18'
		fill='none'
		xmlns='http://www.w3.org/2000/svg'
	>
		<path
			d='M13.5 3.6C13.5 2.25 13.5 1.35 13.5 0H4.5C4.5 1.35 4.5 2.25 4.5 3.6C2.79 3.6 1.71 3.6 0 3.6V5.4H2.7V18C7.83 18 9.27 18 15.3 18V5.4H18V3.6C15.21 3.6 15.93 3.6 13.5 3.6ZM6.3 1.8H11.7V3.6H6.3V1.8ZM13.5 16.2H4.5V5.4H13.5V16.2ZM11.7 14.4H9.9V7.2H11.7V14.4ZM8.1 14.4H6.3V7.2H8.1V14.4Z'
			fill={'#868A92'}
		/>
	</svg>
);

export const WheelIcon = () => (
	<svg
		width='16'
		height='16'
		viewBox='0 0 16 16'
		fill='none'
		xmlns='http://www.w3.org/2000/svg'
	>
		<path
			d='M7.9879 2.26721C4.85096 2.26721 2.26379 4.85831 2.26379 8.00001C2.26379 11.1417 4.85096 13.7328 7.9879 13.7328C11.1248 13.7328 13.712 11.1741 13.712 8.00001C13.712 4.82592 11.1572 2.26721 7.9879 2.26721ZM5.75647 10.5263C6.24156 10.9474 6.50028 11.5304 6.50028 12.1458C5.43307 11.7571 4.5599 10.9474 4.04247 9.94333C4.65692 9.91094 5.27137 10.1053 5.75647 10.5263ZM9.70189 5.99191C9.11978 5.37652 8.40831 5.24697 8.05258 5.24697C8.02024 5.24697 7.9879 5.24697 7.9879 5.24697C7.34111 5.24697 6.72665 5.50608 6.2739 5.99191C5.72413 6.60729 4.8833 6.93118 4.04247 6.93118H3.75141C4.23651 4.98786 5.98284 3.62754 8.02024 3.62754C10.0576 3.62754 11.804 5.02025 12.2891 6.93118C11.7393 6.93118 10.6074 6.93118 9.70189 5.99191ZM7.24409 6.89879C7.63216 6.47774 8.34363 6.47774 8.73171 6.89879C10.0576 8.32389 11.7393 8.2915 12.3861 8.25911C12.3861 8.38867 12.3537 8.51822 12.3537 8.64778C11.2865 8.48584 10.187 8.80972 9.3785 9.55466C8.57001 10.2996 8.11726 11.336 8.18193 12.4049C8.05258 12.4049 7.95556 12.4049 7.8262 12.4049C7.89088 11.336 7.47046 10.2996 6.62964 9.55466C5.82115 8.80972 4.7216 8.48584 3.6544 8.64778C3.62206 8.51822 3.62206 8.38867 3.62206 8.25911C4.26885 8.25911 5.9505 8.2915 7.24409 6.89879ZM10.2517 10.5263C10.6721 10.1377 11.2219 9.94333 11.804 9.94333C11.8686 9.94333 11.901 9.94333 11.9657 9.94333C11.4806 10.9474 10.5751 11.7571 9.50786 12.1458C9.5402 11.5304 9.79891 10.9798 10.2517 10.5263Z'
			fill='#868A92'
		/>
		<path
			d='M13.6473 2.36437C12.1273 0.842105 10.1223 0 7.98787 0C3.58969 0 0 3.59514 0 8C0 12.4049 3.58969 16 7.98787 16C10.1223 16 12.1273 15.1579 13.6473 13.6356C16.7842 10.5587 16.7842 5.47368 13.6473 2.36437ZM14.6175 8C14.6175 11.6599 11.6422 14.6073 8.02021 14.6073C4.39818 14.6073 1.3906 11.6599 1.3906 8C1.3906 4.34008 4.36584 1.39271 7.98787 1.39271C11.6099 1.39271 14.6175 4.37247 14.6175 8Z'
			fill='#868A92'
		/>
	</svg>
);

export const ArrowDown = () => (
	<svg
		width='14'
		height='8'
		viewBox='0 0 14 8'
		fill='none'
		xmlns='http://www.w3.org/2000/svg'
	>
		<path
			d='M1 1L7 7L13 1'
			stroke='#868A92'
			stroke-width='2'
			stroke-linecap='round'
			stroke-linejoin='round'
		/>
	</svg>
);

export const ArrowUp = () => (
	<svg
		width='14'
		height='8'
		viewBox='0 0 14 8'
		fill='none'
		xmlns='http://www.w3.org/2000/svg'
	>
		<path
			d='M1 7L7 1L13 7'
			stroke='#868A92'
			stroke-width='2'
			stroke-linecap='round'
			stroke-linejoin='round'
		/>
	</svg>
);

export const BlueLine = () => (
	<svg
		width='78'
		height='5'
		viewBox='0 0 78 5'
		fill='none'
		xmlns='http://www.w3.org/2000/svg'
	>
		<rect y='0.955078' width='78' height='4' fill='#004DFF' />
	</svg>
);

export const RedDot = () => (
	<svg
		width='8'
		height='8'
		viewBox='0 0 8 8'
		fill='none'
		xmlns='http://www.w3.org/2000/svg'
	>
		<circle cx='4' cy='4' r='4' fill='#FF0000' />
	</svg>
);
