import React, { useState, Fragment } from 'react';
import { useLocation, useHistory } from 'react-router-dom';

import {
	HomeDashboardIcon,
	ScriptIcon,
	SettingIcon,
	TagihanSaya,
} from 'components/icon/iconSvg';

const dataMenu = [
	{ name: 'Dashboard', Icon: HomeDashboardIcon, path: '/vendor' },
	{
		name: 'Daftar Pemesanan',
		Icon: ScriptIcon,
		path: '/vendor/listOrder',
	},
	{ name: 'Credit Note', Icon: TagihanSaya, path: '/vendor/creditNote' },
	{
		name: 'Pengaturan',
		Icon: SettingIcon,
		path: '/vendor/settings',
		childMenu: [
			{
				name: 'Data Kendaraan',
				Icon: SettingIcon,
				path: '/vendor/settings/vehicle',
			},
			{
				name: 'Data Pengemudi',
				Icon: SettingIcon,
				path: '/vendor/settings/driver',
			},
		],
	},
];

export default function Menu() {
	let { pathname } = useLocation();
	let history = useHistory();

	const [activeChildMenu, setActiveChildMenu] = useState('');
	const [activeSubChildMenu, setActiveSubChildMenu] = useState('');
	const [activeMenu, setActiveMenu] = useState(false);
	const [hoverActiveMenu, setHoverActiveMenu] = useState(false);
	const [iconActive, setIconActive] = useState(false);

	const funcActiveChild = param =>
		// Add Logic if hash Child
		param.path === activeChildMenu ||
		(param.childMenu
			? param.childMenu.filter(val =>
					val.childMenu
						? val.childMenu.find(child => child.path === pathname)
						: val.path === pathname
			  ).length
			: false);
	// const funcActiveSubChild = param =>
	// 	param.path === activeChildMenu ||
	// 	(param.childMenu
	// 		? param.childMenu.filter(val => val.path === pathname).length
	// 		: false);

	const clickActiveMenu = param =>
		param.childMenu && param.path
			? setActiveChildMenu(activeChildMenu === param.path ? '' : param.path)
			: history.push(param.path);

	return (
		<div className='wrepper-menu'>
			<div
				className={`menu ${iconActive || hoverActiveMenu ? 'active-menu' : ''}`}
			>
				<div
					className='item-menu item-button'
					onClick={() => setActiveMenu(!activeMenu)}
				>
					<div className='button-menu'>
						<div
							className='icon'
							onClick={() => setIconActive(!iconActive)}
						></div>
					</div>
				</div>
				{dataMenu.map((value, index) => (
					<Fragment key={index}>
						<div
							className={`item-menu ${
								value.path === pathname ? 'item-menu-active' : ''
							} ${value.childMenu ? 'item-menu-child' : ''} ${
								funcActiveChild(value) ? 'rotate-icon' : ''
							}`}
							key={index}
							onClick={() => clickActiveMenu(value)}
							onMouseLeave={() => setHoverActiveMenu(false)}
							onMouseMove={() => setHoverActiveMenu(true)}
						>
							{
								<value.Icon
									color={value.path === pathname ? '#002985' : false}
								/>
							}
							<div className='text-name'>{value.name}</div>
						</div>
						{funcActiveChild(value)
							? value.childMenu.map((item, i) => (
									<div>
										<div
											className={`item-menu item-child ${
												item.path === pathname ? 'item-menu-active' : ''
											} ${funcActiveChild(value) ? 'block' : 'none'} ${
												item.childMenu ? 'item-menu-child' : ''
											} ${
												funcActiveChild(item) || activeSubChildMenu
													? 'rotate-icon'
													: ''
											}`}
											onClick={() => {
												if (item.childMenu && item.childMenu.length > 0) {
													setActiveSubChildMenu(item.path);
												} else {
													history.push(item.path);
												}
											}}
											key={i}
											onMouseLeave={() => setHoverActiveMenu(false)}
											onMouseMove={() => setHoverActiveMenu(true)}
										>
											{
												<item.Icon
													color={item.path === pathname ? '#004DFF' : false}
												/>
											}
											<div className='text-name'>{item.name}</div>
										</div>
										{(funcActiveChild(item) || activeSubChildMenu) &&
											item.childMenu &&
											item.childMenu.map((childItem, childIndex) => (
												<div
													className={`item-menu item-sub-child ${
														childItem.path === pathname
															? 'item-menu-active'
															: ''
													} ${funcActiveChild(value) ? 'block' : 'none'}`}
													onClick={() => history.push(childItem.path)}
													key={childIndex}
													onMouseLeave={() => setHoverActiveMenu(false)}
													onMouseMove={() => setHoverActiveMenu(true)}
												>
													{
														<item.Icon
															color={
																childItem.path === pathname ? '#004DFF' : false
															}
														/>
													}
													<div className='text-name'>{childItem.name}</div>
												</div>
											))}
									</div>
							  ))
							: ''}
					</Fragment>
				))}
			</div>
		</div>
	);
}
