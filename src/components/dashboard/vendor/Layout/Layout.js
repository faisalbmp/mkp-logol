import React from 'react';
import Menu from './Menu';
import Header from 'components/dashboard/header';
import FreightBreadCrumb from 'components/dashboard/FreightBreadCrumb';
import 'assets/fontello/css/fontello.css';
import { useLocation } from 'react-router-dom';

export default function Dashboard({ children, gatePassMenu }) {
	let { pathname } = useLocation();
	let showHeader = pathname !== '/dashboard/e-document/gatePass';
	const renderFreightBreadCrumb = () => {
		if (window.location.pathname.includes('dashboard/add/freight')) {
			return <FreightBreadCrumb title={'Buat Pesanan Kapal'} />;
		}

		return null;
	};
	return (
		<div className='dashboard'>
			<Menu />
			<div className='container'>
				{showHeader && <Header />}
				{renderFreightBreadCrumb()}
				<div className='content'>{children}</div>
			</div>
		</div>
	);
}
