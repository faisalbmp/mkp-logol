import React, { useMemo, useState, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SortIcon, ArrowDownD, ArrowUpD } from 'components/icon/iconSvg';
import SearchInput from 'components/dashboard/searchInput';
import Styles from 'assets/scss/dashboard/table.module.scss';
import Pagination from 'components/paginations/New/Pagination';
import { EditIcon2, DeleteIcon } from '../Icon';
import LoadingDot from 'components/loading/loadingDot';

const VehicleTable = props => {
	const [currentPage, setCurrentPage] = useState(1);
	const [postsPerPage, setPostPerPage] = useState(5);
	const [searchValue, setSearchValue] = useState('');
	const [editIcon, setEditIcon] = useState(false);
	const [deleteIcon, setDeleteIcon] = useState(false);
	const indexOfLastPost = currentPage * postsPerPage;
	const indexOfFirstPost = indexOfLastPost - postsPerPage;

	const { isLoading } = useSelector(state => state.driverReducers);

	const tableTitle = [
		{ label: 'Driver ID', iconSort: true, isBold: true },
		{ label: 'Driver Name', iconSort: true },
		{ label: 'Mobile Phone', iconSort: true },
		{ label: 'Username', iconSort: true },
		{ label: 'Password', iconSort: true },
		{ label: 'Aksi', iconSort: true },
	];

	const posts = useMemo(() => {
		let postData = props.listDriver;

		if (!props.listDriver) {
			postData = [];
		}

		if (searchValue) {
			postData = postData.filter(
				item =>
					item.driverName.toLowerCase().includes(searchValue.toLowerCase()) ||
					item.driverUsername.toLowerCase().includes(searchValue.toLowerCase())
			);
		}

		return postData;
	}, [searchValue, props.listDriver]);

	const gotoPage = page => {
		const currentPage = Math.max(0, Math.min(page, posts.length));
		setCurrentPage(currentPage);
	};

	const handleClick = page => {
		// e.preventDefault();
		gotoPage(page);
	};

	const handleMoveLeft = () => {
		// e.preventDefault();
		gotoPage(currentPage - 1);
	};

	const handleMoveRight = () => {
		// e.preventDefault();
		gotoPage(currentPage + 1);
	};

	return (
		<div className='truck'>
			{isLoading ? (
				<LoadingDot />
			) : (
				<div className='wrepper-table truck-table'>
					<div className='middle-component'>
						<SearchInput
							onChange={e => {
								setSearchValue(e.target.value);
								setCurrentPage(1);
							}}
							value={searchValue}
						/>
						<div className='right'>
							<div className='sort-item'>
								<SortIcon />
								<div className='text'>Urutkan</div>
							</div>

							{/* <div>
							<Select
								number={100}
								// setSelect={setSelect} 
								defaultValue={10} />
						</div> */}
							<div>
								<div className='select'>
									<div className='text left'>Tampilkan</div>
									<select
										defaultValue={postsPerPage}
										onChange={e => {
											setPostPerPage(e.target.value);
											setCurrentPage(1);
										}}
										className='select-dashboard'
									>
										<option value={5}>5</option>
										<option value={10}>10</option>
										<option value={25}>25</option>
										<option value={50}>50</option>
										<option value={100}>100</option>
									</select>

									<div className='text right'>hasil per halaman</div>
								</div>
							</div>
						</div>
					</div>
					<table style={{ width: '100%' }} className={Styles.table}>
						<tr>
							{tableTitle.map((item, index) => (
								<th className={Styles.th} key={index}>
									<div className='flex-row-style'>
										{item.isBold ? (
											<div style={{ color: '#333333' }}>{item.label}</div>
										) : (
											<div>{item.label}</div>
										)}
										{item.iconSort ? (
											<div className={Styles.iconSort}>
												<ArrowUpD active={false} />
												<ArrowDownD active={false} />
											</div>
										) : (
											''
										)}
									</div>
								</th>
							))}
						</tr>
						{posts.slice(indexOfFirstPost, indexOfLastPost).map(item => (
							<tr>
								<td>{item.driverID}</td>
								<td>{item.driverName} </td>
								<td>{item.mobilePhone}</td>
								<td>{item.driverUsername}</td>
								<td>{item.driverPassword}</td>
								<td>
									<div className='flex-row-style'>
										<div
											style={{ marginRight: '16px' }}
											onClick={() => props.editClicked(item.driverID, item.vendorID)}
										>
											<EditIcon2 />
										</div>
										<div
											onMouseMove={() => {
												setDeleteIcon(true);
											}}
											onMouseLeave={() => setDeleteIcon(false)}
											onClick={() => props.deleteClicked(item.driverID, item.driverName)}
										>
											<DeleteIcon isMouseHover={deleteIcon} />
										</div>
									</div>
								</td>
							</tr>
						))}
					</table>
					<Pagination
						totalRecords={posts.length}
						pageLimit={postsPerPage}
						pageNeighbours={1}
						currentPage={currentPage}
						handleClick={handleClick}
						handleMoveLeft={handleMoveLeft}
						handleMoveRight={handleMoveRight}
					/>
				</div>
			)}
		</div>
	);
};

export default VehicleTable;
