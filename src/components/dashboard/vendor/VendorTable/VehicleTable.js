import React, { useMemo, useState, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SortIcon, ArrowDownD, ArrowUpD } from 'components/icon/iconSvg';
import SearchInput from 'components/dashboard/searchInput';
import Styles from 'assets/scss/dashboard/table.module.scss';
import Pagination from 'components/paginations/New/Pagination';
import { EditIcon2, DeleteIcon } from '../Icon';
import LoadingDot from 'components/loading/loadingDot';

const VehicleTable = props => {
	const [currentPage, setCurrentPage] = useState(1);
	const [postsPerPage, setPostPerPage] = useState(5);
	const [searchValue, setSearchValue] = useState('');
	const [editIcon, setEditIcon] = useState(false);
	const [deleteIcon, setDeleteIcon] = useState(false);
	const indexOfLastPost = currentPage * postsPerPage;
	const indexOfFirstPost = indexOfLastPost - postsPerPage;

	const { isLoading } = useSelector(state => state.vehicleReducers);

	const tableTitle = [
		{ label: 'No. Kendaraan', iconSort: true, isBold: true },
		{ label: 'Merk Kendaraan', iconSort: true },
		{ label: 'Tipe Kendaraan', iconSort: true },
		{ label: 'Tahun', iconSort: true },
		{ label: 'No. TID', iconSort: true },
		{ label: 'No. STNK', iconSort: true },
		{ label: 'Masa Berlaku STNK', iconSort: true },
		{ label: 'Aksi', iconSort: true },
	];

	const tableBody = [
		{
			noKendaraan: 'B0205JRM',
			merk: 'Becak Gaul',
			tipe: 'Truk',
			tahun: '2020',
			noTid: 'TID0205JRM',
			noStnk: 'STNK0205',
			validity: '10 Januari 2021',
		},
	];

	const posts = useMemo(() => {
		let postData = props.listVehicle;

		if (!props.listVehicle) {
			postData = [];
		}

		if (searchValue) {
			postData = postData.filter(
				item =>
					item.vehicleNumber.toLowerCase().includes(searchValue.toLowerCase()) ||
					item.tidNumber.toLowerCase().includes(searchValue.toLowerCase()) ||
					item.brand.toLowerCase().includes(searchValue.toLowerCase())
			);
		}

		return postData;
	}, [searchValue, props.listVehicle]);

	const gotoPage = page => {
		const currentPage = Math.max(0, Math.min(page, posts.length));
		setCurrentPage(currentPage);
	};

	const handleClick = page => {
		// e.preventDefault();
		gotoPage(page);
	};

	const handleMoveLeft = () => {
		// e.preventDefault();
		gotoPage(currentPage - 1);
	};

	const handleMoveRight = () => {
		// e.preventDefault();
		gotoPage(currentPage + 1);
	};

	return (
		<div className='truck'>
			{isLoading ? (
				<LoadingDot />
			) : (
				<div className='wrepper-table truck-table'>
					<div className='middle-component'>
						<SearchInput
							onChange={e => {
								setSearchValue(e.target.value);
								setCurrentPage(1);
							}}
							value={searchValue}
						/>
						<div className='right'>
							<div className='sort-item'>
								<SortIcon />
								<div className='text'>Urutkan</div>
							</div>

							{/* <div>
							<Select
								number={100}
								// setSelect={setSelect} 
								defaultValue={10} />
						</div> */}
							<div>
								<div className='select'>
									<div className='text left'>Tampilkan</div>
									<select
										defaultValue={postsPerPage}
										onChange={e => {
											setPostPerPage(e.target.value);
											setCurrentPage(1);
										}}
										className='select-dashboard'
									>
										<option value={5}>5</option>
										<option value={10}>10</option>
										<option value={25}>25</option>
										<option value={50}>50</option>
										<option value={100}>100</option>
									</select>

									<div className='text right'>hasil per halaman</div>
								</div>
							</div>
						</div>
					</div>
					<table style={{ width: '100%' }} className={Styles.table}>
						<tr>
							{tableTitle.map((item, index) => (
								<th className={Styles.th} key={index}>
									<div className='flex-row-style'>
										{item.isBold ? (
											<div style={{ color: '#333333' }}>{item.label}</div>
										) : (
											<div>{item.label}</div>
										)}
										{item.iconSort ? (
											<div className={Styles.iconSort}>
												<ArrowUpD active={false} />
												<ArrowDownD active={false} />
											</div>
										) : (
											''
										)}
									</div>
								</th>
							))}
						</tr>
						{posts.slice(indexOfFirstPost, indexOfLastPost).map(item => (
							<tr>
								<td>{item.vehicleNumber}</td>
								<td>{item.brand} </td>
								<td>{item.type}</td>
								<td>{item.year}</td>
								<td>{item.tidNumber}</td>
								<td>{item.stnkNumber}</td>
								<td>{item.stnkExpired}</td>
								<td>
									<div
										className='flex-row-style'
										style={{
											justifyContent: 'space-between',
											textAlign: 'center',
										}}
									>
										<div
											onMouseMove={() => {
												setEditIcon(true);
											}}
											onMouseLeave={() => setEditIcon(false)}
											onClick={() => props.editClicked(item.vehicleNumber)}
										>
											<EditIcon2 isMouseHover={editIcon} />
										</div>
										<div
											onMouseMove={() => {
												setDeleteIcon(true);
											}}
											onMouseLeave={() => setDeleteIcon(false)}
											onClick={() => props.deleteClicked(item.vehicleNumber)}
										>
											<DeleteIcon isMouseHover={deleteIcon} />
										</div>
									</div>
								</td>
							</tr>
						))}
					</table>
					<Pagination
						totalRecords={posts.length}
						pageLimit={postsPerPage}
						pageNeighbours={1}
						currentPage={currentPage}
						handleClick={handleClick}
						handleMoveLeft={handleMoveLeft}
						handleMoveRight={handleMoveRight}
					/>
				</div>
			)}
		</div>
	);
};

export default VehicleTable;
