import React, { useState } from 'react';
// import { useSelector } from 'react-redux';
import Styles from '../../index.module.scss';
import SelectBox from 'components/checkBox/select-box';

const InputFieldExport = props => {
	const [selectDriver, setSelectDriver] = useState(false);
	const [selectVehicle, setSelectVehicle] = useState(false);
	const [driver, setDriver] = useState('Driver');
	const [vehicle, setVehicle] = useState('No Kendaraan');
	return (
		<tr>
			<td>
				<div className={Styles.box} style={{ width: 57 }}>
					<span style={{ fontSize: 16 }}>{props.no}</span>
				</div>
			</td>
			<td style={{ width: 267 }}>
				{/* <SelectBoxM label='Driver' /> */}
				<SelectBox
					placeholder={driver}
					headerStyle={{ width: 267 }}
					isSearch={true}
					openList={selectDriver}
					selectList={props.formData.drivers.options}
					openSelectList={() => {
						setSelectVehicle(false);
						setSelectDriver(prev => {
							return !prev;
						});
					}}
					selectedList={(item, index) => {
						setDriver(item.name);
						setSelectDriver(false);
						props.changeHandler(item, 'drivers');
					}}
				/>
			</td>
			<td>
				{/* <SelectBoxM label='No Kendaraan' /> */}
				<SelectBox
					placeholder={vehicle}
					headerStyle={{ width: 266 }}
					isSearch={true}
					openList={selectVehicle}
					selectList={props.formData.vehicles.options}
					openSelectList={() => {
						setSelectDriver(false);
						setSelectVehicle(prev => {
							return !prev;
						});
					}}
					selectedList={(item, index) => {
						setVehicle(item.name);
						setSelectVehicle(false);
						props.changeHandler(item, 'vehicles');
					}}
				/>
			</td>
		</tr>
	);
};

export default InputFieldExport;
