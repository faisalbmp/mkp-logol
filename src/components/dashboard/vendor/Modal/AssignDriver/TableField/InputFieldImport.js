import React, { useState } from 'react';
// import { useSelector } from 'react-redux';
import Styles from '../../index.module.scss';
import SelectBox from 'components/checkBox/select-box';

const InputFieldImport = props => {
	const [selectDriver, setSelectDriver] = useState(false);
	const [selectVehicle, setSelectVehicle] = useState(false);
	const [selectKontainer, setSelectKontainer] = useState(false);
	const [driver, setDriver] = useState('Driver');
	const [vehicle, setVehicle] = useState('No Kendaraan');
	const [container, setContainer] = useState('No. Kontainer - Lokasi');
	const kontainer = [{ name: '' }, { name: '' }, { name: '' }];
	return (
		<tr>
			<td>
				<div className={Styles.box} style={{ width: 57 }}>
					<span style={{ fontSize: 16 }}>{props.no}</span>
				</div>
			</td>
			<td style={{ width: 267 }}>
				{/* <SelectBoxM label='Driver' /> */}
				<SelectBox
					placeholder={driver}
					headerStyle={{ width: 291 }}
					isSearch={true}
					openList={selectDriver}
					selectList={props.formData.drivers.options}
					openSelectList={() => {
						setSelectVehicle(false);
						setSelectDriver(prev => {
							return !prev;
						});
					}}
					selectedList={(item, index) => {
						setDriver(item.name);
						setSelectDriver(false);
						props.changeHandler(item, 'drivers');
					}}
				/>
			</td>
			<td>
				{/* <SelectBoxM label='No Kendaraan' /> */}
				<SelectBox
					placeholder={vehicle}
					headerStyle={{ width: 266 }}
					isSearch={true}
					openList={selectVehicle}
					selectList={props.formData.vehicles.options}
					openSelectList={() => {
						setSelectDriver(false);
						setSelectVehicle(prev => {
							return !prev;
						});
					}}
					selectedList={(item, index) => {
						setVehicle(item.name);
						setSelectVehicle(false);
						props.changeHandler(item, 'vehicles');
					}}
				/>
			</td>
			<td>
				<SelectBox
					placeholder={container}
					headerStyle={{ width: 266 }}
					// isSearch={false}
					isDefault
					openList={selectKontainer}
					selectList={props.formData.containers.options}
					openSelectList={() => {
						setSelectVehicle(false);
						setSelectDriver(false);
						setSelectKontainer(!selectKontainer);
					}}
					selectedList={(item, index) => {
						setContainer(item.name);
						setSelectKontainer(false);
						props.changeHandler(item, 'containers');
					}}
				/>
			</td>
		</tr>
	);
};

export default InputFieldImport;
