import React, { useState, useEffect, useMemo } from 'react';
import { useSelector } from 'react-redux';
import Modal from 'components/modal/wrepperModal';
import Styles from '../index.module.scss';
import InputField from './TableField/InputFieldImport';
import Pagination from 'components/paginations/New/Pagination';
import Spinner from 'components/loading/loadingDot';
import { BlueLine } from '../../Icon';

const AssignDriverImport = props => {
	const [activeTab, setActiveTab] = useState('');
	const [formData, setFormData] = useState({
		drivers: { options: [], value: '' },
		vehicles: { options: [], value: '' },
		containers: { options: [], value: [] },
		// sealNumber: {}
	});
	const { isLoading } = useSelector(state => state.containerReducers);

	const [currentPage, setCurrentPage] = useState(1);
	const [postsPerPage, setPostPerPage] = useState(5);
	const indexOfLastPost = currentPage * postsPerPage;
	const indexOfFirstPost = indexOfLastPost - postsPerPage;

	let drivers = props.drivers;
	let vehicles = props.vehicles;
	let containers = props.containers;

	const excludeDriver = id => {
		drivers = props.drivers.filter(driver => driver.id !== id);
		setFormData({ drivers: { options: drivers } }); //jika depedency di useEffect kosong
	};

	const excludeVehicle = id => {
		vehicles = props.vehicles.filter(vehicle => vehicle.id !== id);
		setFormData({ vehicles: { options: vehicles } }); //jika depedency di useEffect kosong
	};

	useEffect(() => {
		setFormData({
			drivers: { options: drivers },
			vehicles: { options: vehicles },
			containers: { options: containers },
		});
		setActiveTab(props.orderData?.containerType.substring(0, 4));
	}, [drivers, vehicles, containers]);

	const changeInputHandler = (formConfig, type) => {
		const updatedFormData = {
			...formData,
		};
		const updatedFormElement = {
			...updatedFormData[type],
		};
		updatedFormElement.value = formConfig.value;
		updatedFormData[type] = updatedFormElement;
		if (type === 'driver') {
			excludeDriver(formConfig.id);
		}
		if (type === 'vehicle') {
			excludeVehicle(formConfig.id);
		}
		setFormData(updatedFormData);
	};

	const cont = {
		name: props.orderData?.containerType,
		quantity: props.orderData?.quantity,
	};

	const fields = [];
	let i = 0;
	for (let key in containers) {
		i++;
		fields.push({
			index: i,
			name: containers[key].name,
		});
	}

	// for (let i = 0; i < props.quantity; i++) {
	// 	fields.push({
	// 		index: i
	// 	})
	// }

	const gotoPage = page => {
		const currentPage = Math.max(0, Math.min(page, fields.length));
		setCurrentPage(currentPage);
	};

	const handleClick = page => {
		// e.preventDefault();
		gotoPage(page);
	};

	const handleMoveLeft = () => {
		// e.preventDefault();
		gotoPage(currentPage - 1);
	};

	const handleMoveRight = () => {
		// e.preventDefault();
		gotoPage(currentPage + 1);
	};
	// console.log(formData);
	return (
		<Modal
			show={props.show}
			onShow={props.onShow}
			onClose={props.onClose}
			width={1026}
			height={663}
		>
			<div className={Styles.body} style={{ width: 1026, height: 663 }}>
				<h3>Assign Driver Import</h3>
				<div className='flex-row-style'>
					<div
						className={`${Styles.boxTab} ${activeTab === '20GP' && Styles.boxTabActive}`}
						onClick={() => setActiveTab('20GP')}
					>
						<div className={Styles.label}>20' General Purpose</div>
						{activeTab === '20GP' && <BlueLine />}
					</div>
					<div
						className={`${Styles.boxTab} ${activeTab === '40GP' && Styles.boxTabActive}`}
						onClick={() => setActiveTab('40GP')}
					>
						<div className={Styles.label}>40' General Purpose</div>
						{activeTab === '40GP' && <BlueLine />}
					</div>
				</div>
				{isLoading ? (
					<Spinner />
				) : (
					<React.Fragment>
						<table
							className={Styles.table}
							style={{ fontSize: 12, textAlign: 'left' }}
							width='100%'
						>
							<tr>
								<th style={{ textAlign: 'center' }}>#</th>
								<th>Nama Driver</th>
								<th>No. Kendaraan</th>
								<th>No. Kontainer - Lokasi</th>
							</tr>
							{fields.slice(indexOfFirstPost, indexOfLastPost).map((item, index) => (
								<InputField
									formData={formData}
									isImport
									key={index}
									no={item.index}
									kontainer={item.kontainer}
									changeHandler={changeInputHandler}
								/>
							))}
							{/* {() => {
								if (cont.name === activeTab) {
									for (let i = 0; i < cont.quantity; i++) {
										return (
											<InputField
												formData={formData}
												isImport
												key={i}
												no={i + 1}
												changeHandler={changeInputHandler}
											/>
										);
									}
								}
							}} */}
						</table>
						<div
							style={{
								marginRight: 26,
								marginLeft: 26,
								display: 'flex',
								justifyContent: 'space-between',
							}}
						>
							<div style={{ marginTop: -16 }}>
								<Pagination
									totalRecords={fields.length}
									pageLimit={postsPerPage}
									pageNeighbours={1}
									currentPage={currentPage}
									handleClick={handleClick}
									handleMoveLeft={handleMoveLeft}
									handleMoveRight={handleMoveRight}
								/>
							</div>
							<div>
								<button className={Styles.buttonBatal} onClick={props.onClose}>
									Batal
								</button>
								<button
									className='btn-primary'
									style={{ width: 144, height: 48 }}
									onClick={() => props.assignDriver(formData)}
								>
									Simpan
								</button>
							</div>
						</div>
					</React.Fragment>
				)}
			</div>
		</Modal>
	);
};

export default AssignDriverImport;
