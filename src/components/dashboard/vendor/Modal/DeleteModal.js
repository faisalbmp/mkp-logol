import React from 'react';
import Modal from 'components/modal/wrepperModal';
import Styles from './index.module.scss';

const DeleteModal = props => {
	return (
		<Modal show={props.show} onClose={props.onClose} width={499} height={256}>
			<div
				className={Styles.body}
				style={{ width: 499, height: 256, padding: 40 }}
			>
				<h4 style={{ fontSize: 20 }}>{props.title}</h4>
				<p>{props.text}</p>
				<div
					style={{
						display: 'flex',
						justifyContent: 'flex-end',
						marginTop: 40,
					}}
				>
					<button className={Styles.buttonBatal} onClick={props.onSuccess}>
						Iya Hapus
					</button>
					<button
						className='btn-primary'
						style={{ width: 144, height: 48 }}
						onClick={props.onClose}
					>
						Batalkan
					</button>
				</div>
			</div>
		</Modal>
	);
};

export default DeleteModal;
