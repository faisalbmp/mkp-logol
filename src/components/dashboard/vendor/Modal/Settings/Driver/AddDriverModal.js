import React, { useState } from 'react';
import Modal from 'components/modal/wrepperModal';
import Styles from '../../index.module.scss';
import InputText from 'components/input/inputText';
import { XIcon } from '../../../Icon';
import { getCookie } from 'utils/cookies';

const AddDriverModal = props => {
	const userId = getCookie('userId');
	return (
		<Modal show={props.show} width={816} height={584} onClose={props.onClose}>
			<div
				className={Styles.body}
				style={{ height: 584, width: 816, padding: '32px 24px 32px 24px' }}
			>
				<div className='flex-row-style' style={{ justifyContent: 'space-between' }}>
					<div style={{ fontSize: 20, color: '#333333' }}>Tambah Pengemudi</div>
					<div style={{ cursor: 'pointer' }} onClick={props.onClose}>
						<XIcon />
					</div>
				</div>
				<div
					style={{
						marginTop: 37,
						marginBottom: 32,
						borderBottom: '1px solid #E0E5E8',
					}}
				>
					<form>
						<div style={{ marginBottom: 32 }}>
							<InputText
								textInputTitle='Vendor ID'
								value={props.formData?.vendorId}
								inputTextStyle={{ width: '100%' }}
							/>
						</div>
						<div
							className='flex-row-style'
							style={{ marginBottom: 32, justifyContent: 'space-between' }}
						>
							<InputText
								name='driverName'
								onChange={props.changeHandler}
								// value={props.formData?.driverName ?? ''}
								textInputTitle='Nama Pengemudi'
								inputTextStyle={{ width: 372 }}
							/>
							<InputText
								name='mobilePhone'
								onChange={props.changeHandler}
								textInputTitle='No. Handphone Pengemudi'
								inputTextStyle={{ width: 372 }}
							/>
						</div>
						<div
							className='flex-row-style'
							style={{ marginBottom: 34, justifyContent: 'space-between' }}
						>
							<InputText
								name='driverUsername'
								onChange={props.changeHandler}
								textInputTitle='Username'
								inputTextStyle={{ width: 372 }}
							/>
							<InputText
								name='driverPassword'
								onChange={props.changeHandler}
								textInputTitle='Password'
								inputTextStyle={{ width: 372 }}
							/>
						</div>
					</form>
				</div>
				<div
					style={{
						display: 'flex',
						justifyContent: 'flex-end',
					}}
				>
					<button className={Styles.buttonBatal} onClick={props.onClose}>
						Batal
					</button>
					<button
						className='btn-primary'
						style={{ width: 144, height: 48 }}
						onClick={props.insertHandler}
					>
						Simpan
					</button>
				</div>
			</div>
		</Modal>
	);
};

export default AddDriverModal;
