import React from 'react';
import Modal from 'components/modal/wrepperModal';
import Styles from '../../index.module.scss';
import InputText from 'components/input/inputText';

const AddVehicleModal = props => {
	return (
		<Modal width={711} height={856} onClose={props.onClose} show={props.show}>
			<div
				className={Styles.body}
				style={{
					height: 856,
					width: 711,
					padding: '32px 24px 32px 24px',
				}}
			>
				<div className={Styles.ModalHeader}>
					<div style={{ fontSize: 20, color: '#333333' }}>ID Vendor</div>
					<div style={{ fontSize: 20, color: '#002985', fontWeight: 'bold' }}>
						{props.formData.vendorId}
					</div>
				</div>
				<div style={{ paddingTop: 32 }}>
					<h2 style={{ marginBottom: 32 }}>Add Vehicle</h2>
					<div>
						<form>
							<InputText
								name='vehicleNumber'
								onChange={props.changeHandler}
								textInputTitle='No Kendaraan'
								inputContainerStyle={{ marginBottom: 16 }}
								inputTextStyle={{ width: '100%' }}
							/>
							<InputText
								name='brand'
								onChange={props.changeHandler}
								textInputTitle='Merk Kendaraan'
								inputContainerStyle={{ marginBottom: 16 }}
								inputTextStyle={{ width: '100%' }}
							/>
							<InputText
								name='type'
								onChange={props.changeHandler}
								textInputTitle='Tipe Kendaraan'
								inputContainerStyle={{ marginBottom: 16 }}
								inputTextStyle={{ width: '100%' }}
							/>
							<InputText
								name='year'
								onChange={props.changeHandler}
								textInputTitle='Tahun'
								inputContainerStyle={{ marginBottom: 16 }}
								inputTextStyle={{ width: '100%' }}
							/>
							<InputText
								name='tidNumber'
								onChange={props.changeHandler}
								textInputTitle='No. TID'
								inputContainerStyle={{ marginBottom: 16 }}
								inputTextStyle={{ width: '100%' }}
							/>
							<div
								className='flex-row-style'
								style={{ justifyContent: 'space-between', marginBottom: 32 }}
							>
								<InputText
									name='stnkNumber'
									onChange={props.changeHandler}
									textInputTitle='No. STNK'
									inputTextStyle={{ width: 319 }}
								/>
								<InputText
									name='stnkExpired'
									value={props.formData.stnkExpired}
									textInputTitle='Masa Berlaku STNK'
									inputTextStyle={{ width: 319 }}
								/>
							</div>
						</form>
					</div>
				</div>
				<div
					style={{
						display: 'flex',
						justifyContent: 'flex-end',
					}}
				>
					<button className={Styles.buttonBatal} onClick={props.onClose}>
						Batal
					</button>
					<button
						className='btn-primary'
						style={{ width: 144, height: 48 }}
						onClick={props.onSuccess}
					>
						Simpan
					</button>
				</div>
			</div>
		</Modal>
	);
};

export default AddVehicleModal;
