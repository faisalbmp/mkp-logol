import React from 'react';
import Modal from 'components/modal/wrepperModal';
import Styles from '../index.module.scss';

import AddForm from './Form/AddForm';
import EditForm from './Form/EditForm';

const OrderModal = props => {
	return (
		<Modal
			show={props.show}
			onShow={props.onShow}
			onClose={props.onClose}
			width={711}
			height={844}
		>
			<div
				className={Styles.body}
				style={{ width: 711, height: 844, padding: '32px 24px 32px 24px' }}
			>
				<div className={Styles.ModalHeader}>
					<div style={{ fontSize: 20, color: '#333333' }}>Truck Order ID</div>
					<div style={{ fontSize: 20, color: '#002985', fontWeight: 'bold' }}>
						VODT-20200909-000001
					</div>
				</div>
				<AddForm style={{ paddingTop: 32 }} />
				<EditForm style={{ paddingTop: 32, paddingBottom: 16 }} />
				<div
					style={{
						display: 'flex',
						justifyContent: 'flex-end',
					}}
				>
					<button className={Styles.buttonBatal} onClick={props.onClose}>
						Batal
					</button>
					<button className='btn-primary' style={{ width: 144, height: 48 }}>
						Simpan
					</button>
				</div>
			</div>
		</Modal>
	);
};

export default OrderModal;
