import React from 'react';
import InputText from 'components/input/inputText';

const AddForm = props => {
	return (
		<div style={props.style}>
			<h2 style={{ marginBottom: 32 }}>Add Data</h2>
			<div>
				<form>
					<InputText
						textInputTitle='Container Number'
						inputContainerStyle={{ marginBottom: 16 }}
						inputTextStyle={{ width: '100%' }}
					/>
					<InputText
						textInputTitle='Iso Code'
						inputContainerStyle={{ marginBottom: 16 }}
						inputTextStyle={{ width: '100%' }}
					/>
					<InputText
						textInputTitle='Serial Number'
						inputContainerStyle={{ marginBottom: 16 }}
						inputTextStyle={{ width: '100%' }}
					/>
				</form>
			</div>
		</div>
	);
};

export default AddForm;
