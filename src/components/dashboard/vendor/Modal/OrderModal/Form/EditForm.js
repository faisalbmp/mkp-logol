import React from 'react';
import InputText from 'components/input/inputText';
import { SelectBoxM } from 'components/select';
import { span } from './editForm.module.scss';

const EditForm = props => {
	return (
		<div style={props.style}>
			<h2 style={{ marginBottom: 32 }}>Edit Data</h2>
			<div>
				<form>
					<div style={{ width: '100%', marginBottom: 16 }}>
						<span className={span}>Nama Driver</span>
						<SelectBoxM label='Driver' />
					</div>
					<InputText
						textInputTitle='No. Kontainer'
						inputContainerStyle={{ marginBottom: 16 }}
						inputTextStyle={{ width: '100%' }}
					/>
				</form>
			</div>
		</div>
	);
};

export default EditForm;
