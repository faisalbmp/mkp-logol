import React from 'react';
import Modal from 'components/modal/wrepperModal';
import Styles from '../index.module.scss';
import { ClockIcon2 } from '../../Icon';

const RuteModal = props => {
	const inputs = [
		{
			title: 'DEPO',
			name: 'Sarana Inti Logitama',
			in: '09 : 11 WIB',
			out: '10 : 44 WIB',
		},
		{
			title: 'Pick Up Barang',
			name: 'Factory 1 (Cikarang)',
			in: '12 : 01 WIB',
			out: ' :  WIB',
		},
		{
			title: 'Tujuan Pengiriman',
			name: 'JICT 1 (TJ. Priuk)',
			in: ' :  WIB',
			out: ' :  WIB',
		},
	];
	return (
		<Modal
			show={props.show}
			onShow={props.onShow}
			onClose={props.onClose}
			width={606}
			height={660}
		>
			<div className={Styles.ruteModal}>
				<div className={Styles.ModalHeader}>
					<div style={{ fontSize: 20, color: '#333333' }}>Truck Order ID</div>
					<div style={{ fontSize: 20, color: '#002985', fontWeight: 'bold' }}>
						VODT-20200909-000001
					</div>
				</div>
				{inputs.map((item, index) => (
					<div className={Styles.ruteModalForm} key={index}>
						<div className={Styles.title}>
							<div className={Styles.label}>{item.title}</div>
							<div className={Styles.dot}></div>
							<div className={Styles.name}>{item.name}</div>
						</div>
						<div className={Styles.formBody}>
							<div
								style={{
									display: 'flex',
									justifyContent: 'space-between',
								}}
							>
								<div>
									<p
										style={{
											fontWeight: 'bold',
											fontSize: 12,
											color: '#333333',
										}}
									>
										Waktu Masuk
									</p>
									<div className={Styles.input}>
										<div className={Styles.clock}>
											<ClockIcon2 />
										</div>
										<div className={Styles.value}>{item.in}</div>
									</div>
								</div>
								<div>
									<p
										style={{
											fontWeight: 'bold',
											fontSize: 12,
											color: '#333333',
										}}
									>
										Waktu Keluar
									</p>
									<div className={Styles.input}>
										<div className={Styles.clock}>
											<ClockIcon2 />
										</div>
										<div className={Styles.value}>{item.out}</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				))}
				<div
					style={{
						display: 'flex',
						justifyContent: 'flex-end',
						marginTop: 32,
					}}
				>
					<button className={Styles.buttonBatal} onClick={props.onClose}>
						Batal
					</button>
					<button className='btn-primary' style={{ width: 144, height: 48 }}>
						Simpan
					</button>
				</div>
			</div>
		</Modal>
	);
};

export default RuteModal;
