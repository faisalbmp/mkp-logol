import React from 'react';
import Styles from '../truckOrderDetail.module.scss';

const DetailItem = props => {
	return (
		<div className={Styles.detailItem}>
			<div className={Styles.title} style={{ marginBottom: 16 }}>
				{props.item.title}
			</div>
			<div className={`flex-row-style ${Styles.desc}`}>
				{props.item.icon && <div style={{ marginRight: 8 }}>{props.item.icon}</div>}
				{props.item.desc}
			</div>
		</div>
	);
};

export default DetailItem;
