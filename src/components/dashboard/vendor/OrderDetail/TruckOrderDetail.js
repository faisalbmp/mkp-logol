import React, { useState, useMemo, useEffect } from 'react';
import { useSelector } from 'react-redux';
import Styles from './truckOrderDetail.module.scss';
import DetailItem from './DetailItem/DetailItem';
import Kontainer from './Kontainer/Kontainer';
import kontainerS from './Kontainer/imgs/20gp.png';
import kontainerL from './Kontainer/imgs/40gp.png';
import Map from 'components/map/MapDirectionsRenderer';
import Pagination from 'components/paginations/New/Pagination';
import { SortIcon, ArrowDownD, ArrowUpD } from 'components/icon/iconSvg';
import SearchInput from 'components/dashboard/searchInput';
import Select from 'components/dashboard/select';
import { EditIcon, ClockIcon, ArrowDown, ArrowUp, RedDot } from '../Icon';
import LoadingDot from 'components/loading/loadingDot';

const googleMapsApiKey = process.env.REACT_APP_GOOGLE_MAP;

const TruckOrderDetail = props => {
	const { isLoading, listOrderDetail, listContainer } = props.vendorReducer;
	console.log(listOrderDetail);
	// const [selectVal, setSelect] = useState(1);
	const [searchValue, setSearchValue] = useState('');
	const [currentPage, setCurrentPage] = useState(1);
	const [postsPerPage] = useState(5);
	const [expandMap, setExpandMap] = useState(false);
	const mapRef = React.createRef();
	const indexOfLastPost = currentPage * postsPerPage;
	const indexOfFirstPost = indexOfLastPost - postsPerPage;

	useEffect(() => {
		//   var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
		(function () {
			var s1 = document.createElement('script'),
				s0 = document.getElementsByTagName('script')[0];
			s1.async = true;
			s1.src = 'https://embed.tawk.to/5f89d405fd4ff5477ea68a80/default';
			s1.charset = 'UTF-8';
			s1.setAttribute('crossorigin', '*');
			s0.parentNode.insertBefore(s1, s0);
		})();
	});

	const getStatus = status => {
		switch (status) {
			case '0':
				return 'Dalam Proses';
			case '1':
				return 'Diterima';
			case '2':
				return 'Depo In';
			case '3':
				return 'Depo Out';
			case '4':
				return 'Factory In';
			case '5':
				return 'Factory Out';
			case '6':
				return 'Port In';
			case '7':
				return 'Port Out';
			case '8':
				return 'Delivered';
			default:
				return '';
		}
	};

	if (listOrderDetail) {
	}

	const detailItems = [
		{ title: 'Tanggal Muat Barang', desc: listOrderDetail[0]?.stuffingDate },
		{ title: 'Customer', desc: listOrderDetail[0]?.customer },
		{ title: 'DEPO', desc: listOrderDetail[0]?.destname1 },
		{ title: 'Alamat Pick-Up', desc: listOrderDetail[0]?.destname2 },
		{ title: 'Tujuan Pengiriman', desc: listOrderDetail[0]?.destname3 },
		{
			title: 'Status',
			desc: props.getVendorStatus(props.orderData.vendorStatus),
			icon: <RedDot />,
		},
	];

	// const details = []
	// for(let key in listOrderDetail) {
	// 	details.push({
	// 		title:
	// 	})
	// }

	const tableTitle = [
		// { name: '', iconSort: false },
		{ name: 'Truck Order ID', iconSort: true },
		{ name: 'Nama Driver', iconSort: true },
		{ name: 'No. Kendaraan', iconSort: true },
		{ name: 'Tipe', iconSort: true },
		{ name: 'No. Kontainer', iconSort: true },
		{ name: 'No. Seal', iconSort: true },
		{ name: 'No. ISO', iconSort: true },
		{ name: 'Destinasi 1', iconSort: true },
		{ name: 'Destinasi 2', iconSort: true },
		{ name: 'Destinasi 3', iconSort: true },
		{ name: 'Gate Pass', iconSort: true },
		{ name: 'Status', iconSort: true },
	];

	const places = [
		// { latitude: 25.8103146, longitude: -80.1751609 },
		// { latitude: 28.4813018, longitude: -81.4387899 },
		'',
		'',
	];

	const expandMapHandler = () => {
		if (expandMap) {
			mapRef.current.style.height = '483px';
		} else {
			mapRef.current.style.height = '84px';
		}
	};

	const posts = useMemo(() => {
		let postData = listOrderDetail;
		// let postData = tableBody;

		if (!listOrderDetail) {
			postData = [];
		}

		if (searchValue) {
			postData = postData.filter(item =>
				item.orderId
					.toLowerCase()
					.includes(
						searchValue.toLowerCase() ||
							item.noIso.toLowerCase().includes(searchValue.toLowerCase())
					)
			);
		}
		return postData;
	}, [searchValue, listOrderDetail]);

	const gotoPage = page => {
		const currentPage = Math.max(0, Math.min(page, posts.length));
		setCurrentPage(currentPage);
	};

	const handleClick = page => {
		// e.preventDefault();
		gotoPage(page);
	};

	const handleMoveLeft = () => {
		// e.preventDefault();
		gotoPage(currentPage - 1);
	};

	const handleMoveRight = () => {
		// e.preventDefault();
		gotoPage(currentPage + 1);
	};

	return (
		<React.Fragment>
			{isLoading ? (
				<div style={{ margin: 'auto', textAlign: 'center' }}>
					<LoadingDot />
				</div>
			) : (
				<div style={{ marginTop: 36 }}>
					<div className={Styles.headerDetail}>
						<div className='flex-row-style'>
							<div className={Styles.headerItem}>
								<div
									className={props.type === 'EXPORT' ? Styles.btExpor : Styles.btImpor}
								>
									{props.type}
								</div>
							</div>
							<div className={Styles.headerItem}>
								<div className={Styles.label}>
									No. Booking : {' ' + props.orderData.orderManagementID}
									{/* {listOrderDetail !== null
										? listOrderDetail[0].vendorOrderDetailID
										: ''} */}
								</div>
							</div>
							<div className={Styles.headerItem}>
								<div className={Styles.label}>
									{props.type === 'EXPORT' ? 'SI ' : 'BL '} :
									{props.type === 'EXPORT'
										? ' ' + listOrderDetail[0]?.shippingInstruction
										: ' ' + props.orderData.blNo}
								</div>
							</div>
							{props.type === 'IMPORT' && (
								<div className={Styles.headerItem}>
									<div className={Styles.label}>SPPB : {' ' + props.orderData.sppb}</div>
								</div>
							)}
							<div className={Styles.headerItem}>
								<div className={Styles.label}>
									DO :{' ' + props.orderData.deliveryOrderID}
								</div>
							</div>
						</div>
						<div className={Styles.mapCanvas} ref={mapRef}>
							<div
								className={Styles.btnMapChange}
								onClick={() => {
									setExpandMap(!expandMap);
									expandMapHandler();
								}}
							>
								<ArrowUp />
							</div>
							<Map
								googleMapURL={
									'https://maps.googleapis.com/maps/api/js?key=' +
									googleMapsApiKey +
									'&libraries=geometry,drawing,places'
								}
								markers={places}
								loadingElement={<div style={{ height: `100%` }} />}
								containerElement={<div style={{ height: '100%' }} />}
								mapElement={<div style={{ height: `100%` }} />}
								defaultCenter={{ lat: -6.149423, lng: 106.897308 }}
								defaultZoom={12}
							/>
						</div>
						<div className='flex-row-style' style={{ justifyContent: 'space-between' }}>
							{detailItems.map((item, index) => (
								<DetailItem key={index} item={item} />
							))}
						</div>
					</div>
					<div style={{ marginTop: 32 }} className={Styles.global}>
						<h4>Total Container</h4>
						<div className='flex-row-style' style={{ width: '100%' }}>
							<Kontainer img={kontainerS} type={"20' GP"} amount='3' />
							<Kontainer img={kontainerL} type={"40' GP"} amount='2' />
						</div>
					</div>
					<div className='wrepper-table truck-table'>
						<div className='middle-component'>
							<SearchInput
								onChange={e => setSearchValue(e.target.value)}
								value={searchValue}
							/>
							<div className='right'>
								<div className='sort-item'>
									<SortIcon />
									<div className='text'>Urutkan</div>
								</div>

								<div>
									<Select
										number={100}
										//   setSelect={setSelect}
										defaultValue={10}
									/>
								</div>
							</div>
						</div>
						<div className={Styles.tableContainer} style={{ overflowX: 'auto' }}>
							<table style={{ width: '2164px' }} className={Styles.table}>
								<tr style={{ textAlign: 'left' }}>
									{tableTitle.map((value, index) => (
										<th className={Styles.th} key={index}>
											<div className='flex-row-style'>
												<div>{value.name}</div>
												{value.iconSort ? (
													<div className={Styles.iconSort}>
														<ArrowUpD active={false} />
														<ArrowDownD active={false} />
													</div>
												) : (
													''
												)}
											</div>
										</th>
									))}
								</tr>
								{posts.slice(indexOfFirstPost, indexOfLastPost).map((item, index) => (
									<tr style={{ fontSize: 14 }}>
										<td style={{ color: '#333333' }}>{item.vendorOrderDetailID}</td>
										<td>{item.driverName}</td>
										<td>{item.vehicleNumber}</td>
										<td>{item.containerTypeDescription}</td>
										<td>{item.containerNumber}</td>
										<td>{item.sealNumber}</td>
										<td>{item.isoCode}</td>
										<td>
											<div className='flex-row-style'>
												<div key={index} className={Styles.detailItem}>
													<div className={Styles.desc} style={{ marginBottom: 10 }}>
														{item.destname1}
													</div>
													<div
														className='flex-row-style'
														style={{
															textAlign: 'center',
															alignItems: 'center',
															fontWeight: 600,
															fontSize: 14,
														}}
													>
														<span
															style={{
																display: 'flex',
																alignItems: 'center',
															}}
														>
															<ClockIcon />{' '}
															{'IN : ' + item.destPoint1In.substring(11, 16)}
														</span>
														<div className={Styles.line}></div>
														<span>{'OUT : ' + item.destPoint1Out.substring(11, 16)}</span>
													</div>
												</div>
											</div>
										</td>
										<td>
											<div className='flex-row-style'>
												<div key={index} className={Styles.detailItem}>
													<div className={Styles.desc} style={{ marginBottom: 10 }}>
														{item.destname2}
													</div>
													<div
														className='flex-row-style'
														style={{
															textAlign: 'center',
															alignItems: 'center',
															fontWeight: 600,
															fontSize: 14,
														}}
													>
														<span
															style={{
																display: 'flex',
																alignItems: 'center',
															}}
														>
															<ClockIcon />{' '}
															{'IN : ' + item.destPoint2In.substring(11, 16)}
														</span>
														<div className={Styles.line}></div>
														<span>{'OUT : ' + item.destPoint2Out.substring(11, 16)}</span>
													</div>
												</div>
											</div>
										</td>
										<td>
											<div className='flex-row-style'>
												<div key={index} className={Styles.detailItem}>
													<div className={Styles.desc} style={{ marginBottom: 10 }}>
														{item.destname3}
													</div>
													<div
														className='flex-row-style'
														style={{
															textAlign: 'center',
															alignItems: 'center',
															fontWeight: 600,
															fontSize: 14,
														}}
													>
														<span
															style={{
																display: 'flex',
																alignItems: 'center',
															}}
														>
															<ClockIcon />{' '}
															{'IN : ' + item.destPoint3In.substring(11, 16)}
														</span>
														<div className={Styles.line}></div>
														<span>{'OUT : ' + item.destPoint3Out.substring(11, 16)}</span>
													</div>
												</div>
											</div>
										</td>
										<td>Gate Pass</td>
										<td>
											<div className={Styles.statusBox}>
												<span>{getStatus(item.vendorDetailStatus)}</span>
											</div>
										</td>
									</tr>
								))}
							</table>
						</div>
						<Pagination
							totalRecords={posts.length}
							pageLimit={postsPerPage}
							pageNeighbours={1}
							currentPage={currentPage}
							handleClick={handleClick}
							handleMoveLeft={handleMoveLeft}
							handleMoveRight={handleMoveRight}
						/>
					</div>
				</div>
				// // <!--Start of Tawk.to Script-->
				// <script type="text/javascript">
				// var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
				// (function(){
				// var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
				// s1.async=true;
				// s1.src='https://embed.tawk.to/5f89d405fd4ff5477ea68a80/default';
				// s1.charset='UTF-8';
				// s1.setAttribute('crossorigin','*');
				// s0.parentNode.insertBefore(s1,s0);
				// {'}'})();
				// </script>
				// // <!--End of Tawk.to Script-->
			)}
		</React.Fragment>
	);
};

export default TruckOrderDetail;
