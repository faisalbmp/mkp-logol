import React from 'react';
import Styles from '../truckOrderDetail.module.scss';

const Kontainer = props => {
	return (
		<div className={`flex-row-style ${Styles.kontainer}`}>
			<div style={{ marginRight: 32 }}>
				<div style={{ color: '#868A92', fontSize: 16 }}>{props.type}</div>
				<div style={{ color: '#333333', fontSize: 36, fontWeight: 800 }}>
					{props.amount}
				</div>
			</div>
			<img src={props.img} alt='not found' />
		</div>
	);
};

export default Kontainer;
