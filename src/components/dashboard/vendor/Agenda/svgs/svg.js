import React from 'react';

export const ArrowLeft = () => (
	<svg
		width='6'
		height='10'
		viewBox='0 0 6 10'
		fill='none'
		xmlns='http://www.w3.org/2000/svg'
	>
		<path
			d='M5.25 9.28571L0.75 5L5.25 0.714287'
			stroke='#868A92'
			stroke-linecap='round'
			stroke-linejoin='round'
		/>
	</svg>
);

export const ArrowRight = () => (
	<svg
		width='6'
		height='10'
		viewBox='0 0 6 10'
		fill='none'
		xmlns='http://www.w3.org/2000/svg'
	>
		<g clip-path='url(#clip0)'>
			<path
				d='M0.5 9.57143L5 5.28571L0.5 1'
				stroke='#868A92'
				stroke-linecap='round'
				stroke-linejoin='round'
			/>
		</g>
		<defs>
			<clipPath id='clip0'>
				<rect
					width='6'
					height='10'
					fill='white'
					transform='matrix(-1 0 0 1 6 0)'
				/>
			</clipPath>
		</defs>
	</svg>
);
