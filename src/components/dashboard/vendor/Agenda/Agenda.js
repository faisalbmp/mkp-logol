import React from 'react';
import Styles from './agenda.module.scss';
import kalender from 'assets/images/vendor/imgs/kalender.png';
import { ArrowLeft, ArrowRight } from './svgs/svg';

const Agenda = props => {
	return (
		<div>
			<div className={Styles.header}>
				<div>
					<h3>Agenda Pengiriman</h3>
					<h4 style={{ color: '#004DFF' }}>April, 2020</h4>
				</div>
				<img src={kalender} alt='not found' />
			</div>
			<div className={Styles.dateContainer}>
				<div style={{ display: 'flex', alignItems: 'center', paddingLeft: 14 }}>
					<ArrowLeft />
				</div>
				<div
					className='flex-row-style'
					style={{ alignItems: 'center', margin: 'auto' }}
				>
					<div className={Styles.dateItem}>
						<div className={Styles.tanggal}>10</div>
						<div className={Styles.hari}>Sel</div>
					</div>
					<div className={Styles.dateItem}>
						<div className={Styles.tanggal}>11</div>
						<div className={Styles.hari}>Rab</div>
					</div>
					<div className={`${Styles.dateItem} ${Styles.active}`}>
						<div className={Styles.tanggal} style={{ color: '#004DFF' }}>
							12
						</div>
						<div className={Styles.hari}>Kam</div>
						<div className={Styles.dot}></div>
					</div>
					<div className={Styles.dateItem}>
						<div className={Styles.tanggal}>13</div>
						<div className={Styles.hari}>Jum</div>
						<div className={Styles.dot}></div>
					</div>
					<div className={Styles.dateItem}>
						<div className={Styles.tanggal}>14</div>
						<div className={Styles.hari}>Sab</div>
						<div className={Styles.dot}></div>
					</div>
				</div>
				<div
					style={{ display: 'flex', alignItems: 'center', paddingRight: 14 }}
				>
					<ArrowRight />
				</div>
			</div>
		</div>
	);
};

export default Agenda;
