import React from 'react'

import { SearchInputIcon } from '../icon/iconSvg'
import { composeInitialProps } from 'react-i18next'

export default function SearchInput({ value, onChange }) {
    return (
        <div className="search">
            <input value={value} onChange={onChange} placeholder="Search" onChange = {(e) => onChange(e)} />
            <SearchInputIcon />
        </div>
    )
}
