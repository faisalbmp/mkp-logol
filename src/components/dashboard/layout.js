import React, { useState } from 'react';
import Menu from './menu';
import Header from './header';
import FreightBreadCrumb from './FreightBreadCrumb';
import 'assets/fontello/css/fontello.css';
import { MenuDocument } from '../gatepass/index';
import { useLocation } from 'react-router-dom';

export default function Dashboard({ children, gatePassMenu, hideSideMenu, freightSteps }) {
  const [isMobileMenuClicked, setIsMobileMenuClicked] = useState(false);
	let { pathname } = useLocation();
	let hideLeftPane = pathname !== '/dashboard/e-document/gatePass';
	const renderFreightBreadCrumb = () => {
		if (window.location.pathname.includes('dashboard/add/freight')) {
			return <FreightBreadCrumb title={'Buat Pesanan Kapal'} steps={freightSteps} />;
		}

		return null;
  };
  
  const isContainesSideMenu = () => {
    if (pathname.includes("e-document")) return "has_side_menu"
    return ""
  }

	const renderMenuSideDocument = () => {
		return pathname === '/dashboard/e-document/gatePass' && !hideSideMenu && (
			<MenuDocument activeMenu={gatePassMenu} />
		);
	};

	const renderHeader = () => {
		return (
			<Header
				showLeftPane={hideLeftPane}
				showTimeOnly={hideSideMenu} />
		)
	};

	return (
		<div className={`dashboard ${isContainesSideMenu()} ${isMobileMenuClicked ? 'mobile_menu_clicked' : ''}`}>
      <div className="mobile-menu item-button" onClick={() => setIsMobileMenuClicked(!isMobileMenuClicked)}>
          <div className="button-menu" onClick={() => setIsMobileMenuClicked(!isMobileMenuClicked)}>
              <div className="icon"></div>
          </div>
      </div>
			<Menu />
			{renderMenuSideDocument()}
			<div className='container' onClick={() => setIsMobileMenuClicked(false)}>
				{renderHeader()}
				{renderFreightBreadCrumb()}
				<div className='content'>{children}</div>
			</div>
		</div>
	);
}
