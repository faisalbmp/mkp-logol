import React, {
  useState,
  useEffect,
  useLayoutEffect,
  useCallback,
} from "react";
import { withTranslation } from "react-i18next";
import { useSelector, useDispatch } from "react-redux";

import backgroundFreightImg from "assets/images/home/bg-freight.png";
import backgroundTruckImg from "assets/images/home/background-top-truck.jpg";
import mobileBgTruck from "assets/images/home/mobile-bg-truck.jpg";
import HeaderHome from "components/header/HeaderHome";
import ScheduleBooking from "components/cards/ScheduleBooking";
import Footer from "components/footer";
import { setScrollposition } from "actions";
import { ArrowUp } from "components/icon/iconSvg";
import useWindowDimensions from "utils/windowDimensions";

function LayoutHome({ children, t }) {
  const dispatch = useDispatch();
  const translate = t;

  const { scrollposition, typeBooking } = useSelector((state) => state.home);
  const [color, setColor] = useState("#005EEB");
  const { width } = useWindowDimensions();
  useEffect(() => {}, []);

  const callBack = useCallback(() => {
    dispatch(setScrollposition());
  }, [scrollposition]);

  useLayoutEffect(() => {
    window.addEventListener("scroll", () => callBack());
    return () => {
      window.removeEventListener("scroll", () => callBack());
    };
  }, []);

  return (
    <div className="app-home">
      <div
        className="layout-home"
        style={{
          backgroundImage: `url(${
            typeBooking === "freight"
              ? backgroundFreightImg
              : width <= 600
              ? mobileBgTruck
              : backgroundTruckImg
          })`,
          backgroundPositionY: scrollposition * 0.2 + "px",
        }}
      >
        <HeaderHome />
        <div className="wrepper-top">
          <div className="text-title-bg">{translate("layoutHome.title")}</div>
          <div className="child-text-title">
            {translate("layoutHome.subTitle")}
          </div>
        </div>
      </div>

      <div className="container">
        <ScheduleBooking />
        {children}
        <div
          className={`wrapper-floating-scroll-top ${
            window.scrollY > 100 && "wrapper-floating-scroll-top-active"
          }`}
        >
          <div
            className="grid"
            onClick={() => {
              window.scrollTo(0, 0);
            }}
          >
            <span className="showSpan">Scroll To Top</span>
            <div>
              <div
                className={`floating-scroll-top ${
                  window.scrollY > 100 && "floating-scroll-top-active"
                }`}
                onMouseOver={() => setColor("white")}
                onMouseOut={() => setColor("#005EEB")}
              >
                <ArrowUp color={color} />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div style={{ marginTop: 148 }}>
        <Footer />
      </div>
    </div>
  );
}

export default withTranslation()(LayoutHome);
