import React from 'react';
import MenuItem from './MenuItem';
import MenuButton from 'components/button/MenuButton';
import Menu from './Menu';
import iconLogol from 'assets/images/logolBaru.png';
import indonesia from 'assets/images/svg/indonesia.svg';
import inggris from 'assets/images/svg/united-states.svg';
import { withRouter } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import i18n from 'i18next';

class HamburgerMenu extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			menuOpen: false,
			selectedLang: i18n.language,
		};
	}

	handleMenuClick() {
		this.setState({ menuOpen: !this.state.menuOpen });
	}

	handleLinkClick = item => {
		const { history } = this.props;
		this.setState(
			{
				menuOpen: false,
			},
			() => {
				history.push(item.href);
			}
		);
	};

	render() {
		const { history, t } = this.props;

		const styles = {
			container: {
				position: 'absolute',
				top: 0,
				left: 0,
				zIndex: '15',
				opacity: 0.9,
				display: 'flex',
				alignItems: 'center',
				background: 'transparent',
				width: '100%',
				color: 'white',
			},
			logo: {
				display: 'flex',
				// width: '63px',
				// height: '44px',
				margin: '10px auto 10px 15px',
			},
			body: {
				display: 'flex',
				flexDirection: 'column',
				alignItems: 'center',
				width: '100vw',
				height: '100vh',
				filter: this.state.menuOpen ? 'blur(2px)' : null,
				transition: 'filter 0.5s ease',
			},
		};
		const menu = [
			{ name: 'about', href: '/aboutUs' },
			{ name: 'service', href: '/ourServices/truck' },
			{ name: 'help', href: '/helpCenter' },
			{ name: 'newsAndEvent', href: '/news' },
		];
		const menuItems = menu.map((item, index) => {
			return (
				<MenuItem
					key={index}
					delay={`${index * 0.1}s`}
					onClick={() => {
						this.handleLinkClick(item);
					}}
				>
					{t(`headerMenu.${item.name}`)}
				</MenuItem>
			);
		});

		const onLink = e => {
			if (e === 'register') {
				this.setState(
					{
						menuOpen: false,
					},
					() => {
						this.props.onRegister();
					}
				);
			} else {
				window.open(process.env.REACT_APP_LOGIN, '_blank');
			}
		};

		const keyChanges = {
			en: 'id',
			id: 'en',
		};

		const renderLang = {
			en: () => {
				return (
					<div className='item-drop-down'>
						<img src={inggris} alt='inggris' />
						<div className='text-language'>EN</div>
					</div>
				);
			},
			id: () => {
				return (
					<div className='item-drop-down'>
						<img src={indonesia} alt='indonesia' />
						<div className='text-language'>ID</div>
					</div>
				);
			},
		};

		return (
			<div>
				<div style={styles.container}>
					<div style={styles.logo}>
						<img
							src={iconLogol}
							style={{ width: 63, height: 44 }}
							alt='logo logol'
							onClick={() => history.push('/')}
						/>
					</div>

					<div
						className='item-drop-down-column'
						style={{
							display: this.state.menuOpen ? 'none' : 'flex',
						}}
					>
						<div
							onClick={() => {
								this.setState({ selectedLang: this.state.selectedLang });
							}}
						>
							{renderLang[this.state.selectedLang]()}
						</div>
						<div
							className='open-item-drop-down'
							onClick={() => {
								this.setState({ selectedLang: this.state.selectedLang });
								history.push(
									`${this.props.location.pathname}?lng=${
										keyChanges[this.state.selectedLang]
									}`
								);
								window.location.reload(false);
							}}
						>
							{renderLang[keyChanges[this.state.selectedLang]]()}
						</div>
					</div>

					<button
						style={{
							display: this.state.menuOpen ? 'none' : 'block',
						}}
						className='btn-secondary register outside'
						onClick={() => onLink('register')}
					>
						{t('register')}
					</button>

					<MenuButton
						open={this.state.menuOpen}
						onClick={() => this.handleMenuClick()}
						color='#002985'
					/>
				</div>
				<Menu open={this.state.menuOpen}>
					{menuItems}
					<div className='inline-row'>
						<button
							className='btn-primary login'
							onClick={() => onLink('login')}
							target='_blank'
						>
							{t('login')}
						</button>
						<button
							className='btn-secondary register'
							onClick={() => onLink('register')}
							target='_blank'
						>
							{t('register')}
						</button>
					</div>
				</Menu>
			</div>
		);
	}
}

export default withTranslation()(withRouter(HamburgerMenu));
