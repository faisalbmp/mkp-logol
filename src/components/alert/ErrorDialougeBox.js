import React from 'react'
import WrepperModal from 'components/modal/wrepperModal'

export default function ErrorAlert(props) {
    return (
        <WrepperModal
            show={props.errorVisible}
            onClose={props.onErrorOkPressed}
            width={499}
            height={255}
            style={{ backgroundColor: '#ffffff' }}
        >
            {/* <div className="error-modal"> */}
                <div className="error-modal-content">
                    <div className="error-modal-header">
                        Error
                </div>
                    <div className="error-modal-body">
                        {props.message}
                    </div>
                    <div className="error-modal-btn-container">
                        <button type="submit" className="error-modal-submit-btn" onClick={props.onErrorOkPressed}>OK</button>
                    </div>
                </div>
            {/* </div> */}
        </WrepperModal>
    )
}