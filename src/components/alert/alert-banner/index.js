import React from 'react';
import { TextM } from 'components/text';
import { alertBanner } from 'assets/splitPage.module.scss';

const AlertBanner = ({ type, message }) => {
  return (
    <div className={alertBanner}>
      <TextM color="#EA4B4B">Mohon Cek Kembali Dokumen Anda</TextM>
    </div>
  );
};

export default AlertBanner;
