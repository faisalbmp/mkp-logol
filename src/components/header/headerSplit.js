import React from 'react'
import { useHistory } from 'react-router-dom'

import { header } from 'assets/splitPage.module.scss';
import Logo from 'assets/images/svg/logo-logol.svg'

export default function HeaderSplit() {
    const history = useHistory()
    return (
        <div className={header}>
            <img
                src={Logo}
                style={{ cursor: "pointer" }}
                onClick={() => history.push("/")}
                alt="logo"
            />
        </div>
    )
}