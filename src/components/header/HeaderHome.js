import React, { useState, Fragment, useEffect } from 'react';

import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import { pageview } from 'utils/analytics';

import Signup from '../modal/signup';
import indonesia from 'assets/images/svg/indonesia.svg';
import inggris from 'assets/images/svg/united-states.svg';
import iconLogol from 'assets/images/logolBaru.png';
import HamburgerMenu from '../menu/hamburgerMenu';

import { withTranslation } from 'react-i18next';
import i18n from 'i18next';

const linkList = [
	{ name: 'about', href: '/aboutUs' },
	{ name: 'service', href: '/ourServices/truck' },
	{ name: 'help', href: '/helpCenter' },
	{ name: 'newsAndEvent', href: '/news' },
];

function HeaderHome(props) {
	const { className, activeHeader, t } = props;
	const translate = t;
	let { pathname } = useLocation();
	const history = useHistory();
	const [showSignup, setShowSignup] = useState('');
	const { scrollposition } = useSelector(state => state.home);
	const [showSelectLang, setShowSelectLang] = useState(0);
	const selectedLang = i18n.language;

	const publicUrl = process.env.PUBLIC_URL;

	const onLink = e => {
		if (e === 'register') window.open(process.env.REACT_APP_REGISTER_USER, '_blank');
		window.open(process.env.REACT_APP_LOGIN, '_blank');
	};

	useEffect(() => {
		window.scrollTo(0, 0);
	}, []);

	useEffect(() => {
		pageview();
	}, [window.location.pathname]);

	const keyChanges = {
		en: 'id',
		id: 'en',
	};

	const renderLang = {
		en: () => {
			return (
				<div className='item-drop-down'>
					<img src={inggris} />
					<div className='text-language'>EN</div>
				</div>
			);
		},
		id: () => {
			return (
				<div className='item-drop-down'>
					<img src={indonesia} />
					<div className='text-language'>ID</div>
				</div>
			);
		},
	};
	return (
		<Fragment>
			<div className='humberger-menu'>
				<HamburgerMenu
					onRegister={() => {
						onLink('register');
						// setShowSignup("register")
					}}
				/>
			</div>
			<header
				className={`header-home ${
					scrollposition > 1 || activeHeader ? 'header-active' : ''
				} ${className}`}
			>
				<div className='container-header-home'>
					<div>
						<img
							style={{ width: 63, height: 44 }}
							src={iconLogol}
							alt='logo logol'
							onClick={() => history.push('/')}
						/>
					</div>

					<div className='menu-home'>
						{linkList.map((value, index) => (
							<div
								className='text-header'
								key={index}
								style={{ color: value.href === pathname && '#004dff' }}
								onClick={() => history.push(value.href)}
							>
								{translate(`headerMenu.${value.name}`)}
							</div>
						))}
						<div className='item-drop-down-column'>
							<div
								onClick={() => {
									setShowSelectLang(prevState => !prevState);
								}}
							>
								{renderLang[selectedLang]()}
							</div>
							<div
								className='open-item-drop-down'
								onClick={() => {
									setShowSelectLang(prevState => !prevState);
									history.push(`${publicUrl + pathname}?lng=${keyChanges[selectedLang]}`);
									window.location.reload(false);
								}}
							>
								{renderLang[keyChanges[selectedLang]]()}
							</div>
						</div>

						{/* <button className="btn-primary login" onClick={() => setShowSignup("login")}>Login</button>
                        <button className="btn-secondary register" onClick={() => setShowSignup("register")}>Daftar</button> */}

						<button
							className='btn-primary login'
							onClick={() => onLink('login')}
							target='_blank'
						>
							{translate('login')}
						</button>
						<button
							className='btn-secondary register'
							onClick={() => setShowSignup('register')}
						>
							{translate('register')}
						</button>
						{/* <button className="btn-secondary register" onClick={() => onLink("register")} target="_blank">Daftar</button> */}
					</div>
				</div>

				<Signup
					show={showSignup === 'register'}
					Title={() => (
						<Fragment>
							<div>{translate('headerMenu.titleRegisterModal')}</div>
							<div>{translate('headerMenu.subTitleRegisterModal')}</div>
							{/* <div> Sign up for free</div>
                            <div>and start shipping today</div> */}
						</Fragment>
					)}
					typeAuth='signUp'
					onClose={() => setShowSignup('')}
					onShow={() => setShowSignup('register')}
				/>

				<Signup
					show={showSignup === 'login'}
					Title={() => (
						<Fragment>
							<div>{translate('headerMenu.titleLoginModal')}</div>
						</Fragment>
					)}
					typeAuth='signIn'
					onClose={() => setShowSignup('')}
					onShow={() => setShowSignup('login')}
				/>
			</header>
		</Fragment>
	);
}

export default withTranslation()(HeaderHome);

HeaderHome.defaultProps = {
	className: '',
};

HeaderHome.propTypes = {
	className: PropTypes.string,
};
