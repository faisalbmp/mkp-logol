import React, { useState, useRef } from 'react'

import { customSelect, customSelectWrapper, customSelectTrigger, textSelect, open, customOptions } from 'assets/scss/select/selectDetail.module.scss'
import { ArrowDown } from 'components/icon/iconSvg'

export default function SelectDetail(props) {
    const { width, title, children } = props
    const ref = useRef()
    const [active, setActive] = useState(false)
    window.onclick = function (event) {
        if (event.target.id) {
            setActive(false)
        }
    }

    return (
        <div
            className={`${customSelectWrapper}`}
            id="selectId"
            onClick={() => setActive(!active)}
            style={{ width }}
            ref={ref}
            name={"name"}
        >
            <div className={`${customSelect}`}>
                <div className={`${customSelectTrigger}`}><span className={textSelect}>{title}</span>
                    <ArrowDown active={active} />
                </div>
                <div className={`${customOptions} ${active ? open : ""}`}>
                    {children}
                </div>
            </div>
        </div>
    )
}