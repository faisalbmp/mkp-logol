import React, { useState, useRef } from 'react'

import { customSelect, customSelectWrapper, customSelectTrigger, customOptions, customOption, selected, open, textSelect } from 'assets/scss/select/outline.module.scss'
import { ArrowDown } from 'components/icon/iconSvg'

export default function OutlineSelect({ option, defaultValue, width, value, name, onSelect }) {
    const ref = useRef()
    const [active, setActive] = useState(false)
    window.onclick = function (event) {
        if (event.target.id) {
            setActive(false)
        }
    }

    return (
        <div
            className={`${customSelectWrapper} ${active && option.length ? open : ""}`}
            id="selectId"
            onClick={() => setActive(!active)}
            style={{ width }}
            ref={ref}
            name={name}
        >
            <div className={`${customSelect}`}>
                <div className={customSelectTrigger}><span className={textSelect}>{value ? value : defaultValue}</span>
                    <ArrowDown />
                </div>
                <div className={customOptions}>
                    {
                        option.map((value, index) => (
                            <option
                                key={index}
                                className={`${customOption} ${selected}`}
                                name={name}
                                value={value.nama}
                                id={value.id}
                                onClick={() => onSelect(name, value)}
                            >{value.nama}</option>
                        ))
                    }
                </div>
            </div>
        </div>
    )
}
