import React from 'react'

import { wpBtn, activeBtn, onActive, button } from 'assets/scss/select/selectRoundBig.module.scss'

export default function selectRoundBig({ active, onClick, rightType, leftType, rightText, leftText }) {
    return (
        <div className={wpBtn}>
            <div className={`${button} ${active === leftType ? activeBtn : onActive}`} onClick={() => onClick(leftType)}>
                {leftText || ""}
            </div>
            <div className={`${button} ${active === rightType ? activeBtn : onActive}`} onClick={() => onClick(rightType)}>
                {rightText || ""}
            </div>
        </div>
    )
}