import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Styles from 'assets/scss/selectbox/selectbox.module.scss';
import { TextM } from 'components/text';
import { Colors } from 'configs';
import { CheckBoxS } from 'components/checkBox';
import RadioButton from 'components/input/radioButton/radioButton';

const SelectBoxM = ({ label, items, width, isRoundCheckBox }) => {
	const [active, setActive] = useState(false);
	return (
		<div>
			<div
				className={`${Styles.selectBoxM} ${active ? `${Styles.active}` : ''}`}
				onClick={() => setActive(!active)}
			>
				<TextM
					style={{ fontSize: '14px' }}
					color={Colors.text.darkgrey}
					className={Styles.label}
				>
					{label}
				</TextM>
				<i className={`icon-arrow-down ${Styles.arrowDown}`}></i>
			</div>

			{active && items ? (
				<div className={Styles.checkBox} style={{ width: width }}>
					{items.map((item, index) => {
						if (isRoundCheckBox) {
							return <RadioButton key={index} title={item.title} />;
						}
						return (
							<label
								key={index}
								htmlFor={index}
								style={{
									display: 'flex',
									flexFlow: 'row',
								}}
							>
								<CheckBoxS />
								{item.title}
							</label>
						);
					})}
				</div>
			) : null}
		</div>
	);
};

export default SelectBoxM;

SelectBoxM.propTypes = {
	label: PropTypes.string.isRequired,
};
