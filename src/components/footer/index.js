import React from 'react';
import instagramImg from 'assets/images/icons/instagram.svg';
// import twitterImg from 'assets/images/icons/twitter.svg'
// import facebookImg from 'assets/images/icons/facebook.svg'
import linkedinImg from 'assets/images/icons/linkedin.svg';
import whatsappImg from 'assets/images/icons/whatsapp.svg';
import emailImg from 'assets/images/icons/email.svg';
import logolFooterImg from 'assets/images/logolBaru.png';
import { useHistory } from 'react-router-dom';
import moment from 'moment';

import { withTranslation } from 'react-i18next';

const dataTable = [
	[
		{ name: 'Cara Memesan', href: '/' },
		{ name: 'Logistik Laut', href: '/ourServices' },
		{ name: 'Blog', href: '/' },
	],
	[
		{ name: 'Tentang Logol', href: '/aboutUs' },
		{ name: 'Logistik Darat', href: '/ourServices' },
		{ name: 'Privacy Policy', href: '/' },
	],
	[
		{ name: 'Karir', href: '/career' },
		{ name: 'Gudang', href: '/' },
		{ name: 'Terms and Conditions', href: '/' },
	],
	[
		{ name: 'Kontak Kami', href: '/' },
		{ name: 'Dokumen', href: '/' },
		{ name: 'Frequently Asked Question (FAQ)', href: '/' },
	],
	[
		{ name: 'Pusat Bantuan', href: '/helpCenter' },
		{ name: 'Dokumen', href: '/' },
		{ name: 'Register Your NIK Services', href: '/' },
	],
];

function Index({ t }) {
	const translate = t;
	const history = useHistory();

	const routeUrl = async href => {
		if (href === '/') window.location.replace(href);
		history.push(href);
	};

	const onClickLink = link => {
		window.open(link, '_blank');
	};

	const linkedinLink = 'https://www.linkedin.com/company/pt-logol-jakarta-mitraindo/';
	const whattAppLink = 'mailto:inquiry@logol.co.id ';
	const instagramLink = 'https://www.instagram.com/logol.id/';

	return (
		<footer>
			<div className='wrepper-top-footer'>
				<div className='content-left'>
					<img
						src={logolFooterImg}
						alt='icon-logol'
						style={{ cursor: 'pointer', width: 102, height: 78 }}
						onClick={() => history.push('/')}
					/>

					<div className='text-logol'>PT Logol Jakarta Mitraindo</div>
					<div className='text-address'>
						Millennium Centennial Center, Level 38. Jl. Jendral Sudirman Kav.25, Jakarta
						Selatan - 12920
					</div>

					{/* <div className="text-address">Jl. Raya Gading Kirana, RT.18/RW.8, Klp.</div>
                    <div className="text-address">Gading Bar., Kec. Klp. Gading, Kota Jkt Utara,</div>
                    <div className="text-address">Daerah Khusus Ibukota Jakarta 14240</div> */}
				</div>
				<div className='content-r-show'>
					<div className='content-right inline-style'>
						<div className='tr-table'>
							<div className='th-table title-table'>
								{translate('footer.endorsement.title')} <div className='text-line'></div>
							</div>
						</div>
						<div className='tr-table' style={{ flexDirection: 'column' }}>
							<div className='th-table' onClick={() => routeUrl('/partner')}>
								{translate('footer.endorsement.becomePartner')}
							</div>
							<div className='th-table' onClick={() => routeUrl('/aboutUs')}>
								{translate('footer.endorsement.about')}
							</div>
							{/* <div className="th-table" onClick={() => routeUrl("/helpCenter")} >Kontak Kami</div> */}
							<div className='th-table' onClick={() => routeUrl('/helpCenter')}>
								{translate('footer.endorsement.help')}
							</div>
						</div>
					</div>
					<div className='content-right inline-style'>
						<div className='tr-table'>
							<div className='th-table title-table'>
								{translate('footer.product.title')} <div className='text-line'></div>
							</div>
						</div>
						<div className='tr-table' style={{ flexDirection: 'column' }}>
							<div className='th-table' onClick={() => routeUrl('/ourServices/freight')}>
								{translate('footer.product.marine')}
							</div>
							<div className='th-table' onClick={() => routeUrl('/ourServices/truck')}>
								{translate('footer.product.inland')}
							</div>
							<div className='th-table' onClick={() => routeUrl('/e-document')}>
								{translate('footer.product.e-document')}
							</div>
							{/* <div className="th-table" onClick={() => routeUrl("/")} >Dokumen</div> */}
						</div>
					</div>
					<div className='content-right inline-style'>
						<div className='tr-table'>
							<div className='th-table title-table'>
								{translate('company')} <div className='text-line'></div>
							</div>
						</div>
						<div className='tr-table' style={{ flexDirection: 'column' }}>
							{/* <div className="th-table" onClick={() => routeUrl("/")} >Blog {"&"} Newsroom</div> */}
							{/* <div className="th-table" onClick={() => routeUrl("/career")} >Karir</div> */}
							<div className='th-table' onClick={() => routeUrl('/termsAndConditions')}>
								{translate('terms&Condition')}
							</div>
							{/* <div className="th-table" onClick={() => routeUrl("/")} >Kebijakan Privasi</div> */}
						</div>
					</div>
				</div>
			</div>

			<div className='wrepper-bottom-footer'>
				<div className='content-left'>
					<div className='wrepper-icon'>
						<div className='card-icon'>
							<img
								src={linkedinImg}
								alt='linkedin-img'
								target='_blank'
								onClick={() => onClickLink(linkedinLink)}
							/>
						</div>
						<div className='card-icon'>
							<img
								src={instagramImg}
								alt='instagram-img'
								target='_blank'
								onClick={() => onClickLink(instagramLink)}
							/>
						</div>
						<div className='card-icon'>
							<img
								src={emailImg}
								alt='whatsapp-img'
								target='_blank'
								onClick={() => onClickLink(whattAppLink)}
							/>
						</div>
					</div>
				</div>
				<div className='content-right'>
					{' '}
					Copyright © {moment().format('YYYY')}. PT Logol Jakarta Mitraindo
				</div>
			</div>
		</footer>
	);
}

export default withTranslation()(Index);
