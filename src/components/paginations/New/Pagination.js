import React, { useState } from "react";

const Pagination = (props) => {
  const {
    totalRecords,
    pageLimit,
    pageNeighbours,
    handleClick,
    currentPage,
    handleMoveLeft,
    handleMoveRight,
  } = props;
  const totalPages = Math.ceil(totalRecords / pageLimit);
  // const [currentPage, setCurrentPage] = useState(1);

  // pageLimit = typeof pageLimit === 'number' ? pageLimit : 30;
  // totalRecords = typeof totalRecords === 'number' ? totalRecords : 0;
  // pageNeighbours = typeof pageNeighbours === 'number' ? Math.max(0, Math.min(pageNeighbours, 2)) : 0;
  // totalP

  const LEFT_PAGE = "LEFT";
  const RIGHT_PAGE = "RIGHT";

  const range = (from, to, step = 1) => {
    let i = from;
    const range = [];

    while (i <= to) {
      range.push(i);
      i += step;
    }
    return range;
  };

  const fetchPageNumbers = () => {
    const totalNumbers = pageNeighbours * 2 + 3;
    const totalBlocks = totalNumbers + 2;

    if (totalPages > totalBlocks) {
      const startPage = Math.max(2, currentPage - pageNeighbours);
      const endPage = Math.min(totalPages - 1, currentPage + pageNeighbours);
      let pages = range(startPage, endPage);

      const hasLeftSpill = startPage > 2;
      const hasRightSpill = totalPages - endPage > 1;
      const spillOffset = totalNumbers - (pages.length + 1);

      switch (true) {
        case hasLeftSpill && !hasRightSpill: {
          const extraPages = range(startPage - spillOffset, startPage - 1);
          pages = [...extraPages, ...pages];
          break;
        }
        case !hasLeftSpill && hasRightSpill: {
          const extraPages = range(endPage + 1, endPage + spillOffset);
          pages = [...pages, ...extraPages];
          break;
        }
        case hasLeftSpill && hasRightSpill:
        default: {
          pages = [...pages];
          break;
        }
      }
      return [1, ...pages, totalPages];
    }
    return range(1, totalPages);
  };

  const pages = fetchPageNumbers();
  return (
    <div className="pagination-container">
      <ul className="pagination">
        <li className="pagination-prev-text">
          <a
            onClick={() => handleMoveLeft()}
            className={`pagination-prev-text ${
              currentPage <= 1 ? "cursor-disable" : ""
            }`}
          >
            {"Previous"}
          </a>
        </li>

        {pages.map((page, index) => {
          let className1 = "page-item ";
          let className2 = "page-link ";
          if (page === currentPage) {
            className1 += "page-item-active";
            className2 += "page-item-active";
          }

          return (
            <li
              key={index}
              className={className1}
              onClick={() => {
                handleClick(page);
              }}
            >
              <a className={className2}>{page}</a>
            </li>
          );
        })}
        <li className="pagination-prev-text">
          <a
            onClick={() => handleMoveRight(totalPages)}
            className={`pagination-prev-text ${
              currentPage === totalPages ? "cursor-disable" : ""
            }`}
          >
            Next
          </a>
        </li>
      </ul>
    </div>
  );
};

export default Pagination;
