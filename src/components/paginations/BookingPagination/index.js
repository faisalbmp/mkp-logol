import React from 'react';
import Styles from 'assets/scss/pagination/bookingpagination.module.scss';
import { TextM } from 'components/text';
import { Colors } from 'configs';
import { ButtonM } from 'components/button';

const BookingPagination = ({ t }) => {
	const pages = 5;

	const renderNumberItem = number => {
		return (
			<div className={`${Styles.numberItem} ${number === 1 ? Styles.active : ''}`}>
				<TextM color={Colors.text.grey1}>{number}</TextM>
			</div>
		);
	};
	const renderNumbers = () => {
		let numbers = [];
		for (let i = 0; i < pages; i++) {
			numbers.push(i + 1);
		}
		return (
			<div className={Styles.numbers}>
				{numbers.map(number => renderNumberItem(number))}
			</div>
		);
	};

	const renderNext = () => {
		return <ButtonM className={Styles.buttonNext}>{t('vSchedules.next')}</ButtonM>;
	};
	return (
		<div className={Styles.container}>
			{renderNumbers()}
			{renderNext()}
		</div>
	);
};

export default BookingPagination;
