import React, { Component } from 'react';

export default function Pagination({
	postsPerPage,
	totalPosts,
	paginate,
	paginatePrevious,
	paginateNext,
	currentPage,
}) {
	const pageNumbers = [];

	for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
		pageNumbers.push(i);
	}

	return (
		<div className='pagination-container'>
			<ul className='pagination'>
				<li className='pagination-prev-text'>
					<a
						onClick={currentPage === 1 ? '' : () => paginatePrevious()}
						className={`pagination-prev-text ${
							currentPage <= 1 ? 'cursor-disable' : ''
						}`}
					>
						{'Previous'}
					</a>
				</li>

				{pageNumbers.map(number => {
					let className1 = 'page-item ';
					let className2 = 'page-link ';
					if (number === currentPage) {
						className1 += 'page-item-active';
						className2 += 'page-item-active';
					}

					return (
						<li
							key={number}
							className={className1}
							onClick={() => paginate(number)}
						>
							<a className={className2}>{number}</a>
						</li>
					);
				})}

				<li className='pagination-next-text'>
					<a
						onClick={
							currentPage === pageNumbers.length ? '' : () => paginateNext()
						}
						className={`pagination-next-text ${
							currentPage === pageNumbers.length ? 'cursor-disable' : ''
						}`}
					>
						{'Next'}
					</a>
				</li>
			</ul>
		</div>
	);
}
