import React, { useState, useEffect }  from 'react'
import InputText from 'components/input/inputTextT'
import PortPanel from "./PortPanel"
const RightPanel = ({selectedLeftMenu, openPickupModal, storageData, searchPort, searchFactory, searchStorage, storageSelected, factoryData, factorySelected, searchDepo, depoSelected, depodata, portData, isItemSelected, setIsItemSelected, setSelectedRightItemTitle, setSelectedRightAddress, setselectedRightItemDistrictId, setSelectedRightItemId}) => {   
    switch (selectedLeftMenu) {
        case "Storage":
          return (
            <React.Fragment>
              <div className='header-input'>
                <InputText
                  placeholder='Cari Storage'
                  inputTextStyle={{ width: '100%', height: 45 }}
                  onChange = {searchStorage}
                />
              </div>
              <div className='mega-list-single-container'>
                {storageData.map((item, key) => (
                  <div className='mega-list-single-item' key={key}>
                    <div className='mega-child-list'>{item.name}</div>
                    <div className='mega-child-list'>{item.address}</div>
                    <div className='mega-child-list'>
                      <button className='btn-secondary' onClick={() => storageSelected(item)}>Pilih</button>
                    </div>
                  </div>
                ))}
              </div>
            </React.Fragment>
          )
        case "Factory":
          return (
            <React.Fragment>
              <div className='header-input'>
                <InputText
                  placeholder='Cari Factory'
                  inputTextStyle={{ width: '100%', height: 45 }}
                  onChange = {searchFactory}
                />
              </div>
              <div className='mega-list-single-container'>
                {factoryData.map((item, key) => (
                  <div className='mega-list-single-item' key={key}>
                    <div className='mega-child-list'>{item.name}</div>
                    <div className='mega-child-list'>{item.address}</div>
                    <div className='mega-child-list'>
                      <button className='btn-secondary' onClick={() => factorySelected(item)}>Pilih</button>
                    </div>
                  </div>
                ))}
  
                <div className='mega-bottom-list'>
                  <div onClick={openPickupModal}>Tambah alamat factory baru</div>
                </div>
              </div>
            </React.Fragment>
          )
          case "Depo":
          return (
            <React.Fragment>
              <div>
                <div className='header-input'>
                  <InputText
                    placeholder='Search Depo'
                    inputTextStyle={{ width: '100%', height: 45, bottom:10 }}
                    onChange = {searchDepo}
                  />
                </div>
                <div style = {{height:'200px', overflowY: "scroll"}}>
                  {depodata.map((item, key) => (
                    <React.Fragment>
                      <div key={key} className='mega-subitem-container'>
                        <div className='mega-list-item' onClick={() => depoSelected(item)}>
                          <span className='mega-child-list'>{item.name}</span>
                        </div>
                    </div>
                    </React.Fragment>
                  ))}
                </div>
              </div>
            </React.Fragment>
          )
          case "Port" :
            return <PortPanel
              data={portData}
              isItemSelected={isItemSelected}
              setIsItemSelected={setIsItemSelected}
              setSelectedRightItemTitle={setSelectedRightItemTitle}
              setSelectedRightItemId={setSelectedRightItemId}
              setSelectedRightAddress={setSelectedRightAddress}
              setselectedRightItemDistrictId={setselectedRightItemDistrictId}
              searchPort={searchPort}
            />
        default:
          return (
            <React.Fragment>
                <React.Fragment>
                  <div className='mega-subitem-container'>
                    <div className='mega-list-item'>
                      <span className='mega-child-list'>{'Not Required'}</span>
                    </div>
                </div>
                </React.Fragment>
            </React.Fragment>
          )
      }
}

export default RightPanel