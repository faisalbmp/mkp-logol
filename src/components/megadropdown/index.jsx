import React, { useState, useEffect } from 'react'
import WrDropDown from 'components/button/wrDropDown'
import { RightArrowIcon, Close } from 'components/icon/iconSvg'
import RadioButton from 'components/button/radio-button'
import InputText from 'components/input/inputTextT'
import PortPanel from './PortPanel'
import RightPanel from "./RightPanel"
import { selectListContainer, showSelect, showFlex } from 'assets/scss/button/wrDropDown.module.scss'

const MegaDropdown = (props) => {
  const [selectedLeftMenu, setSelectedLeftMenu] = useState(props.selectedMenu)
  // const [error, setError] = useState({liftOnNameError: "", liftOnPhoneError: "",  liftOffNameError : "", liftOffPhoneError : "",})
  const [formError, setFormError] = useState({liftOnNameError: "", liftOnPhoneError: "",  liftOffNameError : "", liftOffPhoneError : ""})
  const [selectedRightTitle, setSelectedRightTitle] = useState(props.selectedMenuTitle)
  const [selectedRightItemTitle, setSelectedRightItemTitle] = useState('')
  const [selectedRightItemId, setSelectedRightItemId] = useState('')
  const [selectedRightItemDistrictId, setselectedRightItemDistrictId] = useState('')
  const [selectedRightAddress, setSelectedRightAddress] = useState('')
  const [handlingTitle, sethandlingTitle] = useState('')
  const [isItemSelected, setIsItemSelected] = useState(false)
  const [byLogol, setByLogol] = useState(true)
  const [byItself, setByItself] = useState(false)
  const [liftOn, setLiftOn] = useState({isLiftOn : false, liftOnName : "", liftOnPhone : ""})
  const [liftOff, setLiftOff] = useState({isLiftOff : false, liftOffName : "", liftOffPhone : ""})
  
  const selectMenu = (key, title) => {
    setIsItemSelected(false)
    setSelectedLeftMenu(key)
    setSelectedRightTitle(title)
  }

  useEffect(() => {
    setSelectedLeftMenu(props.selectedMenu)
    setSelectedRightTitle(props.selectedMenuTitle)
  }, [props.selectedMenu, props.selectedMenuTitle]);


  const backToItemList = () => {
    setIsItemSelected(!isItemSelected)
  }

  const RadioLabel = ({ title, description }) => (
    <div className='radio-label-container'>
      <span className='label-title'>{title}</span>
      <span className='label-note'>{description}</span>
    </div>
  )

  const onLogolEGatePassClicked = () => {
    setByLogol(!byLogol)
    if(!byLogol == false) {
      setByItself(true)
    } else {
      setByItself(false)
    }
  }

  const byItselfPressed = () => {
    setByItself(!byItself)
    if(!byItself == false) {
      setByLogol(true)
    } else {
      setByLogol(false)
    }
  }

  const factorySelected = (item) => {
    setIsItemSelected(!isItemSelected)
    setSelectedRightItemTitle(item.name)
    setSelectedRightAddress(item.address)
    setIsItemSelected(false)
    var data = {
      "name" : item.name,
      "address" : item.address,
      "type" : selectedLeftMenu,
      "liftOn" : liftOn.isLiftOn,
      "liftOnName": liftOn.liftOnName,
      "liftOnPhone" : liftOn.liftOnPhone,
      "liftOff" : liftOff.isLiftOff,
      "liftOffName": liftOff.liftOffName,
      "liftOffPhone" : liftOff.liftOffPhone,
      "id" : item.factoryID,
      "districtID" : item.districtID
    }
    props.dataSelected(data)
  }

  const depoSelected = (item) => {
    setIsItemSelected(!isItemSelected)
    setSelectedRightItemTitle(item.name)
    setSelectedRightAddress(item.address)
    setSelectedRightItemId(item.depoID)
    setselectedRightItemDistrictId(item.districtID)
  }

  const storageSelected = (item) => {
    setIsItemSelected(!isItemSelected)
    setSelectedRightItemTitle(item.name)
    setSelectedRightAddress(item.address)
    setIsItemSelected(false)
    var data = {
      "name" : item.name,
      "address" : item.address,
      "type" : selectedLeftMenu,
      "liftOn" : liftOn.isLiftOn,
      "liftOnName": liftOn.liftOnName,
      "liftOnPhone" : liftOn.liftOnPhone,
      "liftOff" : liftOff.isLiftOff,
      "liftOffName": liftOff.liftOffName,
      "liftOffPhone" : liftOff.liftOffPhone,
      "id" : item.storageID,
      "districtID" : ""
    }
    if((props.destination2 || props.destination3)) {
      data.districtID = item.districtID
    }
    props.dataSelected(data)
  }

  const dataSelected = () => {
    var error = {}
    if(liftOn.liftOnName == "" && (selectedLeftMenu === "Depo" || selectedLeftMenu === "Port") && !byLogol && props.destination1) {
      error.liftOnNameError = "This Field Is required"
      setFormError(error)
      return
    }
    if(liftOn.liftOnPhone == "" && (selectedLeftMenu === "Depo" || selectedLeftMenu === "Port") && !byLogol && props.destination1) {
      error.liftOnPhoneError = "This Field Is required"
      setFormError(error)
      return
    }
    if(liftOn.liftOnName == "" && liftOn.liftOnPhone == "" && (selectedLeftMenu === "Depo" || selectedLeftMenu === "Port") && !byLogol && props.destination1) {
      error.liftOnNameError = "This Field Is required"
      error.liftOnPhoneError = "This Field Is required"
      setFormError(error)
      return;
    }

    if(liftOff.liftOffName == "" && (props.destination2 || props.destination3) && (selectedLeftMenu === "Depo" || selectedLeftMenu === "Port") && !byLogol) {
      error.liftOffNameError = "This Field Is required"
      setFormError(error)
      return
    }
    if(liftOff.liftOffPhone == "" && (props.destination2 || props.destination3) && (selectedLeftMenu === "Depo" || selectedLeftMenu === "Port") && !byLogol) {
      error.liftOffPhoneError = "This Field Is required"
      setFormError(error)
      return
    }

    if(liftOff.liftOffName == "" && liftOff.liftOffPhone == "" && (props.destination2 || props.destination3) && (selectedLeftMenu === "Depo" || selectedLeftMenu === "Port") && !byLogol) {
      error.liftOffNameError = "This Field Is required"
      error.liftOffPhoneError = "This Field Is required"
      setFormError(error)
      return;
    }
    setIsItemSelected(false)
    var data = {
      "name" : selectedRightItemTitle,
      "address" : selectedRightAddress,
      "type" : selectedLeftMenu,
      "liftOn" : "",
      "liftOnName": liftOn.liftOnName,
      "liftOnPhone" : liftOn.liftOnPhone,
      "liftOff" : "",
      "liftOffName": liftOff.liftOffName,
      "liftOffPhone" : liftOff.liftOffPhone,
      "id" : selectedRightItemId,
      "districtID" : ''
    }
    if(props.destination1 && (selectedLeftMenu === "Depo" || selectedLeftMenu === "Port") && byLogol) {
      data.liftOn = true
    } else {
      data.liftOn = false
    }
    if((props.destination2 || props.destination3) && (selectedLeftMenu === "Depo" || selectedLeftMenu === "Port") && byLogol) {
      data.liftOff = true
    } else {
      data.liftOff = false
    }
    if((props.destination2 || props.destination3) && (selectedLeftMenu === "Depo" || selectedLeftMenu === "Port")) {
      data.districtID = selectedRightItemDistrictId
    }
    props.dataSelected(data)
  }

  const liftOnNameChange = (e) => {
    var error = {}
    if(props.destination1) {
      liftOn.liftOnName = e.target.value
      error.liftOnNameError = ""
      setFormError(error)
    } else {
      liftOff.liftOffName = e.target.value
      error.liftOffNameError = ""
      setFormError(error)
    }
  }

  const liftOnPhoneChange = (e) => {
    var error = {}
    if(props.destination1) {
      liftOn.liftOnPhone = e.target.value
      error.liftOnPhoneError = ""
      setFormError(error)
    } else {
      liftOff.liftOffPhone = e.target.value
      error.liftOffPhoneError = ""
      setFormError(error)
    }
  }

  return (
    <WrDropDown
      show={props.openList}
      title={props.placeholder}
      onClick={() => props.openSelectList(!props.openList)}
      onClose={() => props.onCloseDropDown()}
      style={props.headerStyle}
      label={props.label}
      label={props.label}
      destination1={props.destination1}
      destination2={props.destination2}
      destination3={props.destination3}
    >
      <div className = {`${props.openList ? showFlex : ""} modal-drop-xy `}>
        <div className={`${selectListContainer}`} >
          <div className='mega-dropdown-container'>
            <div className='mega-dropdown-left'>
              {props.leftData.map(item => (
              <div key={item.key}
                  className={`mega-button-list ${selectedLeftMenu === item.key ? 'selected' : ''}`}
                  onClick={() => selectMenu(item.key, item.title)}
                >
                  <span>{item.title}</span>
                </div>
              ))}
            </div>
            <div className={`mega-dropdown-right ${isItemSelected ? 'finished' : ''}`}>
              <div className='mega-header'>
                {!isItemSelected ? (
                  <span>{selectedRightTitle}</span>
                ) : (
                  <div className='mega-header-container'>
                    <div className='mega-header-left' onClick={() => backToItemList()}>
                      <RightArrowIcon style={{ marginRight: 20, transform: "rotate(180deg)" }} color="#333" />
                    </div>
                    <div className='mega-header-right'>
                      <span>{selectedRightItemTitle}</span>
                      <span className='note'>{selectedRightAddress}</span>
                    </div>
                  </div>
                )}
              </div>
              <div className='mega-container'>
                <div className = "close-modal-btn" onClick = {() => props.onCloseDropDown()}>
                  <div className = "close-btn">
                    <Close/>
                  </div>
                </div>
                {!isItemSelected ? 
                <RightPanel 
                  selectedLeftMenu = {selectedLeftMenu}
                  storageData = {props.storageData}
                  searchStorage = {props.searchStorage}
                  storageSelected = {storageSelected}
                  factoryData = {props.factoryData}
                  searchFactory = {props.searchFactory}
                  factorySelected = {factorySelected}
                  searchDepo = {props.searchDepo}
                  depoSelected = {depoSelected}
                  depodata = {props.depodata}
                  portData = {props.portData}
                  searchPort = {props.searchPort}
                  isItemSelected = {isItemSelected}
                  setIsItemSelected = {setIsItemSelected}
                  setSelectedRightItemId = {setSelectedRightItemId}
                  setSelectedRightItemTitle = {setSelectedRightItemTitle}
                  setSelectedRightAddress = {setSelectedRightAddress}
                  setselectedRightItemDistrictId = {setselectedRightItemDistrictId}
                  openPickupModal = {props.openPickupModal}
                /> 
                : (
                  <div className='mega-container-form'>
                    <div className='mega-container-title'>
                      <span className='h6'>{props.destination1 ? 'Lift On' : 'Lift Off'}</span>
                      <span className='h6 note'>- {selectedRightTitle} Handling</span>
                    </div>
                    <div className='mega-container-row'>
                      <div className='mega-row-left'>
                        <RadioButton
                          name='portHandlingByLogol'
                          title={<RadioLabel title='Logol e-Gatepass' description='Proses lift on akan di handle oleh Logol.' />}
                          checked={byLogol}
                          onChange={() => onLogolEGatePassClicked()}
                        />
                      </div>
                      <div className='mega-row-right'>
                        <InputText
                          textInputTitle='Petugas Lift On'
                          inputTextStyle={{ width: '100%', height: 45 }}
                          disabled={byLogol}
                          onChange = {liftOnNameChange}
                        />
                        <p className = "error_msg">{props.destination1 ? formError.liftOnNameError : formError.liftOffNameError}</p>
                      </div>
                    </div>
                    <div className='mega-container-row'>
                      <div className='mega-row-left'>
                        <RadioButton
                          name='portHandlingMyself'
                          title={<RadioLabel title='I will do it my self' description='Proses lift on sepenuhnya di handle oleh Anda.' />}
                          checked={byItself}
                          onChange={() => byItselfPressed()}
                        />
                      </div>
                      <div className='mega-row-right'>
                        <InputText
                          textInputTitle='No. Telepon'
                          inputTextStyle={{ width: '100%', height: 45 }}
                          disabled={byLogol}
                          onChange = {liftOnPhoneChange}
                        />
                        <p className = "error_msg">{props.destination1 ? formError.liftOnPhoneError : formError.liftOffPhoneError}</p>
                      </div>
                    </div>
                    <div className='mega-container-row right'>
                      <button className='btn-secondary' onClick={() => dataSelected()}>Simpan</button>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </WrDropDown>
  )
}

export default MegaDropdown