import React from 'react'
import InputText from 'components/input/inputTextT'
const PortPanel = ({data, isItemSelected, searchPort, setIsItemSelected, setselectedRightItemDistrictId, setSelectedRightItemTitle, setSelectedRightItemId, setSelectedRightAddress}) => {
  const selectItem = (item) => {
    setIsItemSelected(!isItemSelected)
    setSelectedRightItemTitle(item.portUTC)
    setSelectedRightAddress(item.address)
    setselectedRightItemDistrictId(item.districtID)
    setSelectedRightItemId(item.portID)
  }
  // const getSubItem = (item) => {
  //   if (item.subs) {
  //     return (
  //       <div className='mega-subitem-container'>
  //         {item.subs.map((item, key) => (
  //           <div className='mega-list-item' onClick={() => selectItem(item)}>
  //             <span className='mega-child-list'>{item.title}</span>
  //           </div>
  //         ))}
  //       </div>
  //     )
  //   }
  // }

  return (
    <React.Fragment>
      <div className='header-input'>
        <InputText
          placeholder='Search Port'
          inputTextStyle={{ width: '100%', height: 45, bottom:10 }}
          onChange = {searchPort}
        />
      </div>
      {data.map((item, key) => (
        <React.Fragment>
          {/* <div className='mega-list-item' key={key}>
            <span className='mega-parent-list'>{item.portUTC}</span>
          </div> */}
          {/* {getSubItem(item)} */}
          <div key={key} className='mega-subitem-container'>
            <div className='mega-list-item' onClick={() => selectItem(item)}>
              <span className='mega-child-list'>{item.portUTC}</span>
            </div>
        </div>
        </React.Fragment>
      ))}
    </React.Fragment>
  )
}

export default PortPanel