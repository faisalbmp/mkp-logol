import React from 'react';
import Styles from 'assets/scss/text/label.module.scss';
import {Label} from 'components/generics';
import PropTypes from 'prop-types';

const LabelM = (props) => {
  const {
    children,
    textStyle,
    color,
    style,
    className,
  } = props 
  return (
    <Label className={`${Styles.medium} ${className}`} textStyle={textStyle} color={color} style={style}>{children}</Label>
  );
};

export default LabelM;

LabelM.defaultProps = {
  textStyle: "normal",
  color: "black",
  style: {},
  className: null
}

LabelM.propTypes = {
  textStyle: PropTypes.string,
  color: PropTypes.string,
  style: PropTypes.objectOf(String),
  className: PropTypes.string,
}