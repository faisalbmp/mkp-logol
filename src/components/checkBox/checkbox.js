import React, { useState } from 'react'
import { UploadIcon, CheckIcon, DeleteIcon } from "../icon/iconSvg"

export default function Checkbox (props)  {
    return (
        <div className="checkbox">
            <label>
                <input
                    type="checkbox"
                    checked={props.checked}
                    onChange={props.onChange}
                />
                <span>{props.checkboxTitle}</span>
            </label>
        </div>
    )
}