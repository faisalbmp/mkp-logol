import React, { useState } from 'react';
import {
	DropDownArrow,
	DropDownUpArrow,
	SelectSearchIcon,
	FactoryIcon,
} from '../icon/iconSvg';
import WrDropDown from 'components/button/wrDropDown';

export default function SelectBox(props) {
	const [activeIndex, setActiveIndex] = useState(null);
	const selectList = (item, index) => {
		setActiveIndex(index);
		props.selectedList(item, index);
	};

	return (
		<WrDropDown
			show={props.openList}
			title={props.placeholder}
			onClick={() => props.openSelectList(!props.openList)}
			onClose={() => props.onCloseDropDown()}
			style={props.headerStyle}
			label={props.label}
			destination1={props.destination1}
			destination2={props.destination2}
			destination3={props.destination3}
		>
			<ul style={{ height: 222 }}>
				{props.isSearch && (
					<div className='select-box-search'>
						<input
							type='text'
							className='select-box-search-input'
							onChange={props.searchSelectList}
						/>
						<SelectSearchIcon />
					</div>
				)}
				{props.selectList.map((item, index) => {
					let className = 'bg-drop-down-box-value';
					if (index === activeIndex) {
						className += ' selection-class';
					}
					return props.pickup ? (
						<li
							key={index}
							className={className}
							id='pickup-address-list'
							onClick={() => selectList(item, index)}
						>
							<FactoryIcon />
							<div>
								<span>Factory {index + 1} </span>
								{item.factoryName}
							</div>
						</li>
					) : (
						<li
							key={index}
							className='select-box-list-value'
							onClick={() => selectList(item)}
						>
							{props.isSearch || props.isDefault ? item.name : item.portUTC}
						</li>
					);
				})}
				{props.pickup && (
					<div className='bg-drop-down-add-address'>
						<button
							className='bd-drop-down-add-address-button'
							onClick={props.openPickupModal}
						>
							Tambah Alamat
						</button>
					</div>
				)}
			</ul>
		</WrDropDown>
	);
}
