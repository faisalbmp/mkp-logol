import React from "react";
import Styles from "assets/scss/checkbox/checkbox.module.scss";
import { Colors } from "configs";
import { LabelM } from "components/label";

const CheckBoxS = ({ label, onChange, onClick, checked }) => {
  return (
    <div className={Styles.checkBoxS}>
      <input
        type="checkbox"
        className={Styles.checkBoxModule}
        onChange={onChange}
        onClick={onClick}
        checked={checked}
      />
      <div className={Styles.checkBox}>
        <i
          className="icon-ok"
          style={{ fontSize: "8px", color: Colors.text.white }}
        ></i>
      </div>
      <LabelM>{label}</LabelM>
    </div>
  );
};

export default CheckBoxS;
