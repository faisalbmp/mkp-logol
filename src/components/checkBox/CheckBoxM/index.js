import React from 'react';
import Styles from 'assets/scss/checkbox/checkbox.module.scss';
import { Colors } from 'configs';
import { LabelM } from 'components/label';

const CheckBoxM = ({ label, onChange }) => {
	return (
		<div className={Styles.checkBoxM}>
			<input
				type='checkbox'
				className={Styles.checkBoxModule}
				style={{ width: 22, height: 22, border: '5px solid #868A92' }}
				onChange={onChange}
			/>
			<div className={Styles.checkBox}>
				<i
					className='icon-ok'
					style={{ fontSize: '8px', color: Colors.text.white }}
				></i>
			</div>
			<LabelM>{label}</LabelM>
		</div>
	);
};

export default CheckBoxM;
