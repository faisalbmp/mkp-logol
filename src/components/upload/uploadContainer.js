import React, { useState, useEffect } from 'react'
import { FullSunIcon, HalfSunIcon, AddContainerIcon, RemoveContainerIcon, CancelModalIcon, CalendarSmall, LineIcon, ArrowDown, UploadArrowIcon } from "components/icon/iconSvg"
import SelectBox from "components/checkBox/select-box";
import InputText from "components/input/inputText";
import Counter from "components/content/counter"
import moment from 'moment'
import ReactDatePicker, { CalendarContainer } from 'react-datepicker'
import { megamenu, buttonSelect, gridItem, title, lineCenter } from 'assets/scss/megamenu/megamenu.module.scss';
import 'assets/scss/component/react-datepicker.scss'
export default function UploadContainer(props) {
    const [openSelectTime, setOpenSelectTime] = useState(false)
    const setIconSelectListValue = (item) => {
        props.onSelectedWaktuListClicked(item);
        setOpenSelectTime(false);
    }

    const [startDate, setDate] = useState("")

    const ExampleCustomInput = ({ time, startDate, resetPickers, onClick }) => {
        return (
            <div className="input-box-container" >
                <div className="input-text-box" style=
                    {{
                        alignItems: 'center',
                        width: '180px'
                    }}
                    onClick={onClick}>
                    {
                        startDate != "" ?
                            <div>
                                <div>
                                    {moment(startDate).format('DD MMM, yyyy')}
                                </div>
                            </div>

                            : <div style={{ color: '#AFB5BF' }}>Pilih Tanggal</div>
                    }
                </div>
            </div>
        )
    };

    const MyContainer = ({ className, children }) => {
        return (
            <div style={{ marginTop: 21 }}>
                <CalendarContainer className={className}>
                    <div style={{ position: "relative" }}>{children}</div>
                </CalendarContainer>
            </div>
        );
    }

    const dateSelect = (startDate) => {
        setDate(startDate)
        if (props.tanggalDate) {
            props.tanggalDate(startDate)
        }
    }

    return (
        <div>
            <div className="load-good-container-body" style={props.updateContainerStyle} >
                <div className="tanggal">
                    <ReactDatePicker
                        customInput={
                            <ExampleCustomInput
                                time={"Waktu Pengiriman"}
                                startDate={startDate}
                            />
                        }
                        calendarContainer={MyContainer}
                        selected={new Date()}
                        onChange={startDate => dateSelect(startDate)}
                    >
                    </ReactDatePicker>
                </div>
                <div className="waktu">
                    <div className="waktu-select" onClick={props.openIconSelectBoxList} style={{ marginTop: 10 }}>
                        {
                            props.fullIcon ? <FullSunIcon /> : <HalfSunIcon />
                        }
                        <span style={{ marginLeft: 10 }}>
                            {props.IconSelectBoxvalue}
                        </span>
                    </div>
                    {
                        props.selectedWaktu !== ""
                            ? props.selectedWaktu === "Siang" ? <FullSunIcon /> : <HalfSunIcon />
                            : ""
                    }
                    <span style={{ marginLeft: 10 }}>
                        {props.selectedWaktu}
                    </span>
                </div>
                {
                    openSelectTime &&
                    <div className="waktu-select-box-container" >
                        <ul>
                            {
                                props.iconSelectList.map((item, index) => (
                                    <li key={index} className="waktu-select-list-container" onClick={() => setIconSelectListValue(item)}>
                                        {
                                            item.isFullSun
                                                ? <FullSunIcon />
                                                : <HalfSunIcon />
                                        }
                                        <span>{item.name}</span>
                                    </li>
                                ))
                            }

                        </ul>
                    </div>
                }
            </div>
            <div className="kontainer" >
                <SelectBox value={props.selectBoxValue} headerStyle={{ width: 251, height: 52 }} openList={props.openSelectListFlag} isSearch={false} openSelectList={props.onSelectBoxClicked} selectedList={props.onSelectListClicked} selectList={props.listValue.map((v) => v.description)} />
            </div>
            <div className="jumlah">
                <Counter
                    value={props.counterValue}
                    onIncrementClicked={props.incrementCounterClicked}
                    onDecrementClicked={props.decrementCounterClicked}
                    counterContainerStyle={{ marginTop: 10 }} />
            </div>
            <div className="add-container-icon" style={{ marginTop: 10 }}>
                {
                    props.isremoveContainer
                        ? <div onClick={props.removeContainer} ><RemoveContainerIcon /></div>
                        : <div onClick={props.addContainer}><AddContainerIcon /></div>
                }
            </div>
        </div>
    )
}