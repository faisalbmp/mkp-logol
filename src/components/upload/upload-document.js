import React, { useState } from 'react'
import { UploadIcon, CheckIcon, DeleteIcon } from "components/icon/iconSvg"

export default function UploadDocument (props)  {
    let deliveryFileUpload = null;

    const [deliveryDocument, setDeliveryDocument] = useState('Upload Dokumen')
    const [deliveryDocumentUploaded, setdeliveryDocumentUploaded] = useState(false)

    const uploadDeliveryDocument = () => {
        deliveryFileUpload.click();
    }

    const getDeliveryDocument = (e) => {
        if(e.target.files.length > 0) {
            setDeliveryDocument(e.target.files[0].name)
            setdeliveryDocumentUploaded(true)
        }
        props.getDocument(e.target.files)
    }

    const removeDocument = () => {
        setDeliveryDocument("Upload Dokumen")
        setdeliveryDocumentUploaded(false)
        props.getDocument("")
    }
    return (
        <div className = "shipping-instruction-container" >
            <div className = "shipping-instruction-title" >
                <span>{props.title}</span>
            </div>
            <div className = "shipping-instruction-number" >
                <div>
                    <input type = "text" value = {props.docName} placeholder = {props.placeholder}  onChange={e => props.onTextChange(e)}/>
                </div>
            </div>
            <div className = "shipping-instruction-document" >
                <div className = "upload-file-button" onClick = {uploadDeliveryDocument}>
                    <span className = "upload-button-text" >{deliveryDocument}</span>
                    <input ref = {elem => deliveryFileUpload = elem} value = {props.doc} type = "file" style = {{ display: "none" }} onChange = {(e) =>  getDeliveryDocument(e)}/>
                </div>
                    {
                        deliveryDocumentUploaded 
                        ? <span onClick = {removeDocument} ><DeleteIcon/></span>
                        : <span onClick = {uploadDeliveryDocument}><UploadIcon/></span>
                    }
            </div>
            {
                deliveryDocumentUploaded &&
                <div className = "check-icon-container">
                    <CheckIcon color = {"green"} />
                </div>
            }
        </div>
    )
}