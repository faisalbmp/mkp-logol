import React from "react";
import Styles from "assets/scss/megamenu/portpicker2.module.scss";

const PortPicker2 = (props) => {
  const {
    data,
    show
  } = props;

  const onPortClick = port => {
    const { handleSelect, handleClose } = props;
    handleSelect(port);
    handleClose();
  };

  const renderPorts = () => {
    return data.map(list =>
      <div className={Styles.listItem} onClick={() => onPortClick(list)}>
        <div>{list.portName}</div>
        <div className={Styles.countryText}>{list.countryName}</div>
      </div>
    )
  };
  return (
    <div className={`${Styles.picker} ${show ? Styles.show : ""}`}>
      {data.length ? renderPorts() : "Ada Kendala koneksi"}
    </div>
  );
};

export default PortPicker2;
