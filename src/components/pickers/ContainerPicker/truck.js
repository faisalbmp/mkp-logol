import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Styles from 'assets/scss/megamenu/containerpicker.module.scss';
import { textError, showTextEerror } from 'assets/scss/megamenu/megamenu.module.scss';
import { viewDone } from 'assets/scss/megamenu/portpicker.module.scss';
import { DoneMiniIcon, MinusIconH, PlusIconH } from 'components/icon/iconSvg'
import { formatPrice } from 'utils/function'
import { getContainer } from 'actions';
import { RATECONTAINER } from 'actions/types'
import { containerType } from 'utils/contants';
import LoadingDot from 'components/loading/loadingDot';

import { withTranslation } from 'react-i18next';

const ContainerPicker = (props) => {
    const { show, closeDone, t, loading } = props;
    const { listContainer, bookingTruck } = useSelector(state => state.home);
    const [totalQuantity, setTotalQuantity] = useState(0)
    const dispatch = useDispatch()
    const { addressB, addressA } = bookingTruck
    const { portID } = addressB;
    const translate = t;

    const selectContainer = async (type, i) => {
        let { quantity, price, priceAll } = listContainer[i]
        if (price) {
            if (type === "minus") {
                if (priceAll > 0 || quantity > 0) {
                    listContainer[i].priceAll = priceAll - price
                    listContainer[i].quantity = isNaN(quantity) ? 0 : quantity - 1
                }
            } else {
                listContainer[i].priceAll = isNaN(priceAll) ? price : priceAll + price
                listContainer[i].quantity = isNaN(quantity) ? 1 : quantity + 1
            }
            await dispatch({ type: RATECONTAINER, listContainer })
            const totalQuantity = await listContainer.filter(value => value.priceAll).reduce(function (a, b) {
                return a + b.priceAll;
            }, 0)
            setTotalQuantity(totalQuantity)
        }
    }

    if (listContainer.length && Object.values(addressA).length && Object.values(addressA).length && !loading)
        return (
            <div className={`${Styles.containerTruck} ${show ? Styles.show : ''}`}>
                {
                    listContainer.map((value, index) => {
                        return (
                            <div
                                className={Styles.itemTruck}
                                style={index % 2 ? {} : { borderRight: window.innerWidth >= 600 ? "1px solid #E0E5E8" : "none" }}
                                key={index}
                            >
                                <div className={Styles.leftItem}>
                                    <div className={Styles.type}>{value.containerType}</div>
                                    <div className={Styles.rate}>{value.price ? formatPrice(value.price || 0) : ""}</div>
                                </div>
                                <div className={Styles.rightItem}>
                                    <div className={Styles.btnMp} onClick={() => selectContainer("minus", index)}><MinusIconH color={value.price === 0 ? "#868A92" : "#004DFF"} /></div>
                                    <div className={Styles.noInp}>{value.quantity || 0}</div>
                                    <div className={Styles.btnMp} onClick={() => selectContainer("plus", index)}><PlusIconH /></div>
                                </div>
                            </div>
                        )
                    })
                }

                <div className={Styles.itemTruck}>
                    <div className={Styles.type} style={{ fontWeight: "normal" }}>Total</div>
                    <div className={Styles.type} style={{ color: "#002985" }}>{formatPrice(totalQuantity)}</div>
                </div>

                <div className={`${Styles.viewDone} ${viewDone}  ${Styles.itemTruck}`}>
                    <div className={Styles.finishText} onClick={closeDone}>{translate('finish')}</div>
                    <DoneMiniIcon />
                </div>

            </div>
        )
    else
        if (!loading)
            return (
                <div className={`${Styles.containerTruck} ${Styles.errorText} ${show ? Styles.show : ''}`}>{translate('alert.containerIsNotAvailable')}</div>
            )
        else
            return <div className={`${Styles.containerTruck} ${Styles.errorText} ${show ? Styles.show : ''}`}><LoadingDot /></div>
};

export default withTranslation()(ContainerPicker);