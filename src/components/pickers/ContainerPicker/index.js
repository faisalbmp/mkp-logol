import React from 'react';
import Styles from 'assets/scss/megamenu/containerpicker.module.scss';
import { TextM } from 'components/text';

const rateContainer = [
	{
		type: '20 GP',
		rate: 1700000,
		total: 0,
	},
	{
		type: '40 GP',
		rate: 1800000,
		total: 0,
	},
	{
		type: '40 HU',
		rate: 1800000,
		total: 0,
	},
	{
		type: '45 HU',
		rate: 1900000,
		total: 0,
	},
	{
		type: '20 RF',
		rate: 1700000,
		total: 0,
	},
	{
		type: '40 RH',
		rate: 1800000,
		total: 0,
	},
];

const ContainerPicker = props => {
	const { show, selectContainerLoad } = props;
	return (
		<div className={`${Styles.containerpicker} ${show ? Styles.show : ''}`}>
			<div
				className={Styles.item}
				onClick={() => selectContainerLoad({ id: 1, name: 'FCL - Full Container Load' })}
			>
				<TextM>FCL - Full Container Load</TextM>
			</div>
			{/* <div
        className={Styles.item}
        onClick={() =>
          selectContainerLoad({ id: 2, name: "LCL - Low Container Load" })
        }
      >
        <TextM>LCL - Low Container Load</TextM>
      </div> */}
		</div>
	);
};

export default ContainerPicker;
