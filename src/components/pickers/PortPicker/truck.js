import React, { useState, useEffect, Fragment } from "react";
import { useSelector, useDispatch } from "react-redux";

import Styles from "assets/scss/megamenu/portpicker.module.scss";
import "assets/fontello/css/fontello.css";
import {
  textError,
  showTextEerror,
} from "assets/scss/megamenu/megamenu.module.scss";
import PropTypes from "prop-types";
import { TextM } from "components/text";
import { Colors } from "configs";
import {
  FreightHomeIcon,
  AddressHome,
  SearchIcon,
  ExsporIcon,
  ImportIcon,
} from "../../icon/iconSvg";
import { addBookingTruck, getAllPort } from "actions";
import AutoCompleteAddress from "components/input/autoCompleteAddress";
import { setTypeExporImport } from "actions";
import useWindowDimensions from "utils/windowDimensions";
import { useTranslation } from "react-i18next";
import { getContainer } from "actions";
import { GETCONTAINER } from "actions/types";

export const PortPickerTruck = (props) => {
  const dispatch = useDispatch();
  const {
    onDestinationClick,
    calendarOpen,
    resetPickers,
    openType,
    showPortPicker,
    setLoading,
  } = props;
  const { width } = useWindowDimensions();
  const { bookingTruck, listPort, typeExportImport } = useSelector(
    (state) => state.home
  );
  const { addressA, addressB } = bookingTruck;
  const { t } = useTranslation();

  useEffect(() => {
    dispatch(getAllPort());
  }, [showPortPicker]);

  const fetchDataContainer = async () => {
    dispatch({ type: GETCONTAINER, listContainer: {} });
    setLoading(true);
    const res = await dispatch(getContainer());
    if (res) setLoading(false);
  };

  const addAddresA = async (value) => {
    !Object.keys(addressB).length ? onDestinationClick() : resetPickers();
    await dispatch(addBookingTruck({ ...bookingTruck, addressB: value }));
    fetchDataContainer();
    if (calendarOpen) calendarOpen();
  };

  const doneSelectAddres = async () => {
    !Object.keys(addressA).length ? onDestinationClick() : resetPickers();
    if (calendarOpen) calendarOpen();
  };

  const clickSuggestion = async (value) => {
    await dispatch(addBookingTruck({ ...bookingTruck, addressA: value }));
    await doneSelectAddres();
    fetchDataContainer();
  };

  function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  const renderLeftPane = () => {
    const activeSelectMA =
      (openType == "alamatPickup" && typeExportImport === "import") ||
      (openType == "tujuanPengiriman" && typeExportImport === "export");
    const activeSelectP =
      (openType == "alamatPickup" && typeExportImport === "export") ||
      (openType == "tujuanPengiriman" && typeExportImport === "import");
    return (
      <div
        className={Styles.leftPane}
        style={{ backgroundColor: width >= 600 ? "" : "white" }}
      >
        <div style={{ height: "100%" }}>
          <div
            className={`${Styles.listItem} ${
              activeSelectP ? Styles.active : ""
            }`}
            onClick={() =>
              openType == "alamatPickup"
                ? dispatch(setTypeExporImport("export"))
                : () => {}
            }
          >
            <ExsporIcon color={activeSelectMA ? "#333333" : false} />
            <div
              className={`${Styles.textSelect}  ${
                activeSelectMA ? Styles.activeSelect : ""
              }`}
            >
              {t("scheduleBooking.typePortExport")}
            </div>
            <i className={`icon-arrow-right ${Styles.arrowRight}`} />
          </div>
          <div
            className={`${Styles.listItem} ${
              activeSelectMA ? Styles.active : ""
            }`}
            onClick={() =>
              openType == "alamatPickup"
                ? dispatch(setTypeExporImport("import"))
                : () => {}
            }
          >
            <ImportIcon color={activeSelectP ? "#333333" : false} />
            <div
              className={`${Styles.textSelect}  ${
                activeSelectP ? Styles.activeSelect : ""
              }`}
            >
              {t("scheduleBooking.typePortImport")}
            </div>
            <i className={`icon-arrow-right ${Styles.arrowRight}`} />
          </div>
        </div>
      </div>
    );
  };

  const renderRightPane = () => {
    return (
      <div className={Styles.rightPane}>
        <Fragment>
          {(openType == "alamatPickup" && typeExportImport === "import") ||
          (openType == "tujuanPengiriman" && typeExportImport === "export") ? (
            listPort.length ? (
              <div className={Styles.viewSg}>
                <div className={Styles.titlePicker}>
                  <FreightHomeIcon color={"#333333"} />
                  <div
                    className={`${Styles.textSelect} ${Styles.activeSelect}`}
                  >
                    {t("port")}
                  </div>
                </div>
                <div className={Styles.wrapper}>
                  <div className={Styles.gridItem} style={{ paddingTop: 24 }}>
                    <div
                      color={Colors.text.darkgrey}
                      style={{ fontWeight: "bold" }}
                    >
                      JAKARTA
                    </div>
                  </div>
                  {listPort
                    .filter(
                      (item) =>
                        item.portID === "PORT-0001" ||
                        item.portID === "PORT-0002" ||
                        item.portID === "PORT-0003" ||
                        item.portID === "PORT-0004"
                    )
                    .map((prop, index) => {
                      return (
                        <div
                          className={Styles.gridItem}
                          key={index}
                          onClick={() => addAddresA(prop)}
                        >
                          <TextM color={Colors.text.grey1}>
                            {prop.portID === "PORT-0013"
                              ? prop.portName
                              : prop.portUTC}
                          </TextM>
                        </div>
                      );
                    })}
                </div>
              </div>
            ) : (
              <div
                className={`${textError} ${showTextEerror}`}
                style={{ marginTop: 30, fontSize: 14 }}
              >
                {t("alert.networkProblem")}
              </div>
            )
          ) : (
            ""
          )}
          {(openType == "alamatPickup" && typeExportImport === "export") ||
          (openType == "tujuanPengiriman" && typeExportImport === "import") ? (
            <div className={Styles.viewSg}>
              <div className={Styles.titlePicker}>
                <AddressHome />
                <div className={`${Styles.textSelect} ${Styles.activeSelect}`}>
                  {typeExportImport === "export"
                    ? t("scheduleBooking.enterPickUpAddress")
                    : t("scheduleBooking.enterDeliveryAddress")}
                </div>
              </div>
              <AutoCompleteAddress clickSuggestion={clickSuggestion} />
            </div>
          ) : (
            ""
          )}
        </Fragment>
      </div>
    );
  };
  return (
    <div
      className={`${Styles.portpicker} ${showPortPicker ? Styles.show : ""}`}
      style={{
        width: openType == "alamatPickup" ? 936 : 659,
        minHeight: width === 600 ? "auto" : 336,
      }}
    >
      {openType == "alamatPickup" ? renderLeftPane() : ""}
      {renderRightPane()}
    </div>
  );
};

PortPickerTruck.propTypes = {
  show: PropTypes.bool.isRequired,
};
