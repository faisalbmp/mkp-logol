import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import Styles from 'assets/scss/megamenu/portpicker.module.scss';
import 'assets/fontello/css/fontello.css';
import PropTypes from 'prop-types';
import { TextM } from 'components/text';
import { Colors } from 'configs';
import { textError, showTextEerror } from 'assets/scss/megamenu/megamenu.module.scss';

export const PortPicker = props => {
	const { show } = props;
	const [showPortPicker, setShowPortPicker] = useState(show);
	// const [activeIndex, setActiveIndex] = useState(0);
	const [currentCountryId, setCurrentCountryId] = useState(props.data[0]?.areaId);
	const { typeBooking } = useSelector(state => state.home);
	const onPortClick = port => {
		const { handleSelect, handleClose } = props;
		handleSelect(port);
		handleClose();
	};

	useEffect(() => {
		setShowPortPicker(show);
	}, [show]);

	const renderLeftPane = () => {
		const { data } = props;
		return (
			<div className={Styles.leftPane}>
				{data.map((prop, index) => {
					return (
						<div
							key={index}
							className={Styles.listItem}
							onClick={() => setCurrentCountryId(prop.areaId)}
						>
							<i className={`icon-globe ${Styles.iconGlobe}`} />
							<TextM textStyle='bold' color={Colors.text.darkblue} style={{ flex: 1 }}>
								{prop.areaName}
							</TextM>
							<i className={`icon-arrow-right ${Styles.arrowRight}`} />
						</div>
					);
				})}
			</div>
		);
	};

	const renderPorts = () => {
		const { data, searchValue } = props;

		let selected = (data && data.find(d => d.areaId === currentCountryId)) || null;
		let newArr = [];

		if (!selected) {
			return;
		}

		const replaceAt = (indexArray, [...string]) => {
			const replaceValue = i => (string[i] = <b key={i}>{string[i]}</b>);
			indexArray.forEach(replaceValue);
			return string;
		};

		const boldString = (str, find) => {
			let startIndex = str.toLowerCase().indexOf(find.toLowerCase());
			let indexes = [];
			for (let i = startIndex; i < startIndex + find.length; i++) {
				indexes.push(i);
			}
			return replaceAt(indexes, str);
		};

		return selected.countries.map((p, i) => {
			let ports = p.ports.map((port, index) => {
				return (
					<div
						key={`${port.portName}-${port.id}`}
						className={Styles.gridItem}
						onClick={() => onPortClick(port)}
					>
						<TextM color={Colors.text.darkgrey}>
							{boldString(port.portName, searchValue)}
						</TextM>
					</div>
				);
			});
			return ports;
		});
	};

	const renderRightPane = () => {
		const { data } = props;

		return (
			<div className={Styles.rightPane} style={{ paddingLeft: 24 }}>
				<div className={Styles.wrapper}>
					{data.length ? (
						renderPorts()
					) : (
						<div
							className={`${textError} ${showTextEerror}`}
							style={{ marginTop: 30, fontSize: 14 }}
						>
							Ada kendala di koneksi, mohon coba lagi
						</div>
					)}
				</div>
			</div>
		);
	};

	return (
		<div
			className={`${Styles.portpicker} ${showPortPicker ? Styles.show : ''}`}
			style={{ width: typeBooking === 'freight' ? 936 : 'auto' }}
		>
			{renderLeftPane()}
			{renderRightPane()}
		</div>
	);
};

PortPicker.propTypes = {
	show: PropTypes.bool.isRequired,
};
