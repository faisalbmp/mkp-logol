import React, { Component, Fragment } from 'react';
import ReactDatePicker, { CalendarContainer } from 'react-datepicker'
import moment from 'moment'
import { useSelector, useDispatch, connect } from 'react-redux';

import { megamenu, buttonSelect, gridItem, title, lineCenter, textSelect, textError, showTextEerror } from 'assets/scss/megamenu/megamenu.module.scss';
import { LocationB, LocationA, ContainerHome, CalendarSmall, ArrowDown, LineIcon } from 'components/icon/iconSvg';
import { PortPickerTruck, ContainerPickerTruck } from 'components/pickers'
import { formatPrice } from 'utils/function'
import 'assets/scss/component/react-datepicker.scss'
import { addBookingTruck, getContainer, setTypeExporImport } from 'actions'
import useWindowDimensions from 'utils/windowDimensions';

import { withTranslation } from 'react-i18next';

const ExampleCustomInput = ({ startDate, resetPickers, onOpen, translate }) => {
    return (
        <div
            style={{ width: "100%" }}
        >
            <div className={title}>{translate('scheduleBooking.shippingTime')}</div>
            <div
                className={buttonSelect}
                onClick={onOpen}
                style={{ padding: "0px 16px 0px 16px", width: "100%" }}
            >
                <CalendarSmall
                    color={startDate ? "#004DFF" : ""}
                />
                <div className={lineCenter} style={{ paddingLeft: startDate ? 8 : 37, marginBottom: 7, marginBottom: startDate ? -1 : 8 }}>
                    {startDate ? moment(startDate).format('L') : <LineIcon />}
                </div>
                <ArrowDown />
            </div>
        </div>
    )
};

const MyContainer = ({ className, children }) => {
    const { width } = useWindowDimensions()
    return (
        <div style={{ marginTop: width === 600 ? 0 : 15 }}>
            <CalendarContainer className={className}>
                <div style={{ position: "relative" }}>{children}</div>
            </CalendarContainer>
        </div>
    );
};

class MegaMenu extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            openType: "",
            startDate: "",
            containerTotal: 0,
            totalQuantity: 0,
            totalPrice: 0,
            totalContainer: 0,
            loading: false
        }
    }

    onOriginClick = async () => {
        const { openType } = this.state
        this.setState({ openType: openType === "alamatPickup" ? "" : "alamatPickup" })
    }

    onDestinationClick = async () => {
        const { openType } = this.state
        this.setState({ openType: openType === "tujuanPengiriman" ? "" : "tujuanPengiriman" })
    }

    onDatePicker = async () => {
        await this.resetPickers();
        this._calendar.setOpen(true)
    }

    onContainerPickerClick = async () => {
        const { openType } = this.state
        this.setState({ openType: openType === "containerPicker" ? "" : "containerPicker" })
    }

    resetPickers = () => {
        this.setState({ openType: "" })
    }

    onChangeDate = (date) => {
        const { bookingTruck, addBookingTruck } = this.props
        addBookingTruck({ ...bookingTruck, date })
        this.onContainerPickerClick()
    }

    changeImportExport = (type) => {
        this.setState({ typExpotImport: type })
    }

    render() {
        const { openType, loading } = this.state
        const { bookingTruck, validationInput, listContainer, typeExportImport } = this.props
        const { addressA, addressB, date } = bookingTruck
        const someDate = new Date()
        const dateCorrect = someDate.setDate(someDate.getDate() + 5);

        const listFilt = listContainer.length ? listContainer.filter(value => value.priceAll) : []
        const totalPrice = listFilt.length ? listFilt.reduce(function (a, b) {
            return a + b.priceAll;
        }, 0) : []
        const totalContainer = listFilt.length ? listFilt.reduce(function (a, b) {
            return a + b.quantity;
        }, 0) : []

        const errorCheckA = typeExportImport === "export" ? !Object.keys(addressA).length : !Object.keys(addressB).length
        const errorCheckB = typeExportImport !== "export" ? !Object.keys(addressA).length : !Object.keys(addressB).length

        const valueColorA = typeExportImport === "export" ? (addressA.description ? '#333' : '#808080') : addressB.portUTC ? '#333' : '#808080';
        const valueColorB = typeExportImport === "export" ? (addressB?.portUTC ? '#333' : '#808080') : (addressA?.description ? '#333' : '#808080');

        return (
            <div className={`${megamenu} mega-custom`}>
                <div className={gridItem}>
                    <div className={title}>{this.props.t('scheduleBooking.from')}</div>
                    <div className={buttonSelect} onClick={this.onOriginClick}>
                        {/* <div className={buttonSelect} onClick={onRedirect}> */}
                        <LocationA color={(openType === "alamatPickup" || (addressA.description || "")) && "#004DFF"} />
                        <div
                            className={textSelect}
                            style={{ color: valueColorA }}>{typeExportImport === "export" ? (addressA.description || this.props.t('scheduleBooking.PickaLocation')) : addressB.portUTC || this.props.t('scheduleBooking.PickaLocation')}</div>
                        <ArrowDown />
                    </div>
                    <div className={`${textError}  ${validationInput && errorCheckA ? showTextEerror : ""}`}>{this.props.t('alert.mandatoryScheduleBooking')}</div>
                    <PortPickerTruck
                        setLoading={res => this.setState({ loading: res })}
                        resetPickers={this.resetPickers}
                        onOriginClick={this.onOriginClick}
                        onDestinationClick={this.onDestinationClick}
                        changeImportExport={this.changeImportExport}
                        openType={openType}
                        showPortPicker={openType === "alamatPickup"}
                    />
                </div>
                <div className={gridItem}>
                    <div className={title}>{this.props.t('scheduleBooking.to')}</div>
                    <div className={buttonSelect} onClick={this.onDestinationClick}>
                        {/* <div className={buttonSelect} onClick={onRedirect}> */}
                        <LocationB color={(openType === "tujuanPengiriman" || addressB.portUTC) && "#004DFF"} />
                        <div className={textSelect}
                            style={{ color: valueColorB }}>
                            {typeExportImport === "export" ? (addressB?.portUTC ?? this.props.t('scheduleBooking.PickaLocation')) : (addressA?.description ? addressA?.description : this.props.t('scheduleBooking.PickaLocation')) || this.props.t('scheduleBooking.PickaLocation')}
                        </div>
                        <ArrowDown />
                    </div>
                    <div className={`${textError}  ${validationInput && errorCheckB ? showTextEerror : ""}`}>{this.props.t('alert.mandatoryScheduleBooking')}</div>
                    <PortPickerTruck
                        setLoading={res => this.setState({ loading: res })}
                        resetPickers={this.resetPickers}
                        onOriginClick={this.onOriginClick}
                        onDestinationClick={this.onDestinationClick}
                        calendarOpen={() => this._calendar.setOpen(true)}
                        changeImportExport={this.changeImportExport}
                        openType={openType}
                        showPortPicker={openType === "tujuanPengiriman"}
                    />
                </div>
                <div className={gridItem}>
                    <ReactDatePicker
                        ref={(c) => this._calendar = c}
                        style={{ width: "100%" }}
                        customInput={
                            <ExampleCustomInput
                                translate={this.props.t}
                                onOpen={this.onDatePicker}
                                startDate={date}
                            />
                        }
                        minDate={dateCorrect}
                        calendarContainer={MyContainer}
                        selected={date || dateCorrect}
                        onChange={this.onChangeDate}
                    >
                    </ReactDatePicker>
                    <div className={`${textError}  ${validationInput && typeof date === "string" ? showTextEerror : ""}`}>{this.props.t('alert.mandatoryScheduleBooking')}</div>
                </div>
                <div className={gridItem}>
                    <div className={title}>{this.props.t('scheduleBooking.containerAmount')}</div>
                    <div className={buttonSelect} onClick={this.onContainerPickerClick}>
                        {/* <div className={buttonSelect} onClick={onRedirect}> */}
                        <ContainerHome color={openType === "containerPicker" || totalContainer ? "#004DFF" : ""} />
                        <div
                            style={{ width: "100%", padding: "0px 5px 0px 8px" }}
                            className={`${lineCenter} ${textSelect}`}
                        >
                            {totalContainer ? `${totalContainer} Kontainer - ${formatPrice(totalPrice)}` : ""}
                        </div>
                        <ArrowDown />
                    </div>
                    {/* {
                        Object.keys(addressA).length && Object.keys(addressB).length ?
                            <div className={`${textError}  ${validationInput && !totalContainer ? showTextEerror : "#333333"}`}>{this.props.t('alert.mandatoryScheduleBooking')}</div>
                            :
                            <div className={`${textError}  ${validationInput && openType !== "containerPicker" && !totalContainer ? showTextEerror : "#333333"}`}>{this.props.t('alert.mandatoryScheduleBooking')}</div>
                    } */}

                    <div className={`${textError}  ${validationInput && !listContainer.length ? showTextEerror : "#333333"}`}>{this.props.t('alert.mandatoryScheduleBooking')}</div>

                    {/* <div className={`${textError}  ${!addressB.portLatitude && showContainerPicker ? showTextEerror : ""}`}>Please Select Tujuan Pengiriman</div> */}
                    <ContainerPickerTruck
                        loading={loading}
                        show={openType === "containerPicker"}
                        closeDone={this.resetPickers}
                    />
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({ bookingTruck: state.home.bookingTruck, listContainer: state.home.listContainer, typeExportImport: state.home.typeExportImport })
const mapDispatchToProps = { addBookingTruck, getContainer, setTypeExporImport }
export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(MegaMenu))