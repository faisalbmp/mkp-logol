import React, { Component, Fragment } from "react";
import ReactDatePicker, { CalendarContainer } from "react-datepicker";
import moment from "moment";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as pickerActions from "actions/pickerActions";
import * as bookingActions from "actions/bookingActions";
import {
  megamenu,
  buttonSelect,
  gridItem,
  title,
  lineCenter,
  portInput,
  selectedLabel,
  showTextEerror,
  textError,
  swapImg,
} from "assets/scss/megamenu/megamenu.module.scss";
import {
  LocationB,
  LocationA,
  ContainerHome,
  CalendarSmall,
  ArrowDown,
  LeftRightArrow,
  LineIcon,
} from "components/icon/iconSvg";
import { TextM } from "components/text";
import { PortPicker, DatePicker, DatePicker2, ContainerPicker } from "components/pickers";
// import DatePicker from "react-datepicker";

import { months } from "utils/dates";
import "assets/scss/component/react-datepicker.scss";
import { withTranslation } from "react-i18next";
import PortPicker2 from "../PortPicker2";
import InputDate from "components/input/inputDate";
import { getCookie } from "utils/cookies";

const exampleCustomInput = (time, startDate) => {
  return (
    <div style={{ width: 264 }}>
      <div className={title}>{time}</div>
      <div className={buttonSelect} style={{ padding: "0px 16px 0px 16px" }}>
        <CalendarSmall color={startDate ? "#004DFF" : ""} />
        <div
          className={lineCenter}
          style={{
            paddingLeft: startDate ? 8 : 37,
            marginBottom: 7,
            marginBottom: startDate ? -1 : 8,
          }}
        >
          {startDate ? moment(startDate).format("DD, MM YYYY") : ""}
          {startDate.length ? startDate : <LineIcon />}
        </div>
        <ArrowDown />
      </div>
    </div>
  );
};

const MyContainer = ({ className, children }) => {
  return (
    <div style={{ marginTop: 21 }}>
      <CalendarContainer className={className}>
        <div style={{ position: "relative" }}>{children}</div>
      </CalendarContainer>
    </div>
  );
};

class MegaMenu extends Component {
  constructor() {
    super();

    const cookie = getCookie('bookingOptions')
    const booking = JSON.parse(cookie)

    this.state = {
      showOriginPorts: false,
      showDestinationPorts: false,
      showDatePicker: false,
      showContainerPicker: false,
      startDate: "",
      containerTotal: 0,
      rateContainerT: 0,

      origin_focus: false,
      origin_typing: false,
      origin_typing_val: "",
      selected_origin: booking?.origin,

      destination_focus: false,
      destination_typing: false,
      destination_typing_val: "",
      selected_destination: booking?.destination,
      selectedDate: booking?.date,


      selectedContainer: null,
    };
  }

  componentDidMount() {
    const { origin, destination, date, hideVal } = this.props
    this.getPortData();
    this.forceUpdate()
    const cookie = getCookie('bookingOptions')
    const booking = JSON.parse(cookie)
    if (!hideVal) {
      this.setState({
        selected_origin: booking ? booking.origin : origin,
        selected_destination: booking ? booking.destination : destination,
        selectedDate: booking ? booking.date : date
      });
    } else {
      this.setState({
        selected_origin: "",
        selected_destination: "",
        selectedDate: ""
      });
    }
  }

  /*  componentDidUpdate(prevProps) {
     const { origin, destination, date } = this.props;
 
     // prevProps.origin !== origin && this.setState({ selected_origin: origin, })
     // prevProps.destination !== destination && this.setState({ selected_destination: destination, })
     // prevProps.date !== date && this.setState({ selectedDate: date, })
 
     if (prevProps.origin !== origin || prevProps.destination !== destination || prevProps.date !== date) {
       this.forceUpdate()
     }
   } */

  setBookingOptions = (options) => {
    this.props.actions.searchSchedules(options);
  };

  getPortData = async () => {
    // await this.props.actions.getOriginPorts();
    // await this.props.actions.getDestinationPorts();
    await this.props.actions.getAllPorts()
  };

  onOriginClick = async () => {
    if (!this.state.showOriginPorts) await this.resetPickers();
    this.setState({
      showOriginPorts: !this.state.showOriginPorts,
    });
  };

  onDestinationClick = async () => {
    if (!this.state.showDestinationPorts) await this.resetPickers();
    this.setState({
      showDestinationPorts: !this.state.showDestinationPorts,
    });
  };

  onDatePickerClick = async () => {
    if (!this.state.showDatePicker) await this.resetPickers();
    this.setState({
      showDatePicker: !this.state.showDatePicker,
    });
  };

  onContainerPickerClick = async () => {
    if (!this.state.showContainerPicker) await this.resetPickers();
    this.setState({
      showContainerPicker: !this.state.showContainerPicker,
    });
  };

  resetPickers = () => {
    this.setState({
      showOriginPorts: false,
      showDestinationPorts: false,
      showDatePicker: false,
      showContainerPicker: false,
    });
  };

  selectContainer = (data, type) => {
    let { containerTotal, rateContainerT } = this.state;

    if (type === "minus") {
      containerTotal = containerTotal - 1;
      rateContainerT = rateContainerT - data.rate;
    } else {
      containerTotal = containerTotal + 1;
      rateContainerT = rateContainerT + data.rate;
    }

    this.setState({ containerTotal, rateContainerT });
  };

  _originIn = () => {
    !this.state.origin_focus && this._resetAllPicker();
    this.setState({
      originPicker: true,
      expand: true,
      origin_focus: true,
    });
  };

  _originOut = () => {
    this.setState({
      origin_focus: false,
    });
  };

  _destinationIn = () => {
    !this.state.destination_focus && this._resetAllPicker();
    this.setState({
      destinationPicker: true,
      expand: true,
      destination_focus: true,
    });
  };

  _destinationOut = () => {
    this.setState({
      destination_focus: false,
    });
  };

  _resetAllPicker = async () => {
    this.setState({
      overflow: false,
      qtyPicker: false,
      expand: false,
      showOriginPorts: false,
      showDestinationPorts: false,
      datePicker: false,
      goodsTypePicker: false,
      containerPicker: false,
    });
  };

  _onOriginChange = (e) => {
    let val = e.target.value;
    this.setState(
      {
        origin_typing_val: val,
      },
      () => {
        this._handleOriginSearch();
      }
    );
  };

  _onDestinationChange = (e) => {
    let val = e.target.value;

    this.setState(
      {
        destination_typing_val: val,
      },
      () => {
        this._handleDestinationSearch();
      }
    );
  };

  _handleOriginSearch = () => {
    let { origins } = this.props.ports;
    let { origin_typing_val } = this.state;

    const loopOrigins = () => {
      return origins.filter((list) => {
        return list.portName.toLowerCase().indexOf(origin_typing_val.toLowerCase()) > -1 ||
          list.countryName.toLowerCase().indexOf(origin_typing_val.toLowerCase()) > -1
      })
    };


    let result = loopOrigins();

    this.props.actions.searchOriginPorts(result);
  };

  _handleDestinationSearch = () => {
    let { destinations } = this.props.ports;
    let { destination_typing_val, selected_origin } = this.state;

    const loopDest = () => {

      return destinations.filter((list) => {
        return list.portName.toLowerCase().indexOf(destination_typing_val.toLowerCase()) > -1 ||
          list.countryName.toLowerCase().indexOf(destination_typing_val.toLowerCase()) > -1
      })

    };

    let result = loopDest();
    let new_data = [];

    this.props.actions.searchDestinationPorts(result);
  };

  _handleSwapLocation = () => {
    const {
      selected_destination,
      origin_typing_val,
      selected_origin,
      destination_typing_val,
      selectedDate,
      selectedContainer
    } = this.state

    const temp = {
      selected_destination: selected_destination,
      destination_typing_val: destination_typing_val
    }
    const assignDestination = () => {
      this.setState({
        selected_destination: selected_origin,
        destination_typing_val: origin_typing_val
      })
    }
    const assignOrigin = () => {
      this.setState({
        selected_origin: temp.selected_destination,
        origin_typing_val: temp.destination_typing_val
      })
    }
    const swapLocation = async () => {
      await assignDestination()
      await assignOrigin()

      await this.setBookingOptions({
        selected_origin: selected_destination,
        selected_destination: selected_origin,
        selectedDate,
        selectedContainer,
      });
    }
    swapLocation()
  }

  _setSelectedOrigin = async (origin) => {
    const {
      selected_destination,
      selectedDate,
      selectedContainer,
    } = this.state;
    await this.setState({
      origin_typing: false,
      selected_origin: origin,
    });

    this.setBookingOptions({
      selected_origin: origin,
      selected_destination,
      selectedDate,
      selectedContainer,
    });

    // this._filterDestinations();
  };

  _filterDestinations = () => {
    let { destinations } = this.props.ports;
    let { selected_origin } = this.state;

    const removeChar = (attr) => {
      return attr
        .replace(/\s/g, "")
        .replace(/,/g, "")
        .replace(/\(/g, "")
        .replace(/\)/g, "");
    };

    const loopDest = () => {
      let newDests = destinations.slice(0);
      let result = [];
      newDests.map((prop, index1) => {
        prop.countries.map((con, index2) => {
          let city = con.ports.filter((prop2) => {
            return (
              removeChar(prop2.portName.toLowerCase()).indexOf(
                removeChar(selected_origin.portName.toLowerCase())
              ) < 0
            );
          });
          const newCity = [];
          for (let key in city) {
            newCity.push({
              ...city[key],
              areaId: prop.areaId,
              areaName: prop.areaName,
            });
          }
          city.length > 0
            ? (result = [...result, ...newCity])
            : (result = result);
        });
      });
      return result;
    };

    let result = loopDest();

    let new_data = [];

    result.map((obj) => {
      let newObj = {
        areaId: obj.areaId,
        areaName: obj.areaName,
        countries: [{ countryName: obj.countryName, ports: [obj] }],
      };
      const checkAreaId = (element) => {
        return element.areaId === obj.areaId;
      };

      let existIndex = new_data.findIndex(checkAreaId);

      if (existIndex >= 0) {
        let existCountryIndex = new_data[existIndex].countries.findIndex(
          (c) => c.country === obj.country
        );
        if (existCountryIndex >= 0) {
          new_data[existIndex].countries[existCountryIndex].ports.push(obj);
          return;
        }

        new_data[existIndex].countries.push({
          countryName: obj.countryName,
          ports: [obj],
        });
        return;
      }
      new_data.push(newObj);
    });

    this.props.actions.searchDestinationPorts(new_data);
  };

  _setSelectedDestination = (destination) => {
    const { selected_origin, selectedDate, selectedContainer } = this.state;
    this.setState({
      destination_typing: false,
      selected_destination: destination,
    });

    this.setBookingOptions({
      selected_origin,
      selected_destination: destination,
      selectedDate,
      selectedContainer,
    });
  };

  selectContainerLoad = (container) => {
    const { selected_origin, selected_destination, selectedDate } = this.state;
    this.setState({
      selectedContainer: container,
      showContainerPicker: false,
    });

    this.setBookingOptions({
      selected_origin,
      selected_destination,
      selectedDate,
      selectedContainer: container,
    });
  };

  selectDate = (date) => {
    const {
      selected_origin,
      selected_destination,
      selectedContainer,
    } = this.state;

    this.setState({
      selectedDate: date,
      showDatePicker: false,
    });

    this.setBookingOptions({
      selected_origin,
      selected_destination,
      selectedDate: date,
      selectedContainer,
    });
  };

  render() {
    const {
      showOriginPorts,
      showDestinationPorts,
      showDatePicker,
      showContainerPicker,
      startDate,
      containerTotal,
      rateContainerT,

      selected_origin,
      selected_destination,
      origin_typing,
      origin_typing_val,
      destination_typing_val,
      destination_typing,
      selectedContainer,
      origin_focus,
      destination_focus,
      selectedDate,
    } = this.state;

    const {
      locationA,
      locationB,
      time,
      container,
      validationInput,
      setValidationInput,
    } = this.props;

    return (
      <div className={`${megamenu} mega-custom`}>
        <div className={gridItem}>
          <div className={swapImg} onClick={this._handleSwapLocation}>
            <LeftRightArrow />
          </div>
          <div className={title}>{locationA}</div>
          <div className={buttonSelect} onClick={this.onOriginClick}>
            <LocationA />
            <input
              className={portInput}
              value={
                (!origin_focus &&
                  selected_origin &&
                  `${selected_origin.portName}, ${selected_origin.countryName}`) ||
                origin_typing_val
              }
              onChange={this._onOriginChange}
              onFocus={this._originIn}
              onBlur={this._originOut}
              placeholder={this.props.t("scheduleBooking.pickACity")}
            />

            <ArrowDown />
          </div>
          <div
            className={`${textError}  ${validationInput && !selected_origin ? showTextEerror : ""
              }`}
          >
            {this.props.t("alert.mandatoryScheduleBooking")}
          </div>
          <PortPicker2
            data={this.props.ports.origins_offline}
            show={showOriginPorts}
            handleSelect={this._setSelectedOrigin}
            handleClose={this._resetAllPicker}
          />
          {/* <PortPicker2
            data={this.props.ports.origins_offline}
            show={showOriginPorts}
          /> */}
        </div>
        <div className={gridItem}>
          <div className={title}>{locationB}</div>
          <div className={buttonSelect} onClick={this.onDestinationClick}>
            <LocationB />
            <input
              className={portInput}
              value={
                (!destination_focus &&
                  selected_destination &&
                  `${selected_destination.portName}, ${selected_destination.countryName}`) ||
                destination_typing_val
              }
              onChange={this._onDestinationChange}
              onFocus={this._destinationIn}
              onBlur={this._destinationOut}
              placeholder={this.props.t("scheduleBooking.pickADestination")}
            />
            <ArrowDown />
          </div>
          <div
            className={`${textError}  ${validationInput && !selected_destination ? showTextEerror : ""
              }`}
          >
            {this.props.t("alert.mandatoryScheduleBooking")}
          </div>
          <PortPicker2
            data={this.props.ports.destinations_offline}
            show={showDestinationPorts}
            handleSelect={this._setSelectedDestination}
            handleClose={this._resetAllPicker}
          />
        </div>
        <div className={gridItem}>
          <div className={title}>{time}</div>
          <div className={buttonSelect} onClick={this.onDatePickerClick}>
            <CalendarSmall />
            <TextM textStyle="bold" className={selectedLabel}>
              {selectedDate ?
                `${moment(selectedDate.departure).format("DD")} ${months[moment(selectedDate.departure).format("MM") - 1].abbr
                } ${moment(selectedDate.departure).format("YY")
                }' - ${moment(selectedDate.arrival).format("DD")} ${months[moment(selectedDate.arrival).format("MM") - 1].abbr
                } ${moment(selectedDate.arrival).format("YY")
                }'`
                : ""
              }
            </TextM>
            <ArrowDown />
          </div>
          <div
            className={`${textError}  ${validationInput && !selectedDate ? showTextEerror : ""
              }`}
          >
            {this.props.t("alert.mandatoryScheduleBooking")}
          </div>

          <DatePicker2 show={showDatePicker} handleSelect={this.selectDate} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  ...state,
});

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      Object.assign({}, pickerActions, bookingActions),
      dispatch
    ),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation()(MegaMenu));
