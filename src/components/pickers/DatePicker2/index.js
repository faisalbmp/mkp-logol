import React, { useState, useEffect } from "react";
import Styles from "assets/scss/megamenu/datepicker2.module.scss";
import { TextM, TextS } from "components/text";
import { months, daysOfWeek } from "utils/dates";
import moment from "moment";
import { withTranslation } from "react-i18next";
import { MinusIconH, PlusIconH } from 'components/icon/iconSvg';
import RoundedButton from "components/button/RoundedButton";
import { ButtonM } from "components/button";

const DatePicker2 = (props) => {
  const [startDate, setstartDate] = useState(moment())
  const [endDate, setEndDate] = useState(moment().add(1, "week"))
  const [showDate, setShowDate] = useState(moment())
  const [showWeek, setShowWeek] = useState(1)
  const prevSection = () => {
    const prevMonth = showDate.clone().subtract(1, "month")
    setShowDate(prevMonth)
  };

  const nextSection = () => {
    const nextMonth = showDate.clone().add(1, "month")
    setShowDate(nextMonth)
  };

  const addDuration = () => {
    const addWeek = endDate.clone().add(1, "week")
    setShowWeek(showWeek + 1)
    setEndDate(addWeek)
  }

  const substractDuration = () => {
    const substractWeek = endDate.clone().subtract(1, "week")
    if (showWeek > 1) {
      setShowWeek(showWeek - 1)
      setEndDate(substractWeek)
    }
  }

  const changeStartDate = (date) => {
    setstartDate(date)
    setShowWeek(1)
    setEndDate(moment(date).add(1, "week"))
  }

  const selectDate = () => {
    const { handleSelect } = props;
    handleSelect({
      arrival: endDate,
      departure: startDate,
      range: showWeek * 7,
    });
  };

  const renderDayOfMonth = () => {
    function calculateDayOfMonth() {
      const startDay = showDate.clone().startOf("month").startOf("week")
      const endDay = showDate.clone().endOf("month").endOf("week")
      const day = startDay.clone()
      const calendar = []
      while (day.isBefore(endDay, "day")) {
        calendar.push(
          Array(7).fill(0).map(() => day.add(1, "day").clone())
        )
      }
      return calendar
    }
    return (
      <div>
        {
          calculateDayOfMonth().map((week) => (
            <div className={Styles.datesContent}>
              {week.map((day) => (
                <div className={`${Styles.dateContentWrapper}`}>
                  <div className={`${Styles.dateContentText} `} onClick={() => changeStartDate((day))}>
                    <div className={`
                      ${day.isBetween(startDate, moment(endDate)) && Styles.isbetween}
                      ${day.isSame(startDate, "day") && Styles.isStart}
                      ${day.isSame(endDate, "day") && Styles.isEnd}
                      ${day.isSame(moment(),"day") && Styles.today}
                      }
                    `}>
                      <TextS className={`${((startDate.isSame(day, "day") || endDate.isSame(day, "day")) && Styles.active)
                        || day.isSame(moment(), "day") && Styles.todayText} ${!day.isSame(showDate, "month") && Styles.outsideMonth
                        } ${day.isSame(showDate, "month") && parseInt(day.weekday()) === 0 && Styles.holiday}`}>{day.format("D").toString()}</TextS>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          ))
        }
      </div>)
  }

  const renderHeader = () => {
    return (
      <div className={Styles.navigation}>
        <div className={Styles.navButton} onClick={prevSection}>
          <i className={`icon-arrow-left ${Styles.iconArrow}`}></i>
        </div>
        <div className={Styles.labelHeader}>
          <TextS>{`${months[moment(showDate).format("M") - 1].name} ${moment(showDate).format("Y")}`}</TextS>
        </div>
        <div className={Styles.navButton} onClick={nextSection}>
          <i className={`icon-arrow-right ${Styles.iconArrow}`}></i>
        </div>
      </div>
    );
  };

  const renderDates = () => {
    return (
      <div className={Styles.dates}>
        <div>
          {daysOfWeek.map(({ abbr }) =>
            <div className={Styles.labelDates}>
              <TextS
                className={Styles.labelDatesText}
              >{abbr}
              </TextS>
            </div>
          )}
        </div>
        {renderDayOfMonth()}
      </div >
    );
  };

  const renderFooter = () => {
    return (
      <div className={Styles.footer}>
        <div className={Styles.shipmentDurationForm}>
          <TextS
            className={Styles.labelShipment}
          >{props.t("scheduleBooking.shipmentDuration")}
          </TextS>
          <div className={Styles.shipmentDurationSelection}>
            <div className={Styles.RoundedButton} onClick={substractDuration}>
              <MinusIconH width={8} height={2} color="#868A92" />
            </div>
            <TextS>{`${showWeek} Minggu`}</TextS>
            <div className={Styles.RoundedButton} onClick={addDuration}>
              <PlusIconH width={12} height={12} />
            </div>
          </div>
        </div>
        <div className={Styles.buttonWrapper}>
          <ButtonM className={Styles.buttonChoose} onClick={selectDate} >
            <TextS className={Styles.text}>
              Pilih
          </TextS>
          </ButtonM>
        </div>
      </div>
    )
  }

  const { show } = props;

  return (
    <div className={`${Styles.datepicker} ${show ? Styles.show : ""}`}>
      {renderHeader()}
      {renderDates()}
      {renderFooter()}
    </div>
  );
};

export default withTranslation()(DatePicker2);
