import React, { useState } from "react";
import Styles from "assets/scss/bookingmenu/wrapper.module.scss";
import { LabelM } from "components/label";
import { ButtonM } from "components/button";
import { months } from "utils/dates";
import moment from "moment";
import { Colors } from "configs";
import { flagId } from "assets/images";
import { useHistory } from "react-router-dom";
import useWindowDimensions from "utils/windowDimensions";
import { withTranslation } from "react-i18next";
import EditSearchModal from "pages/schedules/EditSearchModal";


const RutePengiriman = (props) => {
  const { origin, destination, t } = props;
  const { width } = useWindowDimensions();
  return (
    <div className={Styles.menuItem}>
      {width > 600 ? <LabelM>{t("vSchedules.route")}</LabelM> : ""}
      <div className={Styles.menuWrapper}>
        <div className={Styles.directions}>
          <div className={Styles.port}>
            <img src={origin?.countryFlag} alt="" />
            <LabelM
              style={{
                fontSize: 16,
                textOverflow: "ellipsis",
                overflow: "hidden",
              }}
              color={Colors.black}
            >
              {origin && `${origin?.portName}, ${origin?.countryName}`}
            </LabelM>
          </div>
          <i
            className="icon-tail-arrow"
            style={{ paddingLeft: 8, paddingRight: 8 }}
          />
          <div className={Styles.port}>
            <img src={destination?.countryFlag} alt="" />
            <LabelM
              style={{
                fontSize: 16,
                textOverflow: "ellipsis",
                overflow: "hidden",
              }}
              color={Colors.black}
            >
              {destination &&
                `${destination?.portName}, ${destination?.countryName}`}
            </LabelM>
          </div>
        </div>
      </div>
    </div>
  );
};

const WaktuPengiriman = (props) => {
  const { date, t } = props;
  const { width } = useWindowDimensions();
  return (
    <div className={Styles.menuItem}>
      {width > 600 ? <LabelM>{t("vSchedules.time")}</LabelM> : ""}
      <div className={Styles.menuWrapper}>
        <LabelM
          style={{ fontSize: 16, textOverflow: "ellipsis", overflow: "hidden" }}
          color={Colors.black}
        >
          {date ?
            `${moment(date.departure).format("DD")} ${months[moment(date.departure).format("MM") - 1].abbr
            } ${moment(date.departure).format("YY")
            }' - ${moment(date.arrival).format("DD")} ${months[moment(date.arrival).format("MM") - 1].abbr
            } ${moment(date.arrival).format("YY")
            }'`
            : ""
          }
        </LabelM>
      </div>
    </div>
  );
};

const ButtonSearch = ({ onClick, t }) => {
  return (
    <div className={Styles.buttonSearch} onClick={onClick}>
      <ButtonM>
        {/* <a style={{ textDecoration: "none" }} href="/"> */}
        {t("vSchedules.editSearch")}
        {/* </a> */}
      </ButtonM>
    </div>
  );
};

const Counter = () => {
  const [count, setCount] = useState(0);
  const increase = () => {
    setCount(count + 1);
  };

  const decrease = () => {
    if (count < 1) {
      return;
    }
    setCount(count - 1);
  };
  return (
    <div className={Styles.counter}>
      <button className={Styles.buttonSmall} onClick={decrease}>
        <i className={`icon-minus ${Styles.buttonIcon}`} />
      </button>
      <LabelM style={{ fontSize: "16px" }}>{count}</LabelM>
      <button className={Styles.buttonSmall} onClick={increase}>
        <i className={`icon-plus ${Styles.buttonIcon}`} />
      </button>
    </div>
  );
};

const ContainerItem = ({ label, index, active, onActive }) => {
  return (
    <div
      className={`${Styles.containerItem} ${active === index ? `${Styles.active}` : ""
        }`}
      style={{ cursor: "pointer" }}
    >
      <div onClick={() => onActive(index)}>
        <LabelM style={{ fontSize: "16px", cursor: "pointer" }}>{label}</LabelM>
      </div>
      <Counter />
    </div>
  );
};

const ContainerMenu = ({ t }) => {
  const containerItems = [
    { label: "20' General Purpose" },
    { label: "40' General Purpose" },
    { label: "40' High Cube" },
    { label: "45' High Cube" },
    { label: "40' General Purpose" },
    { label: "40' General Purpose" },
    { label: "45' High Cube" },
    { label: "45' High Cube" },
  ];
  const [active, setActive] = useState("");
  const itemRef = React.createRef();
  const containerHandler = (direction) => {
    if (direction === "left") {
      return itemRef ? (itemRef.current.scrollLeft -= 500) : null;
    } else {
      return itemRef ? (itemRef.current.scrollLeft += 500) : null;
    }
  };

  const onActiveHandler = (index) => {
    setActive(index);
  };
  return (
    <div className={Styles.menuItem}>
      <div className={Styles.titleWrapper}>
        <LabelM>Tentukan Tipe Kontainer</LabelM>
      </div>
      <div className={Styles.menuWrapper}>
        <div className={Styles.bottomMenu}>
          <button className={Styles.nav}>
            <i
              className="icon-arrow-left"
              style={{ fontSize: 10, cursor: "pointer" }}
              onClick={() => containerHandler("left")}
            ></i>
          </button>
          <div className={Styles.bottomMenuMid} ref={itemRef}>
            {containerItems.map((item, index) => (
              <ContainerItem
                label={item.label}
                active={active}
                index={`${index}`}
                onActive={onActiveHandler}
              />
            ))}
          </div>
          <button className={Styles.nav}>
            <i
              className="icon-arrow-right"
              style={{ fontSize: 10, cursor: "pointer" }}
              onClick={() => containerHandler("right")}
            ></i>
          </button>
        </div>
      </div>
    </div>
  );
};

const BookingMenu = (props) => {
  const history = useHistory();
  const [modal, setModal] = useState('');
  const { t } = props;
  return (
    <>
      <div className={Styles.wrapper}>
        <div
          className={Styles.top}
          style={{ paddingBottom: props.hideContainerMenu ? "20px" : 0 }}
        >
          <div className={Styles.right}>
            <div className={Styles.menuSeparator} style={{ marginRight: 67 }}>
              <RutePengiriman {...props.booking} t={t} />
            </div>
            <div className={Styles.menuSeparator}>
              <WaktuPengiriman {...props.booking} t={t} />
            </div>
          </div>
          <ButtonSearch t={t} onClick={() => setModal("editSchedule")} />
        </div>
        {!props.hideContainerMenu && (
          <div className={Styles.bottom}>
            <ContainerMenu />
          </div>
        )}
        {/* <div>Hello</div> */}
      </div>
      <EditSearchModal
        show={modal === 'editSchedule'}
        onClose={() => setModal('')}
        booking={props.booking}
      />
    </>
  );
};

export default withTranslation()(BookingMenu);
