import React, { useState, useEffect } from "react";
import Styles from "assets/scss/megamenu/datepicker.module.scss";
import { TextS } from "components/text";
import { months, dates } from "utils/dates";
import moment from "moment";

const DatePicker = (props) => {
  const [activeMonth, setActiveMonth] = useState(
    parseInt(moment().format("M"))
  );
  const [dateList, setDateList] = useState(
    dates.filter((date) => date.monthId === activeMonth)
  );

  const [activeTab, setActiveTab] = useState(
    activeMonth % 3 === 0
      ? Math.floor(activeMonth / 4)
      : Math.round(activeMonth / 4)
  );

  useEffect(() => {
    setDateList(dates.filter((date) => date.monthId === activeMonth));
  }, [activeMonth]);

  const selectDate = (date) => {
    const { handleSelect } = props;
    handleSelect(date);
  };

  const renderMonthTab = () => {
    let tabSection = [];
    let tabs = [];
    months.map((month, index) => {
      tabs = tabs.concat(month);
      if ((index + 1) % 3 == 0 || index + 1 - months.length == 0) {
        tabSection.push(tabs);
        tabs = [];
      }
    });

    return tabSection;
  };

  const prevSection = () => {
    if (activeTab < 1) {
      return;
    }
    setActiveTab(activeTab - 1);
  };

  const nextSection = () => {
    if (activeTab > 2) {
      return;
    }

    setActiveTab(activeTab + 1);
  };

  const renderNavigation = () => {
    return (
      <div className={Styles.navigation}>
        <div className={Styles.navButton} onClick={prevSection}>
          <i className={`icon-arrow-left ${Styles.iconArrow}`}></i>
        </div>
        <div className={Styles.months}>
          {renderMonthTab().map((section, index) => {
            return (
              <section
                key={index}
                className={`${Styles.tabSection} ${
                  index === activeTab ? Styles.active : ""
                  }`}
              >
                {section.map((month, i) => {
                  return (
                    <div
                      key={i}
                      className={`${Styles.item} ${
                        month.id === activeMonth ? Styles.active : ""
                        }`}
                      onClick={() => setActiveMonth(month.id)}
                    >
                      <TextS>{month.name}</TextS>
                    </div>
                  );
                })}
              </section>
            );
          })}
        </div>
        <div className={Styles.navButton} onClick={nextSection}>
          <i className={`icon-arrow-right ${Styles.iconArrow}`}></i>
        </div>
      </div>
    );
  };

  const renderDates = () => {
    return (
      <div className={Styles.dates}>
        {dateList.map((item, index) => {
          const { bottom, top } = item;
          return (
            <div className={Styles.item} onClick={() => selectDate(item)} key={index}>
              <TextS textStyle="bold">{`${bottom} - ${top}`}</TextS>
              <TextS>{moment(activeMonth, "MM").format("MMM")}</TextS>
            </div>
          );
        })}
      </div>
    );
  };

  const { show } = props;

  return (
    <div className={`${Styles.datepicker} ${show ? Styles.show : ""}`}>
      {renderNavigation()}
      {renderDates()}
    </div>
  );
};

export default DatePicker;
