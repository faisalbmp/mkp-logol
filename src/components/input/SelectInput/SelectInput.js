import React from 'react';
// import Styles from './selectInput.module.scss';
import Styles from 'assets/scss/selectbox/selectbox.module.scss';

const SelectInput = props => {
	return (
		<div
			className={Styles.selectBoxM}
			style={{ width: props.width, height: props.height }}
		>
			<select value='select-box'>
				<option>abc</option>
				<option>abc</option>
				<option>abc</option>
			</select>
		</div>
	);
};

export default SelectInput;
