import React, { Fragment } from 'react';
import {
	btnDownload,
	donwloadIfIcon,
	inputFile,
	activeBtn,
	textUpload,
	unError
} from 'assets/scss/button/upload.module.scss';

import LoadingSmall from 'components/loading/loadingSmall'
import { DonwloadIfIcon } from 'components/icon/iconSvg';

export default ({ onChangeFile, activeAction, name, error, loading }) => (
	<div className={`${btnDownload} ${activeAction ? activeBtn : error ? unError : ""}`}>
		<DonwloadIfIcon
			className={donwloadIfIcon}
			color={activeAction ? "#004dff" : error ? "red" : false}
		/>
		{
			loading ?
				<LoadingSmall />
				:
				<Fragment>
					<div className={textUpload}>{name}</div>
					<input
						className={inputFile}
						type='file'
						onChange={onChangeFile}
					/>
				</Fragment>
		}
	</div>
); 