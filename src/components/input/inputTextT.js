import React, { useState } from 'react'
import { UploadIcon, CheckIcon, DeleteIcon } from "../icon/iconSvg"

export default function InputText (props)  {
    return (
        <div className = "input-box-container"  style={props.inputContainerStyle}>
            {
                (props.textInputTitle != "" || props.textInputTitle != null || props.textInputTitle != undefined) &&
                <span>{props.textInputTitle}</span>
            }
            <div className = "input-text-box" style={props.inputTextStyle}>
                {
                    props.isPhone &&
                    <div>
                        +64 -
                    </div>
                }
                <input type = {props.inputType} disabled = {props.disabled} placeholder = {props.placeholder} value = {props.value}  onChange = {props.onChange} />
                {
                    props.isWeight &&
                    <div className = "weight-input-text">
                        <span>Kg</span>
                    </div>
                }
            </div>
        </div>
    )
}