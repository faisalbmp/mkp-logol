import React from "react";
import PropTypes from "prop-types";
import Styles from "assets/scss/input/textinput.module.scss";

const TextInput = (props) => {
  const { placeholder, value, onChange, className, fontSize } = props;
  return (
    <div className={`${Styles.container} ${className}`}>
      <input
        type="text"
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        style={{ fontSize }}
      />
    </div>
  );
};

export default TextInput;

TextInput.defaultProps = {
  placeholder: "",
  value: "",
  onChange: () => {},
  className: "",
  fontSize: "16px",
};

TextInput.propTypes = {
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  className: PropTypes.string,
  fontSize: PropTypes.string,
};
