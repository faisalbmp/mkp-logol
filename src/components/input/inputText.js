import React, { useState } from 'react';
import { UploadIcon, CheckIcon, DeleteIcon } from '../icon/iconSvg';

export default function InputText(props) {
	return (
		<div className='input-box-container' style={props.inputContainerStyle}>
			{(props.textInputTitle != '' ||
				props.textInputTitle != null ||
				props.textInputTitle != undefined) && (
					<span>{props.textInputTitle}</span>
				)}
			<div className={`input-text-box ${props?.disabled && 'input-text-box-disabled'}`} style={props.inputTextStyle}>
				{props.isPhone && <div>+64 -</div>}
				<input
					{...props}
					type='text'
					disabled={props.disabled}
					placeholder={props.placeholder}
					onChange={props.onChange}
					value={props.value}
					name={props.name}
					style={{ background: 'transparent' }}
				/>
				{props.isWeight && (
					<div className='weight-input-text' style={props.isWeightStyle}>
						<span>Kg</span>
					</div>
				)}
			</div>
		</div>
	);
}
