import React, { useState, useRef, useEffect } from 'react'

import { CheckIcon } from '../icon/iconSvg'
import { inputAuth, activeInput, icon, wrapping, active, label, wrepperIcon, input, textareaStyle } from 'assets/scss/input/inputAuth.module.scss';

export default function InputAuth({ Icon, onChange, title, activeIcon, name, type, styleInput, resize }) {
    const [value, setValue] = useState("")
    const [click, setClick] = useState(false)
    const refInput = useRef();

    useEffect(() => {
        const { current } = refInput;

        const handleFocus = () => {
            setClick(true)
        }
        const handleBlur = () => {
            setClick(false)
        }

        current.addEventListener('focus', handleFocus);
        current.addEventListener('blur', handleBlur);

        return () => {
            current.removeEventListener('focus', handleFocus);
            current.removeEventListener('blur', handleBlur);
        }
    });

    const setActive = (e) => {
        onChange(e)
        setValue(e.target.value)
    }
    // height: 164
    return (
        <div className={`${inputAuth} ${value.length ? activeInput : ""}`} style={styleInput, { height: 164, alignItems: "flex-start", padding: "24px 0px 0px 16px" }}>
            {/* {Icon ? <div className={icon}>
                <Icon />
            </div> : ""} */}
            <div className={`${wrapping}`}>
                {/* <div className={label}>{title}</div> */}
                <textarea
                    onChange={setActive}
                    ref={refInput}
                    type={type}
                    className={`${input} ${textareaStyle}`}
                    name={name}
                    style={{ resize }}
                    rows="6"
                    placeholder={title}
                />
            </div>
            {activeIcon ? <div className={wrepperIcon}>
                <CheckIcon color={value.length ? "#004DFF" : "#E0E5E8"} />
            </div> : ""}
        </div>
    )
}