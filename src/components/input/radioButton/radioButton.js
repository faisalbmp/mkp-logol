import React from "react";
import Classes from "./radioButton.module.scss";
const RadioButton = (props) => {
  return (
    <div className={props.class}>
      <label className={`${Classes.container}`}>
        {props.title}
        <input type="radio" name="radio" value={props.value} />
        <span className={Classes.checkmark} style={props.styles}></span>
      </label>
    </div>
  );
};

export default RadioButton;
