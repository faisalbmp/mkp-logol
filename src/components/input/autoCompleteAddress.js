import React, { Fragment, useState, useRef, useEffect } from 'react'
import { autocompletePlace, getPlaceId } from "services/apiGoogleMap"
import Styles from 'assets/scss/input/addressCompleted.module.scss';
import { textError, showTextEerror, wpAutoCompleted } from 'assets/scss/megamenu/megamenu.module.scss';
import { MapInputIcon } from 'components/icon/iconSvg'
import LoadingDot from 'components/loading/loadingDot';
import { useTranslation } from 'react-i18next';

export default ({ clickSuggestion, maxWidth, errorManual }) => {
    const [valueInput, setValue] = useState("")
    const [dataSuggestion, setDataSuggestion] = useState([]);
    const [showError, setShowError] = useState(false);
    const [loading, setLoading] = useState(false)
    const refInput = useRef();

    const { t } = useTranslation();

    useEffect(() => {
        refInput.current.focus();
    }, [])

    const onSearch = async (val) => {
        setValue(val.target.value)
        setLoading(true)
        const res = await autocompletePlace(val.target.value)
        setLoading(false)
        setDataSuggestion(res)

        if (!res.length) {
            setShowError(true)
        } else {
            setShowError(false)
        }
    }

    const closeAuto = async (value) => {
        setValue(value.description)
        setDataSuggestion([])
        clickSuggestion(value)
    }

    return (
        <div className={Styles.wpAutoCompleted} style={{ maxWidth }}>
            <input
                ref={refInput}
                placeholder="Search"
                value={valueInput}
                className={`${Styles.autoCompleted} ${errorManual && Styles.error}`}
                onChange={onSearch}
            // style={{ maxWidth }}
            />
            <div className={Styles.wpTextSg}>
                {
                    loading ? <div style={{ margin: 20 }}> <LoadingDot /></div> :
                        dataSuggestion.length ?
                            dataSuggestion.map((value, index) => (
                                <div
                                    key={index}
                                    className={Styles.textSg}
                                    onClick={() => closeAuto(value)}
                                >
                                    <MapInputIcon />
                                    <div className={Styles.textP}>{value.description}</div>
                                </div>
                            )) :
                            <div className={`${textError} ${showError ? showTextEerror : ""}`} style={{ margin: 15, fontSize: 14 }}>{t('alert.dataNotFound')}</div>
                }
            </div>
        </div>
    )
}