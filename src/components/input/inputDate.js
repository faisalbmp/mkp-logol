import React, { useRef } from 'react';
import ReactDatePicker, { CalendarContainer } from 'react-datepicker';
import DatePicker from 'react-datepicker';
import 'assets/scss/component/react-datepicker.scss';
import { DropDownArrow, DropDownUpArrow } from '../icon/iconSvg';
import PropTypes from 'prop-types';

const ExampleCustomInput = props => {
	const { onOpen, error } = props;
	return (
		<div>
			{(props.label != '' || props.label != null) && (
				<div
					className={`depo-checkbox-container-title ${
						error && 'depo-checkbox-container-title-error'
					}`}
				>
					<span>{props.label}</span>
				</div>
			)}
			<div
				className={`select-box-header ${error && 'select-box-header-error'}`}
				onClick={onOpen}
				style={props.headerStyle}
			>
				<div className='select-box-header-title' style={props.inputStyle}>
					{props?.selectedDate ?? props.placeholderInput}
				</div>
				{<DropDownArrow color='#333333' />}
			</div>
		</div>
	);
};

const MyContainer = ({ className, children }) => {
	return (
		<div style={{ marginTop: 16 }}>
			<CalendarContainer className={className}>
				<div style={{ position: 'relative' }}>{children}</div>
			</CalendarContainer>
		</div>
	);
};

const InputDate = props => {
	const refPicker = useRef();

	const onDatePicker = async () => {
		refPicker.current.setOpen(true);
	};

	if (props.pickerType === 'custom') {
		return (
			<div>
				<ReactDatePicker
					ref={refPicker}
					style={{ width: '100%' }}
					// minDate={new Date()}
					dateFormat='d MMMM yyyy'
					selected={props?.selectedPicker ?? new Date()}
					onChange={props?.onChange}
					customInput={<ExampleCustomInput {...props} onOpen={onDatePicker} />}
					calendarContainer={MyContainer}
				/>
			</div>
		);
	}

	return (
		<div style={props.style}>
			<DatePicker className={props.class} dateFormat='d MMM yyyy' selected={new Date()} />
		</div>
	);
};

export default InputDate;

InputDate.defaultProps = {
	pickerType: 'default',
};
