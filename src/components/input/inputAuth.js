import React, { useState, useRef, useEffect } from 'react';

import { CheckIcon, ErrorIcon } from '../icon/iconSvg';
import {
	inputAuth,
	inputOutlined,
	activeInput,
	icon,
	wrapping,
	active,
	label,
	wrepperIcon,
	input,
	inactiveInput,
} from 'assets/scss/input/inputAuth.module.scss';
import { validationPhone, validateEmail } from 'utils/validation'
import PropTypes from 'prop-types';

export default function InputAuth({
	Icon,
	onChange,
	title,
	name,
	type,
	styleInput,
	onclick,
	rounded,
	isValidateView,
	activeManual
}) {
	const [value, setValue] = useState('');
	const [click, setClick] = useState(false);
	const refInput = useRef();

	useEffect(() => {
		const { current } = refInput;

		const handleFocus = () => {
			setClick(true);
		};
		const handleBlur = () => {
			setClick(false);
		};

		current.addEventListener('focus', handleFocus);
		current.addEventListener('blur', handleBlur);

		return () => {
			current.removeEventListener('focus', handleFocus);
			current.removeEventListener('blur', handleBlur);
		};
	});

	const setActive = e => {
		onChange(e);
		setValue(e.target.value);
	};

	let acSet = type === "text" || type === "password" ? value.length : type === "email" ? value.length && validateEmail(value) : type === "number" ? value.length && validationPhone(value) : false
	let placeholderColor = "#868a92";
	let textColor = "#000000";
	let validationIcons = !acSet && value.length > 0 && isValidateView;

	if (value.length > 0 && !acSet && isValidateView || activeManual) {
		placeholderColor = '#EA4B4B';
		textColor = '#EA4B4B';
	}

	if (onclick) {
		validationIcons = !acSet && onclick;
	}

	return (
		<div
			className={`${inputAuth} ${click && activeInput} ${acSet ? activeInput : (value.length > 0 && isValidateView || activeManual) && inactiveInput} ${rounded && inputOutlined}`}
			style={styleInput}
		>
			{Icon ? (
				<div className={icon}>
					<Icon color={placeholderColor} />
				</div>
			) : (
					''
				)}
			<div
				className={`${wrapping} ${click || value.length ? ' ' + active : ''}`}
			>
				<div
					className={label}
					style={{ color: placeholderColor }}>{title}</div>
				<input
					onChange={setActive}
					ref={refInput}
					// type={type}
					className={input}
					name={name}
					type={type === "number" ? "" : type}
					style={{ background: 'transparent', color: textColor }}
				/>
			</div>
			{!rounded && (
				<div className={wrepperIcon}>
					{
						validationIcons || activeManual ?
							<ErrorIcon />
							:
							<CheckIcon color={acSet ? '#004DFF' : '#E0E5E8'} />
					}
				</div>
			)}
		</div>
	);
}

InputAuth.defaultProps = {
	isValidateView: false,
};

InputAuth.propTypes = {
	isValidateView: PropTypes.bool,
};
