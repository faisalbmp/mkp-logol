import React from 'react'

import { inputNo, input, btnSelect, click } from 'assets/splitPage.module.scss';
import { SelectUp, SelectDown } from 'components/icon/iconSvg'

export default function InputNumber({ setInput, value, name, clickUp, clickDown }) {
    return (
        <div className={inputNo}>
            <input type="number" className={input} onChange={setInput} value={value} name={name} />
            <div className={btnSelect}>
                <div className={click} name={name} onClick={() => clickUp(name)}>
                    <SelectUp />
                </div>
                <div className={click} name={name} onClick={() => clickDown(name)}>
                    <SelectDown />
                </div>
            </div>
        </div>
    )
}
