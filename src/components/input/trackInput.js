import React from 'react';
import { withTranslation } from 'react-i18next';

function TrackInput({ t }) {
    const translate = t;
    return (
        <div className="track-input">
            <input
                className="input-search"
                placeholder={translate('trackYourShipment')}
            />
            <button
                className="btn-track"
            >Track</button>
        </div>
    )
}

export default withTranslation()(TrackInput)
