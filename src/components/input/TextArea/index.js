import React from "react";
import PropTypes from "prop-types";
import Styles from "assets/scss/input/textarea.module.scss";

const TextArea = (props) => {
  const { placeholder, value, onChange, className } = props;
  return (
    <div className={`${Styles.container} ${className}`}>
      <textarea placeholder={placeholder} value={value} onChange={onChange} />
    </div>
  );
};

export default TextArea;

TextArea.defaultProps = {
  placeholder: "",
  value: "",
  onChange: () => {},
  className: "",
};

TextArea.propTypes = {
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  className: PropTypes.string,
};
