import React, { useState } from 'react'
import { CounterIncrementIcon, CounterDecrementCounter, UploadIcon, CheckIcon, DeleteIcon, DropDownArrow, DropDownUpArrow, SelectSearchIcon } from "../icon/iconSvg"

export default function Autocomplete (props)  {
    return (
        <div className = "input-box-container"  style={props.inputContainerStyle}>
            {
                (props.title != "" || props.title != null || props.title != undefined) &&
                <span>{props.title}</span>
            }
            <div className = "input-text-box" style={props.inputTextStyle}>
                <SelectSearchIcon/>
                <input type = "text" placeholder = {props.placeholder} value = {props.value} onChange={e => props.setChange(e.target.value)}/>
                {


                    props.openList  && 
                    <div className = "select-box-list-container"  style = {props.selectBoxListStyle}>
                        <ul>
                            {
                                props.arraySuggestion.map((item,index) => {
                                    return(
                                        <li key={index} className = "select-box-list-value" style = {{width: '396px'}} onClick={() => props.setItem(item)} >{item}</li>
                                    )
                                })
                            }
                            
                        </ul>
                    </div>
                }
                {
                    props.openList 
                    ? <DropDownUpArrow/>
                    : <DropDownArrow/>
                }
            </div>
        </div>
    )
}