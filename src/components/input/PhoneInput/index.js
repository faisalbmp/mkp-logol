import React from "react";
import Styles from "assets/scss/input/phoneinput.module.scss";
import { TextM } from "components/text";

const PhoneInput = () => {
  return (
    <div className={Styles.phoneInput}>
      <div className={Styles.select}>
        <TextM>+62</TextM>
        <i className="icon-arrow-down" style={{ fontSize: "7px" }}></i>
      </div>
      <input
        className={Styles.textInput}
        type="text"
        placeholder="0812xxxxxxx"
      />
    </div>
  );
};

export default PhoneInput;
