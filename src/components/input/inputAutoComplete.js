import React, { useEffect, useRef } from 'react';

import {
	wrapperInput,
	inputCompleted,
	suggestion,
	centerIcon,
	containerIn,
	itemScss,
	itemText,
} from 'assets/scss/input/inputCompleted.module.scss';
import { SearchIcon, MapInputIcon, CloseInputIcon } from '../icon/iconSvg';

export default function InputAutoComplete({
	setChange,
	value,
	arraySuggestion,
	setItem,
}) {
	const refInput = useRef();

	useEffect(() => {
		// const { current } = refInput;
		// current.focus();
		// current.addEventListener('focus', () => setClick(true));
		// current.addEventListener('blur', () => setClick(false))
		// return () => {
		//     current.removeEventListener('focus', () => setClick(true));
		//     current.removeEventListener('blur', () => setClick(false))
		// }
	});

	return (
		<div className={containerIn}>
			<div className={wrapperInput}>
				<input
					name='autoComplete'
					ref={refInput}
					autocomplete='off'
					onChange={e => setChange(e.target.value)}
					value={value}
					className={inputCompleted}
					placeholder='Tulis Lokasi disini...'
				/>
				<div className={centerIcon}>
					{value && value.length ? (
						<div onClick={() => setChange('')}>
							<CloseInputIcon />
						</div>
					) : (
						<SearchIcon color='#000000' />
					)}
				</div>
			</div>
			<div className={suggestion}>
				{arraySuggestion.length
					? arraySuggestion.map((item, index) => (
							<div
								key={index}
								className={itemScss}
								onClick={() => setItem(item)}
							>
								<MapInputIcon />
								{/* <a style={{ marginLeft: 11, color: "#000000" }}>{value}</a> */}
								{item.description ? <a className={itemText}>{item.description}</a> : <a className={itemText}>{item}</a>}
							</div>
					  ))
					: ''}
			</div>
		</div>
	);
}
