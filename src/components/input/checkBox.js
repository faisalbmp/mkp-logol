import React from 'react';
import PropTypes from 'prop-types';

import {
	roundedOne,
	inputCheckBox,
	label,
} from 'assets/scss/checkbox/round.module.scss';

function CheckBox({ name, checked, onClick }) {
	return (
		<div class={roundedOne}>
			<input
				type='checkbox'
				value='None'
				id={name ? name : 'roundedOne'}
				name={name ? name : 'check'}
				className={inputCheckBox}
				checked={checked}
				onClick={onClick}
			/>
			<label className={label} for={name ? name : 'roundedOne'}></label>
		</div>
	);
}

CheckBox.defaultProps = {
	onClick: PropTypes.function,
	checked: PropTypes.bool,
};

CheckBox.defaultProps = {
	onClick: () => {},
	title: false,
};

export default CheckBox;
