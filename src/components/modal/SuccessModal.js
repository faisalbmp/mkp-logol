import React from 'react';

import WrepperModal from './wrepperModal';
import {
	wrepper,
	bg,
	iconAlert,
	titleAlert,
	subTitle,
} from 'assets/scss/modal/successPopup.module.scss';
import { SuccessAlert } from 'components/icon/iconSvg';

export default function SuccessPopup({
	show,
	onClose,
	title,
	textSubTitle1,
	subTitleStyle,
	titleStyle,
	textSubTitle2,
}) {
	return (
		<WrepperModal show={show} onClose={onClose} width={631.67} height={497}>
			<div className={wrepper}>
				<div className={iconAlert}>
					<SuccessAlert />
				</div>

				<div className={bg}>
					<text className={titleAlert} style={titleStyle}>
						{title}
					</text>
					<text className={subTitle}>{textSubTitle1}</text>

					<text className={subTitle} style={subTitleStyle}>
						{textSubTitle2}
					</text>
				</div>
			</div>
		</WrepperModal>
	);
}
