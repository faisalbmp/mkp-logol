import React from 'react';
import {
	modalScss,
	modalContent,
	active,
	animation,
} from 'assets/scss/modal/modal.module.scss';
import PropTypes from 'prop-types';

function WepperModal({
	children,
	show,
	onClose,
	width,
	height,
}) {
	var modal = document.getElementById('modal');
	window.onclick = function (event) {
		if (event.target.id) if (event.target.id === modal.id) onClose();
	};
	document.onkeydown = function(evt) {
		evt = evt || window.event;
		if (evt.keyCode == 27) {
			onClose();
		}
	};
	return (
		<div id='modal' className={`${modalScss} ${show ? active : ''}`}>
			<div
				className={`${modalContent} ${show ? animation : ''}`}
				style={{ maxWidth: width, maxHeight: height }}
			>
				{children}
			</div>
		</div>
	);
}

WepperModal.propTypes = {
	children: PropTypes.object.isRequired,
	width: PropTypes.number.isRequired,
	height: PropTypes.number.isRequired,
	show: PropTypes.string.isRequired
}

export default WepperModal