import React, { useState } from 'react'

import WrepperModal from './wrepperModal'
import {
  wrepper,
  content,
  header,
  textHeader,
  wrapperTambahBaru,
  formInput,
  wrepperCustomer,
  wrepperHeaderCustomer,
  textNpwp,
  textNama,
  wrapperForm,
  textLegend,
} from 'assets/scss/modal/gatepass.module.scss';
import InputText from '../../components/input/inputText';
import { Close } from '../icon/iconSvg';
import { ItemExsistingCustomer } from '../../components/gatepass';
import { withTranslation } from 'react-i18next';

function GatePassNPWP({ show, onClose, t }) {

  const translate = t;

  return (
    <WrepperModal
      show={show}
      onClose={onClose}
      width={696}
      height={455}
    >
      <div className={wrepper}>
        <div className={content}>
          <div className={header}>
            <text className={textHeader}>{translate('gatepass.onbehalfNpwp')}</text>
            <Close />
          </div>

          <fieldset className={wrapperForm} >
            <legend className={textLegend}>{translate('gatepass.addNew')}</legend>
            <div className={wrapperTambahBaru}>
              <div>
                <InputText
                  textInputTitle={translate('gatepass.onbehalfName')}
                  placeholder={translate('gatepass.onbehalfName')}
                  styleInput={{ fontSize: 14 }}
                  inputTextStyle={{ width: 192, height: 42 }} />
              </div>

              <div className={formInput}>
                <InputText
                  textInputTitle={translate('gatepass.onbehalfNpwp')}
                  placeholder={translate('gatepass.onbehalfNpwp')}
                  styleInput={{ fontSize: 14 }}
                  inputTextStyle={{ width: 192, height: 42 }} />
              </div>

              <div className={formInput} style={{ flex: 1 }}>
                <button className="next-button"
                  type="button"
                  onClick={() => { }}
                  style={{ border: '0px', height: 48, width: 96, marginTop: 20, }}>
                  <div className="next-button-text" >{translate('save')}</div>
                </button>
              </div>
            </div>
          </fieldset>

          <div className={wrepperHeaderCustomer}>
            <text></text>
            <text className={textNama}>{translate('gatepass.onbehalfName')}</text>
            <text className={textNpwp}>{translate('gatepass.onbehalfNpwp')}</text>
          </div>

          <div className={wrepperCustomer}>
            {
              [1, 2, 3, 4].map((item, index) => <ItemExsistingCustomer index={index} />)
            }
          </div>

        </div>
      </div>
    </WrepperModal >
  )
}

export default withTranslation()(GatePassNPWP)