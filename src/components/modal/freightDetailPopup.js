import React, { useState } from 'react'
import WrepperModal from './wrepperModal';
import Styles from 'assets/scss/modal/freightDetailPopup.module.scss';
import { Close } from 'components/icon/iconSvg';
import { Ringkasan, DetailBarang, Kontainer, Dokumen } from 'pages/freight/modal/index';

export default function FreightDetailPopup({ show, onClose }) {
  const [menu, setMenu] = useState(
    {
      label: 'Ringkasan',
      screen: Ringkasan,
    },
  );
  const menus = [
    {
      label: 'Ringkasan',
      screen: Ringkasan,
    },
    {
      label: 'Detail Barang',
      screen: DetailBarang,
    },
    {
      label: 'Kontainer',
      screen: Kontainer,
    },
    {
      label: 'Dokumen',
      screen: Dokumen,
    },
  ];
  return (
    <WrepperModal
      show={show}
      onClose={onClose}
      width={1074}
      height={670}
    >
      <div className={Styles.parentContainer}>
        <div className={Styles.header}>
          <div className={Styles.wrapperText}>
            <text>Detail Kontainer</text>
          </div>
          <Close />
        </div>

        <div className={Styles.menuWrapper}>
          {
            menus.map((itemMenu, index) => (
              <div
                key={index}
                onClick={() => {
                  setMenu(itemMenu);
                }}
                className={itemMenu.label === menu.label ? Styles.menuItemActive : Styles.menuItem}>
                {itemMenu.label}
              </div>
            ))
          }
        </div>

        <div className={Styles.childrenWrapper}>
          <menu.screen />
          {/* <Ringkasan /> */}
          {/* <DetailBarang /> */}
          {/* <Kontainer /> */}
          {/* <Dokumen /> */}
        </div>

      </div>

    </WrepperModal >
  )
}
