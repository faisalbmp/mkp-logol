import React from 'react'

import WrepperModal from './wrepperModal'
import Styles from 'assets/scss/modal/gatepassContainer.module.scss'
import { ActionGateppas, Close } from 'components/icon/iconSvg'

export default function GatepassContainer({ show, onClose }) {

  const onDowload = () => {
    fetch('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUTEhIVFRUWFxUWFhUVFxUVFRcVFRcXFxUVFRYYHSggGB0lHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGi0fHyUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAR0AsQMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAACAQMEBQYABwj/xABEEAACAQIDBAcEBwYFAwUAAAABAgADEQQSIQUxQVEGImFxgZGhEzKxwQcjQlJi0fAUcoKisvEVJJLC4TNDYxZTc4Oj/8QAGQEAAwEBAQAAAAAAAAAAAAAAAAECAwQF/8QAIxEAAgICAgMBAQADAAAAAAAAAAECEQMhEjEEE0EiURRhgf/aAAwDAQACEQMRAD8Awi0o6lKKqx1FM7jy6Q/TWcyxyksJlkrsurQwEhhZ2WGFlkUIFihYWSJljQqCUQ7QVhQGdOvBiiMQoMXNEgmMB9Gh5yIwjQy0kaJlDHsJaYba3bM4WnCpGS0ja0dog8ZKp4vtmFp4wjjJ2H2n2x0ido2QrwvazO0Nog8ZMTFgxcQ5Ft7SLK39pE6HEOR5vaOpGRHEkGpMpCGywaENzJ+mnwZhZoy7wc00SMrHXeNmpBnBLykiHJsVXjOL2ilP3jryH60kxqeWnm+0d35zLY+kbk7z27u0zjy+TUuMT0MHiXHlIs6fSBDupse+3yMlDaNPLmLZe/h32vMsFsdde/8AKPq6kagDQ6218oo5pFSwQ/hqErgi6sGHMajznGpMjs/HNRqXNwpOvEeImrUhgCNxnRjyqTp9nLlwuKtbQYqxTWjZWIRN6Oa2GasT2sC0Ewomxz2sT2saMSFBZKp4wjjJ2H2r2ymMG8KE2aX/ABUc50zeedGFIXPDV4xDEzSNeRMp1pzV5GEIQ4g5MLNCBggQxKJ2KJKwWHLsAOJt+d+UjqJdbLpEIXA1YlV7NNSPWYeRl9cGzp8bD7MiQG1EUL1QLA5V7QuhPp5CZbF4a51B3Zm524Ca80PaVMvBdOwAAXPnYecrcVSAJtv393Cn6a+U8WDd2z35JVRj6lHUk7hvkfcb+nZf5mWGKGZwg3Df2nl3DW5jeJohnAXjbwA5/rjOuEjknAFcKHG7iPL9Wk/ZJyhdbq2n7rcR8Zo9g7DBpMbamyr4kaygrYY0nrU7bmFRewXF7esr2btE+rVMsWWARJDjS/PWN2nqRdqzx8keLoZIgkR4rByyjKhoiCRHssErABkiARHysBlgKhu06FlnQFQoWEEh5YYWZm1ABYQWR8Rj6abzc8l1Pidw8ZU4zbz7qYVe1iGPkJLyJFxxSkW1XHU0qGm5yMPvCwPcd0l4OrSdsvtUDWJAzC5sLnjppMjtHFLWpJmfPWUHMbW4mwHOwsPAcpV02uRMXlkzpXjxNHtLpCQbUdwPWJHvAEGwvuGm/f3TW9BsCcTVLJXZlW7ZWYnKAjG1vSeZVpoPo720uExyVHLBGWpTbL/5EKKSOIBIMxybWzfFFJqj1TC7NZEqE2a5KEi+hu2bXx+N5Hfo+tQlqtQqm8pTsLgcGdtw8PGXOOqKVFNSxDM1S6kLmuSUF9xuDe2nCVFDYOIqgmpn3HKpIsovYMwHVzaTiarZ6WumV+P2zRwiMmBpBWIP1ljkvb32qNdqpF9FFxpwmV6PbHZ2FlYg8SNW8OA/RmxwvRqstQs1Z3GSyqSbBr/a0sRw0HlNH0f2e2Y3FgOW69tQDxlpkvb3qhdk4JEUBiBYbiRv7+J+HnMP0zpKtViLaodRyzL+Zml2js3EviCLItJVOS6hmL8LgkadxB7Zj+ndJ6WQ1aao1RCvUJsStr2J7xGkS32JhagamjDlby0+UQyB0ZrZqRB+yxHgQD+csGE9TA/yeN5K/QJgw4k3OSgSIJEMzoANlYBWPRDEFDWWdHJ0AKittkC+gHefjbj2C8qMXtdm3XPoPKV7G+p9dfK26OU6d+6cLbfZ6ShGILsWGt+zl4DhGqiW3yTVoW1BMtMHsF6iBmcLfUApc24G9xHxbHzSVmcB1jtEambDD9HKKoVNyzCxfQEceqNbbu2MjokpNhVYDkVBPncfCHqktiWeD0ZpzAVdeybnD9EMMq3q1KrX3KpRbn/ST6ydgOimDfFpTyn2apepeo5zPb3QwI+0QNN8yeWN0brHKrM7g+lFZEoUsxYIbHNc3Bay2P4VAHhPdcHs2uMPTrLVVw1NWykFSMwBIvqD6TxDpBsqlTarkAA9sFpgaWCre48SD5z2P6OekiYjBJTJs9EezZTv6uim3Ii0xkovZvGc0q7DO0bKfaKU0PW3i/AWF5d7KwrJST2gysxY5TvEq3ohr5qTslicy5cunAgm+vYDImztrp7q062UG4BubX03k6bt0zTo63hnlVwWi/LrmPZPI/pyxymrhqSkXUVHYcg5VV/paeg7V2glFGr1T7NVUlr7+wabzwt2z5923tR8ViKld97toN+VRoq+AA9ZtjVs48tJUaPootg4PEKfT/mWzCV3RvfbmgA8P7S3dJ2+N0zzvLW0R7RLR4rEyzqOEatEtHCsEiAAWgmOEQCIhgzotosAMFiwQx32OoIOljusYNGob2ufE/mIdNshyVB1fUfiU/KClG+5hPPjZ6rqh2sxtwNv12iSztquf+5a+6ypbu1HzkM0yIGSwAPcfEzTZnUWTP8AFsUP+7/Kn5S42b0vCke1oZvvFWtf+Ej5zP0jcWO8QXpaxO67BcU+jTY7pYrtnVDfgGsAvlf9d8LZu28t3Z1DEGwuNAd5PmdP0MwEgvTmPoXw3/yH9NG1X9ocWPVQOSd9+Z+MT9tfCV0q0mNnVGNjr1lDEEd5J7bmZukTffu/Xzk1agBTNqBbyH9ouFFKf0+gOjnSiniaAKuA4FiPyBnV2oYdGruyqqgsSbADu7eyeb7NeiyZqOXcMwG9e/ipmZ6RYurVOVqjMBqqFmsO0C9ibTOMLZr7uCfH6B0x6VVcbUNyVog9Sn/vfm3oOHEmlw1PW/KMgaybhalh3n4fr1nRVKkc129ml2eLAMPshW8A3W9LzSVE1mbwhAU8RZh/qWaeoJfivbRn5kfymRysTLHTOtO48wYKwSskFYOWAyOVgFJKyQSkQIjZJ0k+znQCzPbU2YtUEgDN5Bj2nge3zvMtiMIyE6HTffQi/Aj57jNwI3isEtQa6MNzDeOztHYZzTxfUdePPWpGKpvHSLiTsbshlO6x4W91v3Sdx/CfA8JDS40O+Zr+Gz/qBHBvAx+0AaQ1MtIzbFCzmTSEDFtHRNkOlR60l1aYFlJsf1oZyi2o3xrFDcN51JPfu/XbM5QN4ZAiz02zISp7PXvEcqY0tqbBhYhgOX67fXRS10FxccDxHYeYjCjW0lUynaIjamP5dBGbWMko2/kfjG0BcbHxOqqdc1h4g2/XhNlSqZlvxG8cjymD2eSCG+6fXhNhsvEZtOd/jfX185nB8ciNJx5Y3ZKiwrTrT0zxxJ1oQE60ABtOywrRIhiZZ0KdARTKIYEVVjgWQWAVBFiARxBlVtvYgCe1Vhl3XN7qfutLrLFelmRqZ91xlPZyPgdZnkx8la7NMU+Lp9GAroy6MLfrT4zqbSS6MB7NxqudO5qfWW38JYfwiQfZ8RMYtnU0iaphASCtQjfH6eImiZlKDXQ+ILC++ErAzjKIFsMoS+pObuFrD5+YjXsolH37nnJmMohWAPgfGYcf0dN/kq3ThATTuk2unPwP5xt00Nu/9efpG9BF2iVTayd5ubenzmzoU+urDiF/mCD5GYrA6rlPh4bvD3vKbLY1TNlPDKPRbj4TCS/SOiL/ACyytOtCYxJ6aPIaOiTp14yRIJixDABLzok6MCuUx0GMLHVMzKHhCAjYjtJhxgIp9s4ABxVtoxTN+8pyk+KO/wDpmTemVM9HVA/Vbc2nnpf1nnx1vMONNnUp3FDWUGA1GOZYQMdDuuiMLiP0sRzh5bwGpQ2gbT7HbA6iWYb2iWPvLqO0aXHwlMtxJ2HeOrJtxAdSINDQ24cjJTG8aalG42iI5OLETDlWBXdfdy5TR7CY3Kk2udDyP2fI2lDSudOcm4bEupAIv+IEcOJ7ufZOXLCjuw5FKzWK9wDuPEciND6zrwae69t5J+F/XNOnbilcEzz864zaCvOvBiTQxCvEJiRIALOg3nQAr1EcWBDEzLDEIGBCEYEnBC9RBzZfiJ56OM9C2f8A9VP3gfLWee21mcuzWHRxEEw7QWiKQghCADHQIwYBEcpziIqRkt6H1MNbxtRHVjM2FTGolvgMOhZw7hQCAb8sob5keEq8tiL8YtSpozc2a/Zu0mc4Kao2xZHDdG0LAgW3AWHdz8d8C0jbOqXpIfwr8JJvN4pJUjnnJydsS060W8SWSJaCYZgGIBJ06dALK0NDDSMpjimQVZIBhiMgw1MYE3AGzFvuox9LfOefrNhiQ5QqlQ0ydLgA30Oh427uXhMlisJWpHrqCPvD3fMbvGZSTTs2hTjSBAjTwlxAG8EesOjhXqXNNGe1r5AWIvu0Gsnki1F2MCOrAqIVOVgVPJgQfIx1I0wkEJxWCdI4sozYVIyThVuw75GkzC77xkskVaedSeIOnhaQcSbBu12P+oK3zlngR1pB2xSym3M39FHyk/S/hodkt9Snd8JMzSt2G16K95+MnTePRhLsczTs0bvOvGIcLRM0bvOzQEOXnRrNOgBVKY6pkZTHlMzLH1McBjCxxYxDoMVlBFiLg7wdQe+AIYgNGc2xsfKCye6eHFTy7oXQHGGnjqIvZajim1xfeerpcfatr2maMgEWIuDvEn/Rn0XzbTzmmlSilNnzVFDhWzLkKg6CpfceWYzmzQ0/4deDJv8A2ehvsV6o1GZfxUxl/mbWUmP+jug639iKf4qahLeCtl8xPUpX4jZaEEp9Wfw6L4jdOOGJR6OyWeU9On/w8Xx30YG31WJBPKolv5lPymfxvQjHUrn2JcDjSIfd+HRvSezvQsxGbUadnmI/hQwPMWnSskkc0scWfOboVbKwIYbwQQR3g6iSMMZ9BbQ2TQri1akjj8Sgkdx3jwmP2v8ARhTN2wjmm3/t1CWQ9gf3l8bzRZF9MZYn8MFgF1B8PMSv29/1AOSj4tLfEbNr4V/Z16bIeF9zC+9WGjDulJt2p9eOWRfW7fOV9FWi36Pn6q3ImWZEpejj+8PGXV5vDowl2daJaFEjJBtBMdC3itSisri2MToWWdCw4spFjqmMLJNBbmQ2CVhrHAZaYfBC26R8VhMslZFdGrxOrIwMMGNiO0kJIABJOgA1JJ3AS7M6HKakmwGvIce6ezdBNjnD4UZhapUOd9LEXHVU9w4cyZE6FdEFwyitVGauRuNrUrjVRzbgT4Dt1s5suTlpHVjx8dsWVu2nskspT7cOkxNkUFMSThwc3gf16yLSMlYfee75iMGSgZIpGRJIpNAQeMwNOshp1UV1PBhx5jke0azwv6S+jpwuJGW/s2HUY8VHAnmLkdwB4z3qmdZivpjwYfBLU406qeTgofUp5S8bpkZFo8Z2JiClUA7m0/Kay8x5p38OM1VFrqp5gfCdcDjlseJiXjZMkYWlmltkrbHsOl4/UpaSdhcNDrUZzSlbO2KSiUmUzpY/s06VZFGSp05Jo07GKi2jyGZcmbcVRebPII1nY5ARIFHE2j1TFC0ycWmdMJwcKZVPTs1psvo22SKuJ9owutEZv4zonzP8Myr6mewfR/s32OEViOtV+sPcdEH+kA+Jm05/k5FBJ2aSBVqhd8MmRUs5ccPd/OYFj1R9Lg8L+EoMY5YXuLektNmVSQyN9nTwlPtil7JHI4A277aQGQUS0m4elxkBRu7pZ7ON1Ydtx+UYMILHVpwCdf1yklDcQEKhlL0+2ea+z8RTUXb2ZdAOLUiKgA78tvGWofTTmR5G0b2jXKorAXtUUHuNx87eMEB82YWrcX5zR4Y9Re4Sg2zs/wDZMZWofYSoQvYjWanf+BlmgpbgJ2QZxzhTClngWAkZKekcU2hKdmkMVbL2nXFoIrayvptJNNZzWdKiSridG7Tocg4mXaC4PCdQSPmkZN7HWiE1UiJRcmFVSOYejN+Sow4Oyx2JgzXr06Q+26qexb9Y+AufCe+ooAAAsALAcgNwnl/0XbOzYh6pGlJLD96poP5Q/nPUphN2zRKhnF1cqluQJ/KRNie5c8dYO3H6luZ9BHdlCyyCjsRTytmHE6+Epel9S6Iv33Ueo/KaSst5k+kZvXpJyOby1+UAXY0okrZzWJEj2juFOsoCXfrHw+EfpNY2kQN9Z/CPi35SRxgIZw7daovJv6gG+cfr0w1N1OmlweRXUH0EhUmtiHHNUbyuPyk8b7cxbziA8O+l/D5MbTq26taipPayEqfT2cj7GbPTB3kaGaT6dMJ/l8NVO9KjUz/9iZredKefdHtpeyIY6odHHZzHaLzoxy0Z5I9M21KnFalLTC0VYBlIIIBBG4g7jHMRhhJLsqM1pNw1W8hYpReN06tpDBT2XeadKn9r7Z0k05IrMO+lpaKgKyoXQyzpVNIMaa+kPHU7C8YwtWObQqSPg6JcgKLkkAdpOglrozvZ7R9G+EyYTPbWqzN/COqP6SfGaoSHhMOKFBKY3U0VB2kAC/iZNEzGUu2n6wHISfgB1JT457uT2y5wHuiJA+iRxmOx7Z8b3Zh5IfnNhUNgT2TF7JOfElv3j5/3gxonHDQ6WGMnldYdNNYxWVdRbVR+4P6mj53xysl64H/jH9TTsRTsYAVNZ7YpPxIR5EmWjGUu1Xy1qLdpHqv5mW7RgY/6Z8Pm2ZVb7j0X/wD0CX8nM8KwrWT5DfPovp5Q9ps/Fpa/1DsB2opYeqT5rGJ3HdblLxuhNWqPS/o62ialBgwsEfKvHQgEj19ZpsXWFp5d0c6V+wApuL076FQAUvvJt7w9e/dNqcaHUFSCCLgjcRBhVDOLq6xjPFYXMVlsJDkiuF7Gs5ixrNOlckRTK/OQZdbJ62+Zk4u8scHtHKdJM1rReOajO2anE7PuOBln0J2MGxdPTqofaH+DUfzZZmaO12PK03f0d7Sop7Q1HC1GIVQ1wMo1PW3am3lMY812dufNiyR1GmbvaVXVE+8w8ryViHsrHkDKFa5q4pCuqA6HhZQTe/fLHbdfLT7zbwmlnFRSl7mX2CO4cLTOYZtees0Ozd0SHIPbFXLRc9lvPT5zK9Gxesx5Ifisu+ldW1JV+83oB+ZEp+jC9dz+EDzP/EL2CWrLuod8cpPGnMWjLIAY/wCZX/4v9xk3F0riVlR/81T7advVzLkxDMX0lUgKeTH1H/Et6b5lB5gHzjHS2h9XmHBgfl84OyHzYdDyuvkfytAdasdxiBgFO51dD4j+8+Uq+HKOyN7yMVPepIPwn1ZjD9Xcb1ZW9bfOfOX0gYQUtp4lRuNT2g7qoFT/AHxrsaKSgNbBSxOgUAkm/C03PRrCvTpZXBF2JCk3Kg8NN3O3bMdRVx9ciNkVh9ZlOQHSwLbgd03uDxIqqtRTowv3HiPA3EqTpAtsnKIxiHjNeuRIyOWMxp9ml/Ap0cyTociaMT+0x+ji7SlLGcHMqzP1mtwuO7Ze4DGTzuliWEt9n7SfSA+uz0XBYuohzKzKeakg+YlrV6S4g73zW+9c/OYvAY1jaXNB7iKv6NtfD06iiAArcaht99ZLwG1hT6rC4Jve+4HfpaUVPEHKO6N+1JmHNo6PWn2WHSHalKo461lAtrpvOp18ImwsfRTNdwc1txvuvy75nNpG8rMg5QWR3ZfpVUei/wCK4c7qyb9xIB075YYV0YdV1YcwQZ5lTw6kbvUy36PhixT2jBFRiFv1bkjW3iZfu/qIl4yq0zTYsEYql3AeZaXdjPFsft7F0q7IMQWCN1SypcDeBe3bJuA6WYsEs1UuD9k+6CeVtfWNZUEvFmv4em9IaWag/cD5EGU3RnWlUX7rA+Y/4lB/6txLqVbIQRYjKdx7byPhNrVlvkbLe18o5d94exCWCSVM2T0yVZeYIHlPEvpL2QauPoMvu1aaK5H2SjFSD25Mthvm+xuIqtSqN7aoCEYjKxXUAn7NuU8+2liTiMisLZBYG9yW0uxJ3k2E0i3JWifWotKTPR9mVadKklOmAqIoAUcf7k/GSv2tHUo9NXU7xYDyPA9swezcZUWyM2YW0ve4t2km+6aTZFUm45H4yk7IlDizN9IMAKVTKCSpGZSd9r2se0EH9aSFREtOktYvXynciqB25hmJ/mt4SDTpiKvguR06HlnRcB8z/9k=')
      .then(response => {
        response.blob().then(blob => {
          let url = window.URL.createObjectURL(blob);
          let a = document.createElement('a');
          a.href = url;
          a.download = 'mina.png';
          a.click();
        });
        //window.location.href = response.url;
      });
  };

  const HeaderInformation = ({ title, value }) => (
    <div className={Styles.headerInformation}>
      <text className={Styles.title}>{title}</text>
      <text className={Styles.value}>{value}</text>
    </div>
  );

  const ContainerItem = ({ item }) => {
    return (
      <table className={`${Styles.table}`}>
        <tr className={Styles.tr}>
          <td>{item}</td>
          <td>20’ General Purpuse</td>
          <td>DFSU-7756503</td>
          <td>
            <div className={Styles.wrapperStatus}>Completed</div>
          </td>
          <td>
            {/* <a href="/mina.png" download> */}
            <div
              onClick={onDowload}
              className={Styles.buttonAction}>
              <ActionGateppas
                width="56" />
            </div>
            {/* </a> */}
          </td>
        </tr>
      </table>
    );
  };

  return (
    <WrepperModal
      show={show}
      onClose={onClose}
      width={625}
      height={349}
    >
      <div className={Styles.wrepper}>
        <div className={Styles.grid}>
          <text className={Styles.titleAlert}>Gate pass</text>
          <Close />
        </div>

        <div className={`${Styles.grid} ${Styles.spaceBetween}`}>
          <HeaderInformation
            title="Do Number"
            value="DO/16/07/2020" />
          <HeaderInformation
            title="Total Request"
            value="2 Containers" />
          <HeaderInformation
            title="Completed"
            value="2 Containers" />
          <HeaderInformation
            title="In Process"
            value="-" />
        </div>

        <div
          className={Styles.spaceBetween}
          style={{ width: '100%' }}>
          {
            [1, 2].map((item, index) => {
              return (
                <ContainerItem
                  key={index.toString()}
                  item={item}
                />
              );
            })
          }
        </div>

      </div>
    </WrepperModal >
  )
}
