import React from 'react'

import WrepperModal from './wrepperModal'
import { wrepper, bg, iconAlert, titleAlert, subTitle } from 'assets/scss/modal/successPopup.module.scss'
import { SuccessAlert } from 'components/icon/iconSvg'


export default function SuccessPopup({ show, onClose }) {
  return (
    <WrepperModal
      show={show}
      onClose={onClose}
      width={631.67}
      height={497}
    >
      <div className={wrepper}>
        <div className={iconAlert}>
          <SuccessAlert />
        </div>

        <div className={bg}>
          <text className={titleAlert}>Permintaan Gate Pass Telah Berhasil</text>
          <text className={subTitle}>Permintaan Anda Telah Dikirim</text>

          <text
            className={subTitle}
            style={{ marginTop: 30 }}>Terima kasih telah menggunakan Logol!</text>
        </div>
      </div>
    </WrepperModal >
  )
}
