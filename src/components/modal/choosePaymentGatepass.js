import React from 'react'

import WrepperModal from './wrepperModal'
import Styles from 'assets/scss/modal/choosePaymentGatepass.module.scss'
import { LogolLogo } from 'components/icon/iconSvg'


export default function ChoosePayment({ show, onClose, onOpen }) {

  const ButtonPayment = ({ label }) => {
    return (
      <div
        onClick={() => {
          onOpen(label);
        }}
        className={Styles.buttonPayment}>
        <text>{label}</text>
      </div>
    );
  };

  return (
    <WrepperModal
      show={show}
      onClose={onClose}
      width={396}
      height={476}
    >
      <div className={Styles.wrepper}>
        <text className={Styles.titleAlert}>Pilih Metode Pembayaran</text>

        <ButtonPayment label="Pay Now" />
        <ButtonPayment label="Pay Later" />
        <ButtonPayment label="Logol e-wallet" />

        <div className={Styles.footerGrid}>
          <img
            src={require('assets/images/svg/smartPay.svg')}
            alt='not found'
          ></img>
          <LogolLogo width={38.5} height={27} />
        </div>
      </div>
    </WrepperModal >
  )
}
