import React from 'react'
import WrepperModal from './wrepperModal'
import { bg, wrepper, canceled, title } from 'assets/scss/modal/redirectPopup.module.scss'
import LoadingDot from 'components/loading/loadingDot'

export default function Signin({ show, onClose }) {
    return (
        <WrepperModal
            show={show}
            onClose={onClose}
            width={536}
            height={426}
        >
            <div className={wrepper}>
                <div className={bg}>
                    <LoadingDot />
                    <div className={`title ${title}`}>
                        Mengarahkan Anda
                        ke Sistem Pemesanan
                    </div>
                    <div className={canceled} onClick={onClose}>
                        Batal
                </div>
                </div>
            </div>
        </WrepperModal >
    )
}
