import React from 'react';
import { useHistory } from 'react-router-dom';

import WrepperModal from './wrepperModal';
import {
	bg,
	card,
	icon,
	wrepper,
	content,
	text1,
	text2,
	arrowRight,
} from 'assets/scss/modal/signin.module.scss';
import { ArrowRightSignUp } from 'components/icon/iconSvg';

export default function Signin({ show, onClose, Title, typeAuth, path }) {
	const history = useHistory();
	const node_env = process.env.NODE_ENV
	const checkEnv = node_env === 'development' || node_env === 'staging'
	const onCustomer = () => {
		onClose();
		checkEnv ? history.push(`${typeAuth}?typeUser=Customer`) : window.open(process.env.REACT_APP_REGISTER_USER, '_blank');
	};
	const onVendor = () => {
		onClose();
		checkEnv ? history.push(`${typeAuth}?typeUser=Vendor`) : window.open(process.env.REACT_APP_VENDOR, '_blank');
	};
	return (
		<WrepperModal show={show} onClose={onClose} width={536} height={426}>
			<div className={wrepper}>
				<div className={bg}>
					<Title />
				</div>
				<div className={content}>
					<div className={card} onClick={onCustomer}>
						<div className={text1}>Sign up as</div>
						<div className={text2}>Customer</div>
						<div className={text1} style={{ marginTop: 24 }}>
							Shipper, Consignee, or Freight Forwarder
						</div>
						<div className={icon} style={{ marginTop: 25 }}>
							<ArrowRightSignUp arrowRight={arrowRight} />
						</div>
					</div>

					<div className={card} onClick={onVendor}>
						<div className={text1}>Sign up as</div>
						<div className={text2}>Vendor</div>
						<div className={text1} style={{ marginTop: 24 }}>
							Truck provider, or Shipping Companies
						</div>
						<div className={icon} style={{ marginTop: 25 }}>
							<ArrowRightSignUp arrowRight={arrowRight} />
						</div>
					</div>
				</div>
			</div>
		</WrepperModal>
	);
}
