import React, { useState } from 'react'
import { CounterIncrementIcon, CounterDecrementCounter } from "components/icon/iconSvg"

export default function Counter (props)  {
    return (
        <div className = "counter" style = {props.counterContainerStyle}>
            <div className = "counter-container" >
                <div className = "counter-value" >
                    {props.value}
                </div>
                <div className = "counter-button-container" >
                    <div className = "counter-increment-button" onClick = {props.onIncrementClicked} >
                        <CounterIncrementIcon />
                    </div>
                    <div className = "counter-increment-button" onClick = {props.onDecrementClicked} >
                        <CounterDecrementCounter />
                    </div>
                </div>
            </div>
        </div>
    )
}