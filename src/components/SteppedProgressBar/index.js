import React from "react";
import { RightArrow, Timer,  NextRightArrow, DetailIcon, EditIcon, TruckIcon, BookIcon, DropDownArrow, ProgressDoneIcon, CounterDecrementCounter, UploadIcon, PortHandleIcon } from "../icon/iconSvg"
export default class SteppedProgressBar extends React.Component {
    constructor(props) {
        super(props)
        
        this.state = {
            iconColor: '',
            
        }
    }

    render() {
        return (
            <div style = {{
                display: "flex",
                flex: 1,
                width: "100%",
                flexDirection: "row"
            }}>
                {
                    this.props.steps.map((step, k) => {
                        let leftProgressBar = true;
                        let leftProgressColor = "#E0E5E8";
                        let rightProgressBar = true;
                        let rightProgressBarColor = "#E0E5E8"
                        let progressIcon = null;
                        let icon = null;
                        let className = ""
                        if (k === 0) {
                            leftProgressBar = false;
                        }
                        if (k === this.props.steps.length - 1) {
                            rightProgressBar = false;
                        }


                        if (step.status === "active") {
                            progressIcon = (
                                <div className = "progress-circle" >
                                    <div className = "progress-outer-circle">
                                        <div className = "progress-inner-circle" />
                                    </div>
                                </div>
                            )
                            leftProgressColor = "#002985"
                            className = "progress-title"
                        } else if (step.status === "done") {
                            progressIcon = (
                                <ProgressDoneIcon/>
                            )
                            leftProgressColor = "#002985"
                            rightProgressBarColor = "#002985"
                            className = "progress-title-done"
                        } else {
                            progressIcon = (
                                <div className = "progress-circle">
                                    <div className = "progress-pending-outer-circle" >
                                        <div className = "progress-inner-circle" />
                                    </div>
                                </div>
                            )
                            className = "progress-title-inactive"
                        }

                        return (
                            <div style = {{
                                display: "flex",
                                flex: 1,
                                flexDirection: "column",
                                justifyContent: "center",
                                alignItems: "center"
                            }}>
                                {/* icon */}
                                <div style={{}}>
                                    {step.icon}
                                </div>
                                {/* label */}
                                <div className = {className} style={{ marginTop:10, height: 48, textAlign: 'center', display: 'flex', justifyContent: 'center', alignItems: 'center', fontSize: 12, lineHeight: '15px'}}>
                                    { step.label }
                                </div>
                                {/* step progress */}
                                <div style = {{ display: "flex", width: "100%", flexDirection: "row", alignItems: "center", marginTop:10 }}>
                                    {/* left stretch */}
                                   
                                    <div style = {{ flex: 1, height: "4px", backgroundColor: leftProgressBar ? leftProgressColor : "transparent" }}>
                                        
                                    </div>
                                    {/* center icon */}
                                    <div style = {{ width: "16px", height: "16px"}}>
                                        {progressIcon}
                                    </div>
                                    {/* right stretch */}
                                    <div style = {{ flex: 1, height: "4px", backgroundColor: rightProgressBar ? rightProgressBarColor : "transparent" }}>
                                        
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}