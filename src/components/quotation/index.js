import React, { } from 'react'
import { cardContainer, title, subTitle, btnYellow, syInfo, bgContainer } from 'assets/scss/quotation/index.module.scss'

import { information, text5 } from 'assets/splitPage.module.scss';
import { CallIcon, MailIcon } from 'components/icon/iconSvg'
import { useHistory } from 'react-router-dom';

export default (props) => {
    const history = useHistory()
    return (
        <div className={bgContainer}>
            <div className={cardContainer}>
                <div className={title}>Maaf Jadwal Belum Tersedia</div>
                <div className={subTitle} style={{ marginTop: 20 }}>Kami akan memberi tau Anda jika jadwal sudah tersedia</div>
                <div className={"btn-yellow " + btnYellow} onClick={props.onClick}>Request Jadwal Pelayaran</div>
                <div className={subTitle} style={{ marginTop: 80, maxWidth: 286 }}>Untuk layanan dan informasi lebih lanjut bisa menghubungi kami:</div>
                <div className={`${information} ${syInfo}`}>
                    <CallIcon />
                    <span className={text5}>+62 21 2452 3147</span>
                </div>
                <div className={`${information} ${syInfo}`}>
                    <MailIcon />
                    <span className={text5}>Info@logol.co.id</span>
                </div>
            </div>
        </div>
    )
}