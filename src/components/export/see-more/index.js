import React from 'react';
import { DestinationIcon } from "../../icon/iconSvg"

export default function SeeMore() {
  return (
    <div className="see-more-details" >
      <div className="see-more-container" style={{ width: '100%' }}>
        <div className="see-more-delivery-route" >
          <div className="see-more-delivery-route-title">
            Rute Pengiriman
            </div>
          <div className="see-more-route-container">
            <div className="destination-point">
              <div className="destination">
                A
                    </div>
            </div>
            <div className="destination-address" >
              Cikarang, Jawa Barat
                </div>
            <div className="destination-arrow" >
              <DestinationIcon />
            </div>
            <div className="destination-point">
              <div className="destination">
                A
                    </div>
            </div>
            <div className="destination-address" >
              Cikarang, Jawa Barat
                </div>
          </div>
          <div className="see-more-delivery-route-title" style={{ marginTop: '40px' }}>
            Waktu Pengiriman
            </div>
          <div className="destination-address" style={{ marginLeft: '0px', marginTop: '15px' }}>
            07 Jan - 08 Jan, 2020
            </div>
          <div className="see-more-delivery-route-title" style={{ marginTop: '40px' }}>
            Jenis Pengiriman
            </div>
          <div className="destination-address" style={{ marginLeft: '0px', marginTop: '15px' }}>
            Ekspor
            </div>
        </div>
      </div>
      <div className="see-more-container" style={{ width: '100%' }}>
        <div className="see-more-service-free">
          <div className="see-more-delivery-route-title">
            Biaya Layanan
            </div>
          <div className="price-container" style={{ marginTop: '20px' }}>
            <div className="price-label">
              5 x 20’ General Purpose
                </div>
            <div className="price-value">
              IDR 8.500.000
                </div>
          </div>
          <div className="price-container">
            <div className="price-label">
              2 x 40’ High Cube
                </div>
            <div className="price-value">
              IDR 3.600.000
                </div>
          </div>
          <div className="price-container">
            <div className="price-label">
              Depo Penitipan
                </div>
            <div className="price-value">
              IDR 700.000
                </div>
          </div>
          <div className="see-more-delivery-route-title" style={{ marginTop: '30px' }}>
            Layanan Tambahan
            </div>
          <div className="price-container" style={{ marginTop: '20px' }}>
            <div className="price-label">
              Port Handling - Lift On Lift Off
                </div>
            <div className="price-value">
              IDR 700.000
                </div>
          </div>
          <div className="price-container">
            <div className="price-label">
              Asuransi
                </div>
            <div className="price-value">
              IDR 150.000
                </div>
          </div>
          <div className="price-container">
            <div className="price-label">
              Kualitas Truk
                </div>
            <div className="price-value">
              IDR 150.000
                </div>
          </div>
          <div className="price-total-container">
            <div className="price-total-label">
              Total
                </div>
            <div className="price-total-value">
              IDR 13.800.000
                </div>
          </div>
        </div>
      </div>
    </div>
  );
}
