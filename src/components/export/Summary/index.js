import React, { useState } from 'react'
import { CounterIncrementIcon, CounterDecrementCounter, UploadIcon, CheckIcon, DeleteIcon, LogolLogo, ShareIcon, PrintIcon, Destination1Icon } from "../../icon/iconSvg"
import UploadButton from "../Component/upload-button"
import moment from 'moment'
import OrderMap from 'components/container/truck/map'
export default function Summary({ summaryData, containerStyle }) {
    return (
        <div className="form-inner-container" style={containerStyle}>
            <div className="summary-header-container" >
                <div className="summary-logol-logo" >
                    <LogolLogo />
                </div>
                <div className="summary-print-sharre-icon-container" >
                    <div>
                        <ShareIcon />
                    </div>
                    <div>
                        <PrintIcon />
                    </div>
                </div>
            </div>
            <div className="summary-delivery-route-container" >
                <div className="summary-delivery-warapper" >
                    <div className="summary-delivery-title" >
                        Rute Pengiriman ( Pengiriman Ekspor )
                    </div>
                    <div style={{ marginTop: 30 }}>
                        <div className="summary-delivery">
                            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <div className="destination-progress-circle" >
                                    <div>
                                        <Destination1Icon />
                                    </div>
                                </div>
                                <div style={{ height: '100px', width: 2, backgroundColor: '#E0E5E8' }}></div>
                            </div>
                            <div className="summary-destination" >
                                <div className="destination-title" >
                                    {summaryData.destination1.name}
                                </div>
                                <div className="summary-destination-subtitle" >
                                    {summaryData.destination1.type}
                                </div>
                            </div>
                        </div>
                        <div className="summary-delivery">
                            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                <div className="destination-progress-circle" >
                                    <div>
                                        <Destination1Icon />
                                    </div>
                                </div>
                                {
                                    summaryData.destination3.name != "" &&
                                    <div style={{ height: '100px', width: 2, backgroundColor: '#E0E5E8' }}></div>
                                }
                            </div>
                            <div className="summary-destination" >
                                <div className="destination-title" >
                                    {summaryData.destination2.name}
                                </div>
                                <div className="summary-destination-subtitle" >
                                    {summaryData.destination2.type}
                                </div>
                            </div>
                        </div>
                        {
                            summaryData.destination3.name != "" &&
                            <div className="summary-delivery">
                                <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                    <div className="destination-progress-circle" >
                                        <div>
                                            <Destination1Icon />
                                        </div>
                                    </div>
                                </div>
                                <div className="summary-destination" >
                                    <div className="destination-title" >
                                        {summaryData.destination3.name}
                                    </div>
                                    <div className="summary-destination-subtitle" >
                                        {summaryData.destination3.type}
                                    </div>
                                </div>
                            </div>
                        }
                    </div>
                </div>
                <div className="summary-delivery-warapper" >
                    <div className="summary-delivery-title" >
                        Detail Alamat Pickup
                    </div>
                    <div className="summary-pickup-address">
                        {summaryData.destination1.address}
                    </div>
                    <div className="summary-pickup-address-info" >
                        <div className="summary-pickup-address-info-name" >
                            <span>Nama Petugas</span>
                            <div>
                                {summaryData.destination1.name}
                            </div>
                        </div>
                        <div className="summary-pickup-address-info-phone">
                            <span>No Telp</span>
                            <div>
                                {summaryData.destination1.liftOnPhone}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="summary-load-time-container" >
                <div className="summary-load-time-title" >
                    Waktu Muat Barang
                </div>
                <div className="summary-load-time-table">
                    <div className="summary-load-time-table-header">
                        <span className="header-no" >No</span>
                        <span className="header-tanggal" >Tanggal</span>
                        <span className="header-waktu" >Waktu</span>
                        <span className="header-kontainer" >Kontainer</span>
                        <span className="header-jumlah" >Jumlah</span>
                    </div>
                    {
                        summaryData.containerInformation.map((item, index) => {
                            return (
                                <div className="summary-load-time-table-body" >
                                    <div className="body-no" >
                                        {'0' + index + 1}
                                    </div>
                                    <div className="body-tanggal" >
                                        {moment(item.date).format('DD MMM, yyyy')}
                                    </div>
                                    <div className="body-waktu" >
                                        {item.waktu}
                                    </div>
                                    <div className="body-kontainer" >
                                        {item.containerName}
                                    </div>
                                    <div className="body-jumlah" >
                                        {item.quantity}
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
            {
                summaryData.depoService.isService &&
                <div className="summary-depo-care-service" >
                    <div className="summary-depo-care-title" >
                        Layanan DEPO Penitipan
                    </div>
                        <div className="summary-depo-care-wrapper" >
                            <div className="summary-depo-care-depo-detail" >
                                <span>DEPO</span>
                                <div>{summaryData.depoService.pilihDEPO}</div>
                            </div>
                            <div className="summary-depo-care-tanggal-detail" >
                                <span>DEPO Date</span>
                                <div>{moment(summaryData.depoService.tanggalPenitipan).format('DD MMM, yyyy')}</div>
                            </div>
                        </div>
                </div>
            }
            <div className="summary-additional-service-container" >
                <div className="summary-additional-service-title" >
                    Layanan Tambahan
                </div>
                <div className="summary-additional-service-info" >
                    {
                        summaryData.service.map((item, index) => {
                            let title = ""
                            if (item.serviceMasterCode === "DepoStorage") {
                                title = "Kontainer Handling - Lift On Lift Off"
                            } else if (item.serviceMasterCode === "TruckQuality") {
                                title = "Kualitas Truk ( Grade 1 )"
                            } else if (item.serviceMasterCode === "Fumigation") {
                                title = "Fumigasi"
                            } else if (item.serviceMasterCode === "Insurance") {
                                title = "Asuransi Darat"
                            }
                            return (
                                <div className="summary-additional-service" key={index} >
                                    {title}
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        </div>
    )
}
