import React, { useEffect, useState } from 'react'
import { PortHandleIcon, DropDownArrow, DepoPenitipanIcon, AsuransiIcon, ContainerTruckIcon } from "../../icon/iconSvg"
import { addOrUpdateCartItem } from "actions"
import { useDispatch, useSelector } from "react-redux"
import { megamenu, buttonSelect, gridItem, title, lineCenter } from 'assets/scss/megamenu/megamenu.module.scss';
import 'assets/scss/component/react-datepicker.scss'
import ReactDatePicker, { CalendarContainer } from 'react-datepicker'
import moment from 'moment'
export default function AdditionalService(props) {
    const [startDate, setDate] = useState("")
    const ExampleCustomInput = ({ time, startDate, resetPickers, onClick }) => {
        return (
            <div className="input-box-container" >
                <div className="input-text-box"
                    onClick={onClick} style = {{width:'345px'}}>
                    <div>
                        {
                            startDate != ""
                                ? moment(startDate).format('DD MMM, yyyy')
                                : "Select Tanggal Penitipan"
                        }
                    </div>
                </div>
            </div>
        )
    };

    const MyContainer = ({ className, children }) => {
        return (
            <div style={{ marginTop: 21 }}>
                <CalendarContainer className={className}>
                    <div style={{ position: "relative" }}>{children}</div>
                </CalendarContainer>
            </div>
        );
    }
    const dateSelect = (startDate) => {
        setDate(startDate)
        props.depoTanggalDate(startDate)
    }
    const onPhilipDepoChange = (e) => {
        props.philipDepo(e.target.value)
    }
    const [depoStorage, setDepoStorage] = useState({})
    const [truckingFee, setTruckingFee] = useState({})
    const [truckQuality, setTruckQuality] = useState({})
    const [fumigation, setFumigation] = useState({})
    const [insurance, setInsurance] = useState({})
    const [depoStorageCheck, depoStorageToggle] = useState(false)
    const [truckQualityCheck, truckQualityToggle] = useState(false)
    const [fumigationCheck, fumigationToggle] = useState(false)
    const [insuranceCheck, insuranceToggle] = useState(false)
    const [philipDepo, setphilipDepo] = useState("")
    var additionalService = useSelector( state => state.additionalServices.additionalService)
    useEffect(() => {
        additionalService.value.map((item, index) => {
            if(item.serviceMasterCode === "DepoStorage")  {
                setDepoStorage(item)
            }else if(item.serviceMasterCode === "Fumigation")  {
                setFumigation(item)
            } else if(item.serviceMasterCode === "Insurance")  {
                setInsurance(item)
            }
        })


        if(props.data.service.length > 0) {
            props.data.service.map((item, index) => {
                if(item.serviceMasterCode === "DepoStorage")  {
                    depoStorageToggle(true)
                }else if(item.serviceMasterCode === "Fumigation")  {
                    fumigationToggle(true)
                } else if(item.serviceMasterCode === "Insurance")  {
                    insuranceToggle(true)
                }
            })
        }

    }, []);
    const onChangeDepoStorage = () => {
        depoStorageToggle(!depoStorageCheck)
        if(!depoStorageCheck == true) {
            props.depoService(depoStorage)
        } else {
            props.depoService("")
        }
    }
    const onChangeInsurance = () => {
        insuranceToggle(!insuranceCheck)
        if(!insuranceCheck == true) {
            props.insuranceService(insurance)
        } else {
            props.insuranceService("")
        }
    }
    const onChangeTruckQuality = () => {
        truckQualityToggle(!truckQualityCheck)
        props.truckQualityService(truckQuality)
    }
    const onChangeFumigation = () => {
        fumigationToggle(!fumigationCheck)
        if(!fumigationCheck == true) {
            props.fumigationService(fumigation)
        } else {
            props.fumigationService("")
        }
    }
    return(
        <div className = "form-inner-container" style={props.containerStyle}>
            <div className = "additional-service-container" >
                <div className = "additional-service-first-container" >
                    <div className = "port-handling-container" >
                        <div className = "port-handling-header">
                            <div className = "port-handling-title">
                                <div style={{marginTop:10}}>
                                    <div className="checkbox">
                                        <label>
                                            <input
                                                type="checkbox"
                                                checked={depoStorageCheck}
                                                onChange = {onChangeDepoStorage}
                                            />
                                        </label>
                                    </div>

                                </div>
                                <div className= "port-title" >
                                    <span className = "port-title" >DEPO Penitipan</span>                                                    
                                    <span className = "port-price" >{depoStorage.price}</span>
                                </div>
                            </div>
                            <div className = "port-handling-header-icon" >
                                <DepoPenitipanIcon/>
                            </div>
                        </div>
                        <div className = "port-body" >
                            <span>
                                Dengan adanya DEPO penitipan, Anda tidak perlu kawatir akan terlambat pengiriman.
                            </span>
                        </div>
                        <div className = "port-handling-name-input-container" >
                            <span>
                                Pilih DEPO
                            </span>
                            <div>
                                <input type = "text"  disabled = {!depoStorageCheck} style = {{paddingLeft:'20px'}} placeholder = {'Enter Pilih DEPO'} onChange = {e => onPhilipDepoChange(e)}/>
                            </div>
                        </div>
                        <div className = "port-handling-name-telephone-container" >
                            <span>
                                Tanggal Penitipan
                            </span>
                            <div>
                                {!depoStorageCheck 
                                    ? <input type = "text"  disabled={true} placeholder = "Select Tanggal Penitipan" value = {
                                        startDate != ""
                                        ? moment(startDate).format('DD MMM, yyyy')
                                        : "Select Tanggal Penitipan"
                                    } style = {{paddingLeft:'20px'}}/>
                                    : <ReactDatePicker
                                            customInput={
                                                <ExampleCustomInput
                                                    time={"Waktu Pengiriman"}
                                                    startDate={startDate}
                                                //resetPickers={this.resetPickers}
                                                />
                                            }
                                            calendarContainer={MyContainer}
                                            selected={new Date()}
                                            onChange={startDate => dateSelect(startDate)}
                                        >
                                        </ReactDatePicker>
                                }
                            </div>
                        </div>
                    </div>
                    <div className = "port-handling-container" style = {{marginTop:30}}>
                        <div className = "port-handling-header">
                            <div className = "port-handling-title">
                                <div style={{marginTop:10}}>
                                    <div className="checkbox">
                                        <label>
                                            <input
                                                type="checkbox"
                                                checked={insuranceCheck}
                                                onChange = {onChangeInsurance}
                                            />
                                        </label>
                                    </div>
                                </div>
                                <div className= "port-title" >
                                    <span className = "port-title" >Asuransi</span>                                                    
                                    <span className = "port-price" >{insurance.price}</span>
                                </div>
                            </div>
                            <div className = "port-handling-header-icon" >
                                <AsuransiIcon/>
                            </div>
                        </div>
                        <div>
                            <span>
                                Asuransi Darat
                            </span>
                        </div>
                        <div className = "port-body" >
                            <span>
                                Memberikan jaminan untuk risiko yang mungkin terjadi pada barang selama pengiriman.
                            </span>
                        </div>
                    </div>
                </div>    
                <div className = "additional-service-second-container" >
                    <div className = "port-handling-container" style = {{marginLeft:20}}>
                        <div className = "port-handling-header">
                            <div className = "port-handling-title">
                                <div className= "port-title" >
                                    <span className = "port-title" >Kualitas Truk</span>                                                    
                                    <span className = "port-price" >{truckQuality.price}</span>
                                </div>
                            </div>
                            <div className = "port-handling-header-icon" >
                                <ContainerTruckIcon/>
                            </div>
                        </div>
                        <div className = "port-body" >
                            <span>
                                Kami menyediakan Truk dengan tingkatan kualitas yang berbeda dengan driver yang berpengalaman.
                            </span>
                        </div>
                        <div className = "port-handling-name-input-container" >
                            <div className = "depo-checkbox-wrapper">
                                <div className = "depo-checkbox-header">
                                    <div className = "depo-checkbox-header-title">{''}</div>
                                    <DropDownArrow/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className = "port-handling-container" style = {{marginLeft:20, marginTop: 30}}>
                        <div className = "port-handling-header">
                            <div className = "port-handling-title">
                                <div style={{marginTop:10}}>
                                    <div className="checkbox">
                                        <label>
                                            <input
                                                type="checkbox"
                                                checked={fumigationCheck}
                                                onChange = {onChangeFumigation}
                                            />
                                        </label>
                                    </div>
                                </div>
                                <div className= "port-title" >
                                    <span className = "port-title" >Fumigasi</span>                                                    
                                    <span className = "port-price" >{fumigation.price}</span>
                                </div>
                            </div>
                            <div className = "port-handling-header-icon" >
                                <DepoPenitipanIcon/>
                            </div>
                        </div>
                        <div className = "port-body" >
                            <span>
                                Untuk memastikan barang tersebut bebas dari Organisme Pengganggu
                            </span>
                        </div>
                        <div className = "port-handling-name-input-container" >
                            <div className = "depo-checkbox-wrapper">
                                <div className = "depo-checkbox-header">
                                    <div className = "depo-checkbox-header-title">{''}</div>
                                    <DropDownArrow/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}