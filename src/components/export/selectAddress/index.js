import React, { useState, useEffect } from 'react'
import { CounterIncrementIcon, CounterDecrementCounter, UploadIcon, CheckIcon, DeleteIcon, DropDownArrow, SearchIcon, MapInputIcon, CloseInputIcon } from "../../icon/iconSvg"
import UploadButton from "../Component/upload-button"
import UploadDocument from "components/upload/upload-document"
import Checkbox from "../../checkBox/checkbox"
import InputText from "../../input/inputTextT"
import { megamenu, buttonSelect, gridItem, title, lineCenter } from 'assets/scss/megamenu/megamenu.module.scss';
import {
	wrapperInput,
	inputCompleted,
	suggestion,
	centerIcon,
	containerIn,
	itemScss,
	itemText,
} from 'assets/scss/input/inputCompleted.module.scss';
import 'assets/scss/component/react-datepicker.scss'
import ReactDatePicker, { CalendarContainer } from 'react-datepicker'
import moment from 'moment'
import Autocomplete from "../../input/autocomplete"
import { getPortData } from "../../../actions/Export/portList"
import { getShippingList } from "../../../actions/Export/shippingList"
import { useDispatch, useSelector } from "react-redux"
import InputAutoComplete from 'components/input/inputAutoComplete';
export default function Address(props) {
    const [startDate, setDate] = useState("")
    const [sppbDate, setsppbDate] = useState("")
    const [doExpDate, setdoExpDate] = useState("")
    const [pibDate, setpibDate] = useState("")
    const ExampleCustomInput = ({ time, startDate, resetPickers, onClick }) => {
        return (
            <div className="input-box-container" >
                <span>{'Shipping Date'}</span>
                <div className="input-text-box"
                    onClick={onClick}>
                    <div>
                        {
                            startDate != ""
                                ? moment(startDate).format('DD MMM, yyyy')
                                : ""
                        }
                    </div>
                </div>
            </div>
        )
    };

    const DateInput = ({ time, startDate, title, resetPickers, onClick }) => {
        return (
            <div className="shipping-instruction-container">
                <div className = "shipping-instruction-title" >
                    <span>{time}</span>
                </div>
                <div className = "shipping-instruction-number" >
                    <div className="input-text-box" style = {{width:'500px'}}
                        onClick={onClick}>
                        <div>
                            {
                                startDate != ""
                                    ? moment(startDate).format('DD MMM, yyyy')
                                    : 'Select ' + time
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    };

    const MyContainer = ({ className, children }) => {
        return (
            <div style={{ marginTop: 21 }}>
                <CalendarContainer className={className}>
                    <div style={{ position: "relative" }}>{children}</div>
                </CalendarContainer>
            </div>
        );
    }
    const [kontainerChecked, setkontainerChecked] = useState(false)
    const [liftOnChecked, setLiftOnChecked] = useState(false)
    const [liftOffChecked, setLiftOffChecked] = useState(false)
    const [phone, setPhone] = useState('')
    const [SIDocument, setSIDocument] = useState('')
    const [SPPBDocument, setSPPBDocument] = useState('')
    const [SJDocument, setSJDocument] = useState('')
    const [DODocument, setDODocument] = useState('')
    const [portOfDischarge, setPortOfDischarge] = useState('')
    const [openPODList, setPOdListToggle] = useState(false)
    const [portFinalDestination, setPortFinalDestination] = useState('')
    const [openPFDList, setPFDListToggle] = useState(false)
    const [shippingLine, setshippingLine] = useState('')

    const cartItemData = useSelector(state => state.cartItem.cartItemData)
    const toggleKontainerHandling = () => {
        setkontainerChecked(!kontainerChecked)
        props.kontainerCheckValue(!kontainerChecked)
    }

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getShippingList(''))
    }, []);

    var shippingList = useSelector(state => state.shippingData.shippingListData)

    const liftOnChange = () => {
        setLiftOnChecked(!liftOnChecked)
        props.liftOnCheckValue(!liftOnChecked)
    }

    const liftOffChange = () => {
        setLiftOffChecked(!liftOffChecked)
        props.liftOffCheckValue(!liftOffChecked)
    }
    const getSIDocument = (files) => {
        setSIDocument(files)
        props.SiDocumentUpload(files)
    }
    const getSPPBDocument = (files) => {
        setSPPBDocument(files)
        props.SPPBDocumentUpload(files)
    }
    const getDODocument = (files) => {
        setDODocument(files)
        props.doDocumentUpload(files)
    }

    const getPibDocument = (files) => {
        props.pibDoumentUpload(files)
    }
    const getSJDocument = (files) => {
        setSJDocument(files)
        props.sjDocumentUpload(files)
    }
    const siDocumentName = (val) => {
        props.getSIDocumentName(val.target.value)
    }

    const sPPBDocumentName = (val) => {
        props.getSPPBDocumentName(val.target.value)
    }

    const doDocumentName = (val) => {
        props.getDODocumentName(val.target.value)
    }

    const getPibDocumentName = (val) => {
        props.getPibName(val.targqet.value)
    }

    const sjDocumentName = (val) => {
        props.getSJDocumentName(val.target.value)
    }

    const LiftOnNameOnChange = (val) => {
        props.liftOnNameValue(val.target.value)

    }

    const LiftOnPhoneOnChange = (val) => {
        props.liftOnPhoneValue(val.target.value)
    }

    const LiftOFFNameOnChange = (val) => {
        props.liftOFFNameValue(val.target.value)

    }

    const LiftOFFPhoneOnChange = (val) => {
        props.liftOFFPhoneValue(val.target.value)
    }
    const onChangeVesselName = (val) => {
        props.vesselNameValue(val.target.value)
    }
    const onChangeShippingDate = (val) => {
        props.shippingDateValue(val.target.value)
    }
    const onChangeVoyageNo = (val) => {
        props.voyageNoValue(val.target.value)
    }
    const onChangeWeight = (val) => {
        props.weightValue(val.target.value)
    }
    const dateSelect = (startDate) => {
        setDate(startDate)
        props.shippingDateValue(startDate)
    }
    const onPODSearch = (term) => {
        setPortOfDischarge(term)
        setPOdListToggle(true)
        dispatch(getPortData(term))
    }

    const onPFDSearch = (term) => {
        setPortFinalDestination(term)
        setPFDListToggle(true)
        dispatch(getPortData(term))
    }
    var portListData = []
    portListData = useSelector(state => state.portList.porListData)

    const selectedPOD = (item) => {
        setPOdListToggle(false)
        setPortOfDischarge(item)
        props.pODValue(item)
    }
    const selectedPFD = (item) => {
        setPFDListToggle(false)
        setPortFinalDestination(item)
        props.pFDValue(item)
    }
    const getBillOfLandingDocument = (files) => {
        this.props.billOfLandingDocument(files)
    }
    const getDeliveryDocumentDocument = (files) => {
        this.props.deliveryDocumen(files)
    }
    const billOfLandingDocName = (val) => {
        this.props.getBillOfLandingName(val.target.value)
    }

    const deliveryOrderDocName = (val) => {
        this.props.getDeliveryOrderName(val.target.value)
    }

    const sppbDateSelect = (date) => {
        setsppbDate(date)
        props.sppbDateSelected(date)
    }

    const doExpDateSelected = (date) => {
        setdoExpDate(date)
        props.doExpDateSelected(date)
    }

    const pibDateSelected = (date) => {
        setpibDate(date)
        props.pibDateSelected(date)
    }

    const getShippingData = (e) => {
        props.shippingLineChange(e)
        setshippingLine(e)
        dispatch(getShippingList(e))
    }

    const setShippingLineItem = (val) => {
        props.onShippingLineChange(val)
        setshippingLine('')
    }
      
    return (
        <div className="form-inner-container" style={props.containerStyle}>
            <div className="form-title" >
                Dokumen Wajib
            </div>
            <div className="required-container" >
                {
                    props.data.orderType === "EXPORT" ?
                    <div>
                        <UploadDocument placeholder = {"Shipping Instruction"} docName={props.data.siDocumentName} getDocument={getSIDocument} onTextChange={siDocumentName} title={"Shipping Instruction"} />
                        <UploadDocument placeholder = {"Delivery order"} docName={props.data.doDocumentName} getDocument={getDODocument} onTextChange={doDocumentName} title={"Delivery Order"} />
                        <UploadDocument placeholder = {"Surat Jalan"} docName={props.data.suratJalanDocumentName} getDocument={getSJDocument} onTextChange={sjDocumentName} title={"Surat Jalan"} />
                    </div>
                    :
                    <div>
                        <UploadDocument placeholder = {"SPPB Document"} docName={props.data.sppbDocumentName} getDocument={getSPPBDocument} onTextChange={sPPBDocumentName} title={"SPPB"} />
                        <ReactDatePicker
                            customInput={
                                <DateInput
                                    time={"SPPB Date"}
                                    startDate={props.data.sppbDate}
                                />
                            }
                            calendarContainer={MyContainer}
                            selected={new Date()}
                            onChange={startDate => sppbDateSelect(startDate)}
                        >
                        </ReactDatePicker>
                        <UploadDocument placeholder = {"Delivery order"} docName={props.data.doDocumentName} getDocument={getDODocument} onTextChange={doDocumentName} title={"Delivery Order"} />
                        <ReactDatePicker
                            customInput={
                                <DateInput
                                    time={"Delivery Order Exp Date"}
                                    startDate={props.data.deliveryOrderExpDate}
                                />
                            }
                            calendarContainer={MyContainer}
                            selected={new Date()}
                            onChange={startDate => doExpDateSelected(startDate)}
                        >
                        </ReactDatePicker>
                        <UploadDocument placeholder = {"PIB"} docName={props.data.pib} getDocument={getPibDocument} onTextChange={getPibDocumentName} title={"PIB"} />
                        <ReactDatePicker
                            customInput={
                                <DateInput
                                    time={"PIB Date"}
                                    startDate={props.data.pibDate}
                                />
                            }
                            calendarContainer={MyContainer}
                            selected={new Date()}
                            onChange={startDate => pibDateSelected(startDate)}
                        >
                        </ReactDatePicker>
                        <UploadDocument placeholder = {"Bill Of Landing"} getDocument = {getBillOfLandingDocument} onTextChange = {billOfLandingDocName} title = {"Bill Of Landing"} />
                        <UploadDocument placeholder = {"Surat Jalan"} docName={props.data.suratJalanDocumentName} getDocument={getSJDocument} onTextChange={sjDocumentName} title={"Surat Jalan"} />
                    </div>
                }
            </div>
            <div className="lift-on-off-container">
                <div className="kontainer-handling-container" >
                    <div className="checkbox">
                        <label>
                            <span>{'Container Handling'}</span>
                        </label>
                    </div>
                    <div className="kontainer-handling-price-container" >
                        <span>IDR 700.000</span>
                    </div>
                </div>
                {
                    (!props.data.destination1.liftOn && (props.data.destination1.type === "Depo" || props.data.destination1.type === "Port")) &&
                    <div>
                        <div className="lift-on-off-body-container" >
                            <div className="lift-on-off-title" >
                                Lift On - {props.data.destination1.type + ' Handling'}
                            </div> 
                            <div className="lift-on-btn" >
                                <label>
                                    Self handle
                                </label>
                            </div>
                        </div>
                        <div className="lift-on-off-wrapper" >
                            <div>
                                <InputText value={props.data.destination1.liftOnName} inputType={"text"} onChange={LiftOnNameOnChange} textInputTitle={"Petugas Lift On"} disabled={liftOnChecked} inputTextStyle={{ backgroundColor: liftOnChecked ? "#FAFAFA" : '#fff' }} inputContainerStyle={{ marginTop: 5 }} />
                                <p className = "error_msg">{props.data.error.address.liftOnName.msg}</p>
                            </div>    
                            <div className="lift-on-off-second-container" >
                                <InputText value={props.data.destination1.liftOnPhone} inputType={"number"} onChange={LiftOnPhoneOnChange} textInputTitle={"No. Telepon"} isPhone={true} disabled={liftOnChecked} inputTextStyle={{ backgroundColor: liftOnChecked ? "#FAFAFA" : '#fff' }} inputContainerStyle={{ marginTop: 5 }} />
                                <p className = "error_msg">{props.data.error.address.liftOnPhone.msg}</p>
                            </div>
                        </div>
                    </div>
                }
                {
                    (props.data.destination1.liftOn && (!props.data.destination2.liftOff && !props.data.destination3.liftOff) && (props.data.destination1.type === "Depo" || props.data.destination1.type === "Port")) &&
                    <div>
                        <div className="lift-on-off-body-container" >
                            <div className="lift-on-off-title" >
                                Lift On - {props.data.destination1.type + ' Handling'}
                            </div>
                            <div className="lift-on-btn-logol" >
                                <label>
                                    Be handled by Logol
                                </label>
                            </div>
                        </div>
                    </div>
                }
                {
                   (!props.data.destination2.liftOff && !props.data.destination3.liftOff && ((props.data.destination2.type === "Port" || props.data.destination2.type === "Depo") || (props.data.destination3.type === "Port" || props.data.destination3.type === "Depo"))) &&
                    <div>
                        <div className="lift-on-off-body-container" >
                            <div className="lift-on-off-title" >
                                Lift Off - {props.data.destination2.liftOff ? props.data.destination2.type + ' Handling' : props.data.destination3.type  + ' Handling'}
                            </div> 
                            <div className="lift-on-btn" >
                                <label>
                                    Self handle
                                </label>
                            </div>
                        </div>
                        <div className="lift-on-off-wrapper" >
                            <div>
                                <InputText value={props.data.destination3.type === "Port" ? props.data.destination3.liftOffName : props.data.destination3.liftOffName} inputType={"text"} onChange={LiftOFFNameOnChange} textInputTitle={"Petugas Lift Off"} disabled={liftOffChecked} inputContainerStyle={{ marginTop: 5 }} inputTextStyle={{ backgroundColor: liftOffChecked ? "#FAFAFA" : '#fff' }} />
                                <p className = "error_msg">{props.data.error.address.liftOffName.msg}</p>
                            </div>
                            <div className="lift-on-off-second-container" >
                                <InputText value={props.data.destination3.type === "Port" ? props.data.destination3.liftOffPhone : props.data.destination3.liftOffPhone} inputType={"number"} onChange={LiftOFFPhoneOnChange} isPhone={true} textInputTitle={"No. Telepon"} disabled={liftOffChecked} inputTextStyle={{ backgroundColor: liftOffChecked ? "#FAFAFA" : '#fff' }} inputContainerStyle={{ marginTop: 5 }} />
                                <p className = "error_msg">{props.data.error.address.liftOffPhone.msg}</p>
                            </div>
                        </div>
                    </div>
                }
                {
                    (!props.data.destination1.liftOn && (props.data.destination2.liftOff || props.data.destination3.liftOff) && ((props.data.destination2.type === "Port" || props.data.destination2.type === "Depo") || (props.data.destination3.type === "Port" || props.data.destination3.type === "Depo"))) &&
                    <div>
                        <div className="lift-on-off-body-container" >
                            <div className="lift-on-off-title" >
                                Lift Off- {props.data.destination2.liftOff ? props.data.destination2.type + ' Handling' : props.data.destination2.type  + ' Handling'}
                            </div> 
                            <div className="lift-on-btn-logol" >
                                <label>
                                    Be handled by Logol
                                </label>
                            </div>
                        </div>
                    </div>
                }    

                {
                    props.data.destination1.liftOn && (props.data.destination3.liftOn || props.data.destination3.liftOff) &&
                    <div className="lift-on-off-body-container" >
                        <div className="lift-on-off-title" >
                            Lift On - Lift Off - {props.data.destination1.type + ' Handling '}  -  {props.data.destination3.liftOff ? props.data.destination3.type + ' Handling' : props.data.destination2.type  + ' Handling'}
                        </div>
                        <div className="lift-on-btn-logol" >
                            <label>
                                Be handled by Logol
                            </label>
                        </div>
                    </div>   
                }   

                {
                    (props.data.destination1.liftOn || (props.data.destination2.liftOff || props.data.destination3.liftOff )) &&
                    <div>
                        <div style={{marginTop:'10px'}}>
                            <div className={containerIn}>
                                <div className={wrapperInput}>
                                    <input
                                        name='autoComplete'
                                        autocomplete='off'
                                        onChange={e => getShippingData(e.target.value)}
                                        value={props.data.shippingLine}
                                        className={inputCompleted}
                                        placeholder='Select Shipping Line...'
                                    />
                                    <div className={centerIcon}>
                                        {props.data.shippingLine && props.data.shippingLine.length ? (
                                            <div onClick={() => getShippingData('')}>
                                                <CloseInputIcon />
                                            </div>
                                        ) : (
                                            <SearchIcon color='#000000' />
                                        )}
                                    </div>
                                </div>
                                {shippingLine.length ?
                                    <div className={suggestion}>
                                        {shippingList.map((item, index) => (
                                            <div
                                                key={index}
                                                className={itemScss}
                                                onClick={() => setShippingLineItem(item)}
                                            >
                                                {/* <a style={{ marginLeft: 11, color: "#000000" }}>{value}</a> */}
                                                <a className={itemText}>{item.name}</a>
                                            </div>
                                        ))}
                                    </div>
                                : ''}
                            </div>
                            <p className = "error_msg">{props.data.error.address.shippingLine.msg}</p>
                        </div>
                        
                        <div className="lift-on-off-wrapper" >
                            <div>
                                <div>
                                    <InputText value={props.data.vesselName} inputType={"text"} onChange={onChangeVesselName} textInputTitle={"Vessel Name "} inputContainerStyle={{ marginTop: 25 }} />
                                    <p className = "error_msg">{props.data.error.address.vesselName.msg}</p>
                                </div>
                                <div>
                                    <Autocomplete title={'Port Of Discharge'} openList={openPODList} setChange={onPODSearch} value={props.data.portOfDischarge} arraySuggestion={portListData.data == undefined ? [] : portListData.data.map((item, index) => item.portdesc)} setItem={selectedPOD} inputContainerStyle={{ marginTop: 25 }} selectBoxListStyle={{ marginTop: 50, width: '396px', height: '152px' }} />
                                    <p className = "error_msg">{props.data.error.address.portOfDischarge.msg}</p>
                                </div>
                            </div>
                            <div className="lift-on-off-second-container" >
                                <div>
                                    <div>
                                        <InputText value={props.data.voyageNo} inputType={"text"} onChange={onChangeVoyageNo} textInputTitle={"Voyage No"} inputContainerStyle={{ marginTop: 25 }} />
                                        <p className = "error_msg">{props.data.error.address.voyageNo.msg}</p>
                                    </div>
                                    <div>
                                        <Autocomplete title={'Port Final Destination'} openList={openPFDList} setChange={onPFDSearch} value={props.data.portOfFinalDestination} arraySuggestion={portListData.data == undefined ? [] : portListData.data.map((item, index) => item.portdesc)} setItem={selectedPFD} inputContainerStyle={{ marginTop: 25 }} selectBoxListStyle={{ marginTop: 180, width: '396px', height: '152px' }} />
                                        <p className = "error_msg">{props.data.error.address.portOfFinalDestination.msg}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="lift-on-off-wrapper" >
                            <div>
                                <div style={{ marginTop: 23 }}>
                                    <ReactDatePicker
                                        customInput={
                                            <ExampleCustomInput
                                                time={"Waktu Pengiriman"}
                                                startDate={props.data.ShippingDate}
                                            //resetPickers={this.resetPickers}
                                            />
                                        }
                                        calendarContainer={MyContainer}
                                        selected={new Date()}
                                        onChange={startDate => dateSelect(startDate)}
                                    >
                                    </ReactDatePicker>
                                    <p className = "error_msg">{props.data.error.address.ShippingDate.msg}</p>
                                </div>
                            </div>
                            <div className="lift-on-off-second-container" >
                                <div>
                                    <div>
                                        <InputText inputType={"number"} onChange={onChangeWeight} textInputTitle={"Weight"} isWeight={true} inputContainerStyle={{ marginTop: 25 }} />
                                        <p className = "error_msg">{props.data.error.address.weight.msg}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                }
                
            </div>
        </div>
    )
}
