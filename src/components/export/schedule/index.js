import React, { useState, useEffect } from 'react'
import Title from "../Component/export-title"
import SelectBox from "../../checkBox/select-box"
import RadioButton from "components/button/radio-button"
import PickupAddressModal from '../Component/pickup-address-modal'
import { useDispatch, useSelector } from "react-redux"
import { getDepo, getPort, getFactoryList, getDestinationPicker, getStorageList } from "actions"
import { CheckIcon, Timer, PrevLeftArrow, NextRightArrow, DetailIcon, EditIcon, TruckIcon, BookIcon, DropDownArrow, DestinationIcon, DropDownUpArrow, UploadIcon, PortHandleIcon, LocationCheck, LocationRightArrow  } from "components/icon/iconSvg"
import MegaDropdown from 'components/megadropdown'
import { ItemExsistingCustomer } from 'components/gatepass'
import ErrorAlert from "../../alert/ErrorDialougeBox"

const pickupLeftData = [{
  key: "port",
  title: "Port"
}, {
  key: "storage",
  title: "Storage"
}]

const deliverLeftData = [{
  key: "factory",
  title: "Factory"
}, {
  key: "port",
  title: "Port"
}, {
  key: "storage",
  title: "Storage"
}]

export default function Schedule(props) {
    const [listOpen, setToggle] = useState(false);
    const [pickupListOpen, setPickupToggle] = useState(false);
    const [orderType, setOrderType] = useState(props.data.orderType);
    const [error, setError] = useState({pickupErrorMessage : '', deliveryErrorMessage : '', dropErrorMessage : ''});
    const [searchTerm, setsearchTerm] = useState("");
    // const [depo, setDepo] = useState("")
    // const [depoListData, setDepoList] = useState("")
    // const [pickupAddress, setpickupAddress] = useState("")
    const [showPickupModal, setPickupModal] = useState(false)
    const [showDeliveryList, deliveryListToggle] = useState(false)
    // const [deliveryLocation, setDeliveryLocation] = useState('')
    // const [pickupName, setPickupName] = useState("");
    // const [pickupPhone, setPickupPhone] = useState("");
    // const [exportRadioValue, setExportRadioValue] = useState(true);
    // const [importRadioValue, setImportRadioValue] = useState(false);

    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(getDestinationPicker(props.data.orderType, '', ''))
        dispatch(getDepo(''));
        dispatch(getPort(''));
        dispatch(getFactoryList(''));
        dispatch(getStorageList(''))
    }, []);
    var depoList = useSelector(state => state.depo.searchDepoData)
    var PortList = useSelector(state => state.searchPortData.searchPortData)
    var FactoryList = useSelector(state => state.factoryList.factoryListData)
    var destinationPickerIds = useSelector(state => state.destinationPicker.destinationPickerIds)
    var storageList = useSelector(state => state.storage.storageListData)
    var pickupData = []
    var pickupDataLeft = []
    var deliverToData = []
    var deliverToDataLeft = []
    var dropOffData = []
    var dropOffDataLeft = []
    const toggleButton = () => {
        dispatch(getDestinationPicker(props.data.orderType, '', ''))
        setToggle(!listOpen)
        setPickupToggle(false)
        deliveryListToggle(false)
        props.resetData('Destination1')
    }
    if(destinationPickerIds.length > 0){
        destinationPickerIds.map((item,index) => {
            var data = {}
            data.key = item.destinationPoint1
            data.title = item.destinationPoint1
            pickupData.push(data)
        })
        pickupDataLeft = dedupe(pickupData)
    }
    

    const pickupClicked = () => {
        if(props.data.destination1.name == "") {
            return;
            error.pickupErrorMessage = "Please Select Pickup"
            setError(error)
        } else {
            dispatch(getDestinationPicker(props.data.orderType, props.data.destination1.type, ''))
            setPickupToggle(!pickupListOpen)
        }
        setToggle(false)
        deliveryListToggle(false)
        props.resetData('Destination2')
    }
    if(destinationPickerIds.length > 0){
        destinationPickerIds.map((item,index) => {
            var data = {}
            data.key = item.destinationPoint2
            data.title = item.destinationPoint2
            deliverToData.push(data)
        })
        deliverToDataLeft = dedupe(deliverToData)
    }
    const openDelievryLocationList = () => {
        if(props.data.destination1.name == "" && props.data.destination1.name ==  "") {
            return;
        } else {
            dispatch(getDestinationPicker(props.data.orderType, props.data.destination1.type, props.data.destination2.type))
            deliveryListToggle(!showDeliveryList)
        }
        setToggle(false)
        setPickupToggle(false)
    }

    if(destinationPickerIds.length > 0){
        destinationPickerIds.map((item,index) => {
            var data = {}
            data.key = item.destinationPoint3
            data.title = item.destinationPoint3
            dropOffData.push(data)
        })
        dropOffDataLeft = dedupe(dropOffData)
    }
    function dedupe(arr) {
        return arr.reduce(function(p, c) {
          var id = [c.key, c.title].join('|');
          if (p.temp.indexOf(id) === -1) {
            p.out.push(c);
            p.temp.push(id);
          }
          return p;
        }, {
          temp: [],
          out: []
        }).out;
    }

    const selectDepo = (item) => {
        // setDepo(item.name)
        setToggle(false)
        props.selectedDepo(item)
    }

    const selectPickupAddress = (item, index) => {
        var address = "Factory " + (index + 1) + ' - ' + item.factoryName
        var addressIndex = "Factory " + (index + 1)
        props.selectedFactoryAddress(item, addressIndex)
        setPickupToggle(false)
    }

    const searchDepo = (e) => {
        dispatch(getDepo(e.target.value));
    }

    const searchStorage = (e) => {
        dispatch(getStorageList(e.target.value))
    }

    const searchFactory = (e) => {
        dispatch(getFactoryList(e.target.value));
    }

    const searchPort = (e) => {
        dispatch(getPort(e.target.value));
    }
    

    const openPickupModal = () => {
        setPickupModal(true)
        setPickupToggle(false)
    }

    const closeModal = () => {
        setPickupModal(false)
    }

    const selectDeliveryDestination = (item) => {
        deliveryListToggle(false)
        props.selectedDeliveryLocation(item)
    }

    const submitPickupAddress = (address) => {
        var add = "Factory " + (FactoryList.length + 1) + ' - ' + address.pickupAddress
        setPickupModal(false)
        props.pickupAddressSelected(address)
    }

    const changeExportRadio = () => {
        props.exportRadioValue()
    }

    const changeImportRadio = () => {
        props.importRadioValue()
    }

    const pickupDataSelected = (data) => {
        setToggle(false)
        props.selectedPickupData(data)
    }

    const deliveryDataSelected = (data) => {
        setPickupToggle(false)
        props.deliveryDataSelected(data)
    }

    const dropOffDataSelected = (data) => {
        deliveryListToggle(!showDeliveryList)
        props.dropOffDataSelected(data)
    }
    return (
        <div className="form-inner-container" style={props.containerStyle}>
            <div style={{display:'flex', flexDirection:'column'}}>
                <Title title="Tentukan Jadwal Pengiriman" />
                <div className="form-checkbox-title" >
                    Pengiriman
                </div>
                <div className="form-checkbox-container" >
                    <RadioButton title={'Ekspor'} checked={props.exportCheckValue} onChange={changeExportRadio} />
                    <RadioButton title={'Impor'} checked={props.importCheckValue} containerStyle={{ marginLeft: 20 }} onChange={changeImportRadio} />
                </div>
                <div className="schedule-select-box-container" style={props.containerStyle} >
                    {/* <SelectBox
                        onCloseDropDown={() => setToggle(false)}
                        placeholder={props.data.destination1 || "Select Depo"}
                        title={'DEPO'}
                        value={props.data.destination1}
                        openList={listOpen}
                        isSearch={true}
                        pickup={false}
                        openSelectList={toggleButton}
                        selectedList={selectDepo}
                        selectList={depoList}
                        searchSelectList={searchDepo}
                    /> */}

                    <div>
                        <MegaDropdown
                        onCloseDropDown={() => setToggle(false)}
                        placeholder={props.data.destination1.name || "Destinasi 1"}
                        label={'Pick Up'}
                        searchStorage={searchStorage}
                        searchDepo={searchDepo}
                        searchStorage={searchStorage}
                        searchFactory={searchFactory}
                        searchPort={searchPort}
                        value={props.data.destination1}
                        openList={listOpen}
                        openSelectList={toggleButton}
                        selectedList={selectDepo}
                        portData = {PortList}
                        depodata = {depoList}
                        storageData = {storageList.data}
                        factoryData = {FactoryList}
                        leftData={pickupDataLeft}
                        searchStorage={searchStorage}
                        selectedMenu={pickupDataLeft[0]?.key}
                        selectedMenuTitle={pickupDataLeft[0]?.title}
                        destination1={true}
                        destination3={false}
                        destination2={false}
                        dataSelected={pickupDataSelected}
                        openPickupModal={openPickupModal}
                        headerStyle={{  width: 261, height: 52, marginTop: '10px' }}
                        />
                        <p className = "error_msg">{props.data.error.destination1Error.msg}</p>
                        <p className = "error_msg">{error.pickupErrorMessage}</p>
                    </div>
                    <div className = "destination-timeline-container" >
                        <div className = "destination-loc-container" >
                            <div className = "destination-Line" style = {{backgroundColor:props.data.destination1.name != "" ? '#002985' : '#E0E5E8'}}/>
                            <div className = "destination-loc-icon-container" style = {{backgroundColor:props.data.destination1.name != "" ? '#0045FF' : '#E0E5E8'}}>
                                {
                                    props.data.destination1.name != "" &&
                                    <LocationCheck color = {'#fff'} />
                                }
                            </div>
                            <div className = "destination-Line" style = {{backgroundColor:props.data.destination1.name != "" ? '#002985' : '#E0E5E8'}}/>
                        </div>
                        {
                            (props.data.destination1.type === 'Port' || props.data.destination1.type === "Depo") &&
                            <div className = "destination-liftOn-container" > 
                                <div className="destination-liftOn">
                                    {
                                        props.data.destination1.name != "" &&
                                        
                                        'LIFT ON'
                                    }
                                </div>
                                <div className = "handling-text">
                                    {
                                        props.data.destination1.liftOn && props.data.destination1.name != "" &&
                                        'By Logol'
                                    }
                                    {
                                        !props.data.destination1.liftOn && props.data.destination1.name != "" &&
                                        'Self Handle'
                                    }
                                </div>
                            </div>
                        }
                    </div>
                    <div>
                        {/* <SelectBox placeholder={props.data.destination2Name || "Select Pickup Address"} title={'Alamat Pick-Up'} value={props.data.destination2Name} pickup={true} headerStyle={{ width: 291, height: 52 }} openList={pickupListOpen} isSearch={false} openSelectList={pickupClicked} selectedList={selectPickupAddress} selectList={FactoryList} selectBoxListStyle={{ width: 606, height: 182 }} openPickupModal={openPickupModal}
                            onCloseDropDown={() => setPickupToggle(false)} /> */}     
                      <MegaDropdown
                        onCloseDropDown={() => setPickupToggle(false)}
                        placeholder={props.data.destination2.name || "Destinasi 2"}
                        label={'Deliver To'}
                        value={props.data.destination2Name}
                        openList={pickupListOpen}
                        openSelectList={pickupClicked}
                        selectedList={selectPickupAddress}
                        portData = {PortList}
                        depodata = {depoList}
                        storageData = {storageList.data}
                        factoryData = {FactoryList}
                        leftData={deliverToDataLeft}
                        selectedMenu={deliverToDataLeft[0]?.key}
                        selectedMenuTitle={deliverToDataLeft[0]?.title}
                        dataSelected={deliveryDataSelected}
                        headerStyle={{ width: 291, height: 52, marginTop: '10px'  }}
                        destination2={true}
                        destination1={false}
                        destination3={false}
                        searchDepo={searchDepo}
                        searchStorage={searchStorage}
                        searchFactory={searchFactory}
                        searchPort={searchPort}
                        openPickupModal={openPickupModal}
                      />
                      <p className = "error_msg">{props.data.error.destination2Error.msg}</p>
                      <p className = "error_msg">{props.data.error.destination2Error.districtError}</p>
                    </div>
                    {
                        props.data.destination2.type === "Port" 
                        ?
                        <div className = "destination-timeline-container" >
                            <div className = "destination-loc-container" >
                                <div className = "destination-Line" style = {{backgroundColor:'#002985'}}/>
                                <div className = "destination-loc-icon-container" style = {{backgroundColor: '#0045FF'}}>
                                    <LocationCheck color = {'#fff'} />
                                </div>
                                <div className = "destination-Line" style = {{backgroundColor: '#002985'}}/>
                            </div>
                            <div className = "destination-liftOn-container" > 
                                <div className="destination-liftOn">
                                    LIFT OFF
                                </div>
                                <div className = "handling-text">
                                    {
                                        props.data.destination2.liftOn && props.data.destination2.name != "" &&
                                        'By Logol'
                                    }
                                    {
                                        !props.data.destination2.liftOn && props.data.destination2.name != "" &&
                                        'Self Handle'
                                    }
                                </div>
                            </div>
                        </div>
                        :
                        <div className = "destination-timeline-container" >
                            <div className = "destination-loc-container" >
                                <div className = "destination-Line" style = {{backgroundColor:props.data.destination2.name != "" ? '#002985' : '#E0E5E8'}}/>
                                <div className = "destination-loc-icon-container" style = {{backgroundColor:props.data.destination2.name != "" ? '#0045FF' : '#E0E5E8'}} >
                                    {
                                        props.data.destination2.name != "" &&
                                        <LocationRightArrow color = {'#fff'} />
                                    }
                                </div>
                                <div className = "destination-Line" style = {{backgroundColor:props.data.destination2.name != "" ? '#002985' : '#E0E5E8'}}/>
                            </div>
                        </div>
                    }
                    <div>
                        {/* <SelectBox placeholder={props.data.destination3 || "Destinasi 3"} label={"Drop Off"} value={props.data.destination3} pickup={false} headerStyle={{ width: 291, height: 52 }} 
                        openList={showDeliveryList} isSearch={false} 
                        openSelectList={openDelievryLocationList} 
                        selectedList={selectDeliveryDestination} selectList={PortList}
                            onCloseDropDown={() => deliveryListToggle(false)} 
                            destination3={true}
                            destination1={false}
                            destination2={false}
                        /> */}
                        <MegaDropdown
                            placeholder={props.data.destination3.name || "Destinasi 3"}
                            label={'Drop Off'}
                            value={props.data.destination2Name}
                            openList={showDeliveryList}
                            openSelectList={openDelievryLocationList}
                            selectedList={selectDeliveryDestination}
                            portData = {PortList}
                            depodata = {depoList}
                            storageData = {storageList.data}
                            factoryData = {FactoryList}
                            leftData={dropOffDataLeft}
                            selectedMenu={dropOffDataLeft[0]?.key}
                            selectedMenuTitle={dropOffDataLeft[0]?.title}
                            dataSelected={dropOffDataSelected}
                            headerStyle={{ width: 291, height: 52, marginTop: '10px'  }}
                            destination2={true}
                            destination1={false}
                            destination3={false}
                            onCloseDropDown={() => deliveryListToggle(false)} 
                            destination3={true}
                            destination1={false}
                            destination2={false}
                            searchDepo={searchDepo}
                            searchStorage={searchStorage}
                            searchFactory={searchFactory}
                            searchPort={searchPort}
                            openPickupModal={openPickupModal}
                        />
                        <p className = "error_msg">{props.data.error.destination3Error.msg}</p>
                        <p className = "error_msg">{props.data.error.destination3Error.districtError}</p>
                    </div>
                    {
                        props.data.destination3.type === "Port" || props.data.destination3.type === "Depo"
                        ? 
                        <div className = "destination-timeline-container" >
                            <div className = "destination-loc-container" >
                                <div className = "destination-Line" style = {{backgroundColor:'#002985'}}/>
                                <div className = "destination-loc-icon-container" style = {{backgroundColor: '#0045FF'}}>
                                    <LocationCheck color = {'#fff'} />
                                </div>
                            </div>
                            <div className = "destination-liftOn-container" > 
                                <div className="destination-liftOn">
                                    {
                                        props.data.destination3.name != "" &&
                                        
                                        'LIFT OFF'
                                    }
                                </div>
                                <div className = "handling-text">
                                    {
                                        props.data.destination3.liftOff && props.data.destination3.name != "" &&
                                        'By Logol'
                                    }
                                    {
                                        !props.data.destination3.liftOff && props.data.destination3.name != "" &&
                                        'Self Handle'
                                    }
                                </div>
                            </div>
                        </div>
                        :
                        <div className = "destination-timeline-container" >
                            <div className = "destination-loc-container" >
                                <div className = "destination-Line" style = {{backgroundColor:props.data.destination3.name != "" ? '#002985' : '#E0E5E8'}}/>
                                <div className = "destination-loc-icon-container" style = {{backgroundColor:props.data.destination3.name != "" ? '#0045FF' : '#E0E5E8'}} >
                                    {
                                        props.data.destination3.name != "" &&
                                        <LocationCheck color = {'#fff'} />
                                    }
                                </div>
                            </div>
                        </div>
                    }
                </div>
                {/* {
                    showPickupModal && */}
                <PickupAddressModal
                    showPickupModal={showPickupModal}
                    closeModal={() => setPickupModal(false)}
                    onModalSubmitClick={submitPickupAddress}
                />

                <ErrorAlert
                    message={error.errorMessage}
                    onErrorOkPressed={() => error.errorVisible = false}
                    errorVisible={error.errorVisible}
                />

            </div>
        </div>
    )
}