import React, {  } from 'react'

export default function Title (props) {
    return (
        <div className = "form-title" >
            {props.title}
        </div>  
    )
}