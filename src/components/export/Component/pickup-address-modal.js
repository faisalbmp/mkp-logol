import React, { useState } from 'react'
import { CancelModalIcon, MarkerIcon } from "../../icon/iconSvg"
import InputText from "../../input/inputText"
import { autocompletePlace, getPlaceId } from "services/apiGoogleMap"
import delayInput from 'utils/delayInput'
import WrepperModal from 'components/modal/wrepperModal'

export default function PickupAddressModal(props) {
    // const pickupAddress = [
    //     {
    //         id: 0,
    //         address: "Jl. Deltamas Boulevard No.11B,",
    //         street: "Sukamahi, Bekasi, Jawa Barat"
    //     },
    //     {
    //         id: 0,
    //         address: "Jl. Deltamas Boulevard No.11B,",
    //         street: "Hegarmukti, Bekasi, Jawa Barat"
    //     },
    //     {
    //         id: 0,
    //         address: "Jl. Deltamas Boulevard No.11B,",
    //         street: "Tambun Utara, Bekasi, Jawa Barat"
    //     }
    // ]
    const [listOpen, setListToggle] = useState(false)
    const [pickupAddressSelected, setPickupAddress] = useState('')
    const [pickupAddress, setAddress] = useState("")
    const [pickupName, setPickupName] = useState('')
    const [pickupPhoneNumber, setPickupPhoneNumber] = useState('')
    const [coordinates, setcoordinates] = useState({lat: "",  long: ""})

    const openAddressList = () => {
        setListToggle(!listOpen)
    }

    const selectPickupAddress = (item) => {
        var address = item.address + item.street
        setPickupAddress(address)
        setListToggle(false)
        //props.selectedPickupAddress(address)
    }

    const selectPickupName = (e) => {
    }

    const onPickupNameChange = (e) => {
        setPickupName(e.target.value)
    }

    const onPickupAddressPhoneChange = (e) => {
        setPickupPhoneNumber(e.target.value)
    }

    const submitPickupAddressModal = () => {
        var pickupAddress = {
            "pickupAddress": pickupAddressSelected,
            "pickupName": pickupName,
            "pickupPhone": pickupPhoneNumber,
            "latitude" : coordinates.lat,
            "longitude" : coordinates.long
        }

        props.onModalSubmitClick(pickupAddress)
    }
    const getPlaces = async (val) => {
        setPickupAddress(val)
        setListToggle(true)
        if (val.length) {
            const res = await autocompletePlace(val)
            setAddress(res)
        }
    }

    const setItem = async (val) => {
        setListToggle(false)
        const res = await getPlaceId(val.place_id)
        coordinates.lat = res.lat
        coordinates.long = res.lng
        setPickupAddress(val.description)
        setAddress([])
    }

    return (
        <WrepperModal
            show={props.showPickupModal}
            onClose={props.closeModal}
            width={816}
            height={435}
            style={{ backgroundColor: '#ffffff' }}
        >
            <div className="pickup-address-modal-container" >
                <div className="pickup-address-modal-header-container" >
                    <span>Tambah Alamat Pickup</span>
                    <button onClick={props.closeModal} ><CancelModalIcon /></button>
                </div>
                <div className="pickup-address-modal-search-container" >
                    <span>Alamat Pickup</span>
                    <input type="text" className="pickup-address-modal-search-box" value={pickupAddressSelected} onChange={e => getPlaces(e.target.value)} />
                </div>
                {
                    listOpen &&
                    <div className="pickup-address-search-result-container" >
                        <ul>
                            {
                                pickupAddress.length ? pickupAddress.map((item, index) => {
                                    return (
                                        <li key={index} className="pickup-address-list" onClick={() => setItem(item)} >
                                            <div><MarkerIcon /></div>
                                            <div><span className="pickup-add" >{item.description}</span></div>
                                        </li>
                                    )
                                }) : ""
                            }
                        </ul>
                    </div>
                }
                <div className="pickup-address-info-container" >
                    <div className="pickup-address-modal-text-box-container" >
                        <InputText textInputTitle={"Name Petugas"} onChange={onPickupNameChange} inputTextStyle={{ width: '372px', height: '52px' }} />
                    </div>
                    <div className="pickup-address-modal-text-box-container" style={{ marginLeft: '15px' }}>
                        <InputText textInputTitle={"No. Handphone"} onChange={onPickupAddressPhoneChange} isPhone={true} inputTextStyle={{ width: '372px', height: '52px' }} />
                    </div>
                </div>
                <div className="pickup-address-modal-button-container" >
                    <button className="modal-pickup-modal-cancel-button" onClick={props.closeModal} >Batal</button>
                    <button className="modal-pickup-modal-save-button" onClick={submitPickupAddressModal} >Simpan</button>
                </div>
            </div>
        </WrepperModal>
    )
}