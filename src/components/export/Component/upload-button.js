import React, {  } from 'react'
import { UploadIcon } from "../../icon/iconSvg"

export default function UploadButton () {
    return (
        <div className = "shipping-instruction-document" >
            <span>Dokumen</span>
            <div className = "upload-file-button">
                <span className = "upload-button-text" >Upload Dokumen</span>
                <span><UploadIcon/></span>
            </div>
        </div>
    )
}
