import React, { useState } from 'react'
import { CompleteOrderIcon } from "../../icon/iconSvg"
export default function CompleteOrder (props) {
    return (
        <div className = "complete-order-container">
            <div className = "complete-order-inner-container">
                <div className = "complete-order-image-container" >
                    <CompleteOrderIcon/>
                </div>
                <div className = "complete-order-title" >
                    <span>Pemesanan Berhasil</span>
                    <p>Terima Kasih mengguankan Logol!</p>
                </div>
                <div className = "complete-order-info">
                    Nomor Pesanan And
                    <p style={{color:'#004DFF', marginLeft:10}}>1212211</p>
                </div>
                <div className = "complete-order-button">
                    <button type = "button">
                        <div>Lihat Pesanan Saya</div>
                    </button>
                </div>
            </div>
        </div>
    )
}