import React, { useState } from 'react'
import { RightArrow, Timer, PrevLeftArrow, NextRightArrow, DetailIcon, EditIcon, TruckIcon, BookIcon, RemoveContainerIcon, DropDownUpArrow, DropDownArrow, CounterIncrementIcon, CounterDecrementCounter, AddContainerIcon, FullSunIcon, HalfSunIcon } from "../../icon/iconSvg"
import SelectBox from "components/checkBox/select-box";
import InputText from "components/input/inputText";
import Counter from "components/content/counter";
import { connect } from 'react-redux'
import UploadContainer from "components/upload/uploadContainer"
import { getContainerTypeList } from "actions"
import moment from 'moment'
import ReactDatePicker, { CalendarContainer } from 'react-datepicker'
import { megamenu, buttonSelect, gridItem, title, lineCenter } from 'assets/scss/megamenu/megamenu.module.scss';
import 'assets/scss/component/react-datepicker.scss'
const waktuInfo = [
    {
        id: 0,
        name: "Siang",
        isFullSun: true
    },
    {
        id: 1,
        name: "Pagi",
        isFullSun: false
    }
]

const ExampleCustomInput = ({ time, startDate, resetPickers, onClick }) => {
    return (
        <div className="input-box-container" >
            <div className="input-text-box" style=
                {{
                    alignItems: 'center',
                    width: '180px'
                }}
                onClick={onClick}>
                {
                    startDate != "" ?
                        <div>
                            <div>
                                {moment(startDate).format('DD MMM, yyyy')}
                            </div>
                        </div>

                        : <div style={{ color: '#AFB5BF' }}>Pilih Tanggal</div>
                }
            </div>
        </div>
    )
};

const MyContainer = ({ className, children }) => {
    return (
        <div style={{ marginTop: 21 }}>
            <CalendarContainer className={className}>
                <div style={{ position: "relative" }}>{children}</div>
            </CalendarContainer>
        </div>
    );
}

class DetailContainer extends React.Component {
    state = {
        date: '',
        showWaktu: false,
        fullSun: false,
        waktu: 'Pagi',
        standardArr: [],
        standardOpen: false,
        containerName: 'Pilih Kontainer',
        containerType: '',
        quantity: 0,
        containerTypeList: [],
        openArrayStandardList: false,
        openArrayWaktuList: false
    }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(getContainerTypeList()).then(() => {
            this.setState({
                containerTypeList: this.props.containerTypeListData
            })
        })
    }

    showWaktuDetail = () => {
        this.setState({
            showWaktu: !this.state.showWaktu
        })
    }

    setWaktuDetail = (item, i) => {
        this.setState({
            fullSun: item.isFullSun,
            waktu: item.name,
            showWaktu: false
        })
    }

    openStandardList = () => {
        this.setState({
            standardOpen: !this.state.standardOpen
        })
    }

    selectStandard = (item) => {
        this.setState({
            containerName: item.description,
            containerType: item.containerTypeID,
            standardOpen: false
        })
    }

    addContainer = () => {
        const { standardArr, date, waktu, containerType, containerName, quantity, fullSun } = this.state;
        var data = {
            "date": date,
            "waktu": waktu,
            "containerName": containerName,
            "containerType": containerType,
            "quantity": quantity,
            "isFullSun": fullSun
        }
        standardArr.push(data)
        // tempArr.push(data)
        this.setState({
            standardArr: standardArr,
        }, () => {
            this.setState({
                date: '',
                showWaktu: false,
                fullSun: false,
                waktu: 'Pagi',
                containerName: 'Pilih Kontainer',
                containerType: '',
                quantity: 0,
            })
        })

        this.props.containerData(this.state.standardArr)
    }

    incrementCounter = () => {
        this.setState({ quantity: this.state.quantity + 1 })
    }

    decrementCounter = () => {
        const { quantity } = this.state;
        if (quantity == 0) {
            this.setState({ quantity: 0 })
        } else {
            this.setState({ quantity: this.state.quantity - 1 })
        }
    }

    removeContainer = (item) => {
        const { standardArr } = this.state;
        const index = standardArr.indexOf(item);
        if (index > -1) {
            standardArr.splice(index, 1)
        }
        this.setState({
            standardArr: standardArr
        })
    }
    selectedDate = (date, i) => {
        const { standardArr } = this.state;
        standardArr.map((item, index) => {
            if (index === i) {
                item.date = date
            }
        })
        this.setState({
            standardArr: standardArr
        })
        this.props.containerData(this.state.standardArr)
    }
    openStandardArrayStandardList = () => {
        this.setState({
            openArrayStandardList: !this.state.openArrayStandardList
        })
    }

    selectedArrayStandardList = (item, index) => {
        const { standardArr } = this.state;
        standardArr.map((it, i) => {
            if (index === i) {
                it.containerName = item.description
                it.containerType = item.containerTypeID
            }
        })
        this.setState({
            standardArr: standardArr,
            openArrayStandardList: false
        })
        this.props.containerData(this.state.standardArr)
    }

    selectedArrayWaktuList = (item, index) => {
        const { standardArr } = this.state;
        standardArr.map((it, i) => {
            if (index === i) {
                it.waktu = item.name
                it.isFullSun = item.isFullSun
            }
        })
        this.setState({
            standardArr: standardArr,
            openArrayWaktuList: false
        })
        this.props.containerData(this.state.standardArr)
    }

    toggleArrayWaktuList = () => {
        this.setState({
            openArrayWaktuList: !this.state.openArrayWaktuList
        })
    }

    onArrayIncrementClicked = (index) => {
        const { standardArr } = this.state;
        standardArr.map((it, i) => {
            if (index === i) {
                it.quantity = it.quantity + 1
            }
        })
        this.setState({
            standardArr: standardArr,
        })
        this.props.containerData(this.state.standardArr)
    }

    onArrayDecrementClicked = (index) => {
        const { standardArr } = this.state;
        standardArr.map((it, i) => {
            if (index === i) {
                if (it.quantity == 0) {
                    it.quantity = 0
                } else {
                    it.quantity = it.quantity - 1
                }
            }
        })
        this.setState({
            standardArr: standardArr,
        })
        this.props.containerData(this.state.standardArr)
    }
    render() {
        const { waktu, date, fullSun, showWaktu, standardArr, standardOpen, containerName, quantity, containerTypeList } = this.state;
        return (
            <div className="form-inner-container" style={this.props.containerStyle}>
                <div className="form-title" >
                    Waktu Muat Barang dan Pilih Kontainer
                </div>
                <div className="load-goods-container" >
                    <div className="load-good-container-header" >
                        <span className="tanggal">Tanggal</span>
                        <span className="waktu">Waktu</span>
                        <span className="kontainer">Kontainer</span>
                        <span className="jumlah">Jumlah</span>
                    </div>
                    {
                        this.props.data.containerInformation.map((item, index) => {
                            return (
                                <div key={index} className="load-good-container-body" >
                                    <div className="tanggal">
                                        <ReactDatePicker
                                            customInput={
                                                <ExampleCustomInput
                                                    time={"Waktu Pengiriman"}
                                                    startDate={item.date}
                                                />
                                            }
                                            calendarContainer={MyContainer}
                                            //selected={new Date()}
                                            onChange={startDate => this.selectedDate(startDate, index)}
                                        >
                                        </ReactDatePicker>
                                    </div>
                                    <div className="waktu">
                                        <div className="waktu-select" onClick={this.toggleArrayWaktuList} style={{ marginTop: 10 }}>
                                            {
                                                item.isFullSun ? <FullSunIcon /> : <HalfSunIcon />
                                            }
                                            <span style={{ marginLeft: 10 }}>
                                                {item.waktu}
                                            </span>
                                        </div>
                                        {
                                            this.state.openArrayWaktuList &&
                                            <div className="waktu-select-box-container" >
                                                <ul>
                                                    {
                                                        waktuInfo.map((item, i) => (
                                                            <li key={index} className="waktu-select-list-container" onClick={() => this.selectedArrayWaktuList(item, index)}>
                                                                {
                                                                    item.isFullSun
                                                                        ? <FullSunIcon />
                                                                        : <HalfSunIcon />
                                                                }
                                                                <span>{item.name}</span>
                                                            </li>
                                                        ))
                                                    }

                                                </ul>
                                            </div>
                                        }
                                    </div>
                                    <div className="kontainer" >
                                        <div style={{ marginTop: '10px' }}>
                                            <div className="select-box-wrapper">
                                                <div className="select-box-header" onClick={this.openStandardArrayStandardList} style={{ width: 251, height: 52 }}>
                                                    <div className="select-box-header-title">{item.containerName}</div>
                                                    {
                                                        this.state.openArrayStandardList
                                                            ? <DropDownArrow />
                                                            : <DropDownUpArrow />
                                                    }
                                                </div>
                                                {
                                                    this.state.openArrayStandardList &&
                                                    <div className="select-box-list-container">
                                                        <ul>
                                                            {
                                                                containerTypeList.map((item, i) => {
                                                                    return (
                                                                        <li key={index} className="select-box-list-value" onClick={() => this.selectedArrayStandardList(item, index)} >{item.description}</li>
                                                                    )
                                                                })
                                                            }

                                                        </ul>
                                                    </div>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div className="jumlah" style={{ marginTop: 10 }}>
                                        <div className="counter">
                                            <div className="counter-container" >
                                                <div className="counter-value" >
                                                    {item.quantity}
                                                </div>
                                                <div className="counter-button-container" >
                                                    <div className="counter-increment-button" onClick={() => this.onArrayIncrementClicked(index)} >
                                                        <CounterIncrementIcon />
                                                    </div>
                                                    <div className="counter-increment-button" onClick={() => this.onArrayDecrementClicked(index)} >
                                                        <CounterDecrementCounter />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="add-container-icon" style={{ marginTop: 10 }}>
                                        <div onClick={() => this.removeContainer(item)} ><RemoveContainerIcon /></div>
                                    </div>
                                </div>
                            )
                        })
                    }
                    <div className="load-good-container-body" >
                        <div className="tanggal">
                            <ReactDatePicker
                                customInput={
                                    <ExampleCustomInput
                                        time={"Waktu Pengiriman"}
                                        startDate={date}
                                    />
                                }
                                calendarContainer={MyContainer}
                                onChange={startDate => this.setState({ date: startDate })}
                            >
                            </ReactDatePicker>
                        </div>
                        <div className="waktu">
                            <div className="waktu-select" onClick={this.showWaktuDetail} style={{ marginTop: 10 }}>
                                {
                                    fullSun ? <FullSunIcon /> : <HalfSunIcon />
                                }
                                <span style={{ marginLeft: 10 }}>
                                    {waktu}
                                </span>
                            </div>
                            {
                                showWaktu &&
                                <div className="waktu-select-box-container" >
                                    <ul>
                                        {
                                            waktuInfo.map((item, i) => (
                                                <li key={i} className="waktu-select-list-container" onClick={() => this.setWaktuDetail(item)}>
                                                    {
                                                        item.isFullSun
                                                            ? <FullSunIcon />
                                                            : <HalfSunIcon />
                                                    }
                                                    <span>{item.name}</span>
                                                </li>
                                            ))
                                        }

                                    </ul>
                                </div>
                            }
                        </div>
                        <div className="kontainer" >
                            <div style={{ marginTop: '10px' }}>
                                <div className="select-box-wrapper">
                                    <div className="select-box-header" onClick={this.openStandardList} style={{ width: 251, height: 52 }}>
                                        <div className="select-box-header-title">{containerName}</div>
                                        {
                                            this.state.openArrayStandardList
                                                ? <DropDownArrow />
                                                : <DropDownUpArrow />
                                        }
                                    </div>
                                    {
                                        standardOpen &&
                                        <div className="select-box-list-container">
                                            <ul>
                                                {
                                                    containerTypeList.map((item, i) => {
                                                        return (
                                                            <li key={i} className="select-box-list-value" onClick={() => this.selectStandard(item)} >{item.description}</li>
                                                        )
                                                    })
                                                }

                                            </ul>
                                        </div>
                                    }
                                </div>
                            </div>
                            {/* <SelectBox value = {containerName} headerStyle = {{width:251, height: 52}} openList = {standardOpen} isSearch = {false} openSelectList = {this.openStandardList} selectedList = {this.selectStandard} selectList = {containerTypeList.map((v) => v.description)} /> */}
                        </div>
                        <div className="jumlah">
                            <Counter value={quantity} onIncrementClicked={this.incrementCounter} onDecrementClicked={this.decrementCounter} counterContainerStyle={{ marginTop: 10 }} />
                        </div>
                        <div className="add-container-icon" style={{ marginTop: 10 }}>
                            <div onClick={this.addContainer}><AddContainerIcon /></div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        containerTypeListData: state.containerTypeList.containerTypeListData,
    }
}

export default connect(mapStateToProps)(DetailContainer)