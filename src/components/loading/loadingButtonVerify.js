import React from 'react'
import { load } from 'assets/scss/loading/loadingGetepass.module.scss'

export default () => (
    <div className={load}></div>
)