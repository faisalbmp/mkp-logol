import React from 'react'
import { spinner, dot } from 'assets/scss/loading/loadingSmall.module.scss'

export default function loadingLoad({ height, width }) {
    return (
        <div className={spinner}>
            <div className={dot} style={{ height, width }}></div>
        </div>
    )
}