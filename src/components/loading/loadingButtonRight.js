import React from 'react'
import { load } from 'assets/scss/loading/loadingButtonRight.module.scss'

export default () => (
    <div className={load}></div>
)