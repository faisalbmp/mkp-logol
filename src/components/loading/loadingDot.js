import React from 'react'

import {
    spinner,
    bounce1,
    bounce2,
    bounce3
} from 'assets/scss/loading/loadingDot.module.scss'

export default function loadingLoad() {
    return (
        <div className={spinner}>
            <div className={bounce1}></div>
            <div className={bounce2}></div>
            <div className={bounce3}></div>
        </div>
    )
}