import React from "react";
import Slider, { Range } from "rc-slider";
import "rc-slider/assets/index.css";
import Styles from "assets/scss/schedules/durationfilter.module.scss";
import { TextXS } from "components/text";
import { Colors } from "configs";

const DurationFilter = ({ t, durationFilterHandler }) => {
  return (
    <div className={Styles.durationFilter}>
      <Range
        // count={1}
        min={0}
        max={20}
        defaultValue={[1, 20]}
        onChange={durationFilterHandler}
        // className={Styles.slider}
        tipFormatter={(value) => `${value}`}
      />
      <div className={Styles.sliderValue}>
        <TextXS color={Colors.text.darkgrey}>1 {t("vSchedules.day")}</TextXS>
        <TextXS color={Colors.text.darkgrey}>10 {t("vSchedules.day")}</TextXS>
        <TextXS color={Colors.text.darkgrey}>20 {t("vSchedules.day")}</TextXS>
      </div>
    </div>
  );
};

export default DurationFilter;
