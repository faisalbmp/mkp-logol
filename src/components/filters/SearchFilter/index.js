import React from 'react';
import Styles from 'assets/scss/dashboard/searchfilter.module.scss';
import { TextM } from 'components/text';
import { SelectBoxM } from 'components/select';
import { ButtonM } from 'components/button';

const SearchFilter = () => {
	const items = [
		{ title: 'Pilih Semua' },
		{ title: 'OOCL' },
		{ title: 'Evergreen' },
		{ title: 'Maersk Line' },
		{ title: 'ANL' },
	];

	const sortByItem = [
		{ title: 'Harga Terendah' },
		{ title: 'Waktu Berangkat Paling Awal' },
		{ title: 'Waktu Berangkat Paling AKhir' },
		{ title: 'Waktu Tiba Paling Awal' },
		{ title: 'Waktu Tiba Paling Akhir' },
	];
	return (
		<div className={Styles.container}>
			<TextM textStyle='bold' className={Styles.title}>
				Filter Pencarian:
			</TextM>
			<div className={Styles.filters}>
				<div className={Styles.filterItem} style={{ width: 164 }}>
					<SelectBoxM label='Pelayaran' items={items} width={164} />
					{/* <div style={{height: }}></div>
					<OutlineSelect option={items} /> */}
				</div>
				<div className={Styles.filterItem}>
					<SelectBoxM label='Waktu' items={items} />
				</div>
				<div className={Styles.filterItem}>
					<SelectBoxM label='Durasi' items={items} />
				</div>
			</div>

			<div className={Styles.right}>
				<div className={Styles.filterItem} style={{ width: 223 }}>
					<SelectBoxM
						width={223}
						label='Urutkan Berdasarkan'
						items={sortByItem}
						isRoundCheckBox
					/>
				</div>
				<ButtonM className={Styles.buttonSearch}>Lihat Jadwal Lain</ButtonM>
			</div>
		</div>
	);
};

export default SearchFilter;
