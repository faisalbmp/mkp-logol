import React from "react";
import Styles from "assets/scss/schedules/shippinglinefilter.module.scss";
import { CheckBoxS } from "components/checkBox";
import { Colors } from "configs";
import { ButtonM } from "components/button";

const ShippingLineFilter = (props) => {
  const { shippingLines, t, filterHandler, activeFilter } = props;
  // const shippingLines = ['OOCL', 'Evergreen', 'Maersk Line', 'ANL'];
  // console.log(activeFilter);

  return (
    <div className={Styles.shippingLineFilter}>
      <CheckBoxS
        label={t("vSchedules.selectAll")}
        onChange={() => filterHandler("ALL")}
      />
      {shippingLines.map((shippingLine, index) => (
        <CheckBoxS
          key={index}
          label={shippingLine.alias}
          onChange={() => filterHandler(shippingLine.name)}
          onClick={() => filterHandler(shippingLine.name)}
          checked={activeFilter.includes(shippingLine.name)}
        />
      ))}
      {shippingLines.length > 5 ? (
        <ButtonM
          color={Colors.text.mainblue}
          style={{ backgroundColor: Colors.base.transparent, width: "auto" }}
        >
          {t("vSchedules.viewAll")}
        </ButtonM>
      ) : null}
    </div>
  );
};

export default ShippingLineFilter;
