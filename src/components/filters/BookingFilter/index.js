import React, { useState } from "react";
import Styles from "assets/scss/schedules/bookingfilter.module.scss";
import { LabelM } from "components/label";
import { ButtonM } from "components/button";
import { Colors } from "configs";
// import DateFilter from "../DateFilter";
import ShippingLineFilter from "../ShippingLineFilter";
import DurationFilter from "../DurationFilter";
import { withTranslation } from "react-i18next";

const BookingFilter = (props) => {
  const { t, filterHandler, activeFilter, reset, durationFilter, rangeFilter } = props;
  return (
    <div className={Styles.bookingFilter}>
      <div className={Styles.header}>
        <LabelM textStyle="bold">{t("vSchedules.searchFilter")}</LabelM>
        <ButtonM
          color={Colors.text.mainblue}
          textStyle="bold"
          style={{ backgroundColor: Colors.base.transparent, width: "auto" }}
          onClick={reset}
        >
          Reset
        </ButtonM>
      </div>
      <div className={Styles.body}>
        {/* <div className={Styles.filterItem}>
          <LabelM textStyle="bold" className={Styles.filterTitle}>
            {t("vSchedules.shippingTime")}
          </LabelM>
          <DateFilter handleSelect={rangeFilter} />
        </div> */}
        <div className={Styles.filterItem}>
          <LabelM textStyle="bold" className={Styles.filterTitle}>
            {t("vSchedules.shippingLine")}
          </LabelM>
          <ShippingLineFilter
            shippingLines={props.shippingLines}
            t={t}
            filterHandler={filterHandler}
            activeFilter={activeFilter}
          />
        </div>
        <div className={Styles.filterItem}>
          <LabelM textStyle="bold" className={Styles.filterTitle}>
            {t("vSchedules.shippingDuration")}
          </LabelM>
          <DurationFilter t={t} durationFilterHandler={durationFilter} />
        </div>
      </div>
    </div>
  );
};

export default withTranslation()(BookingFilter);
