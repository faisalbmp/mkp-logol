import React, { useState, useEffect } from 'react';
import Styles from 'assets/scss/schedules/datefilter.module.scss';
import { TextS } from 'components/text';
import { months, dates } from 'utils/dates';
import moment from 'moment';

const DateFilter = props => {
	const [activeMonth, setActiveMonth] = useState(parseInt(moment().format('M')));
	const [activeDate, setActiveDate] = useState(null)
	const [dateList, setDateList] = useState(
		dates.filter(date => date.monthId === activeMonth)
	);

	useEffect(() => {
		setDateList(dates.filter(date => date.monthId === activeMonth));
	}, [activeMonth]);

	const selectDate = (date) => {
		const { handleSelect } = props;
		if (date.id === activeDate) {
			setActiveDate(null)
			handleSelect({});
		} else {
			setActiveDate(date.id)
			handleSelect(date);
		}
	};

	const prevSection = () => {
		if (activeMonth < 1) {
			return;
		}
		setActiveMonth(activeMonth - 1);
	};

	const nextSection = () => {
		if (activeMonth > 11) {
			return;
		}

		setActiveMonth(activeMonth + 1);
	};

	const renderMonthTab = () => {
		return months.map((month, index) => {
			return (
				<div
					key={index}
					className={`${Styles.item} ${month.id === activeMonth ? Styles.active : ''}`}
				>
					<TextS>{month.name}</TextS>
				</div>
			);
		});
	};

	const renderNavigation = () => {
		return (
			<div className={Styles.navigation}>
				<div className={Styles.navButton} onClick={prevSection}>
					<i className={`icon-arrow-left ${Styles.iconArrow}`}></i>
				</div>
				<div className={Styles.months}>{renderMonthTab()}</div>
				<div className={Styles.navButton} onClick={nextSection}>
					<i className={`icon-arrow-right ${Styles.iconArrow}`}></i>
				</div>
			</div>
		);
	};

	const renderDates = () => {
		return (
			<div className={Styles.dates}>
				{dateList.map((item, index) => {
					const { bottom, top } = item;
					return (
						<div className={`${Styles.item} ${activeDate === item.id && Styles.active}`} key={index} onClick={() => selectDate(item)}>
							<TextS textStyle='bold'>{`${bottom} - ${top}`}</TextS>
							<TextS>{moment(activeMonth, 'MM').format('MMM')}</TextS>
						</div>
					);
				})}
			</div>
		);
	};

	const { show } = props;

	return (
		<div className={`${Styles.datepicker} ${show ? Styles.show : ''}`}>
			{renderNavigation()}
			{renderDates()}
		</div>
	);
};

export default DateFilter;
