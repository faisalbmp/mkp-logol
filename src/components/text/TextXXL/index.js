import React from "react";
import Styles from "assets/scss/text/paragraph.module.scss";
import { Text } from 'components/generics';
import PropTypes from "prop-types";

const TextXXL = (props) => {
  const { children, textStyle, color, style } = props;
  return (
    <Text
      className={Styles.xxLarge}
      textStyle={textStyle}
      color={color}
      style={style}
    >
      {children}
    </Text>
  );
};

export default TextXXL;

TextXXL.defaultProps = {
  textStyle: "normal",
  color: "black",
  style: {},
};

TextXXL.propTypes = {
  textStyle: PropTypes.string,
  color: PropTypes.string,
  style: PropTypes.objectOf(String),
};
