import React from 'react';
import Styles from 'assets/scss/text/paragraph.module.scss';
import { Text } from 'components/generics';
import PropTypes from 'prop-types';

const TextXL = (props) => {
  const {
    children,
    textStyle,
    color,
    style,
    className,
  } = props 
  return (
    <Text className={`${Styles.extraLarge} ${className}`} textStyle={textStyle} color={color} style={style}>{children}</Text>
  );
};

export default TextXL;

TextXL.defaultProps = {
  textStyle: "normal",
  color: "black",
  style: {},
  className: ''
}

TextXL.propTypes = {
  textStyle: PropTypes.string,
  color: PropTypes.string,
  style: PropTypes.objectOf(String),
  className: PropTypes.string,
}