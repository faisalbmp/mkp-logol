import React from 'react';
import Styles from 'assets/scss/text/paragraph.module.scss';
import { Text } from 'components/generics';
import PropTypes from 'prop-types';

const TextXS = (props) => {
  const {
    children,
    textStyle,
    color,
    style,
  } = props 
  return (
    <Text className={Styles.extraSmall} textStyle={textStyle} color={color} style={style}>{children}</Text>
  );
};

export default TextXS;

TextXS.defaultProps = {
  textStyle: "normal",
  color: "black",
  style: {},
}

TextXS.propTypes = {
  textStyle: PropTypes.string,
  color: PropTypes.string,
  style: PropTypes.objectOf(String),
}