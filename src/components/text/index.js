export { default as TextL } from "./TextL";
export { default as TextM } from "./TextM";
export { default as TextS } from "./TextS";
export { default as TextXS } from "./TextXS";
export { default as TextXL } from "./TextXL";
export { default as TextXXL } from "./TextXXL";
export { default as TextXXS } from "./TextXXS";
export { default as TextXXXL } from "./TextXXXL";
