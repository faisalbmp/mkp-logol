import React from "react";
import Styles from "assets/scss/text/paragraph.module.scss";
import { Text } from 'components/generics';
import PropTypes from "prop-types";

const TextXXXL = (props) => {
  const { children, textStyle, color, style, className } = props;
  return (
    <Text
      className={`${Styles.xxxLarge} ${className}`}
      textStyle={textStyle}
      color={color}
      style={style}
    >
      {children}
    </Text>
  );
};

export default TextXXXL;

TextXXXL.defaultProps = {
  textStyle: "normal",
  color: "black",
  style: {},
  className: "",
};

TextXXXL.propTypes = {
  textStyle: PropTypes.string,
  color: PropTypes.string,
  style: PropTypes.objectOf(String),
  className: PropTypes.string,
};
