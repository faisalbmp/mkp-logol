import React from 'react';
import { CheckboChecked, CheckboxUnCheck } from '../icon/iconSvg';

const CheckBox = ({ checked, isStacked = false }) => {
  let color = isStacked ? "#868A92" : '#E0E5E8';
  return checked ? <CheckboChecked /> : <CheckboxUnCheck color={color} />;
};

export default CheckBox;
