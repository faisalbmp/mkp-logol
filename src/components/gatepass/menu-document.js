import React, { useState } from 'react';
import { CalendarMiddle } from '../icon/iconSvg';
import {
  sideMenuDocument,
  wrapperCalendar,
  textCalendar,
  textDocument,
  textSubDocument,
  wrapperButton,
  buttonActive,
  textButton,
} from 'assets/scss/gatePass/gatePass.module.scss';
import { EGatePass, ESP2 } from '../icon/iconSvg';
import { withTranslation } from 'react-i18next';
import moment from 'moment';

function MenuDocuments({ activeMenu, t }) {
  const translate = t;
  // 0 : Export / 1 : Import
  const [activeButton, setActiveButton] = useState('Ekspor')

  const setActiveMenu = (menu) => {
    setActiveButton(menu)
    activeMenu(menu);
  };

  return (
    <div className={sideMenuDocument}>
      <div className={wrapperCalendar}>
        <CalendarMiddle />
        <div className={textCalendar}>{moment(Date()).format("dddd, DD MMM YYYY")}</div>
      </div>
      <text className={textDocument}>{translate('gatepass.document')}</text>
      <div>
        <text className={textSubDocument}>{translate('gatepass.breadcumb')}</text>
      </div>

      <div className={wrapperButton}>
        <button
          className={activeButton === 'Ekspor' ? buttonActive : null}
          onClick={() => setActiveMenu('Ekspor')}>
          <EGatePass
            color={activeButton === 'Ekspor' ? 'white' : "#002985"} />
          <text className={textButton}>{translate('gatepass.export')}</text>
        </button>
        <button
          className={activeButton === 'Import' ? buttonActive : null}
          onClick={() => setActiveMenu('Import')}>
          <ESP2
            color={activeButton === 'Import' ? 'white' : "#002985"} />
          <text className={textButton}>{translate('gatepass.import')}</text>
        </button>
      </div>

    </div>
  );
}

export default withTranslation()(MenuDocuments);
