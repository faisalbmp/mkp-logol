import React from 'react';
import { gatepassItems, textNpwp, textNama } from 'assets/scss/gatePass/gatePass.module.scss';
import { withTranslation } from 'react-i18next';

function ExsistingCustomer(props) {

  const { t } = props;

  return (
    <div className={gatepassItems}>
      <text>{props.index + 1}</text>
      <text className={textNama}>Muhammad Nur Rachman</text>
      <text className={textNpwp}>90.000.000.0-000.000</text>
      <button>
        {t('choose')}
      </button>
    </div>
  );
}

export default withTranslation()(ExsistingCustomer);

