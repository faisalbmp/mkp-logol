import React, { useState } from 'react'
import { UploadIcon, CheckIcon, DeleteIcon } from "../icon/iconSvg"
import 'assets/scss/gatePass/gatePass.module.scss';
import PropTypes from 'prop-types';
import TopUp from 'components/dashboard/tagihanSaya/inputTopUp/topUp';

function UploadDocument(props) {
  let deliveryFileUpload = null;

  const { selectedFile, docName } = props;

  const [deliveryDocument, setDeliveryDocument] = useState(docName ?? 'Unggah Dokumen')
  const [deliveryDocumentUploaded, setdeliveryDocumentUploaded] = useState(docName)

  const uploadDeliveryDocument = ({ isActionIcon = false }) => {
    if (isActionIcon && deliveryDocumentUploaded) {
      setDeliveryDocument('Unggah Dokumen')
      setdeliveryDocumentUploaded(false);
      selectedFile(null);
    } else {
      deliveryFileUpload.click();
    }
  }

  const getDeliveryDocument = (e) => {
    if (e.target.files.length > 0) {
      setDeliveryDocument(e.target.files[0].name)
      setdeliveryDocumentUploaded(true);

      selectedFile(e.target.files[0]);
    }
  }
  return (
    <div>
      {
        (props.title !== "" || props.title != null) &&
        <div className={`depo-checkbox-container-title ${props.error && "depo-checkbox-container-title-error"}`} >
          <span>{props.title}</span>
        </div>
      }
      <div className={`select-box-header ${props.error && "select-box-header-error"} ${!deliveryDocumentUploaded && `select-box-header-unselected`}`} style={props.headerStyle} >
        <div className="select-box-header-title" onClick={uploadDeliveryDocument}>
          <span style={{ fontSize: 14 }}>{deliveryDocument}</span>
          <input
            ref={elem => deliveryFileUpload = elem}
            type="file"
            style={{ display: "none", flexShrink: 1 }}
            onChange={(e) => getDeliveryDocument(e)} />
        </div>
        <div onClick={() => uploadDeliveryDocument({ isActionIcon: true })}>
          {
            deliveryDocumentUploaded ? <DeleteIcon color="#868A92" /> : <UploadIcon color="#868A92" />
          }
        </div>
      </div>
    </div>
  )
}

export default UploadDocument;

UploadDocument.defaultProps = {
  selectedFile: () => { },
};

UploadDocument.propTypes = {
  selectedFile: PropTypes.func,
};
