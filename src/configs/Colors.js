export default {
  base: {
    transparent: "rgba(255,255,255,0)",
    white: "#FFF",
  },
  text: {
    darkblue: "#002985",
    darkgrey: "#333333",
    darkgrey2: "#707070",
    black: "#000000",
    mainblue: "#0045FF",
    white: "#FFF",
    lightgrey: "#E0E5E8",
    grey1: "#868A92",
    grey2: "#B8C1C6",
    grey3: "#838E95",
  },
};