import React, { useState } from 'react';
import useWindowDimensions from 'utils/windowDimensions';

import {
	verticalSplitSlider,
	viewLeft,
	viewRight,
	item,
} from 'assets/afterBooking.module.scss';
import Left from 'components/container/auth/signup/left';
// import LeftDone from 'components/doneBooking/left'
// import RightDone from 'components/doneBooking/right'
import Right from 'components/container/auth/signup/right';
import Login from 'components/container/login';

export default function Index() {
	const { height, width } = useWindowDimensions();

	const positionRegistration = {
		left: 0,
		right: -0,
	};

	const positionLogin = {
		left: -height,
		right: -0,
	};

	const [slider, setSlider] = useState(positionRegistration);

	return (
		<div className={verticalSplitSlider}>
			<div
				className={viewLeft}
				style={{ top: slider.left, width: width > 1000 ? '50%' : '100%' }}
			>
				<div className={item}>
					<Left sliderLogin={() => setSlider(positionLogin)} />
				</div>
				<div className={item}>
					<Login sliderRegister={() => setSlider(positionRegistration)} />
				</div>
			</div>

			{width > 1000 ? (
				<div className={viewRight} style={{ top: slider.right }}>
					<div className={item}>
						<Right />
					</div>
				</div>
			) : (
				''
			)}
		</div>
	);
}
