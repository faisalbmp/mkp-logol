import React, { useState } from 'react'
import { useHistory, useLocation } from "react-router-dom";
import queryString from 'query-string'

import { verticalSplitSlider, viewLeft, viewRight, item } from 'assets/afterBooking.module.scss'
import Login from 'components/container/login'
import useWindowDimensions from 'utils/windowDimensions'
// import LeftDone from 'components/doneBooking/left'
// import RightDone from 'components/doneBooking/right'
import Right from 'components/container/auth/signin/right'

export default function Index() {
    // const { height } = useWindowDimensions();
    const history = useHistory();
    const { search } = useLocation()
    const { typeUser } = queryString.parse(search)
    const { width } = useWindowDimensions()
    // const positionLogin = {
    //     left: 0,
    //     right: -height
    // }

    // const positionDone = {
    //     left: -height,
    //     right: 0
    // }

    // const [slider, setSlider] = useState(positionLogin)
    return (
        <div className={verticalSplitSlider}>
            <div className={viewLeft} style={{ width: width > 1000 ? "50%" : "100%" }}>
                <div className={item}>
                    <Login
                        sliderRegister={() => history.push(`/signup?typeUser=${typeUser}`)}
                    // sliderDone={() => setSlider(positionDone)}
                    />
                </div>
            </div>

            {width > 1000 ? <div className={viewRight}>
                <div className={item}>
                    <Right />
                </div>
            </div> : ""}
        </div>
    )
}