import React, { useCallback, useState, useLayoutEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import HeaderHome from 'components/header/HeaderHome';
import Footer from 'components/footer';
import CustomerService from 'components/container/customerService';
import { setScrollposition } from 'actions';
import JoinOur from 'components/container/joinOur';
import useWindowDimensions from 'utils/windowDimensions';

import {
	header,
	cardTitleCeo,
	cardTitleName,
	contentTrustedBy,
	contentStep,
	cardOne,
	ourService,
	cardFunctionary,
	textCtCard,
	visiUs,
	subTitle,
	functionaryCss,
	title,
	txPositionScss,
	lineCardTitle,
	talksScss,
	cardDescription,
	left,
	cardTitle,
	indicator,
	indicatorActive,
	description,
	containerTop,
	wrIndicator,
	topCard,
	bottomCard,
	roundIndicator,
	stepImg,
	ourServiceNmae,
	ourServiceDoc,
	detail,
	wpStep,
	detailText,
	listIcon,
	contentBg1,
	contentBg2,
	contentBg3,
	flexMd,
	imgCeoScss,
	cardDescription604,
	ourStory,
} from 'assets/scss/aboutUs/index.module.scss';

import truck from 'assets/images/aboutUs/truck.svg';
import freight from 'assets/images/aboutUs/freight.svg';
import doc from 'assets/images/aboutUs/doc.svg';
import arrowx5Feft3 from 'assets/images/svg/arrowx5Feft3.svg';
import ArrowDown from 'assets/images/svg/arrow-down.svg';
import amtLogoImg from 'assets/images/logo/amt-logo.svg';
import tangguhLogoImg from 'assets/images/logo/tangguh.svg';
// import karimataJayaLogoImg from 'assets/images/logo/karimata-jaya-logo.svg'
import pantos from 'assets/images/logo/pantos.svg';
import kuehne from 'assets/images/logo/kuehne.svg';
import ironbird from 'assets/images/logo/ironbird.svg';
import agility from 'assets/images/logo/agility.svg';
import konoikeGroup from 'assets/images/logo/konoike-group.svg';
// import toll from 'assets/images/logo/toll.svg'
import nutrifood from 'assets/images/logo/nutrifood.svg';
import kamadjajaLogistics from 'assets/images/logo/kamadjajaLogistics.svg';
import maersk from 'assets/images/logo/maersk.svg';
import selamatSempurna from 'assets/images/logo/selamat_sempurna.svg';
import cmaCgm from 'assets/images/logo/cma_cgm.svg';
import ceo2 from 'assets/images/aboutUs/ceo2.png';
import cfo from 'assets/images/aboutUs/cfo.png';
import cto from 'assets/images/aboutUs/cto.png';
import cmo from 'assets/images/aboutUs/cmo.png';
import coo from 'assets/images/aboutUs/coo.png';
import { ArrowUp } from 'components/icon/iconSvg';

import { withTranslation } from 'react-i18next';

const listIconVendor = [amtLogoImg, ironbird, kamadjajaLogistics, tangguhLogoImg];

const listIconCustomer = [
	agility,
	cmaCgm,
	konoikeGroup,
	kuehne,
	maersk,
	nutrifood,
	pantos,
	selamatSempurna,
];

const aboutCeo = {
	name: 'Michael Kartono',
	txPosition: 'Founder & CEO Logol',
	img: ceo2,
};

const dataAboutCeo = ['story1', 'story2', 'story3'];

const dataStep = [
	{
		name: 'service1',
		desc: 'service1Desc',
		img: truck,
		link: '/ourServices/truck',
	},
	{
		name: 'service2',
		desc: 'service2Desc',
		img: freight,
		link: '/ourServices/freight',
	},
	{
		name: 'service3',
		desc: 'service3Desc',
		img: doc,
		link: '/e-document',
	},
];

const CardDescription = ({ activeIndex, clickArowDone, setIndex, translate }) => (
	<div className={cardDescription}>
		<CardTitleCeo
			translate={translate}
			name={aboutCeo.name}
			txPosition={aboutCeo.txPosition}
		/>
		<div className={topCard}>
			<div className={textCtCard}>
				{translate(`aboutUs.${dataAboutCeo[activeIndex]}`) || ''}
			</div>
			<div className={wrIndicator}>
				{dataAboutCeo.map((val, i) => (
					<div
						className={roundIndicator}
						style={{ borderColor: activeIndex === i ? '#0045ff' : '#ffffff' }}
						key={i}
					>
						<div
							key={i}
							className={`${indicator} ${activeIndex === i ? indicatorActive : ''}`}
							onClick={() => setIndex(i)}
						></div>
					</div>
				))}
			</div>
		</div>
		<div className={bottomCard}>
			<div onClick={clickArowDone}>
				<img src={ArrowDown} alt='img' />
			</div>
		</div>
	</div>
);

const CardTitle = ({ name, txPosition }) => (
	<div className={cardTitle}>
		<div className={title}>{name}</div>
		<div className={lineCardTitle}></div>
		<div className={txPositionScss}>{txPosition}</div>
	</div>
);

const CardTitleCeo = ({ name, txPosition }) => (
	<div className={cardTitleCeo}>
		<div className={cardTitleName}>{name}</div>
		<div className={txPositionScss}>{txPosition}</div>
	</div>
);

function Index({ t }) {
	const translate = t;
	const [active, setActive] = useState(0);
	const [color, setColor] = useState('#005EEB');

	const dispatch = useDispatch();
	const { scrollposition } = useSelector(state => state.home);
	const callBack = useCallback(() => {
		dispatch(setScrollposition());
	}, [scrollposition]);
	const { width } = useWindowDimensions();
	const history = useHistory();

	useLayoutEffect(() => {
		window.addEventListener('scroll', () => callBack());
		return () => {
			window.removeEventListener('scroll', () => callBack());
		};
	}, []);

	const clickArowDone = () => {
		if (active < 2) setActive(active + 1);
	};

	return (
		<div>
			<div className={header}>
				<HeaderHome />
			</div>

			<div className={containerTop}>
				<div className={visiUs}>
					<div className={title}>{translate('aboutUs.title')}</div>
					<div className={subTitle}>{translate('aboutUs.subTitle')}</div>
				</div>
			</div>

			<div
				className={contentBg1}
				style={
					width < 604 ? {} : { backgroundPositionY: (scrollposition + 550) * 0.2 + 'px' }
				}
			>
				<div className={ourStory}>
					<div className={title}>{translate('aboutUs.storyTitle')}</div>
					<div className={description}>{translate('aboutUs.storySubTitle')}</div>
				</div>
				<div className={talksScss}>
					<div className={imgCeoScss} style={{ backgroundImage: `url(${ceo2})` }}>
						{width < 600 ? (
							<CardTitle
								translate={translate}
								name={aboutCeo.name}
								txPosition={aboutCeo.txPosition}
							/>
						) : (
							''
						)}
					</div>
					<div className={left}>
						{width > 500 ? (
							<CardDescription
								translate={translate}
								activeIndex={active}
								clickArowDone={clickArowDone}
								setIndex={setActive}
							/>
						) : (
							<div className={cardDescription604}>
								<div
									style={{
										margin: 24,
										overflow: 'scroll',
										boxSizing: 'border-box',
										height: 336,
									}}
								>
									<div className={textCtCard}>
										{translate(`aboutUs.${dataAboutCeo[0]}`) || ''}
										{translate(`aboutUs.${dataAboutCeo[1]}`) || ''}
										{translate(`aboutUs.${dataAboutCeo[2]}`) || ''}
									</div>
								</div>
							</div>
						)}
					</div>
				</div>

				<div className={functionaryCss}>
					<div className={flexMd}>
						<div className={cardFunctionary} style={{ backgroundImage: `url(${coo})` }}>
							<CardTitle name={'Ronald Susanto'} txPosition={'Co-Founder, COO'} />
						</div>
						<div className={cardFunctionary} style={{ backgroundImage: `url(${cmo})` }}>
							<CardTitle name={'David'} txPosition={'Chief Commercial Officer'} />
						</div>
					</div>
					<div className={flexMd}>
						<div className={cardFunctionary} style={{ backgroundImage: `url(${cto})` }}>
							<CardTitle name={'Kelvin Yong'} txPosition={'Chief Technology Officer'} />
						</div>
						<div className={cardFunctionary} style={{ backgroundImage: `url(${cfo})` }}>
							<CardTitle name={'Manli Yap'} txPosition={'Financial Controler'} />
						</div>
					</div>
				</div>
			</div>

			<div
				className={contentBg2}
				style={
					width < 604 ? {} : { backgroundPositionY: (scrollposition + 900) * 0.2 + 'px' }
				}
			>
				<div className={ourService}>
					<div className={contentStep}>
						<div className={cardOne}>
							<div className={title}>
								{translate('aboutUs.serviceOurLogistic')}{' '}
								<span style={{ color: '#0045FF' }}>{translate('aboutUs.logistic')}</span>{' '}
								{translate('aboutUs.serviceLastTitle')}
							</div>
							<div className={description}>{translate('aboutUs.servicesSubTitle')}</div>
						</div>
						<div className={flexMd}>
							{dataStep.map((value, index) => (
								<div
									className={stepImg}
									style={{ backgroundImage: `url(${value.img}` }}
									key={index}
								>
									<div className={wpStep}>
										<div className={ourServiceNmae}>
											{translate(`aboutUs.${value.name}`)}
										</div>
										<div className={ourServiceDoc}>
											{translate(`aboutUs.${value.desc}`)}
										</div>
										<div className={detail} onClick={() => history.push(value.link)}>
											<div className={detailText}>{translate('aboutUs.detail')}</div>
											<img src={arrowx5Feft3} alt='img' />
										</div>
									</div>
								</div>
							))}
						</div>
					</div>
				</div>

				<div className={contentTrustedBy}>
					<div className={title}>Trusted by</div>
					<div className={description}>{translate('aboutUs.trustedDesc')}</div>

					<div className={title} style={{ margin: '64px 0px 32px 0px', fontSize: 28 }}>
						Our Customer
					</div>
					<div className={listIcon}>
						{listIconCustomer.map((value, index) => (
							<div key={index}>
								<img src={value} alt='img' />
							</div>
						))}
					</div>

					<div className={title} style={{ margin: '64px 0px 32px 0px', fontSize: 28 }}>
						Our Vendor
					</div>
					<div className={listIcon}>
						{listIconVendor.map((value, index) => (
							<div key={index}>
								<img src={value} key={index} alt='img' />
							</div>
						))}
					</div>
				</div>
			</div>

			<div
				className={contentBg3}
				style={
					width < 604
						? {}
						: { backgroundPositionY: (scrollposition + -3900) * 0.2 + 'px' }
				}
			>
				<JoinOur />
			</div>
			<CustomerService />
			<div
				className={`wrapper-floating-scroll-top ${
					window.scrollY > 100 && 'wrapper-floating-scroll-top-active'
				}`}
			>
				<div
					className='grid'
					onClick={() => {
						window.scrollTo(0, 0);
					}}
				>
					<span className='showSpan'>Scroll To Top</span>
					<div>
						<div
							className={`floating-scroll-top ${
								window.scrollY > 100 && 'floating-scroll-top-active'
							}`}
							onMouseOver={() => setColor('white')}
							onMouseOut={() => setColor('#005EEB')}
						>
							<ArrowUp color={color} />
						</div>
					</div>
				</div>
			</div>
			<Footer />
		</div>
	);
}

export default withTranslation()(Index);
