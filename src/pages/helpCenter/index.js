import React, { useCallback, useState, useLayoutEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import HeaderHome from "components/header/HeaderHome";
import Footer from "components/footer";
import { setScrollposition, sendEmailPusatBantuan } from "actions";
import Loading from "components/loading/loadingButtonRight";
import {
  textError,
  showTextEerror,
} from "assets/scss/megamenu/megamenu.module.scss";

import csIcon from "assets/images/svg/csIcon.svg";
import emailIcon from "assets/images/svg/emailIcon.svg";
import bgHelpCenter from "assets/images/helpCenter/bg-help-center.png";

import {
  content,
  layout,
  subTitle,
  title,
} from "assets/scss/ourServices/index.module.scss";
import {
  layoutHC,
  contentHC,
  right,
  left,
  bgRight,
  wpTitle,
  wpContentRight,
  wpContentLeft,
  subTitleHC,
  itemCardInfo1,
  itemCardInfo2,
  btnRes,
} from "assets/scss/helpCenter/index.module.scss";
import InputAuth from "components/input/inputAuth";
import InputTextArea from "components/input/inputTextArea";
import { btnLeft } from "assets/splitPage.module.scss";
import { NoPhone, EmailIcon, ProfileIcon } from "components/icon/iconSvg";
import djpktn1 from "assets/images/helpCenter/djpktn1.png";
import { CardInfo } from "components/cards";
import CardInfo2 from "components/cards/CardInfo2/CardInfo2";
import useWindowDimensions from "utils/windowDimensions";
import { validationPhone, validateEmail } from "utils/validation";

import { withTranslation } from "react-i18next";

function Index({ t }) {
  const translate = t;
  const dispatch = useDispatch();
  const { scrollposition } = useSelector((state) => state.home);
  const callBack = useCallback(() => {
    dispatch(setScrollposition());
  }, [scrollposition]);
  const [dataCompany, setValue] = useState({
    mobilePhone: "",
    email: "",
    contactName: "",
    message: "",
  });
  const [valueValidat, setValidation] = useState({
    companyName: true,
    contactName: true,
    email: false,
    mobilePhone: false,
  });
  const [filterValue, setFilterValue] = useState([]);
  const { mobilePhone, email, contactName, message } = dataCompany;
  const { width } = useWindowDimensions();
  const { sendEmail } = useSelector((state) => state);
  const { requestSendEmail, successSendEmail, errorSendEmail } = sendEmail;
  const [onclick, setOnClick] = useState(false);

  useLayoutEffect(() => {
    window.addEventListener("scroll", () => callBack());
    return () => {
      window.removeEventListener("scroll", () => callBack());
    };
  }, []);

  const onChange = (e) => {
    dataCompany[e.target.name] = e.target.value;
    setValue(dataCompany);
    if (e.target.name === "mobilePhone") {
      valueValidat.mobilePhone = validationPhone(e.target.value);
      setValidation(valueValidat);
    }
    if (e.target.name === "email") {
      valueValidat.email = validateEmail(e.target.value);
      setValidation(valueValidat);
    }
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    const filtValue = await Object.values(dataCompany).filter(
      (value) => value === ""
    );
    setFilterValue(filtValue);
    setOnClick(true);

    const checkAllValueIsFilled = Object.values(dataCompany).every(
      (item) => item !== ""
    );
    const checkAllValueIsValid = Object.values(valueValidat).every(
      (item) => item
    );

    if (
      Object.values(dataCompany).length &&
      checkAllValueIsFilled &&
      checkAllValueIsValid
    ) {
      await dispatch(sendEmailPusatBantuan(dataCompany));
    }
  };

  const onLink = (link) => {
    window.open(link, "_blank");
  };

  const emailLink = "mailto:inquiry@logol.co.id ";
  const djpktnLink = "mailto:pengaduan.konsumen@kemendag.go.id";
  const noLink = "tel:+622124523147";

  return (
    <div className="app-home">
      <HeaderHome activeHeader={true} />
      <div
        className={`${layout}  ${layoutHC}`}
        style={{
          backgroundImage: `url(${bgHelpCenter})`,
        }}
      >
        <div className={wpTitle}>
          <div className={`title ${title}`}>{translate("help.title")}</div>
        </div>
      </div>

      <div className={`${content} ${contentHC}`}>
        <div className={left}>
          <div className={wpContentLeft}>
            <div className={title}>{translate("help.titleForm")}</div>
            <div className={`${subTitle}  ${subTitleHC}`}>
              {translate("help.subTitleForm")}
            </div>

            <form onSubmit={onSubmit}>
              <div style={{ maxWidth: 500 }}>
                <InputAuth
                  isValidateView
                  Icon={ProfileIcon}
                  title={translate("help.inputName")}
                  activeIcon={true}
                  value={contactName}
                  name={"contactName"}
                  type="text"
                  onChange={onChange}
                />

                <InputAuth
                  isValidateView
                  Icon={NoPhone}
                  title={translate("help.inputPhone")}
                  value={mobilePhone}
                  name="mobilePhone"
                  onChange={onChange}
                  activeIcon={true}
                  activeProps={() => valueValidat.mobilePhone}
                  type={"number"}
                />

                <InputAuth
                  isValidateView
                  Icon={EmailIcon}
                  title={translate("help.inputEmail")}
                  value={email}
                  name="email"
                  onChange={onChange}
                  activeIcon={true}
                  activeProps={() => valueValidat.email}
                  type={"email"}
                />

                <InputTextArea
                  type="text"
                  title={translate("help.inputNotes")}
                  value={message}
                  textArea={true}
                  name="message"
                  activeIcon={true}
                  onChange={onChange}
                  resize={"none"}
                />
              </div>
              <div
                className={`${textError}  ${
                  filterValue.length ? showTextEerror : ""
                }`}
                style={{ fontSize: 14, marginTop: 20 }}
              >
                {translate("alert.allMandatory")}
              </div>
              <div
                className={`${textError}  ${
                  errorSendEmail ? showTextEerror : ""
                }`}
                style={{ fontSize: 14, marginTop: 20 }}
              >
                {translate("alert.networkProblem")}
              </div>
              <div
                className={`${textError}  ${
                  !valueValidat.email && onclick && !filterValue.length
                    ? showTextEerror
                    : ""
                }`}
                style={{ fontSize: 14, marginTop: 20 }}
              >
                {translate("alert.invalidEmail")}
              </div>
              <div
                className={`${textError}  ${
                  !valueValidat.mobilePhone && onclick && !filterValue.length
                    ? showTextEerror
                    : ""
                }`}
                style={{ fontSize: 14, marginTop: 20 }}
              >
                {translate("alert.invalidPhoneNumber")}
              </div>
              <div
                className={`${textError}  ${
                  successSendEmail ? showTextEerror : ""
                }`}
                style={{ fontSize: 14, marginTop: 20, color: "blue" }}
              >
                {translate("alert.successSendEmail")}
              </div>

              <button
                className={`${btnLeft} ${btnRes}`}
                type="submit"
                onClick={onSubmit}
              >
                {requestSendEmail ? <Loading /> : <a>{translate("send")}</a>}
              </button>
            </form>
            {width < 800 ? (
              <div>
                <div className={itemCardInfo1} onClick={() => onLink(noLink)}>
                  <CardInfo
                    icon={csIcon}
                    title={translate("customerServices")}
                    subTitle={"021-2452-3147"}
                  />
                </div>
                <div
                  className={itemCardInfo2}
                  onClick={() => onLink(emailLink)}
                >
                  <CardInfo
                    icon={emailIcon}
                    title={translate("emailSupport")}
                    subTitle={"inquiry@logol.co.id"}
                  />
                </div>
                <div
                  className={itemCardInfo2}
                  onClick={() => onLink(djpktnLink)}
                >
                  <CardInfo2
                    icon={djpktn1}
                    title={translate("djpktnSubmission")}
                    subTitle={"pengaduan.konsumen@kemendag.go.id"}
                  />
                </div>
              </div>
            ) : (
              ""
            )}
          </div>
        </div>

        {width > 800 ? (
          <div className={right}>
            <div className={bgRight}></div>
            <div className={bgRight}></div>
            <div className={bgRight}></div>

            <div className={wpContentRight}>
              <div className={itemCardInfo1}>
                <div style={{ width: 384 }} onClick={() => onLink(noLink)}>
                  <CardInfo
                    icon={csIcon}
                    title={translate("customerServices")}
                    subTitle={"021-2452-3147"}
                  />
                </div>
              </div>
              <div className={itemCardInfo2}>
                <div style={{ width: 384 }} onClick={() => onLink(emailLink)}>
                  <CardInfo
                    icon={emailIcon}
                    title={translate("emailSupport")}
                    subTitle={"inquiry@logol.co.id"}
                  />
                </div>
              </div>
              <div
                style={{
                  width: 384,
                  border: "2px solid #ECECEC",
                  marginTop: 48,
                  marginBottom: 16,
                }}
              ></div>
              <div className={itemCardInfo2}>
                <CardInfo2
                  onClick={() => onLink(djpktnLink)}
                  title={translate("djpktnSubmission")}
                  subTitle={"pengaduan.konsumen@kemendag.go.id"}
                  phone="+62-21-3858171, +62-21-3451692"
                />
              </div>
            </div>
          </div>
        ) : (
          ""
        )}
      </div>
      <Footer />
    </div>
  );
}

export default withTranslation()(Index);
