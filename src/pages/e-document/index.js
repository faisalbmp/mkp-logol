import React, { useCallback, useLayoutEffect, useState } from 'react'
import { useSelector, useDispatch, } from 'react-redux';
import { useHistory } from 'react-router-dom'

import HeaderHome from 'components/header/HeaderHome'
import Footer from 'components/footer'
import CustomerService from 'components/container/customerService'
import { setScrollposition } from 'actions'
import JoinOur from 'components/container/joinOur'
import CardwhyUseLogol from 'components/cards/CardwhyUseLogol'
import bgeDocument from 'assets/images/bg/bg-e-document.png'
import IconSvgEDocDO from 'assets/images/svg/iconSvgEDocDO.svg'
import IconSvgEDocBL from 'assets/images/svg/iconSvgEDocBL.svg'
import IconSvgEDocSI from 'assets/images/svg/iconSvgEDocSI.svg'
import IconSvgEDocSP2 from 'assets/images/svg/iconSvgEDocSP2.svg'
import { withTranslation } from 'react-i18next';
import { content, bgTop, contentBg, title, subTitle, cardEDoc, wpCard, titleCard, subTitleCard } from 'assets/scss/e-document/index.module.scss'
import { ArrowUp } from 'components/icon/iconSvg';

const dataDoc = [
    {
        icon: IconSvgEDocDO,
        title: "E-Delivery Order",
        dec: "e-deliveryOrder"
    },
    {
        icon: IconSvgEDocBL,
        title: "E-Bill of Lading",
        dec: "e-billOfLanding"
    },
    {
        icon: IconSvgEDocSI,
        title: "E-Shipping Instruction",
        dec: "e-shippingInstruction"
    },
    {
        icon: IconSvgEDocSP2,
        title: "E-Gate Pass",
        dec: "e-gatePass"
    },
]

function Index({ t }) {
    const translate = t;
    const dispatch = useDispatch();
    const { scrollposition } = useSelector(state => state.home);
    const callBack = useCallback(() => { dispatch(setScrollposition()) }, [scrollposition])
    // const history = useHistory()
    const [color, setColor] = useState('#005EEB');

    useLayoutEffect(() => {
        window.addEventListener('scroll', () => callBack())
        return () => {
            window.removeEventListener('scroll', () => callBack())
        }
    }, []);

    return (
        <div className="app-home">
            <HeaderHome activeHeader={true} />
            <div
                className={bgTop}
                style={{
                    backgroundImage: `url(${bgeDocument})`,
                }}
            >
                <div className={contentBg}>
                    <div className={title}>{translate('e-document.e-document')}</div>
                    <div className={subTitle}>{translate('e-document.title')}</div>
                </div>
            </div>

            <div className={content}>
                <div className={wpCard}>
                    {
                        dataDoc.map((value, index) => {
                            const Icon = value.icon
                            return (
                                <div key={index} className={cardEDoc}>
                                    <img src={value.icon} alt="e-doc" />
                                    <div className={titleCard}>{value.title}</div>
                                    <div className={subTitleCard}>{translate(`e-document.${value.dec}`)}</div>
                                </div>
                            )
                        })
                    }
                </div>
                <CardwhyUseLogol />
                <JoinOur />
            </div>
            <CustomerService />
            <div className={`wrapper-floating-scroll-top ${window.scrollY > 100 && 'wrapper-floating-scroll-top-active'}`}>
                <div className="grid" onClick={() => {
                    window.scrollTo(0, 0);
                }}>
                    <span className="showSpan">Scroll To Top</span>
                    <div>
                        <div
                            className={`floating-scroll-top ${window.scrollY > 100 && 'floating-scroll-top-active'}`}
                            onMouseOver={() => setColor('white')}
                            onMouseOut={() => setColor('#005EEB')}>
                            <ArrowUp color={color} />
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div >
    )
}

export default withTranslation()(Index)
