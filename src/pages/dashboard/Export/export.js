import React, { } from 'react'
import LayoutDashboard from 'components/dashboard/layout'
import { connect } from 'react-redux'
import { RightArrow, Timer, PrevLeftArrow, NextRightArrow, DetailIcon, EditIcon, TruckIcon, BookIcon, DropDownArrow, DestinationIcon, DropDownUpArrow, UploadIcon, PortHandleIcon } from "components/icon/iconSvg"
import Schedule from "components/export/schedule"
import DetailContainer from "components/export/container"
import Address from "components/export/selectAddress"
import AdditionalService from "components/export/additional-service"
import Summary from "components/export/Summary"
import SteppedProgressBar from "components/SteppedProgressBar"
import CompleteOrder from "components/export/completeOrder"
import { uploadFile } from "actions/uploadFile"
import ConfirmationBox from "components/confirmation/confirmationDialogeBox"
import ErrorAlert from "components/alert/ErrorDialougeBox"
import { addTrackers } from 'react-ga'
import moment from "moment"
import MautaImport from "components/container/Import/mautImport"
import { addNewCart, getAdditionalServices, addOrUpdateCartItem, getCart, createNewOrder, deleteCart, createFactory, getFactoryList, } from "actions"
import { useHistory, Redirect, useLocation } from 'react-router-dom';
import LoadingLoad from 'components/loading/loadingLoad'
import NextButton from 'components/button/nextBtn'
import PreviousBtn from "components/button/previousBtn";
import { getCookie } from 'utils/cookies';
class Export extends React.Component {
    constructor() {
        super();
        this.state = {
            exportSelected: true,
            importSelected: false,
            tentukanProgress: "active",
            tentukanIconColor: "",
            lengkapiProgress: "pending",
            lengkapiIconColor: "#B8C1C6",
            mautProgress: "pending",
            mautIconColor: "#B8C1C6",
            layananProgress: "pending",
            layananIconColor: "#B8C1C6",
            ringkaanProgress: "pending",
            ringkaanIconColor: "#B8C1C6",
            errorVisible: false,
            plusIconColor: '#B8C1C6',
            errorMessage: '',
            disabledPrevButton: true,
            loading: false,
            order: {
                destination1 : {
                    "name" : '',
                    "address" : '',
                    "liftOn" : '',
                    "liftOnName": "",
                    "liftOnPhone" : "",
                    "liftOff" : '',
                    "liftOffName": "",
                    "liftOffPhone" : "",
                    "type" : "",
                    "id" : ""
                },
                destination2 : {
                    "name" : '',
                    "address" : '',
                    "liftOn" : '',
                    "liftOnName": "",
                    "liftOnPhone" : "",
                    "liftOff" : '',
                    "liftOffName": "",
                    "liftOffPhone" : "",
                    "type" : "",
                    "id" : ""
                },
                destination3 : {
                    "name" : '',
                    "address" : '',
                    "liftOn" : '',
                    "liftOnName": "",
                    "liftOnPhone" : "",
                    "liftOff" : '',
                    "liftOffName": "",
                    "liftOffPhone" : "",
                    "type" : "",
                    "id" : ""
                },
                error : {
                    "destination1Error" : {
                        "msg" : ""
                    },
                    "destination2Error" : {
                        "msg" : "",
                        "districtError" : ""
                    },
                    "destination3Error" : {
                        "msg" : "",
                        "districtError" : ""
                    },
                    "address" : {
                        "liftOnName" : {
                            "msg" : ""
                        },
                        "liftOnPhone" : {
                            "msg" : ""
                        },
                        "liftOffName" : {
                            "msg" : ""
                        },
                        "liftOffPhone" : {
                            "msg" : ""
                        },
                        "shippingLine" : {
                            "msg" : ""
                        },
                        "vesselName" : {
                            "msg" : ""
                        },
                        "portOfDischarge" : {
                            "msg" : ""
                        },
                        "voyageNo" : {
                            "msg" : ""
                        },
                        "portOfFinalDestination" : {
                            "msg" : ""
                        },
                        "ShippingDate" : {
                            "msg" : ""
                        },"weight" : {
                            "msg" : ""
                        }
                    }
                },
                //"destination1": '',
                //"destination1ID": "",
                //"destination2": '',
                //"destination2phone": '',
                //"destination2Name": '',
                //"destination2Address": '',
                "destination2Index": "",
                "districtId": "",
                //"destination3": '',
                //"destination3ID": "",
                "sppbDocumentName": "",
                "SppbDocument": "",
                "sppbDate" : "",
                "deliveryOrderExpDate" : "",
                "pibName" : "",
                "pibDocument" : "",
                "pibDate" : "",
                "siDocument": '',
                "siDocumentName": '',
                "doDocument": '',
                "doDocumentName": '',
                "suratJalanDocument": '',
                "suratJalanDocumentName": '',
                "billOfLandingDocName": "",
                "billOfLandingDocument": "",
                "deliveryDocumentName": "",
                "deliveryDocument": "",
                "kontainerCheck": false,
                "liftOnCheck": false,
                "liftOffCheck": false,
                "liftOnName": '',
                "liftOnPhone": '',
                "liftOffName": '',
                "liftOffPhone": '',
                "shippingLine": '',
                "vesselName": '',
                "portOfDischarge": '',
                "ShippingDate": '',
                "voyageNo": '',
                "portOfFinalDestination": '',
                "Weight": '',
                "containerInformation": [],
                "service": [],
                "depoService" : {
                    "isService" : false,
                    "pilihDEPO" : "",
                    "tanggalPenitipan" : ""
                },
                "orderType": "EXPORT"
            },
            containerInformation: {},
            showConfirmBox: false,
            steps: [
                {
                    icon: <Timer color={"#004DFF"} />,
                    label: "Tentukan Jadwal",
                    status: "active"
                },
                {
                    icon: <EditIcon color={"#B8C1C6"} />,
                    label: "Lengkapi Data",
                    status: "pending"
                },
                {
                    icon: <DetailIcon color={"#B8C1C6"} />,
                    label: "Muat Barang",
                    status: "pending"
                },
                {
                    icon: <TruckIcon color={"#B8C1C6"} plusColor={"#B8C1C6"} />,
                    label: "Layanan Tambahan",
                    status: "pending"
                },
                {
                    icon: <BookIcon color={"#B8C1C6"} />,
                    label: "Ringkasan",
                    status: "pending"
                },
            ],
            latestDate: '',
            earliestDate: '',
        };
        this.expandableSeeMore = React.createRef();
    }

    selectedDepo = (depo) => {
        const { order } = this.state;
        order.destination1 = depo.name;
        order.destination1ID = depo.depoID;
        this.setState({
            order: order
        })
    }
    selectedPickupData = (data) => {
        const { order } = this.state;
        order.destination1.name = data.name
        order.destination1.address = data.address
        order.destination1.liftOn = data.liftOn
        order.destination1.liftOnName =  data.liftOnName
        order.destination1.liftOnPhone =  data.liftOnPhone
        order.destination1.liftOff = data.liftOff
        order.destination1.liftOffName =  data.liftOffName
        order.destination1.liftOffPhone =  data.liftOffPhone
        order.destination1.type = data.type
        order.destination1.id =  data.id
        order.error.destination1Error.msg = ""

        if(data.type === "Factory") {
            order.districtId = data.districtID
        }

        this.setState({
            order : order
        }, () => {
        })
    }

    deliveryDataSelected = (data) => {
        const { order } = this.state;
        order.destination2.name = data.name
        order.destination2.address = data.address
        order.destination2.liftOn = data.liftOn
        order.destination2.liftOnName =  data.liftOnName
        order.destination2.liftOnPhone =  data.liftOnPhone
        order.destination2.liftOff = data.liftOff
        order.destination2.liftOffName =  data.liftOffName
        order.destination2.liftOffPhone =  data.liftOffPhone
        order.destination2.type = data.type
        order.destination2.id =  data.id
        order.error.destination2Error.msg = ""

        if(data.type === "Factory") {
            order.districtId = data.districtID
        } else {
            if(order.destination1.type != "Factory" || data.type != 'Factory') {
                if(data.districtID == null || data.districtID == "") {
                    order.error.destination2Error.districtError = "District Cannot be null or empty."
                } else {
                    order.districtId = data.districtID
                    order.error.destination2Error.districtError = ""
                }
            }
        }


        this.setState({
            order : order
        }, () => {
        })
    }

    dropOffDataSelected = (data) => {
        const { order } = this.state;
        order.destination3.name = data.name
        order.destination3.address = data.address
        order.destination3.liftOn = data.liftOn
        order.destination3.liftOnName =  data.liftOnName
        order.destination3.liftOnPhone =  data.liftOnPhone
        order.destination3.liftOff = data.liftOff
        order.destination3.liftOffName =  data.liftOffName
        order.destination3.liftOffPhone =  data.liftOffPhone
        order.destination3.type = data.type
        order.destination3.id =  data.id
        order.error.destination3Error.msg = ""
        if(data.type === "Factory") {
            order.districtId = data.districtID
        } else {
            if(order.destination1.type != "Factory" && order.destination2.type != "Factory" && data.type != 'Factory') {
                if(data.districtID == null || data.districtID == "") {
                    order.error.destination3Error.districtError = "District Cannot be null or empty."
                } else {
                    order.districtId = data.districtID
                    order.error.destination3Error.districtError = ""
                }
            }
        }
        this.setState({
            order : order
        }, () => {
        })
    }

    selectedPickupAddress = async (address) => {
        const { order } = this.state;
        const { dispatch } = this.props
        const userId = getCookie('userId');
        var factoryData = {
            "address": address.pickupAddress,
            "customerID": userId,
            "factoryID": "",
            "latitude": address.latitude,
            "longitude": address.longitude,
            "mobilePhone": "",
            "name": address.pickupName,
            "officePhone": address.pickupPhoneNumber,
            "pic": ""
        }
        await dispatch(createFactory(factoryData)).then(() => {
            dispatch(getFactoryList(''));
        })
    }

    selectedFactoryAddress = (address, addressIndex) => {
        const { order } = this.state;
        order.destination2 = address.DistrictName;
        order.districtId = address.DistrictID;
        order.destination2Address = address.Address;
        order.destination2Name = address.FactoryName;
        order.destination2phone = address.mobilePhone;
        order.desdestination2Index = addressIndex
        this.setState({
            order: order
        })
    }

    selectedDeliveryLocation = (item) => {
        const { order } = this.state;
        order.destination3 = item.portUTC;
        order.destination3ID = item.portID;
        this.setState({
            order: order
        }, () => {
        })
    }

    SiDocumentUpload = (files) => {
        const { dispatch } = this.props;
        const formData = new FormData();
        const { order } = this.state;
        if (files == "") {
            order.siDocument = ''
        } else {
            formData.append('file', files[0]);
            formData.append('fileType', files[0].type);
            dispatch(uploadFile(formData)).then(() => {
                order.siDocument = this.props.uploadedFileData
                this.setState({
                    order: order
                })
            })
        }
    }

    getSIDocumentName = (val) => {
        const { order } = this.state;
        order.siDocumentName = val

        this.setState({
            order: order
        })
    }

    doDocumentUpload = (files) => {
        const { dispatch } = this.props;
        const formData = new FormData();
        const { order } = this.state;
        if (files == "") {
            order.doDocument = ''
        } else {
            formData.append('file', files[0]);
            formData.append('fileType', files[0].type);
            dispatch(uploadFile(formData)).then(() => {
                order.doDocument = this.props.uploadedFileData
                this.setState({
                    order: order
                })
            })
        }
    }

    getDODocumentName = (val) => {
        const { order } = this.state;
        order.doDocumentName = val

        this.setState({
            order: order
        })
    }

    getPibName = (val) => {
        const { order } = this.state;
        order.pibName = val

        this.setState({
            order: order
        })
    }

    pibDoumentUpload = (files) => {
        const { dispatch } = this.props;
        const formData = new FormData();
        const { order } = this.state;
        if (files == "") {
            order.pibDocument = ''
        } else {
            formData.append('file', files[0]);
            formData.append('fileType', files[0].type);
            dispatch(uploadFile(formData)).then(() => {
                order.pibDocument = this.props.uploadedFileData
                this.setState({
                    order: order
                })
            })
        }
    }

    getBillOfLandingName = (val) => {
        const { order } = this.state;
        order.billOfLandingDocName = val

        this.setState({
            order: order
        })
    }

    billOfLandingDocument = (files) => {
        const { dispatch } = this.props;
        const formData = new FormData();
        const { order } = this.state;
        if (files == "") {
            order.billOfLandingDocument = ''
        } else {
            formData.append('file', files[0]);
            formData.append('fileType', files[0].type);
            dispatch(uploadFile(formData)).then(() => {
                order.billOfLandingDocument = this.props.uploadedFileData
                this.setState({
                    order: order
                })
            })
        }
    }

    getDeliveryOrderName = (val) => {
        const { order } = this.state;
        order.deliveryDocumentName = val

        this.setState({
            order: order
        })
    }

    deliveryDocumen = (files) => {
        const { dispatch } = this.props;
        const formData = new FormData();
        const { order } = this.state;
        if (files == "") {
            order.deliveryDocument = ''
        } else {
            formData.append('file', files[0]);
            formData.append('fileType', files[0].type);
            dispatch(uploadFile(formData)).then(() => {
                order.deliveryDocument = this.props.uploadedFileData
                this.setState({
                    order: order
                })
            })
        }
    }

    sjDocumentUpload = (files) => {
        const { dispatch } = this.props;
        const formData = new FormData();
        const { order } = this.state;
        if (files == "") {
            order.suratJalanDocument = ''
        } else {
            formData.append('file', files[0]);
            formData.append('fileType', files[0].type);
            dispatch(uploadFile(formData)).then(() => {
                order.suratJalanDocument = this.props.uploadedFileData
                this.setState({
                    order: order
                })
            })
        }
    }

    getSJDocumentName = (val) => {
        const { order } = this.state;
        order.suratJalanDocumentName = val

        this.setState({
            order: order
        })
    }

    getSPPBDocumentName = (val) => {
        const { order } = this.state;
        order.sppbDocumentName = val

        this.setState({
            order: order
        })
    }

    SPPBDocumentUpload = (files) => {
        const { dispatch } = this.props;
        const formData = new FormData();
        const { order } = this.state;
        if (files == "") {
            order.SppbDocument = ''
        } else {
            formData.append('file', files[0]);
            formData.append('fileType', files[0].type);
            dispatch(uploadFile(formData)).then(() => {
                order.SppbDocument = this.props.uploadedFileData
                this.setState({
                    order: order
                })
            })
        }
    }

    kontainerCheckValue = (val) => {
        const { order } = this.state;
        const {newCart, dispatch, additionalService } = this.props
        let ind = 0;
        order.kontainerCheck = val
        if(val == true) {
            additionalService.value.map((item, index) => {
                if(item.serviceMasterCode === "AddSvcGatePass")  {
                    order.service.push(item)
                    var cartData = {
                        "cartID": newCart.value.cartID,
                        "cartItemType": "Service",
                        "containerTypeID": "",
                        "newQty": 1,
                        "serviceMasterID": item.serviceMasterID,
                        "withTurckingFee": true
                    }
                    dispatch(addOrUpdateCartItem(cartData)).then(() => {
                    })
                } 
            })
        } else {
            order.service.map((it, index) => {
                if (it.serviceMasterCode === "AddSvcGatePass") {
                    ind = index
                }
            })
            this.props.cartItemData.value.items.map((i, inde) => {
                if (i.itemType == "Service" && i.itemDesc == "Gate Pass") {
                    var cartDeleteData = {
                        "cartID": newCart.value.cartID,
                        "cartItemID": i.cartItemID
                    }
                    dispatch(deleteCart(cartDeleteData)).then(() => {
                        if (this.props.deletedCartData.statusCode == 200) {
                            dispatch(getCart(newCart.value.cartID)).then(() => {
                            })
                        }

                    })
                }
            })
            if (ind > -1) {
                order.service.splice(ind, 1)
            }
        }

        this.setState({
            order: order
        })
    }

    liftOnCheckValue = (val) => {
        const { order } = this.state;
        order.liftOnCheck = val

        this.setState({
            order: order
        })
    }

    liftOffCheckValue = (val) => {
        const { order } = this.state;
        order.liftOffCheck = val

        this.setState({
            order: order
        })
    }

    liftOnNameValue = (val) => {
        const { order } = this.state;
        order.liftOnName = val

        this.setState({
            order: order
        })
    }

    liftOnPhoneValue = (val) => {
        const { order } = this.state;
        order.liftOnPhone = val

        this.setState({
            order: order
        })
    }

    liftOFFNameValue = (val) => {
        const { order } = this.state;
        order.liftOffName = val

        this.setState({
            order: order
        })
    }

    liftOFFPhoneValue = (val) => {
        const { order } = this.state;
        order.liftOffPhone = val

        this.setState({
            order: order
        })
    }

    shippingLineValue = (val) => {
        const { order } = this.state;
        order.shippingLine = val

        this.setState({
            order: order
        })
    }

    vesselNameValue = (val) => {
        const { order } = this.state;
        if(val != "") {
            order.error.address.vesselName.msg = ""
        }
        order.vesselName = val

        this.setState({
            order: order
        })
    }

    pODValue = (val) => {
        const { order } = this.state;
        if(val != "") {
            order.error.address.portOfDischarge.msg = ""
        }
        order.portOfDischarge = val

        this.setState({
            order: order
        })
    }

    shippingDateValue = (val) => {
        const { order } = this.state;
        if(val != "") {
            order.error.address.ShippingDate.msg = ""
        }
        order.ShippingDate = val

        this.setState({
            order: order
        })
    }

    voyageNoValue = (val) => {
        const { order } = this.state;
        if(val != "") {
            order.error.address.voyageNo.msg = ""
        }
        order.voyageNo = val

        this.setState({
            order: order
        })
    }

    pFDValue = (val) => {
        const { order } = this.state;
        if(val != "") {
            order.error.address.portOfFinalDestination.msg = ""
        }
        order.portOfFinalDestination = val

        this.setState({
            order: order
        })
    }

    weightValue = (val) => {
        const { order } = this.state;
        order.Weight = val

        this.setState({
            order: order
        }, () => {
        })
    }

    nextButtonClicked = async () => {
        const { deliveryScheduleCompleted, depo, pickupAddress, deliveryLocation, order, tentukanProgress, lengkapiProgress, layananProgress, mautProgress, ringkaanProgress } = this.state;
        if (tentukanProgress == "active") {
            if(order.destination1.name == ""){
                order.error.destination1Error.msg = "This Field Is Required"
                this.setState({
                    order : order
                })
                return null;
            }
            if(order.destination2.name == "") {
                order.error.destination2Error.msg = "This Field Is Required"
                this.setState({
                    order : order
                })
                return null;
            }
            if(order.destination3.name == "" && (order.destination2.type != "Depo" && order.destination2.type != "Port" && order.destination2.type && "Storage")) {
                order.error.destination3Error.msg = "This Field Is Required"
                this.setState({
                    order : order
                })
                return null;
            }
            //if (order.destination1 != "" && order.destination2 != "" && order.destination2Address != "" && order.destination2Name != "" && order.destination2phone != "") {
                await this.createNewCart();
                // this.setState({
                //     tentukanProgress: "done",
                //     tentukanIconColor: "#B8C1C6",
                //     lengkapiIconColor: "#004DFF",
                //     lengkapiProgress: "active",
                //     disabledPrevButton: false,
                //     loading: false
                // }, () => {
                //     this.updateStatus()
                // })
                
            //} else {
            //     this.setState({
            //         errorVisible: true,
            //         errorMessage: 'Please Select Destinaltion 1 and Destination 2 Information'
            //     })
            // }
        }

        if (tentukanProgress == "done" && lengkapiProgress == "active") {
            this.validateAddress()
        }

        if (lengkapiProgress == "done" && mautProgress == "active") {
            await this.updateCartData()
        }

        if (mautProgress == "done" && layananProgress == "active") {
            this.setState({
                tentukanProgress: "done",
                lengkapiProgress: "done",
                mautProgress: "done",
                layananProgress: "done",
                layananIconColor: "#B8C1C6",
                ringkaanIconColor: "#004DFF",
                ringkaanProgress: "active",
                plusIconColor: "#B8C1C6"
            }, () => {
                this.updateStatus()
            })
        }

        if (layananProgress == "done" && ringkaanProgress == "active") {
            this.setState({
                showConfirmBox: true
            })
        }

    }

    updateStatus = () => {
        const { tentukanProgress, lengkapiProgress, layananProgress, mautProgress, ringkaanProgress, tentukanIconColor, lengkapiIconColor, layananIconColor, mautIconColor, ringkaanIconColor } = this.state;
        var steps = [
            {
                icon: <Timer color={tentukanIconColor} />,
                label: "Tentukan Jadwal",
                status: tentukanProgress
            },
            {
                icon: <EditIcon color={lengkapiIconColor} />,
                label: "Lengkapi Data",
                status: lengkapiProgress
            },
            {
                icon: <DetailIcon color={mautIconColor} />,
                label: "Muat Barang",
                status: mautProgress
            },
            {
                icon: <TruckIcon color={layananIconColor} plusColor={this.state.plusIconColor} />,
                label: "Layanan Tambahan",
                status: layananProgress
            },
            {
                icon: <BookIcon color={ringkaanIconColor} />,
                label: "Ringkasan",
                status: ringkaanProgress
            },
        ]

        this.setState({
            steps: steps
        })
    }

    setExportRadioValue = () => {
        const { order } = this.state
        var data = {
            "name" : '',
            "address" : '',
            "liftOn" : '',
            "liftOnName": "",
            "liftOnPhone" : "",
            "liftOff" : '',
            "liftOffName": "",
            "liftOffPhone" : "",
            "type" : "",
            "id" : ""
        }
        order.destination1 = data
        order.destination2 = data
        order.destination3 = data
        this.setState({
            exportSelected: !this.state.exportSelected,
            order: order
        }, () => {
            if (this.state.exportSelected) {
                order.orderType = "EXPORT"
                this.setState({
                    order: order,
                    importSelected: false
                }, () => {
                })
            } else {
                this.setState({
                    importSelected: true
                })
            }
        })
    }

    setImportRadioValue = () => {
        const { order } = this.state
        var data = {
            "name" : '',
            "address" : '',
            "liftOn" : '',
            "liftOnName": "",
            "liftOnPhone" : "",
            "liftOff" : '',
            "liftOffName": "",
            "liftOffPhone" : "",
            "type" : "",
            "id" : ""
        }
        order.destination1 = data
        order.destination2 = data
        order.destination3 = data
        this.setState({
            importSelected: !this.state.importSelected,
            order: order
        }, () => {
            if (this.state.importSelected) {
                order.orderType = "IMPORT"
                this.setState({
                    order: order,
                    exportSelected: false
                }, () => {
                })
            } else {
                this.setState({
                    exportSelected: true
                })
            }
        })
    }

    prevButtonClicked = () => {

        const { deliveryScheduleCompleted, depo, pickupAddress, deliveryLocation, order, tentukanProgress, lengkapiProgress, layananProgress, mautProgress, ringkaanProgress } = this.state;

        if (tentukanProgress == "done" && lengkapiProgress == "active") {
            this.setState({
                tentukanProgress: "active",
                lengkapiProgress: 'pending',
                tentukanIconColor: "#004DFF",
                lengkapiIconColor: "#B8C1C6",
                disabledPrevButton: true,
            }, async () => {
                this.updateStatus()
            })
        }

        if (lengkapiProgress == "done" && mautProgress == "active") {
            this.setState({
                lengkapiProgress: "active",
                mautProgress: "pending",
                lengkapiIconColor: "#004DFF",
                mautIconColor: "#B8C1C6",

            }, () => {
                this.updateStatus()
            })
        }

        if (mautProgress == "done" && layananProgress == "active") {
            this.setState({
                mautProgress: "active",
                layananProgress: "pending",
                mautIconColor: '#004DFF',
                layananIconColor: "#B8C1C6",
                plusIconColor: '#B8C1C6'
            }, () => {
                this.updateStatus()
            })
        }

        if (layananProgress == "done" && ringkaanProgress == "active") {
            this.setState({
                layananProgress: "active",
                ringkaanProgress: 'pending',
                layananIconColor: '#004DFF',
                ringkaanIconColor: "#B8C1C6",
                plusIconColor: '#16C89D'
            }, () => {
                this.updateStatus()
            })
        }

    }
    containerInfo = (data) => {
        this.setState({
            containerInformation: data
        })
    }

    onConfirmCancelPressed = () => {
        this.setState({
            showConfirmBox: false
        })

    }

    onConfirmSubmitPressed = () => {
        const { cartItemData, dispatch } = this.props
        const { order } = this.state

        var createOrder = {
            "cartID": cartItemData.value.cartID,
            "freightOrder": {
                "adminFees": [
                {
                    "adminFeeCode": "",
                    "adminFeeID": 0,
                    "desc": "",
                    "price": 0,
                    "qty": 0,
                    "value1": "",
                    "value2": "",
                    "value4": "",
                    "value5": "",
                    "vlaue3": ""
                }
                ],
                "containers": [
                {
                    "containerInfos": [
                    {
                        "containerNumber": "",
                        "location": "",
                        "sealNumber": ""
                    }
                    ],
                    "containerTypeID": "",
                    "desc": "",
                    "price": 0,
                    "qty": 0,
                    "stuffingDate": ""
                }
                ],
                "deliveryOrderID": "",
                "destPointID1": "",
                "destPointID2": "",
                "destPointID3": "",
                "doPdf": "",
                "liftOffContactNo": "",
                "liftOffPicName": "",
                "liftOnContactNo": "",
                "liftOnPicName": "",
                "pib": "",
                "pibAttachment": "",
                "portFinalDestination": "",
                "portOfDischarge": "",
                "shippingDate": "",
                "shippingID": "",
                "siPdf": "",
                "suratJalanpdf": "",
                "totalWeight": 0,
                "vesselID": "",
                "voyageNo": ""
            },
            "truckOrder": {
                "cartItems": cartItemData.value.items,
                "deliveryOrderID": order.doDocumentName,
                "destPointID1": order.destination1.id,
                "destPointID2": order.destination2.id,
                "destPointID3": order.destination3.id,
                "districtID": order.districtId,
                "doPdf": order.doDocument,
                "isLeftOff": order.destination2.liftOff ? order.destination2.liftOff : order.destination3.liftOff,
                "isLeftOn": order.destination1.liftOn,
                "liftOffContactNo": order.destination2.liftOff ? order.destination2.liftOffPhone : order.destination3.liftOffPhone,
                "liftOffPicName": order.destination2.liftOff ? order.destination2.liftOffName : order.destination3.liftOffName,
                "liftOnContactNo": order.destination1.liftOnPhone,
                "liftOnPicName": order.destination1.liftOnName,
                "orderType": order.orderType,
                "pod": order.portOfDischarge,
                "portFd": order.portOfFinalDestination,
                "shippingDate": order.ShippingDate,
                "shippingInvoiceID": order.siDocumentName,
                "siPdf": order.siDocument,
                "suratJalanpdf": order.suratJalanDocument,
                "tierID": "TIER-0001",
                "totalWeight": order.Weight,
                "vesselName": order.vesselName,
                "voyageNo": order.voyageNo
            }
        }

        var infoData = {
            "containerNumber": "",
            "location": "",
            "sealNumber": ""
        }

        order.containerInformation.map((item, index) => {
            createOrder.truckOrder.cartItems.map((it, i) => {
                if (order.orderType === "IMPORT") {
                    it.containerInfos = item.info
                }
                if ("CNTP-0001" == it.containerTypeID) {
                    it.stuffingDate = item.date
                }
            })
        })

        dispatch(createNewOrder(createOrder)).then(() => {
            if (this.props.newCustomerOrder.statusCode == "200") {
                // this.setState({
                //     orderCreated : true
                // })

            }
        })
    }

    containerTypeData = (data) => {
        var dates = []
        data.map((item, index) => {
            dates.push(item.date)
        })
        var latest = new Date(Math.max.apply(null, dates));
        var earliest = new Date(Math.min.apply(null, dates));
        const { order } = this.state;
        order.containerInformation = data
        this.setState({
            order: order,
            latestDate: latest,
            earliestDate: earliest
        })
    }

    createNewCart = () => {
        this.setState({ loading: true })
        const { dispatch } = this.props;
        const { order } = this.state;
        let cartData = {
            "cartType": "",
            "logolDistrictID": order.districtId
        }

        if (Object.keys(this.props.newCart).length > 0) {
            this.setState({
                tentukanProgress: "done",
                tentukanIconColor: "#B8C1C6",
                lengkapiIconColor: "#004DFF",
                lengkapiProgress: "active",
                disabledPrevButton: false,
                loading: false
            }, () => {
                this.updateStatus()
            })
        } else {
            dispatch(addNewCart(cartData)).then(() => {
                if (this.props.newCart.statusCode == "200") {
                    this.getAdditionalService()
                }
            })
        }
    }

    getAdditionalService = () => {
        const { dispatch, newCart } = this.props;
        var cartID = newCart.value.cartID
        dispatch(getAdditionalServices()).then(() => {
            if (this.props.additionalService.statusCode == "200") {
                this.setState({
                    tentukanProgress: "done",
                    tentukanIconColor: "#B8C1C6",
                    lengkapiIconColor: "#004DFF",
                    lengkapiProgress: "active",
                    disabledPrevButton: false,
                    loading: false
                }, () => {
                    this.updateStatus()
                })
            }
        })
    }

    updateCartData = () => {
        const { dispatch, newCart } = this.props;
        const { order } = this.state;
        var count = 0;
        if (order.containerInformation == "") {
            this.setState({
                errorVisible: true,
                errorMessage: 'Please Select Container Information'
            })
            return;
        }
        this.setState({ loading: true })
        if (Object.keys(this.props.cartItemData).length > 0) {
            this.setState({
                tentukanProgress: "done",
                lengkapiProgress: "done",
                mautProgress: "done",
                mautIconColor: "#B8C1C6",
                layananIconColor: "#004DFF",
                layananProgress: "active",
                plusIconColor: '#16C89D',
                loading: false
            }, () => {
                this.updateStatus()
            })
        } else {
            order.containerInformation.map((item, index) => {
                var cartData = {
                    "cartID": newCart.value.cartID,
                    "cartItemType": "Container",
                    "containerTypeID": "CNTP-0001",
                    "newQty": item.quantity,
                    "serviceMasterID": "",
                    "withTurckingFee": true
                }

                dispatch(getCart(newCart.value.cartID)).then(() => {
                    dispatch(addOrUpdateCartItem(cartData)).then(() => {
                        if (this.props.cartItemData.statusCode == "200") {
                            count += 1
                            this.props.additionalService.value.map((item, index) => {
                                if (item.serviceMasterCode === "TruckQuality") {
                                    const { order } = this.state;
                                    order.service.push(item)
                                    var cartData = {
                                        "cartID": newCart.value.cartID,
                                        "cartItemType": "Service",
                                        "containerTypeID": "",
                                        "newQty": 1,
                                        "serviceMasterID": item.serviceMasterID,
                                        "withTurckingFee": true
                                    }
                                    dispatch(addOrUpdateCartItem(cartData))
                                    this.setState({
                                        order: order
                                    })
                                }
                            })
                            if (count === order.containerInformation.length) {
                                this.setState({
                                    tentukanProgress: "done",
                                    lengkapiProgress: "done",
                                    mautProgress: "done",
                                    mautIconColor: "#B8C1C6",
                                    layananIconColor: "#004DFF",
                                    layananProgress: "active",
                                    plusIconColor: '#16C89D',
                                    loading: false
                                }, () => {
                                    this.updateStatus()
                                })
                            }
                        }
                    })
                })
            })

        }
    }

    validateAddress = () => {
        const { order } = this.state;
        if(order.destination1.liftOnName == "" && !order.destination1.liftOn) {
            order.error.address.liftOnName.msg = "This Field Is Required"
            this.setState({
                order : order
            })
            return null;
        }
        if(order.destination1.liftOnPhone == "" && !order.destination1.liftOn) {
            order.error.address.liftOnPhone.msg = "This Field Is Required"
            this.setState({
                order : order
            })
            return null;
        }
        // if((order.destination2.liftOffName == "" && order.destination3.liftOffName == "") && (!order.destination2.liftOff || !order.destination3.liftOff)) {
        //     alert('hi')
        //     order.error.address.liftOffName.msg = "This Field Is Required"  
        //     this.setState({
        //         order : order
        //     })
        //     return null;
        // }
        // if((order.destination2.liftOffPhone == "" && order.destination3.liftOffPhone == "") && (!order.destination2.liftOff || !order.destination3.liftOff)) {
        //     order.error.address.liftOffPhone.msg = "This Field Is Required"
        //     this.setState({
        //         order : order
        //     })
        //     return null;
        // }
        if (order.shippingLine == "" && (order.destination1.liftOn || order.destination2.liftOff || order.destination3.liftOff)) {
            order.error.address.shippingLine.msg = "This Field Is Required"
            this.setState({
                order : order
            })
            return null;
        }
        if (order.vesselName == "" && (order.destination1.liftOn || order.destination2.liftOff || order.destination3.liftOff)) {
            order.error.address.vesselName.msg = "This Field Is Required"
            this.setState({
                order : order
            })
            return null;
        }
        if (order.voyageNo == "" && (order.destination1.liftOn || order.destination2.liftOff || order.destination3.liftOff)) {
            order.error.address.voyageNo.msg = "This Field Is Required"
            this.setState({
                order : order
            })
            return null;
        }
        if (order.portOfDischarge == "" && (order.destination1.liftOn || order.destination2.liftOff || order.destination3.liftOff)) {
            order.error.address.portOfDischarge.msg = "This Field Is Required"
            this.setState({
                order : order
            })
            return null;
        }
        if (order.portOfFinalDestination == "" && (order.destination1.liftOn || order.destination2.liftOff || order.destination3.liftOff)) {
            order.error.address.portOfFinalDestination.msg = "This Field Is Required"
            this.setState({
                order : order
            })
            return null;
        }
        if (order.ShippingDate == "" && (order.destination1.liftOn || order.destination2.liftOff || order.destination3.liftOff)) {
            order.error.address.ShippingDate.msg = "This Field Is Required"
            this.setState({
                order : order
            })
            return;
        }
        if (order.Weight == "" && (order.destination1.liftOn || order.destination2.liftOff || order.destination3.liftOff)) {
            order.error.address.weight.msg = "This Field Is Required"
            this.setState({
                order : order
            })
            return;
        }

        this.setState({
            tentukanProgress: "done",
            lengkapiProgress: "done",
            lengkapiIconColor: "#B8C1C6",
            mautIconColor: "#004DFF",
            mautProgress: "active",
            loading: false
        }, () => {
            this.updateStatus()
        })
    }

    selectDepoAddService = (item) => {
        const { order } = this.state;
        const { newCart, dispatch } = this.props
        let ind = 0;
        if (item == "") {
            order.depoService.isService = false
            order.service.map((it, index) => {
                if (it.serviceMasterCode === "DepoStorage") {
                    ind = index
                }
            })
            this.props.cartItemData.value.items.map((i, inde) => {
                if (i.itemType == "Service" && i.itemDesc == "Depo Storage") {
                    var cartDeleteData = {
                        "cartID": newCart.value.cartID,
                        "cartItemID": i.cartItemID
                    }
                    dispatch(deleteCart(cartDeleteData)).then(() => {
                        if (this.props.deletedCartData.statusCode == 200) {
                            dispatch(getCart(newCart.value.cartID)).then(() => {
                            })
                        }

                    })
                }
            })
            if (ind > -1) {
                order.service.splice(ind, 1)
            }
        } else {
            order.service.push(item)
            order.depoService.isService = true
            var cartData = {
                "cartID": newCart.value.cartID,
                "cartItemType": "Service",
                "containerTypeID": "",
                "newQty": 1,
                "serviceMasterID": item.serviceMasterID,
                "withTurckingFee": true
            }
            dispatch(addOrUpdateCartItem(cartData)).then(() => {
            })
        }
        this.setState({
            order: order
        }, () => {
            console.log('order === ', this.state.order)
        })
    }

    selectInsuranceAddService = (item) => {
        const { order } = this.state;
        const { newCart, dispatch } = this.props
        let ind = 0;
        if (item == "") {
            order.service.map((item, index) => {
                if (item.serviceMasterCode === "Insurance") {
                    ind = index
                }
            })
            this.props.cartItemData.value.items.map((i, inde) => {
                if (i.itemType == "Service" && i.itemDesc == "Insurance") {
                    var cartDeleteData = {
                        "cartID": newCart.value.cartID,
                        "cartItemID": i.cartItemID
                    }
                    dispatch(deleteCart(cartDeleteData)).then(() => {
                        if (this.props.deletedCartData.statusCode == 200) {
                            dispatch(getCart(newCart.value.cartID))
                        }

                    })
                }
            })
            if (ind > -1) {
                order.service.splice(ind, 1)
            }
        } else {
            order.service.push(item)
            var cartData = {
                "cartID": newCart.value.cartID,
                "cartItemType": "Service",
                "containerTypeID": "",
                "newQty": 1,
                "serviceMasterID": item.serviceMasterID,
                "withTurckingFee": true
            }
            dispatch(addOrUpdateCartItem(cartData))
        }
        this.setState({
            order: order
        })
    }

    selectTruckQualityAddService = (item) => {
        const { order } = this.state;
        order.service.push(item)
        this.setState({
            order: order
        })
    }

    selectFumigationAddService = (item) => {
        const { order } = this.state;
        const { newCart, dispatch } = this.props
        let ind = 0;
        if (item == "") {
            order.service.map((item, index) => {
                if (item.serviceMasterCode === "Fumigation") {
                    ind = index
                }
            })
            this.props.cartItemData.value.items.map((i, inde) => {
                if (i.itemType == "Service" && i.itemDesc == "Fumigation") {
                    var cartDeleteData = {
                        "cartID": newCart.value.cartID,
                        "cartItemID": i.cartItemID
                    }
                    dispatch(deleteCart(cartDeleteData)).then(() => {
                        if (this.props.deletedCartData.statusCode == 200) {
                            dispatch(getCart(newCart.value.cartID))
                        }

                    })
                }
            })
            if (ind > -1) {
                order.service.splice(ind, 1)
            }
        } else {
            order.service.push(item)
            var cartData = {
                "cartID": newCart.value.cartID,
                "cartItemType": "Service",
                "containerTypeID": "",
                "newQty": 1,
                "serviceMasterID": item.serviceMasterID,
                "withTurckingFee": true
            }
            dispatch(addOrUpdateCartItem(cartData))
        }
        this.setState({
            order: order
        })
    }

    toggleSeeMore = () => {
        const myReference = this.expandableSeeMore.current
        if (!this.state.disabledPrevButton) {
            this.setState({ openSeeMore: !this.state.openSeeMore }, () => {
                if (this.state.openSeeMore) {
                    myReference.style.height = "400px"
                } else {
                    myReference.style.height = "0px"
                }
            })
        }
    }

    resetData = (val) => {
        const { order } = this.state
        var data = {
            "name" : '',
            "address" : '',
            "liftOn" : '',
            "liftOnName": "",
            "liftOnPhone" : "",
            "liftOff" : '',
            "liftOffName": "",
            "liftOffPhone" : "",
            "type" : "",
            "id" : ""
        }
        if(val === 'Destination1') {
            order.destination2 = data;
            order.destination3 = data;
            this.setState({
                order : order
            })
        }

        if(val === "Destination2") {
            order.destination3 = data;
            this.setState({
                order : order
            })
        }
    }

    sppbDateSelected = (date) => {
        const { order } = this.state
        order.sppbDate = date
        this.setState({
            order : order
        })
    }

    doExpDateSelected = (date) => {
        const { order } = this.state
        order.deliveryOrderExpDate = date
        this.setState({
            order : order
        })
    }

    pibDateSelected = (date) => {
        const { order } = this.state
        order.pibDate = date
        this.setState({
            order : order
        })
    }

    shipingLineChange = (e) => {
        const { order } = this.state;
        order.shippingLine = e

        this.setState({
            order: order
        })
    }

    shipingLineSelected = (e) => {
        const { order } = this.state;
        if(e != "") {
            order.error.address.shippingLine.msg = ""
        }
        order.shippingLine = e.name

        this.setState({
            order: order
        })
    }

    philipDepo = (val) => {
        const { order } = this.state;
        order.depoService.pilihDEPO = val
        this.setState({
            order : order
        })
    }

    depoTanggalDate = (date) => {
        const { order } = this.state;
        order.depoService.tanggalPenitipan = date
        this.setState({
            order : order
        })
    }

    render() {
        const { loading, tentukanProgress, lengkapiProgress, importSelected, exportSelected, order, errorVisible, errorMessage, mautProgress, layananProgress, ringkaanProgress, disabledPrevButton, openSeeMore, depo, showConfirmBox } = this.state;
        const { cartItemData, orderCreated } = this.props;
        if (orderCreated) {
            return <Redirect
                to={{
                    pathname: "/dashboard/list/truck",
                    //state: { value:  this.props.newCustomerOrder.value}
                }}
            />
        }
        // if (loading) {
        //     return <LoadingLoad />
        // }
        return (
            <LayoutDashboard>
                <div className="title" >Layanan Truk</div>
                <div className="subtitle-container" >
                    Layanan Truk &nbsp;
                    <RightArrow /> &nbsp;
                    Pesanan Baru
                </div>
                <div className="container1">
                    {
                        (ringkaanProgress == "done") &&
                        <CompleteOrder />
                    }
                    {
                        (ringkaanProgress != "done") &&
                        <div>
                            <div className="progress-container" >
                                <div className="progress-inner-container" style={{ height: "140px" }}>
                                    <SteppedProgressBar steps={this.state.steps} tentukanProgress={tentukanProgress} lengkapiProgress={lengkapiProgress} muatProgress={mautProgress} layananProgress={layananProgress} ringkasanProgress={ringkaanProgress} />
                                </div>
                            </div>

                            <div className="form-container">
                                {
                                    tentukanProgress == "active" &&
                                    <div>
                                        <Schedule data={order} resetData = {this.resetData} selectedPickupData = {this.selectedPickupData} deliveryDataSelected = {this.deliveryDataSelected} dropOffDataSelected = {this.dropOffDataSelected} selectedDepo={this.selectedDepo} pickupAddressSelected={this.selectedPickupAddress} selectedFactoryAddress={this.selectedFactoryAddress} selectedDeliveryLocation={this.selectedDeliveryLocation} exportRadioValue={this.setExportRadioValue} importRadioValue={this.setImportRadioValue} exportCheckValue={exportSelected} importCheckValue={importSelected} containerStyle={{ marginTop: 30 }} />
                                    </div>
                                }
                                {
                                    (lengkapiProgress == "active") &&
                                    <Address
                                        data={order}
                                        SiDocumentUpload={this.SiDocumentUpload}
                                        getSIDocumentName={this.getSIDocumentName}
                                        doDocumentUpload={this.doDocumentUpload}
                                        getDODocumentName={this.getDODocumentName}
                                        pibDoumentUpload={this.pibDoumentUpload}
                                        getPibName={this.getPibName}
                                        sjDocumentUpload={this.sjDocumentUpload}
                                        getSJDocumentName={this.getSJDocumentName}
                                        kontainerCheckValue={this.kontainerCheckValue}
                                        liftOnCheckValue={this.liftOnCheckValue}
                                        liftOffCheckValue={this.liftOffCheckValue}
                                        liftOnNameValue={this.liftOnNameValue}
                                        liftOnPhoneValue={this.liftOnPhoneValue}
                                        liftOFFNameValue={this.liftOFFNameValue}
                                        liftOFFPhoneValue={this.liftOFFPhoneValue}
                                        shippingLineValue={this.shippingLineValue}
                                        vesselNameValue={this.vesselNameValue}
                                        SPPBDocumentUpload={this.SPPBDocumentUpload}
                                        getSPPBDocumentName={this.getSPPBDocumentName}
                                        pODValue={this.pODValue}
                                        shippingDateValue={this.shippingDateValue}
                                        voyageNoValue={this.voyageNoValue}
                                        pFDValue={this.pFDValue}
                                        weightValue={this.weightValue}
                                        isExport={exportSelected}
                                        getBillOfLandingName={this.getBillOfLandingName}
                                        billOfLandingDocument={this.billOfLandingDocument}
                                        getDeliveryOrderName={this.getDeliveryOrderName}
                                        deliveryDocumen={this.deliveryDocumen}
                                        sppbDateSelected={this.sppbDateSelected}
                                        doExpDateSelected={this.doExpDateSelected}
                                        pibDateSelected={this.pibDateSelected}
                                        onShippingLineChange={this.shipingLineSelected}
                                        shippingLineChange={this.shipingLineChange}
                                        containerStyle = {{width:'960px'}}
                                    />
                                }

                                {
                                    (mautProgress == "active" && exportSelected) &&
                                    <DetailContainer
                                        data={order}
                                        containerData={this.containerTypeData}
                                        containerStyle = {{width:'960px'}}
                                    />
                                }
                                {
                                    (mautProgress == "active" && importSelected) &&
                                    <MautaImport
                                        // getBillOfLandingName={this.getBillOfLandingName}
                                        // billOfLandingDocument={this.billOfLandingDocument}
                                        // getDeliveryOrderName={this.getDeliveryOrderName}
                                        // deliveryDocumen={this.deliveryDocumen}
                                        containerData={this.containerTypeData}
                                        containerStyle = {{width:'960px'}}
                                    />
                                }
                                {
                                    (layananProgress == "active") &&
                                    <AdditionalService
                                        data={order}
                                        depoService={this.selectDepoAddService}
                                        insuranceService={this.selectInsuranceAddService}
                                        truckQualityService={this.selectTruckQualityAddService}
                                        fumigationService={this.selectFumigationAddService}
                                        philipDepo={this.philipDepo}
                                        depoTanggalDate={this.depoTanggalDate}
                                        containerStyle = {{width:'960px'}}
                                    />
                                }
                                {
                                    (ringkaanProgress == "active") &&
                                    <Summary
                                        summaryData={order} 
                                        containerStyle = {{width:'960px'}}
                                    />
                                }
                            </div>
                            {
                                (ringkaanProgress != "active") &&
                                <div className="container-break">
                                    <div className="see-more-wrapper" >
                                        <div className="break-horizontal-line" />
                                        <div className="break-title-container" disabled={disabledPrevButton} onClick={this.toggleSeeMore}>
                                            <span className="break-title" style={{ color: disabledPrevButton ? '#E0E5E8' : '#333' }} >Lihat Rincian</span>
                                            <span>
                                                {
                                                    openSeeMore
                                                        ? <DropDownUpArrow />
                                                        : <DropDownArrow color={disabledPrevButton ? '#E0E5E8' : '#333'} />
                                                }
                                            </span>
                                        </div>
                                        <div className="break-horizontal-line" />
                                    </div>
                                    <div className="expandableSeeMore" ref={this.expandableSeeMore}>
                                        <div className="see-more-details">
                                            <div className="see-more-container" style={{ width: '100%' }}>
                                                <div className="see-more-delivery-route" >
                                                    <div className="see-more-delivery-route-title">
                                                        Rute Pengiriman
                                                    </div>
                                                    <div className="see-more-route-container">
                                                        <div className="destination-point">
                                                            <div className="destination">
                                                                A
                                                            </div>
                                                        </div>
                                                        <div className="destination-address" >
                                                            {order.destination1.name}
                                                        </div>
                                                        <div className="destination-arrow" >
                                                            <DestinationIcon />
                                                        </div>
                                                        <div className="destination-point">
                                                            <div className="destination">
                                                                B
                                                            </div>
                                                        </div>
                                                        {
                                                            order.destination3 != ""
                                                                ?
                                                                <div className="destination-address" >
                                                                    {order.destination3.name}
                                                                </div>
                                                                :
                                                                <div className="destination-address" >
                                                                    {order.destination2.name}
                                                                </div>
                                                        }
                                                    </div>
                                                    <div className="see-more-delivery-route-title" style={{ marginTop: '40px' }}>
                                                        Waktu Pengiriman
                                                    </div>
                                                    <div className="destination-address" style={{ marginLeft: '0px', marginTop: '15px' }}>
                                                        {moment(this.state.earliestDate).format('DD MMM')} - {moment(this.state.latestDate).format('DD MMM, yyyy')}
                                                    </div>
                                                    <div className="see-more-delivery-route-title" style={{ marginTop: '40px' }}>
                                                        Jenis Pengiriman
                                                    </div>
                                                    <div className="destination-address" style={{ marginLeft: '0px', marginTop: '15px' }}>
                                                        {this.state.exportSelected ? 'Ekspor' : 'Impor'}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="see-more-container" style={{ width: '100%' }}>
                                                <div className="see-more-service-free">
                                                    <div className="see-more-delivery-route-title">
                                                        Biaya Layanan
                                                    </div>
                                                    {
                                                        Object.keys(cartItemData).length > 0 &&
                                                        cartItemData.value.items.map((item, index) => {
                                                            return (
                                                                <div className="price-container" style={{ marginTop: '20px' }}>
                                                                    <div className="price-label">
                                                                        {
                                                                            (item.grossAmt > 0 && item.containerTypeDesc != null) &&
                                                                            item.containerTypeDesc
                                                                        }
                                                                    </div>
                                                                    <div className="price-value">
                                                                        {
                                                                            (item.grossAmt > 0 && item.containerTypeDesc != null) &&
                                                                            'IDR ' + item.grossAmt
                                                                        }
                                                                    </div>
                                                                </div>
                                                            )
                                                        })
                                                    }
                                                    <div className="see-more-delivery-route-title" style={{ marginTop: '30px' }}>
                                                        Layanan Tambahan
                                                    </div>
                                                    {
                                                        order.service.length > 0 &&
                                                        order.service.map((item, index) => {
                                                            return (
                                                                <div className="price-container" style={{ marginTop: '20px' }}>
                                                                    <div className="price-label">
                                                                        {item.serviceMasterCode}
                                                                    </div>
                                                                    <div className="price-value">
                                                                        IDR  {item.price}
                                                                    </div>
                                                                </div>
                                                            )
                                                        })
                                                    }
                                                    <div className="price-total-container">
                                                        <div className="price-total-label">
                                                            Total
                                                        </div>
                                                        <div className="price-total-value">
                                                            {
                                                                Object.keys(cartItemData).length > 0 &&
                                                                cartItemData.value.grossAmt
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            }
                            <div className="form-footer">
                                <div className = "btn-container">
                                    <PreviousBtn
                                        title={"Kembali"}
                                        onClick={this.prevButtonClicked}
                                        style={{ width: 176 }}
                                    />
                                    <NextButton
                                        title={"Berikutnya"}
                                        onClick={this.nextButtonClicked}
                                        loading={loading}
                                    />
                                </div>
                            </div>
                        </div>
                    }
                </div>
                {
                    showConfirmBox &&
                    <ConfirmationBox onCancelPressed={this.onConfirmCancelPressed} onSubmitPressed={this.onConfirmSubmitPressed} />
                }
                <ErrorAlert
                    message={errorMessage}
                    onErrorOkPressed={() => this.setState({ errorVisible: false })}
                    errorVisible={errorVisible}
                />
            </LayoutDashboard>
        )
    }
}

function mapStateToProps(state) {
    return {
        uploadedFileData: state.uploadFile.uploadedFileData,
        newCart: state.createNewCart.newCart,
        additionalService: state.additionalServices.additionalService,
        cartItemData: state.cartItem.cartItemData,
        newCustomerOrder: state.newOrder.newCustomerOrder,
        orderCreated: state.newOrder.orderCreated,
        deletedCartData: state.deletedCart.deletedCartData,
        factoryCreated: state.newFactory.factoryCreated
    }
}

export default connect(mapStateToProps)(Export)
