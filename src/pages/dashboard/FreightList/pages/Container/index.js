import React, { useState } from "react";
import { TextXXL, TextS, TextL, TextM } from "components/text";
import LayoutDashboard from "components/dashboard/layout";
import Styles from "assets/scss/dashboard/freightlist/container.module.scss";
import {
  ArrowRightBreadcrumb,
  PlusCircleIcon,
  SearchIcon,
} from "components/icon/iconSvg";
import { Colors } from "configs";
import FreightListItem from "components/container/freight/freightListItem";
import Select from 'components/dashboard/select';
import Pagination from "components/paginations/pagination";
import FreightDetailPopup from "components/modal/freightDetailPopup";
// import FreightPopup from "components/modal/freightPopup";

const renderTitle = () => {
  return (
    <div className={Styles.header}>
      <div className={Styles.title}>
        <TextXXL textStyle="bold">Kontainer</TextXXL>
        <div className={Styles.breadcrumb}>
          <TextS className={Styles.label}>Daftar Pemesanan</TextS>
          <ArrowRightBreadcrumb />
          <TextS className={Styles.label}>Kontainer</TextS>
        </div>
      </div>
      <button className={Styles.newOrderButton}>
        <TextL textStyle="bold" className={Styles.buttonLabel}>
          Buat Pesanan
        </TextL>
        <PlusCircleIcon fill={Colors.text.mainblue} />
      </button>
    </div>
  );
};

const TabItem = ({ label, active }) => {
  return (
    <div className={`${Styles.tabItem} ${active ? Styles.active : ""}`}>
      <TextM
        textStyle="bold"
        color={active ? Colors.text.black : Colors.text.grey1}
      >
        {label}
      </TextM>
    </div>
  );
};
const renderTabs = () => {
  return (
    <div className={Styles.tabContainer}>
      <TabItem label="Dalam Proses" active />
      <TabItem label="Riwayat" />
    </div>
  );
};

const SearchBox = () => {
  return (
    <div className={Styles.searchBox}>
      <input type="text" placeholder="Search" />
      <SearchIcon width={14} height={14} />
    </div>
  );
};

const ResultCount = () => {
  return (
    <div className={Styles.resultCount}>
      {/* <TextS color={Colors.text.grey3}>Tampilkan</TextS> */}
      <Select
        number={50}
        setSelect={() => { }}
        defaultValue={10}
      />
      {/* <TextS color={Colors.text.grey3}>hasil per halaman</TextS> */}
    </div >
  );
};

const renderToolBar = () => {
  return (
    <div className={Styles.toolbar}>
      <SearchBox />
      <ResultCount />
    </div>
  );
};

const FreightListContainer = () => {

  const [previewDetail, setPreviewDetail] = useState(false);

  const renderResultTable = () => {
    return (
      <div className={Styles.resultTable}>
        {renderTabs()}
        {renderToolBar()}
        <FreightListItem
          isPartialFilled
          onPress={() => setPreviewDetail(true)} />
        <FreightListItem
          onPress={() => setPreviewDetail(true)} />
        <Pagination
          postsPerPage={4}
          totalPosts={20}
          paginate={() => { }}
          currentPage={1}
          paginatePrevious={() => { }}
          paginateNext={() => { }}
        />
      </div>
    );
  };

  const renderPreviewPoup = () => {
    return (
      <FreightDetailPopup
        show={previewDetail}
        onClose={() => { setPreviewDetail(false) }} />
    )
  };

  return (
    <LayoutDashboard>
      <div className={Styles.freightListContainer}>
        {renderTitle()}
        {renderResultTable()}
        {renderPreviewPoup()}
      </div>
    </LayoutDashboard>
  );
};

export default FreightListContainer;
