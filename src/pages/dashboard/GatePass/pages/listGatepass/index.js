import React, { useState } from 'react';
import { GatePassIcon, ActionGateppas, ActionPay, ActionProcess } from 'components/icon/iconSvg';
import Styles from 'assets/scss/gatePass/gatePass.module.scss';
import ChoosePayment from 'components/modal/choosePaymentGatepass';
import GatepassContainer from 'components/modal/gatepassContainerPopup';
import LayoutDashboard from 'components/dashboard/layout';
import { Invoice } from '../index';
import { useHistory, useLocation } from 'react-router-dom';
import { withTranslation } from 'react-i18next';

function ListGatepass(props) {

  const { history, t } = props;

  const translate = t;
  const [showModalGatepass, setShowModalGatepass] = useState(false);
  const [showModalPayment, setShowModalPayment] = useState(false);
  const [showInvoice, setShowInvoice] = useState({
    isShow: false,
    label: '',
  });
  const [acitveMenu, setActiveMenu] = useState({
    type: 0,
    status: 1,
  });

  const tableHeader = [
    'port',
    'noDo',
    'docType',
    'transactionType',
    'payThruDate',
    'amount',
    'sppbNo',
    'price',
    'action',
  ];

  const action = {
    1: {
      icon: <ActionGateppas />,
      color: '#33CCB2',
    },
    3: {
      icon: <ActionPay />,
      color: '#FE9301',
    },
  };

  const data = [1, 1, 1, 3, 3];

  const onNavigateToRequestGatepass = () => {
    history.replace('/dashboard/e-document/gatePass');
  };

  const onClickStatus = (status) => {
    if (acitveMenu.status !== status) {
      setActiveMenu((prev) => ({
        ...prev,
        status: status,
      }))
    }
  };

  const onClickTypeGatepass = (type) => {
    if (acitveMenu.type !== type) {
      setActiveMenu((prev) => ({
        ...prev,
        type: type,
      }))
    }
  };


  if (showInvoice.isShow) {
    return (
      <LayoutDashboard hideSideMenu  >
        <Invoice
          label={showInvoice.label}
          onBackPress={() => {
            setShowInvoice({
              isShow: false,
              label: ''
            });
          }} />
      </LayoutDashboard>
    )
  }

  return (
    <LayoutDashboard hideSideMenu  >
      <div className={Styles.listGatepass}>
        <div className={Styles.gridHeader}>
          <div className={Styles.column}>
            <text className={Styles.title}>{translate('gatepass.registerRequestGatepass')}</text>
            <text className={Styles.subTitle}>Gatepass</text>
          </div>
          <div
            onClick={onNavigateToRequestGatepass}
            className={Styles.buttonRequestGatepass}>
            <text className={Styles.textButtonRequestGatepass}>Request Gate Pass</text>
            <GatePassIcon />
          </div>
        </div>

        <div className={Styles.container}>
          <div className={`${Styles.gridHeader} ${Styles.separator} ${Styles.paddingHorizontal}`}>
            <div className={Styles.tabsWrapper}>
              {
                [translate('gatepass.inprogress'), translate('gatepass.completed')].map((item, index) => (
                  <text
                    key={index.toString()}
                    onClick={() => onClickStatus(index)}
                    className={`${Styles.tab} ${acitveMenu.status === index && Styles.tabActive}`}>{item}</text>
                ))
              }
            </div>

            <div className={Styles.wrapperSectionType}>
              {
                [translate('gatepass.export'), translate('gatepass.import')].map((item, index) => (
                  <text
                    key={index.toString()}
                    onClick={() => onClickTypeGatepass(index)}
                    className={`${Styles.sectionType} ${acitveMenu.type === index && Styles.sectionTypeActive}`}>{item}</text>
                ))
              }
            </div>
          </div>

          <table className={Styles.table}>
            <tr className={`${Styles.tr}`}>
              {
                tableHeader.map((item, index) => (
                  <td key={index.toString} className="td">
                    <div className={Styles.paddingHorizontal}>{translate(`gatepass.${item}`)}</div>
                  </td>
                ))
              }
            </tr>
            {
              data.map((item, index) => (
                <tr key={index.toString} className={`${Styles.tr} ${Styles.trContent}`}>
                  <td>
                    <div className={Styles.paddingHorizontal}>TJ. Priok - NPCT1</div>
                  </td>
                  <td>
                    <div className={Styles.paddingHorizontal}>DO/16/07/2020</div>
                  </td>
                  <td>
                    <div className={Styles.paddingHorizontal}>BC 2.3</div>
                  </td>
                  <td>
                    <div className={Styles.paddingHorizontal}>Normal</div>
                  </td>
                  <td>
                    <div className={Styles.paddingHorizontal}>04 Jul 2020</div>
                  </td>
                  <td>
                    <div className={Styles.paddingHorizontal}>3/3</div>
                  </td>
                  <td>
                    <div className={Styles.paddingHorizontal}>SPPB 2222</div>
                  </td>
                  <td>
                    <div className={Styles.paddingHorizontal}>Rp20.000</div>
                  </td>
                  <td>
                    <div
                      className={Styles.action}
                      style={{ backgroundColor: action[item].color }}
                      onClick={() => {
                        if (item === 1) {
                          setShowModalGatepass(true);
                        } else if (item === 3) {
                          setShowModalPayment(true);
                        }
                      }}>
                      {action[item].icon}
                    </div>
                  </td>
                </tr>
              ))
            }
          </table>

        </div>

        <ChoosePayment
          show={showModalPayment}
          onClose={() => setShowModalPayment(false)}
          onOpen={(label) => {
            setShowModalPayment(false);
            setShowInvoice({
              isShow: true,
              label: label,
            });
          }} />

        <GatepassContainer
          show={showModalGatepass}
          onClose={() => setShowModalGatepass(false)} />
      </div>

    </LayoutDashboard>
  );
}

export default withTranslation()(ListGatepass);