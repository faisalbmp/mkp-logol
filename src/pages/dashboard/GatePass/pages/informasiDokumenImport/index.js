/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import InputText from 'components/input/inputText';
import InputDate from 'components/input/inputDate';
import SelectBox from 'components/checkBox/select-box';
import GatePassNPWP from 'components/modal/gatePassNPWP';
import { flexContainer, flexItem, wrapperSelectedNPWP, buttonFooter, loadingBlockView } from 'assets/scss/gatePass/gatePass.module.scss';
import { UploadDocument } from 'components/gatepass';
import LoadingButtonVerify from 'components/loading/loadingButtonVerify';
import { uploadDocumentGatepass, verifyGatepass, getCustomCodeDoc } from 'actions';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import { withTranslation } from 'react-i18next';

function InformasiDokumenImport(props) {

  const { onSubmit, t } = props;

  const translate = t;

  // const [selectTerminal, setSelectTerminal] = useState(false);
  const [selectNPWP, setSelectNPWP] = useState(false);
  const [selectedNPWP, setSelectedNPWP] = useState(false);
  const [formData, setFormData] = useState(props?.formData ?? {});
  // const [showAlert, setShowAlert] = useState(false);
  const dispatch = useDispatch();
  const { gatepassReducers } = useSelector(state => state);
  const { verifyGatepassResponse, isLoading, getCustomCodeDocResponse } = gatepassReducers;
  const [showCheckbox, setShowCheckbox] = useState({
    terminal: false,
    dokumen: false,
    transaksi: false,
  });
  const [dataCombo, setDataCombo] = useState({
    terminal: [
      {
        key: '1',
        portName: 'Tj. Priok - KOJA',
        terminal: 'PORT-0002',
      },
      {
        key: '2',
        portName: 'NPCT1',
        terminal: 'PORT-0003',
      }
    ],
    dokumen: [],
  });

  const tipeTransaksi = [
    {
      key: '1',
      portName: 'Normal',
    }
  ];

  useEffect(() => {
    dispatch(getCustomCodeDoc());
  }, []);

  useEffect(() => {
    if (verifyGatepassResponse?.value?.success) {
      onSubmit(formData);
    }

    if (getCustomCodeDocResponse?.value) {
      const data = getCustomCodeDocResponse?.value ?? [];
      const tempData = data.map((item) => ({
        key: item.customsDocumentID,
        portName: item.customsDocumentNBSID,
        terminal: item.customsDocumentNBSID,
      }));
      setDataCombo((prev) => ({
        ...prev,
        dokumen: tempData,
      }));
    }

  }, [gatepassReducers]);

  const onSelectedDocument = (params) => {
    if (params?.file) {
      const data = new FormData();
      data.append('file', params.file);
      data.append('fileType', "document");
      dispatch(uploadDocumentGatepass(data));
    }
    setFormData((prev) => ({
      ...prev,
      [`${params.field}`]: params.file,
    }));
  };

  const onChangeInput = (e) => {
    setFormData((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }))
  };

  const onChangeInputDate = (date, state) => {
    setFormData((pre) => ({
      ...pre,
      [state]: date,
    }));
  };

  const navigateTo = () => {
    dispatch(verifyGatepass(formData));
  };

  return (
    <div style={{ paddingBottom: 48 }}>
      {
        isLoading && (<div className={loadingBlockView} />)
      }
      <div className='form-inner-container'>
        <SelectBox
          label={translate('gatepass.departureTerminal')}
          openList={showCheckbox.terminal}
          selectList={dataCombo.terminal}
          placeholder={
            dataCombo.terminal.find((item) => item.terminal === formData?.terminal).portName
          }
          headerStyle={{ width: 216, height: 42, marginTop: 10, }}
          openSelectList={() => {
            setShowCheckbox((prev) => ({
              ...prev,
              terminal: !showCheckbox.terminal,
            }));
          }}
          selectedList={(item, index) => {
            setShowCheckbox((prev) => ({
              ...prev,
              terminal: false,
            }))
            setFormData((prevState) => {
              return {
                ...prevState,
                terminal: item.terminal,
              }
            })
          }} />

        <div className={flexContainer}>
          <div className={flexItem}>
            <InputDate
              selectedDate={formData?.payDate ? moment(formData.payDate).format('DD-MMM-YYYY') : null}
              pickerType="custom"
              label={translate('gatepass.payThruDate')}
              placeholderInput={translate('gatepass.chooseDate')}
              inputStyle={{ fontSize: 14 }}
              headerStyle={{ width: 200, height: 42 }}
              onChange={(date) => {
                setFormData((pre) => ({
                  ...pre,
                  payDate: date,
                }));
              }}
            />
          </div>

          <div className={flexItem}>
            <SelectBox
              label={translate('gatepass.typeDocument')}
              placeholder={formData?.dokumen ?? translate('gatepass.choose', { type: translate('gatepass.typeDocument') })}
              openList={showCheckbox.dokumen}
              selectList={dataCombo.dokumen}
              headerStyle={{ width: 216, height: 42, marginTop: 7 }}
              openSelectList={() => {
                setShowCheckbox((prev) => ({
                  ...prev,
                  dokumen: !showCheckbox.dokumen,
                }));
              }}
              selectedList={(item, index) => {
                // setSelectTerminal(false)
                setShowCheckbox((prev) => ({
                  ...prev,
                  dokumen: false,
                }));
                setFormData((prev) => ({
                  ...prev,
                  dokumen: item.portName,
                }));
              }} />
          </div>

          <div className={flexItem}>
            <SelectBox
              value={
                tipeTransaksi[tipeTransaksi.length - 1].portName
              }
              label={translate('gatepass.typeTransaction')}
              placeholder={formData?.transaksi ?? translate('gatepass.choose', { type: translate('gatepass.typeTransaction') })}
              openList={showCheckbox.transaksi}
              selectList={tipeTransaksi}
              headerStyle={{ width: 216, height: 42, marginTop: 7 }}
              openSelectList={() => {
                setShowCheckbox((prev) => ({
                  ...prev,
                  transaksi: true,
                }))
              }}
              selectedList={(item, index) => {
                setShowCheckbox((prev) => ({
                  ...prev,
                  transaksi: false,
                }));
                setFormData((prev) => ({
                  ...prev,
                  transaksi: item.portName,
                }));
              }} />
          </div>

        </div>
        <div style={{ paddingTop: 30 }}>
          <h4 style={{ fontWeight: "bold" }}>{translate('gatepass.documentInformation')}</h4>
        </div>

        {
          selectedNPWP ? (
            <div className={wrapperSelectedNPWP}>
              <div className={flexItem}>
                <InputText
                  editable={false}
                  textInputTitle={translate('gatepass.onbehalfName')}
                  placeholder={translate('gatepass.onbehalfName')}
                  value="Rachman"
                  styleInput={{ fontSize: 14 }}
                  inputTextStyle={{ width: 251, height: 42 }} />
              </div>

              <div className={flexItem}>
                <InputText
                  editable={false}
                  textInputTitle={translate('gatepass.onbehalfNpwp')}
                  placeholder={translate('gatepass.onbehalfNpwp')}
                  value="90.000.000.0-000.000"
                  styleInput={{ fontSize: 14 }}
                  inputTextStyle={{ width: 251, height: 42 }} />
              </div>

              <button
                className="disabled-button"
                type="button"
                onClick={() => { }}
                style={{ border: '0px', width: 121, height: 48, marginTop: 24 }}>
                <div className="disabled-button-text" >{translate('gatepass.changeNpwp')}</div>
              </button>

            </div>
          ) : formData.terminal === "PORT-0003" ? (
            <div
              onClick={() => {
                setSelectNPWP(true);
                setSelectedNPWP(true);
              }}>
              <InputText
                disabled
                editable={false}
                value={formData?.npwp}
                textInputTitle='NPWP'
                placeholder={translate('gatepass.chooseNpwp')}
                styleInput={{ fontSize: 14 }}
                inputTextStyle={{ width: 251, height: 42, fontSize: 14 }} />
            </div>
          ) : null
        }

        <div className={flexContainer}>
          <div className={flexItem}>
            <InputText
              name="blNo"
              value={formData?.blNo ?? ''}
              textInputTitle='No. Bill of Lading'
              placeholder="No. Bill of Lading"
              styleInput={{ fontSize: 14 }}
              inputTextStyle={{ width: 251, height: 42 }}
              onChange={onChangeInput} />
          </div>
          <div className={flexItem}>
            <UploadDocument
              title={translate('gatepass.bolFile')}
              headerStyle={{ width: 200, height: 42, marginTop: 8 }}
              docName={formData?.bolDoc?.name ?? null}
              selectedFile={(file) => {
                onSelectedDocument({
                  file,
                  field: 'bolDoc',
                });
              }} />
          </div>
        </div>
        <div className={flexContainer}>
          <div className={flexItem}>
            <InputText
              name="doNo"
              value={formData?.doNo ?? ''}
              onChange={onChangeInput}
              textInputTitle={translate('gatepass.doNo')}
              inputTextStyle={{ width: 251, height: 42 }}
              placeholder={translate('gatepass.doNo')}
              styleInput={{ fontSize: 14 }}
              type="number" />
          </div>
          <div className={flexItem}>
            <UploadDocument
              title={translate('gatepass.doFile')}
              headerStyle={{ width: 200, height: 42, marginTop: 8 }}
              docName={formData?.doDoc?.name ?? null}
              selectedFile={(file) => {
                onSelectedDocument({
                  file,
                  field: 'doDoc',
                });
              }} />
          </div>
          {
            formData.terminal === 'PORT-0003' && (
              <div className={flexItem}>
                <InputDate
                  selectedDate={formData?.doDate ? moment(formData.doDate).format('DD-MMM-YYYY') : null}
                  onChange={(date) => onChangeInputDate(date, 'doDate')}
                  pickerType="custom"
                  label={translate('gatepass.doExpiry')}
                  placeholderInput={translate('gatepass.chooseDate')}
                  headerStyle={{ width: 200, height: 42 }}
                  inputStyle={{ fontSize: 14 }}
                />
              </div>
            )
          }
        </div>

        {
          formData.terminal === 'PORT-0003' && (
            <div className={flexContainer}>
              <div className={flexItem}>
                <InputText
                  name="pib"
                  value={formData?.pib ?? ''}
                  onChange={onChangeInput}
                  textInputTitle={translate('gatepass.noPib')}
                  inputTextStyle={{ width: 251, height: 42 }}
                  placeholder={translate('gatepass.noPib')}
                  styleInput={{ fontSize: 14 }}
                  type="number" />
              </div>
              <div className={flexItem}>
                <UploadDocument
                  title={translate('gatepass.filePib')}
                  headerStyle={{ width: 200, height: 42, marginTop: 8 }}
                  docName={formData?.pibDoc?.name ?? null}
                  selectedFile={(file) => {
                    onSelectedDocument({
                      file,
                      field: 'pibDoc',
                    });
                  }} />
              </div>
              <div className={flexItem}>
                <InputDate
                  selectedDate={formData?.pibDate ? moment(formData.pibDate).format('DD-MMM-YYYY') : null}
                  onChange={(date) => onChangeInputDate(date, 'pibDate')}
                  pickerType="custom"
                  label={translate('gatepass.datePib')}
                  placeholderInput={translate('gatepass.chooseDate')}
                  headerStyle={{ width: 200, height: 42 }}
                  inputStyle={{ fontSize: 14 }}
                />
              </div>
            </div>
          )
        }

        <div className={flexContainer}>
          <div className={flexItem}>
            <InputText
              name="sppb"
              value={formData?.sppb ?? ''}
              onChange={onChangeInput}
              textInputTitle='No. SPPB'
              inputTextStyle={{ width: 251, height: 42 }}
              placeholder="No. SPPB"
              styleInput={{ fontSize: 14 }}
              type="number" />
          </div>
          <div className={flexItem}>
            <UploadDocument
              title={translate('gatepass.sppbFile')}
              headerStyle={{ width: 200, height: 42, marginTop: 8 }}
              docName={formData?.sppbDoc?.name ?? null}
              selectedFile={(file) => {
                onSelectedDocument({
                  file,
                  field: 'sppbDoc',
                });
              }} />
          </div>
          {
            formData.terminal === 'PORT-0003' && (
              <div className={flexItem}>
                <InputDate
                  selectedDate={formData?.sppbDate ? moment(formData.sppbDate).format('DD-MMM-YYYY') : null}
                  onChange={(date) => onChangeInputDate(date, 'sppbDate')}
                  pickerType="custom"
                  label={translate('gatepass.sppbDate')}
                  placeholderInput={translate('gatepass.chooseDate')}
                  headerStyle={{ width: 200, height: 42 }}
                  inputStyle={{ fontSize: 14 }}
                />
              </div>
            )
          }
        </div>

      </div>
      <div className={buttonFooter}>
        <button
          disabled={isLoading}
          className="next-button"
          type="button"
          onClick={navigateTo} style={{ border: '0px', marginTop: 39, height: 48, }}>
          <div className="next-button-text" >
            {isLoading ? <LoadingButtonVerify /> : translate('gatepass.verification')}
          </div>
        </button>
      </div>
      <GatePassNPWP
        show={selectNPWP}
        onClose={() => setSelectNPWP(false)}>
      </GatePassNPWP>
    </div>
  );
}

export default withTranslation()(InformasiDokumenImport);
