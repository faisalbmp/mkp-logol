import React, { useState } from 'react';

import LayoutDashboard from 'components/dashboard/layout';
import { header } from 'assets/scss/gatePass/gatePass.module.scss';
import { InformasiDokumen, PilihKontainer, Invoice, InformasiDokumenDokumen, ListGatepass } from '../index';
import { useDispatch } from 'react-redux';
import { resetVerifyGatepass } from 'actions';
import { withTranslation } from 'react-i18next';

function GetePass(props) {

	const { t } = props;
	const translate = t;

	const dispatch = useDispatch();
	const [pages, setPages] = useState('informasiDokumen');
	const [menu, setMenu] = useState('Ekspor');
	const [formData, setFormData] = useState(
		{
			paymentType: "CASH",
			requestID: "san-test-20201117-npct1-export-4",
			npwp: "017206863426001",
			terminal: "PORT-0003",
			payDate: "2020-11-07",
			docType: "1",
			transType: "0",
			doNo: "EGLV020000155262",
			sppb: "234280/KPU.01/2020",
			blNo: "EGLV020000155262",
			company: "EXP_NPCT113NAGASE IMPOR-EKSPOR INDONESIA",
			address: "EXP_NPCT13WISMA KEIAI LT.12 JL. JEND. SUDIRMAN KAV.3 KARET TENGSIN KEL.KARET TENGSIN, KEC.TANAH ABANG, JAKARTA PUSAT, DKI JAKARTA, 0",
			email: "dev@logol.co.in",
			mobile: "0003121-57900391",
			contactName: "EXP_NPCTARI ARIYANTO",
			sppbDate: "2020-11-20",
			doExpDate: "2020-11-30",
			cargo_npwp_name: "017206863426001",
			npwp_cargo_owner: "npwp_cargo_owner",
			totalQuantity: "1",
			orderType: "EXPORT",
			peb: "670949",
			pebDate: "2020-10-30",
			npe: "670998",
			npeDate: "2020-10-30",
			bookingNo: "205948523",
			requestContainer: [],
			sppbDoc: {},
			pebDoc: {},
			doDoc: {},
		}
	);

	const [formDataImport, setFormDataImport] = useState(
		{
			requestID: "2bef0903-afde-4874-bc72-59ac1ea66cb5",
			npwp: "011057858431000",
			terminal: "PORT-0003",
			payDate: "2020-10-15",
			docType: "1",
			transType: "1",
			doNo: "EGLV020000176570",
			sppb: "236020/KPU.01/2020",
			blNo: "EGLV020000176570",
			company: "PT Dimas Customer",
			address: "jalan cakung",
			email: "dimas.setiawan@logol.co.id",
			mobile: "0899123123",
			contactName: "Dimas",
			doExpDate: "2020-12-30",
			pib: "235652",
			pibDate: "2020-05-14",
			sppbDate: "2020-05-14",
			npwp_cargo_owner: "011057858431000",
			cargo_npwp_name: "011057858431000",
			orderType: "IMPORT",
			requestContainer: [],
			sppbDoc: {},
			pebDoc: {},
			doDoc: {},
		}
	);

	const gatePassMenu = (changedMenu) => {
		if (changedMenu !== menu) {
			setMenu(changedMenu);
			setPages('informasiDokumen');
			dispatch(resetVerifyGatepass());
		}
	}

	const renderChild = () => {
		if (pages === 'listGatepass') {
			return <ListGatepass />
		} else if (pages === 'invoice') {
			return (
				<Invoice
					formData={menu === 'Ekspor' ? formData : formDataImport}
					onBackPress={() => setPages('pilihKontainer')} />
			)
		} else {
			return (
				<div className='wrapper'>
					<div className={header}>
						<div className='title'>{translate('gatepass.newE-gatepass')}</div>
						<p>{menu}</p>
					</div>
					<div className='wrapper-body'>

						{
							pages === 'informasiDokumen' && menu === 'Ekspor' && (
								<InformasiDokumen
									formData={formData}
									onSubmit={(payload) => {
										setFormData((prev) => ({
											...prev,
											...payload,
										}))
										setPages('pilihKontainer');
									}} />
							)
						}

						{
							pages === 'informasiDokumen' && menu === 'Import' && (
								<InformasiDokumenDokumen
									formData={formDataImport}
									onSubmit={(payload) => {
										setFormDataImport((prev) => ({
											...prev,
											...payload,
										}))
										setPages('pilihKontainer');
									}} />
							)
						}

						{
							pages === 'pilihKontainer' && (
								<PilihKontainer
									formData={menu === 'Ekspor' ? formData : formDataImport}
									onSubmit={(payload) => {
										setFormData((prev) => ({
											...prev,
											...payload,
										}));
										setPages('listGatepass')
									}}
									onBackPress={() => setPages('informasiDokumen')} />
							)
						}
					</div>
				</div>
			);
		}
	};

	return (
		<LayoutDashboard
			gatePassMenu={gatePassMenu}
			hideSideMenu={pages === 'listGatepass' || pages === 'invoice'} >
			{renderChild()}
		</LayoutDashboard>
	);
};

export default withTranslation()(GetePass);
