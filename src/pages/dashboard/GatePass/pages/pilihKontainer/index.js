/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import { IconBack } from 'components/icon/iconSvg';
import { wrapperActionBack, wrapperPilihKontainer, headerKontainer, textCliclable, table, buttonFooter, loadingBlockView, statusContainerIsStack, statusContainerIsStacked } from 'assets/scss/gatePass/gatePass.module.scss';
import { CheckBox } from 'components/gatepass/index';
import { useDispatch, useSelector } from 'react-redux';
import { requestGatepass, resetVerifyGatepass } from 'actions';
import { v4 as uuidv4 } from 'uuid';
import { msContainerType } from 'utils/list';
import SuccessPopup from 'components/modal/successPopup';
import { useHistory, useLocation } from 'react-router-dom';
import { withTranslation } from 'react-i18next';

function PilihKontianer(props) {

  const { onSubmit, onBackPress, t } = props;

  const translate = t;

  const
    [data, setData] = useState([]),
    [showModalSuccess, setShowModalSuccess] = useState(false),
    { gatepassReducers } = useSelector(state => state),
    { isLoading, verifyGatepassResponse, requestGatepassResponse } = gatepassReducers,
    [formData, setFormData] = useState(props?.formData ?? {}),
    dispatch = useDispatch(),
    history = useHistory();

  useEffect(() => {
    if (verifyGatepassResponse?.value?.success) {
      setData(verifyGatepassResponse?.value?.data?.containers ?? []);
    }
  }, [gatepassReducers]);

  useEffect(() => {
    if (showModalSuccess) {
      setTimeout(
        () => {
          dispatch(resetVerifyGatepass());
          setShowModalSuccess(false);
          history.push('/dashboard/list/e-document/gatePass');
        },
        3000
      );
    }
  }, [showModalSuccess])

  const selectedData = data.filter((item) => item?.isSelected);

  const onSelectAll = () => {
    const temp = [...data];
    const filter = temp.map((item) => {
      item['isSelected'] = true;
      return item;
    });
    setData(filter);
    setFormData((prevState) => ({
      ...prevState,
      requestContainer: filter,
    }));
  };

  const onSubmitContainer = () => {
    setShowModalSuccess(true);
    // const payload = formData;
    // payload['requestContainer'] = selectedData;
    // const requestID = uuidv4();
    // payload['requestID'] = requestID;
    // setFormData(payload);
    // onSubmit(payload);
  };

  const onNavigateBack = () => {
    dispatch(resetVerifyGatepass());
    onBackPress();
  };

  const StatusContainer = ({ isStacked }) => {
    let status = isStacked ? 'STACKING' : 'ON VESSEL';
    return (
      <span className={`${statusContainerIsStack} ${isStacked && statusContainerIsStacked}`}>
        {status}
      </span>
    );
  };

  return (
    <div style={{ paddingBottom: 48, }}>
      {
        isLoading && (<div className={loadingBlockView} />)
      }
      <div className='form-inner-container'>
        <div className={wrapperActionBack}
          onClick={onNavigateBack}>
          <IconBack />
          <text>{translate('gatepass.chooseContainer')}</text>
        </div>
        <div className={wrapperPilihKontainer}>
          <span>
            <text>{data.length}</text> {translate('gatepass.container')} : <text>2</text> x 20' GP | <text>1</text> x 40' GP
        </span>

          <table className={table}>
            <thead>
              <tr>
                <th>No</th>
                <th>{translate('gatepass.containerNumber')}</th>
                <th>{translate('gatepass.typeContainer')}</th>
                <th>{translate('gatepass.status')}</th>
                <th
                  className={textCliclable}
                  onClick={onSelectAll}>{translate('gatepass.chooseAll')}</th>
              </tr>
            </thead>
            <tbody>
              {
                data.map((item, index) => {
                  const msContainer = msContainerType().find((container) => container.ContainerTypeID === item.logolContainerTypeID);
                  const reqContainer = formData.requestContainer.find((req) => req.container_no === item.container_no);

                  const onClickChecked = () => {
                    if (item.isStacked) {
                      const temp = [...data];
                      temp[index] = {
                        ...item,
                        isSelected: !item?.isSelected ?? false,
                      };

                      setData(temp);
                      setFormData((prevState) => ({
                        ...prevState,
                        requestContainer: temp.filter((item) => item?.isSelected),
                      }));
                    }
                  };
                  return (
                    <tr key={index.toString()}>
                      <td>{index + 1}</td>
                      <td><span>{item?.container_no ?? ''}</span></td>
                      <td>{msContainer?.Size ?? ''}' {msContainer?.ExtContainerTypeID ?? ''}</td>
                      <td><StatusContainer isStacked={item.isStacked} /></td>
                      <td style={{ textAlign: 'center' }}>
                        <div onClick={onClickChecked}>
                          <CheckBox
                            isStacked={item.isStacked}
                            checked={reqContainer?.isSelected} />
                        </div>
                      </td>
                    </tr>
                  )
                })
              }
            </tbody>
          </table>

        </div>
      </div>
      <div className={buttonFooter}>
        <button
          disabled={formData?.requestContainer?.length < 1}
          className={`next-button  ${formData?.requestContainer?.length < 1 && 'next-button-disabled'}`}
          type="button"
          onClick={onSubmitContainer}
          style={{ border: '0px', marginTop: 39, height: 48, float: 'none' }}>
          <div className="next-button-text" >{formData?.requestContainer?.length > 0 ? translate('gatepass.processContainer', { amount: formData?.requestContainer?.length ?? 0 }) : translate('gatepass.chooseContainer')}</div>
        </button>
      </div>

      <SuccessPopup
        show={showModalSuccess}
        onClose={() => { }} />
    </div>
  );
}

export default withTranslation()(PilihKontianer);