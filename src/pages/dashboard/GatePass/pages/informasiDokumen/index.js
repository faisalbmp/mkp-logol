/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import InputText from 'components/input/inputText';
import InputDate from 'components/input/inputDate';
import SelectBox from 'components/checkBox/select-box';
import GatePassNPWP from 'components/modal/gatePassNPWP';
import { flexContainer, flexItem, wrapperSelectedNPWP, buttonFooter, loadingBlockView } from 'assets/scss/gatePass/gatePass.module.scss';
import { UploadDocument } from 'components/gatepass';
import { AlertBanner } from 'components/alert';
import LoadingButtonVerify from 'components/loading/loadingButtonVerify';
import { uploadDocumentGatepass, verifyGatepass } from 'actions';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import { gatepassTerminal } from 'utils/list';
import { withTranslation } from 'react-i18next';

function InformasiDokumen(props) {

  const { onSubmit, t } = props;

  const translate = t;

  const dispatch = useDispatch();

  const
    [selectTerminal, setSelectTerminal] = useState(false),
    [selectNPWP, setSelectNPWP] = useState(false),
    [selectedNPWP, setSelectedNPWP] = useState(false),
    [formData, setFormData] = useState(props?.formData ?? {}),
    [showAlert, setShowAlert] = useState(false),
    { gatepassReducers } = useSelector(state => state),
    { isLoading, verifyGatepassResponse } = gatepassReducers,
    terminal = gatepassTerminal();

  useEffect(() => {
    if (showAlert) {
      setTimeout(
        () => setShowAlert(false),
        3000
      );
    }
  }, [showAlert])

  useEffect(() => {
    if (verifyGatepassResponse?.value) {
      if (verifyGatepassResponse.value.success) {
        onSubmit(formData);
      } else {
        if (!showAlert) {
          setShowAlert(true);
        }
      }
    }
  }, [verifyGatepassResponse?.value?.success]);

  const navigateTo = () => {
    dispatch(verifyGatepass(formData));
  };

  const onSelectedDocument = (params) => {
    if (params?.file) {
      const data = new FormData();
      data.append('file', params.file);
      data.append('fileType', "document");
      dispatch(uploadDocumentGatepass(data));
    }
    setFormData((prev) => ({
      ...prev,
      [`${params.field}`]: params.file,
    }));
  };

  const onChangeInput = (e) => {
    setFormData((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));
  };

  return (
    <div style={{ paddingBottom: 48 }}>
      {
        isLoading && (<div className={loadingBlockView} />)
      }
      <div className='form-inner-container'>
        <SelectBox
          label={translate('gatepass.departureTerminal')}
          placeholder={terminal.find((item) => item.terminal === formData?.terminal).portName}
          openList={selectTerminal}
          selectList={terminal}
          value={
            terminal.find((item) => item.terminal === formData?.terminal).portName
          }
          headerStyle={{ width: 216, height: 42, marginTop: 7}}
          openSelectList={() => setSelectTerminal((prevState) => {
            return !prevState;
          })}
          selectedList={(item, index) => {
            setSelectTerminal(false)
            setFormData((prevState) => {
              return {
                ...prevState,
                terminal: item.terminal,
              }
            })
          }} />
        <div style={{ paddingTop: 30 }}>
          <h4 style={{ fontWeight: "bold" }}>{translate('gatepass.documentInformation')}</h4>
        </div>

        {
          showAlert && <AlertBanner />
        }

        {
          selectedNPWP ? (
            <div className={wrapperSelectedNPWP}>
              <div className={flexItem}>
                <InputText
                  editable={false}
                  textInputTitle={translate('gatepass.onbehalfName')}
                  placeholder={translate('gatepass.onbehalfName')}
                  value="Rachman"
                  styleInput={{ fontSize: 14 }}
                  inputTextStyle={{ width: 251, height: 42 }} />
              </div>

              <div className={flexItem}>
                <InputText
                  editable={false}
                  textInputTitle={translate('gatepass.onbehalfNpwp')}
                  placeholder={translate('gatepass.onbehalfNpwp')}
                  value="90.000.000.0-000.000"
                  styleInput={{ fontSize: 14 }}
                  inputTextStyle={{ width: 251, height: 42 }} />
              </div>

              <button
                className="disabled-button"
                type="button"
                onClick={() => { }}
                style={{ border: '0px', width: 121, height: 48, marginTop: 24 }}>
                <div className="disabled-button-text" >{translate('gatepass.changeNpwp')}</div>
              </button>

            </div>
          ) : formData.terminal === "PORT-0003" ? (
            <div
              onClick={() => {
                setSelectNPWP(true);
                setSelectedNPWP(true);
              }}>
              <InputText
                disabled
                editable={false}
                value={formData?.npwp}
                textInputTitle='NPWP'
                placeholder={translate('gatepass.chooseNpwp')}
                styleInput={{ fontSize: 14 }}
                inputTextStyle={{ width: 251, height: 42, fontSize: 14 }} />
            </div>
          ) : null
        }

        <div className={flexContainer}>
          <div className={flexItem}>
            <InputText
              error
              name="doNo"
              value={formData?.doNo ?? ''}
              textInputTitle={translate('gatepass.noDoPlaceholder')}
              placeholder={translate('gatepass.noDoPlaceholder')}
              styleInput={{ fontSize: 14 }}
              inputTextStyle={{ width: 251, height: 42 }}
              onChange={onChangeInput} />
          </div>
          <div className={flexItem}>
            <InputDate
              selectedDate={formData?.doExpDate ? moment(formData.doExpDate).format('DD-MMM-YYYY') : null}
              pickerType="custom"
              label={translate('gatepass.doDate')}
              placeholderInput={translate('gatepass.chooseDate')}
              inputStyle={{ fontSize: 14 }}
              headerStyle={{ width: 200, height: 42 }}
              error
              onChange={(date) => {
                setFormData((pre) => ({
                  ...pre,
                  doExpDate: date,
                }));
              }}
            />
          </div>
          <div className={flexItem}>
            <UploadDocument
              docName={formData?.doDoc?.name ?? null}
              title={translate('gatepass.doFile')}
              headerStyle={{ width: 200, height: 42, marginTop: 8 }}
              selectedFile={(file) => {
                onSelectedDocument({
                  file,
                  field: 'doDoc',
                });
              }}
              error />
          </div>
        </div>
        <div className={flexContainer}>
          <div className={flexItem}>
            <InputText
              name="peb"
              value={formData?.peb ?? ''}
              textInputTitle={translate('gatepass.pebNo')}
              inputTextStyle={{ width: 251, height: 42 }}
              placeholder={translate('gatepass.pebNo')}
              styleInput={{ fontSize: 14 }}
              type="number"
              onChange={onChangeInput} />
          </div>
          <div className={flexItem}>
            <InputDate
              selectedDate={formData?.pebDate ? moment(formData.pebDate).format('DD-MMM-YYYY') : null}
              pickerType="custom"
              label={translate('gatepass.pebDate')}
              placeholderInput={translate('gatepass.chooseDate')}
              headerStyle={{ width: 200, height: 42 }}
              inputStyle={{ fontSize: 14 }}
              onChange={(date) => {
                setFormData((pre) => ({
                  ...pre,
                  pebDate: date,
                }));
              }}
            />
          </div>
          <div className={flexItem}>
            <UploadDocument
              docName={formData?.pebDoc?.name ?? null}
              title={translate('gatepass.pebFile')}
              headerStyle={{ width: 200, height: 42, marginTop: 8 }}
              selectedFile={(file) => {
                onSelectedDocument({
                  file,
                  field: 'pebDoc',
                });
              }} />
          </div>
        </div>
        <div className={flexContainer}>
          <div className={flexItem}>
            <InputText
              name="npe"
              value={formData?.npe ?? ''}
              onChange={onChangeInput}
              textInputTitle={translate('gatepass.npeNo')}
              inputTextStyle={{ width: 251, height: 42 }}
              placeholder={translate('gatepass.npeNo')}
              styleInput={{ fontSize: 14 }}
              type="number" />
          </div>
          <div className={flexItem}>
            <InputDate
              selectedDate={formData?.npeDate ? moment(formData.npeDate).format('DD-MMM-YYYY') : null}
              pickerType="custom"
              label={translate('gatepass.npeDate')}
              placeholderInput={translate('gatepass.chooseDate')}
              headerStyle={{ width: 200, height: 42 }}
              inputStyle={{ fontSize: 14 }}
              onChange={(date) => {
                setFormData((pre) => ({
                  ...pre,
                  npeDate: date,
                }));
              }}
            />
          </div>
          <div className={flexItem}>
            <UploadDocument
              docName={formData?.npeDoc?.name ?? null}
              title={translate('gatepass.npeFile')}
              headerStyle={{ width: 200, height: 42, marginTop: 8 }}
              selectedFile={(file) => {
                onSelectedDocument({
                  file,
                  field: 'npeDoc',
                });
              }} />
          </div>
        </div>

        {
          formData.terminal === "PORT-0002" && (
            <div>
              <div style={{ paddingTop: 30 }}>
                <h4 style={{ fontWeight: "bold" }}>Informasi Kargo</h4>
              </div>

              <div className={flexContainer}>
                <div className={flexItem}>
                  <SelectBox
                    label={translate('gatepass.pod')}
                    placeholder={translate('gatepass.choosePort')}
                    openList={false}
                    selectList={terminal}
                    headerStyle={{ width: 216, height: 42, marginTop: 7 }}
                    openSelectList={() => setSelectTerminal((prevState) => {
                      return !prevState;
                    })}
                    selectedList={(item, index) => {
                      setSelectTerminal(false)
                      setFormData((prevState) => {
                        return {
                          ...prevState,
                          terminalKeberangkatan: item,
                        }
                      })
                    }} />
                </div>
                <div className={flexItem}>
                  <SelectBox
                    label={translate('gatepass.pfd')}
                    placeholder={translate('gatepass.choosePort')}
                    openList={false}
                    selectList={terminal}
                    headerStyle={{ width: 216, height: 42, marginTop: 7 }}
                    openSelectList={() => setSelectTerminal((prevState) => {
                      return !prevState;
                    })}
                    selectedList={(item, index) => {
                      setSelectTerminal(false)
                      setFormData((prevState) => {
                        return {
                          ...prevState,
                          terminalKeberangkatan: item,
                        }
                      })
                    }} />
                </div>
                <div className={flexItem}>
                  <InputDate
                    pickerType="custom"
                    label={translate('gatepass.date')}
                    placeholderInput={translate('gatepass.chooseDate')}
                    inputStyle={{ fontSize: 14 }}
                    headerStyle={{ width: 200, height: 42 }}
                  />
                </div>
              </div>
              <div className={flexContainer}>
                <div className={flexItem}>
                  <SelectBox
                    label={translate('gatepass.shippingOwner')}
                    placeholder={translate('gatepass.shippingOwner')}
                    openList={false}
                    selectList={terminal}
                    headerStyle={{ width: 216, height: 42, marginTop: 7 }}
                    openSelectList={() => setSelectTerminal((prevState) => {
                      return !prevState;
                    })}
                    selectedList={(item, index) => {
                      setSelectTerminal(false)
                      setFormData((prevState) => {
                        return {
                          ...prevState,
                          terminalKeberangkatan: item,
                        }
                      })
                    }} />
                </div>
                <div className={flexItem}>
                  <InputText
                    textInputTitle={translate('gatepass.vesselName')}
                    placeholder={translate('gatepass.vesselName')}
                    value=""
                    styleInput={{ fontSize: 14 }}
                    inputTextStyle={{ width: 216, height: 42 }} />
                </div>
                <div className={flexItem}>
                  <InputText
                    textInputTitle={translate('gatepass.voyageNumber')}
                    placeholder={translate('gatepass.voyageNumber')}
                    value=""
                    styleInput={{ fontSize: 14 }}
                    inputTextStyle={{ width: 216, height: 42 }} />
                </div>
              </div>
              <div className={flexContainer}>
                <div className={flexItem}>
                  <InputText
                    isWeight={false}
                    textInputTitle={translate('gatepass.weight')}
                    placeholder={translate('gatepass.weight')}
                    value=""
                    styleInput={{ fontSize: 14 }}
                    inputTextStyle={{ width: 216, height: 42 }}
                    isWeightStyle={{ height: 41, }} />
                </div>
              </div>
            </div>
          )
        }

      </div>
      <div className={buttonFooter}>
        <button
          disabled={isLoading}
          className="next-button"
          type="button"
          onClick={navigateTo}
          style={{ border: '0px', marginTop: 39, height: 48, }}>
          <div className="next-button-text" >
            {isLoading ? <LoadingButtonVerify /> : translate('gatepass.verification')}
          </div>
        </button>
      </div>
      <GatePassNPWP
        show={selectNPWP}
        onClose={() => setSelectNPWP(false)}>
      </GatePassNPWP>
    </div>
  );
}

export default withTranslation()(InformasiDokumen);
