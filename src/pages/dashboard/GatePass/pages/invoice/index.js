import React from 'react';
import { IconBack } from 'components/icon/iconSvg';
import {
  wrapperActionBack,
  headerKontainer,
  wrapperPilihKontainer,
  buttonOutline,
  buttonFooter,
  table,
  textPrice,
  textLeft,
  loadingBlockView,
  header,
  headerInvoice,
  labelGantiMetodePembayaran,
  p,
} from 'assets/scss/gatePass/gatePass.module.scss';
// import LoadingButtonVerify from 'components/loading/loadingButtonVerify';
import { requestGatepass } from 'actions';
import { useDispatch, useSelector } from 'react-redux';
import { formatRupiah } from 'utils/formatNumber';

function Invoice(props) {

  const { onBackPress, formData, label = "Pay Later" } = props;
  const { gatepassReducers } = useSelector(state => state);
  const { isLoading } = gatepassReducers;

  const dispatch = useDispatch();


  const onSubmitContainer = (payment) => {
    const payload = formData;
    payload['paymentType'] = payment;

    const { doDoc, pebDoc, npeDoc, sppDoc, ...sendData } = payload;

    dispatch(requestGatepass(sendData));
  };

  return (
    <div className='wrapper'>
      <div className={`${header} ${headerInvoice}`}>
        <div>
          <div className='title'>{label}</div>
          <p className={p}>Gate Pass</p>
        </div>
        <p className={labelGantiMetodePembayaran}>Ganti Metode Pembayaran</p>
      </div>
      <div className='wrapper-body'>
        <div style={{ paddingBottom: 48 }}>
          {
            isLoading && (<div className={loadingBlockView} />)
          }
          <div className='form-inner-container'>
            <div
              className={wrapperActionBack}
              onClick={onBackPress}>
              <IconBack />
              <text>Proforma Invoice</text>
            </div>

            <table className={table}>
              <thead>
                <tr>
                  <th>No</th>
                  <th>Item</th>
                  <th>Jumlah</th>
                  <th>Harga</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td className={textLeft}><span>NPCT1  Reimbursement - <a href="http://localhost:8777/dashboard/e-document/gatePass">PF.20201</a></span></td>
                  <td></td>
                  <td className={textPrice}>IDR 5.000.000</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td className={textLeft}><span>E-gatepass services fee</span></td>
                  <td> IDR 20.000 x {formData?.requestContainer.length}</td>
                  <td className={textPrice}>IDR {formatRupiah(`${20000 * formData?.requestContainer.length}`)}</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td className={textLeft}>VAT 10%</td>
                  <td></td>
                  <td className={textPrice}>IDR 4.000</td>
                </tr>
              </tbody>
            </table>

            <div className={wrapperPilihKontainer}>
              <div className={headerKontainer}>
                <text style={{ fontWeight: 'bold' }}>Total</text>
                <text style={{ fontWeight: 'bold' }}>IDR 5.660.000</text>
              </div>
            </div>

          </div>
          <div className={buttonFooter}>
            <button
              disabled={isLoading}
              className={buttonOutline} type="button" onClick={() => {
                onSubmitContainer('PAY LATER');
              }}
              style={{ marginTop: 39, height: 48 }}>Batalkan</button>
            <button
              disabled={isLoading}
              className="next-button" type="button" onClick={() => {
                onSubmitContainer('CASH');
              }}
              style={{ border: '0px', marginTop: 39, height: 48, cursor: 'pointer' }}>
              <div className="next-button-text" >Proses</div>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Invoice;