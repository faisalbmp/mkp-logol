import React, { useState, useEffect } from "react";
import moment from 'moment';
import { FullScreen, useFullScreenHandle } from "react-full-screen";
import { useHistory } from 'react-router-dom'

import lightLogo from 'assets/images/dashboard/light_logo.png';
import darkLogol from 'assets/images/dashboard/dark_logo.png';
import { SunIcon, MonthIcon, PauseMonitorIcon, PlayMonitorIcon } from 'components/icon/dashboard/monitor';
import { monitor, boxShadow, headerMonitor, childHeaderMonitor, footer, smallText, footerRight, borderCardMonitorLabel, lightMonitorLabel, cardMonitorLabel, cardMonitorBoxShadow, cardMonitor, cardChild, cardChildLight, fontTextWeight, fontTextSemiBold, dot, row, rowChild, column, lineHeight, fontTextNormal, wizardProgress, stepName, pending, animationChangeColor, pending1, pending2, pending2Light, pending2Dark, anticon, classFullSc } from 'assets/scss/page/monitor/monitor.module.scss'
import LayoutDashboard from 'components/dashboard/layout'
import dataDummy from 'locales/dummy/monitor.json'
import { FullScreen as FullScreenIcon, CloseScreen } from 'components/icon/iconSvg'
import { getOrderListMonitor } from 'actions/TruckBookingList/getOrderListing'
import { useDispatch, useSelector } from "react-redux";
import CardMonitor from 'components/cards/CardMonitor'
import delayInput from 'utils/delayInput';

const darkMode = {
    type: "dark",
    backgroundColor: "#192231",
    backgroundHeader: "#192231",
    colorHeader: "#E8EEF2",
    colorText: "#E8EEF2",
    colorCardDark: "#263245",
    blueColor: "#30B1FF",
    boxShadowHeader: "0px 3px 10px rgba(0, 0, 0, 0.05)",
    borderBottomHeader: "#494F58",
    textDarkMode: "Dark Mode",
    backgroundColorCardExport: "#192231",
    backgroundColorCardImport: "#263245",
    colorIcon: "#E8EEF2"
}

const lightMode = {
    type: "light",
    backgroundColor: "#FAFAFA",
    backgroundHeader: "#FFFFFF",
    colorHeader: " #868A92",
    colorText: "#333333",
    colorCardDark: "#263245",
    blueColor: "#30B1FF",
    boxShadowHeader: "",
    borderBottomHeader: "transparent",
    textDarkMode: "Light Mode",
    backgroundColorCardExport: "#FFFFFF",
    backgroundColorCardImport: "#FFFFFF",
    colorIcon: "#333333"
}

const Monitor = () => {
    const [fullScreenActive, setFullScreen] = useState(false)
    const [play, setPlay] = useState(true)
    const [activeAnimation, setActiveAnimation] = useState(false)
    const [styleMode, setStyleMode] = useState(lightMode)
    const [second, setSecond] = useState(10)
    const [record, setRecord] = useState(5)
    const [pageNo, setPageNo] = useState(1)
    const [params, setParams] = useState(`?type=${"INPROGRESS"}&pageNo=${pageNo}&pageSize=${record}`)
    const history = useHistory()
    const { backgroundColor, colorText, type, colorIcon, backgroundHeader, colorHeader, backgroundColorCardImport, borderBottomHeader, textDarkMode, backgroundColorCardExport } = styleMode
    const handle = useFullScreenHandle();
    const dispatch = useDispatch()
    const { getOrderDetail: { getTruckOrderDetail } } = useSelector(state => state)
    const { data, totalRecords } = getTruckOrderDetail

    const dataGet = (valParam) => dispatch(getOrderListMonitor(valParam));
    const getApiDely = () => delayInput(async () => {
        await dataGet(`?type=${"INPROGRESS"}&pageNo=${pageNo}&pageSize=${record}`)
        // setParams(`?type=${"INPROGRESS"}&pageNo=${pageNo + 1}&pageSize=${record}`)
        setPageNo(pageNo + 1)
        console.log(pageNo, params)
        getApiDely()
    }, second * 1000);

    console.log(play)
    if (play) {
        getApiDely()
    }

    useEffect(() => {
        dataGet(`?type=${"INPROGRESS"}&pageNo=${pageNo}&pageSize=${record}`)
    }, [])

    const onChangePlay = () => {
        setPlay(!play)
    }
    const onChangeColor = () => {
        setActiveAnimation(!activeAnimation)
    }
    const onAnimationEnd = (styleMode) => {
        setStyleMode(styleMode)
        setActiveAnimation(true)
    }

    const onChangeFullScreen = () => {
        setFullScreen(!fullScreenActive)
        if (fullScreenActive) {
            handle.exit()
        } else {
            handle.enter()
        }
    }

    const onChangeRecord = async (value) => {
        console.log(value.target.value)
        dataGet(`?type=${"INPROGRESS"}&pageNo=${pageNo}&pageSize=${value.target.value}`)
        await setRecord(value.target.value)
    }

    const onChangeSecond = async (value) => {
        await setSecond(value.target.value);
        dataGet(`?type=${"INPROGRESS"}&pageNo=${pageNo}&pageSize=${record}`)
    }
    // const itemRender = (current, type, originalElement) => {
    //     if (type === 'prev') {
    //         return <span className={smallText} style={{ marginRight: 16 }}>Previous</span>;
    //     }
    //     if (type === 'next') {
    //         return <span className={smallText} style={{ marginLeft: 16 }}>Next</span>;
    //     }
    //     return originalElement;
    // }
    return (
        <LayoutDashboard>

            <FullScreen handle={handle}>
                <div style={{ backgroundColor: backgroundColor, padding: fullScreenActive ? 30 : 0 }} className={monitor} >
                    <div className={`${headerMonitor}  ${type === "light" ? boxShadow : boxShadow}`} style={{ backgroundColor: backgroundHeader, borderBottomColor: borderBottomHeader }}>
                        <div className={childHeaderMonitor}>
                            <div className={row}>
                                <img alt="dark-logo" src={type === "light" ? darkLogol : lightLogo} style={{ marginLeft: 32 }} onClick={() => history.push(`${process.env.PUBLIC_URL}`)} />
                            </div>
                            <div className={row} >
                                <div className={dot} style={{ backgroundColor: "#FA9D13" }}></div>
                                <div className={fontTextSemiBold} style={{ color: colorHeader }}>Job Order : {totalRecords}</div>
                            </div>
                            <div className={row} >
                                <div className={dot} style={{ backgroundColor: "#13FAA7" }}></div>
                                <div className={fontTextSemiBold} style={{ color: colorHeader }}>On Going : 25</div>
                            </div>
                            <div className={row} style={{ justifyContent: "center" }}>
                                <div style={{ color: colorHeader, fontWeight: "bold", fontFamily: "Nunito Sans", fontSize: 14, letterSpacing: 0.5, marginRight: 16 }}>{moment(new Date()).format("MMMM DD YYYY")}</div>
                                <div className={lineHeight}></div>
                                <div className={fontTextNormal} style={{ color: colorHeader, marginLeft: 16 }}>{moment(new Date()).format("HH:mm:ss")}</div>
                                <div
                                    style={{ color: colorHeader, marginLeft: 32, fontWeight: "600", fontSize: 14, letterSpacing: 0.5, cursor: "pointer" }}
                                    onClick={() => onChangeColor()}
                                    onAnimationEnd={() => onAnimationEnd(type === "dark" ? darkMode : lightMode)}
                                >{textDarkMode}</div>
                                {
                                    type === "dark" ?
                                        <div
                                            style={{ marginLeft: 8 }}
                                            onClick={() => onChangeColor()}
                                            onAnimationEnd={() => onAnimationEnd(lightMode)}
                                            className={activeAnimation ? animationChangeColor : ""}
                                        >
                                            <SunIcon />
                                        </div>
                                        :
                                        <div
                                            style={{ marginLeft: 8 }}
                                            onClick={() => onChangeColor()}
                                            onAnimationEnd={() => onAnimationEnd(darkMode)}
                                            className={activeAnimation ? animationChangeColor : ""}
                                        >
                                            <MonthIcon />
                                        </div>
                                }
                                <div
                                    onClick={onChangeFullScreen}
                                    className={`${anticon} ${classFullSc}`}
                                >
                                    {
                                        fullScreenActive ?
                                            <CloseScreen
                                                color={styleMode.colorIcon}
                                            />
                                            :
                                            <FullScreenIcon
                                                color={styleMode.colorIcon}
                                            />
                                    }

                                </div>
                            </div>
                        </div>
                    </div>

                    <div className={`${cardMonitor} ${cardMonitorLabel} ${type === "light" ? cardMonitorBoxShadow + "  " + borderCardMonitorLabel : lightMonitorLabel}`} style={{ height: 68, backgroundColor: backgroundHeader }}>
                        <div className={row}>
                            <div className={fontTextWeight} style={{ color: colorText, marginLeft: 32 }}>Order #</div>
                        </div>
                        <div className={row} >
                            <div className={fontTextWeight} style={{ color: colorText }}>S. Date</div>
                            <div className={fontTextWeight} style={{ color: colorText, marginLeft: 44 }}>Container</div>
                        </div>
                        <div className={row}>
                            <div className={fontTextWeight} style={{ color: colorText }}>Trucker</div>
                        </div>
                        <div className={row} style={{ justifyContent: "center" }}>
                            <div className={rowChild}>
                                <div className={fontTextWeight} style={{ color: colorText, fontSize: 16 }}>D1</div>
                                <div className={fontTextWeight} style={{ color: colorText, fontSize: 16 }}>D2</div>
                                <div className={fontTextWeight} style={{ color: colorText, fontSize: 16 }}>D3</div>
                            </div>
                        </div>
                    </div>

                    {
                        data && data.map((value, index) => (
                            <CardMonitor
                                key={index}
                                data={value}
                                cardChildLight={cardChildLight}
                                type={type}
                                backgroundColorCardExport={backgroundColorCardExport}
                                styleMode={styleMode}
                            />
                        ))
                    }

                    <div className={footer}>
                        <div className={footerRight}>
                            <div className={smallText}>Show</div>
                            <select style={{ marginLeft: 8, borderColor: type === "light" ? "#E0E5E8" : "#192231" }} name="record" defaultValue={record} onChange={onChangeRecord}>
                                {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15].map((value => (
                                    <option value={value} key={value}>{value}</option>
                                )))}
                            </select>
                            <div className={smallText}>record</div>
                            <div className={smallText} style={{ marginLeft: 32 }}>Page change interval</div>
                            <div className={smallText}>record</div>
                            <select style={{ marginLeft: 8 }} defaultValue={second} name="second" onChange={onChangeSecond}>
                                {[2, 5, 10, 20, 30, 40, 50, 60].map(((value) => (
                                    <option value={value} key={value}>{value}</option>
                                )))}
                            </select>
                            <div className={smallText}>second</div>
                            <div onClick={onChangePlay} style={{ marginLeft: 24, cursor: "pointer" }}>
                                {
                                    play ?
                                        <PlayMonitorIcon color={colorIcon} />
                                        :
                                        <PauseMonitorIcon color={colorIcon} />
                                }
                            </div>
                            <div className={smallText}>Pause Monitoring</div>
                        </div>
                        <div>
                            {/* <Pagination defaultCurrent={1} total={50} itemRender={itemRender} /> */}
                        </div>
                    </div>
                </div >
            </FullScreen>
        </LayoutDashboard >
    );
    // }
}

export default Monitor;

