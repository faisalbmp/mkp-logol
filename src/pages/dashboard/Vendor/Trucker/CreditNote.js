import React, { useState, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { getCookie } from 'utils/cookies';
import LayoutDashboard from 'components/dashboard/vendor/Layout/Layout';
import Breadcrumb from 'components/dashboard/breadcrumb';
import Tagihan from 'components/dashboard/vendor/CreditNote/Tagihan/Tagihan';
import Table from 'components/dashboard/vendor/CreditNote/Table/Table';
import Styles from './filterPencarian.module.scss';
import SelectBoxM from 'components/select/SelectBoxM';
import Button from 'components/button/Button';
import DateInput from 'components/input/inputDate';
import SelectBox from 'components/checkBox/select-box';
import moment from 'moment';

import {
	getVendorListCn,
	getVendorUnpaidCn,
	getVendorPaidCn,
	getVendorTotalCn,
} from 'actions';

const CreditNote = props => {
	// const history = useHistory();
	// const userId = getCookie('userId');

	// if (userId.substr(0, 4) !== 'VEND') {
	// 	history.push('/dashboard');
	// }

	const [typeSelected, setTypeSelected] = useState(false);
	const [statusSelected, setStatusSelected] = useState(false);
	const [status, setStatus] = useState('Status');
	// const [startDate, setStartDate] = useState('');
	// const [endDate, setEndDate] = useState('');
	const [type, setType] = useState('Type');
	const [formData, setFormData] = useState({
		type: {
			options: [
				{ value: 'EXPORT', name: 'Export', title: 'type' },
				{ value: 'IMPORT', name: 'Import', title: 'type' },
			],
			value: 'EXPORT',
		},
		status: {
			options: [
				{ value: 'Unpaid', name: 'Unpaid', title: 'status' },
				{ value: 'Paid', name: 'Paid', title: 'status' },
			],
			value: 'Unpaid',
		},
		dateFrom: { value: '2001-01-01' },
		dateTo: { value: '2020-12-12' },
		// date: [
		// 	{ name: 'dateFrom', value: '2001-12-12' },
		// 	{ name: 'dateTo', value: '2020-01-01' },
		// ],
	});

	const dateStyle = {
		marginBottom: 10,
		height: 52,
		width: 164,
	};

	const dispatch = useDispatch();
	const showUnpaidCN = useCallback(
		(dateFrom, dateTo, status) => dispatch(getVendorUnpaidCn(dateFrom, dateTo, status)),
		[dispatch]
	);
	const showListCN = useCallback(formData => dispatch(getVendorListCn(formData)), [
		dispatch,
	]);
	const showPaidCN = useCallback(
		(dateFrom, dateTo, status) => dispatch(getVendorPaidCn(dateFrom, dateTo, status)),
		[dispatch]
	);

	// const formatDate = (date) => {
	// 	let month = (date.getMonth()+1).toString(),
	// 		day = (date.getDate()).toString(),
	// 		year = date.getFullYear();
	// 		if (month.length < 2)
	//     month = '0' + month;
	// if (day.length < 2)
	// 	day = '0' + day;

	// 	return [year, month, day].join('')
	// }

	const onChangeinput = formConfig => {
		const updatedFormData = {
			...formData,
		};
		const updatedFormElement = {
			...updatedFormData[formConfig.title],
		};
		// if (item.title === 'date') {
		// 	set
		// 	updatedFormElement.value = ;
		// }
		updatedFormElement.value = formConfig.value;
		updatedFormData[formConfig.title] = updatedFormElement;
		console.log(updatedFormData[formConfig.title]);
		setFormData(updatedFormData);
	};

	const data = {};
	for (let key in formData) {
		data[key] = formData[key].value;
	}

	const showCreditNote = () => {
		const updatedData = {};
		console.log(formData);
		for (let key in formData) {
			updatedData[key] = formData[key].value;
		}
		// console.log(data);
		dispatch(getVendorListCn(updatedData));
	};

	const { vendorReducer } = useSelector(state => state);

	useEffect(() => {
		showUnpaidCN('', '', 'UNPAID');
		showListCN(data);
		showPaidCN('', '', 'PAID');
	}, [showUnpaidCN, showListCN]);
	console.log(formData);

	// const formElementArray = [];
	// for (let key in formData) {
	// 	// console.log(key);
	// 	formElementArray.push({
	// 		id: key,
	// 		config: formData[key],
	// 	});
	// }
	// formElementArray.map((item, index) => console.log(item));
	return (
		<LayoutDashboard>
			<Breadcrumb title='Penagihan dan Pembayaran' />
			<Tagihan vendorReducer={vendorReducer} />
			<div style={{ marginTop: 24 }}>
				<div className={`box-white ${Styles.content}`}>
					<div className={Styles.item}>
						<div className={Styles.title}>Filter Pencarian : </div>
					</div>
					<div className={Styles.item}>
						<SelectBox
							headerStyle={{
								marginTop: 18,
								width: 180,
								paddingLeft: 12,
								paddingRight: 12,
							}}
							placeholder={type}
							isDefault
							openList={typeSelected}
							selectList={formData.type.options}
							openSelectList={() => {
								setStatusSelected(false);
								setTypeSelected(!typeSelected);
							}}
							selectedList={(item, index) => {
								setType(item.name);
								setTypeSelected(false);
								onChangeinput(item);
							}}
						/>
					</div>
					<div className={Styles.item}>
						<SelectBox
							headerStyle={{
								marginTop: 18,
								width: 180,
								paddingLeft: 12,
								paddingRight: 12,
							}}
							placeholder={status}
							isDefault
							openList={statusSelected}
							selectList={formData.status.options}
							openSelectList={() => {
								setTypeSelected(false);
								setStatusSelected(!statusSelected);
							}}
							selectedList={(item, index) => {
								setStatus(item.name);
								setStatusSelected(false);
								onChangeinput(item);
							}}
						/>
					</div>
					<div className={Styles.item}>
						{/* <SelectBoxM label={'16/10/2020 - 16/10/2020'} /> */}
						<DateInput
							selectedDate={formData.dateFrom.value}
							pickerType='custom'
							placeholderInput='2001-01-01'
							headerStyle={dateStyle}
							onChange={date => {
								setFormData(prev => ({
									...prev,
									dateFrom: { value: moment(date).format('YYYY-MM-DD') },
								}));
							}}
						/>
					</div>
					<div className={Styles.item}>
						{/* <SelectBoxM label={'16/10/2020 - 16/10/2020'} /> */}
						<DateInput
							selectedDate={formData.dateTo.value}
							pickerType='custom'
							placeholderInput='2021-01-01'
							headerStyle={dateStyle}
							onChange={date => {
								setFormData(prev => ({
									...prev,
									dateTo: { value: moment(date).format('YYYY-MM-DD') },
								}));
							}}
						/>
					</div>
					<div className={Styles.item}>
						<Button
							className={Styles.btTampilkan}
							label='Tampilkan'
							clicked={() => showCreditNote()}
						/>
					</div>
				</div>
			</div>
			<div style={{ marginTop: 24 }}>
				<Table vendorReducer={vendorReducer} />
			</div>
		</LayoutDashboard>
	);
};

export default CreditNote;
