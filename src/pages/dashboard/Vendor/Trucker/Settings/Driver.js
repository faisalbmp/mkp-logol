import React, { useState, useCallback, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import LayoutVendor from 'components/dashboard/vendor/Layout/Layout';
import Breadcrumb from 'components/dashboard/breadcrumb';
import AddDriverModal from 'components/dashboard/vendor/Modal/Settings/Driver/AddDriverModal';
import EditDriverModal from 'components/dashboard/vendor/Modal/Settings/Driver/EditDriverModal';
import SuccessModal from 'components/modal/SuccessModal';
import DriverTable from 'components/dashboard/vendor/VendorTable/DriverTable';
import DeleteModal from 'components/dashboard/vendor/Modal/DeleteModal';
import { getCookie } from 'utils/cookies';

import { AddIcon } from 'components/icon/iconSvg';

import {
	insertVendorDriver,
	updateVendorDriver,
	deleteVendorDriver,
	getVendorListDriver,
} from 'actions';

const Vehicle = props => {
	// const history = useHistory();
	// const userId = getCookie('userId');

	// if (userId.substr(0, 4) !== 'VEND') {
	// 	history.push('/dashboard');
	// }

	const vendorId = getCookie('userId');
	const [driverId, setDriverId] = useState('');
	const [driverName, setDriverName] = useState('');
	const [modal, setModal] = useState('');
	const [isInserted, setIsInserted] = useState(false);
	const [insertForm, setInserForm] = useState({
		vendorId: vendorId,
		driverName: '',
		mobilePhone: '',
		driverUsername: '',
		driverPassword: '',
		deviceId: '',
	});
	const [updateForm, setUpdateForm] = useState({
		driverId: '',
		vendorId: vendorId,
		driverName: '',
		mobilePhone: '',
		deviceId: '',
		driverUsername: '',
		driverPassword: '',
	});
	const dispatch = useDispatch();

	const { listDriver, message } = useSelector(state => state.driverReducers);

	const onFetchDrivers = useCallback(() => dispatch(getVendorListDriver()), [dispatch]);

	useEffect(() => {
		onFetchDrivers();
		setIsInserted(false);
	}, [onFetchDrivers, isInserted]);

	const onChangeInserFormInput = e => {
		setInserForm({
			...insertForm,
			[e.target.name]: e.target.value,
		});
	};

	const onChangeUpdateFormInput = e => {
		setUpdateForm({
			...updateForm,
			[e.target.name]: e.target.value,
		});
	};

	const insertDriver = async () => {
		const formData = { ...insertForm };
		const res = await dispatch(insertVendorDriver(formData));
		if (res) {
			setIsInserted(true);
			setModal('successPopUp');
			setTimeout(() => {
				setModal('');
			}, 3000);
		} else {
			alert(message);
		}
	};

	const showEditModal = (driverId, vendorId) => {
		setUpdateForm({ ...updateForm, driverId: driverId, vendorId: vendorId });
		setModal('editModal');
		console.log(driverId + ' ' + vendorId);
	};

	const updateHandler = async () => {
		const formData = { ...updateForm };
		const res = await dispatch(updateVendorDriver(formData));
		if (res) {
			setModal('successPopUp');
			setTimeout(() => {
				setModal('');
			}, 3000);
		} else {
			alert('Galat');
		}
	};

	const showDeleteModal = (driverId, driverName) => {
		setModal('deleteModal');
		setDriverId(driverId);
		setDriverName(driverName);
	};

	const deleteHandler = async () => {
		const res = dispatch(deleteVendorDriver(driverId));
		if (res) {
			setModal('successPopUp');
			setTimeout(() => {
				setModal('');
			}, 3000);
		} else {
			alert('Galat');
		}
	};

	return (
		<LayoutVendor>
			<div
				style={{
					display: 'flex',
					flexDirection: 'row',
					justifyContent: 'space-between',
					marginBottom: 36,
				}}
			>
				<Breadcrumb title='Data Pengemudi' />
				<div
					className='btn-dsb-right'
					style={{ cursor: 'pointer' }}
					onClick={() => setModal('addDriver')}
				>
					Tambah Pengemudi
					<AddIcon />
				</div>
			</div>
			<DriverTable
				editClicked={(driverId, vendorId) => showEditModal(driverId, vendorId)}
				deleteClicked={(driverId, driverName) => showDeleteModal(driverId, driverName)}
				listDriver={listDriver}
			/>
			<AddDriverModal
				show={modal === 'addDriver'}
				onClose={() => setModal('')}
				onSuccess={() => setModal('successPopUp')}
				insertHandler={insertDriver}
				changeHandler={onChangeInserFormInput}
				formData={insertForm}
			/>
			<EditDriverModal
				show={modal === 'editModal'}
				onClose={() => setModal('')}
				onSuccess={() => setModal('successPopUp')}
				changeHandler={onChangeUpdateFormInput}
				updateHandler={updateHandler}
				formData={updateForm}
			/>
			<SuccessModal
				subTitleStyle={{ fontSize: '16px', fontWeight: 600 }}
				show={modal === 'successPopUp'}
				onClose={() => setModal('')}
				title='Data Pengemudi Berhasil Disimpan'
				titleStyle={{ marginBottom: 13 }}
				textSubTitle1='Dengan Nomor ID Pengemudi'
				textSubTitle2='VEND-20200429-0003'
			/>
			<DeleteModal
				show={modal === 'deleteModal'}
				onClose={() => setModal('')}
				onSuccess={deleteHandler}
				title='Menghapus Data Pengemudi'
				text={`Apakah anda yakin ingin menghapus data pengemudi yang bernama “${driverName}” ?`}
			/>
		</LayoutVendor>
	);
};

export default Vehicle;
