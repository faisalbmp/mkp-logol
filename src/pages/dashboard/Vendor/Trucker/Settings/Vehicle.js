import React, { useState, useCallback, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import LayoutVendor from 'components/dashboard/vendor/Layout/Layout';
import Breadcrumb from 'components/dashboard/breadcrumb';
import AddVehicleModal from 'components/dashboard/vendor/Modal/Settings/Vehicle/AddVehicleModal';
import EditVehicleModal from 'components/dashboard/vendor/Modal/Settings/Vehicle/EditVehicleModal';
import SuccessModal from 'components/modal/SuccessModal';
import VehicleTable from 'components/dashboard/vendor/VendorTable/VehicleTable';
import DeleteModal from 'components/dashboard/vendor/Modal/DeleteModal';
import { getCookie } from 'utils/cookies';

import { AddIcon } from 'components/icon/iconSvg';

import {
	insertVendorVehicle,
	updateVendorVehicle,
	deleteVendorVehicle,
	getVendorListVehicle,
} from 'actions';

const Vehicle = props => {
	// const history = useHistory();
	// const userId = getCookie('userId');

	// if (userId.substr(0, 4) !== 'VEND') {
	// 	history.push('/dashboard');
	// }

	const vendorId = getCookie('userId');
	const [modal, setModal] = useState('');
	const [vehicleNumber, setVehicleNumber] = useState('');

	const [vehicleForm, setVehicleForm] = useState({
		vendorId: vendorId,
		vehicleNumber: '',
		brand: '',
		type: '',
		year: '',
		tidNumber: '',
		stnkNumber: ' ',
		stnkExpired: '2021-01-01',
	});

	const dispatch = useDispatch();
	const { listVehicle, message } = useSelector(state => state.vehicleReducers);
	const onFetchVehicles = useCallback(() => dispatch(getVendorListVehicle()), [dispatch]);

	useEffect(() => {
		onFetchVehicles();
	}, [onFetchVehicles]);

	const onChangeInput = e => {
		setVehicleForm({
			...vehicleForm,
			[e.target.name]: e.target.value,
		});
	};

	const addVehicleHandler = async () => {
		const formData = { ...vehicleForm };
		const res = await dispatch(insertVendorVehicle(formData));
		if (res) {
			setModal('successPopUp');
			setTimeout(() => {
				setModal('');
			}, 3000);
		} else {
			alert(message);
		}
	};

	const showEditModal = vehicleNumber => {
		setVehicleForm({ ...vehicleForm, vehicleNumber: vehicleNumber });
		setModal('editVehicle');
	};

	const updateVehicleHandler = async () => {
		const formData = { ...vehicleForm };
		console.log(formData);
		const res = await dispatch(updateVendorVehicle(formData));
		if (res) {
			setModal('successPopUp');
		} else {
			alert(message);
		}
	};

	const showDeleteModal = vehicleNumber => {
		setVehicleNumber(vehicleNumber);
		setModal('deleteModal');
	};

	const deleteVehicleHandler = async () => {
		const res = await dispatch(deleteVendorVehicle(vehicleNumber));
		if (res) {
			setModal('');
		} else {
			alert('Galat');
		}
	};

	return (
		<LayoutVendor>
			<div
				style={{
					display: 'flex',
					flexDirection: 'row',
					justifyContent: 'space-between',
					marginBottom: 36,
				}}
			>
				<Breadcrumb title='Data Kendaraan' />
				<div
					className='btn-dsb-right'
					style={{ cursor: 'pointer' }}
					onClick={() => setModal('addVehicle')}
				>
					Add Vehicle
					<AddIcon />
				</div>
			</div>
			<VehicleTable
				listVehicle={listVehicle}
				editClicked={vehicleNumber => showEditModal(vehicleNumber)}
				deleteClicked={vehicleNumber => showDeleteModal(vehicleNumber)}
			/>
			<AddVehicleModal
				show={modal === 'addVehicle'}
				onClose={() => setModal('')}
				onSuccess={addVehicleHandler}
				formData={vehicleForm}
				changeHandler={onChangeInput}
				addHandler={addVehicleHandler}
			/>
			<EditVehicleModal
				show={modal === 'editVehicle'}
				onClose={() => setModal('')}
				onSuccess={() => setModal('successPopUp')}
				formData={vehicleForm}
				changeHandler={onChangeInput}
				updateHandler={updateVehicleHandler}
			/>
			<SuccessModal
				// titleStyle={{ fontSize: '16px', fontWeight: 600 }}
				subTitleStyle={{ fontSize: '16px', fontWeight: 600 }}
				show={modal === 'successPopUp'}
				onClose={() => setModal('')}
				title='Data Kendaraan Berhasil Disimpan'
				titleStyle={{ marginBottom: 13 }}
				textSubTitle1='Dengan Nomor ID Kendaraan'
				textSubTitle2='VEND-20200429-0003'
			/>
			<DeleteModal
				show={modal === 'deleteModal'}
				onClose={() => setModal('')}
				onSuccess={deleteVehicleHandler}
				title='Menghapus Data Kendaraan'
				text={`Apakah anda yakin ingin menghapus data kendaraan dengan No. Kendaraan  “${vehicleNumber}” ?`}
			/>
		</LayoutVendor>
	);
};

export default Vehicle;
