import React from 'react';
import { useHistory } from 'react-router-dom';
import { LineChart, CartesianGrid, XAxis, YAxis, Tooltip, Legend, Line } from 'recharts';

import LayoutDashboard from 'components/dashboard/vendor/Layout/Layout';
import Styles from 'assets/scss/dashboard/vendor/vendor.module.scss';
import { ETipeCont, ITipeIcon } from 'assets/images/vendor/svg/icon';
import ListContainer from 'components/container/vendor/dashboard/ListContainer';
import { getCookie } from 'utils/cookies';
import Agenda from 'components/dashboard/vendor/Agenda/Agenda';

import unprocessedBox from 'assets/images/vendor/svg/unprocessed.svg';
import processedBox from 'assets/images/vendor/svg/processed.svg';
import orderFinishedBox from 'assets/images/vendor/svg/orderFinished.svg';
import gatepassReleasedBox from 'assets/images/vendor/svg/gatepassReleased.svg';

const Dashboard = props => {
	const vendorId = getCookie('vendorId');
	console.log(vendorId);
	// const history = useHistory();
	// const userId = getCookie('userId');

	// if (userId.substr(0, 4) !== 'VEND') {
	// 	history.push('/dashboard');
	// }

	console.log('Dashboard Page');
	const containers = [
		{
			type: 'LG/202009/E0004',
			status: { label: 'Dalam Proses', type: 'no-proses' },
			from: 'Cikarang',
			to: 'Tj. Priok',
			icon: <ETipeCont />,
		},
		{
			type: 'LG/202009/I0023',
			status: { label: 'Dalam Proses', type: 'no-proses' },
			from: 'Tj. Priok',
			to: 'Banten',
			icon: <ITipeIcon />,
		},
	];

	const graphData = [
		{ name: 'Jan', expor: 10, impor: 40 },
		{ name: 'Feb', expor: 40, impor: 20 },
		{ name: 'Mar', expor: 30, impor: 25 },
		{ name: 'Apr', expor: 70, impor: 50 },
		{ name: 'Mei', expor: 25, impor: 10 },
		{ name: 'Jun', expor: 5, impor: 5 },
	];
	console.log(props);

	return (
		<LayoutDashboard>
			<div>
				<div className='title-home'>Hallo, PT. Anugrah Mulia Transindo</div>
				<div style={{ fontSize: 16, color: '#333333', marginTop: 8 }}>
					Selamat datang di Logol Management System
				</div>
				<div
					style={{
						display: 'flex',
						flexFlow: 'row',
						justifyContent: 'space-between',
						marginTop: 48,
					}}
				>
					<div>
						<div
							style={{
								display: 'flex',
								flexFlow: 'row',
								justifyContent: 'space-between',
								width: 816,
							}}
						>
							<div className={`${Styles.box} ${Styles.unprocessedBox}`}>
								<div
									className={Styles.label}
									style={{
										position: 'absolute',
										marginLeft: 32,
										marginTop: 24,
									}}
								>
									<div className={Styles.big}>9</div>
									<div className={Styles.small} style={{ widht: 100 }}>
										Unprocessed
									</div>
									<div className={Styles.small} style={{ widht: 100 }}>
										Order
									</div>
								</div>
								<img src={unprocessedBox} alt='Not Found' />
							</div>
							<div className={`${Styles.box} ${Styles.processedBox}`}>
								<div
									className={Styles.label}
									style={{
										position: 'absolute',
										marginLeft: 32,
										marginTop: 24,
									}}
								>
									<div className={Styles.big}>16</div>
									<div className={Styles.small} style={{ widht: 100 }}>
										Processed
									</div>
									<div className={Styles.small} style={{ widht: 100 }}>
										Order
									</div>
								</div>
								<img src={processedBox} alt='Not Found' />
							</div>
							<div className={`${Styles.box} ${Styles.orderFinishedBox}`}>
								<div
									className={Styles.label}
									style={{
										position: 'absolute',
										marginLeft: 32,
										marginTop: 24,
									}}
								>
									<div className={Styles.big}>12</div>
									<div className={Styles.small} style={{ widht: 100 }}>
										Order
									</div>
									<div className={Styles.small} style={{ widht: 100 }}>
										Finished
									</div>
								</div>
								<img src={orderFinishedBox} alt='Not Found' />
							</div>
							<div className={`${Styles.box} ${Styles.gatepassReleasedBox}`}>
								<div
									className={Styles.label}
									style={{
										position: 'absolute',
										marginLeft: 32,
										marginTop: 24,
									}}
								>
									<div className={Styles.big}>16</div>
									<div className={Styles.small} style={{ widht: 100 }}>
										Gate Pass
									</div>
									<div className={Styles.small} style={{ widht: 100 }}>
										Released
									</div>
								</div>
								<img src={gatepassReleasedBox} alt='Not Found' />
							</div>
						</div>
						<div className={Styles.graph} style={{ marginTop: 50 }}>
							<h3>Total Order</h3>
							<LineChart width={768} height={320} data={graphData} margin={{ left: -24 }}>
								<CartesianGrid strokeDasharray='3 3' />
								<XAxis dataKey='name' />
								<YAxis />
								<Tooltip />
								<Legend verticalAlign='top' align='right' height={36} iconType='circle' />
								<Line type='monotone' dataKey='expor' stroke='#1501FE' strokeWidth={4} />
								<Line type='monotone' dataKey='impor' stroke='#00E599' strokeWidth={4} />
							</LineChart>
						</div>
					</div>

					<div className={Styles.rightContent}>
						<Agenda />
						{containers.map((cont, index) => {
							return (
								<ListContainer
									key={index}
									type={cont.type}
									from={cont.from}
									to={cont.to}
									label={cont.status.label}
									icon={cont.icon}
								/>
							);
						})}
					</div>
				</div>
			</div>
		</LayoutDashboard>
	);
};

export default Dashboard;
