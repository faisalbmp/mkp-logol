import React, { useMemo, useState, useEffect, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Styles from 'assets/scss/dashboard/vendor/vendor.module.scss';
import LayoutDashboard from 'components/dashboard/vendor/Layout/Layout';
import Breadcrumb from 'components/dashboard/breadcrumb';
import TableStyles from 'assets/scss/dashboard/table.module.scss';
import {
	ArrowDownD,
	ArrowUpD,
	AddIcon,
	ArrowRightTable,
	EyeIcon,
	FilterIcon,
} from 'components/icon/iconSvg';
import SearchInput from 'components/dashboard/searchInput';
import TruckOrderDetail from 'components/dashboard/vendor/OrderDetail/TruckOrderDetail';
import Pagination from 'components/paginations/New/Pagination';
import AssignDriverExport from 'components/dashboard/vendor/Modal/AssignDriver/AssignDriverExport';
import AssignDriverImport from 'components/dashboard/vendor/Modal/AssignDriver/AssignDriverImport';
import OrderModal from 'components/dashboard/vendor/Modal/OrderModal/OrderModal';
import RuteModal from 'components/dashboard/vendor/Modal/RuteModal/RuteModal';
import LoadingDot from 'components/loading/loadingDot';
import { XIcon, WheelIcon } from 'components/dashboard/vendor/Icon';
import DateInput from 'components/input/inputDate';
import { getCookie } from 'utils/cookies';
import moment from 'moment';

import {
	getVendorListOrder,
	getVendorListOrderDetail,
	getVendorListDriver,
	getVendorListVehicle,
	getVendorListContainer,
	vendorAssignDriver,
} from 'actions';

const dateStyle = {
	marginLeft: 12,
	marginBottom: 12,
	height: 36,
	width: 136,
};

export default function Truck() {
	const vendorId = getCookie('vendorId');
	const [searchVal, setSearchVal] = useState('');
	const [activeTab, setActiveTab] = useState('EXPORT');
	const [loadOrderDetail, setLoadOrderDetail] = useState(false);
	// const [showExpendedView, setExpendedView] = useState(false);
	// const [activeIndex, setActiveIndex] = useState(0);
	const [currentPage, setCurrentPage] = useState(1);
	const [postsPerPage, setPostPerPage] = useState(10);
	const [showFilter, setShowFilter] = useState(false);
	const [modal, setModal] = useState('');
	const [orderId, setOrderId] = useState('');
	const [orderData, setOrderData] = useState({});
	const [dateFilter, setDate] = useState({
		dateFrom: '2001-01-01',
		dateTo: '2020-12-13',
	});

	const {
		vendorReducer,
		driverReducers,
		vehicleReducers,
		containerReducers,
	} = useSelector(state => state);
	const { isLoading, listOrderData } = vendorReducer;
	const { listContainer } = containerReducers;
	const { listDriver } = driverReducers;
	const { listVehicle } = vehicleReducers;
	const dispatch = useDispatch();
	const onFetchListOrder = useCallback(
		(orderType, date) => dispatch(getVendorListOrder(orderType, date)),
		[dispatch]
	);
	const onFetchListOrderDetail = useCallback(
		(OrderId, orderType) => dispatch(getVendorListOrderDetail(OrderId, orderType)),
		[dispatch]
	);
	const onFetchlistDriver = useCallback(() => dispatch(getVendorListDriver()), [
		dispatch,
	]);
	const onFetchListVehicle = useCallback(() => dispatch(getVendorListVehicle()), [
		dispatch,
	]);

	const onFetchListContainer = useCallback(
		orderMgtDetailId => dispatch(getVendorListContainer(orderMgtDetailId)),
		[dispatch]
	);

	const drivers = [];
	for (let key in listDriver) {
		drivers.push({
			id: key,
			name: listDriver[key].DriverName,
			value: listDriver[key].DriverID,
			type: 'drivers',
		});
	}
	const vehicles = [];
	for (let key in listVehicle) {
		vehicles.push({
			id: key,
			name: listVehicle[key].VehicleNumber,
			value: listVehicle[key].VehicleNumber,
			type: 'vehicles',
		});
	}
	const containers = [];
	for (let key in listContainer) {
		containers.push({
			id: key,
			name: listContainer[key].containerID,
			value: [listContainer[key].containerNumber, listContainer[key].sealNumber],
			type: 'containers',
		});
	}
	useEffect(() => {
		onFetchListOrder(activeTab, dateFilter);
		onFetchlistDriver();
		onFetchListVehicle();
	}, [onFetchListOrder, activeTab, onFetchlistDriver, onFetchListVehicle]);

	const modalHandler = name => {
		setModal(name);
	};
	const indexOfLastPost = currentPage * postsPerPage;
	const indexOfFirstPost = indexOfLastPost - postsPerPage;

	const posts = useMemo(() => {
		let postData = listOrderData;

		if (searchVal) {
			postData = postData.filter(
				item =>
					item.orderManagementID.toLowerCase().includes(searchVal.toLowerCase()) ||
					item.deliveryOrderID.toLowerCase().includes(searchVal.toLowerCase()) ||
					item.customer.toLowerCase().includes(searchVal.toLowerCase())
			);
		}

		return postData;
	}, [searchVal, listOrderData]);

	const gotoPage = page => {
		const currentPage = Math.max(0, Math.min(page, posts.length));
		setCurrentPage(currentPage);
	};

	const handleClick = page => {
		// e.preventDefault();
		gotoPage(page);
	};

	const handleMoveLeft = () => {
		// e.preventDefault();
		gotoPage(currentPage - 1);
	};

	const handleMoveRight = () => {
		// e.preventDefault();
		gotoPage(currentPage + 1);
	};

	const onOrderDetailClick = (orderId, orderType, orderData) => {
		onFetchListContainer(orderData.orderManagementDetailID);
		setOrderData(orderData);
		onFetchListOrderDetail(orderId, orderType);
		setLoadOrderDetail(true);
	};

	const onAssignClick = (orderId, orderMgtDetailId) => {
		// onFetchListOrderDetail(orderId);
		setOrderId(orderId);
		onFetchListContainer(orderMgtDetailId);
		setModal('assignDriver' + activeTab);
	};

	const assignDriver = async formData => {
		const payload = {
			vendorOrderId: orderId,
			vendorId: vendorId,
			driverId: formData.drivers.value,
			vehicleNumber: formData.vehicles.value,
			containerNumber: formData.containers ? formData.containers.value[0] : '',
			sealNumber: formData.containers ? formData.containers.value[1] : '',
			orderType: activeTab,
		};
		const res = await dispatch(vendorAssignDriver(payload));
		if (res) {
			setModal('');
		} else {
			alert('galat');
		}
	};

	let titleTable = [
		{ name: 'No. Booking Truk', iconSort: true },
		{
			name: activeTab === 'EXPORT' ? 'Shipping Instruction' : 'Bill of Landing',
			iconSort: true,
		},
		{ name: 'SPPB', iconSort: true },
		{ name: 'Delivery Order', iconSort: true },
		{ name: 'Customer', iconSort: true },
	];

	if (activeTab === 'EXPORT') {
		titleTable = titleTable.filter(item => item.name !== 'SPPB');
	}

	const titleTable2 = [
		{ name: activeTab === 'EXPORT' ? 'Stuffing Date' : 'ETA Date', iconSort: true },
		{ name: 'Party', iconSort: true },
		{ name: 'Rute Pengiriman', iconSort: true },
		{ name: 'Status', iconSort: true },
		{ name: 'Aksi', iconSort: true },
	];

	const getStatus = status => {
		switch (status) {
			case '0':
				return 'Picked';
			case '1':
				return 'Assigned';
			case '2':
				return 'On Process';
			case '3':
				return 'Delivered';
			case '10':
				return 'Cancelled';
			default:
				return '';
		}
	};

	return (
		<LayoutDashboard>
			<div className='truck'>
				<div className='header-truk'>
					{!loadOrderDetail ? (
						<Breadcrumb title='Pemesanan Truk' />
					) : (
						<div className='truck-order-detail-breadcrum'>
							<Breadcrumb title='Detail Pemesanan' />
						</div>
					)}
					{loadOrderDetail && !isLoading && (
						<div
							className='btn-dsb-right'
							style={{ cursor: 'pointer' }}
							onClick={() => modalHandler('assignDriver' + activeTab)}
						>
							Assign Driver
							<AddIcon />
						</div>
					)}
				</div>
				{!loadOrderDetail ? (
					<div className='wrepper-table truck-table'>
						<div
							className='flex-row-style'
							style={{ justifyContent: 'space-between', height: 52 }}
						>
							<div className='select-tab'>
								<div
									className={`tab ${activeTab === 'EXPORT' ? 'tab-active' : ''}`}
									onClick={() => setActiveTab('EXPORT')}
								>
									Export
								</div>
								<div
									className={`tab ${activeTab === 'IMPORT' ? 'tab-active' : ''}`}
									onClick={() => setActiveTab('IMPORT')}
								>
									Import
								</div>
							</div>
							<div className='flex-row-style'>
								<div
									className='flex-row-style'
									style={{
										alignItems: 'center',
										marginRight: 32,
									}}
								>
									<label className={Styles.dateLabel}>Stuffing Date From</label>
									<DateInput
										selectedDate={dateFilter.dateFrom}
										pickerType='custom'
										headerStyle={dateStyle}
										onChange={date =>
											setDate(prev => ({
												...prev,
												dateFrom: moment(date).format('YYYY-MM-DD'),
											}))
										}
									/>
								</div>
								<div className='flex-row-style' style={{ alignItems: 'center' }}>
									<label className={Styles.dateLabel}>Stuffing Date To</label>
									<DateInput
										selectedDate={dateFilter.dateTo}
										pickerType='custom'
										headerStyle={dateStyle}
										onChange={date =>
											setDate(prev => ({
												...prev,
												dateTo: moment(date).format('YYYY-MM-DD'),
											}))
										}
									/>
								</div>
							</div>
						</div>
						<div className='middle-component'>
							<SearchInput
								onChange={e => {
									setSearchVal(e.target.value);
									setCurrentPage(1);
								}}
								value={searchVal}
							/>
							<div className='right'>
								<div className={`sort-item ${Styles.filterTooltip}`}>
									<div
										style={{ cursor: 'pointer', display: 'flex', flexDirection: 'row' }}
										onClick={() => setShowFilter(!showFilter)}
									>
										<FilterIcon />
										<div className='text'>Filter</div>
									</div>

									{showFilter && (
										<div className={Styles.tooltipBody}>
											<div className={Styles.tooltipHeader}>
												<h3>Filter</h3>
												<div
													style={{ cursor: 'pointer' }}
													onClick={() => setShowFilter(false)}
												>
													<XIcon />
												</div>
											</div>
											<div className={Styles.tooltipContent} style={{ width: '70%' }}>
												<p className={Styles.subtitle}>Status Pemesanan</p>
												<div
													className='flex-row-style'
													style={{ justifyContent: 'space-between' }}
												>
													<div className={Styles.item}>
														<span className={Styles.text}>Delivered</span>
													</div>
													<div className={Styles.item}>
														<span className={Styles.text}>On Process</span>
													</div>
													<div className={Styles.item}>
														<span className={Styles.text}>Not Process</span>
													</div>
												</div>
											</div>
											<div className={Styles.tooltipContent}>
												<p className={Styles.subtitle}>Ukuran Kontainer</p>
												<div
													className='flex-row-style'
													style={{ justifyContent: 'space-between' }}
												>
													<div className={Styles.item}>
														<span className={Styles.text}>40 RH</span>
													</div>
													<div className={Styles.item}>
														<span className={Styles.text}>40 RF</span>
													</div>
													<div className={Styles.item}>
														<span className={Styles.text}>40 HU</span>
													</div>
													<div className={Styles.item}>
														<span className={Styles.text}>45 HU</span>
													</div>
													<div className={Styles.item}>
														<span className={Styles.text}>40 GP</span>
													</div>
													<div className={Styles.item}>
														<span className={Styles.text}>20 GP</span>
													</div>
												</div>
											</div>
											<div
												style={{
													margin: 'auto',
													marginTop: 30,
													display: 'flex',
													justifyContent: 'flex-end',
												}}
											>
												<button className={Styles.buttonReset}>Reset</button>
												<button
													className='btn-primary'
													style={{ width: 144, height: 48 }}
												>
													Terapkan
												</button>
											</div>
										</div>
									)}
								</div>

								<div>
									<div className='select'>
										<div className='text left'>Tampilkan</div>
										<select
											onChange={e => {
												setPostPerPage(parseInt(e.target.value));
												setCurrentPage(1);
											}}
											defaultValue={postsPerPage}
											// onChange={e => setSelect(e.target.value)}
											className='select-dashboard'
										>
											<option value={5}>5</option>
											<option value={10}>10</option>
											<option value={25}>25</option>
											<option value={50}>50</option>
											<option value={100}>100</option>
										</select>

										<div className='text right'>hasil per halaman</div>
									</div>
								</div>
							</div>
						</div>
						{isLoading ? (
							<div
								style={{
									marginLeft: 'auto',
									marginRight: 'auto',
									textAlign: 'center',
								}}
							>
								<LoadingDot />
							</div>
						) : (
							<div className='flex-row-style' style={{ height: '100%' }}>
								<div style={{ width: '70%' }}>
									<table style={{ width: '100%' }} className={TableStyles.table}>
										<thead>
											<tr>
												{titleTable.map((item, index) => (
													<th className={TableStyles.th} key={index}>
														<div className='flex-row-style'>
															<div>{item.name}</div>
															{item.iconSort ? (
																<div className={TableStyles.iconSort}>
																	<ArrowUpD active={false} />
																	<ArrowDownD active={false} />
																</div>
															) : (
																''
															)}
														</div>
													</th>
												))}
											</tr>
										</thead>
										<tbody>
											{posts
												.slice(indexOfFirstPost, indexOfLastPost)
												.map((value, index) => {
													// expandableRowRefs.push(React.createRef());
													return (
														<tr>
															<td>{value.orderManagementID}</td>
															<td>
																{activeTab === 'IMPORT'
																	? value.blNo
																	: value.shippingInvoiceID}
															</td>
															{activeTab === 'IMPORT' && <td>{value.sppb}</td>}
															<td>{value.deliveryOrderID}</td>
															<td>{value.customer}</td>
														</tr>
													);
												})}
										</tbody>
									</table>
								</div>
								<div style={{ width: '30%', overflowX: 'auto' }}>
									<table className={TableStyles.table} style={{ width: '600px' }}>
										<thead>
											<tr>
												{titleTable2.map((item, index) => (
													<th className={TableStyles.th} key={index}>
														<div className='flex-row-style'>
															<div>{item.name}</div>
															{item.iconSort ? (
																<div className={TableStyles.iconSort}>
																	<ArrowUpD active={false} />
																	<ArrowDownD active={false} />
																</div>
															) : (
																''
															)}
														</div>
													</th>
												))}
											</tr>
										</thead>
										<tbody>
											{posts
												.slice(indexOfFirstPost, indexOfLastPost)
												.map((value, index) => {
													// expandableRowRefs.push(React.createRef());
													return (
														<tr>
															<td>{value.orderStuffingDate}</td>
															<td>{''}</td>
															<td>
																<div>
																	{value.destinationName1} <ArrowRightTable />{' '}
																	{value.destinationName3}
																</div>
															</td>
															<td>
																<button
																	className={`${Styles.statusBox} ${Styles.status2}`}
																>
																	{getStatus(value.vendorStatus)}
																</button>
															</td>
															<td>
																<div
																	className='flex-row-style'
																	style={{ justifyContent: 'space-between' }}
																>
																	<div
																		onClick={() =>
																			onOrderDetailClick(
																				value.vendorOrderID,
																				activeTab,
																				// value.orderManagementDetailID
																				value
																			)
																		}
																	>
																		<EyeIcon color={'blue'} />
																	</div>
																	<div
																		onClick={() =>
																			onAssignClick(
																				value.vendorOrderID,
																				value.orderManagementDetailID
																			)
																		}
																	>
																		<WheelIcon />
																	</div>
																</div>
															</td>
														</tr>
													);
												})}
										</tbody>
									</table>
								</div>
							</div>
						)}
						<Pagination
							totalRecords={posts.length}
							pageLimit={postsPerPage}
							pageNeighbours={1}
							currentPage={currentPage}
							handleClick={handleClick}
							handleMoveLeft={handleMoveLeft}
							handleMoveRight={handleMoveRight}
						/>
					</div>
				) : (
					<TruckOrderDetail
						getVendorStatus={getStatus}
						orderData={orderData}
						onClickModal={() => setModal('order')}
						vendorReducer={vendorReducer}
						type={activeTab}
					/>
				)}
			</div>
			<AssignDriverExport
				assignDriver={assignDriver}
				drivers={drivers}
				vehicles={vehicles}
				containers={containers}
				show={modal === 'assignDriverEXPORT'}
				onClose={() => setModal('')}
				onShow={() => setModal('assignDriverEXPORT')}
			/>
			<AssignDriverImport
				assignDriver={assignDriver}
				drivers={drivers}
				vehicles={vehicles}
				containers={containers}
				show={modal === 'assignDriverIMPORT'}
				onClose={() => setModal('')}
				onShow={() => setModal('assignDriverIMPORT')}
			/>
			<OrderModal
				show={modal === 'order'}
				onClose={() => setModal('')}
				onShow={() => setModal('order')}
			/>
			<RuteModal
				show={modal === 'rute'}
				onClose={() => setModal('')}
				onShow={() => setModal('rute')}
			/>
		</LayoutDashboard>
	);
}
