import React, { Component, useState } from 'react';
import GlobalStyles from 'assets/scss/globals/layout.module.scss';
import Styles from 'assets/scss/dashboard/addons.module.scss';
import {
	TextXXL,
	TextXL,
	TextS,
	TextM,
	TextXXS,
	TextL,
} from 'components/text';
import { ButtonM, ButtonFull } from 'components/button';
import { flagId, longArrowRight } from 'assets/images';
import { Colors } from 'configs';
import CheckBoxM from 'components/checkBox/CheckBoxM';

import MapModal from 'components/container/freightOrder/Modal/mapModal/mapModal';
import RuteModal from 'components/container/freightOrder/Modal/ruteModal/ruteModal';
import PetugasHandlingModal from 'components/container/freightOrder/Modal/petugasHandlingModal/handlingModal';

import { autocompletePlace, getPlaceId } from 'services/apiGoogleMap';
import delayInput from 'utils/delayInput';

const PortDetail = ({ name }) => {
	return (
		<div className={Styles.portDetailItem}>
			<img src={flagId} className={Styles.portFlag} alt='not found' />
			<div className={Styles.detail}>
				<TextM textStyle='bold' className={Styles.portName}>
					{name}
				</TextM>
			</div>
		</div>
	);
};

const BookingSummary = () => {
	return (
		<div className={Styles.bookingSummary}>
			<div className={Styles.head}>
				<TextXL textStyle='bold'>Ringkasan</TextXL>
			</div>
			<div className={Styles.content}>
				<TextS color={Colors.text.grey1} className={Styles.title}>
					Rute Pengiriman
				</TextS>
				<PortDetail name='Tanjung Priok, Jakarta, Indonesia' />
				<i className={`icon-arrow-down-tail ${Styles.iconArrowDown}`} />
				<PortDetail name='Singapore Port, Singapore, Singapore' />

				<div className={Styles.summaryItem}>
					<TextS color={Colors.text.grey1} className={Styles.title}>
						Waktu Pengiriman
					</TextS>
					<TextM textStyle='bold'>25 Januari 2020- 30 Januari 2020</TextM>
				</div>

				<div className={Styles.summaryItem}>
					<TextS color={Colors.text.grey1} className={Styles.title}>
						Jenis Pengiriman
					</TextS>
					<TextM textStyle='bold'>Full Load Container</TextM>
				</div>

				<div className={Styles.summaryItem}>
					<TextS color={Colors.text.grey1} className={Styles.title}>
						Jumlah Container
					</TextS>
					<TextM textStyle='bold' className={GlobalStyles.mt5}>
						20' General Purpose x4
					</TextM>
					<TextM textStyle='bold' className={GlobalStyles.mt5}>
						40' General Purpose x4
					</TextM>
					<TextM textStyle='bold' className={GlobalStyles.mt5}>
						40' High Cube x1
					</TextM>
				</div>
			</div>
		</div>
	);
};

const AddonCard = props => {
	const onCheckedHandler = e => {
		setActive(!active);
	};

	const [active, setActive] = useState(false);
	const {
		name,
		isRecommended,
		startingPrice,
		description,
		isChecked,
		isTruck,
		onClick,
	} = props;
	return (
		<div className={`${Styles.addonCard} ${active ? Styles.active : ''}`}>
			<div className={Styles.top}>
				<div
					className={Styles.headerLeft}
					onClick={name === 'Handling' ? () => onClick() : ''}
				>
					{/* <div className={Styles.checkbox}></div> */}
					<CheckBoxM onChange={onCheckedHandler} />
					<TextXXL textStyle='bold'>{name}</TextXXL>
					{isRecommended ? (
						<div className={Styles.recommended}>
							<TextS>Disarankan</TextS>
						</div>
					) : null}
				</div>
				<div className={Styles.price}>
					<TextXXS color={Colors.text.darkblue}>mulai dari</TextXXS>
					<TextL textStyle='bold' color={Colors.text.darkblue}>
						IDR {startingPrice}
					</TextL>
				</div>
			</div>
			{!isTruck ? (
				<div className={Styles.description}>
					<TextS color={Colors.text.grey1}>{description}</TextS>
				</div>
			) : (
				<div onClick={() => onClick()}>
					<ButtonFull className={Styles.pickupButton}>
						<TextM
							className={Styles.pickupButtonLabel}
							color={Colors.text.white}
						>
							Alamat Pick Up
						</TextM>
						<i className={`icon-arrow-right ${Styles.pickupButtonIcon}`}></i>
					</ButtonFull>
				</div>
			)}
		</div>
	);
};

const Addons = props => {
	const [truckVal, setTruckVal] = useState('');
	const [dataAutoComplete, setDataAutoComplete] = useState([]);
	const [selectVal, setSelectVal] = useState('');
	const [showModal, setShowModal] = useState('');
	// const [showRuteModal, setShowRuteModal] = useState(false);
	// const [showHandlingModal, setShowHandlingModal] = useState(false);

	const getPlaces = async val => {
		setTruckVal(val);
		if (val.length) {
			delayInput(async () => {
				const res = await autocompletePlace(val);
				console.log(res);
				setDataAutoComplete(res);
				// const placeDet = await getPlaceId(res[0].place_id);
				// console.log(placeDet);
			}, 1000);
		}
	};

	const setItem = async val => {
		setSelectVal(val.description);
		setTruckVal(val.description);
		setDataAutoComplete([]);
		// const coordinates = await getPlaceId(val.place_id);
		// console.log(coordinates);
		// console.log(val);
	};

	const ruteModalHandler = () => {
		if (selectVal) {
			setShowModal('rute');
		}
	};

	return (
		// <LayoutDashboard>
		<div className={Styles.container}>
			<div className={Styles.left}>
				<div className={Styles.row}>
					<AddonCard
						name='Trucking'
						startingPrice={10000}
						isRecommended
						isTruck
						description='Layanan bongkar muat mulai dari kapal hingga penyerahan ke pemilik barang'
						onClick={() => setShowModal('map')}
					/>
					<AddonCard
						name='Handling'
						startingPrice={10000}
						isRecommended
						description='Layanan bongkar muat mulai dari kapal hingga penyerahan ke pemilik barang'
						onClick={() => setShowModal('handling')}
					/>
				</div>
				<div className={Styles.row}>
					<AddonCard
						name='Asuransi Truck'
						startingPrice={10000}
						description='Memberikan jaminan untuk risiko yang mungkin terjadi pada barang selama transportasi darat dan laut'
					/>
					<AddonCard
						name='Titip Kontainer'
						startingPrice={10000}
						description='Layanan titip kontainer di Depo dekat Pelabuhan untuk menghindari keterlambatan pengiriman barang.'
					/>
				</div>
				<div className={Styles.row}>
					<AddonCard
						name='Fumigasi'
						startingPrice={10000}
						description='Untuk memastikan hama tidak masuk ke negara dan bebas dari penyakit'
					/>
					<AddonCard
						name='Karantina'
						startingPrice={10000}
						description='Layanan titip kontainer di Depo dekat Pelabuhan untuk menghindari keterlambatan pengiriman barang.'
					/>
				</div>
				<div className={Styles.row}>
					<AddonCard
						name='E-COO'
						startingPrice={10000}
						isRecommended
						description='Jasa Pengurusan Surat Keterangan Asal (SKA), Diperlukan jika Anda Melakukan Eksport Barang. '
					/>
					<AddonCard
						name='E-PEB'
						startingPrice={10000}
						isRecommended
						description='Jasa Pengurusan Surat Pemberitahuan Ekspor (PEB), Diperlukan jika Anda Melakukan Eksport Barang. '
					/>
				</div>
				<div className={Styles.row}>
					<AddonCard
						name='Asuransi'
						startingPrice={10000}
						description='Memberikan jaminan untuk risiko yang mungkin terjadi pada barang selama transportasi darat dan laut'
					/>
				</div>
			</div>
			<div className={Styles.right}>
				<BookingSummary />
				<ButtonFull
					className={Styles.buttonNext}
					// onClick={() =>
					// 	this.props.history.push('/dashboard/add/freight/summary')
					// }
					onClick={() => props.nextStep('ringkasan')}
				>
					<TextL color={Colors.text.white}>Berikutnya</TextL>
					<img src={longArrowRight} alt='' />
				</ButtonFull>
			</div>
			<MapModal
				show={showModal === 'map'}
				onClose={() => setShowModal('')}
				onShow={() => setShowModal('map')}
				setItem={setItem}
				getPlaces={getPlaces}
				truckVal={truckVal}
				dataAutoComplete={dataAutoComplete}
				nextStep={ruteModalHandler}
			/>
			<RuteModal
				show={showModal === 'rute'}
				onClose={() => setShowModal('')}
				onShow={() => setShowModal('rute')}
				address={selectVal}
			/>
			<PetugasHandlingModal
				show={showModal === 'handling'}
				onClose={() => setShowModal('')}
				onShow={() => setShowModal('handling')}
			/>
		</div>
		// </LayoutDashboard>
	);
};

export default Addons;
