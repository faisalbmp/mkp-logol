import React, { Component } from 'react';
import LayoutDashboard from 'components/dashboard/layout';
import { RoundedDuoTab } from 'components/tabs';
import GlobalStyles from 'assets/scss/globals/layout.module.scss';
import Styles from 'assets/scss/dashboard/completedata.module.scss';
import {
	TextXL,
	TextS,
	TextM,
	TextXS,
	TextL,
} from 'components/text';
import { ButtonM, ButtonFull } from 'components/button';
import {
	TextInput as Input,
	PhoneInput,
	TextArea,
} from 'components/input';
import { SelectBoxM } from 'components/select';
import { Colors } from 'configs';
import { flagId, longArrowRight } from 'assets/images';

const InputTitle = ({ title, isRequired }) => {
	return (
		<TextS className={Styles.inputTitle}>
			{title} {isRequired ? <b>*</b> : null}
		</TextS>
	);
};

const SelectInput = ({ label, isRequired }) => {
	return (
		<div className={Styles.selectInput}>
			<InputTitle title={label} isRequired={isRequired} />
			<div className={Styles.inputWrapper}>
				<SelectBoxM label='Bapak' />
			</div>
		</div>
	);
};

const TextInput = ({ label, isPhone, isRequired }) => {
	return (
		<div className={Styles.textInput}>
			<InputTitle title={label} isRequired={isRequired} />
			<div className={Styles.inputWrapper}>
				{!isPhone ? <Input placeholder={label} /> : <PhoneInput />}
			</div>
		</div>
	);
};

const TextAreaWrapper = ({ label, placeholder, isRequired }) => {
	return (
		<div className={Styles.textInput}>
			<InputTitle title={label} isRequired={isRequired} />
			<div className={Styles.inputWrapper}>
				<TextArea placeholder={placeholder} className={Styles.addressInput} />
			</div>
		</div>
	);
};

const DataPenerima = () => {
	return (
		<div className={Styles.consignee}>
			<TextXL textStyle='bold'>Data Penerima</TextXL>
			<div className={Styles.row}>
				<SelectInput label='Titel' isRequired />
				<TextInput label='Nama Lengkap' isRequired />
				<TextInput label='Email' isRequired />
			</div>
			<div className={Styles.row}>
				<TextInput label='Perusahaan' isRequired />
				<TextInput label='No Telepon' isPhone isRequired />
			</div>
			<div className={Styles.row}>
				<TextAreaWrapper
					label='Alamat'
					isRequired
					placeholder='eg. Jl. Pegangsaan Lama, Puri Anjasmoro, Singapore.'
				/>
				<TextInput label='Kode Pos' isRequired />
			</div>
		</div>
	);
};

const NotifyParty = () => {
	return (
		<div className={Styles.notifyParty}>
			<div className={Styles.left}>
				<TextXL textStyle='bold'>Tambah Notify Party</TextXL>
				<TextS className={GlobalStyles.mt10} color={Colors.text.grey1}>
					Kirim pemberitahuan pengiriman ke pihak lain.{' '}
				</TextS>
			</div>
			<ButtonM
				className={Styles.buttonAdd}
				textStyle='bold'
				color={Colors.text.white}
			>
				Tambah
			</ButtonM>
		</div>
	);
};

const TabPane = ({ data }) => {
	return (
		<div className={Styles.tabPane}>
			{data.map((d, index) => {
				return (
					<div
						className={`${Styles.tab} ${index === 0 ? Styles.active : ''}`}
						key={`tab-${d.id}`}
					>
						<TextS
							color={index === 0 ? Colors.text.white : Colors.text.darkblue}
						>
							{d.name.toUpperCase()}
						</TextS>
					</div>
				);
			})}
		</div>
	);
};

const SortToggle = () => {
	return (
		<div className={Styles.sortToggle}>
			<div className={Styles.sortUp} />
			<div className={Styles.sortDown} />
		</div>
	);
};

const ContainerTable = () => {
	const header = [
		'Nama Barang',
		'Jumlah Barang',
		'Netto',
		'Gross',
		'Nilai Barang',
		'',
	];

	const data = [
		{
			name: 'Coconut Bricket',
			qty: 10000,
			qtyUnit: 'Pallets',
			netto: 1000,
			nettoUnit: 'Kg',
			gross: 1500,
			grossUnit: 'Kg',
			value: 1000000,
		},
		{
			name: 'Coconut Bricket',
			qty: 10000,
			qtyUnit: 'Pallets',
			netto: 1000,
			nettoUnit: 'Kg',
			gross: 1500,
			grossUnit: 'Kg',
			value: 1000000,
		},
		{
			name: 'Coconut Bricket',
			qty: 10000,
			qtyUnit: 'Pallets',
			netto: 1000,
			nettoUnit: 'Kg',
			gross: 1500,
			grossUnit: 'Kg',
			value: 1000000,
		},
		{
			name: 'Coconut Bricket',
			qty: 10000,
			qtyUnit: 'Pallets',
			netto: 1000,
			nettoUnit: 'Kg',
			gross: 1500,
			grossUnit: 'Kg',
			value: 1000000,
		},
	];
	return (
		<table className={Styles.containerTable}>
			<thead>
				<tr>
					{header.map(head => {
						return (
							<td>
								<div className={Styles.header}>
									<TextS className={GlobalStyles.mr5}>{head}</TextS>
									{head !== '' ? <SortToggle /> : null}
								</div>
							</td>
						);
					})}
				</tr>
			</thead>
			<tbody>
				{data.map((d, index) => {
					const {
						name,
						qty,
						qtyUnit,
						netto,
						nettoUnit,
						gross,
						grossUnit,
						value,
					} = d;
					return (
						<tr>
							<td>
								<TextS>{name}</TextS>
							</td>
							<td>
								<div className={Styles.row}>
									<TextS className={GlobalStyles.mr5}>{qty}</TextS>{' '}
									<TextS color={Colors.text.grey1}>{qtyUnit}</TextS>
								</div>
							</td>
							<td>
								<div className={Styles.row}>
									<TextS className={GlobalStyles.mr5}>{netto}</TextS>{' '}
									<TextS color={Colors.text.grey1}>{nettoUnit}</TextS>
								</div>
							</td>
							<td>
								<div className={Styles.row}>
									<TextS className={GlobalStyles.mr5}>{gross}</TextS>{' '}
									<TextS color={Colors.text.grey1}>{grossUnit}</TextS>
								</div>
							</td>
							<td>
								<div className={Styles.row}>
									<TextS color={Colors.text.grey1} className={GlobalStyles.mr5}>
										IDR
									</TextS>
									<TextS>{value}</TextS>{' '}
								</div>
							</td>
							<td>
								{index !== data.length - 1 ? (
									<button className={Styles.removeButton}>
										<i className={`icon-minus ${Styles.iconMinus}`}></i>
									</button>
								) : (
									<button className={Styles.addButton}>
										<i className={`icon-plus ${Styles.iconMinus}`}></i>
									</button>
								)}
							</td>
						</tr>
					);
				})}
				<tr>
					<td>
						<Input
							placeholder='Nama Barang'
							className={Styles.input}
							fontSize={'12px'}
						/>
					</td>
					<td>
						<Input
							placeholder='qty'
							className={Styles.input}
							fontSize={'12px'}
						/>
					</td>
					<td>
						<Input
							placeholder='netto'
							className={Styles.input}
							fontSize={'12px'}
						/>
					</td>
					<td>
						<Input
							placeholder='gross'
							className={Styles.input}
							fontSize={'12px'}
						/>
					</td>
					<td>
						<Input
							placeholder='nilai barang'
							className={Styles.input}
							fontSize={'12px'}
						/>
					</td>
				</tr>
				<tr>
					<td colSpan={2}>
						<TextS textStyle='bold'>Total</TextS>
					</td>
					<td>
						<TextS textStyle='bold'>4000 kg</TextS>
					</td>
					<td>
						<TextS textStyle='bold'>6000 kg</TextS>
					</td>
					<td colSpan={2}>
						<TextS textStyle='bold'>IDR 4.000.000</TextS>
					</td>
				</tr>
			</tbody>
		</table>
	);
};

const ContainerDetails = () => {
	const containers = [
		{ id: 1, name: 'Kontainer 1' },
		{ id: 2, name: 'Kontainer 2' },
		{ id: 3, name: 'Kontainer 3' },
		{ id: 4, name: 'Kontainer 4' },
	];
	return (
		<div className={Styles.containerDetails}>
			<TabPane data={containers} />
			<ContainerTable />
		</div>
	);
};

const DataBarang = () => {
	return (
		<div className={Styles.dataBarang}>
			<div className={Styles.top}>
				<TextXL textStyle='bold'>Data Barang</TextXL>
				<div className={Styles.filters}>
					<div className={Styles.filterItem}>
						<SelectInput label='Jenis Kontainer' />
					</div>
					<div className={Styles.filterItem}>
						<SelectInput label='Jenis Barang Kiriman' />
					</div>
				</div>
			</div>

			<ContainerDetails />
			<div className={Styles.disclaimer}>
				<TextS className={GlobalStyles.mr5}>Maksimum 18000 kg (18 ton)</TextS>
				<div className={Styles.tooltip}>
					<TextXS color={Colors.text.white}>?</TextXS>
				</div>
			</div>
		</div>
	);
};

const PortDetail = ({ name }) => {
	return (
		<div className={Styles.portDetailItem}>
			<img src={flagId} className={Styles.portFlag} />
			<div className={Styles.detail}>
				<TextM textStyle='bold' className={Styles.portName}>
					{name}
				</TextM>
			</div>
		</div>
	);
};

const BookingSummary = () => {
	return (
		<div className={Styles.bookingSummary}>
			<div className={Styles.head}>
				<TextXL textStyle='bold'>Ringkasan</TextXL>
			</div>
			<div className={Styles.content}>
				<TextS color={Colors.text.grey1} className={Styles.title}>
					Rute Pengiriman
				</TextS>
				<PortDetail name='Tanjung Priok, Jakarta, Indonesia' />
				<i className={`icon-arrow-down-tail ${Styles.iconArrowDown}`} />
				<PortDetail name='Singapore Port, Singapore, Singapore' />

				<div className={Styles.summaryItem}>
					<TextS color={Colors.text.grey1} className={Styles.title}>
						Waktu Pengiriman
					</TextS>
					<TextM textStyle='bold'>25 Januari 2020- 30 Januari 2020</TextM>
				</div>

				<div className={Styles.summaryItem}>
					<TextS color={Colors.text.grey1} className={Styles.title}>
						Jenis Pengiriman
					</TextS>
					<TextM textStyle='bold'>Full Load Container</TextM>
				</div>

				<div className={Styles.summaryItem}>
					<TextS color={Colors.text.grey1} className={Styles.title}>
						Jumlah Container
					</TextS>
					<TextM textStyle='bold' className={GlobalStyles.mt5}>
						20' General Purpose x4
					</TextM>
					<TextM textStyle='bold' className={GlobalStyles.mt5}>
						40' General Purpose x4
					</TextM>
					<TextM textStyle='bold' className={GlobalStyles.mt5}>
						40' High Cube x1
					</TextM>
				</div>
			</div>
		</div>
	);
};

const UploadDocument = ({ label, isRequired }) => {
	return (
		<div className={Styles.textInput}>
			<InputTitle title={label} isRequired={isRequired} />
			<div className={Styles.inputWrapper}>
				<ButtonM className={Styles.buttonUpload}>
					<i className={`icon-upload ${Styles.iconUpload}`}></i>
					<TextM>Upload Dokumen</TextM>
				</ButtonM>
			</div>
		</div>
	);
};

const DokumenWajib = () => {
	return (
		<div className={Styles.dokumenWajib}>
			<TextXL textStyle='bold'>Dokumen Wajib</TextXL>
			<TextS className={GlobalStyles.mt5}>
				Isi dan upload dokumen pengiriman
			</TextS>
			<div className={Styles.row}>
				<div className={Styles.doneIndicator}>
					<i className={`icon-ok ${Styles.iconCheck}`}></i>
				</div>
				<TextM className={Styles.rowTitle}>Shipping Instructions :</TextM>
				<TextInput label='Nomor' isRequired />
				<div className={GlobalStyles.ml24}>
					<UploadDocument label='Dokumen' />
				</div>
			</div>
		</div>
	);
};

export default class CompleteData extends Component {
	constructor(props) {
		super(props);
		this.state = {
			activeTab: 0,
		};
	}
	render() {
		const { activeTab } = this.state;
		return (
			// <LayoutDashboard>
			<React.Fragment>
				<RoundedDuoTab
					data={['Buat Shipping Instruction', 'Unggah Shipping Instruction']}
					activeTab={activeTab}
					onSelect={index => this.setState({ activeTab: index })}
				/>
				<div className={Styles.container}>
					<div className={Styles.left}>
						{activeTab === 0 ? (
							<>
								<DataPenerima />
								<NotifyParty />
								<DataBarang />
							</>
						) : (
							<DokumenWajib />
						)}
					</div>
					<div className={Styles.right}>
						<BookingSummary />
						<ButtonFull
							className={Styles.buttonNext}
							// onClick={() =>
							// 	this.props.history.push('/dashboard/add/freight/addons')
							// }
							onClick={() => this.props.nextStep('tambahan')}
						>
							<TextL color={Colors.text.white}>Berikutnya</TextL>
							<img src={longArrowRight} alt='' />
						</ButtonFull>
					</div>
				</div>
			</React.Fragment>
			// {/* // </LayoutDashboard> */}
		);
	}
}
