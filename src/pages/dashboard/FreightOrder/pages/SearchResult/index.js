import React, { Component } from 'react';
import Styles from 'assets/scss/dashboard/searchresult.module.scss';
import { BookingMenu } from 'components/pickers';
import { ResultCardDashboard } from 'components/cards';
import { SearchFilter } from 'components/filters';
import { BookingPagination } from 'components/paginations';
import Modal from 'components/modal/wrepperModal';

export default class SearchResult extends Component {
	constructor(props) {
		super(props);
		this.state = {
			results: ['', '', '', '', '', ''],
			modal: 'alamat',
		};
	}
	onCloseModal() {
		this.setState({ modal: '' });
	}

	onShowModal() {
		this.setState({ modal: 'alamat' });
	}

	render() {
		const { results } = this.state;
		// console.log(this.props.history);
		return (
			<div className={Styles.container}>
				<div className={Styles.bookingMenu}>
					<BookingMenu booking={this.props.booking} />
				</div>
				<div className={Styles.searchFilter}>
					<SearchFilter />
				</div>
				<div className={Styles.result}>
					<div className={Styles.resultContent}>
						{results.map((result, index) => (
							<ResultCardDashboard
								// onChoose={() =>
								// 	this.props.history.push('/dashboard/add/freight/completedata')
								// }
								key={index}
								onChoose={() => this.props.nextStep('lengkapiData')}
							/>
						))}
					</div>
					<div className={Styles.pagination}>
						<BookingPagination />
					</div>
				</div>
				{/* <Modal show={true} onClose={this.onCloseModal}>
					<h3>Hello world</h3>
				</Modal> */}
			</div>
		);
	}
}
