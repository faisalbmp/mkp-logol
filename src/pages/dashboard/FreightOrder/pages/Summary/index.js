import React, { Component, useState } from 'react';
// import LayoutDashboard from 'components/dashboard/layout';
import Styles from 'assets/scss/dashboard/summary.module.scss';
import { TextXL, TextM, TextS } from 'components/text';
import { ButtonFull } from 'components/button';
import { flagId, arrowDown, download, print } from 'assets/images';
import { Colors } from 'configs';

import {
	EditIcon,
	EyeIcon,
	DownloadIcon,
} from 'components/container/freightOrder/icon/icon';

import ConfirmationModal from 'components/container/freightOrder/Modal/confirmationModal/confirmationModal';

const WarningCard = () => {
	return (
		<div className={Styles.warning}>
			<i className={`icon-warning ${Styles.iconWarning}`} />
			<TextXL>Cek kembali data pesanan Anda</TextXL>
		</div>
	);
};

const StakeHolder = props => {
	const {
		type,
		company,
		companyAddress,
		userPhoneNumber,
		userName,
		userEmail,
	} = props;
	return (
		<div className={Styles.stakeholder}>
			<TextXL textStyle='bold' className={Styles.stakeholderType}>
				{type}
			</TextXL>
			<TextM textStyle='bold'>{company}</TextM>
			<TextM className={Styles.address}>{companyAddress}</TextM>

			<div className={Styles.userWrapper}>
				<TextM textStyle='bold'>{userName}</TextM>
				<div className={Styles.userData}>
					<TextM className={Styles.phoneNumber}>{userPhoneNumber}</TextM>
					<TextM>{userEmail}</TextM>
				</div>
			</div>
		</div>
	);
};

const ShippingInstruction = props => {
	return (
		<div className={Styles.shippingIns}>
			<div style={{ display: 'flex', justifyContent: 'space-between' }}>
				<TextXL textStyle='bold'>Shipping Instruction</TextXL>
				<EditIcon />
			</div>
			<div
				style={{
					display: 'flex',
					justifyContent: 'space-between',
					marginTop: 40,
					width: '95%',
				}}
			>
				<TextM textStyle='bold'>Shipping Instruction</TextM>
				<TextM textStyle='bold'>No. 12122333112</TextM>
				<TextM textStyle='bold'>SI-12122333112.PDF</TextM>
				<div
					style={{
						display: 'flex',
						justifyContent: 'space-around',
						width: 118,
					}}
				>
					<EyeIcon />
					<DownloadIcon />
				</div>
			</div>
		</div>
	);
};

const PortDetail = ({ name }) => {
	return (
		<div className={Styles.portDetailItem}>
			<img src={flagId} className={Styles.portFlag} alt='Not Found' />
			<div className={Styles.detail}>
				<TextM textStyle='bold' className={Styles.portName}>
					{name}
				</TextM>
			</div>
		</div>
	);
};

const FreightRoute = () => {
	return (
		<div className={Styles.summaryItem}>
			<TextS className={Styles.infoTitle}>Rute Pengiriman</TextS>
			<PortDetail name='Tanjung Priok, Jakarta, Indonesia' />
			<i className={`icon-arrow-down-tail ${Styles.iconArrowDown}`} />
			<PortDetail name='Singapore Port, Singapore, Singapore' />
		</div>
	);
};

const Estimation = () => {
	return (
		<div className={Styles.summaryItem}>
			<TextS color={Colors.text.grey1} className={Styles.infoTitle}>
				Waktu Pengiriman (ETD-ETA)
			</TextS>
			<TextM textStyle='bold' className={Styles.etaEtd}>
				25 Januari 2020 - 30 Januari 2020
			</TextM>
		</div>
	);
};

const JenisPengiriman = () => {
	return (
		<div className={Styles.summaryItem}>
			<TextS color={Colors.text.grey1} className={Styles.infoTitle}>
				Jenis Pengiriman
			</TextS>
			<TextM textStyle='bold' className={Styles.etaEtd}>
				Full Container Load
			</TextM>
		</div>
	);
};

const TotalContainer = () => {
	return (
		<div className={Styles.summaryItem}>
			<TextS color={Colors.text.grey1} className={Styles.infoTitle}>
				Jumlah Kontainer
			</TextS>
			<TextM textStyle='bold' className={Styles.ctnQty}>
				20' General Purpose x4
			</TextM>
			<TextM textStyle='bold' className={Styles.ctnQty}>
				40' General Purpose x4
			</TextM>
			<TextM textStyle='bold' className={Styles.ctnQty}>
				40' High Cube x1
			</TextM>
		</div>
	);
};

const InvoiceSummary = () => {
	return (
		<div className={Styles.invoiceSummary}>
			<div className={Styles.summaryHeader}>
				<div className={Styles.summaryItem}>
					<TextS color={Colors.text.grey1} className={Styles.infoTitle}>
						No. Pesanan
					</TextS>
					<TextM textStyle='bold' className={Styles.etaEtd}>
						0004112513
					</TextM>
				</div>
				<div className={Styles.summaryItem}>
					<TextS color={Colors.text.grey1} className={Styles.infoTitle}>
						No. Tagihan
					</TextS>
					<TextM textStyle='bold' className={Styles.etaEtd}>
						INV-0004112513
					</TextM>
				</div>
			</div>

			<div className={Styles.summaryBody}>
				<TextXL textStyle='bold' className={Styles.bodyTitle}>
					Rincian Biaya
				</TextXL>
				<div className={Styles.costItem}>
					<TextM className={Styles.costItemValue}>20’ General Purpose</TextM>
					<TextM className={Styles.costItemValue}>IDR 150.000</TextM>
					<TextM className={Styles.costItemValue}>x3</TextM>
				</div>
				<div className={Styles.costItem}>
					<TextM className={Styles.costItemValue}>20’ General Purpose</TextM>
					<TextM className={Styles.costItemValue}>IDR 150.000</TextM>
					<TextM className={Styles.costItemValue}>x3</TextM>
				</div>
				<div className={Styles.costItem}>
					<TextM className={Styles.costItemValue}>20’ General Purpose</TextM>
					<TextM className={Styles.costItemValue}>IDR 150.000</TextM>
					<TextM className={Styles.costItemValue}>x3</TextM>
				</div>

				<div className={Styles.toggleOthers}>
					<TextM className={Styles.toggleLabel}>Lihat Lainnya</TextM>
					<div className={Styles.line}></div>
					<img src={arrowDown} className={Styles.portFlag} alt='Not Found' />
				</div>
			</div>
		</div>
	);
};

const SummaryAction = () => {
	return (
		<div className={Styles.summaryActions}>
			<button className={Styles.actionButton}>
				<img src={download} className={Styles.portFlag} alt='Not Found' />
				<TextM className={Styles.buttonLabel}>Unduh</TextM>
			</button>
			<button className={Styles.actionButton}>
				<img src={print} className={Styles.portFlag} alt='Not Found' />
				<TextM className={Styles.buttonLabel}>Cetak</TextM>
			</button>
		</div>
	);
};

const Summary = props => {
	const [sender, setSender] = useState({
		company: 'PT. Maju Sejahtera Mantab',
		companyAddress:
			'Jl. Kaliurang Selatan, Jakarta Utara, 12122, Jakarta Indonesia ',
		userName: 'Fahmi Hikmawan',
		userPhoneNumber: '+62-6716628911',
		userEmail: 'Fahmi@Hikmawan.com',
	});

	const [consignee, setConsignee] = useState({
		company: 'Ahmad Sobirin Ltd.',
		companyAddress:
			'Jl. Pegangsaan Lama, Puri Anjasmoro, 12122, Singapore Singapore ',
		userName: 'Ahmad Sobirin ',
		userPhoneNumber: '+971-200199200',
		userEmail: 'Ahmad@Sobirin.com',
	});

	const [isShippingIns, setIsShippingIns] = useState(false);
	const [showModal, setShowModal] = useState('');

	return (
		// <LayoutDashboard>
		<div className={Styles.container}>
			<div className={Styles.left}>
				<WarningCard />
				<div className={Styles.summaryCard}>
					{isShippingIns ? (
						<ShippingInstruction />
					) : (
						<div className={Styles.mainInfo}>
							<StakeHolder type='Pengirim' {...sender} />
							<StakeHolder type='Penerima' {...consignee} />
							<EditIcon />
						</div>
					)}
					<div className={Styles.detailInfo}>
						<div style={{ display: 'flex', justifyContent: 'space-between' }}>
							<TextXL textStyle='bold'>Rincian Pesanan</TextXL>
							<EditIcon />
						</div>
						<div className={Styles.detailInfoContent}>
							<div className={Styles.contentLeft}>
								<FreightRoute />
								<Estimation />
							</div>
							<div className={Styles.contentRight}>
								<JenisPengiriman />
								<TotalContainer />
							</div>
						</div>
					</div>
					<div className={Styles.detailInfo}>
						<div style={{ display: 'flex', justifyContent: 'space-between' }}>
							<TextXL textStyle='bold'>Layanan Tambahan</TextXL>
							<EditIcon />
						</div>

						<div className={Styles.addons}>
							<div className={Styles.addonItem}>
								<TextM className={Styles.addonItemLabel}>E-PEB</TextM>
							</div>
							<div className={Styles.addonItem}>
								<TextM className={Styles.addonItemLabel}>Asuransi Laut</TextM>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div className={Styles.right}>
				<InvoiceSummary />
				<SummaryAction />
				<ButtonFull
					textStyle='bold'
					className={Styles.submitButton}
					color='#fff'
					onClick={() => setShowModal('confirmation')}
				>
					<TextXL textStyle='bold' color='#fff'>
						Proses Pesanan
					</TextXL>
				</ButtonFull>
				<ButtonFull
					className={Styles.saveButton}
					onClick={() =>
						props.history.push('/dashboard/add/freight/bookingsuccess')
					}
					// onClick={() => this.props.nextStep('success')}
				>
					<TextXL textStyle='bold'>Simpan</TextXL>
				</ButtonFull>
			</div>
			<ConfirmationModal
				show={showModal === 'confirmation'}
				onClose={() => setShowModal('')}
				onShow={() => setShowModal('confirmation')}
				history={props.history}
			/>
		</div>
		// </LayoutDashboard>
	);
};

export default Summary;
