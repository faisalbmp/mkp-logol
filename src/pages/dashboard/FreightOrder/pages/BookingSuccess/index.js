import React from "react";
import { TextXXXL, TextXL, TextM } from "components/text";
import { ButtonFull } from "components/button";
import iconLogol from "assets/images/svg/logo-logol.svg";
import { phone, mail } from "assets/images";
import Styles from "assets/scss/dashboard/bookingsuccess.module.scss";

const InfoItem = (props) => {
  const { icon, label } = props;
  return (
    <div className={Styles.infoItem}>
      <img src={icon} alt="" />
      <TextM className={Styles.label} textStyle="bold">
        {label}
      </TextM>
    </div>
  );
};

const BookingSuccess = (props) => {
  return (
    <div className={Styles.bookingSuccess}>
      <div className={Styles.left}>
        <img src={iconLogol} alt="" className={Styles.logo} />
        <TextXXXL textStyle="bold" className={Styles.title}>
          Pesanan Sudah Diterima
        </TextXXXL>
        <TextXL className={Styles.subtitle}>
          Nomor Pesanan Anda <b>1212111</b>
        </TextXL>
        <TextXL className={Styles.info}>
          Tim kami akan menghubungi dalam waktu 24 jam untuk mengkonfirmasi
          pesanan anda
        </TextXL>
        <ButtonFull
          className={Styles.buttonBack}
          onClick={() => props.history.replace("/dashboard/list/freight")}
        >
          <TextM color="#fff" textStyle="bold">
            Kembali ke Pesanan Saya
          </TextM>
        </ButtonFull>

        <TextM className={Styles.moreInfo}>
          Untuk layanan dan informasi lebih lanjut bisa menghubungi kami:
        </TextM>
        <InfoItem icon={phone} label="+62 21 2452 3147" />
        <InfoItem icon={mail} label="Info@logol.co.id" />
      </div>
      <div className={Styles.right}></div>
    </div>
  );
};

export default BookingSuccess;
