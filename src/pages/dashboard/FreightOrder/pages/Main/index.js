import React, { Component, useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { MegaMenu } from 'components/pickers';
import LayoutDashboard from 'components/dashboard/layout';
import Styles from 'assets/scss/dashboard/freightorder.module.scss';
import { TextL } from 'components/text';
import { Colors } from 'configs';
import { arrowRightButton, shipFront } from 'assets/images';
import { setTypeBooking } from 'actions';
import { useSelector, useDispatch } from 'react-redux';
import BreadCrumb from 'components/dashboard/breadcrumb';
import SteppedProgressBar from 'components/SteppedProgressBar';
import {
	Timer,
	EditIcon2,
	DetailIcon,
	TruckIcon,
	BookIcon,
} from 'components/container/freightOrder/icon/icon';

import SearchResult from '../SearchResult';
import CompleteData from '../CompleteData';
import Addons from '../Addons';
import Summary from '../Summary';

const Main = props => {
	const BookType = 'freight';
	const { booking } = useSelector(state => state);

	// const dispatch = useDispatch();

	// const onSetBookType = () => dispatch(setTypeBooking('freight'));
	// console.log(selected_origin);

	// useEffect(() => {
	// 	onSetBookType();
	// }, []);

	const [page, setPage] = useState('');
	const [jadwal, setJadwal] = useState('active');
	const [lengkapiData, setLengkapiData] = useState('pending');
	const [muatBarang, setMuatBarang] = useState('pending');
	const [tambahan, setTambahan] = useState('pending');
	const [ringkasan, setRingkasan] = useState('pending');
	const history = useHistory();
	console.log(history);
	const steps = [
		{
			id: 1,
			icon: <Timer color={jadwal === 'active' ? '#004DFF' : '#B8C1C6'} />,
			label: 'Tentukan Jadwal',
			status: jadwal,
		},
		{
			id: 2,
			icon: (
				<DetailIcon color={muatBarang === 'active' ? '#004DFF' : '#B8C1C6'} />
			),
			label: 'Detail Container',
			status: muatBarang,
		},
		{
			id: 3,
			icon: (
				<EditIcon2 color={lengkapiData === 'active' ? '#004DFF' : '#B8C1C6'} />
			),
			label: 'Lengkapi Data',
			status: lengkapiData,
		},
		{
			id: 4,
			icon: (
				<TruckIcon
					color={tambahan === 'active' ? '#004DFF' : '#B8C1C6'}
					plusColor={tambahan === 'active' ? '#004DFF' : '#B8C1C6'}
				/>
			),
			label: 'Tambahan',
			status: tambahan,
		},
		{
			id: 5,
			icon: <BookIcon color={ringkasan === 'active' ? '#004DFF' : '#B8C1C6'} />,
			label: 'Ringkasan',
			status: ringkasan,
		},
	];

	const cariJadwal = () => {
		const {
			selected_origin,
			selected_destination,
			selectedDate,
			selectedContainer,
		} = booking;
		// nextStepHandler('detailContainer');

		if (
			selected_origin &&
			selected_destination &&
			selectedDate &&
			selectedContainer
		) {
			nextStepHandler('detailContainer');
		} else {
			alert('All field must not empty');
		}
	};

	let component = (
		<div className={Styles.container}>
			<div className={Styles.menuTitle}>
				<div className={Styles.icon}>
					<img src={shipFront} alt='' />
				</div>
				<TextL color={Colors.text.white} className={Styles.title}>
					Cari Jadwal Pelayaran
				</TextL>
			</div>
			<div className={Styles.megamenu}>
				<MegaMenu
					locationA={'Pelabuhan Asal'}
					locationB={'Pelabuhan Tujuan'}
					// typeBooking={'freight'}
					time={'Waktu Pengiriman'}
					container={'Jenis Pengiriman'}
				/>
			</div>
			<button
				className={Styles.searchButton}
				// onClick={() => props.history.push('/dashboard/add/freight/result')}
				onClick={() => cariJadwal()}
			>
				Cari Jadwal
				<img src={arrowRightButton} alt='arrow-right-button' />
			</button>
		</div>
	);

	const nextStepHandler = step => {
		// console.log(steps[1]);
		switch (step) {
			case 'cariJadwal':
				setJadwal('active');
				break;
			case 'detailContainer':
				setJadwal('done');
				setMuatBarang('active');
				break;
			case 'lengkapiData':
				setMuatBarang('done');
				setLengkapiData('active');
				break;
			case 'tambahan':
				setLengkapiData('done');
				setTambahan('active');
				break;
			case 'ringkasan':
				setTambahan('done');
				setRingkasan('active');
				break;
			case 'success':
				setRingkasan('done');
				break;
			default:
				break;
		}
		setPage(step);
	};

	if (page === 'detailContainer') {
		component = <SearchResult booking={booking} nextStep={nextStepHandler} />;
	}

	if (page === 'lengkapiData') {
		component = <CompleteData nextStep={nextStepHandler} />;
	}

	if (page === 'tambahan') {
		component = <Addons nextStep={nextStepHandler} />;
	}

	if (page === 'ringkasan') {
		component = <Summary history={history} nextStep={nextStepHandler} />;
	}

	return <LayoutDashboard hideSideMenu={true} freightSteps={steps}>{component}</LayoutDashboard>;
};

export default Main;
