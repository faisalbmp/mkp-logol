import React from 'react';
import { useHistory } from 'react-router-dom';

import LayoutDashboard from '../../../components/dashboard/layout';
import {
	MoneyIcon,
	ArrowDownD,
	ArrowUpD,
	ArrowRightTable,
	EyeIcon,
	CopyImg,
} from '../../../components/icon/iconSvg';

import freightMainDashboard from 'assets/images/svg/freight-main-dashboard.svg';
import truckMainDashboard from 'assets/images/svg/truck-main-dashboard.svg';
import docMainDashboard from 'assets/images/svg/doc-main-dashboard.svg';

import { withTranslation } from 'react-i18next';

function Dashboard({ t }) {
	const history = useHistory();

	const translate = t;

	const dataCard = [
		{
			title: 'totalFreightOrder',
			number: 56,
			icon: truckMainDashboard,
		},
		{
			title: 'totalTruckOrder',
			number: 64,
			icon: freightMainDashboard,
		},
		{
			title: 'totalDocumentInProgress',
			number: 560,
			icon: docMainDashboard,
		},
	];

	const titleTable = [
		{ name: 'noOrder', iconSort: true },
		{ name: 'servcies', iconSort: true },
		{ name: 'deliveryRoute', iconSort: false },
		{ name: 'status', iconSort: true },
		{ name: 'action', iconSort: false },
	];

	const dataTable = [
		{
			no: '0004112513',
			layanan: 'Kapal dan Truk',
			rute: ['Jakarta', 'Sydney'],
			status: { type: 'no-proses', name: 'Dalam Proses' },
			eye: true,
		},
		{
			no: '0004112513',
			layanan: 'Truk',
			rute: ['Cakung', 'Tj. Priok'],
			status: { type: 'no-proses', name: 'Dalam Proses' },
			eye: true,
		},
		{
			no: '0004112513',
			layanan: 'Kapal',
			rute: ['Jakarta', 'Osaka'],
			status: { type: 'cancel', name: 'Dibatalkan' },
			eye: true,
		},
		{
			no: '0004112513',
			layanan: 'Kapal dan Truk',
			rute: ['Jakarta', 'New Delhi'],
			status: { type: 'finish', name: 'Selesai' },
			eye: true,
		},
	];

	const dataTypeCustomer = [
		{ name: 'Customer', path: '/dashboard' },
		{ name: 'Trucking Vendor', path: '/vendor' },
	];

	return (
		<LayoutDashboard>
			<div className='top-content'>
				<div>
					<div className='title-home'> Hallo, John Doe!</div>
					<div className='title'>{translate('dashboardPage.greetings')}</div>
				</div>
				<div className='card-type-customer'>
					{dataTypeCustomer.map((item, index) => {
						return (
							<div
								className={`item-type-customer ${index === 0 && 'item-active'}`}
								onClick={() => history.push(item.path)}
							>
								{item.name}
							</div>
						);
					})}
					{/* <MoneyIcon />
                    <div className="wrepper-text">
                        <div className="text">{translate('dashboardPage.yourBalance')}</div>
                        <div className="text">IDR 105.000.000</div>
                    </div>
                    <div className="btn-topup">
                        Top Up
                        </div> */}
				</div>
			</div>

			<div className='list-card'>
				{dataCard.map((value, index) => (
					<div className='card-calculation' key={index}>
						{/* <div className="bg-1"></div>
                            <div className="bg-2"></div> */}
						<div className='flex'>
							<div className='wrepper-text'>
								<div className='number'>{value.number}</div>
								<div className='text-card'>
									{translate(`dashboardPage.${value.title}`)}
								</div>
							</div>
							<img src={value.icon} className='icon-img' alt='instagram-img' />
						</div>
					</div>
				))}
				<div className='card-calculation'>
					<div className='bg-1'></div>
					<div className='bg-2'></div>
					<div className='flex'>
						<div className='wrepper-text'>
							<div className='number'>{translate('dashboardPage.seeBill')}</div>
							<div className='text-card'>
								{translate('dashboardPage.amountOfTheUnpidBill')}{' '}
								<text className='text-bill'>20</text>
							</div>
						</div>
						<div className='btn-card-go'>Go</div>
					</div>
				</div>
			</div>

			<div className='bottom-content'>
				<div className='card-left'>
					<div className='grid-card'>
						<div className='title'>{translate('dashboardPage.recentOrders')}</div>
						<div style={{ cursor: 'pointer' }}>{translate('dashboardPage.showAll')}</div>
					</div>

					<div className='wrepper-table'>
						<table className='table'>
							<div className='tr'>
								{titleTable.map((value, index) => (
									<div key={index} className='td'>
										<div>{translate(`dashboardPage.${value.name}`)}</div>
										{value.iconSort ? (
											<div className='icon-sort'>
												<ArrowUpD active={false} />
												<ArrowDownD active={false} />
											</div>
										) : (
											''
										)}
									</div>
								))}
							</div>
							{dataTable.map((value, index) => (
								<div key={index} className='tr'>
									<div className='td no'>{value.no}</div>
									<div className='td'>{value.layanan}</div>
									<div className='td'>
										{value.rute[0]} <ArrowRightTable /> {value.rute[1]}
									</div>
									<div className='td'>
										<button className={'btn ' + value.status.type}>
											{value.status.name}
										</button>
									</div>
									<div className='td'>{value.eye ? <EyeIcon /> : ''}</div>
								</div>
							))}
						</table>
					</div>
				</div>
				<div className='card-right'>
					<div className='top-content'>
						<div className='title'>{translate('dashboardPage.titleFreeServices')}</div>
						<CopyImg />
					</div>
					<div className='text-content'>{translate('dashboardPage.subTitle')}</div>
					<button className='btn-daftar'>
						{translate('dashboardPage.buttonFreeServices')}
					</button>
				</div>
			</div>
		</LayoutDashboard>
	);
}

export default withTranslation()(Dashboard);
