import React, { useState } from 'react';
import Header from '../../../../components/dashboard/tagihanSaya/Header/header';
import MetodePembayaran from '../../../../components/dashboard/tagihanSaya/metodePembayaran/metodePembayaran';
import Payment from '../../../../components/dashboard/tagihanSaya/pembayaran/payment';
import Konfirmasi from '../../../../components/dashboard/tagihanSaya/konfirmasi/konfirmasi';
import Styles from 'assets/scss/dashboard/tagihanSaya/tagihanSaya.module.scss';
import { withTranslation } from 'react-i18next';

const Bayar = props => {
	const [page, setPage] = useState('');
	const [payStepOne, setPayStepOne] = useState('active');
	const [payStepTwo, setPayStepTwo] = useState('pending');
	const [payStepThree, setPayStepThree] = useState('pending');
	const { t } = props;

	const steps = [
		{ label: t('myBill.paymentMethod'), status: payStepOne },
		{ label: t('myBill.payment'), status: payStepTwo },
		{ label: t('myBill.uploadEvidance'), status: payStepThree },
	];

	const changePageHandler = page => {
		switch (page) {
			case '':
				setPayStepOne('active');
				setPayStepTwo('');
				break;
			case 'tf':
				setPayStepOne('done');
				setPayStepTwo('active');
				break;

			case 'konfirmasi':
				setPayStepTwo('done');
				setPayStepThree('active');
				break;

			case 'allDone':
				setPayStepThree('done');
				break;
			default:
				break;
		}
		setPage(page);
	};

	// const changePageHandler = (page, id) => {
	// 	steps.map(step => {
	// 		if (step.id === id) {
	// 			step.status = 'done';
	// 		}
	// 	});
	// 	setPage(page);
	// };

	let content = <MetodePembayaran translate={t} nextStep={changePageHandler} />;

	if (page === 'tf') {
		content = <Payment translate={t} nextStep={changePageHandler} />;
	}

	if (page === 'konfirmasi') {
		content = <Konfirmasi translate={t} nextStep={changePageHandler} />;
	}

	return (
		<div style={{ backgroundColor: '#fafafa', minHeight: 984 }}>
			<Header steps={steps} />
			<div className={Styles.content}>{content}</div>
		</div>
	);
};

export default withTranslation()(Bayar);
