import React from 'react';

import LayoutDashboard from '../../../components/dashboard/layout';
import SearchInput from '../../../components/dashboard/tagihanSaya/searchInput/searchInput';
import saldoBox from '../../../components/dashboard/tagihanSaya/svg/saldoBox.svg';
import payLater from '../../../components/dashboard/tagihanSaya/svg/payLater.svg';
import TopUp from '../../../components/dashboard/tagihanSaya/inputTopUp/topUp';
import InfoTagihan from '../../../components/dashboard/tagihanSaya/InfoTagihan';
import ArrowRight from '../../../components/dashboard/tagihanSaya/svg/arrowRightWhite.svg';
import {
	ArrowDown,
	SemuaTagihan,
	TagihanKapalIcon,
	TagihanTruckIcon,
	TagihanEDocumentIcon,
	TagihanJatuhTempiIcon,
} from '../../../components/icon/iconSvg';
import {
	content,
	flexContent,
	left,
	img,
	grid,
	gridItem,
	tagihanItem,
	btnCari,
	selectLayanan,
	inputText,
	right,
	boxSaldo,
	boxPayLater,
	innerBoxText,
} from 'assets/scss/dashboard/tagihanSaya/tagihanSaya.module.scss';
import { withTranslation } from 'react-i18next';

const TagihanSaya = props => {
	const { t } = props;
	const translate = t;

	const bayar = () => {
		props.history.push('/dashboard/bayarTagihan');
	};

	const listTagihan = [
		{ title: translate('myBill.shipBill'), icon: TagihanKapalIcon },
		{ title: translate('myBill.tuckBill'), icon: TagihanTruckIcon },
		{ title: translate('myBill.eDocBill'), icon: TagihanEDocumentIcon },
		{ title: translate('myBill.dueDate'), icon: TagihanJatuhTempiIcon },
	];

	const tagihanItems = listTagihan.map(tagihan => {
		return (
			<div className={gridItem}>
				<div className={tagihanItem}>
					{<tagihan.icon />}
					<p
						style={{
							marginLeft: -52,
							marginRight: -52,
							marginTop: 10,
						}}
					>
						{tagihan.title}
					</p>
				</div>
			</div>
		);
	});
	return (
		<LayoutDashboard isTrackInput={false}>
			<div className={content}>
				<div className={flexContent}>
					<div className={left}>
						<h2 style={{ marginBottom: 32 }}>{translate('myBill.title')}</h2>
						<div className={grid}>
							<div className={gridItem} style={{ width: 438, marginRight: 20 }}>
								<SearchInput />
							</div>
							<div style={{ marginRight: 20 }}>
								<div className={selectLayanan}>
									<p className={inputText}>{translate('myBill.allService')}</p>
									<div style={{ marginTop: 12, marginLeft: 20 }}>
										<ArrowDown />
									</div>
								</div>
							</div>
							<div style={{ marginRight: 20 }}>
								<button className={btnCari}>
									{translate('myBill.search')}
								</button>
							</div>
						</div>
						<div className={grid} style={{ marginTop: 32 }}>
							<div className={gridItem}>
								<div className={tagihanItem} style={{ background: '#004dff' }}>
									<SemuaTagihan />
									<p
										style={{
											color: ' #ffffff',
											marginLeft: -52,
											marginRight: -52,
											marginTop: 10,
										}}
									>
										{translate('myBill.allBills')}
									</p>
								</div>
							</div>
							{tagihanItems}
						</div>
						<InfoTagihan translate={translate} click={bayar} />
					</div>
					<div className={right}>
						<h2 style={{ marginBottom: 24 }}>{translate('myBill.wallet')}</h2>
						<div className={boxSaldo}>
							<img className={img} src={saldoBox} alt='not found' />
							<div className={flexContent}>
								<div className={innerBoxText}>
									<div style={{ fontWeight: 'bold', fontSize: 20 }}>
										{translate('myBill.myBalance')}
									</div>
									<div style={{ fontWeight: 800, fontSize: 25 }}>
										IDR 105.000.000
									</div>
								</div>
								<img
									style={{ marginLeft: 70 }}
									src={ArrowRight}
									alt='Not Found'
								/>
							</div>
						</div>
						<h2 style={{ paddingTop: 60, marginBottom: 40 }}>
							{translate('myBill.walletTopUp')}
						</h2>
						<TopUp />
						<h2 style={{ paddingTop: 60, marginBottom: 20 }}>
							{translate('myBill.payLater')}
						</h2>
						<div className={boxPayLater}>
							<img className={img} src={payLater} alt='not found' />
							<div className={flexContent}>
								<div className={innerBoxText}>
									<div style={{ fontWeight: 'bold', fontSize: 20 }}>
										{translate('myBill.availableLimits')}
									</div>
									<div style={{ fontWeight: 800, fontSize: 25 }}>
										IDR 50.000.000
									</div>
								</div>
								<img
									style={{ marginLeft: 80 }}
									src={ArrowRight}
									alt='Not Found'
								/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</LayoutDashboard>
	);
};

export default withTranslation()(TagihanSaya);
