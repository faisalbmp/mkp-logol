import React, { useState, useEffect } from 'react'

import LayoutDashboard from 'components/dashboard/layout'
import Breadcrumb from 'components/dashboard/breadcrumb'
import { ArrowDownD, ArrowUpD, ArrowRightPrimary, AddIcon, SortIcon, ArrowRightTable, EyeIcon, GatePassIcon, DropDownTableArrow } from 'components/icon/iconSvg'
import SearchInput from 'components/dashboard/searchInput'
import Select from 'components/dashboard/select'
import TruckOrderDetail from "components/container/truck/truckOrderDetail"
import Pagination from "components/paginations/pagination"
import { useDispatch, useSelector } from "react-redux"
import { getOrderListData } from "actions/TruckBookingList/getOrderListing"
import SubmitOrderModal from "components/button/submitOrderModal"
import { Redirect } from 'react-router-dom';

const titleTable = [
    { name: "" },
    { name: "No. Pesanan", iconSort: true, field : 'mkpBookingNo' },
    { name: "No. Booking Truk", iconSort: true, field : 'orderManagementID' },
    { name: "Delivery Order", iconSort: true, field : 'deliveryOrderID' },
    { name: "Tipe", iconSort: true, field : 'orderType' },
    { name: "Rute Pengiriman", iconSort: false, field : ''  },
    { name: "Status", iconSort: true, field : 'status'  },
    { name: "Aksi", iconSort: false, field : ''  },
]

const dataTable = [
    { noPesanan: "0004112513", noBookingTruk: "LG/202007/E0053", si: "SI/16/07/2020", do: "DO/16/07/2020", rute: ["Cakung", "Tj. Priok (JICT1)"], type: "export", status: { type: "no-proses", name: "Dalam Proses" }, eye: true }
]

export default function Truck(props) {
    const expandableRowRefs = []
    const [activeTab, setActiveTab] = useState("semua")
    // const [selectVal, setSelect] = useState(1)
    const [loadOrderDetail, setLoadOrderDetail] = useState(false)
    const [showExpendedView, setExpendedView] = useState(false)
    const [activeIndex, setActiveIndex] = useState(0)
    // const [posts, setPosts] = useState([]);
    // const [loading, setLoading] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [postsPerPage] = useState(5);
    const [showModal, setModalToggle] = useState()
    const [buatClicked, setBuatToggle] = useState(false)
    const [sortUpindex, setsortUpindex] = useState('')
    const [sortDownindex, setsortDownindex] = useState('')


    const [orderID, setOrderID] = useState('')

    const dispatch = useDispatch()

    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;
    const orderListData = useSelector(state => state.getOrderDetail.getTruckOrderDetail)
    const orderCreated = useSelector(state => state.newOrder.orderCreated)
    const newCustomerOrder = useSelector(state => state.newOrder.newCustomerOrder)

    useEffect(() => {
        dispatch(getOrderListData('INPROGRESS', 'orderManagementID', false, ''));
        setModalToggle(orderCreated)

    }, []);

    const onDalamClicked = () => {
        setActiveTab("semua")
        dispatch(getOrderListData('INPROGRESS', 'orderManagementID', false, ''));
    }
    const onRiwayatPressed = () => {
        setActiveTab("ekspor")
        dispatch(getOrderListData('HISTORY', 'orderManagementID', false, ''));
    }
    var currentPosts = [];
    if (orderListData.data != undefined) {
        currentPosts = currentPosts = orderListData.data.slice(indexOfFirstPost, indexOfLastPost);
    }

    const paginate = (pageNumber) => {
        setCurrentPage(pageNumber);
    }

    const paginatePrevious = () => {
        setCurrentPage(currentPage - 1)
    }

    const paginateNext = () => {
        setCurrentPage(currentPage + 1)
    }

    const expandView = (index) => {
        setExpendedView(!showExpendedView)
        setActiveIndex(index)
    }

    const onOrderDetailClick = (val) => {
        setOrderID(val.orderManagementID)
        setLoadOrderDetail(true)
    }

    const modalPressed = () => {
        setModalToggle(false)
    }

    const navigateToCreateOrder = () => {
        setBuatToggle(true)
    }

    const sortUpClick = (val, index) => {
        setsortUpindex(index)
        setsortDownindex('')
        var active = activeTab === "semua" ? 'INPROGRESS' : 'HISTORY'
        dispatch(getOrderListData(active, val, true, ''));
    }

    const sortDownClick = (val, index) => {
        setsortDownindex(index)
        var active = activeTab === "semua" ? 'INPROGRESS' : 'HISTORY'
        dispatch(getOrderListData(active, val, false, ''));
        setsortUpindex('')
    }

    const searchOrder = (text) => {
        var active = activeTab === "semua" ? 'INPROGRESS' : 'HISTORY'
        dispatch(getOrderListData(active, 'orderManagementID', false, text.target.value));
    }

    return (
        <LayoutDashboard>
            <div className="truck">
                {
                    !loadOrderDetail
                    ?
                    <div className="header-truk">
                        <Breadcrumb title="Truk" />
                        <div className="btn-dsb-right" onClick={navigateToCreateOrder}>
                            Buat Pesanan
                            <AddIcon />
                        </div>
                    </div> : null
                }
                {
                    !loadOrderDetail
                        ?
                        <div className="wrepper-table truck-table">
                            <div className="select-tab">
                                <div className={`tab ${activeTab === "semua" ? "tab-active" : ""}`} onClick={onDalamClicked}>Dalam Process</div>
                                <div className={`tab ${activeTab === "ekspor" ? "tab-active" : ""}`} onClick={onRiwayatPressed}>Riwayat</div>
                            </div>
                            <div className="middle-component">
                                <SearchInput onChange = {searchOrder} />
                                <div className="right">
                                    <div className="sort-item">
                                        <SortIcon />
                                        <div className="text">Urutkan</div>
                                    </div>

                                    <div>
                                        <Select
                                            number={100}
                                            // setSelect={setSelect}
                                            defaultValue={10}
                                        />
                                    </div>
                                </div>
                            </div>

                            <table className="table">
                                <tr className="tr">
                                    {
                                        titleTable.map((value, index) => (
                                            <td key={index} className="td">
                                                <div>{value.name}</div>
                                                {value.iconSort ? <div className="icon-sort">
                                                    <div onClick = {() => sortUpClick(value.field, index)} ><ArrowUpD active={sortUpindex === index ? true : false} /></div>
                                                    <div onClick = {() => sortDownClick(value.field, index)} ><ArrowDownD active={sortDownindex === index ? true : false} /></div>
                                                </div> : ""}
                                            </td>
                                        ))
                                    }
                                </tr>
                                {
                                    currentPosts.map((value, index) => {
                                        expandableRowRefs.push(React.createRef())
                                        var status = "" 
                                        if(value.status == "0") {
                                            status = "Assigned"
                                        } else if(value.status == "1") {
                                            status = "Receive "
                                        } else if(value.status == "2") {
                                            status = "Destination 1 In"
                                        } else if(value.status == "3") {
                                            status = "Destination 1 Out"
                                        } else if(value.status == "4") {
                                            status = "Destination 2 In"
                                        }  else if(value.status == "5") {
                                            status = "Destination 2 Out"
                                        }  else if(value.status == "6") {
                                            status = "Destination 3 In"
                                        }  else if(value.status == "7") {
                                            status = "Destination 3 Out"
                                        }  else if(value.status == "8") {
                                            status = "Delivered"
                                        }  else if(value.status == "9") {
                                            status = "Reject"
                                        } else if(value.status == "10") {
                                            status = "Cancelled"
                                        } else if(value.status == "11") {
                                            status = "Export Limit"
                                        }
                                        return (
                                            <div className="tr-expandable">

                                                <div
                                                    style=
                                                    {{
                                                        display: 'flex',
                                                        width: "100%",
                                                        backgroundColor: showExpendedView && activeIndex === index ? '#FAFAFA' : 'transparent'
                                                    }}
                                                >
                                                    <div className="td" onClick={() => {
                                                        expandView(index)
                                                        if (showExpendedView && activeIndex === index) {
                                                            expandableRowRefs[index].current.style.height = "0px"
                                                        } else {
                                                            expandableRowRefs[index].current.style.height = "60px"
                                                        }
                                                    }}>
                                                        {
                                                            showExpendedView && activeIndex === index
                                                                ? <DropDownTableArrow />
                                                                : <ArrowRightPrimary />
                                                        }
                                                    </div>
                                                    <div className="td">{value.mkpBookingNo}</div>
                                                    <div className="td">{value.orderManagementID}</div>
                                                    <div className="td">{value.deliveryOrderID}</div>
                                                    <div className="td">
                                                        <button className={"btn " + value.orderType}>{value.orderType}</button>
                                                    </div>
                                                    <div className="td" style={{ flexDirection: "column", alignItems: "flex-start" }}>
                                                        <div>{value.destinationName1} <ArrowRightTable /> {value.destinationName3 != null ? value.destinationName3 : value.destinationName2}</div>
                                                        {/* <div className="type-booking">{value.destinationName1}</div> */}
                                                    </div>
                                                    <div className="td">
                                                        <button className={"btn " + value.orderType}>{status}</button>
                                                    </div>
                                                    <div className="td"><div onClick={() => onOrderDetailClick(value)}>{<EyeIcon color={value.eye ? "blue" : ""} />}</div></div>
                                                </div>

                                                {/* expanded view */}
                                                <div ref={expandableRowRefs[index]} key={index} className="expandable-view">
                                                    <div className="td">
                                                    </div>
                                                    <div className="td expadableView-title">{"NO. SI"}</div>
                                                    <div className="td expadableView-value">
                                                        {value.shippingInvoiceID}
                                                        <div className="border" />
                                                    </div>
                                                    <div className="td expadableView-title">{"Jumlah Kontainer"}</div>
                                                    <div className="td expadableView-value">
                                                        {value.quantity}
                                                        <div className="border" />
                                                    </div>
                                                    <div className="td expadableView-title">
                                                        Shipping Line
                                            </div>
                                                    <div className="td"><img src={require('assets/images/svg/shippingLine.svg')}></img></div>
                                                    <div className="td"></div>
                                                </div>

                                            </div>
                                        )
                                    })
                                }
                            </table>
                            <Pagination
                                postsPerPage={postsPerPage}
                                totalPosts={orderListData.data != undefined ? orderListData.data.length : 0}
                                paginate={paginate}
                                currentPage={currentPage}  
                                paginatePrevious={paginatePrevious}
                                paginateNext={paginateNext}
                                currentPage={currentPage}
                            />
                            {

                                showModal &&
                                <SubmitOrderModal value={newCustomerOrder.value.orderManagementID} onPress={modalPressed} />
                            }
                        </div>
                        : <TruckOrderDetail orderID={orderID} navigateBack = {() => setLoadOrderDetail(false)} />
                }
            </div>
            {
                buatClicked &&
                <Redirect
                    to={{
                        pathname: "/dashboard/add/truck",
                        //state: { value:  this.props.newCustomerOrder.value}
                    }}
                />
            }
        </LayoutDashboard>
    )
}
