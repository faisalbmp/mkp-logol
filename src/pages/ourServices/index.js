import React, { useCallback, useState, useLayoutEffect, Fragment } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { withTranslation } from "react-i18next";

import HeaderHome from "components/header/HeaderHome";
import Footer from "components/footer";
import CustomerService from "components/container/customerService";
import { setScrollposition, setTypeBooking } from "actions";
import SelectRoundBig from "components/select/selectRoundBig";
import JoinOur from "components/container/joinOur";

import backgroundFreightImg from "assets/images/png/bg-top-freight-2.png";
import backgroundTruckImg from "assets/images/png/bg-top-truck-2.png";

import {
  content,
  cardCenterChild,
  cardCenter,
  wpSelectRoundBig,
  textLayanan,
  contentBg3,
  layout,
  wpImg,
  textXlCard,
  textSCard,
  title,
  contentDecText,
  textDecText,
  centerCard,
  imgWpImg,
  rsContentDecText,
  rsImgCd,
} from "assets/scss/ourServices/index.module.scss";

import amtLogoImg from "assets/images/logo/amt-logo.svg";
import agilityLogo from "assets/images/logo/agility-logo.svg";
import aplLogo from "assets/images/logo/apl-logo.svg";
import cevaLogo from "assets/images/logo/ceva-logo.svg";
import cmacgmLogo from "assets/images/logo/cmacgm-logo.svg";
import tangguhLogoImg from "assets/images/logo/tangguh.svg";
import kamadjajaLogistics from "assets/images/logo/kamadjajaLogistics.svg";
import ironbird from "assets/images/logo/ironbird.svg";
import toll from "assets/images/logo/toll.svg";
import maersk from "assets/images/logo/maersk-new-logo.svg";
import truckImg from "assets/images/ourService/truckImg.png";
import freightImg from "assets/images/ourService/freightImg.png";
import useWindowDimensions from "utils/windowDimensions";
import CardwhyUseLogol from "components/cards/CardwhyUseLogol";
import { ArrowUp } from "components/icon/iconSvg";

const listFreight = [agilityLogo, aplLogo, cevaLogo, cmacgmLogo, maersk, toll];

const listTruck = [amtLogoImg, ironbird, kamadjajaLogistics, tangguhLogoImg];

const childTruckCard = [
  { textCont: "50+", title: "50+TruckVendor" },
  { textCont: "1,500+", title: "1,500+Trucks" },
  { textCont: "A+", title: "A+TruckQuality" },
  { textCont: "200+", title: "200+Routes" },
];

const childFreightCard = [
  { textCont: "80+", title: "80+sailing" },
  { textCont: "200+", title: "200+freight" },
  { textCont: "50+", title: "50+destination" },
  { textCont: "1000+", title: "1000+sailingSchedule" },
];

const selectTruck = {
  logo: listTruck,
  childCardImg: childTruckCard,
};

const selectFreight = {
  logo: listFreight,
  childCardImg: childFreightCard,
};

function Index({ t }) {
  const translate = t;
  const history = useHistory();
  const pathname = history.location.pathname.split("/");
  const type = pathname[pathname.length - 1];
  const [active, setActive] = useState(type || "");
  const [selectData, setSelectData] = useState(
    active === "freight" ? selectFreight : selectTruck
  );

  const dispatch = useDispatch();
  const { scrollposition } = useSelector((state) => state.home);
  const callBack = useCallback(() => {
    dispatch(setScrollposition());
  }, [scrollposition]);
  const { width } = useWindowDimensions();
  const [color, setColor] = useState("#005EEB");

  useLayoutEffect(() => {
    window.addEventListener("scroll", () => callBack());
    return () => {
      window.removeEventListener("scroll", () => callBack());
    };
  }, []);

  const clickTab = (res) => {
    setActive(res);
    if (res === "freight") {
      setSelectData(selectFreight);
    } else {
      setSelectData(selectTruck);
    }
  };

  const onClickCardWol = () => {
    history.push(`/`);
    if (active === "freight") {
      dispatch(setTypeBooking("freight"));
    } else {
      dispatch(setTypeBooking("truck"));
    }
  };

  return (
    <div className="app-home">
      <HeaderHome activeHeader={true} />
      <div
        className={layout}
        style={{
          backgroundImage: `url(${
            active === "freight" ? backgroundFreightImg : backgroundTruckImg
          })`,
        }}
      ></div>

      <div className={content}>
        <div className={textLayanan}>{translate("ourServices.title")}</div>
        <div className={wpSelectRoundBig}>
          <SelectRoundBig
            active={active}
            onClick={clickTab}
            rightType={"freight"}
            leftType={"truck"}
            rightText={translate("ourServices.oceanFreight")}
            leftText={translate("ourServices.trucking")}
          />
        </div>

        {600 < width ? (
          <div
            className={contentDecText}
            style={{
              backgroundImage: `url(${
                active === "freight" ? freightImg : truckImg
              })`,
            }}
          >
            <div>
              <div className={title}>
                {active === "freight"
                  ? translate("ourServices.oceanFreight")
                  : translate("ourServices.trucking")}
              </div>
              <div className={textDecText}>
                {active === "freight"
                  ? translate("ourServices.oceanFreightDesc")
                  : translate("ourServices.truckingDesc")}
              </div>
            </div>
          </div>
        ) : (
          <Fragment>
            <div className={title} style={{ marginTop: 64 }}>
              {active === "freight"
                ? translate("ourServices.oceanFreight")
                : translate("ourServices.trucking")}
            </div>
            <div className={rsContentDecText}>
              <img
                src={active === "freight" ? freightImg : truckImg}
                alt="img"
                className={rsImgCd}
              />
              <div className={textDecText}>
                {active === "freight"
                  ? translate("ourServices.oceanFreightDesc")
                  : translate("ourServices.truckingDesc")}
              </div>
            </div>
          </Fragment>
        )}

        <div className={cardCenter}>
          <div className={title}>{translate("ourServices.ourPartner")}</div>
          <div className={wpImg}>
            {selectData.logo.map((value, index) => (
              <div key={index} className={imgWpImg}>
                <img src={value} />
              </div>
            ))}
          </div>
        </div>
        <div className={cardCenterChild}>
          {selectData.childCardImg.map((value, index) => (
            <div key={index} className={centerCard}>
              <div className={textXlCard}>{value.textCont}</div>
              <div className={textSCard}>
                {translate(`ourServices.${value.title}`)}
              </div>
            </div>
          ))}
        </div>
        <CardwhyUseLogol
          onClick={onClickCardWol}
          titleButton={translate("bookAShipping")}
        />
      </div>

      <div
        className={contentBg3}
        style={{ backgroundPositionY: (scrollposition + -3900) * 0.2 + "px" }}
      >
        <JoinOur />
      </div>
      <CustomerService />
      <div
        className={`wrapper-floating-scroll-top ${
          window.scrollY > 100 && "wrapper-floating-scroll-top-active"
        }`}
      >
        <div
          className="grid"
          onClick={() => {
            window.scrollTo(0, 0);
          }}
        >
          <span className="showSpan">Scroll To Top</span>
          <div>
            <div
              className={`floating-scroll-top ${
                window.scrollY > 100 && "floating-scroll-top-active"
              }`}
              onMouseOver={() => setColor("white")}
              onMouseOut={() => setColor("#005EEB")}
            >
              <ArrowUp color={color} />
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default withTranslation()(Index);
