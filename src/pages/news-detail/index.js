import React, { useCallback, useLayoutEffect } from 'react'
import { useSelector, useDispatch, } from 'react-redux'
import { setScrollposition } from 'actions'

import HeaderHome from 'components/header/HeaderHome'
import Footer from 'components/footer'

import {
  content, bgDetailTop, contentBg, title, imageContent,
  contentWrapper, wrContainer, wpFooter, wpIcon, iconRn, topDetailColumn, topDetailColumnItem
} from 'assets/scss/news/index.module.scss'
import RoundedButton from 'components/button/RoundedButton'

import { withTranslation } from 'react-i18next';

function Index(props) {
  const dispatch = useDispatch()
  const { scrollposition } = useSelector(state => state.home);
  const callBack = useCallback(() => { dispatch(setScrollposition()) }, [scrollposition])

  const { t } = props;
  const translate = t;

  useLayoutEffect(() => {
    window.addEventListener('scroll', () => callBack())
    return () => {
      window.removeEventListener('scroll', () => callBack())
    }
  }, [])

  let data = {};
  if (props?.location?.state?.data) {
    data = props.location.state.data;
  }

  return (
    <div className={wrContainer}>
      <HeaderHome activeHeader={true} />

      <div className={bgDetailTop} >
        <div className={contentBg}>
          <div className={title} style={{
            fontWeight: 800,
            fontSize: '36px',
            lineHeight: '45px',
          }}>{data.title}</div>

          <div className={topDetailColumn}>
            <div className={topDetailColumnItem} style={{
              display: 'flex',
              flexDirection: 'column',
            }}>
              <span style={{
                fontWeight: 'bold',
                fontSize: '16px',
                color: '#868A92'
              }}>{data.source}</span>
              <span style={{
                fontSize: '16px',
                color: '#868A92'
              }}>{data.createdDate}</span>
            </div>
            <div className={wpIcon}>
              <RoundedButton title='share' className={iconRn} />
              <RoundedButton title='fb' className={iconRn} />
              <RoundedButton title='tw' className={iconRn} />
              <RoundedButton title='li' className={iconRn} />
            </div>
          </div>
        </div>
      </div>

      <div className={content} style={{ marginTop: '-35px' }}>
        <div className={imageContent} style={{
          backgroundImage: `url(${data.image})`
        }} />

        <div className={contentWrapper} dangerouslySetInnerHTML={{ __html: data.description }} />

        <div className={contentWrapper} style={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          marginTop: '40px'
        }}>
          <div style={{
            fontSize: '20px',
            fontWeight: 'bold',
            color: '#333',
          }}>{translate('news.share')} :</div>
          <div
            style={{
              marginLeft: '10px'
            }}
            className={wpIcon}
          >
            <RoundedButton title='share' className={iconRn} />
            <RoundedButton title='fb' className={iconRn} />
            <RoundedButton title='tw' className={iconRn} />
            <RoundedButton title='li' className={iconRn} />
          </div>
        </div>
      </div>

      <div className={wpFooter}>
        <Footer />
      </div>
    </div>
  )
}

export default withTranslation()(Index);
