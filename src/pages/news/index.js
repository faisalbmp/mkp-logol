import React, { useCallback, useLayoutEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { setScrollposition } from 'actions';
import { useHistory } from 'react-router-dom';

import HeaderHome from 'components/header/HeaderHome';
import Footer from 'components/footer';
import LearnMore from 'components/button/learnMore';

import bgeDocument from 'assets/images/bg/bg-top-news.jpeg';

import newsImage1 from 'assets/images/news/image-1.png';
import newsImage2 from 'assets/images/news/image-2.png';
import newsImage3 from 'assets/images/news/image-3.png';
import newsImage4 from 'assets/images/news/image-4.png';
import newsImage5 from 'assets/images/news/image-5.png';
import newsImage6 from 'assets/images/news/image-6.png';
import newsImage7 from 'assets/images/news/image-7.jpg';
import newsImage8 from 'assets/images/news/image-8.jpg';
import newsImage9 from 'assets/images/news/image-9.jpg';

import {
	content,
	bgTop,
	contentBg,
	title,
	contentTitle,
	contentSubtitle,
	topTitle,
	cardWrapper,
	card,
	cardImage,
	cardContent,
	cardTitle,
	cardSubtitle,
	cardFooter,
	readMore,
} from 'assets/scss/news/index.module.scss';

import { withTranslation } from 'react-i18next';

const newsData = [
	{
		id: 9,
		title:
			'NPCT1 Collaborates with Truck Marketplace Operators to Digitally Enhance Customer Experience',
		image: newsImage9,
		source: 'globalpsa.com',
		createdDate: 'Kamis, 14 January 2021',
		description: `<p><a href="https://www.npct1.co.id/" style="text-decoration: none; font-weight: bold">New Priok Container Terminal 1 (NPCT1)</a> has announced a collaboration with three leading truck marketplace operators in Indonesia – Logol, Logee and Ritase – to enhance its customers’ user experience through digitalisation.</P>
    <p>With over 15,000 weekly import and export truck container movements at NPCT1, it is vital that services for its customers are reliable, smooth and easy to access. To achieve this, NPCT1 has collaborated with the trio of truck marketplace operators to create a simple and integrated logistics process by leveraging their combined industry expertise and state-of-the-art integrated digital platforms.</p>
    <p>As a result of the collaboration, NPCT1 customers will be able to access a new and complete end-to-end service to easily accomplish tasks like customs clearance, container truck management, booking and scheduling, as well as e-document processing and legislative compliance at their own convenience.</p>
    <p>In addition to improving customer experience, the new service also reduces the need for physical contact and is more sustainable, as all transactions are now digitally processed and rendered completely paperless. This is in line with NPCT1’s sustainability commitment and strict COVID-19 safety measures.</p>
    <p>Director of NPCT1 Pak Hudadi said, “We are delighted to see the fruition of this collaborative effort. With the integration of NPCT1’s paperless billing platform Express Container Online (ECON) and the three platforms, customers will have the opportunity to reliably access services from any location, giving them dynamic flexibility, as well as reducing the need for physical entry into port facility offices, which is vital in these challenging times.”</p>
    <p style="text-align: center">For more information, please click <a href="https://www.globalpsa.com/wp-content/uploads/nr210106.pdf" target="__blank" style="text-decoration: none; font-weight: bold">HERE<a/> for the press release.</p>`,
	},
	{
		id: 1,
		title: 'Jurus RI Bikin Angkutan Logistik Bisa Bersaing dengan Negara Lain',
		image: newsImage1,
		source: 'Herdi Alif Al Hikam | detikFinance',
		createdDate: 'September, 27th 2020',
		description: `<p>Jakarta - Pemerintah baru saja meluncurkan ekosistem logistik nasional atau national logistics ecosystem (NLE). Salah satu terobosannya adalah membuat pengurusan logistik bisa diurus secara online.</p>
  <p>Pemerintah pun sudah meresmikan platform digital untuk pencarian truk dan penerbitan eSP2, yang ditunjuk menjalankan hal tersebut adalah PT Logol Jakarta Mitraindo.</p>
  <p>"Satu inovasi digital dalam NLE ini adalah pemesanan truk online dan PT Logol Jakarta Mitraindo resmi ditunjuk sebagai platform digital untuk pencarian truk dan penerbitan eSP2, " papar CEO PT Logol Jakarta Mitraindo, Michael K. Kartono, dalam keterangannya, Minggu (27/9/2020).</p>
  <p>Michael menjelaskan, pemesanan atau delivery order dan persetujuan pengeluaran peti kemas mulai saat ini dilakukan online, sehingga dokumen tidak perlu jalan fisik.</p>
  <p>Pemesanan truk bentuknya akan menjadi seperti sistem ojek online, truk bisa dipesan dan mereka tidak perlu ngetem fisik di satu tempat, sehingga akan terjadi efisiensi.</p>
  <p>"Platform ini memberikan kemudahan jasa untuk para pelaku bisnis, UMKM maupun manufaktur untuk melakukan pengiriman barang secara melalui jalur darat dan laut secara online, semua proses ini dapat dilakukan hanya dari ujung jari atau at your finger tips," ujar Michael.</p>
  <p>PT Logol sendiri adalah market place logistic online yang menghubungkan stakeholder di suatu ekosistem logistik. Mulai dari pemilik kargo, perusahaan logistik, perusahaan pelayaran, hingga pemerintah dalam hal ini Bea Cukai dan urusan kepelabuhan.</p>
  <p>(dna/dna)</p>`,
	},
	{
		id: 2,
		title: 'Pemerintah Luncurkan Platform Digital Ekosistem Logistik',
		image: newsImage2,
		source: 'Hariyanto Kurniawan | Kompas.tv',
		createdDate: 'September, 26th 2020',
		description: `<p><b>JAKARTA, KOMPAS.TV</b> - Pemerintah meluncurkan Ekosistem Logistik Nasional atau National Logistic Ecosystem (NLE) berbasis digital. Dengan NLE ini, pencarian transportasi logistik bisa dilakukan secara digital, serupa dengan ojek online.</p>
  <p>Menteri Perhubungan Budi Karya Sumadi berharap, lewat penerapan NLE ini, praktik repetisi dan replikasi dalam proses logistik terutama di pelabuhan bisa diberantas.</p>
  <p>"Khusus di laut yang memang sangat lekat atau syarat dengan permasalahan ini, dalam jangka pendek tentu pengembangan platform NLE, pemetaan dan penyederhanaan dari proses bisnis pre clearance dan dalam rangka menghilangkan repetisi dan replikasi, ini sangat penting," kata Budi Karya dalam keterangan tertulis yang diterima Kompas TV, Sabtu (16/9/2020).</p>
  <p>Selain itu, Budi mengatakan pihaknya sudah melakukan inovasi menggunakan platform digital yang menggabungkan segala proses perizinan yang diperlukan dalam arus logistik.</p>
  <p>"Kami juga sudah mengimplementasikan berbagai platform digital tentang efisiensi proses dan biaya salah satunya menggabungkan platform logistik, menyederhanakan proses bisnis dari clearance melalui single submission pengangkutan, penerapan online on gate di pelabuhan, dan juga melakukan integrasi semua moda transportasi logistik," katanya.</p>
  <p>Dalam platform digital ekosistem logistik ini, pemerintah menggandeng PT Logol Jakarta Mitraindo yang akan mempermudah transportasi logistik berupa pencarian truk dan penerbitan eSP2.</p>
  <p>Menurut CEO PT logol Jakarta Mitraindo Michael K Kartono, platform ini akan memberikan kemudahan jasa untuk para pelaku bisnis, UMKM maupun manufaktur untuk melakukan pengiriman barang secara melalui jalur darat dan laut secara online.</p>
  <p>"Semua proses ini dapat dilakukan hanya dari ujung jari atau at your fingertips," ujarnya.</p>
  <p>Jadi, imbuhnya, pemesanan atau delivery order dan persetujuan pengeluaran peti kemas dilakukan online, sehingga dokumen tidak perlu jalan fisik.</p>
  <p>"Pemesanan truk juga dilakukan online. Jadi seperti sistem Gojek, truk bisa dipesan dan mereka tidak perlu ngetem fisik di satu tempat, akan ada efisiensi," paparnya.</p>
  <p>PT Logol Jakarta Mitraindo yang berdiri sejak 2018, merupakan marketplace logistic online yang menghubungkan stakeholder di satu ekosistem logistik. Mulai dari pemilik kargo, perusahaan logistik, perusahaan pelayaran, badan otoritas pemerintah (dalam hal ini Bea Cukai dan pelabuhan), lembaga keuangan dan asuransi melalui satu platform yang sudah terintegrasi.</p>
  <p>Saat ini PT Logol melakukan pelayanan jasa ocean freight (vessel schedule), trucking (trucking booking management/containerised), e-document (pembuatan gate pass secara digital), dan round use services (proses container import dapat digunakan kembali secara langsung untuk proses ekspor).</p>
  <p>"PT Logol, saat ini adalah satu-satunya platform logistic sudah terkoneksi dengan tiga terminal di Pelabuhan Tanjung Priok (JICT, Koja, dan NPCT) dalam melayani proses ekspor dan impor barang secara digital. Khusus dalam proses pengurusan dokumen yang terkait," ujarnya.</p>`,
	},
	{
		id: 3,
		title: 'Mudahkan Pelaku Bisnis, Pemerintah Meluncurkan Ekosistem Logistik Nasional',
		image: newsImage3,
		source: 'Sugeng Adji Soenarso, Handoyo | Kontan.co.id',
		createdDate: 'September, 26th 2020',
		description: `<p><b>KONTAN.CO.ID - JAKARTA.</b> Pemerintah meluncurkan Ekosistem Logistik Nasional atau National Logistics Ecosystem (NLE) dan PT Logol Jakarta Mitraindo resmi ditunjuk menjadi platform digital  untuk pencarian truck dan penerbitan eSP2.</p>
  <p>"Satu inovasi digital dalam NLE ini adalah pemesanan truk online dan PT Logol Jakarta Mitraindo resmi ditunjuk sebagai platform digital untuk pencarian truck dan penerbitan eSP2.  Logol berdiri sejak 2018 dan hadir untuk menjawab tantangan NLE pemerintah, " papar CEO PT logol Jakarta Mitraindo,  Michael K. Kartono dalam keterangan resminya, Sabtu (26/9).</p>
  <p>Dia menjelaskan,  pemesanan atau delivery order dan persetujuan pengeluaran peti kemas dilakukan online, sehingga dokumen tidak perlu jalan fisik. Pemesanan truk juga dilakukan online sehingga seperti sistem Gojek, truk bisa dipesan dan tidak perlu ngetem fisik di satu tempat dan akan ada efisiensi.</p>
  <p>"Platform ini memberikan kemudahan jasa untuk para pelaku bisnis, UMKM maupun manufaktur untuk melakukan pengiriman barang melalui jalur darat dan laut secara online, semua proses ini dapat dilakukan hanya dari ujung jari atau at your finger tips," ujarnya.</p>
  <p>Adapun, PT Logol adalah marketplace logistic online yang menghubungkan stake holder di satu ekosistem logistik , mulai dari pemilik kargo, perusahaan logistik, perusahaan pelayaran , Badan Otoritas Pemerintah (bea cukai dan pelabuhan), lembaga keuangan dan asuransi melalui satu platform yang sudah terintegrasi.</p>
  <p>"PT Logol, saat ini adalah satu-satunya platform logistic sudah terkoneksi dengan tiga terminal di Pelabuhan Tj. Priok (JICt, Koja, dan NPct) dalam melayani proses export dan import barang secara digital khusus dalam proses pengurusan dokumen yang terkait," ujarnya.</p>
  <p>Jasa pelayanan PT Logol sendiri, katanya, mencakup layanan Ocean Freight (vessel schedule), Trucking (trucking booking management/containerised), E-document (pembuatan gate pass secara digital), dan Round Use Services (proses container import dapat digunakan kembali secara langsung untuk proses ekspor).</p>
  <p>Sementara itu, Menteri Perhubungan (Menhub), Budi Karya Sumadi berharap lewat penerapan NLE ini praktik repetisi dan replikasi dalam proses logistik terutama di pelabuhan bisa diberantas.</p>
  <p>"Khusus di laut yang memang sangat lekat atau syarat dengan permasalahan ini, dalam jangka pendek tentu pengembangan platform NLE, pemetaan dan penyederhanaan dari proses bisnis pre-clearance dan dalam rangka menghilangkan repetisi dan replikasi, ini sangat penting," tutur dia.</p>
  <p>Selain itu, Budi mengatakan pihaknya sudah melakukan inovasi menggunakan platform digital yang menggabungkan segala proses perizinan yang diperlukan dalam arus logistik.</p>
  <p>"Kami juga sudah mengimplementasikan berbagai platform digital tentang efisiensi proses dan biaya salah satunya menggabungkan platform logistik, menyederhanakan proses bisnis dari clearance melalui single submission pengangkutan, penerapan online on gate di pelabuhan, dan juga melakukan integrasi semua moda transportasi logistik," katanya.</p>`,
	},
	{
		id: 4,
		title: 'Ekosistem Logistik Nasional Bisa Kuatkan Sektor Bisnis',
		image: newsImage4,
		source: 'Ucha Julistian, Bernadetta Febriana | Gatra.com',
		createdDate: 'September, 25th 2020',
		description: `<p><b>Jakarta, Gatra.com</b> - Pemerintah baru-baru ini resmi meluncurkan Ekosistem Logistik Nasional atau National Logistics Ecosystem (NLE). Dengan adanya penerapan NLE ini, diharapkan repetisi dan replikasi dalam proses logistik terutama di pelabuhan bisa diberantas.</p>
  <p>Hal tersebut disampaikan oleh Menteri Perhubungan (Menhub), Budi Karya Sumadi. Dirinya mengatakan, pengembangan platform NLE dalam jangka pendek akan membantu pemetaan dan penyederhanaan dari proses bisnis pre clearance dan dalam rangka menghilangkan repetisi dan replikasi.</p>
  <p>"Ini sangat penting, khususnya di laut yang memang sangat lekat atau syarat dengan permasalahan ini, dalam jangka pendek tentu pengembangan platform NLE akan sangat membantu, " kata Budi dalam keterangannya, Jumat (25/9).</p>
  <p>Menhub Budi juga menuturkan, bahwa selama ini mengatakan pihaknya sudah melakukan inovasi menggunakan platform digital yang menggabungkan segala proses perizinan yang diperlukan dalam arus logistik. Selain itu, imoelenentasi di  berbagai platform digital  tentang efisiensi proses dan biaya sudah dilakukan.</p>
  <p>"Salah satunya menggabungkan platform logistik, menyederhanakan proses bisnis dari clearance melalui single submission pengangkutan, penerapan online on gate di pelabuhan, dan juga melakukan integrasi semua moda transportasi logistik," terangnya.</p>
  <p>Dalam platform NLE tersebut, PT Logol Jakarta Mitraindo ditunjuk menjadi platform digital resmi ditunjuk untuk pencarian truck dan penerbitan eSP2. Ditambahkan oleh CEO PT logol Jakarta Mitraindo,  Michael K. Kartono, salah satu inovasi digital dalam platform NLE tersebut yakni untuk untuk pencarian truck dan penerbitan eSP2.</p>
  <p>"Pemesanan atau delivery order dan persetujuan pengeluaran peti kemas dilakukan online, sehingga dokumen tidak perlu jalan fisik. Pemesanan truk juga dilakukan online. Jadi seperti sistem Gojek, truk bisa dipesan dan mereka tidak perlu ngetem fisik di satu tempat, akan ada efisiensi," jelas Michael.</p>
  <p>Disampaikannya, Platform tersebutjufa akan memberikan kemudahan jasa untuk para pelaku bisnis, UMKM maupun manufaktur untuk melakukan pengiriman barang secara melalui jalur darat dan laut secara online. Nantinya, semua proses tersebut dapat dilakukan hanya dari ujung jari atau at your finger tips.</p>`,
	},
	{
		id: 5,
		title: 'Dicari! Platform Digital Khusus Layanan Ekspor Impor',
		image: newsImage5,
		source: 'Vadhia Lidyana | detikFinance',
		createdDate: 'September, 24th 2020',
		description: `<p><b>Jakarta</b> - Pemerintah sudah meluncurkan National Logistics Ecosystem (NLE) yang mengintegrasikan seluruh proses pelayanan logistik untuk menghemat waktu dan biaya. Salah satu harapan pemerintah dengan adanya NLE ini proses ekspor dan impor semakin cepat.</p>
  <p>"Karena penerapan NLE ini akan mempercepat proses ekspor-impor sehingga menyelaraskan arus lalu lintas barang dengan arus dokumen internasional. Ini kita sambut dengan sangat baik karena akan meningkatkan produktivitas dan juga nilai ekspor maupun impor," kata Menteri Perdagangan Agus Suparmanto dalam konferensi pers virtual NLE, Kamis (24/9/2020).</p>
  <p>Untuk menyempurnakan jalur logistik di Indonesia, Agus berharap semakin banyak platform digital baru yang bermain di sektor logistik ini. Harapannya, dengan semakin banyaknya pemain, maka layanan logistik bisa semakin beragam dan berdaya saing.</p>
  <p>"Dengan peran entitas logistik ini memang masih sangat dibutuhkan agar ekosistem logistik di Indonesia bisa berdampak maksimal, sehingga munculnya platform-platform digital baru di Indonesia sangat diharapkan," ujar Agus.</p>
  <p>Selain itu, ia juga berharap bisa tercipta banyak terminal untuk barang yang dapat mendongkrak produktivitas ekspor, dan memperlancar impor bahan baku.</p>
  <p>"Nah terminal-terminal barang ini juga sangat penting, sehingga meningkatkan juga kepada proses atau produktivitas ekspor-impor kita sehingga nantinya ekspor naik, dan impornya lancar khususnya untuk bahan baku penolong ekspor," imbuh Agus.</p>
  <p>Hal serupa juga diutarakan oleh Kepala Badan Karantina Ikan, Pengendalian Mutu dan Keamanan Hasil Perikanan (BKIPM) Kementerian Kelautan dan Perikanan (KKP) Rina. Ia berharap semakin banyak kemunculan platform digital di sektor logistik untuk memperluas jangkauan produk perikanan Indonesia, terutama dari wilayah Indonesia bagian timur.</p>
  <p>"Kami berharap akan muncul banyak penyedia platform digital baru di seluruh wilayah Indonesia. Untuk perikanan kita harap betul di Indonesia bagian timur sehingga bisa lebih banyak mengakses produk-produk kelautan dan perikanan," jelas Rina.</p>
  <p>Sebelumnya, Menteri Keuangan Sri Mulyani Indrawati mengatakan, salah satu inovasi digital dalam NLE ini adalah pemesanan truk online.</p>
  <p>"Kita lakukan integrasi layanan perizinan ship to ship dan floating storage unit, perizinan usaha dan pemasukan barang konsumsi di kawasan khusus Batam. Pemesanan atau delivery order dan persetujuan pengeluaran peti kemas dilakukan online. Sehingga dokumen tidak perlu jalan fisik. Pemesanan truk juga dilakukan online, jadi seperti sistem Gojek, truk bisa dipesan dan mereka tidak perlu ngetem fisik di satu tempat, akan ada efisiensi," terang Sri Mulyani.</p>
  <p>(hns/hns)</p>`,
	},
	{
		id: 6,
		title: 'Menhub Akui Daya Saing Logistik RI Masih Bermasalah',
		image: newsImage6,
		source: 'Vadhia Lidyana | detikFinance',
		createdDate: 'September, 24th 2020',
		description: `<p><b>Jakarta</b> - Menteri Perhubungan (Menhub) Budi Karya Sumadi mengakui daya saing logistik Indonesia masih bermasalah apabila dibandingkan dengan negara lain. Oleh sebab itu, pemerintah ingin meningkatkan daya saing dengan melakukan efisiensi proses dan tarif logistik melalui National Logistics Ecosystem (NLE) atau ekosistem logistik nasional.
  "Daya saing kita memang masih bermasalah, insyaallah ini (NLE) akan menjadi hal yang baik untuk kita semua," kata Budi dalam konferensi pers bersama NLE, Kamis (24/9/2020).</p>
  Melalui NLE, ini, Budi berharap praktik repetisi dan replikasi dalam proses logistik terutama di pelabuhan bisa diberantas.</p>
  <p>"Khusus di laut yang memang sangat lekat atau syarat dengan permasalahan ini, dalam jangka pendek tentu pengembangan platform NLE, pemetaan dan penyederhanaan dari proses bisnis pre clearance dan dalam rangka menghilangkan repetisi dan replikasi, ini sangat penting," tutur dia.</p>
  <p>Untuk mendukung NLE yang dikembangkan Direktorat Jenderal Bea dan Cukai Kementerian Keuangan (Kemenkeu) ini, Kemenhub juga akan melakukan integrasi sistem perizinan dan pelayanan ekspor-impor di lingkup logistik.</p>
  <p>"Kami mendukung penuh dan berperan aktif, dan kita ingin segera bahwa upaya penataan konektivitas, aksesibilitas antar kawasan menjadi lebih baik. Integrasi dan menghubungkan infrastruktur simpul-simpul transportasi seperti di pelabuhan, bandara, KA, terminal, dan pusat distribusi harus dilakukan. Dan tentu ini akan menghubungkan darat, laut, dan udara," urainya.</p>
  <p>Selain itu, Budi mengatakan pihaknya sudah melakukan inovasi menggunakan platform digital yang menggabungkan segala proses perizinan yang diperlukan dalam arus logistik.</p>
  <p>"Kami juga sudah mengimplementasikan berbagai platform digital tentang efisiensi proses dan biaya salah satunya menggabungkan platform logistik, menyederhanakan proses bisnis dari clearance melalui single submission pengangkutan, penerapan online on gate di pelabuhan, dan juga melakukan integrasi semua moda transportasi logistik," jelas Budi.</p>
  <p>Sebelumnya, Menteri Keuangan Sri Mulyani Indrawati juga mengakui bahwa daya saing logistik Indonesia masih rendah. Pasalnya, tarif logistik di Indonesia jauh lebih tinggi dibandingkan dengan negara-negara tetangga di ASEAN.</p>
  <p>"Biaya logistik kita dibandingkan negara tetangga terutama ASEAN dan terdekat seperti Malaysia dan Singapura itu masih dianggap lebih tinggi. Ini menyebabkan perekonomian indonesia harus perbaiki daya kompetisinya. Kita mengeluarkan lebih dari 23,5% dari PDB untuk biaya logistik. Malaysia hanya 13%. Performance dari logistik kita dalam EODB mengenai berapa jumlah hari/ jam/ waktu untuk menyelesaikan proses logistik itu belum tunjukkan perbaikan signifikan. Dari trade across border, dari EODB hanya naik sedikit dari 67,3 ke 69,3 atau sebetulnya tidak terlalu bagus," ungkap Sri Mulyani.</p>
  <p>Melalui NLE ini, ia yakin biaya logistik di Indonesia bisa ditekan. "Dengan adanya pembentukan NLE ini diharapkan akan bisa menurunkan biaya logistik kita yang sekarang 23,5% dari PDB akan bisa ditekan menjadi 17%," pungkas dia.</p>
  <p>(fdl/fdl)</p>`,
	},
	{
		id: 7,
		title: 'Marketplace Logistik Logol Bidik Pendapatan US$ 20 Juta',
		image: newsImage7,
		source: 'Whisnu Bagus Prasetyo / WBP | BeritaSatu.com',
		createdDate: 'Kamis, 8 Oktober 2020',
		description: `<div class="story">
  <p><strong>Jakarta, Beritasatu.com</strong> - PT Logol Jakarta Mitraindo sebagai <em>marketplace</em> yang menghubungkan pelaku usaha di bidang logistik menargetkan pendapatan sekitar US$ 10-20 juta pada 2021. Langkah ini sejalan dengan penunjukkan Logol menjadi platform digital untuk pencarian truk dan penerbitan dokumen eSP2 dalam rangka Ekosistem Logistik Nasional (National Logistics Ecosystem/NLE).</p>
  <p>"Sales tahun depan bisa naik 2-3 kali lipat dari proyeksi tahun ini," kata Chief Financial Officer (CFO) PT Logol Jakarta Mitraindo, Manli Yap di Jakarta, Kamis (8/10/2020).</p>
  <p>Jasa pelayanan Logol mencakup layanan <em>ocean freight (vessel schedule), trucking (trucking booking management/containerised), e-document</em> (pembuatan gate pass secara digital), dan <em>round use services</em>, yakni proses kontainer impor dapat digunakan kembali secara langsung untuk proses ekspor. "Dengan bergabung di <em>marketplace</em> Logol, pemilik armada truk bisa melakukan efisiensi hingga 50 persen dari modal kerja," kata Manli Yap.</p>
  <p>Founder &amp; CEO PT Logol Jakarta Mitraindo Michael Kartono mengatakan, berdiri pada September 2018, Logol merupakan <em>marketplace</em> yang memungkinkan manufaktur, distributor, UMKM, dan pelaku usaha lainnya memesan layanan pengiriman barang secara <em>online</em> melalui jalur darat dan laut. Selain itu, memberi kemudahan pengurusan dokumen ekspor impor secara elektronik.</p>
  <p>“Logol hadir untuk menjawab kesulitan mencari armada di bidang logistik untuk ekspor/impor. Ini konsep pertama yang dilakukan perusahaan yang bergerak di bidang logistik dengan mendigitalisasi proses. Logol juga satu-satunya <em>startup campany</em> yang berhasil mengkoneksi prasarana untuk mendigitalisasi <em>gate pass</em>,” ujar Michael.</p>
  <p>Logol menghubungkan para pelaku di industri logistik, mulai pemilik truk, transporter, <em>forwarder</em>, lembaga pemerintahan, hingga pelaku usaha, untuk menciptakan ekosistem logistik yang lebih efisien, terjangkau dan transparan. "Fitur pertamanya berupa layanan pengiriman barang ekspor dan impor yang terintegrasi dengan salah satu terminal di Tanjung Priok pada April 2019," kata dia.</p>
  <p>Chief Marketing Officer (CMO) PT Logol Jakarta Mitraindo, David Trisno mengatakan PT Logol saat ini sudah terkoneksi dengan tiga pelabuhan utama di Jakarta yakni New Priok Container Terminal (NPCT), Koja, dan Jakarta International Container Terminal dalam melayani proses ekspor/impor barang secara digital khusus dalam pengurusan dokumen. "Ke depan, Logol juga akan mengintegrasikan layanan logistik di Pelabuhan Tanjung Perak (Surabaya), Pelabuhan Tanjung Emas (Semarang) dan Pelabuhan Belawan, Sumatera Utara," kata dia.</p>
  <p>Dia mengatakan, layanan Logol saat ini dapat dinikmati pebisnis di seluruh Indonesia dengan dua filur utama, yailu pemesanan truk serta layanan kargo kapal untuk kebutuhan ekspor dan impor. Dengun kemudahan pemesanan logislik secara online, Logol diharapkan dapat mempermudah pelaku usaha melakukan seluruh proses ekspor/impor, mulai <em>freight booking, truck booking, port handling</em> hingga pengurusan Bea Cukai secara <em>online.</em></p><br><br>
`,
	},
	{
		id: 8,
		title: 'Digitalisasi, Proses Logistik Jadi Lebih Efisien',
		image: newsImage8,
		source: 'Mediaindonesia.com | Ekonomi',
		createdDate: 'Kamis 08 Oktober 2020',
		description: `<p>LOGISTIK sangat berperan dalam pergerakan perekonomian khususnya di Indonesia. Namun sistem logistik masih menyita banyak waktu dan biaya, karena dilakukan secara tradisional. Untuk itu PT Logol&nbsp; Jakarta Mitraindo melakukan terobosan mempermudah para pelaku bisnis menjalankan kegiatan proses logistik dengan cara mendigitalisasi pelayanan dalam bidang pengiriman.</p>
  <p>“Kami hadir untuk menjawab kesulitan mencari armada di bidang logistik untuk ekspor impor. Ini Konsep pertama yang dilakukan perusahaan dengan mendigitalisasi proses, karena secara manual atau tradisional tidak efektif dan efisien lagi. Selain itu, mereka adalah satu-satunya startup company yang berhasil mengoneksi prasarana untuk mendigitalisasi gate pass,” ujar Michael Kartono, Founder &amp; CEO Logol dalam jumpa pers secara virtual.</p>
  <p>Dia menjelaskan, perusahaan yang baru berdiri dua tahun lalu itu adalah perusahaan pertama yang memberikan solusi kepada berbagai pelaku bisnis untuk mencari layanan pemesanan truk secara digital. Seiring dengan berkembang permintaan, mereka meluncurkan fitur pertamanya berupa layanan pengiriman barang ekspor dan impor yang terintegrasi dengan salah satu terminal di Tanjung Priok pada April 2019 silam. Logol merupakan marketplace yang memungkinkan manufaktur, distributor, UMKM, dan pelaku usaha lainnya memesan layanan pengiriman barang secara online melalui jalur darat dan laut serta dengan kemudahan pengurusan dokumen ekspo-impor secara elektronik.</p>
  <p>Mereka berhasil menghubungkan para pelaku di industri logistik, mulai dari pemilik truk, transporter, forwarder, lembaga pemerintahan, hingga pelaku usaha, untuk menciptakan ekosistem logistik yang lebih efisien, terjangkau, dan transparan.</p>
  <p>“Layanan kami saat ini dapat dinikmati oleh pebisnis di seluruh Indonesia dengan dua fitur utama, yaitu pemesanan truk serta layanan kargo kapal untuk kebutuhan ekspor dan impor. Dengan kemudahan pemesanan logistik secara online, Logol diharapkan dapat mempermudah para pelaku usaha untuk melakukan seluruh proses ekspor-impor, mulai dari freight booking, truck booking, port handling hingga pengurusan bea cukai secara online. Salah satu keunggulan dari marketplace kami adalah kemudahan pengurusan dokumen secara digital yang terkait dengan pengiriman barang, termasuk ekspor dan impor, sehingga Anda tidak perlu repot lagi,” lanjut Michael.</p>
  <p>Prestasi Logol inilah, ketika pemerintah meluncurkan Ekosistem Logistik Nasional atau National Logistics Ecosystem (NLE), secara resmi menunjuk Logol menjadi platform digital&nbsp; untuk pencarian truk dan penerbitan eSP2. "Satu inovasi digital dalam NLE ini adalah pemesanan truk online dan PT Logol Jakarta Mitraindo resmi ditunjuk sebagai platform digital untuk pencarian truk dan penerbitan eSP2. Platform ini memberikan kemudahan jasa untuk para pelaku bisnis, UMKM maupun manufaktur untuk melakukan pengiriman barang melalui jalur darat dan laut secara online, semua proses ini dapat dilakukan hanya dari ujung jari atau at your finger tips," ujarnya.</p>
  <p>Dia menjelaskan,&nbsp; PT Logol adalah market place logistik online yang menghubungkan stake holder di satu ekosistem logistik, mulai dari Pemilik Cargo, Perusahaan Logistik, Perusahaan Pelayaran, Badan Otoritas Pemerintah (dalam hal ini Bea Cukai dan pelabuhan), Lembaga Keuangan dan Asuransi melalui satu platform yang sudah terintegrasi.</p>
  <p>"PT Logol, saat ini sudah terkoneksi dengan tiga pelabuhan (JICt, Koja, dan NPct) dalam melayani proses ekspor dan impor barang secara digital khusus dalam proses pengurusan dokumen yang terkait," ujarnya. (RO/A-1)</p>
  `,
	},
];

function Index({ t }) {
	const dispatch = useDispatch();
	const history = useHistory();
	const { scrollposition } = useSelector(state => state.home);
	const callBack = useCallback(() => {
		dispatch(setScrollposition());
	}, [scrollposition]);
	const translate = t;

	useLayoutEffect(() => {
		window.addEventListener('scroll', () => callBack());
		return () => {
			window.removeEventListener('scroll', () => callBack());
		};
	}, []);

	return (
		<div className='app-home'>
			<HeaderHome activeHeader={true} />

			<div
				className={bgTop}
				style={{
					backgroundImage: `linear-gradient(97.17deg, #FFFFFF 11.18%, rgba(255, 255, 255, 0) 100%), url(${bgeDocument})`,
				}}
			>
				<div className={contentBg}>
					<div className={title}>{translate('news.title')}</div>
				</div>
			</div>

			<div className={content}>
				<div className={topTitle}>
					<h1 className={contentTitle}>{translate('news.titleContent')}</h1>
					<span className={contentSubtitle}>{translate('news.subTitle')}</span>
				</div>

				<div className={cardWrapper}>
					{newsData.map((item, i) => (
						<div key={i} className={card}>
							<div
								className={cardImage}
								style={{
									backgroundImage: `url(${item.image})`,
								}}
							/>
							<div className={cardContent}>
								<span className={cardSubtitle}>{item.createdDate}</span>
								<span
									className={cardTitle}
									onClick={() =>
										history.push(`/news-detail/${item.id}`, {
											data: item,
										})
									}
								>
									{item.title}
								</span>
							</div>
							<div className={cardFooter}>
								<LearnMore
									title={translate('learnMore')}
									onClick={() =>
										history.push(`/news-detail/${item.id}`, {
											data: item,
										})
									}
									style={readMore}
								/>
							</div>
						</div>
					))}
				</div>
			</div>

			<Footer />
		</div>
	);
}

export default withTranslation()(Index);
