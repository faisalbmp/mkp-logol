import React, { useState } from 'react'
import useWindowDimensions from 'utils/windowDimensions';

import { verticalSplitSlider, viewLeft, viewRight, item } from 'assets/afterBooking.module.scss'
import Login from 'components/container/login'
import Register from 'components/container/register'
import LeftDone from 'components/container/doneBooking/left'
import RightDone from 'components/container/doneBooking/right'
import RightLogin from 'components/container/truck/rightLogin'
import RightRegister from 'components/container/truck/rightRegister'
import LeftDetailContainer from 'components/container/truck/leftDetailContainer'
import RightDetailContainer from 'components/container/truck/rightDetailContainer'


export default function Index() {
    const { height, width } = useWindowDimensions();

    const positionDetailCt = {
        left: 0,
        right: -height * 3
    }

    const positionRegister = {
        left: -height,
        right: -height * 2
    }

    const positionLogin = {
        left: -height * 2,
        right: -height
    }

    const positionDone = {
        left: -height * 3,
        right: 0
        // left: -height * 2,
        // right: 0
    }

    const [slider, setSlider] = useState(positionDetailCt)

    return (
        <div className={verticalSplitSlider}>
            <div className={viewLeft} style={{ top: slider.left, width: width > 1000 ? "50%" : "100%" }}>
                <div className={item}>
                    <LeftDetailContainer
                        sliderRegister={() => setSlider(positionRegister)}
                    />
                </div>

                <div className={item}>
                    <Register
                        sliderLogin={() => setSlider(positionLogin)}
                        sliderDone={() => setSlider(positionDone)}
                    />
                </div>
                <div className={item}>
                    <Login
                        sliderRegister={() => setSlider(positionRegister)}
                        sliderDone={() => setSlider(positionDone)}
                    />
                </div>
                <div className={item}>
                    <LeftDone />
                </div>
            </div>

            {width > 1000 ?
                <div className={viewRight} style={{ top: slider.right }}>
                    <div className={item}>
                        <RightDone />
                    </div>
                    <div className={item}>
                        <RightLogin />
                    </div>
                    <div className={item}>
                        <RightRegister />
                    </div>
                    <div className={item}>
                        <RightDetailContainer />
                    </div>
                </div>
                : ""}
        </div>
    )
}