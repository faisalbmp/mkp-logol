
import React, { useCallback, useState, useLayoutEffect, Fragment } from 'react'
import { useSelector, useDispatch, } from 'react-redux';
import { useHistory } from "react-router-dom";

import HeaderHome from 'components/header/HeaderHome'
import Footer from 'components/footer'
import { setScrollposition } from 'actions'
import CustomerService from 'components/container/customerService'

import { subTitle, title, btnPrimary, conatinerTop, imgTop, containerRight, subTitleTop, titleTop, cardCenter, leftCard, rightCard, itemCard, activeCard, titleS, itemLine, imgLine, textLine, contentListItem, btnList, contentBtnList, wrpRound, bgBottom, contentBottom, wpBottom, whyTitle, imgLineRight, imgLineLeft, wpCardCenter, wpContentBtnList } from 'assets/scss/partner/index.module.scss'
import { btnYellow } from 'assets/scss/ourServices/index.module.scss'

import peopleHands from 'assets/images/partner/people_hands.png'
import networkAccess from 'assets/images/partner/network-access.svg'
import pay from 'assets/images/partner/pay.svg'
import manage from 'assets/images/partner/manage.svg'
import deriver from 'assets/images/partner/deriver.svg'
import note from 'assets/images/partner/note.svg'
import arrowRightHome from 'assets/images/icons/arrow-right-home.svg'
import useWindowDimensions from 'utils/windowDimensions';
import { useTranslation } from 'react-i18next';
import { ArrowUp } from 'components/icon/iconSvg';

const dataWhyLogol = [
    {
        icon: networkAccess,
        title: "options1",
        dec: "options1Desc",
    },
    {
        icon: pay,
        title: "options2",
        dec: "options2Desc",
    },
    {
        icon: manage,
        title: "options3",
        dec: "options3Desc",
    },
    {
        icon: deriver,
        title: "options4",
        dec: "options4Desc",
    },
    {
        icon: note,
        title: "options5",
        dec: "options5Desc",
    }
]

export default function Index() {
    const [active, setActive] = useState(1)
    const history = useHistory();

    const dispatch = useDispatch();
    const { scrollposition } = useSelector(state => state.home);
    const callBack = useCallback(() => { dispatch(setScrollposition()) }, [scrollposition])
    const { width } = useWindowDimensions()
    const [color, setColor] = useState('#005EEB');

    const { t } = useTranslation();

    useLayoutEffect(() => {
        window.addEventListener('scroll', () => callBack())
        return () => {
            window.removeEventListener('scroll', () => callBack())
        }
    }, []);

    // const clickArowDone = () => {
    //     if (active < 2)
    //         setActive(active + 1)
    // }

    return (
        <div>
            <HeaderHome />

            <div className={conatinerTop}>
                <div className={containerRight}>
                    <div className={titleTop}>{t('partner.title')}</div>
                    {width > 700 ? <div className={subTitleTop}>{t('partner.subTitle')}</div> : ""}
                    {width > 700 ? <div className={btnPrimary}
                        onClick={() => history.push("/aboutUs")}
                    >{t('learnMore')}</div> : ""}
                </div>

                <img src={peopleHands} className={imgTop} />
                {width < 700 ? <div className={subTitleTop}>{t('partner.subTitle')}</div> : ""}
                {width < 700 ? <div className={btnPrimary}
                    onClick={() => history.push("/aboutUs")}
                >{t('learnMore')}</div> : ""}
            </div>
            <div className={wpCardCenter}>
                <div className={cardCenter}>
                    <div className={leftCard}>
                        <div className={title} >{t('partner.weInviteYouToJoin')}</div>
                        <div className={subTitle}>{t('partner.weInviteYouToJoinDesc')}</div>
                    </div>
                    <div className={rightCard}>
                        {
                            [t('partner.truckVendor'), t('partner.shippingLine'), t('partner.freightForwarder')].map((value, index) => (
                                <div key={index} className={`${itemCard} ${index === active && activeCard}`}>{value}</div>
                            ))
                        }
                    </div>
                </div>
            </div>

            <div className={contentListItem}>
                <div className={title} style={{ margin: "0px auto 0px auto", display: "flex", maxWidth: 585, justifyContent: 'center' }}>{t('partner.whyBecomeaVendoronLogol')}</div>

                <div>
                    {
                        dataWhyLogol.map((value, index) => (
                            <div className={itemLine}>
                                {
                                    index % 2 === 0 || width < 600 ?
                                        <Fragment>
                                            <div className={`${imgLine} ${imgLineRight}`}>
                                                <img src={value.icon} alt="img" />
                                            </div>
                                            <div className={textLine}>
                                                <div className={titleS}>{t(`partner.${value.title}`)}</div>
                                                <div className={subTitle} style={{ marginTop: 16 }}>{t(`partner.${value.dec}`)}</div>
                                            </div>
                                        </Fragment>
                                        :
                                        <Fragment>
                                            <div className={textLine}>
                                                <div className={titleS}>{t(`partner.${value.title}`)}</div>
                                                <div className={subTitle} style={{ marginTop: 16 }}>{t(`partner.${value.dec}`)}</div>
                                            </div>
                                            <div className={`${imgLine} ${imgLineLeft}`}>
                                                <img src={value.icon} alt="img" />
                                            </div>
                                        </Fragment>
                                }
                            </div>
                        ))
                    }
                </div>
            </div>

            <div className={wpContentBtnList}>
                <div className={`${title} ${whyTitle}`} style={{ display: 'flex', justifyContent: 'center', textAlign: 'center' }}>{t('partner.howtoJoin')}</div>
                <div className={contentBtnList}>
                    {
                        [t('partner.filltheForm'), t('partner.review'), t('partner.approval')].map((value, index) => (
                            <Fragment>
                                <div key={index} className={btnList}>{value}</div>
                                {index < 2 ? <div className={wrpRound}>
                                    <img src={arrowRightHome} alt="arrow-right-home" />
                                </div> : ""}
                            </Fragment>
                        ))
                    }
                </div>
            </div>

            <div className={contentBottom}>
                <div className={bgBottom}></div>
                <div className={bgBottom}></div>
                <div className={bgBottom}></div>
                <div className={bgBottom}></div>

                <div className={wpBottom}>
                    <div
                        className={title}
                        style={{ display: 'flex', justifyContent: 'center', textAlign: 'center' }}>{t('partner.registerYourBusinessonOurPlatform')}</div>
                    <div className={subTitle}>{t('partner.registerYourBusinessonOurPlatformDesc')}</div>
                    <div className={"btn-yellow " + btnYellow} style={{ marginTop: 40 }}
                        onClick={() => {
                            window.open(process.env.REACT_APP_VENDOR, '_blank')
                        }}
                    >{t('partner.signupasVendor')}</div>
                </div>
            </div>
            <CustomerService />
            <div className={`wrapper-floating-scroll-top ${window.scrollY > 100 && 'wrapper-floating-scroll-top-active'}`}>
                <div className="grid" onClick={() => {
                    window.scrollTo(0, 0);
                }}>
                    <span className="showSpan">Scroll To Top</span>
                    <div>
                        <div
                            className={`floating-scroll-top ${window.scrollY > 100 && 'floating-scroll-top-active'}`}
                            onMouseOver={() => setColor('white')}
                            onMouseOut={() => setColor('#005EEB')}>
                            <ArrowUp color={color} />
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}
