import React, { useState, useEffect, useLayoutEffect } from 'react'
import { useHistory } from 'react-router-dom'
import useWindowDimensions from 'utils/windowDimensions';

import { verticalSplitSlider, viewLeft, viewRight, item } from 'assets/afterBooking.module.scss'
import FormEmail from 'components/container/register/formEmail'
import LeftDone from 'components/container/doneBooking/left'
import RightDone from 'components/container/doneBooking/right'
import RightLogin from 'components/container/truck/rightLogin'
import HeaderHome from 'components/header/HeaderHome';

export default function Index() {
    const { height, width } = useWindowDimensions();
    const history = useHistory()
    // const [redirect, setRedirect] = useState(true)
    // const performance = window.performance
    // function NoMatch() {
        // window.location.reload();
    //     return null;
    // }
    useLayoutEffect(() => {
        // console.log(NoMatch())
        // let redirect = true
        // const functionCall = async () => {
        //     console.log(redirect, "1")
        //     await history.listen(async () => {
        //         redirect = false
        //         //  await setRedirect(false)
        //         console.log(redirect, "2")
        //     });

        //     console.log(redirect, "3")
        //     setTimeout(() => {
        //         if (window.performance) {
        //             if (performance.navigation.type == 1) {
        //                 console.log(redirect, "4")
        //                 if (redirect)
        //                     history.push('/')
        //             }
        //         }
        //     }, 1000)
        // }


        // functionCall()
    })


    const positionDetailCt = {
        left: 0,
        right: -height
    }

    const positionDone = {
        left: -height,
        right: 0
    }

    const [slider, setSlider] = useState(positionDetailCt)

    return (
        <React.Fragment>
            {width <= 600 ? (
                <div className="app-home">
                    <HeaderHome activeHeader={true} />

                    <div className={viewLeft} style={{
                        top: "100px",
                        width: width > 1000 ? "50%" : "100%"
                    }}>
                        <div className={item}>
                            <FormEmail
                                noHeader
                                sliderDone={() => history.push("/")}
                            />
                        </div>
                    </div>
                </div>
            ) : (
                    <div className={verticalSplitSlider}>
                        <div className={viewLeft} style={{ top: slider.left, width: width > 1000 ? "50%" : "100%" }}>
                            <div className={item}>
                                <FormEmail
                                    sliderDone={() => setSlider(positionDone)}
                                />
                            </div>
                            <div className={item}>
                                <LeftDone />
                            </div>
                        </div>

                        {width > 1000 ?
                            <div className={viewRight} style={{ top: slider.right }}>
                                <div className={item}>
                                    <RightDone />
                                </div>
                                <div className={item}>
                                    <RightLogin />
                                </div>
                            </div>
                            : ""}
                    </div>
                )}
        </React.Fragment>
    )
}