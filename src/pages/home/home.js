import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { withTranslation } from 'react-i18next';

import LayoutHome from 'components/layout/LayoutHome'
import CustomerService from 'components/container/customerService'
import HomeWhyLogol from 'components/cards/HomeWhyLogol'
import posterImg from 'assets/images/home/posterImg.png'
import balanceImg from 'assets/images/icons/balance.svg'
import payImg from 'assets/images/icons/pay.svg'
import searchImg from 'assets/images/icons/search.svg'
import trackImg from 'assets/images/icons/track.svg'
import finishIcon from 'assets/images/svg/finish-icon.svg'
import arrowx5Fleft from 'assets/images/svg/arrowx5Fleft1.svg'
import nutrifoodImg from 'assets/images/logo/nutrifood.svg'
import tollImg from 'assets/images/logo/toll.svg'
import konoikeGroupImg from 'assets/images/logo/konoike-group.svg'
import freightImg from 'assets/images/home/fright.png'
import truckImg from 'assets/images/home/truck.png'
import eDocumentImg from 'assets/images/home/e-document.png'
import ediiLogoImg from 'assets/images/logo/edii-logo.svg'
import npct1LogoImg from 'assets/images/logo/npct1.svg'
import kojaLogoImg from 'assets/images/logo/koja-logo.svg'
import useWindowDimensions from 'utils/windowDimensions';

const dataContentEasierOrder = [
  {
    title: "findSchedule",
    desc: "findScheduleDesc",
    img: searchImg
  },
  {
    title: "compareRates",
    desc: "compareRatesDesc",
    img: balanceImg
  },
  {
    title: "instantBooking",
    desc: "instantBookingDesc",
    img: trackImg
  },
  {
    title: "securePayment",
    desc: "securePaymentDesc",
    img: payImg
  },
  {
    title: "successfulBooking",
    desc: "successfulBookingDesc",
    img: finishIcon
  }
]

const dataTab = [
  {
    title: "logistic",
    content: "seaShippingTab",
    img: freightImg,
    onClick: "/ourServices/freight"
  },
  {
    title: "freight",
    content: "inlandShippingTab",
    img: truckImg,
    onClick: "/ourServices/truck"
  },
  {
    title: "e-document",
    content: "e-DocumentTab",
    img: eDocumentImg,
    onClick: "/e-document"
  },
]

const testimoniData = [
  {
    name: "Yulita A N Pelamonia",
    company: "Konoike Transport Indonesia",
    description: "person_1_desc",
    img: konoikeGroupImg
  },
  {
    name: "Iwan Hulu ",
    company: "Shipping Department - PT. Nutrifood",
    description: "person_2_desc",
    img: nutrifoodImg
  },
  {
    name: "Mutiara",
    company: "Spil Toll",
    description: "person_3_desc",
    img: tollImg
  }
]

const LearnMore = ({ onClick, label }) => (
  <div className="learn-more" onClick={onClick}>
    <div>{label}</div>
    <div className="animation-arrow-left">
      <img src={arrowx5Fleft} alt="animation-arrow-left" className="icon-arrow-left" />
    </div>
  </div>
)

/* 
Scroll animations
*/

let rulesMap = false
let ourService = false
let mustLogol = false
let contentMembership = false
let contentEasierOrder = false
let showContentVideo = false
let showTestimoni = false
let showCustomerService = false
let showMobilePhone = false
let showSponsor = false

const setTimeOutAnimation = (theposition) => {

  if (!rulesMap && theposition > 214) {
    rulesMap = true
  }

  if (!ourService && theposition > 934) {
    ourService = true
  }

  if (!mustLogol && theposition > 1940) {
    mustLogol = true
  }

  if (!contentMembership && theposition > 2826) {
    contentMembership = true
  }

  if (!contentEasierOrder && theposition > 3112) {
    contentEasierOrder = true
  }

  if (!showContentVideo && theposition > 3779) {
    showContentVideo = true
  }

  if (!showTestimoni && theposition > 4441) {
    showTestimoni = true
  }

  if (!showMobilePhone && theposition > 5344) {
    showMobilePhone = true
  }

  if (!showSponsor && theposition > 5000) {
    showSponsor = true
  }

  if (!showCustomerService && theposition > 6312) {
    showCustomerService = true
  }
}

function Welcome({ t }) {
  // const [reasonActive, setActiveReason] = useState(0)
  const [cardActive, setActiveCard] = useState(0)
  const [play, setPlayVideo] = useState(false)
  const { scrollposition } = useSelector(state => state.home);
  const history = useHistory()
  const { location: { pathname } } = history
  const { width } = useWindowDimensions()
  const translate = t;

  /* useEffect(() => {
    console.log('Faizal Bima remarks: erase this');
    if (pathname === "/") {
      // var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
      (
        function () {
          var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
          s1.async = true;
          s1.src = 'https://embed.tawk.to/5f89d405fd4ff5477ea68a80/default';
          s1.charset = 'UTF-8';
          s1.setAttribute('crossorigin', '*');
          s0.parentNode.insertBefore(s1, s0);
        }
      )();
    }
  }) */

  const [tabOurService, setTabOurService] = useState(0)
  const descriptionTestimoni = testimoniData.filter((value, index) => index === cardActive)[0]

  setTimeOutAnimation(scrollposition)

  const onPlayVideo = () => {
    const idVideo = document.getElementById('idVideo')
    idVideo.play()
    setPlayVideo(true)
  }
  return (
    <LayoutHome>

      <div
        className="bg-image">
        <div className={`wrepper-rules-map ${rulesMap ? "item-animation" : ""}`} style={{ backgroundImage: width < 600 && "none" }}>
          <div className="container-rules">
            <div className={`text-title-bg text-title-rules`}>{translate('layoutHome.aboutLogol')}</div>
            <div className={`child-title-rules`}>{translate('layoutHome.descAboutLogol')}</div>
            <LearnMore
              onClick={() => history.push("/aboutUs")}
              label={translate('learnMore')}
            />
            <button className={`btn-primary btn-rules`} style={{ marginTop: 56 }} onClick={() => window.scrollTo(0, 0)}>{translate('bookAShipping')}</button>
          </div>
        </div>
      </div>

      <div
        className="bg-image"
        style={{ backgroundPositionY: (scrollposition - 600) * 0.2 + "px" }}
      >
        <div
          className="our-service"
        >
          <div className={`title-home  ${ourService ? "item-animation" : ""}`}>{translate('layoutHome.ourServices.title')}</div>
          <div className={`text-description-home  ${ourService ? "item-animation" : ""}`} style={{ marginTop: 32 }}>{translate('layoutHome.ourServices.subTitle')}</div>
          <div className={`tab-pane   ${ourService ? "item-animation" : ""}`}>
            {
              dataTab.map((value, index) => (
                <div className="wrepper-tab" key={index}>
                  <div className={`tab  ${tabOurService === index && "active"}`} onClick={() => setTabOurService(index)}>{translate(`layoutHome.ourServices.${value.title}`)}
                    {
                      index === dataTab.length - 1 && (
                        <span>{translate('new')}</span>
                      )
                    }
                  </div>
                  <div className="text-active" style={{ visibility: tabOurService === index ? "inherit" : "hidden" }}></div>
                </div>
              ))
            }
          </div>
          <div className={`tab-active-wp  ${ourService ? "item-animation" : ""}`}>
            {
              dataTab.map((value, index) => (
                index === tabOurService ?
                  <div className={`tab-active`} key={index}>
                    <div className="background-img">
                      <img src={value.img} alt="rectangle-img" className="fright-img" />
                    </div>

                    <div className="tab-right">
                      <div>
                        <div className="text-description">{translate(`layoutHome.ourServices.${value.content}`)}</div>
                        <LearnMore
                          onClick={() => history.push(value.onClick)}
                          label={translate('learnMore')}
                        />
                      </div>
                      <button className="btn-primary btn-next" onClick={() => window.scrollTo(0, 0)}>{translate('bookAShipping')}</button>
                    </div>
                  </div>
                  : ""))
            }
          </div>
        </div>

        <div className="must-logol">
          <div className={`title-home ${mustLogol ? "item-animation" : ""}`}>{translate('layoutHome.whyLogol.title')}</div>
          <div className={`text-description-home  ${mustLogol ? "item-animation" : ""}`}>{translate('layoutHome.whyLogol.subTitle')}</div>
          <HomeWhyLogol />
        </div>
      </div>

      <div
        className="content-membership"
        style={{ backgroundPositionY: (scrollposition - (width > 600 ? 3200 : 5000)) * 0.3 + "px" }}
      >
        <div className="wrepper-membership">
          <div>
            <div className={`title-home  ${contentMembership ? "item-animation" : ""}`} >{translate('layoutHome.signUpBanner.title')}</div>
            <div className={`text-description-home ${contentMembership ? "item-animation" : ""}`} >{translate('layoutHome.signUpBanner.subTitle')}</div>
          </div>
          <button className={`btn-yellow ${contentMembership ? "item-animation" : ""}`}
            onClick={() => {
              window.open(process.env.REACT_APP_REGISTER_USER, '_blank')
            }}
          >{translate('layoutHome.signUpBanner.signUpNow')}</button>
        </div>
      </div>

      <div
        className="content-easier-order"
        style={{ backgroundPositionY: (scrollposition - 4000) * 0.4 + "px" }}
      >
        <div className="wrepper-easier-order">
          <div>
            <div className={`title-home ${contentEasierOrder ? "item-animation" : ""}`}>{translate('layoutHome.howToUse.title')}</div>
            <div className={`text-description-home  ${contentEasierOrder ? "item-animation" : ""}`} style={{ marginTop: 32 }}>{translate('layoutHome.howToUse.subTitle')}</div>
          </div>
          <div className={`content-card  ${contentEasierOrder ? "item-animation" : ""}`}>
            {
              dataContentEasierOrder.map((value, index) => (
                <div className="wr-card-list" key={index}>
                  <div className="card-wrepper">
                    <img src={value.img} alt="search-img" />
                    <div className="child-round">{translate(`layoutHome.howToUse.${value.title}`)}</div>
                    <div className="child-desc">{translate(`layoutHome.howToUse.${value.desc}`)}</div>
                  </div>
                </div>
              ))
            }
          </div>

          <div
            className="content-video"
          >
            <div className="wrepper-video">
              <div className={`bg-video`} style={{ backgroundImage: `url(${posterImg})` }} onClick={onPlayVideo}>
                <video
                  style={{ position: play && "static" }}
                  id="idVideo"
                  controls
                  playsInline
                  className="video"
                >
                  <source
                    src={"https://logol.co.id/assets/vid/FINAL%20LOGOL%20MOGRAPH.mp4"}
                    type="video/mp4"
                  />
                  {translate('alert.browserProblem')}
                </video>
              </div>
              <div className="wrepper-right">
                <div className={`title-home  ${showContentVideo ? "item-animation" : ""}`}>{translate('layoutHome.howToUse.videoTitle')}</div>
                <div className={`text-description-home ${showContentVideo ? "item-animation" : ""}`}>{translate('layoutHome.howToUse.videoSubTitle')}</div>
                <button className={`btn-primary btn-rules  ${showContentVideo ? "item-animation" : ""}`} style={{ marginTop: 40 }} onClick={() => window.scrollTo(0, 0)}>{translate('bookAShipping')}</button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="bg-parallax-two"
        style={{ backgroundPositionY: (scrollposition - 6800) * 0.4 + "px" }}
      >
        <div className="testimoni">
          <div className="content-testimoni">
            <div className={`title-home  ${showTestimoni ? "item-animation" : ""}`}>{translate('layoutHome.testimonial.title')}</div>
            <div className={`text-description-home  ${showTestimoni ? "item-animation" : ""}`}>{translate('layoutHome.testimonial.subTitle')}</div>
          </div>

          <div className={`wrepper-testimoni  ${showTestimoni ? "item-animation" : ""}`}>
            <div>
              {
                testimoniData.map((value, index) => (
                  <div className="list-card" key={index}>
                    <div className={`select-view ${index === cardActive ? "select-view-active" : ""}`} onClick={() => setActiveCard(index)}>
                      <div className="text-name">{value.name} </div>
                      <div className="text-company">{value.company}</div>
                    </div>
                  </div>
                ))
              }
            </div>

            <div className="testimoni-wrapper">
              <div className="card-testimoni">
                <img src={descriptionTestimoni.img} alt="nutrifood-logo-lmg" />
                <div className="text-testimoni">{translate(`layoutHome.testimonial.${descriptionTestimoni.description}`)}</div>
              </div>
            </div>
          </div>
        </div>

        <div className="wp-content-logo">
          <div className={`title-home  ${showSponsor ? "item-animation" : ""}`}>{translate(`layoutHome.ourPartner`)}</div>
          <div className="content-logo">
            <img src={ediiLogoImg} alt="edii-logo-img" className={`${showSponsor ? "item-animation" : ""}`} />
            <img src={npct1LogoImg} alt="npct1-logo-lmg" className={`${showSponsor ? "item-animation" : ""}`} />
            <img src={kojaLogoImg} alt="koja-logo-img" className={`${showSponsor ? "item-animation" : ""}`} />
          </div>
        </div>
      </div>
      <CustomerService />
    </LayoutHome >
  )
}

export default withTranslation()(Welcome);