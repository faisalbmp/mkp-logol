import React from 'react';
import Styles from 'assets/scss/freight/freightDokumen.module.scss';
import { BorderedPDF, EyeIcon, FreightDownload } from 'components/icon/iconSvg';

const DetailBarang = () => {

  const LabelPreview = ({ title, content, additionContentStyles }) => {
    return (
      <div className={Styles.labelPreview}>
        <text className={Styles.textTitle}>
          {title}
        </text>
        <text style={additionContentStyles}>
          {content}
        </text>
      </div>
    );
  }

  const RingkasanFooter = ({ title, content, additionStyles, additionHeaderStyles }) => {
    return (
      <div
        className={Styles.ringkasanFooter}
        style={additionStyles}>
        <text
          className={Styles.header}
          style={additionHeaderStyles}>{title}</text>
        <text className={Styles.content}>{content}</text>
      </div>
    );
  };

  return (
    <div>

      <div className={Styles.rowSpaceBetween}>
        <div className={Styles.row}>
          <LabelPreview
            title="Selesai"
            content="5 Dokumen" />
          <LabelPreview
            title="Dalam Proses"
            content="1 Dokumen" />
          <LabelPreview
            title="Total Dokumen"
            content="6 Dokumen" />
        </div>

        <button className={Styles.buttonSendDokumen}>
          Kirim Dokumen Asli
        </button>
      </div>

      <table className={Styles.table}>
        <thead>
          <tr>
            <td colSpan={2}>Dokumen</td>
            <td>Tanggal Upload</td>
            <td>Terakhir Dirubah </td>
            <td>Ukuran</td>
            <td>Status</td>
            <td colSpan={2}>Aksi</td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td
              colSpan={2}>
              <div className={Styles.gridDokumen}>
                <BorderedPDF />
                <text className={Styles.textDokumen}>Shipping Instruction</text>
              </div>
            </td>
            <td>12 Jan 2020 - 22:29 WIB</td>
            <td>12 Jan 2020 - 22:29 WIB</td>
            <td>5 MB</td>
            <td>
              Selesai
            </td>
            <td colSpan={2}>
              <div className={Styles.gridDokumen}>
                <EyeIcon color="#004DFF" />
                <div className={Styles.gapItem} />
                <FreightDownload />
              </div>
            </td>
          </tr>
          <tr>
            <td
              colSpan={2}>
              <div className={Styles.gridDokumen}>
                <BorderedPDF />
                <text className={Styles.textDokumen}>Shipping Instruction</text>
              </div>
            </td>
            <td>12 Jan 2020 - 22:29 WIB</td>
            <td>12 Jan 2020 - 22:29 WIB</td>
            <td>5 MB</td>
            <td>
              Selesai
            </td>
            <td colSpan={2}>
              <div className={Styles.gridDokumen}>
                <EyeIcon color="#004DFF" />
                <div className={Styles.gapItem} />
                <FreightDownload />
              </div>
            </td>
          </tr>
          <tr>
            <td
              colSpan={2}>
              <div className={Styles.gridDokumen}>
                <BorderedPDF />
                <text className={Styles.textDokumen}>Shipping Instruction</text>
              </div>
            </td>
            <td>12 Jan 2020 - 22:29 WIB</td>
            <td>12 Jan 2020 - 22:29 WIB</td>
            <td>5 MB</td>
            <td>
              Selesai
            </td>
            <td colSpan={2}>
              <div className={Styles.gridDokumen}>
                <EyeIcon color="#004DFF" />
                <div className={Styles.gapItem} />
                <FreightDownload />
              </div>
            </td>
          </tr>
          <tr>
            <td
              colSpan={2}>
              <div className={Styles.gridDokumen}>
                <BorderedPDF />
                <text className={Styles.textDokumen}>Shipping Instruction</text>
              </div>
            </td>
            <td>12 Jan 2020 - 22:29 WIB</td>
            <td>12 Jan 2020 - 22:29 WIB</td>
            <td>5 MB</td>
            <td>
              Selesai
            </td>
            <td colSpan={2}>
              <div className={Styles.gridDokumen}>
                <EyeIcon color="#004DFF" />
                <div className={Styles.gapItem} />
                <FreightDownload />
              </div>
            </td>
          </tr>
        </tbody>
      </table>

      <RingkasanFooter
        title="Aksi Pengirim"
        content="Belum ada"
        additionHeaderStyles={{ backgroundColor: '#FF9D3B' }} />
    </div>
  );
};

export default DetailBarang;
