import React from 'react';
import Styles from 'assets/scss/freight/freightKontainer.module.scss'

const DetailBarang = () => {
  return (
    <div>
      <table className={Styles.table}>
        <thead>
          <tr>
            <td>No Kontainer</td>
            <td>No Seal</td>
            <td>Tipe </td>
            <td>Berat</td>
            <td>Status</td>
            <td>Waktu</td>
            <td>Armada</td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className={Styles.noKontainer}>MCU-121112</td>
            <td>XX-121111</td>
            <td>40’ General Purpose</td>
            <td>18.000 Kg</td>
            <td>
              <text className={Styles.status}>Proses di Pelabuhan</text>
            </td>
            <td>13 Jun : 12.00 WIB</td>
            <td>Truk</td>
          </tr>
          <tr>
            <td className={Styles.noKontainer}>MCU-121112</td>
            <td>XX-121111</td>
            <td>40’ General Purpose</td>
            <td>18.000 Kg</td>
            <td>
              <text className={Styles.status}>Proses di Pelabuhan</text>
            </td>
            <td>13 Jun : 12.00 WIB</td>
            <td>Truk</td>
          </tr>
          <tr>
            <td className={Styles.noKontainer}>MCU-121112</td>
            <td>XX-121111</td>
            <td>40’ General Purpose</td>
            <td>18.000 Kg</td>
            <td>
              <text className={Styles.status}>Proses di Pelabuhan</text>
            </td>
            <td>13 Jun : 12.00 WIB</td>
            <td>Truk</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default DetailBarang;
