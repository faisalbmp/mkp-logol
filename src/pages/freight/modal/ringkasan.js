import React from 'react';
import Styles from 'assets/scss/freight/freightRingkasan.module.scss';
import FreightButtonOutline from 'components/button/FreightButtonOutline';

const Ringkasan = () => {
  const LabelPreview = ({ title, content, additionContentStyles }) => {
    return (
      <div className={Styles.labelPreview}>
        <text className={Styles.textTitle}>
          {title}
        </text>
        <text style={additionContentStyles}>
          {content}
        </text>
      </div>
    );
  }

  const RingkasanFooter = ({ title, content, additionStyles, additionHeaderStyles }) => {
    return (
      <div
        className={Styles.ringkasanFooter}
        style={additionStyles}>
        <text
          className={Styles.header}
          style={additionHeaderStyles}>{title}</text>
        <text className={Styles.content}>{content}</text>
      </div>
    );
  };

  return (
    <div>
      <div className={Styles.rowFlex}>
        <LabelPreview
          title="No. Pesanan"
          content="0004112513"
          additionContentStyles={{ color: '#0045FF' }} />
        <LabelPreview
          title="No. Booking Kapal"
          content="OF/202007/E0053" />
        <LabelPreview
          title="No. B/L"
          content="0004112513" />
        <LabelPreview
          title="No. Shipping Instruction"
          content="0004112513" />
        <LabelPreview
          title="No. Delivery Order"
          content="0004112513" />
      </div>
      <div className={Styles.row}>
        <LabelPreview
          title="Rute Pengiriman"
          content="(CY) Tanjung Priok, Jakar..." />
        <LabelPreview
          title="ETD -  ETA"
          content="12 Jun - 22 Jun 2020" />
        <LabelPreview
          title="Pelayaran"
          content="OOCL" />
        <LabelPreview
          title="Vessel"
          content="ONEPIECE" />
        <LabelPreview
          title="Voyyage No."
          content="909P" />
        <LabelPreview
          title="Jumlah "
          content="3 Kontainer" />
      </div>

      <RingkasanFooter
        title="Informasi Akhir"
        content="22 Juni 2020 : 12.00 | Kontainer dalam perjalanan ke pelabuhan Tanjung Priok"
        additionStyles={{ marginTop: 32 }} />

      <RingkasanFooter
        title="Aksi Pengirim"
        content="Belum ada"
        additionHeaderStyles={{ backgroundColor: '#FF9D3B' }} />

      <div className={Styles.footerButton}>
        <FreightButtonOutline
          disalbed
          title="Pesan Lagi" />
        <FreightButtonOutline
          title="Batalkan Pesanan"
          additionStyles={{ marginLeft: 20, marginRight: 20 }} />
        <FreightButtonOutline
          title="Unduh Informasi" />
      </div>
    </div>
  );
};

export default Ringkasan;