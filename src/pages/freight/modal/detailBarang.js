import React, { useState } from 'react';
import InputSelect from 'components/checkBox/select-box';
import Styles from 'assets/scss/freight/freightDetailBarang.module.scss'
import FreightButtonOutline from 'components/button/FreightButtonOutline';

const DetailBarang = () => {
  const [showJenisKontainer, setShowJenisKontainer] = useState(false);
  const [showKontainer, setShowKontainer] = useState(false);
  const [formData, setFormData] = useState({
    jenis_terminal: "20' General Purpose",
    kontainer: 'Kontainer 1',
  });
  const jenisKontainer = [
    {
      id: 0,
      title: "20' General Purpose",
    },
    {
      id: 1,
      title: "21' General Purpose",
    },
  ];
  const kontainer = [
    {
      id: 0,
      title: "Kontainer 1",
    },
    {
      id: 1,
      title: "Kontainer 2",
    },
  ];

  return (
    <div>
      <div className={Styles.terminalWrapper}>
        <InputSelect
          title='Jenis Kontainer'
          openList={showJenisKontainer}
          selectList={jenisKontainer}
          value={formData.jenis_terminal}
          headerStyle={{ width: 267, height: 42 }}
          openSelectList={() => setShowJenisKontainer((prevState) => {
            return !prevState;
          })}
          selectedList={(item, index) => {
            setShowJenisKontainer(false)
            setFormData((prevState) => {
              return {
                ...prevState,
                jenis_terminal: item,
              }
            })
          }} />
        <div className={Styles.gapItem} />
        <InputSelect
          title='Kontainer'
          openList={showKontainer}
          selectList={kontainer}
          value={formData.kontainer}
          headerStyle={{ width: 267, height: 42, }}
          openSelectList={() => setShowKontainer((prevState) => {
            return !prevState;
          })}
          selectedList={(item, index) => {
            setShowKontainer(false)
            setFormData((prevState) => {
              return {
                ...prevState,
                kontainer: item,
              }
            })
          }} />
        <div className={Styles.gapItem} />
        <div style={{ flexDirection: 'column', display: 'flex', }}>
          <div className="depo-checkbox-container-title" >
            <span>Jenis Barang Kiriman</span>
          </div>
          <FreightButtonOutline
            disalbed
            title="General Goods"
            additionStyles={{ backgroundColor: '#F5F5F5', fontSize: 16, fontWeight: 'normal' }} />
        </div>
      </div>

      <table className={Styles.table}>
        <thead>
          <tr>
            <td>Nama Barang</td>
            <td>Jumlah Barang</td>
            <td>Netto</td>
            <td>Gross</td>
            <td>Nilai Barang</td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Coconut Bricket</td>
            <td>
              <div className={Styles.row}>
                10000
              <text className={Styles.textSatuan}>Pallets</text>
              </div>
            </td>
            <td>
              <div className={Styles.row}>
                1.000
              <text className={Styles.textSatuan}>Kg</text>
              </div>
            </td>
            <td>
              <div className={Styles.row}>
                1.500
            <text className={Styles.textSatuan}>Kg</text>
              </div>
            </td>
            <td>
              <div className={Styles.row}>
                <text className={Styles.textSatuan}>IDR. </text>
              1.000.000
              </div>
            </td>
          </tr>
          <tr>
            <td>Coconut Bricket</td>
            <td>
              <div className={Styles.row}>
                10000
              <text className={Styles.textSatuan}>Pallets</text>
              </div>
            </td>
            <td>
              <div className={Styles.row}>
                1.000
              <text className={Styles.textSatuan}>Kg</text>
              </div>
            </td>
            <td>
              <div className={Styles.row}>
                1.500
            <text className={Styles.textSatuan}>Kg</text>
              </div>
            </td>
            <td>
              <div className={Styles.row}>
                <text className={Styles.textSatuan}>IDR. </text>
              1.000.000
              </div>
            </td>
          </tr>
          <tr>
            <td>Coconut Bricket</td>
            <td>
              <div className={Styles.row}>
                10000
              <text className={Styles.textSatuan}>Pallets</text>
              </div>
            </td>
            <td>
              <div className={Styles.row}>
                1.000
              <text className={Styles.textSatuan}>Kg</text>
              </div>
            </td>
            <td>
              <div className={Styles.row}>
                1.500
            <text className={Styles.textSatuan}>Kg</text>
              </div>
            </td>
            <td>
              <div className={Styles.row}>
                <text className={Styles.textSatuan}>IDR. </text>
              1.000.000
              </div>
            </td>
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <td>Total</td>
            <td>
              <div className={Styles.row}>
                10000 Pallets
              </div>
            </td>
            <td>
              <div className={Styles.row}>
                1.000 Kg
              <text></text>
              </div>
            </td>
            <td>
              <div className={Styles.row}>
                1.000 Kg
              </div>
            </td>
            <td>
              <div className={Styles.row}>
                IDR. 1.000.000
              </div>
            </td>
          </tr>
        </tfoot>
      </table>
    </div>
  );
};

export default DetailBarang;
