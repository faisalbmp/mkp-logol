import React, { useState, useEffect, useLayoutEffect, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import Styles from 'assets/scss/dashboard/searchresult.module.scss';
import './schedule.scss';
import { BookingMenu } from 'components/pickers';
import { ResultCardDashboard } from 'components/cards';
import { SearchFilter } from 'components/filters';
import HeaderHome from 'components/header/HeaderHome';
import { setScrollposition } from 'actions';

import useWindowDimensions from 'utils/windowDimensions';

const Schedule = props => {
	const results = ['', '', '', ''];
	const { booking } = useSelector(state => state);
	const dispatch = useDispatch();
	// const translate = t;
	// console.log(this.props.history);
	const { scrollposition, typeBooking } = useSelector(state => state.home);
	const [color, setColor] = useState('#005EEB');
	const { width } = useWindowDimensions();
	useEffect(() => {}, []);

	const callBack = useCallback(() => {
		dispatch(setScrollposition());
	}, [scrollposition]);

	useLayoutEffect(() => {
		window.addEventListener('scroll', () => callBack());
		return () => {
			window.removeEventListener('scroll', () => callBack());
		};
	}, []);
	return (
		<div className='app-home'>
			<div className='layout-home'>
				<HeaderHome />
			</div>
			<div className='container'>
				<div className={Styles.container}>
					<div className={Styles.bookingMenu}>
						<BookingMenu booking={booking} />
					</div>
					{/* <div className={Styles.searchFilter}>
						<SearchFilter />
					</div> */}
					<div className='search-result'>
						<div
							style={{
								width: 300,
								height: 100,
								boxSizing: 'border-box',
								border: '2px solid',
							}}
						></div>
						<div className={Styles.result}>
							<div className={Styles.resultContent}>
								{results.map((result, index) => (
									<ResultCardDashboard key={index} />
								))}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Schedule;
