import React, { useState } from 'react';
import useWindowDimensions from 'utils/windowDimensions';

import {
	verticalSplitSlider,
	viewLeft,
	viewRight,
	item,
} from 'assets/afterBooking.module.scss';
import Login from 'components/container/login';
import Register from 'components/container/register';
import LeftDone from 'components/container/doneBooking/left';
import RightDone from 'components/container/doneBooking/right';
import RightLogin from 'components/container/freight/rightLogin';
import RightRegister from 'components/container/freight/rightRegister';
import AddTruckLeft from 'components/container/freight/addTruckLeft';
import AddTruckRight from 'components/container/freight/addTruckRight';
import MapRight from 'components/container/freight/mapRight';

export default function Index() {
	const { height } = useWindowDimensions();

	const positionAddTruck = {
		left: 0,
		right: -height * 4,
	};

	const positionMap = {
		left: 0,
		right: -height * 3,
	};

	const positionLogin = {
		left: -height,
		right: -height * 2,
	};

	const positionRegister = {
		left: -height * 2,
		right: -height,
	};

	const positionDone = {
		left: -height * 3,
		right: 0,
	};

	const [slider, setSlider] = useState(positionAddTruck);

	return (
		<div className={verticalSplitSlider}>
			<div className={viewLeft} style={{ top: slider.left }}>
				<div className={item}>
					<AddTruckLeft
						sliderLogin={() => setSlider(positionLogin)}
						sliderMap={() => setSlider(positionMap)}
					/>
				</div>
				<div className={item}>
					<Login
						sliderRegister={() => setSlider(positionRegister)}
						sliderDone={() => setSlider(positionDone)}
					/>
				</div>
				<div className={item}>
					<Register
						sliderLogin={() => setSlider(positionLogin)}
						sliderDone={() => setSlider(positionDone)}
					/>
				</div>
				<div className={item}>
					<LeftDone />
				</div>
			</div>

			<div className={viewRight} style={{ top: slider.right }}>
				<div className={item}>
					<RightDone />
				</div>
				<div className={item}>
					<RightRegister />
				</div>
				<div className={item}>
					<RightLogin />
				</div>
				<div className={item}>
					<MapRight />
				</div>
				<div className={item}>
					<AddTruckRight />
				</div>
			</div>
		</div>
	);
}
