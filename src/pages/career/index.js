
import React, { useState } from 'react'
// import { useSelector, useDispatch, } from 'react-redux';

import HeaderHome from 'components/header/HeaderHome'
import Footer from 'components/footer'
import CustomerService from 'components/container/customerService'

import { cardSearch, title, titleS, inputSearch, wpInputSearch, iconSearch, btnSearch, containerTop, cardTop, cardBottom, btnRound, orTx, btnRoundBlock, wpTitleCard, cardCollapse, middleCard, titleCC, subCC, titleRightCC, rightCC, subRightCC, cardRightCC, wpIcon, activeCardC, noActiveCardC, headerCard, contentCenter, wpContentCenter, cardCenter, description, leftJoinOur, rightJoinOur, wrJoinOur, infoBtn, cardSlider, rightCS, childText, boldText, roundIndicator, indicator, indicatorActive, topCardSlider, wpIndicator, contentGallery, boxGallery, bgImage, bgImage2 } from 'assets/scss/career/index.module.scss'
import { btnYellow } from 'assets/scss/ourServices/index.module.scss'

import searchIcon from 'assets/images/svg/search-icon.svg'
import instagramImg from 'assets/images/icons/instagram.svg'
import twitterImg from 'assets/images/icons/twitter.svg'
import facebookImg from 'assets/images/icons/facebook.svg'
import otherIcon from 'assets/images/svg/otherIcon.svg'
// import otherIcon from 'assets/images/career/bg-map.png'
import iconLogol from 'assets/images/svg/logo-logol.svg'
import manImg1 from 'assets/images/career/man-1.png'
import img1 from 'assets/images/career/img-1.png'
import img2 from 'assets/images/career/img-2.png'
import img3 from 'assets/images/career/img-3.png'
import quotesStartIcon from 'assets/images/png/quotes-start-icon.png'
import quotesLastIcon from 'assets/images/png/quotes-last-icon.png'




import { Arrowx5Fup1Icon } from 'components/icon/iconSvg'
import { useHistory } from 'react-router-dom'

const contentsDm = [
    "• Maintain and improve the service quality standards by delivering insights (related to service, product, system, and guideline) & managing quality initiatives as part of continuous improvement.",
    "• Help customer service representatives to improve their performance.",
    "• Responsible to assess, evaluate, and calibrate the service quality process.",
    "• Work with internal and external teams to ensure quality standards.",
    "• Ensure the coaching and refreshment process working properly."
]

const contentsDm2 = [
    "• Min. bachelor degree from any major (Business, Management, and Psychology is preferable).",
    "• Strong critical thinking, analytical skills, and problem-solving capabilities.",
    "• Excellent interpersonal, verbal, and written communication skills",
    "•  Experienced in customer support min. 2 years or quality assurance related min. 1 year",
    "•  Have sufficient knowledge on total quality management and CRM tools (Point plus if having experience on QA tools)",
    "• Have experience in partnership is point plus.",
    "• Great in collaboration, coordination, and monitoring.",
    "• Attention to detail."
]

const dataOperasional = [
    {
        title: "Service Excellence Associate (Operation)",
        contents: [
            {
                title: "Job Description: ",
                contents: contentsDm
            },
            {
                title: "Requirments: ",
                contents: contentsDm2
            }
        ]
    },
    {
        title: "Ocean Freight Specialist",
        contents: [
            {
                title: "Job Description: ",
                contents: contentsDm
            },
            {
                title: "Requirments: ",
                contents: contentsDm2
            }
        ]
    }
]


const dataMarketing = [
    {
        title: "Sr. Digital Marketing",
        contents: [
            {
                title: "Job Description: ",
                contents: contentsDm
            },
            {
                title: "Requirments: ",
                contents: contentsDm2
            }
        ]
    },
]


const dataTechDesign = [
    {
        title: "Sr. Graphic Designer",
        contents: [
            {
                title: "Job Description: ",
                contents: contentsDm
            },
            {
                title: "Requirments: ",
                contents: contentsDm2
            }
        ]
    }
]


const ComponentCC = ({ index, value, active, setActive }) => (
    <div className={`${cardCollapse}  ${active === index ? activeCardC : noActiveCardC}`} key={index} onClick={() => setActive(active === index ? null : index)}>
        <div className={middleCard}>
            <div className={headerCard}>
                <Arrowx5Fup1Icon
                    style={active === index ? {
                        transform: "rotate(90deg)",
                        color: "#0045FF"
                    } : { color: "#0045FF" }}
                />
                <div className={titleCC} style={{ marginLeft: 24 }}>{value.title}</div>
            </div>
            <div style={{ marginLeft: 30 }}>
                {
                    value.contents.map((item, iItem) => (
                        <div key={iItem} style={{ display: 'flex', flexDirection: "column" }}>
                            <div className={titleCC} style={{ marginTop: 24 }}>{item.title}</div>
                            {
                                item.contents.map((itemChild, iIC) => (
                                    <td key={iIC} className={subCC}>{itemChild}</td>
                                ))
                            }
                        </div>
                    ))
                }
            </div>
        </div>
        <div className={rightCC} style={{ visibility: index !== active ? "hidden" : "visible" }}>
            <div className={cardRightCC}>
                <div className={titleRightCC}>Lokasi:</div>
                <div className={subRightCC}>Jakarta, Indonesia</div>

                <div className={titleRightCC}>Waktu:</div>
                <div className={subRightCC}>1 July 2020 - 31 Agust 2010</div>
                <div className={btnRoundBlock} style={{ marginTop: 37 }}>Lamar</div>
            </div>
            <div className={titleRightCC} style={{ textAlign: "center" }}>Share</div>
            <div className={wpIcon}>
                <img src={instagramImg} alt="instagram-img" />
                <img src={twitterImg} alt="twitter-img" />
                <img src={facebookImg} alt="facebook-img" />
                <img src={otherIcon} alt="facebook-img" />
            </div>
        </div>
    </div>
)


export default function Index() {
    const [active1, setActive1] = useState(0)
    const [active2, setActive2] = useState(null)
    const [active3, setActive3] = useState(null)
    const [activeIndex, setActiveIndex] = useState(0)
    const history = useHistory()

    return (
        <div>
            <HeaderHome activeHeader={true} />
            {/* <div
                className={contentBg3}
                style={{ backgroundPositionY: (scrollposition + -3900) * 0.2 + "px" }}
            >
            </div> */}

            <div className={containerTop}>
                <div className={cardSearch}>
                    <div className={cardTop}>
                        <div className={titleS}>Cari Karirmu disini</div>
                        <div className={wpInputSearch}>
                            <img src={searchIcon} />
                            <input className={inputSearch} placeholder="Product Designer" />
                            <div className={btnSearch}>Cari</div>
                        </div>
                    </div>
                    <div className={cardBottom}>
                        {
                            ["Operasional (2)", "Marketing (1)", "Tech & Design (1)"].map((value, index) => (
                                <div key={index} className={btnRound}>{value}</div>
                            ))
                        }
                    </div>
                </div>
                <div className={orTx}>atau</div>
                <div className={btnRoundBlock}>Lihat Semua Lowongan</div>

                <div className={wpTitleCard} style={{ marginTop: 147 }}>
                    <div className={title}>Operasional</div>
                    <div className={title}>2 Lowongan</div>
                </div>

                {
                    dataOperasional.map((value, index) => (
                        <ComponentCC
                            value={value}
                            active={active1}
                            setActive={setActive1}
                            index={index}
                        />
                    ))
                }

                <div className={wpTitleCard} style={{ marginTop: 147 }}>
                    <div className={title}>Marketing</div>
                    <div className={title}>1 Lowongan</div>
                </div>

                {
                    dataMarketing.map((value, index) => (
                        <ComponentCC
                            value={value}
                            active={active2}
                            setActive={setActive2}
                            index={index}
                        />
                    ))
                }

                <div className={wpTitleCard} style={{ marginTop: 147 }}>
                    <div className={title}>{"Tech & Design"}</div>
                    <div className={title}>1 Lowongan</div>
                </div>

                {
                    dataTechDesign.map((value, index) => (
                        <ComponentCC
                            value={value}
                            active={active3}
                            setActive={setActive3}
                            index={index}
                        />
                    ))
                }
            </div>


            <div className={contentCenter}>
                <div className={bgImage}></div>
                <div className={bgImage}></div>
                <div className={bgImage}></div>
                <div className={bgImage}></div>

                <div className={wpContentCenter}>
                    <div className={cardCenter}>
                        <img src={iconLogol} />
                        <div className={titleCC}>Marketplace Logistik Terdepan</div>
                        <div className={description}>
                            Sebagai platform logistik digital, Logol berkomitmen untuk menciptakan ekosistem logistik yang lebih efisien, mudah, dan transparan melalui solusi digitalisasi dan teknologi serta merangkul seluruh elemen dalam industri logistik di Indonesia.
                        </div>
                    </div>
                </div>

                <div className={bgImage2}>
                    <div className={title}> Jadilah Bagian dari Transformasi Ekosistem Logistik Masa Depan</div>
                    <div className={"btn-yellow " + btnYellow} style={{ marginTop: 48 }}
                        onClick={() => history.push("/aboutUs")}
                    >Pelajari Lebih Lanjut</div>
                </div>
            </div>

            <div className={contentGallery}>
                <div style={{ width: "100%" }}>
                    <div className={title}>Bahagia, Sehat dan Terinspirasi</div>
                    <div className={description}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent scelerisque vehicula erat vel placerat. </div>
                </div>
                <div className={boxGallery}></div>
                <img src={img1} />
            </div>

            <div className={contentGallery} style={{ gridTemplateColumns: "216px auto 216px", marginTop: 24 }}>
                <img src={img2} />
                <img src={img3} />
                <div className={boxGallery}></div>
            </div>

            <div className={cardSlider}>
                <div className={topCardSlider}>
                    <img src={manImg1} />
                    <div className={rightCS}>
                        <div className={title}>"I’ve always wanted to build the best tech company to contribute back to society."</div>
                        <div className={boldText}>Joko Prianto</div>
                        <div className={childText}>HR Team Lead</div>
                    </div>
                </div>

                <div className={wpIndicator}>
                    {
                        [1, 2, 3, 4].map((val, i) => (
                            <div className={roundIndicator} style={{ borderColor: activeIndex === i ? '#0045ff' : '#ffffff' }}>
                                <div key={i} className={`${indicator} ${activeIndex === i ? indicatorActive : ""}`} onClick={() => setActiveIndex(i)}></div>
                            </div>
                        ))
                    }
                </div>
            </div>


            <div className={wrJoinOur}>
                <div className={leftJoinOur}>
                    <div className={title}>Benefit yang Akan Kamu Dapatkan</div>
                    <div className={description} >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent scelerisque vehicula erat vel placerat. </div>
                </div>

                <div className={rightJoinOur}>
                    <div className={infoBtn}>Ansuransi</div>
                    <div className={infoBtn}>Waktu Kerja Fleksibel</div>
                    <div className={infoBtn}>Jenjang Karir</div>
                </div>
            </div>

            <CustomerService />
            <Footer />
        </div >
    )
}
