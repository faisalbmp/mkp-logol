import React, { useState } from "react"
import Modal from 'components/modal/wrepperModal';
// import Styles from '../../index.module.scss';
import { MegaMenu } from "components/pickers";
import { useDispatch, useSelector } from "react-redux";
import RedirectModal from "components/modal/redirectPopup";
import { withTranslation } from "react-i18next";
import { ScheduleBooking } from "components/cards";
import NextBtn from "components/button/nextBtn";
import moment from "moment";
import { months } from "utils/dates";
import i18n from "i18next";
import * as slActions from "actions/vesselSchedules";
import { useHistory } from "react-router";

function EditSearchModal({ t, ...props }) {
    const translate = t;
    const selectedLang = i18n.language;

    const dispatch = useDispatch();
    const history = useHistory();

    const [portPicker, setPortPicker] = useState(false);
    const [showModal, setShowModal] = useState(false);
    const [validationInput, setValidationInput] = useState(false);

    const { home, booking, vesselSchedules } = useSelector((state) => state);
    const { typeBooking, bookingTruck, listContainer } = home;
    const {
        selected_origin,
        selected_destination,
        selectedDate,
        selectedContainer,
    } = booking;
    const { addressA, addressB, date, container } = bookingTruck;

    const { isLoading } = vesselSchedules;

    const onRedirect = () => {
        setShowModal(true);
        // interval()
    };


    const cariJadwal = () => {
        if (typeBooking === "truck") {
            if (
                Object.keys(addressA).length &&
                Object.keys(addressB).length &&
                listContainer.length &&
                typeof date !== "string"
            ) {
                // history.push("/bookingTruck");
                history.push("/requestSchedule");
            } else {
                setValidationInput(true);
            }
        } else {
            // history.push("/schedules");
            // history.push("/handleSchedules");

            // if (selected_origin && selected_destination && selectedDate) {
            // 	// history.push("/bookingTruck");
            // 	history.push('/schedules');
            // } else {
            // 	setValidationInput(true);
            // }
            // console.log(selected_origin);
            // console.log(selected_destination);
            if (
                selected_origin &&
                selected_destination &&
                selectedDate
            ) {
                const monthId = months.find(
                    (month) => month.id === selectedDate?.monthId
                )?.id;

                const dDate = `${moment(selectedDate?.departure).format("YYYY")}-${moment(selectedDate?.departure).format("MM")
                    }-${moment(selectedDate?.departure)?.format("DD").toString().padStart(2, "0")}`;
                const aDate = `${moment(selectedDate?.arrival).format("YYYY")}-${moment(selectedDate?.arrival).format("MM")
                    }-${moment(selectedDate?.arrival)?.format("DD").toString().padStart(2, "0")}`;


                // console.log(dDate);
                const dateRange = selectedDate?.range;
                // history.push("/bookingTruck");
                dispatch(
                    slActions.getVesselSchedules(
                        selected_origin.internationalCode,
                        selected_destination.internationalCode,
                        dDate,
                        aDate,
                        dateRange
                    )
                )
                    .then((res) => {
                        if (res === 1) {
                            history.push("/schedules");
                            props.onClose()
                        } else {
                            // history.push("/handleSchedules");
                            history.push("/schedules");
                            props.onClose()
                        }
                    })
                    .catch((err) => {
                        // console.log(err);
                    });
            } else {
                setValidationInput(true);
            }
        }
    };

    return (
        <>
            <Modal width={1128} height={856} onClose={props.onClose} show={props.show}>

                <section style={{ backgroundColor: "#fff" }} className="intro-search">
                    <div className="intro-card-container">
                        <div className="tab-pane">
                            {/* <ScheduleBooking /> */}
                            <MegaMenu
                                onOriginClick={() => setPortPicker(!portPicker)}
                                locationA={translate("scheduleBooking.from")}
                                locationB={translate("scheduleBooking.to")}
                                typeBooking={typeBooking}
                                time={translate("scheduleBooking.shippingTime")}
                                container={translate("scheduleBooking.shippingType")}
                                onRedirect={onRedirect}
                                validationInput={validationInput}
                                setValidationInput={() => setValidationInput(true)}
                                // booking={props.booking}
                                {...props.booking}
                            />
                        </div>
                    </div>
                </section>
                <NextBtn
                    title={translate("layoutHome.howToUse.findSchedule")}
                    width={selectedLang === "en" ? 200 : 190}
                    height={60}
                    style={{
                        width: window.innerWidth > 480 && (selectedLang === "en" ? 200 : 190),
                        height: 60,
                        margin:
                            window.innerWidth > 480 ? "16px 0px 0px auto" : "75px 0px 0px auto",
                    }}
                    loading={isLoading}
                    onClick={cariJadwal}
                // onClick={onRedirect}
                />
            </Modal>
            {/* <RedirectModal show={showModal} onClose={() => setShowModal(false)} /> */}
        </>
    )
}

export default withTranslation()(EditSearchModal);
