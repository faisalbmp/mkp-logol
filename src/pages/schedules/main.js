import React, { Component } from "react";
import Styles from "assets/scss/schedules/main.module.scss";
import HeaderHome from "components/header/HeaderHome";
import { BookingMenu } from "components/pickers";
import { BookingFilter } from "components/filters";
import { LabelM } from "components/label";
// import { ButtonM } from "components/button";
import { Colors } from "configs";
import { SelectBoxM } from "components/select";
import { ResultCard } from "components/cards";
import { setCookie } from "utils/cookies";
import moment from "moment";
import { withTranslation } from "react-i18next";
import { months } from "utils/dates";
import { getCookie } from "utils/cookies";
import Pagination from "components/paginations/New/Pagination";

class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      results: [{}, {}, {}],
      isCookie: false,
      activeFilter: [],
      durationMin: 1,
      durationMax: 20,
      currentPage: 1,
      selectionDate: {},
    };
  }

  componentDidMount() {
    const {
      actions,
      selected_origin,
      selected_destination,
      selectedDate,
      selectedContainer,
    } = this.props;

    if (selected_origin && selected_destination && selectedDate) {
      setCookie(
        "bookingOptions",
        JSON.stringify({
          origin: selected_origin,
          destination: selected_destination,
          date: selectedDate,
          container: selectedContainer,
        }),
        2
      );
    }

    const cookie = getCookie("bookingOptions");
    if (cookie) {
      this.setState({ isCookie: true });
    } else {
      return;
    }
    const booking = JSON.parse(cookie);
    const { origin, destination, date } = booking;

    const monthId = months.find((month) => month.id === date?.monthId)?.id;

    const dDate = `${moment(date?.departure).format("YYYY")}-${moment(date?.departure).format("MM")
      }-${moment(date?.departure)?.format("DD").toString().padStart(2, "0")}`;
    const aDate = `${moment(date?.arrival).format("YYYY")}-${moment(date?.arrival).format("MM")
      }-${moment(date?.arrival)?.format("DD").toString().padStart(2, "0")}`;

    const dateRange = date?.range;

    // actions.getSchedules({
    // 	origin_port_id: selected_origin?.id,
    // 	destination_port_id: selected_destination?.id,
    // 	book_loading_date: selectedDate,
    // 	cargo_id: null,
    // 	container_id: null,
    // 	container_number: null,
    // });
    actions.getVesselSchedules(
      origin.internationalCode,
      destination.internationalCode,
      dDate,
      aDate,
      dateRange
    );
    actions.fetchShippingLines();
  }

  componentWillUnmount() {
    document.cookie = `bookingOptions=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
  }

  componentDidUpdate(prevProps) {

    const {
      actions,
      selected_origin,
      selected_destination,
      selectedDate,
      selectedContainer,
    } = this.props;

    if (
      selected_origin !== prevProps.selected_origin ||
      selected_destination !== prevProps.selected_destination ||
      selectedDate !== prevProps.selectedDate
    ) {
      setCookie(
        "bookingOptions",
        JSON.stringify({
          origin: selected_origin,
          destination: selected_destination,
          date: selectedDate,
          container: selectedContainer,
        }),
        2
      );
    }
  }

  onFilterChange = (filter) => {
    const { activeFilter } = this.state;
    const { schedules } = this.props;
    if (filter === "ALL") {
      if (activeFilter.length === schedules.shippingLines.length) {
        this.setState({ activeFilter: [] });
      } else {
        this.setState({
          activeFilter: schedules.shippingLines.map((filter) => filter.name),
        });
      }
    } else {
      if (activeFilter.includes(filter)) {
        const filterIndex = activeFilter.indexOf(filter);
        const newFilter = [...activeFilter];
        newFilter.splice(filterIndex, 1);
        this.setState({ activeFilter: newFilter });
      } else {
        this.setState({ activeFilter: [...activeFilter, filter] });
      }
    }
  };

  checkedHandler = (filter) => {
    const { activeFilter } = this.state;
    return activeFilter.includes(filter);
  };

  resetHandler = () => {
    this.setState({ activeFilter: [] });
    this.setState({ durationMin: 1, durationMax: 5 });
  };

  durationFilter = (value) => {
    const min = Math.min(...value);
    const max = Math.max(...value);
    this.setState({ durationMin: min, durationMax: max });
  };

  gotoPage = (page) => {
    const currentPage = Math.max(
      0,
      Math.min(page, this.props.schedules.elements)
    );
    this.setState({ currentPage: currentPage });
  };

  handleClick = (page) => {
    // e.preventDefault();
    this.gotoPage(page);
  };

  handleMoveLeft = () => {
    // e.preventDefault();
    if (this.state.currentPage > 1) {
      this.setState({ currentPage: this.state.currentPage - 1 });
    }
  };

  handleMoveRight = (totalPages) => {
    // e.preventDefault();
    if (this.state.currentPage < totalPages) {
      this.setState({ currentPage: this.state.currentPage + 1 });
    }
  };

  rangeFilter = (date) => {
    this.setState({
      selectionDate: date
    })
  }

  render() {
    const { t } = this.props;
    const { results, activeFilter, durationMin, durationMax, selectionDate } = this.state;
    let filteredList;
    const cookie = getCookie("bookingOptions");
    const booking = JSON.parse(cookie);
    const indexOfLastPost = this.state.currentPage * 5;
    const indexOfFirstPost = indexOfLastPost - 5;


    if (
      activeFilter.length === 0 ||
      activeFilter.length === this.props.schedules.shippingLines.length
    ) {
      // filteredList = this.props.schedules.schedules;
      // if (durationMin >=0  || durationMax  5) {
      filteredList = this.props.schedules.schedules.filter(
        (item) =>
          parseInt(item.duration) >= durationMin &&
          parseInt(item.duration) <= durationMax
      );
      // }
    } else {
      filteredList = this.props.schedules.schedules.filter(
        (item) =>
          this.state.activeFilter.includes(item.shippingName) &&
          parseInt(item.duration) >= durationMin &&
          parseInt(item.duration) <= durationMax
      );
    }
    filteredList = this.state.selectionDate.id ?
      filteredList?.filter(
        (item) =>
          moment(item.arriveDate).isSameOrAfter(moment(`2021-${this.state.selectionDate?.monthId}-${this.state.selectionDate?.bottom}`)) &&
          moment(item.arriveDate).isSameOrBefore(moment(`2021-${this.state.selectionDate?.monthId}-${this.state.selectionDate?.top}`))
      )
      : filteredList
    const sortByItem = [
      { title: t("vSchedules.lowestPrice") },
      { title: t("vSchedules.earliestDeparture") },
      { title: t("vSchedules.lastDeparture") },
      { title: t("vSchedules.earliestArrival") },
      { title: t("vSchedules.lastArrival") },
    ];

    let content = "";
    if (this.props.schedules.isLoading) {
      content = "Loading";
    } else {
      content = (
        <div className={Styles.resultContent}>
          {filteredList
            .slice(indexOfFirstPost, indexOfLastPost)
            .map((schedule, index) => (
              <ResultCard key={index} hidePrice schedule={schedule} t={t} />
            ))}
        </div>
      );
    }
    return (
      <div className={Styles.container}>
        <HeaderHome className="schedules" />
        <div className={Styles.content}>
          <div className={Styles.bookingMenu}>
            <BookingMenu hideContainerMenu booking={booking} />
          </div>
          <div className={Styles.divider}>
            <div className={Styles.filter}>
              <LabelM>
                {t("vSchedules.showing")} {results.length}{" "}
                {`${t("vSchedules.shippingSchedule")}`}
              </LabelM>
              <div className={Styles.filterContent}>
                <BookingFilter
                  shippingLines={this.props.schedules.shippingLines}
                  filterHandler={this.onFilterChange}
                  durationFilter={this.durationFilter}
                  rangeFilter={this.rangeFilter}
                  activeFilter={activeFilter}
                  reset={this.resetHandler}
                />
              </div>
            </div>
            <div className={Styles.result}>
              <div
                style={{
                  backgroundColor: Colors.base.white,
                  border: `1px solid ${Colors.text.lightgrey}`,
                  alignSelf: "flex-end",
                }}
              >
                <SelectBoxM
                  isRoundCheckBox
                  width={223}
                  label={t("vSchedules.sortBy")}
                  items={sortByItem}
                />
              </div>
              {content}
              <div className={Styles.pagination}>
                <Pagination
                  totalRecords={filteredList.length}
                  pageLimit={5}
                  pageNeighbours={1}
                  currentPage={this.state.currentPage}
                  handleClick={this.handleClick}
                  handleMoveLeft={this.handleMoveLeft}
                  handleMoveRight={this.handleMoveRight}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation()(Main);
