
import React from 'react'

import HeaderHome from 'components/header/HeaderHome'
import Footer from 'components/footer'
import { header } from 'assets/scss/aboutUs/index.module.scss'

export default function Index() {
    return (
        <div>
            <div className={header}>
                <HeaderHome />
            </div>
            <div style={{ height: 800, justifyContent: 'center', alignItems: 'center', display: 'flex', fontWeight: "bold", fontSize: 34 }}>
                <div>Page Not Found</div>
            </div>
            <Footer />
        </div>
    )
}