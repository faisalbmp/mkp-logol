
import React, { useState, Fragment } from 'react'

import HeaderHome from 'components/header/HeaderHome'
import Footer from 'components/footer'
import "assets/scss/termsAndConditions/index.scss"
import { ArrowUp } from 'components/icon/iconSvg';

export default function Index() {
    const [color, setColor] = useState('#005EEB');
    return (
        <Fragment>
            <HeaderHome activeHeader={true} />
            <div className="container">
                <div className="children">
                    <section className="arch-content m-2 h-100">
                        <div className="term-and-condition">
                            <h4 className="title-term-and-condition">SYARAT DAN KETENTUAN PENGGUNAAN APLIKASI LOGOL UNTUK PENGGUNA</h4>
                            <div className="form-control-custom">
                                <div className="col-xs-11">
                                    <p className="solid">
                                        <b>MOHON BACA SELURUH SYARAT DAN KETENTUAN INI DENGAN SEKSAMA SEBELUM MENGAKSES ATAU MENGGUNAKAN APLIKASI LOGOL, LAYANAN LOGOL, DAN TRANSPORTASI YANG DISEDIAKAN OLEH LOGOL DAN/ATAU MITRA PENGANGKUT YANG MERUPAKAN KONTRAKTOR INDEPENDEN</b>
                                    </p>
                                </div>
                            </div>
                            <br />
                            <div className="modal-row">
                                <h5>
                                    Dengan mendaftar dan/atau menggunakan Perangkat Lunak LOGOL,
                                    Pengguna menyatakan bahwa Pengguna telah membaca, mengerti dan
                                    setuju untuk terikat secara hukum oleh Syarat dan Ketentuan
                                    ini dan dokumen-dokumen sebagaimana dimaksud di dalamnya.
												</h5>
                            </div>
                            <div className="modal-row">
                                <h5>
                                    Syarat dan Ketentuan ini berlaku efektif per tanggal 15 April 2020. Apabila Pengguna memiliki pertanyaan,
															silahkan menghubungi email LOGOL di <span className="text-blue">cs@logol.co.id</span>, atau melalui telepon ke nomor <span className="text-blue">021-24523147.</span>
                                    Semua korespondensi Pengguna akan dicatat, direkam dan disimpan untuk arsip LOGOL.
												</h5>
                            </div>
                            <br />
                            <h4><b>1.	DEFINISI</b></h4>
                            <div className="modal-row">
                                <h5 className="text-number">1.1</h5>
                                <h5>"LOGOL" mengacu kepada PT LOGOL JAKARTA MITRAINDO, atau afiliasinya, tergantung pada konteks (bersama-sama disebut sebagai LOGOL).</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">1.2</h5>
                                <h5>"Mitra Pengangkut" mengacu kepada pemberi jasa angkutan barang yang telah sepakat dengan LOGOL untuk menerima pemesanan pengiriman
													     dari Pengguna sebagai bentuk Layanan</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">1.3</h5>
                                <h5>"Muatan" mengacu kepada segala barang, container, parsel, paket, baik satu ataupun lebih, yang dikirim sebagai bentuk Layanan</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">1.4</h5>
                                <h5>
                                    "Layanan" atau "Layanan-layanan" mengacu kepada layanan yang disediakan oleh LOGOL dimana, melalui Perangkat Lunak, Pengguna membuat
                                    pesanan untuk pengiriman Muatan dan Mitra Pengangkut menerima pesanan dari Pengguna, dan operasi atau kegiatan LOGOL lainnya termasuk
                                    mencocokan pesanan pengiriman Pengguna dengan Mitra Pengangkut dan memfasilitasi pengiriman Muatan, seperti menyediakan informasi
                                    lokasi titik pengangkutan dan penurunan, kontrak informasi Pengguna dan Mitra Pengangkut, dan segala informasi lainnya terkait pengiriman dan layanan
													  yang dipesan oleh Pengguna kepada Mitra Pengangkut.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">1.5</h5>
                                <h5>"Perangkat Lunak" mengacu kepada teknologi, situs web, dan/atau aplikasi LOGOL</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">1.6</h5>
                                <h5>"Syarat dan Ketentuan" mengacu kepada Syarat dan Ketentuan secara keseluruhan, yang berlaku untuk Layanan, sebagaimana ditetapkan dalam www.logol.co.id dan segala perubahan yang terdapat di dalamnya dari waktu ke waktu oleh LOGOL dan dipublikasikan di dalam situs web LOGOL</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">1.7</h5>
                                <h5>
                                    "Pengiriman" mengacu kepada layanan yang dilaksanakan oleh Mitra Pengangkut dimana termasuk ketentuan menyediakan kendaraan atau transportasi dan membawa
                                    Muatan dari titik A ke titik B, termasuk bantuan nyata pada saat pengiriman dan pengangkutan barang termasuk ketentuan atau bantuan tambahan untuk
													   pengangkutan dan penurunan Muatan</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">1.8</h5>
                                <h5>
                                    "Pengguna" mengacu kepada setiap orang atau pelanggan LOGOL yang mengakses, mengunduh, mendaftar, atau menggunakan Perangkat Lunak untuk membuat pesanan
													   pengiriman Muatan melalui Layanan.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">1.9</h5>
                                <h5>
                                    "Situs Web" mengacu kepada <a style={{ textDecoration: "underline" }} href="https://logol.co.id">www.logol.co.id</a> dan/atau setiap subdomainnya.</h5>
                            </div>
                            <br />
                            <h4><b>2.	KEWAJIBAN PENGGUNA</b></h4>
                            <div className="modal-row">
                                <h5 className="text-number">2.1</h5>
                                <h5>
                                    Pengguna harus menjamin dan memastikan bahwa dia adalah pemilik sebenarnya dari Muatan atau wakil dari pemilik Muatan
                                    yang dikirim dan memiliki wewenang untuk menerima Syarat dan Ketentuan yang berlaku atau sebagai wakil sah dari pemilik
													   Muatan tersebut.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">2.2</h5>
                                <h5>
                                    Pengguna harus memastikan bahwa Muatan yang dikirim telah siap untuk dikirim dan dibungkus dengan baik dan sanggup
                                    menahan tekanan yang wajar dari kondisi perjalanan seperti ketika memuat barang, mengangkut barang, pengangkutan dengan
                                    kendaraan bermotor, dan bongkar barang dari kendaraan. Pengguna harus menyegel setiap barang dengan sangat hati-hati
													   untuk menghindari barang rusak, robek, bocor, tumpah, menyusut, atau kehilangan dan kerugian lainnya.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">2.3</h5>
                                <h5>
                                    Sebelum pengiriman dilakukan, Pengguna harus memberitahu LOGOL setiap tindakan pencegahan yang harus diterapkan untuk
													  menangani Muatan sesuai dengan sifatnya.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">2.4</h5>
                                <h5>
                                    Pengguna diwajibkan, apabila diminta atau diperlukan oleh LOGOL atau Mitra Pengangkut, memberikan deskripsi Muatan yang
                                    terperinci. Dengan memberikan hal tersebut kepada LOGOL dan/atau Mitra Pengangkut, Pengguna menjamin bahwa deskripsi
													  Muatan adalah jujur dan benar pada saat pemesanan dilakukan, dan selama Layanan dan Pengiriman akan dilakukan.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">2.5</h5>
                                <h5>
                                    Pengguna menjamin bahwa, pada saat menggunakan Layanan dan pengiriman Muatan, Pengguna mematuhi segala hukum dan peraturan
                                    perundang-undangan terkait dengan sifat, kondisi, pengepakan, penanganan, penyimpanan dan pembawaan Muatan. Pengguna juga
                                    menjamin bahwa Muatan bukan merupakan barang yang terlarang oleh hukum, merupakan barang berbahaya atau beracun, sangat
													mudah rusak, dan/atau mengandung radioaktif.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">2.6</h5>
                                <h5>
                                    Pengguna bertanggung jawab penuh untuk kehilangan atau kerusakan yang diderita oleh Mitra Pengangkut atau pihak ketiga
													lainnya yang merupakan akibat dari pelanggaran Syarat dan Ketentuan di atas.</h5>
                            </div>

                            <br />
                            <h4>
                                <b>3.	LARANGAN PENGGUNA</b></h4>
                            <div className="modal-row">
                                <h5 className="text-number">3.1</h5>
                                <h5>
                                    Pengguna dengan cara apapun tidak akan menggunakan Layanan atau Perangkat Lunak untuk melakukan, menyebabkan, mendorong,
                                    mempromosikan, atau mendukung setiap tindakan ilegal atau melanggar hak hukum maupun privasi dari setiap orang termasuk
														melacak, menguntit, dan melecehkan orang lain.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">3.2</h5>
                                <h5>
                                    Pengguna tidak akan menggunakan Layanan atau Perangkat Lunak untuk menghasilkan atau menyebarkan iklan, spam, atau segala
														jenis pesan yang dapat menyebabkan gangguan bagi pihak manapun.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">3.3</h5>
                                <h5>
                                    Pengguna tidak akan mengambil tindakan apapun yang dapat menyebabkan keterlambatan, gangguan, atau kerusakan sistem
														operasi Layanan maupun server jaringan yang terkait dengan Perangkat Lunak.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">3.4</h5>
                                <h5>
                                    Pengguna tidak akan membuat upaya untuk mengganggu, melemahkan, atau merusak Layanan atau usaha LOGOL, sebagai contoh,
                                    mencoba untuk menghindari penggunaan Layanan atau Perangkat Lunak, tidak hadir pada titik pengangkutan, atau tidak
                                    mempersiapkan barang untuk pengangkutan pada tanggal dan waktu pengangkutan yang ditentukan oleh Pengguna. LOGOL berhak
                                    sepenuhnya untuk menentukan tindakan mana yang dianggap sebagai merugikan dan dapat memutuskan untuk menahan ketentuan
														Layanan atau melarang akses kepada Perangkat Lunak</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">3.5</h5>
                                <h5>
                                    Pengguna tidak akan melakukan misrepresentasi dalam menggunakan Layanan atau Perangkat Lunak termasuk penggunaan Layanan
                                    atau Perangkat Lunak atas nama atau untuk keuntungan orang lain yang tidak diberikan persetujuan olehnya. Pengguna
                                    bertanggung jawab untuk penggunaan Perangkat Lunak yang diunduh di dalam alat/device Pengguna dan bertanggung jawab atas
                                    penggunaan Perangkat Lunak atau permintaan Layanan yang dilakukan melalui akun Pengguna tersebut, walaupun tanpa izin
                                    Pengguna. Pengguna juga tidak akan memperbolehkan orang lain untuk menggunakan Perangkat Lunak yang diunduh di alat/device
														Pengguna.</h5>
                            </div>
                            <br />
                            <h4><b>4.	DATA PRIBADI PENGGUNA</b></h4>
                            <div className="modal-row">
                                <h5 className="text-number">4.1</h5>
                                <h5>
                                    Pengguna memastikan bahwa data pribadi yang diberikan Pengguna tersebut benar adanya dan merupakan data terbaru.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">4.2</h5>
                                <h5>
                                    Pengguna setuju untuk mengizinkan LOGOL atas penggunaan data pribadinya (termasuk namun tidak terbatas pada nama,
                                    nomor kontak, email, dan alamat) dan memberikan data tersebut kepada Mitra Pengangkut untuk menyediakan Layanan dan
                                    mengkoordinir Pengiriman. Pengguna juga memberikan LOGOL kewenangan untuk menyatukan, mengumpulkan, menyimpan, dan
                                    memperbaharui data pribadi Pengguna sedemikian rupa, untuk dan pada periode waktu tertentu sebagaimana diperlukan
														pada saat LOGOL menyediakan Layanan.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">4.3</h5>
                                <h5>
                                    Pengguna setuju dan menyadari bahwa penyatuan, pengumpulan, penyimpanan, penggunaan dan pemberian data pribadi
                                    Pengguna tunduk kepada Privasi dan Pengumpulan Data Pribadi LOGOL ("Kebijakan Privasi") sebagaimana ditampilkan dan
                                    diubah secara periodik di Situs Web dan/atau Perangkat Lunak. Pengguna juga menyadari bahwa dia telah membaca dan
														memahami Kebijakan Privasi sebelum menyetujui Syarat dan Ketentuan.</h5>
                            </div>
                            <br />
                            <h4><b>5.	KEWAJIBAN MITRA PENGANGKUT</b></h4>
                            <div className="modal-row">
                                <h5 className="text-number">5.1</h5>
                                <h5>
                                    Mitra Pengangkut akan membuat usaha yang wajar secara komersial dalam rangka pengiriman Muatan sesuai dengan waktu
                                    dan jadwal perkiraan. Pengguna menyadari bahwa waktu dan jadwal perkiraan hanyalah perkiraan dan mungkin tidak akurat
														karena faktor eksternal seperti kecelakaan, kemacetan, cuaca, kematian listrik, dan gangguan sipil.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">5.2</h5>
                                <h5>
                                    Mitra Pengangkut akan, dalam keadaan yang terjadi pada saat Pengiriman, melakukan tindakan pencegahan yang wajar untuk
                                    mencegah segala kehilangan atau kerusakan Muatan di luar kategori pemakaian dan kerusakan dari kondisi pengiriman biasa
														atau segala percobaan akses yang tidak berwenang atau tidak sah atas Muatan oleh pihak manapun.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">5.3</h5>
                                <h5>
                                    Dalam rangka Mitra Pengangkut tidak dapat mengikuti kewajibannya di bawah Pasal 5 karena keadaan di luar kuasanya,
                                    Mitra Pengangkut akan memberikan usaha yang wajar secara komersial untuk menangani segala kehilangan atau kerusakan
														yang mungkin terjadi pada Muatan.</h5>
                            </div>
                            <br />
                            <h4><b>6.	TANGGUNG JAWAB TERBATAS MITRA PENGANGKUT</b></h4>
                            <div className="modal-row">
                                <h5 className="text-number">6.1</h5>
                                <h5>
                                    Mitra Pengangkut tidak bertanggung jawab atas segala kerugian atau kerusakan Muatan yang timbul dari atau berkaitan
														dengan tindakan, kegagalan, atau keadaan berikut ini:</h5>
                            </div>
                            <div style={{ marginLeft: 34 }}>
                                <div className="modal-row">
                                    <h5 className="text-number">6.1.1</h5>
                                    <h5>Kegagalan Pengguna untuk melakukan kewajibannya di bawah Pasal 2.2 atau 2.3 untuk mematuhi kewajibannya di
														        bawah Pasal 2.4 Syarat dan Ketentuan ini</h5>
                                </div>
                                <div className="modal-row">
                                    <h5 className="text-number">6.1.2</h5>
                                    <h5>Pelanggaran Pengguna atas jaminannya di bawah Pasal 2.5 Syarat dan Ketentuan ini</h5>
                                </div>
                                <div className="modal-row">
                                    <h5 className="text-number">6.1.3</h5>
                                    <h5>Kegagalan Mitra Pengangkut untuk melakukan kewajibannya karena keadaan di luar kontrolnya</h5>
                                </div>
                                <div className="modal-row">
                                    <h5 className="text-number">6.1.4</h5>
                                    <h5>Setiap tindakan, yang disengaja atau karena kelalaian, yang dilakukan oleh pihak lain selain Mitra Pengangkut.</h5>
                                </div>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">6.2</h5>
                                <h5>
                                    Kecuali disebutkan dalam Pasal 6.3, Mitra Pengangkut tidak memiliki hak untuk membuka atau memeriksa barang selama
                                    pengiriman sehingga dilepaskan tanggung jawabnya atas legalitas hukum dan/atau peraturan mengenai barang yang dibawa.
                                    LOGOL maupun Mitra Pengangkut dapat diizinkan untuk memeriksa barang pengiriman disaksikan oleh Pengguna atau
														perwakilannya untuk memastikan sesuai dengan Pasal 2.4.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">6.3</h5>
                                <h5>
                                    Dalam keadaan dimana LOGOL atau Mitra Pengangkut memiliki dasar yang beralasan untuk percaya bahwa (i) Pengguna
                                    melanggar kewajibannya dalam Pasal 2.4, dan/atau Muatan mengandung materi apapun yang dianggap berbahaya, atau dilarang
                                    oleh hukum, peraturan perundang-undangan, atau kebijakan publik, Pengguna setuju bahwa LOGOL dan/atau Mitra Pengangkut
                                    dapat, dengan kebijakannya sendiri, menolak/membatalkan pengiriman dan/atau menahan Muatan, atau segala barang atau
                                    materi tertentu yang terdapat di dalamnya, menyerahkan Muatan kepada otoritas yang berwenang, atau mengembalikan dalam
                                    hal yang sama kepada Pengguna, tanpa dibebani tanggung jawab karena keadaan barang, kehilangan kepemilikan Muatan, atau
														gagal, tidak lengkap, atau keterlambatan pengiriman Muatan.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">6.4</h5>
                                <h5>
                                    Dalam keadaan dimana LOGOL dan/atau Mitra Pengangkut diwajibkan oleh hukum, peraturan perundang-undangan, administratif
                                    judisial, atau peraturan keputusan, perintah atau keputusan, atau permintaan dari organisasi pemerintahan lainnya yang
                                    memiliki kewenangan menurut hukum, Pengguna menyadari bahwa LOGOL dan/atau Mitra Pengangkut dapat mengemukakan data pribadi
                                    dan/atau pengemukaan lainnya yang dibuat oleh Pengguna, termasuk pengemukaan Muatan menurut Pasal 2.4, dalam penjalanan
                                    Layanan dan Pengiriman. Dalam segala keadaan tersebut, LOGOL dapat, sebelum pengemukaan, apabila dapat dilakukan,
                                    memberitahukan Pengguna secara tertulis dan menyediakan bantuan kepada Pengguna untuk melindungi datanya. Pengguna menyetujui
                                    untuk tidak membebani LOGOL dan/atau Mitra Pengangkut tanggung jawab untuk segala kehilangan, atau kerusakan yang diakibatkan
														dari penundukan dari perintah atau otoritas hukum tersebut.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">6.5</h5>
                                <h5>
                                    Pengguna menyetujui untuk mengganti kerugian dan tidak membebani tanggung jawab kepada LOGOL dan/atau Mitra Pengangkut
                                    atas segala tanggung jawab, klaim, atau tuntutan hukum yang LOGOL dan/atau Mitra Pengangkut mungkin derita atau segala
                                    biaya, ganti rugi atau pengeluaran, termasuk biaya hukum, yang mungkin timbul dari pelanggaran Pengguna atas
														kewajibannya di bawah Syarat dan Ketentuan atau tindakan LOGOL dan/atau Mitra Pengangkut dalam Pasal 6.4.</h5>
                            </div>
                            <br />
                            <h4><b>7.	BIAYA-BIAYA</b></h4>
                            <div className="modal-row">
                                <h5 className="text-number">7.1</h5>
                                <h5>
                                    Biaya pengiriman akan dikenakan sesuai dengan kesepakatan sebelumnya antara Pengguna dan LOGOL, sebagaimana tercantum dalam Perangkat </h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">7.2</h5>
                                <h5>
                                    Pengguna diwajibkan membayar biaya pengiriman tersebut dengan segera setelah jatuh tempo, atau setiap promosi potongan
                                    harga untuk mendapatkan Layanan. Sebagai tambahan dari biaya pengiriman tersebut, Pengguna juga bertanggung jawab untuk
                                    membayar atau mengganti LOGOL atau Mitra Pengangkut untuk biaya-biaya tambahan yang terkena dalam menyediakan
                                    Layanan termasuk, namun tidak terbatas pada, biaya parkir, biaya tol, biaya untuk waktu tunggu tambahan, dan biaya-biaya
                                    yang terkena untuk mendapatkan izin atau izin khusus yang diperlukan untuk mengirimkan muatan untuk dan atas nama
														Pengguna.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">7.3</h5>
                                <h5>
                                    Dengan menggunakan jasa Layanan LOGOL maka Pengguna setuju untuk membayarkan semua komponen biaya yang akan ditagihkan
                                    termasuk namun tak terbatas pada komponen biaya yang ditagihkan oleh pihak Terminal Peti Kemas Koja dan dengan ini
														Pengguna menyatakan tidak akan mengajukan keberatan atas tagihan tersebut. </h5>
                            </div>
                            <br />
                            <h4><b>8.	PEMBATALAN</b></h4>
                            <h5>Biaya pembatalan sebesar 50% - 100% (lima puluh persen sampai dengan seratus persen) dari total biaya pengiriman, bergantung
													pada kesepakatan antara LOGOL dengan Pengguna, akan dikenakan jika:</h5>
                            <div className="modal-row">
                                <h5 className="text-number">8.1</h5>
                                <h5>
                                    Pengguna melakukan pembatalan setelah pesanan atas Layanan diterima oleh Mitra Pengangkut dan/atau setelah Mitra Pengangkut tiba di lokasi.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">8.2</h5>
                                <h5>
                                    Muatan tidak dapat dimuat/dimasukkan ke dalam kendaraan karena ukuran dan/atau berat Muatan melebihi ukuran dan/atau
														berat standar untuk jenis kendaraan yang telah dipilih atau ditentukan dalam Layanan</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">8.3</h5>
                                <h5>
                                    Kendaraan tidak dapat sampai di lokasi penjemputan atau pengiriman karena kondisi jalanan yang tidak memungkinkan untuk
														dilalui kendaraan, misalnya lebar jalan yang sempit dan sebagainya</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">8.4</h5>
                                <h5>
                                    Muatan tidak dapat dimuat/dimasukkan ke dalam kendaraan karena kurangnya atau tidak tersedianya
														alat bantu pindah khusus.</h5>
                            </div>
                            <br />
                            <h4><b>9.	PROMOSI</b></h4>
                            <div className="modal-row">
                                <h5 className="text-number">9.1</h5>
                                <h5>
                                    LOGOL akan, dari waktu ke waktu, memberikan promosi yang tunduk kepada ketentuan peraturan perundang-undangan yang berlaku. LOGOL berhak untuk mengubah atau menarik segala
														promosi atas kebijakannya sendiri tanpa memberikan pemberitahuan terlebih dahulu sesuai dengan persyaratan peraturan yang berlaku, apabila ada.</h5>
                            </div>
                            <br />
                            <h4><b>10.	KOMPENSASI</b></h4>
                            <div className="modal-row">
                                <h5 className="text-number">10.1</h5>
                                <h5>
                                    Tanpa prasangka terhadap Pernyataan Penyangkalan dan Batasan Tanggung Jawab dalam Pasal 11 dan apabila Pengguna telah memenuhi
                                    persyaratan klaim yang ditentukan di bawah ini, LOGOL dapat menawarkan kompensasi kepada Pengguna atas setiap kehilangan, kerusakan,
                                    atau penghancuran Muatan yang terjadi pada saat Waktu Pengiriman. Untuk menentukan kelayakan Pengguna untuk menuntut klaim untuk
                                    kompensasi berdasarkan pasal ini, Waktu Pengiriman memiliki arti, waktu dimana Muatan telah ditempatkan dengan aman di dalam kendaraan
														Mitra Pengangkut, sampai pada saat Mitra Pengangkut tiba di titik penurunan barang.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">10.2</h5>
                                <h5>
                                    Kompensasi atas segala kehilangan, kerusakan, atau penghancuran Muatan harus sesuai dengan nilai pasar Muatan yang
														hilang, rusak, atau hancur namun <b>tidak melebihi dari 10x (sepuluh kali) biaya pengiriman</b> yang telah disepakati
sebelumnya antara Pengguna dan LOGOL. LOGOL berhak untuk menentukan nilai pasar Muatan berdasarkan tarif pasar yang
														berlaku.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">10.3</h5>
                                <h5>
                                    Kecuali persyaratan di bawah ini dipenuhi, Pengguna tidak berhak untuk mendapatkan kompensasi klaim dari LOGOL.</h5>
                            </div>

                            <div className="modal-row">
                                <h5 className="text-number" style={{ marginLeft: 50 }}>a.</h5>
                                <h5>Pengguna telah memeriksa Muatan dan memberitahukan Mitra Pengangkut atas segala kehilangan, kerusakan,
                                atau kehancuran Muatan yang ditemukan sebelum menandatangani Berita Acara Serah Terima sehubungan dengan
                                telah selesainya pengiriman. Dengan ditandatanganinya Berita Acara Serah Terima tersebut, Pengguna dengan
                                demikian memberikan konfirmasi bahwa Pengguna telah memeriksa Muatan dan tidak ditemukan segala kehilangan,
														            kerusakan, atau kehancuran pada Muatan.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number" style={{ marginLeft: 50 }}>b.</h5>
                                <h5>Dalam keadaan kehilangan atau kerusakan tidak dapat ditentukan dengan kasat mata, Pengguna telah memberikan
                                klaim tertulis melalui email kepada layanan pelanggan LOGOL dalam waktu dua puluh empat (24) jam setelah Mitra
														        Pengangkut tiba di titik penurunan.</h5>
                            </div>

                            <div className="modal-row">
                                <h5 className="text-number">10.4</h5>
                                <h5>
                                    Klaim tertulis yang diberikan kepada LOGOL sebagaimana dipersyaratkan dalam Pasal 10.3 (b) di atas harus
                                    mengandung: nomor pemesanan pengiriman; deskripsi terperinci atas Muatan yang hilang, rusak, atau hancur; gambar
                                    yang jelas atas Muatan yang hilang, rusak, atau hancur; dan kemungkinan akibat kehilangan, kerusakan,
														atau kehancuran.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">10.5</h5>
                                <h5>
                                    Segala klaim tertulis yang tidak mengandung persyaratan minimal di atas akan dianggap klaim yang tidak sah dan akan
														dianggap tidak ada klaim tertulis yang telah diberikan kepada LOGOL.</h5>
                            </div>
                            <br />
                            <h4><b>11.	PERNYATAAN PENYANGKALAN LOGOL</b></h4>
                            <div className="modal-row">
                                <h5 className="text-number">11.1</h5>
                                <h5>
                                    LOGOL dan Layanan, termasuk namun tidak terbatas pada Layanan, Perangkat Lunak, dan layanan pelanggan, disediakan untuk Pengguna apa adanya.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">11.2</h5>
                                <h5>
                                    LOGOL tidak bertanggung jawab atas segala kehilangan, kerugian, tuntutan, atau biaya termasuk setiap kehilangan atau
                                    kerusakan konsekuensial, tidak langsung, atau incidental terhadap perangkat mobile atau setiap aplikasi yang
														terdapat di dalamnya, sebagai akibat dari pengunduhan atau penggunaan Perangkat Lunak LOGOL.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">11.3</h5>
                                <h5>
                                    LOGOL tidak bertanggung jawab atas setiap kehilangan atau kerugian, termasuk namun tidak terbatas kepada,
                                    cedera Pengguna atau orang lain yang mungkin derita, segala kerusakan terhadap properti yang dimiliki Pengguna
														atau pihak lainnya, yang diakibatkan oleh Layanan, dalam hal terkait Mitra Pengangkut, atau proses pengiriman Muatan.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">11.4</h5>
                                <h5>
                                    Dalam menyediakan Layanan, LOGOL tidak bertindak sebagai agen atau perwakilan dari Pengguna atau Mitra Pengangkut.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">11.5</h5>
                                <h5>
                                    LOGOL berhak untuk menghentikan atau menghapus segala hubungan dengan Pengguna atau akun Pengguna, yang dibuat untuk
                                    menggunakan Layanan atau Perangkat Lunak, apabila LOGOL yakin bahwa Pengguna melanggar ketentuan manapun dari Syarat
														dan Ketentuan ini dan segala kebijakan lainnya yang terkait.</h5>
                            </div>
                            <br />
                            <h4><b>12.	HAK KEKAYAAN INTELEKTUAL</b></h4>
                            <div className="modal-row">
                                <h5 className="text-number">12.1</h5>
                                <h5>
                                    Situs Web, Perangkat Lunak, dan segala hak, kepemilikan, dan kepentingan di dalam dan yang terkait, merupakan properti
                                    milik LOGOL, pihak yang me-lisensi, dan dilindungi oleh Hukum Hak Kekayaan Intelektual. Kecuali, apabila dapat diaplikasikan,
                                    lisensi terbatas diberikan secara nyata kepada Pengguna di dalam Syarat dan Ketentuan, LOGOL berhak untuk dirinya
														dan para pihak yang me-lisensi, seluruh hak, kepemilikan, dan kepentingan lainnya.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">12.2</h5>
                                <h5>
                                    Materi milik pihak ketiga, apabila ada, dan segala hak, kepemilikan, dan kepentingan di dalamnya, adalah properti
														milik pihak ketiga tersebut dan tunduk kepada segala lisensi, kondisi, dan/atau reservasi.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">12.3</h5>
                                <h5>
                                    Tanpa pembatasan hal di atas, Pengguna tidak diperbolehkan untuk memproduksi, menyalin, memodifikasi, menampilkan,
                                    menjual atau mendistribusikan Perangkat Lunak, atau konten yang ditampilkan di dalam Perangkat Lunak, atau
                                    menggunakannya dengan cara lainnya untuk konsumsi publik atau maksud komersial, tanpa persetujuan LOGOL,
                                    dan/atau pihak ketiga terkait. Pelanggaran atas ketentuan ini akan secara otomatis menghapus izin Pengguna
                                    untuk menggunakan Perangkat Lunak atau akses kepada Layanan, sebagaimana demikian Pengguna harus dengan segera
                                    menghancurkan segala dan seluruh salinan yang dibuat atas Perangkat Lunak dan segala konten yang ditampilkan
														 di dalamnya.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">12.4</h5>
                                <h5>
                                    Tanpa mengabaikan hal yang bertentangan yang terdapat disini, larangan ini termasuk: (a) menyalin atau mengadaptasi
                                    kode HTML yang digunakan untuk menghasilkan laman web dalam situs; (b) menggunakan atau mencoba untuk menggunakan
                                    mesin, perangkat lunak manual atau otomatis, alat-alat, perangkat, agen, naskah robot, atau hal lainnya, perangkat,
                                    mekanisme, atau proses (termasuk, namun tidak terbatas kepada, browsers, spiders, robots, avatars, atau agen pintar
                                    lainnya) untuk mengarahkan, mencari, mengakses, "scrape", "crawl", atau "spider" setiap laman web atau Layanan yang
                                    tersedia di dalam Perangkat Lunak selain daripada mesin pencarian dan agen pencarian yang tersedia dari LOGOL
                                    di dalam Perangkat Lunak dan web browser pihak ketiga (seperti Internet Explorer, Firefox, Safari, Opera, Chrome,
                                    etc.); dan (c) menggabungkan, menyalin atau menduplikat dalam arti apapun setiap konten atau informasi yang tersedia
                                    dari Perangkat Lunak, tanpa persetujuan tertulis yang nyata dari LOGOL. Penggunaan konten dari Perangkat Lunak
                                    di situs web lainnya, intranet, atau dalam lingkungan komputer jaringan tertentu untuk tujuan apapun, dilarang
														dengan keras.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">12.5</h5>
                                <h5>
                                    "LOGOL," "logo LOGOL" dan segala nama atau logo yang merupakan merek layanan atau merek dari LOGOL, dan segala produk
                                    terkait dan nama layanan, merek desain dan slogan adalah merek layanan atau merek dari LOGOL. "Tampilan dan nuansa"
                                    Perangkat Lunak dan Situs Web (termasuk, tanpa batasan, kombinasi warna, bentuk tombol, tampilan, desain, dan segala
                                    elemen geografis) juga dilindungi oleh merek, merek layanan, dan hak cipta milik LOGOL. Kode apapun yang LOGOL
                                    ciptakan atau mungkin diciptakan untuk menampilkan tampilan pada Perangkat Lunak, Situs Web atau laman Situs Web
                                    juga dilindungi oleh hak kekayaan intelektual. Segala merek dan merek layanan yang terdapat di dalam Perangkat Lunak
														dan Situs Web adalah merek atau merek layanan dari masing-masing pemiliknya.</h5>
                            </div>
                            <br />
                            <h4><b>13.	KEADAAN KAHAR</b></h4>
                            <div className="modal-row">
                                <h5 className="text-number">13.1</h5>
                                <h5>
                                    Perangkat Lunak dan/atau Mitra Pengangkut dapat diinterupsi oleh kejadian di luar kewenangan atau kontrol LOGOL
                                    ("Keadaan Kahar"), termasuk namun tidak terbatas pada gangguan listrik, gangguan telekomunikasi, peperangan,
                                    pemberontakan, kerusuhan sipil, blokade, sabotase, embargo, pemogokan dan perselisihan perburuhan lainnya, keributan,
                                    epidemi, bencana alam, gempa bumi, angin ribut, banjir atau keadaan cuaca lainnya yang merugikan, ledakan, kebakaran,
                                    perintah atau petunjuk atau kebijakan yang merugikan dari setiap Pemerintah baik "de jure" maupun "de facto" ataupun
                                    perangkatnya atau sub divisinya, dan setiap sebab lainnya yang secara wajar tidak dapat dikuasai oleh LOGOL dan/atau
                                    Mitra Pengangkut, dan yang sifatnya sedemikian rupa, sehingga mengakibatkan penundaan, pembatasan atau menghalangi
														tindakan tepat pada waktunya oleh LOGOL dan/atau Mitra Pengangkut.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">13.2</h5>
                                <h5>
                                    LOGOL tidak bertanggung jawab atas setiap penundaan atau kegagalan yang terjadi dalam pemberian Layanan sehubungan
                                    dengan Keadaan Kahar dan Pengguna setuju untuk membebaskan LOGOL dari setiap tuntutan dan tanggung jawab, jika LOGOL
                                    tidak dapat memfasilitasi Layanan, termasuk memenuhi instruksi yang Pengguna berikan melalui Perangkat Lunak, baik
														sebagian maupun seluruhnya, karena suatu Keadaan Kahar.</h5>
                            </div>
                            <br />
                            <h4><b>14.	KOMENTAR DAN SARAN DARI PENGGUNA</b></h4>
                            <div className="modal-row">
                                <h5 className="text-number">14.1</h5>
                                <h5>
                                    LOGOL menerima komentar dan saran terkait Layanan, Pengiriman, dan Perangkat Lunak, dan Situs Web. Mohon diingat,
                                    LOGOL tidak menerima atau mempertimbangkan ide, saran, ciptaan, atau materi kreatif, selain daripada pihak yang
														diminta.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">14.2</h5>
                                <h5>
                                    Apabila Pengguna memberikan saran mengenai Perangkat Lunak, Situs Web, Layanan atau Pengiriman, mohon untuk
                                    memberikan komentar yang spesifik dan tidak memberikan ide, ciptaan, saran, atau materi kreatif. Apabila anda tetap
                                    melakukannya, walau telah diberitahukan di Pasal ini, dengan mengirimkan kepada LOGOL segala saran, ide, gambar,
                                    konsep, ciptaan, atau konten atau informasi kreatif, anda mengerti dan menyetujui bahwa pengiriman tersebut akan
                                    menjadi properti LOGOL. Lalu, dengan mengirimkan hal yang sama di atas, anda memberikan hak yang tidak bisa ditarik
                                    kembali atas kiriman tersebut dan LOGOL akan memiliki hak eksklusif pada kiriman tersebut dan berhak atas
                                    penggunaannya tanpa larangan apapun, untuk tujuan apapun, tanpa memberikan kompensasi kepada anda
														atau orang lainnya.</h5>
                            </div>
                            <br />
                            <h4><b>15.	KETENTUAN LAINNYA</b></h4>
                            <div className="modal-row">
                                <h5 className="text-number">15.1</h5>
                                <h5>
                                    LOGOL dapat mengubah Syarat dan Ketentuan ini dari waktu ke waktu dan berhak untuk melakukannya tanpa memberikan
														pemberitahuan terlebih dahulu. Segala perubahan Syarat dan Ketentuan akan ditampilkan di Perangkat Lunak LOGOL.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">15.2</h5>
                                <h5>
                                    LOGOL dapat, dengan persetujuan nyata dari Pengguna, mengirim segala informasi terkait Layanan atau promosi kepada
                                    Pengguna dalam bentuk pesan elektronik, termasuk namun tidak terbatas kepada, email, SMS, pesan telepon otomatis,
                                    atau notifikasi push dari Perangkat Lunak. Pengguna memberikan izin kepada LOGOL untuk mengirimkan pesan elektronik
                                    ini kepada Pengguna melalui channel kontak yang disediakan LOGOL kepada Pengguna pada saat Pengguna mendaftarkan
														akunnya.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">15.3</h5>
                                <h5>
                                    Pengguna menerima seluruhnya dan tanpa syarat bentuk Syarat dan Ketentuan terkini sebagaimana direpresentasikan oleh
														situs web LOGOL pada setiap digunakannya Layanan.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">15.4</h5>
                                <h5>
                                    Jika salah satu istilah atau ketentuan dalam Syarat dan Ketentuan ini atau bagian daripadanya tidak sah, ilegal atau
                                    tidak dapat diberlakukan di yurisdiksi manapun, maka ketidakabsahan, ilegalitas atau tidak adanya kekuatan hukum tersebut
                                    tidak akan mempengaruhi istilah atau ketentuan lain dalam Syarat dan Ketentuan ini atau membatalkan atau mengesahkan istilah
														atau ketentuan yang tidak dapat diberlakukan tersebut di yurisdiksi lain.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">15.5</h5>
                                <h5>
                                    Syarat dan Ketentuan ini serta pelaksanaannya, atau persoalan yang timbul dari atau berhubungan dengan Syarat dan
														Ketentuan ini, akan diatur, tunduk kepada dan ditafsirkan sesuai dengan ketentuan hukum Republik Indonesia.</h5>
                            </div>
                            <div className="modal-row">
                                <h5 className="text-number">15.6</h5>
                                <h5>
                                    Setiap sengketa yang berhubungan dengan Syarat dan Ketentuan ini sedapat mungkin diselesaikan secara musyawarah antara
                                    Para Pihak. Apabila penyelesaian secara musyawarah tidak berhasil dicapai, maka setiap perselisihan, tuntutan hukum atau
                                    perkara yang timbul dari Syarat dan Ketentuan ini, akan diselesaikan melalui
														Pengadilan Negeri Jakarta Utara, Indonesia.</h5>
                            </div>
                            <br />

                            <div className="form-control-custom">
                                <div className="col-xs-11">
                                    <p className="solid"><b>
                                        Dengan mengklik kotak Saya Setuju dengan Syarat dan Ketentuan, Pengguna menyatakan bahwa Pengguna telah membaca, mengerti dan menyetujui seluruh
                                        Syarat dan Ketentuan Penggunaan Aplikasi LOGOL untuk Pengguna ini, dan dengan ini menerima setiap hak, kewajiban,
                                        dan ketentuan yang diatur di dalamnya.
                                                         </b>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div className={`wrapper-floating-scroll-top ${window.scrollY > 100 && 'wrapper-floating-scroll-top-active'}`}>
                <div className="grid" onClick={() => {
                    window.scrollTo(0, 0);
                }}>
                    <span className="showSpan">Scroll To Top</span>
                    <div>
                        <div
                            className={`floating-scroll-top ${window.scrollY > 100 && 'floating-scroll-top-active'}`}
                            onMouseOver={() => setColor('white')}
                            onMouseOut={() => setColor('#005EEB')}>
                            <ArrowUp color={color} />
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </Fragment>
    )
}
