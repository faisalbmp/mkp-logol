import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as pickerActions from "actions/pickerActions";
import * as bookingActions from "actions/bookingActions";
import Main from "./main";

const mapStateToProps = (state) => ({
  booking: state.booking,
  selected_origin: state.booking.selected_origin,
  selected_destination: state.booking.selected_destination,
  selectedDate: state.booking.selectedDate,
  selectedContainer: state.booking.selectedContainer,
});

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      Object.assign({}, pickerActions, bookingActions),
      dispatch
    ),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);
