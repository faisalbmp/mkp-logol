import React from 'react';
import Styles from 'assets/scss/schedules/main.module.scss';
import HeaderHome from 'components/header/HeaderHome';
import { BookingMenu } from 'components/pickers';
import Quotation from 'components/quotation';
import { useHistory } from 'react-router-dom';

export default props => {
	const history = useHistory();
	return (
		<div className={Styles.container}>
			<HeaderHome className='schedules' />
			<div className={Styles.content}>
				<div className={Styles.bookingMenu}>
					<BookingMenu hideContainerMenu {...props} />
				</div>
				<Quotation onClick={() => history.push('/requestSchedule')} />
			</div>
		</div>
	);
};
