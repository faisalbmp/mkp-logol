import axios from 'axios'
import { apiGoogleMap, proxyHeroku } from './init'

const ifProxyDev = proxyHeroku

export const autocompletePlace = async (text) => {
    try {
        const { data } = await axios.get(`${ifProxyDev}${apiGoogleMap}/place/autocomplete/json?input=${text}&types=establishment&components=country:id&postal_code&language=ind`)
        return data.predictions
    } catch (err) {
        return err
    }
}

export const getPlaceId = async (place_id) => {
    try {
        const { data } = await axios.get(`${ifProxyDev}${apiGoogleMap}/place/details/json?place_id=${place_id}&fields=geometry`)
        return data.result.geometry.location
    } catch (err) {
        return err
    }
}