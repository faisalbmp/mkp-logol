import axios from 'services/axios'

export const signInUser = (auth) => axios.post(`/auth/login`, auth)
export const signUpVendor = (auth) => axios.post(`/marketplace/public/vendorRegistration`, auth)
export const signUpCustomer = (auth) => axios.post(`/marketplace/public/customerRegistration`, auth)