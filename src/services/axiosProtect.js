import axios from 'axios';
import { base, vendorScheduler } from './init';
import { getCookie } from 'utils/cookies';

const token = getCookie('token');

export default axios.create({
	// timeout: 10000,
	baseURL: base,
	headers: {
		'Content-Type': 'application/json',
		'Access-Control-Allow-Origin': '*',
		Authorization: token,
	},
});
