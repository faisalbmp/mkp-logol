import axios from 'axios'
import { base } from '../init'
import { getCookie } from 'utils/cookies';

const token = getCookie('token');

export const getRequestPass = async () => {
    try {
        const response = await axios.get(`${base}/marketplace/requestGatepass`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            },
        })
        return response.data.value
    } catch (err) {
        return []
    }
};