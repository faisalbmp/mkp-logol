import { base } from 'services/init'
import axiosProtect from 'services/axiosProtect'

export const getTruckOrderListingData = (params) => axiosProtect.get(`${base}/marketplace/getOrderListing${params}`)