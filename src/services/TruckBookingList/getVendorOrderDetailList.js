import { base } from 'services/init'
import axiosProtect from 'services/axiosProtect'

export const getVendorOrderDetailList = params => axiosProtect.get(`${base}/marketplace/getVendorOrderDetailList${params}`)