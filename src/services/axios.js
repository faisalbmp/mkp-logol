import axios from 'axios';
import { base, vendorScheduler } from './init';

export default axios.create({
	// timeout: 10000,
	baseURL: base,
	headers: {
		'Content-Type': 'application/json',
		'Access-Control-Allow-Origin': '*',
	},
});
