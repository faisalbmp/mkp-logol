import axios from 'axios'
import { base } from '../init'
import { getCookie } from 'utils/cookies';

const token = getCookie('token');

export const getShippingListData = async (term) => {
    try {
        const response = await axios.get(`${base}/shippingLine/public/shippingList?terms=${term}`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            },
        })
        return response.data.value.data
    } catch (err) {
        return []
    }
};