// import axios from 'axios'
// import { base } from '../init'

import { base } from 'services/init'
import axiosProtect from 'services/axiosProtect'

export const getFactoryListData = (params) => axiosProtect.get(`${base}/factory/getFactoryList${params}`)

