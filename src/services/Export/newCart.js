import axios from 'axios'
import { base } from '../init'
import { getCookie } from 'utils/cookies';

const token = getCookie('token');

export const createNewCart = async (carData) => {
   try {
        const response = await axios.post(`${base}/cart/newCart`, carData, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json'
            },
        })
        return response.data
    } catch (err) {
        return []
    }
};