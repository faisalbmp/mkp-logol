import { base } from 'services/init'
import axiosProtect from 'services/axiosProtect'


export const getDestinationPickerID = (params) => axiosProtect.get(`${base}/destination/getDestinationPickerDs${params}`)
