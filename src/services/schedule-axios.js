import axios from "axios";
import { apiFreight } from "services/init";

export default axios.create({
  // timeout: 10000,
  baseURL: apiFreight,
  headers: {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
  },
});
