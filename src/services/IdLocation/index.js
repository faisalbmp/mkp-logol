import axios from 'services/locationService'

export const getProvinsi = () => axios.get(`/provinsi`)
export const getKota = (id) => axios.get(`/kota?id_provinsi=${id}`)
export const getKecamatan = (id) => axios.get(`/kecamatan?id_kota=${id}`)
export const getKelurahan = (id) => axios.get(`/kelurahan?id_kecamatan=${id}`)