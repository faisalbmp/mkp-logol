import axios from './schedule-axios';

export const getSchedules = (origin, destination, dDate, aDate, range) =>
	axios.get(
		`/schedule?placeOfLoading=${origin}&placeOfDischarge=${destination}&departureDate=${dDate}&range=${range}`
	);

export const getShippingLines = () => axios.get('/shipping');
