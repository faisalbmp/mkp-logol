import axios from 'axios';
import { apiLocation } from './init'

export default axios.create({
    // timeout: 10000,
    baseURL: apiLocation,
    headers: {
        'Content-Type': 'application/json'
    }
});