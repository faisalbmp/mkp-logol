import { api } from "./request";

export default {
  getOrigins: (payload) => api.get(`/port/origins`),
  getDestinations: (payload) => api.get(`/port/destinations`),
  getAll: (payload) => api.get(`/port/getAll`),
};