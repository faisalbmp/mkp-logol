const flagId = require("./png/flag-id.png");
const oocl = require("./svg/oocl.svg");
const arrowRightButton = require("./icons/arrow-right-button.png");
const longArrowRight = require("./svg/long-arrow-right.svg");
const arrowDown = require("./svg/arrow-down.svg");
const download = require("./svg/download.svg");
const print = require("./svg/print.svg");
const successIllustration = require("./png/success-illustration.png");
const mail = require("./svg/mail.svg");
const phone = require("./svg/phone.svg");
const shipFront = require("./svg/ship-front.svg");
const clock = require("./svg/clock.svg");
const container = require("./svg/container.svg");
const edit = require("./svg/edit.svg");
const truck = require("./svg/truck-plus.svg");
const leftRightArrow = require("./icons/left-right.svg");
const text = require("./svg/text.svg");

export {
  flagId,
  oocl,
  leftRightArrow,
  arrowRightButton,
  longArrowRight,
  arrowDown,
  download,
  print,
  successIllustration,
  mail,
  phone,
  shipFront,
  clock,
  container,
  edit,
  truck,
  text,
};
