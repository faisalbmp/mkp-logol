import React from 'react';

export const ETipeCont = () => (
	<svg
		width='48'
		height='48'
		viewBox='0 0 48 48'
		fill='none'
		xmlns='http://www.w3.org/2000/svg'
	>
		<circle cx='24' cy='24' r='24' fill='#1501FE' />
		<path
			d='M19.6313 31V16.9H29.0713V18.94H22.1513V22.82H28.6713V24.84H22.1513V28.96H29.0713V31H19.6313Z'
			fill='white'
		/>
	</svg>
);

export const ITipeIcon = () => (
	<svg
		width='48'
		height='48'
		viewBox='0 0 48 48'
		fill='none'
		xmlns='http://www.w3.org/2000/svg'
	>
		<circle cx='24' cy='24' r='24' fill='#00E599' />
		<path d='M22.7075 31V16.9H25.2875V31H22.7075Z' fill='white' />
	</svg>
);
